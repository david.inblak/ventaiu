/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;

public class PeticionReversoTarjetaBean
  extends PeticionBaseVentaDto
{
  private double monto;
  private String track2;
  private String terminal;
  private String afiliacion;
  private String trace;
  private String horaLocal;
  private String fechaLocal;
  private double comision;
  private boolean chip;
  private boolean falloLecturaChip;
  private long pagoTarjetaId;
  private String canalPago;
  
  public String getAfiliacion()
  {
    return afiliacion;
  }
  
  public void setAfiliacion(String afiliacion)
  {
    this.afiliacion = afiliacion;
  }
  
  public boolean isChip()
  {
    return chip;
  }
  
  public void setChip(boolean chip)
  {
    this.chip = chip;
  }
  
  public double getComision()
  {
    return comision;
  }
  
  public void setComision(double comision)
  {
    this.comision = comision;
  }
  
  public boolean isFalloLecturaChip()
  {
    return falloLecturaChip;
  }
  
  public void setFalloLecturaChip(boolean falloLecturaChip)
  {
    this.falloLecturaChip = falloLecturaChip;
  }
  
  public String getFechaLocal()
  {
    return fechaLocal;
  }
  
  public void setFechaLocal(String fechaLocal)
  {
    this.fechaLocal = fechaLocal;
  }
  
  public String getHoraLocal()
  {
    return horaLocal;
  }
  
  public void setHoraLocal(String horaLocal)
  {
    this.horaLocal = horaLocal;
  }
  
  public double getMonto()
  {
    return monto;
  }
  
  public void setMonto(double monto)
  {
    this.monto = monto;
  }
  
  public long getPagoTarjetaId()
  {
    return pagoTarjetaId;
  }
  
  public void setPagoTarjetaId(long pagoTarjetaId)
  {
    this.pagoTarjetaId = pagoTarjetaId;
  }
  
  public String getTerminal()
  {
    return terminal;
  }
  
  public void setTerminal(String terminal)
  {
    this.terminal = terminal;
  }
  
  public String getTrace()
  {
    return trace;
  }
  
  public void setTrace(String trace)
  {
    this.trace = trace;
  }
  
  public String getTrack2()
  {
    return track2;
  }
  
  public void setTrack2(String track2)
  {
    this.track2 = track2;
  }
  
  public String getCanalPago()
  {
    return canalPago;
  }
  
  public void setCanalPago(String canalPago)
  {
    this.canalPago = canalPago;
  }
  
  public String toString()
  {
    return "PeticionReversoTarjetaBean{monto=" + monto + ", track2=" + track2 + ", terminal=" + terminal + ", afiliacion=" + afiliacion + ", trace=" + trace + ", horaLocal=" + horaLocal + ", fechaLocal=" + fechaLocal + ", comision=" + comision + ", chip=" + chip + ", falloLecturaChip=" + falloLecturaChip + ", pagoTarjetaId=" + pagoTarjetaId + ", canalPago=" + canalPago + '}';
  }
}
