/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

/**
 *
 * @author fvegap
 */
public class RespuestaReversoTarjetaBean {

    private String descError;
    private int codError;

    public int getCodError() {
        return codError;
    }

    public void setCodError(int codError) {
        this.codError = codError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    @Override
    public String toString() {
        return "RespuestaReversoTarjetaBean{" + "descError=" + descError + ", codError=" + codError + '}';
    }
}
