/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neto.sion.tienda.venta.bean;

/* @author Sammy Guergachi <sguergachi at gmail.com> */
public class RespuestaActualizaConciliacionPendienteBean {
    int codigoError;
    String DescripcionError;

    public String getDescripcionError() {
        return DescripcionError;
    }

    public void setDescripcionError(String DescripcionError) {
        this.DescripcionError = DescripcionError;
    }

    public int getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }
    
}
