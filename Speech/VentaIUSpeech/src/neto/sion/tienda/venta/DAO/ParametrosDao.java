package neto.sion.tienda.venta.DAO;


import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.modelo.ParametrosBean;
import oracle.jdbc.internal.OracleTypes;

public class ParametrosDao {
ParametrosBean parametrosBean;
public ParametrosDao()
  {
    //sql = null;
    //cs = null;
    //rs = null;
  }
    
        public ParametrosBean consultaParametrosOperacion(int paisId, int sistemaId, int moduloId, int submoduloId, 
                int configuracionId)
	{
            SION.log(Modulo.VENTA,"Consultando parámetros de devolución ", Level.INFO);
                
		String sql = null;
		CallableStatement cs = null;
		ResultSet rs = null;
                
                ParametrosBean parametrosBean = new ParametrosBean(0, "", 0);
                
                try{
                   
			sql = SION.obtenerParametro(Modulo.VENTA,"USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS");
                        SION.log(Modulo.VENTA,"SQL a ejecutar... " + sql, Level.FINE);
                        cs = Conexion.prepararLlamada(sql);	
			cs.setInt(1, paisId);
                        cs.setInt(2, sistemaId);
                        cs.setInt(3, moduloId);
                        cs.setInt(4, submoduloId);
                        cs.setInt(5, configuracionId);
			cs.registerOutParameter(6, OracleTypes.NUMBER);
                        cs.registerOutParameter(7, OracleTypes.VARCHAR);
                        cs.registerOutParameter(8, OracleTypes.NUMBER);
			cs.execute();						
                        
                        parametrosBean.setErrorId(cs.getInt(6));
                        parametrosBean.setDescripcionError(cs.getString(7));
                        parametrosBean.setValorConfiguracion(cs.getDouble(8));
                        
		}
		catch(Exception e)
		{
                        SION.logearExcepcion(Modulo.VENTA, e, sql);
		}
		finally
		{
			Conexion.cerrarResultSet(rs);
			Conexion.cerrarCallableStatement(cs);
		}
    SION.log(Modulo.VENTA, "Respuesta de paràmetro: " + parametrosBean.toString(), Level.INFO);
            
                return parametrosBean;
                
        }
	
	
	
}

