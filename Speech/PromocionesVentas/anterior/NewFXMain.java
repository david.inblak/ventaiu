import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import neto.sion.tienda.venta.promociones.mvp.IComando;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesPresentador;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesRepositorio;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesVista;
import neto.sion.tienda.venta.promociones.redondeo.PresentadorPromociones;
import neto.sion.tienda.venta.promociones.redondeo.VistaPromociones;

public class NewFXMain
  extends Application
{
  public static void main(String[] args)
  {
    launch(args);
  }
  
  public void start(final Stage primaryStage)
  {
    primaryStage.setTitle("Hello World!");
    
    IPromocionesRepositorio repositorio = new RepositorioFake();
    IPromocionesVista vista = new VistaPromociones(repositorio);
    
    IPromocionesPresentador presentador = new PresentadorPromociones(vista, repositorio);
    vista.setPresentador(presentador);
    
    StackPane root = new StackPane();
    if (presentador.esVentaCandidata(Integer.valueOf(34)))
    {
      root.getChildren().add((Node)presentador.getVistaPromociones());
      presentador.iniciarVista();
      
      vista.setImporteTotal(Double.valueOf(100.0D));
      vista.setImporteRecibido(Double.valueOf(100.0D));
    }
    presentador.setComandoCerrarPromocion(new IComando()
    {
      public void executar(Object... parametro)
      {
        primaryStage.close();
      }
    });
    primaryStage.setScene(new Scene(root, 900.0D, 750.0D));
    primaryStage.show();
  }
}
