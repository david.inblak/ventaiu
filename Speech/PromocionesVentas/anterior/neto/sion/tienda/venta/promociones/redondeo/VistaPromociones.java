package neto.sion.tienda.venta.promociones.redondeo;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyCombination.Modifier;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesPresentador;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesRepositorio;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesVista;
import neto.sion.tienda.venta.promociones.redondeo.modelo.ModeloArticulo;
import neto.sion.tienda.venta.promociones.redondeo.util.Util;
import neto.sion.tienda.venta.promociones.redondeo.vista.UIArticuloPromocion;

public class VistaPromociones
  implements IPromocionesVista
{
  private IPromocionesRepositorio repositorio;
  private IPromocionesPresentador presentador;
  private Text lblTitulo_NombreProducto = new Text();
  private Text lblTitulo_PrecioDescuento = new Text();
  private Text lblTitulo_PrecioRegular = new Text();
  private VBox vBoxPrincipal;
  private AnchorPane anchorPane = new AnchorPane();
  private HBox hboxScroll = new HBox(20.0D);
  private Pane paneLoading = new Pane();
  private Pane paneError = new Pane();
  private Text txtMensajeError;
  private TextField textInputTotal = new TextField("");
  private TextField textInputCambio = new TextField("");
  private TextField textInputRecibido = new TextField("");
  private HBox cntGrey = new HBox();
  Label keyCodeF11 = new Label("F11 ");
  Label keyCodeF12 = new Label("F12 ");
  Label keyCodeF10 = new Label("F10 ");
  Button btnFinalizaVenta = new Button("Continuar Venta");
  Button btnCancelar = new Button("No acepta promoción");
  private final KeyCombination f9 = new KeyCodeCombination(KeyCode.F9, new KeyCombination.Modifier[0]);
  private final KeyCombination f10 = new KeyCodeCombination(KeyCode.F10, new KeyCombination.Modifier[0]);
  private final KeyCombination f11 = new KeyCodeCombination(KeyCode.F11, new KeyCombination.Modifier[0]);
  private final KeyCombination f12 = new KeyCodeCombination(KeyCode.F12, new KeyCombination.Modifier[0]);
  
  public VistaPromociones(IPromocionesRepositorio _repositorio)
  {
    Label kc11 = new Label();
    kc11.textProperty().bind(keyCodeF10.textProperty());
    kc11.setStyle("-fx-text-fill: #e5e5e5;");
    Label kc12 = new Label();
    kc12.textProperty().bind(keyCodeF10.textProperty());
    kc12.setStyle("-fx-text-fill: #e5e5e5;");
    try
    {
      keyCodeF11.setStyle("-fx-font: 13pt System;");
      String txt = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.btnAceptaPromo");
      btnFinalizaVenta = new Button((txt == null) || ("".contains(txt)) ? "Continuar Venta" : txt.trim(), keyCodeF11);
    }
    catch (Exception e)
    {
      btnFinalizaVenta = new Button("Continuar Venta", keyCodeF11);
    }
    try
    {
      keyCodeF12.setStyle("-fx-font: 13pt System;");
      String txt = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.btnCancelaPromo");
      btnCancelar = new Button((txt == null) || ("".contains(txt)) ? "No acepta promoción" : txt.trim(), keyCodeF12);
    }
    catch (Exception e)
    {
      btnCancelar = new Button("No acepta promoción", keyCodeF12);
    }
    String verShortCut = "";
    try
    {
      verShortCut = SION.obtenerParametro(Modulo.VENTA, "VENTA.REDONDEO.VERFS");
    }
    catch (Exception ex) {}
    if ("0".equals(verShortCut))
    {
      keyCodeF10.setText("");
      keyCodeF10.setVisible(false);
      keyCodeF11.setText("");
      keyCodeF11.setVisible(false);
      keyCodeF12.setText("");
      keyCodeF12.setVisible(false);
    }
    repositorio = _repositorio;
    
    textInputRecibido.setFocusTraversable(true);
    btnFinalizaVenta.setFocusTraversable(true);
    btnCancelar.setFocusTraversable(true);
    
    paneLoading.setPrefWidth(950.0D);
    paneLoading.setPrefHeight(600.0D);
    paneLoading.setStyle("-fx-text-fill: white; -fx-font: 35pt System;");
    AnchorPane.setLeftAnchor(paneLoading, Double.valueOf(0.0D));
    AnchorPane.setRightAnchor(paneLoading, Double.valueOf(0.0D));
    AnchorPane.setBottomAnchor(paneLoading, Double.valueOf(0.0D));
    AnchorPane.setTopAnchor(paneLoading, Double.valueOf(0.0D));
    
    ImageView imgLoading = new ImageView();
    paneLoading.getChildren().add(imgLoading);
    paneLoading.setVisible(false);
    
    paneError.setPrefWidth(750.0D);
    paneError.setPrefHeight(600.0D);
    paneError.setStyle("-fx-text-fill: white; -fx-font: 35pt System;");
    AnchorPane.setLeftAnchor(paneError, Double.valueOf(0.0D));
    AnchorPane.setRightAnchor(paneError, Double.valueOf(0.0D));
    AnchorPane.setBottomAnchor(paneError, Double.valueOf(0.0D));
    AnchorPane.setTopAnchor(paneError, Double.valueOf(0.0D));
    paneError.setVisible(false);
    
    VBox vb2 = new VBox(20.0D);
    vb2.setAlignment(Pos.CENTER);
    vb2.setFillWidth(true);
    vb2.setPrefHeight(120.0D);
    vb2.setPrefWidth(500.0D);
    vb2.setMinHeight(70.0D);
    vb2.setStyle("-fx-background-color:#f93535;");
    
    txtMensajeError = new Text("");
    txtMensajeError.setFill(Color.WHITE);
    txtMensajeError.setTextAlignment(TextAlignment.CENTER);
    txtMensajeError.setWrappingWidth(450.0D);
    txtMensajeError.setStyle("-fx-font: 17pt System;");
    VBox.setVgrow(txtMensajeError, Priority.ALWAYS);
    
    vb2.getChildren().add(txtMensajeError);
    
    Button btn1 = new Button("Aceptar");
    btn1.setStyle("-fx-background-color: #9c1421;-fx-border-color: #ffffff;-fx-text-color: #FFFFFF;-fx-font: 15pt System;");
    
    vb2.getChildren().add(btn1);
    
    vBoxPrincipal = new VBox(10.0D);
    vBoxPrincipal.alignmentProperty().setValue(Pos.TOP_CENTER);
    vBoxPrincipal.setPrefWidth(900.0D);
    vBoxPrincipal.setPrefHeight(750.0D);
    vBoxPrincipal.setStyle("-fx-background-color: #ffffff;-fx-border-color: #cccccc;-fx-background-radius: 10 10 10 10;");
    vBoxPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler()
    {
      public void handle(KeyEvent arg0)
      {
        if (arg0.getCode().equals(KeyCode.ESCAPE)) {
          cerrarPromociones();
        }
      }
    });
    vBoxPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler()
    {
      public void handle(KeyEvent ke)
      {
        if (f12.match(ke)) {
          btnCancelar.fire();
        } else if (f11.match(ke)) {
          btnFinalizaVenta.fire();
        } else if (f10.match(ke)) {
          textInputRecibido.requestFocus();
        } else if (!f9.match(ke)) {}
      }
    });
    VBox hbTitulo = new VBox();
    hbTitulo.alignmentProperty().setValue(Pos.CENTER);
    hbTitulo.setPadding(new Insets(10.0D));
    
    VBox.setVgrow(hbTitulo, Priority.ALWAYS);
    HBox.setHgrow(hbTitulo, Priority.ALWAYS);
    
    Text lblTitulo1 = new Text();
    
    HBox.setHgrow(lblTitulo1, Priority.ALWAYS);
    HBox.setHgrow(lblTitulo_NombreProducto, Priority.ALWAYS);
    
    Text lblTitulo3 = new Text();
    
    HBox.setHgrow(lblTitulo3, Priority.ALWAYS);
    HBox.setHgrow(lblTitulo_PrecioDescuento, Priority.ALWAYS);
    
    Text lblTitulo5 = new Text();
    HBox.setHgrow(lblTitulo5, Priority.ALWAYS);
    HBox.setHgrow(lblTitulo_PrecioRegular, Priority.ALWAYS);
    try
    {
      lblTitulo1.setText(SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.1"));
      lblTitulo3.setText(SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.3"));
      lblTitulo5.setText(SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.5"));
      int fontSize = Integer.parseInt(SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.fontSize"));
      
      lblTitulo1.setFont(Font.font(null, FontWeight.NORMAL, fontSize));
      lblTitulo_NombreProducto.setFont(Font.font(null, FontWeight.BOLD, fontSize));
      lblTitulo3.setFont(Font.font(null, FontWeight.NORMAL, fontSize));
      lblTitulo_PrecioDescuento.setFont(Font.font(null, FontWeight.BOLD, fontSize));
      lblTitulo5.setFont(Font.font(null, FontWeight.NORMAL, fontSize));
      lblTitulo_PrecioRegular.setFont(Font.font(null, FontWeight.NORMAL, fontSize));
      
      hbTitulo.setStyle(SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.stylebackground"));
      
      String colorBase = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.styleTextColorBase");
      String colorResalta = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.styleTextColorResaltado");
      
      lblTitulo1.setFill(Color.web(colorBase));
      
      lblTitulo_NombreProducto.setFill(Color.web(colorBase));
      
      lblTitulo3.setFill(Color.web(colorBase));
      
      lblTitulo_PrecioDescuento.setFill(Color.web(colorResalta));
      
      lblTitulo5.setFill(Color.web(colorBase));
      
      lblTitulo_PrecioRegular.setFill(Color.web(colorBase));
    }
    catch (Exception e)
    {
      e = 
      
        e;e.printStackTrace();lblTitulo1.setText("Por su compra puede llevarse el");lblTitulo3.setText("por");lblTitulo5.setText("en lugar de ");hbTitulo.setStyle("-fx-background-color: green;");
    }
    finally {}
    VBox.setVgrow(hbTitulo, Priority.ALWAYS);
    HBox ultLinea = new HBox(10.0D);
    ultLinea.setAlignment(Pos.CENTER);
    
    hbTitulo.getChildren().addAll(new Node[] { lblTitulo1, lblTitulo_NombreProducto, ultLinea });
    
    ultLinea.getChildren().addAll(new Node[] { lblTitulo3, lblTitulo_PrecioDescuento, lblTitulo5, lblTitulo_PrecioRegular });
    
    vBoxPrincipal.getChildren().add(hbTitulo);
    
    ScrollPane scrollPane = new ScrollPane();
    scrollPane.setPrefHeight(350.0D);
    scrollPane.setPrefWidth(650.0D);
    scrollPane.setMinHeight(350.0D);
    scrollPane.setMaxHeight(350.0D);
    VBox.setMargin(scrollPane, new Insets(0.0D, 10.0D, 0.0D, 10.0D));
    
    anchorPane.setPrefHeight(330.0D);
    anchorPane.setPrefWidth(750.0D);
    
    hboxScroll.setPrefHeight(330.0D);
    hboxScroll.setPrefWidth(850.0D);
    hboxScroll.setAlignment(Pos.CENTER);
    
    vBoxPrincipal.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler()
    {
      public void handle(KeyEvent ke)
      {
        if (ke.getCode().equals(KeyCode.ALT)) {
          if (keyCodeF10.isVisible())
          {
            keyCodeF10.setText("");
            keyCodeF11.setText("");
            keyCodeF12.setText("");
            keyCodeF10.setVisible(false);
            keyCodeF11.setVisible(false);
            keyCodeF12.setVisible(false);
          }
          else
          {
            keyCodeF10.setText("F10 ");
            keyCodeF11.setText("F11 ");
            keyCodeF12.setText("F12 ");
            keyCodeF10.setVisible(true);
            keyCodeF11.setVisible(true);
            keyCodeF12.setVisible(true);
          }
        }
      }
    });
    anchorPane.getChildren().add(hboxScroll);
    
    ListChangeListener listener = new ListChangeListener()
    {
      public void onChanged(ListChangeListener.Change<? extends Node> nodo)
      {
        while (nodo.next()) {
          if (nodo.wasAdded())
          {
            UIArticuloPromocion nodoUIA = (UIArticuloPromocion)nodo.getAddedSubList().get(0);
            nodoUIA.asignarIndice(hboxScroll.getChildren().size());
            vBoxPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, nodoUIA.getFiltroEventosF());
            vBoxPrincipal.addEventFilter(KeyEvent.KEY_RELEASED, nodoUIA.getFiltroEventosAltRelease());
          }
        }
      }
    };
    hboxScroll.getChildren().addListener(listener);
    
    scrollPane.setContent(anchorPane);
    
    vBoxPrincipal.getChildren().add(scrollPane);
    
    HBox contImportes = new HBox(15.0D);
    contImportes.setAlignment(Pos.TOP_CENTER);
    contImportes.setPrefHeight(200.0D);
    contImportes.setMaxHeight(200.0D);
    VBox.setVgrow(contImportes, Priority.SOMETIMES);
    
    VBox titImportes = new VBox(10.0D);
    titImportes.setAlignment(Pos.TOP_RIGHT);
    titImportes.setPrefHeight(200.0D);
    titImportes.setPrefWidth(100.0D);
    
    HBox.setHgrow(titImportes, Priority.ALWAYS);
    
    HBox hbxTotal = new HBox();
    hbxTotal.setPrefHeight(200.0D);
    hbxTotal.setPrefWidth(100.0D);
    hbxTotal.setAlignment(Pos.TOP_RIGHT);
    
    Label lblTotal = new Label("Total:");
    lblTotal.setStyle("-fx-font: 30pt System ;");
    
    hbxTotal.getChildren().add(lblTotal);
    
    titImportes.getChildren().add(hbxTotal);
    
    HBox hbxRecibido = new HBox();
    hbxRecibido.setPrefHeight(200.0D);
    hbxRecibido.setPrefWidth(100.0D);
    hbxRecibido.setAlignment(Pos.TOP_RIGHT);
    
    Label lblRecibido = new Label("Importe recibido:");
    lblRecibido.setStyle("-fx-font: 25pt System ;");
    
    hbxRecibido.getChildren().add(lblRecibido);
    
    titImportes.getChildren().add(hbxRecibido);
    
    HBox hbxCambio = new HBox();
    hbxCambio.setPrefHeight(200.0D);
    hbxCambio.setPrefWidth(100.0D);
    hbxCambio.setAlignment(Pos.TOP_RIGHT);
    
    Label lblCambio = new Label("Cambio:");
    lblCambio.setStyle("-fx-font: 25pt System ;");
    
    hbxCambio.getChildren().add(lblCambio);
    
    titImportes.getChildren().add(hbxCambio);
    
    contImportes.getChildren().add(titImportes);
    
    VBox inputImportes = new VBox(10.0D);
    inputImportes.setAlignment(Pos.TOP_LEFT);
    inputImportes.setPrefHeight(200.0D);
    inputImportes.setPrefWidth(100.0D);
    
    HBox.setHgrow(inputImportes, Priority.ALWAYS);
    
    HBox hbxInputTotal = new HBox();
    hbxInputTotal.setPrefHeight(200.0D);
    hbxInputTotal.setPrefWidth(100.0D);
    HBox.setHgrow(hbxInputTotal, Priority.SOMETIMES);
    
    textInputTotal.setFocusTraversable(false);
    textInputTotal.setPromptText("0.00");
    textInputTotal.setEditable(false);
    textInputTotal.setPrefHeight(35.0D);
    textInputTotal.setPrefWidth(100.0D);
    textInputTotal.setStyle("-fx-background-color: #e5e5e5;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
    Label pesos = new Label(" $ ");
    pesos.setStyle("-fx-font: 25pt System;");
    
    HBox cntGrey1 = new HBox();
    cntGrey1.setStyle("-fx-background-color: #e5e5e5;-fx-background-radius: 5 5 5 5;");
    cntGrey1.getChildren().addAll(new Node[] { pesos, textInputTotal, kc11 });
    
    hbxInputTotal.getChildren().addAll(new Node[] { cntGrey1 });
    
    inputImportes.getChildren().add(hbxInputTotal);
    
    HBox hbxInputRecibido = new HBox();
    hbxInputRecibido.setPrefHeight(200.0D);
    hbxInputRecibido.setPrefWidth(100.0D);
    HBox.setHgrow(hbxInputRecibido, Priority.SOMETIMES);
    
    textInputRecibido.setPromptText("0.00");
    textInputRecibido.setEditable(true);
    textInputRecibido.setPrefHeight(35.0D);
    textInputRecibido.setPrefWidth(100.0D);
    textInputRecibido.setStyle("-fx-background-color: #e5e5e5;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
    
    textInputRecibido.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler()
    {
      public void handle(KeyEvent arg0)
      {
        if (arg0.getCharacter().equals("+"))
        {
          arg0.consume();
          textInputRecibido.textProperty().set(((String)textInputTotal.textProperty().get()).replace("$", "").trim());
        }
        else if (!arg0.getCharacter().matches("[0-9]|\\."))
        {
          arg0.consume();
        }
      }
    });
    textInputRecibido.textProperty().addListener(new ChangeListener()
    {
      public void changed(ObservableValue<? extends String> arg0, String viejoValor, String nuevoVal)
      {
        if (nuevoVal.matches("^[0]{2,}$")) {
          textInputRecibido.setText("0");
        } else if (nuevoVal.matches("^[0]{1,}[0-9]$")) {
          textInputRecibido.setText(nuevoVal.replace("0", ""));
        } else if (!nuevoVal.matches("\\d{0,5}([\\.]\\d{0,2})?")) {
          textInputRecibido.setText(viejoValor);
        }
        VistaPromociones.this.calcularCambio(nuevoVal);
      }
    });
    Label pesos1 = new Label(" $ ");
    pesos1.setStyle("-fx-font: 25pt System;");
    
    keyCodeF10.setStyle("-fx-font: 12pt System;");
    
    cntGrey.setStyle("-fx-background-color: #e5e5e5;-fx-background-radius: 5 5 5 5;");
    cntGrey.getChildren().addAll(new Node[] { pesos1, textInputRecibido, keyCodeF10 });
    
    hbxInputRecibido.getChildren().addAll(new Node[] { cntGrey });
    
    inputImportes.getChildren().add(hbxInputRecibido);
    
    HBox hbxInputCambio = new HBox();
    hbxInputCambio.setPrefHeight(200.0D);
    hbxInputCambio.setPrefWidth(100.0D);
    
    HBox.setHgrow(hbxInputCambio, Priority.SOMETIMES);
    
    textInputCambio.setFocusTraversable(false);
    textInputCambio.setPromptText("0.00");
    textInputCambio.setEditable(false);
    textInputCambio.setPrefHeight(35.0D);
    textInputCambio.setPrefWidth(100.0D);
    textInputCambio.setStyle("-fx-background-color: #e5e5e5;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
    Label pesos2 = new Label(" $ ");
    pesos2.setStyle("-fx-font: 25pt System;");
    
    HBox cntGrey2 = new HBox();
    cntGrey2.setStyle("-fx-background-color: #e5e5e5;-fx-background-radius: 5 5 5 5;");
    cntGrey2.getChildren().addAll(new Node[] { pesos2, textInputCambio, kc12 });
    
    hbxInputCambio.getChildren().addAll(new Node[] { cntGrey2 });
    
    inputImportes.getChildren().add(hbxInputCambio);
    
    contImportes.getChildren().add(inputImportes);
    
    vBoxPrincipal.getChildren().add(contImportes);
    
    HBox hbxPie = new HBox(30.0D);
    hbxPie.setAlignment(Pos.CENTER);
    hbxPie.setPrefHeight(100.0D);
    hbxPie.setMinHeight(100.0D);
    VBox.setVgrow(hbxPie, Priority.SOMETIMES);
    
    btnFinalizaVenta.setStyle("-fx-background-color: #0f3b84; -fx-text-fill: white; -fx-font: 25pt System; -fx-faint-focus-color: transparent; -fx-focus-color:#e08906;");
    
    btnFinalizaVenta.setOnAction(new EventHandler()
    {
      public void handle(ActionEvent arg0)
      {
        aceptarPromociones();
      }
    });
    btnCancelar.setStyle("-fx-background-color: #e08906; -fx-text-fill: white; -fx-font: 25pt System;");
    btnCancelar.setOnAction(new EventHandler()
    {
      public void handle(ActionEvent arg0)
      {
        rechazarPromociones();
      }
    });
    hbxPie.getChildren().add(btnFinalizaVenta);
    hbxPie.getChildren().add(btnCancelar);
    
    vBoxPrincipal.getChildren().add(hbxPie);
  }
  
  public void setPresentador(IPromocionesPresentador _presentador)
  {
    presentador = _presentador;
  }
  
  private void speachPrimerArticulo(final Set<ModeloArticulo> articulosRedondeo)
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if ((articulosRedondeo != null) && (articulosRedondeo.iterator().hasNext()))
          {
            ModeloArticulo art = (ModeloArticulo)articulosRedondeo.iterator().next();
            lblTitulo_NombreProducto.setText(art.getNombre());
            lblTitulo_PrecioDescuento.setText(String.format("$ %.2f", new Object[] { art.getImporte() }));
            lblTitulo_PrecioRegular.setText(String.format("$ %.2f", new Object[] { Double.valueOf(art.getImporte().doubleValue() + art.getDescuento().doubleValue()) }));
          }
        }
        catch (Exception e)
        {
          SION.logearExcepcion(Modulo.VENTA, e, new String[] { "speachPrimerArticulo :: " + Util.getStackTrace(e) });
        }
      }
    });
  }
  
  public void generarArticuloPromocion(final ModeloArticulo articulo)
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        try
        {
          final UIArticuloPromocion art = new UIArticuloPromocion(articulo);
          
          art.setEventoBoton(new EventHandler()
          {
            public void handle(ActionEvent arg0)
            {
              presentador.agregarPromocion(art.getModelo());
            }
          });
          hboxScroll.getChildren().add(art);
          
          VistaPromociones.this.recalcularTamanoScroll(Double.valueOf(art.getPrefWidth()));
        }
        catch (Exception e)
        {
          SION.logearExcepcion(Modulo.VENTA, e, new String[] { "generarArticuloPromocion :: " + Util.getStackTrace(e) });
        }
      }
    });
  }
  
  private void limpiarArticulosPromocion()
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        hboxScroll.getChildren().removeAll(hboxScroll.getChildren());
      }
    });
  }
  
  private void recalcularTamanoScroll(Double widthPromocion)
  {
    if (hboxScroll.getChildren().size() > 3)
    {
      final Double incremento = Double.valueOf((widthPromocion.doubleValue() + hboxScroll.getSpacing()) * (hboxScroll.getChildren().size() - 3));
      Platform.runLater(new Runnable()
      {
        public void run()
        {
          anchorPane.setPrefWidth(850 + incremento.intValue());
          hboxScroll.setPrefWidth(850 + incremento.intValue());
        }
      });
    }
  }
  
  public void cargarPromociones()
  {
    limpiarArticulosPromocion();
    
    Service servicioCargaArticulos = new Service()
    {
      protected Task createTask()
      {
        new Task()
        {
          protected Object call()
            throws Exception
          {
            mostrarLoading();
            Set<ModeloArticulo> articulosRedondeo = (Set)repositorio.getPromocionesDisponibles();
            
            VistaPromociones.this.speachPrimerArticulo(articulosRedondeo);
            for (ModeloArticulo art : articulosRedondeo) {
              generarArticuloPromocion(art);
            }
            return "OK";
          }
        };
      }
    };
    servicioCargaArticulos.stateProperty().addListener(new ChangeListener()
    {
      public void changed(ObservableValue arg0, Object arg1, Object arg2)
      {
        if ((arg2.equals(Worker.State.SUCCEEDED)) || (arg2.equals(Worker.State.CANCELLED)) || (arg2.equals(Worker.State.FAILED))) {
          ocultarLoading();
        }
        VistaPromociones.this.setFocusImporteRecibido();
      }
    });
    servicioCargaArticulos.exceptionProperty().addListener(new ChangeListener()
    {
      public void changed(ObservableValue<? extends Exception> arg0, Exception arg1, Exception nuevoVal)
      {
        SION.logearExcepcion(Modulo.VENTA, arg1, new String[] { "servicioCargaArticulos" });
        SION.logearExcepcion(Modulo.VENTA, nuevoVal, new String[] { "servicioCargaArticulos" });
      }
    });
    servicioCargaArticulos.start();
  }
  
  public void desactivarPromociones(Object promocionSeleccionada)
  {
    ModeloArticulo modeloSeleccionado = (ModeloArticulo)promocionSeleccionada;
    for (Node nodo : hboxScroll.getChildren())
    {
      UIArticuloPromocion nodoUIA = (UIArticuloPromocion)nodo;
      if (nodoUIA.getModelo() == modeloSeleccionado) {
        nodoUIA.cambiarEstatusSeleccionado();
      } else {
        nodoUIA.cambiarEstatusNoSeleccionado();
      }
    }
    Platform.runLater(new Runnable()
    {
      public void run() {}
    });
  }
  
  public void mostrarError(final String mensaje)
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        paneError.setVisible(true);
        txtMensajeError.textProperty().set(mensaje);
      }
    });
  }
  
  public void ocultarError()
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        paneError.setVisible(true);
        txtMensajeError.textProperty().set("");
      }
    });
  }
  
  public void mostrarLoading()
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        paneLoading.setVisible(true);
      }
    });
  }
  
  public void ocultarLoading()
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        paneLoading.setVisible(false);
      }
    });
  }
  
  public Object obtenerVista()
  {
    return vBoxPrincipal;
  }
  
  public void mostrarCostoPromocionesSeleccionadas(Object promocionesSeleccionadas)
  {
    Set<ModeloArticulo> promos = (Set)promocionesSeleccionadas;
    ModeloArticulo promo = null;
    if (promos.iterator().hasNext())
    {
      promo = (ModeloArticulo)promos.iterator().next();
      setImporteTotal(String.format("%,.2f", new Object[] { promo.getTotal() }));
      setFocusImporteRecibido();
    }
  }
  
  public void aceptarPromociones()
  {
    if (!btnFinalizaVenta.isDisabled()) {
      presentador.aceptarPromociones();
    }
  }
  
  public void rechazarPromociones()
  {
    presentador.rechazarPromociones();
  }
  
  public void cerrarPromociones()
  {
    presentador.cerrarPromociones();
  }
  
  private void calcularCambio(String _recibido)
  {
    try
    {
      Double total = Double.valueOf(Double.parseDouble(((String)textInputTotal.textProperty().get()).replace("$", "")));
      Double recibido = Double.valueOf(Double.parseDouble("0" + _recibido));
      
      Double cambio = Double.valueOf(recibido.doubleValue() - total.doubleValue());
      if (cambio.doubleValue() < 0.0D)
      {
        setImporteCambio("");
        deshabilitarFinalizarVenta();
      }
      else
      {
        setImporteCambio(String.format("%.2f", new Object[] { cambio }));
        habilitarFinalizarVenta();
      }
    }
    catch (Exception e)
    {
      System.out.println("Error al calcular el cambio  ");
    }
  }
  
  private void setImporteTotal(final String total)
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        textInputTotal.textProperty().set(total);
        VistaPromociones.this.calcularCambio((String)textInputRecibido.textProperty().get());
      }
    });
  }
  
  private void setImporteCambio(final String importe)
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        textInputCambio.textProperty().set(importe);
      }
    });
  }
  
  private void deshabilitarFinalizarVenta()
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        textInputRecibido.setStyle("-fx-background-color: #dd9d9d;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
        cntGrey.setStyle("-fx-background-color: #dd9d9d;-fx-background-radius: 5 5 5 5;");
      }
    });
  }
  
  private void habilitarFinalizarVenta()
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        textInputRecibido.setStyle("-fx-background-color: #e5e5e5;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
        cntGrey.setStyle("-fx-background-color: #e5e5e5;-fx-background-radius: 5 5 5 5;");
      }
    });
  }
  
  private void setFocusImporteRecibido()
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        textInputRecibido.selectAll();
        textInputRecibido.requestFocus();
      }
    });
  }
  
  public void setImporteRecibido(final Double importe)
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        textInputRecibido.textProperty().set(String.format("%.2f", new Object[] { importe }));
      }
    });
  }
  
  public void setImporteTotal(final Double importe)
  {
    Platform.runLater(new Runnable()
    {
      public void run()
      {
        textInputTotal.textProperty().set(String.format("%.2f", new Object[] { importe }));
      }
    });
  }
  
  public Double getImporteRecibido()
  {
    return Double.valueOf(Double.parseDouble("0" + (String)textInputRecibido.textProperty().get()));
  }
}
