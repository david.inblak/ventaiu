package neto.sion.tienda.venta.promociones.mvp;

public abstract interface IPromocionesRepositorio
{
  public abstract Object getPromocionesDisponibles();
  
  public abstract boolean esVentaCandidatoPromocion(Object paramObject);
  
  public abstract boolean esPromocionesActivas();
}
