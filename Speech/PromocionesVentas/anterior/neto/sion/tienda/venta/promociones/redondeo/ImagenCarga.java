package neto.sion.tienda.venta.promociones.redondeo;

import java.io.File;
import java.io.FileInputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;

class ImagenCarga
  implements Runnable
{
  public static int INSERTADO_CON_EXITO = 0;
  File fileImagen;
  String raiz;
  
  public ImagenCarga(String _raiz, File _fileImagen)
  {
    fileImagen = _fileImagen;
    raiz = _raiz;
  }
  
  public void run()
  {
    String fileName = fileImagen.getName();
    SION.log(Modulo.VENTA, "leerImagenesRedondeo :: fileName: " + fileName, Level.INFO);
    
    String artName = fileName.split("\\.")[0];
    Connection conexion = null;
    CallableStatement cs = null;
    try
    {
      String sql = SION.obtenerParametro(Modulo.VENTA, "VENTA.SPINSERTAIMAGENES.REDONDEO");
      
      conexion = Conexion.obtenerConexion();
      cs = conexion.prepareCall(sql);
      
      FileInputStream fin = new FileInputStream(raiz + fileName);
      
      cs.setLong(1, Long.parseLong(artName));
      cs.setBinaryStream(2, fin, fin.available());
      cs.registerOutParameter(3, 2);
      cs.registerOutParameter(4, 12);
      
      cs.execute();
      
      fin.close();
      SION.log(Modulo.VENTA, "Inserta imagen BD :: Respuesta : " + artName + " - " + cs.getInt(3), Level.INFO);
      if (cs.getInt(3) == INSERTADO_CON_EXITO) {
        if (fileImagen.delete()) {
          SION.log(Modulo.VENTA, "leerImagenesRedondeo :: Se eliminó el archivo insertado en base: " + artName, Level.INFO);
        } else {
          SION.log(Modulo.VENTA, "leerImagenesRedondeo :: Error al eliminar el archivo: " + artName, Level.INFO);
        }
      }
    }
    catch (Exception e)
    {
      SION.logearExcepcion(Modulo.VENTA, e, new String[] { "leerImagenesRedondeo :: Error durante la carga de archivos para articulos en el redondeo." });
    }
    finally
    {
      Conexion.cerrarRecursos(conexion, cs, null);
    }
  }
}
