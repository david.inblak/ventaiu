package neto.sion.tienda.venta.promociones.redondeo;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.promociones.mvp.IComando;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesPresentador;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesRepositorio;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesVista;
import neto.sion.tienda.venta.promociones.redondeo.modelo.ModeloArticulo;

public class PresentadorPromociones
  implements IPromocionesPresentador
{
  public IPromocionesVista vista;
  public IPromocionesRepositorio repositorio;
  IComando tareaRechazado;
  IComando tareaAceptado;
  IComando tareaCerrar;
  private Set<ModeloArticulo> promocionesSeleccionadas;
  private Boolean esVentaCandidata = null;
  
  public PresentadorPromociones(IPromocionesVista _vista, IPromocionesRepositorio _modelo)
  {
    vista = _vista;
    repositorio = _modelo;
    
    promocionesSeleccionadas = new HashSet();
  }
  
  public void iniciarVista()
  {
    vista.mostrarLoading();
    vista.cargarPromociones();
  }
  
  public boolean esVentaCandidata(Object venta)
  {
    SION.log(Modulo.VENTA, "esVentaCandidatoPromocion ", Level.INFO);
    return repositorio.esVentaCandidatoPromocion(venta);
  }
  
  public Object getVistaPromociones()
  {
    return vista.obtenerVista();
  }
  
  public void agregarPromocion(Object modelo)
  {
    ModeloArticulo modeloArt = (ModeloArticulo)modelo;
    promocionesSeleccionadas.removeAll(promocionesSeleccionadas);
    promocionesSeleccionadas.add(modeloArt);
    
    vista.desactivarPromociones(modeloArt);
    calculoCostoPromocionesSeleccionadas();
    SION.log(Modulo.VENTA, "Se selecciona artículo de la promocion :: " + modeloArt, Level.INFO);
  }
  
  public void calculoCostoPromocionesSeleccionadas()
  {
    vista.mostrarCostoPromocionesSeleccionadas(promocionesSeleccionadas);
  }
  
  public void aceptarPromociones()
  {
    SION.log(Modulo.VENTA, "Finalizar promociones (Aceptadas) " + promocionesSeleccionadas.size() + " Promociones seleccionadas.", Level.INFO);
    SION.log(Modulo.VENTA, "Importe recibido : " + vista.getImporteRecibido(), Level.INFO);
    if (tareaAceptado != null) {
      tareaAceptado.executar(new Object[] { promocionesSeleccionadas, vista.getImporteRecibido() });
    }
  }
  
  public void rechazarPromociones()
  {
    SION.log(Modulo.VENTA, "Finalizar promociones (Rechazadas) " + promocionesSeleccionadas.size() + " Promociones seleccionadas.", Level.INFO);
    if (tareaRechazado != null) {
      tareaRechazado.executar(new Object[] { null, null });
    }
  }
  
  public void cerrarPromociones()
  {
    SION.log(Modulo.VENTA, "Cierra promociones (Escape) ", Level.INFO);
    if (tareaCerrar != null) {
      tareaCerrar.executar(new Object[0]);
    }
  }
  
  public void setComandoRechazarPromocion(IComando _tareaRechazado)
  {
    tareaRechazado = _tareaRechazado;
  }
  
  public void setComandoAceptarPromocion(IComando _tareaAceptado)
  {
    tareaAceptado = _tareaAceptado;
  }
  
  public void setComandoCerrarPromocion(IComando _tareaAceptado)
  {
    tareaAceptado = _tareaAceptado;
  }
}
