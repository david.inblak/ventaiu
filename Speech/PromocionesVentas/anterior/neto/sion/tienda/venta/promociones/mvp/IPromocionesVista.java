package neto.sion.tienda.venta.promociones.mvp;

public abstract interface IPromocionesVista
{
  public abstract void setPresentador(IPromocionesPresentador paramIPromocionesPresentador);
  
  public abstract Object obtenerVista();
  
  public abstract void cargarPromociones();
  
  public abstract void desactivarPromociones(Object paramObject);
  
  public abstract void mostrarError(String paramString);
  
  public abstract void ocultarError();
  
  public abstract void mostrarLoading();
  
  public abstract void ocultarLoading();
  
  public abstract void mostrarCostoPromocionesSeleccionadas(Object paramObject);
  
  public abstract void aceptarPromociones();
  
  public abstract void rechazarPromociones();
  
  public abstract void cerrarPromociones();
  
  public abstract void setImporteRecibido(Double paramDouble);
  
  public abstract Double getImporteRecibido();
  
  public abstract void setImporteTotal(Double paramDouble);
}
