package com.javafx.main;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

public class Main
{
  private static boolean verbose = false;
  private static final String fxApplicationClassName = "javafx.application.Application";
  private static final String fxLaunchClassName = "com.sun.javafx.application.LauncherImpl";
  private static final String manifestAppClass = "JavaFX-Application-Class";
  private static final String manifestPreloaderClass = "JavaFX-Preloader-Class";
  private static final String manifestClassPath = "JavaFX-Class-Path";
  private static final String JAVAFX_FAMILY_VERSION = "2.";
  private static final String ZERO_VERSION = "0.0.0";
  
  private static URL fileToURL(File file)
    throws IOException
  {
    return file.getCanonicalFile().toURI().toURL();
  }
  
  private static Method findLaunchMethod(File jfxRtPath, String fxClassPath)
  {
    Class[] argTypes = { Class.class, Class.class, new String[0].getClass() };
    try
    {
      ArrayList urlList = new ArrayList();
      
      String cp = System.getProperty("java.class.path");
      if (cp != null) {
        while (cp.length() > 0)
        {
          int pathSepIdx = cp.indexOf(File.pathSeparatorChar);
          if (pathSepIdx < 0)
          {
            String pathElem = cp;
            urlList.add(fileToURL(new File(pathElem)));
            break;
          }
          if (pathSepIdx > 0)
          {
            String pathElem = cp.substring(0, pathSepIdx);
            urlList.add(fileToURL(new File(pathElem)));
          }
          cp = cp.substring(pathSepIdx + 1);
        }
      }
      cp = fxClassPath;
      if (cp != null) {
        while (cp.length() > 0)
        {
          int pathSepIdx = cp.indexOf(" ");
          if (pathSepIdx < 0)
          {
            String pathElem = cp;
            urlList.add(fileToURL(new File(pathElem)));
            break;
          }
          if (pathSepIdx > 0)
          {
            String pathElem = cp.substring(0, pathSepIdx);
            urlList.add(fileToURL(new File(pathElem)));
          }
          cp = cp.substring(pathSepIdx + 1);
        }
      }
      if (jfxRtPath != null)
      {
        File jfxRtLibPath = new File(jfxRtPath, "lib");
        urlList.add(fileToURL(new File(jfxRtLibPath, "jfxrt.jar")));
        urlList.add(fileToURL(new File(jfxRtLibPath, "deploy.jar")));
        urlList.add(fileToURL(new File(jfxRtLibPath, "plugin.jar")));
        urlList.add(fileToURL(new File(jfxRtLibPath, "javaws.jar")));
      }
      URL[] urls = (URL[])urlList.toArray(new URL[0]);
      if (verbose)
      {
        System.err.println("===== URL list");
        for (int i = 0; i < urls.length; i++) {
          System.err.println("" + urls[i]);
        }
        System.err.println("=====");
      }
      ClassLoader urlClassLoader = new URLClassLoader(urls, null);
      Class launchClass = Class.forName("com.sun.javafx.application.LauncherImpl", true, urlClassLoader);
      
      Method m = launchClass.getMethod("launchApplication", argTypes);
      if (m != null)
      {
        Thread.currentThread().setContextClassLoader(urlClassLoader);
        return m;
      }
    }
    catch (Exception ex)
    {
      if (jfxRtPath != null) {
        ex.printStackTrace();
      }
    }
    return null;
  }
  
  private static Method findLaunchMethodInClasspath(String fxClassPath)
  {
    return findLaunchMethod(null, fxClassPath);
  }
  
  private static Method findLaunchMethodInJar(String jfxRtPathName, String fxClassPath)
  {
    File jfxRtPath = new File(jfxRtPathName);
    
    File jfxRtLibPath = new File(jfxRtPath, "lib");
    File jfxRtJar = new File(jfxRtLibPath, "jfxrt.jar");
    if (!jfxRtJar.canRead())
    {
      System.err.println("Unable to read " + jfxRtJar.toString());
      return null;
    }
    return findLaunchMethod(jfxRtPath, fxClassPath);
  }
  
  private static int[] convertVersionStringtoArray(String version)
  {
    int[] v = new int[3];
    if (version == null) {
      return null;
    }
    String[] s = version.split("\\.");
    if (s.length == 3)
    {
      v[0] = Integer.parseInt(s[0]);
      v[1] = Integer.parseInt(s[1]);
      v[2] = Integer.parseInt(s[2]);
      return v;
    }
    return null;
  }
  
  private static int compareVersionArray(int[] a1, int[] a2)
  {
    boolean isValid1 = (a1 != null) && (a1.length == 3);
    boolean isValid2 = (a2 != null) && (a2.length == 3);
    if ((!isValid1) && (!isValid2)) {
      return 0;
    }
    if (!isValid2) {
      return -1;
    }
    if (!isValid1) {
      return 1;
    }
    for (int i = 0; i < a1.length; i++)
    {
      if (a2[i] > a1[i]) {
        return 1;
      }
      if (a2[i] < a1[i]) {
        return -1;
      }
    }
    return 0;
  }
  
  private static String lookupRegistry()
  {
    if (!System.getProperty("os.name").startsWith("Win")) {
      return null;
    }
    String javaHome = System.getProperty("java.home");
    if (verbose) {
      System.err.println("java.home = " + javaHome);
    }
    if ((javaHome == null) || (javaHome.equals(""))) {
      return null;
    }
    try
    {
      File jreLibPath = new File(javaHome, "lib");
      File deployJar = new File(jreLibPath, "deploy.jar");
      
      URL[] urls = { fileToURL(deployJar) };
      if (verbose) {
        System.err.println(">>>> URL to deploy.jar = " + urls[0]);
      }
      ClassLoader deployClassLoader = new URLClassLoader(urls, null);
      try
      {
        String configClassName = "com.sun.deploy.config.Config";
        Class configClass = Class.forName(configClassName, true, deployClassLoader);
        
        Method m = configClass.getMethod("getInstance", null);
        Object config = m.invoke(null, null);
        m = configClass.getMethod("loadDeployNativeLib", null);
        m.invoke(config, null);
      }
      catch (Exception ex) {}
      String winRegistryWrapperClassName = "com.sun.deploy.association.utility.WinRegistryWrapper";
      
      Class winRegistryWrapperClass = Class.forName(winRegistryWrapperClassName, true, deployClassLoader);
      
      Method mGetSubKeys = winRegistryWrapperClass.getMethod("WinRegGetSubKeys", new Class[] { Integer.TYPE, String.class, Integer.TYPE });
      
      Field HKEY_LOCAL_MACHINE_Field2 = winRegistryWrapperClass.getField("HKEY_LOCAL_MACHINE");
      
      int HKEY_LOCAL_MACHINE2 = HKEY_LOCAL_MACHINE_Field2.getInt(null);
      String registryKey = "Software\\Oracle\\JavaFX\\";
      
      String[] fxVersions = (String[])mGetSubKeys.invoke(null, new Object[] { new Integer(HKEY_LOCAL_MACHINE2), "Software\\Oracle\\JavaFX\\", new Integer(255) });
      if (fxVersions == null) {
        return null;
      }
      String version = "0.0.0";
      for (int i = 0; i < fxVersions.length; i++) {
        if (fxVersions[i].startsWith("2."))
        {
          int[] v1Array = convertVersionStringtoArray(version);
          int[] v2Array = convertVersionStringtoArray(fxVersions[i]);
          if (compareVersionArray(v1Array, v2Array) > 0) {
            version = fxVersions[i];
          }
        }
      }
      if (version.equals("0.0.0")) {
        return null;
      }
      String winRegistryClassName = "com.sun.deploy.util.WinRegistry";
      Class winRegistryClass = Class.forName(winRegistryClassName, true, deployClassLoader);
      
      Method mGet = winRegistryClass.getMethod("getString", new Class[] { Integer.TYPE, String.class, String.class });
      
      Field HKEY_LOCAL_MACHINE_Field = winRegistryClass.getField("HKEY_LOCAL_MACHINE");
      int HKEY_LOCAL_MACHINE = HKEY_LOCAL_MACHINE_Field.getInt(null);
      String path = (String)mGet.invoke(null, new Object[] { new Integer(HKEY_LOCAL_MACHINE), "Software\\Oracle\\JavaFX\\" + version, "Path" });
      if (verbose) {
        System.err.println("FOUND KEY: Software\\Oracle\\JavaFX\\" + version + " = " + path);
      }
      return path;
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    return null;
  }
  
  private static Attributes getJarAttributes()
    throws Exception
  {
    String theClassFile = "Main.class";
    Class theClass = Main.class;
    String classUrlString = theClass.getResource(theClassFile).toString();
    if ((!classUrlString.startsWith("jar:file:")) || (classUrlString.indexOf("!") == -1)) {
      return null;
    }
    String urlString = classUrlString.substring(4, classUrlString.lastIndexOf("!"));
    File jarFile = new File(new URI(urlString).getPath());
    String jarName = jarFile.getCanonicalPath();
    
    JarFile jf = null;
    try
    {
      jf = new JarFile(jarName);
      Manifest mf = jf.getManifest();
      return mf.getMainAttributes();
    }
    finally
    {
      if (jf != null) {
        try
        {
          jf.close();
        }
        catch (Exception ex) {}
      }
    }
  }
  
  private static String getAppName(Attributes attrs, boolean preloader)
  {
    String propName = preloader ? "javafx.preloader.class" : "javafx.application.class";
    
    String className = System.getProperty(propName);
    if ((className != null) && (className.length() != 0)) {
      return className;
    }
    if (preloader)
    {
      String appName = attrs.getValue("JavaFX-Preloader-Class");
      if ((appName == null) || (appName.length() == 0))
      {
        if (verbose) {
          System.err.println("Unable to find preloader class name");
        }
        return null;
      }
      return appName;
    }
    String appName = attrs.getValue("JavaFX-Application-Class");
    if ((appName == null) || (appName.length() == 0))
    {
      System.err.println("Unable to find application class name");
      return null;
    }
    return appName;
  }
  
  private static Class getAppClass(String appName)
  {
    try
    {
      if (verbose) {
        System.err.println("Try calling Class.forName(" + appName + ") using classLoader = " + Thread.currentThread().getContextClassLoader());
      }
      Class appClass = Class.forName(appName, false, Thread.currentThread().getContextClassLoader());
      if (verbose) {
        System.err.println("found class: " + appClass);
      }
      return appClass;
    }
    catch (NoClassDefFoundError ncdfe)
    {
      ncdfe.printStackTrace();
      errorExit("Unable to find class: " + appName);
    }
    catch (ClassNotFoundException cnfe)
    {
      cnfe.printStackTrace();
      errorExit("Unable to find class: " + appName);
    }
    return null;
  }
  
  private static void launchApp(Method launchMethod, String appName, String preloaderName, String[] args)
  {
    Class preloaderClass = null;
    if (preloaderName != null) {
      preloaderClass = getAppClass(preloaderName);
    }
    Class appClass = getAppClass(appName);
    Class fxApplicationClass = null;
    try
    {
      fxApplicationClass = Class.forName("javafx.application.Application", true, Thread.currentThread().getContextClassLoader());
    }
    catch (NoClassDefFoundError ex)
    {
      errorExit("Cannot find javafx.application.Application");
    }
    catch (ClassNotFoundException ex)
    {
      errorExit("Cannot find javafx.application.Application");
    }
    if (fxApplicationClass.isAssignableFrom(appClass)) {
      try
      {
        if (verbose) {
          System.err.println("launchApp: Try calling " + launchMethod.getDeclaringClass().getName() + "." + launchMethod.getName());
        }
        launchMethod.invoke(null, new Object[] { appClass, preloaderClass, args });
      }
      catch (InvocationTargetException ex)
      {
        ex.printStackTrace();
        errorExit("Exception while running Application");
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
        errorExit("Unable to invoke launch method");
      }
    } else {
      try
      {
        if (verbose) {
          System.err.println("Try calling " + appClass.getName() + ".main(String[])");
        }
        Method mainMethod = appClass.getMethod("main", new Class[] { new String[0].getClass() });
        
        mainMethod.invoke(null, new Object[] { args });
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
        errorExit("Unable to invoke main method");
      }
    }
  }
  
  private static void checkJre()
  {
    String javaVersion = System.getProperty("java.version");
    if (verbose)
    {
      System.err.println("java.version = " + javaVersion);
      System.err.println("java.runtime.version = " + System.getProperty("java.runtime.version"));
    }
    if ((!javaVersion.startsWith("1.6")) && (!javaVersion.startsWith("1.7")) && (!javaVersion.startsWith("1.8")) && (!javaVersion.startsWith("1.9"))) {
      errorExit("Unsupported Java Version: " + javaVersion);
    }
  }
  
  public static void main(String[] args)
  {
    verbose = Boolean.getBoolean("javafx.verbose");
    
    checkJre();
    
    Attributes attrs = null;
    try
    {
      attrs = getJarAttributes();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      errorExit("Unable to load jar manifest");
    }
    String appName = getAppName(attrs, false);
    if (verbose) {
      System.err.println("appName = " + appName);
    }
    if (appName == null) {
      errorExit("Unable to find application class name");
    }
    String preloaderName = getAppName(attrs, true);
    if (verbose) {
      System.err.println("preloaderName = " + preloaderName);
    }
    String fxClassPath = attrs.getValue("JavaFX-Class-Path");
    
    Method launchMethod = null;
    if (verbose) {
      System.err.println("1) Try existing classpath...");
    }
    launchMethod = findLaunchMethodInClasspath(fxClassPath);
    if (launchMethod != null)
    {
      launchApp(launchMethod, appName, preloaderName, args);
      return;
    }
    if (verbose) {
      System.err.println("2) Try javafx.runtime.path property...");
    }
    String javafxRuntimePath = System.getProperty("javafx.runtime.path");
    if (javafxRuntimePath != null)
    {
      if (verbose) {
        System.err.println("    javafx.runtime.path = " + javafxRuntimePath);
      }
      launchMethod = findLaunchMethodInJar(javafxRuntimePath, fxClassPath);
      if (launchMethod != null)
      {
        launchApp(launchMethod, appName, preloaderName, args);
        return;
      }
    }
    if (verbose) {
      System.err.println("3) Look in the OS platform registry...");
    }
    javafxRuntimePath = lookupRegistry();
    if (javafxRuntimePath != null)
    {
      if (verbose) {
        System.err.println("    Installed JavaFX runtime found in: " + javafxRuntimePath);
      }
      launchMethod = findLaunchMethodInJar(javafxRuntimePath, fxClassPath);
      if (launchMethod != null)
      {
        launchApp(launchMethod, appName, preloaderName, args);
        return;
      }
    }
    if (verbose) {
      System.err.println("4) Look in hardcoded paths");
    }
    String[] hardCodedPaths = { "../rt", "../../../../rt", "../../sdk/rt", "../../../artifacts/sdk/rt" };
    for (int i = 0; i < hardCodedPaths.length; i++)
    {
      javafxRuntimePath = hardCodedPaths[i];
      launchMethod = findLaunchMethodInJar(javafxRuntimePath, fxClassPath);
      if (launchMethod != null)
      {
        launchApp(launchMethod, appName, preloaderName, args);
        return;
      }
    }
    errorExit("Unable to load JavaFX runtime");
  }
  
  private static void errorExit(String string)
  {
    try
    {
      Runnable runnable = new Runnable()
      {
        private final String val$string;
        
        public void run()
        {
          try
          {
            Class componentClass = Class.forName("java.awt.Component");
            Class jOptionPaneClass = Class.forName("javax.swing.JOptionPane");
            Field ERROR_MESSAGE_Field = jOptionPaneClass.getField("ERROR_MESSAGE");
            int ERROR_MESSAGE = ERROR_MESSAGE_Field.getInt(null);
            Method showMessageDialogMethod = jOptionPaneClass.getMethod("showMessageDialog", new Class[] { componentClass, Object.class, String.class, Integer.TYPE });
            
            showMessageDialogMethod.invoke(null, new Object[] { null, val$string, "JavaFX Launcher Error", new Integer(ERROR_MESSAGE) });
          }
          catch (Exception ex)
          {
            ex.printStackTrace();
          }
        }
      };
      Class swingUtilsClass = Class.forName("javax.swing.SwingUtilities");
      Method invokeAndWaitMethod = swingUtilsClass.getMethod("invokeAndWait", new Class[] { Runnable.class });
      
      invokeAndWaitMethod.invoke(null, new Object[] { runnable });
      if (verbose) {
        System.err.println("Done with invoke and wait");
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    System.exit(1);
  }
}
