package com.javafx.main;

import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.net.URL;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JTextPane;

public class NoJavaFXFallback
  extends JApplet
  implements ActionListener
{
  boolean isInBrowser = false;
  boolean oldJRE = true;
  boolean isSupportedPlatform = false;
  
  private static float getJavaVersionAsFloat()
  {
    String versionString = System.getProperty("java.version", "1.5.0");
    
    StringBuffer sb = new StringBuffer();
    
    int firstDot = versionString.indexOf(".");
    sb.append(versionString.substring(0, firstDot));
    
    int secondDot = versionString.indexOf(".", firstDot + 1);
    sb.append(versionString.substring(firstDot + 1, secondDot));
    
    int underscore = versionString.indexOf("_", secondDot + 1);
    if (underscore >= 0)
    {
      int dash = versionString.indexOf("-", underscore + 1);
      if (dash < 0) {
        dash = versionString.length();
      }
      sb.append(versionString.substring(secondDot + 1, underscore)).append(".").append(versionString.substring(underscore + 1, dash));
    }
    else
    {
      int dash = versionString.indexOf("-", secondDot + 1);
      if (dash < 0) {
        dash = versionString.length();
      }
      sb.append(versionString.substring(secondDot + 1, dash));
    }
    float version = 150.0F;
    try
    {
      version = Float.parseFloat(sb.toString());
    }
    catch (NumberFormatException e) {}
    return version;
  }
  
  private void test()
  {
    oldJRE = (getJavaVersionAsFloat() < 160.18F);
    try
    {
      Class jclass = Class.forName("netscape.javascript.JSObject");
      Method m = jclass.getMethod("getWindow", new Class[] { Applet.class });
      isInBrowser = (m.invoke(null, new Object[] { this }) != null);
    }
    catch (Exception e) {}
    isSupportedPlatform = System.getProperty("os.name").startsWith("Win");
  }
  
  String getText()
  {
    String text = "";
    if ((isSupportedPlatform) && (oldJRE))
    {
      text = "This JavaFX application requires a recent Java runtime. Please download and install the latest JRE from java.com.";
      if (isInBrowser) {
        text = text + " Then restart the browser.";
      } else {
        text = text + " Then restart the application.";
      }
    }
    else
    {
      text = "JavaFX 2.0 is required to view this content but JavaFX. Get the JavaFX runtime from javafx.com and run the installer.";
      if (isInBrowser) {
        text = text + " Then restart the browser.";
      } else {
        text = text + " Then restart the application.";
      }
    }
    return text;
  }
  
  public void init()
  {
    test();
    
    Container pane = getContentPane();
    pane.setLayout(new BorderLayout());
    JTextPane l = new JTextPane();
    l.setText(getText());
    l.setEditable(false);
    
    pane.add(l, "Center");
    if (getJavaVersionAsFloat() > 150.0F)
    {
      JButton installButton = new JButton("Install Now");
      installButton.addActionListener(this);
      pane.add(installButton, "South");
    }
  }
  
  public void actionPerformed(ActionEvent ae)
  {
    try
    {
      URL u = new URL("http://javafx.com");
      if (isInBrowser)
      {
        getAppletContext().showDocument(u);
      }
      else
      {
        Class sm = Class.forName("javax.jnlp.ServiceManager");
        Class bs = Class.forName("javax.jnlp.BasicService");
        Method lookup = sm.getMethod("lookup", new Class[] { String.class });
        
        Method showDoc = bs.getMethod("showDocument", new Class[] { URL.class });
        
        Object s = lookup.invoke(null, new Object[] { "javax.jnlp.BasicService" });
        
        showDoc.invoke(s, new Object[] { u });
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}
