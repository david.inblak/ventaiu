package neto.sion.tienda.venta.promociones.redondeo;

import java.io.File;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesRepositorio;
import neto.sion.tienda.venta.promociones.redondeo.modelo.ModeloArticulo;

public class RepositorioRedondeo
  implements IPromocionesRepositorio
{
  Boolean redondeoActivo = null;
  final Object lock = new Object();
  Set<ModeloArticulo> articulosRedondeo = new HashSet();
  Boolean Existe_StoreProcedure = null;
  private int ORA_CODE_WRONG_NUM_PARAMS = 6550;
  public static final int CANT_HILOS_PROCESANDO_IMAGENES = 5;
  public static final int INSERTADO_CON_EXITO = 0;
  
  public Object getPromocionesDisponibles()
  {
    return articulosRedondeo;
  }
  
  public boolean esVentaCandidatoPromocion(Object venta)
  {
    SION.log(Modulo.VENTA, "esVentaCandidatoPromocion ", Level.INFO);
    boolean esCandidata = false;
    String sql = null;
    try
    {
      sql = SION.obtenerParametro(Modulo.VENTA, "REDONDEO.USRVELIT.SPCONSULTAARTICULOSREDONDEO");
      SION.log(Modulo.VENTA, ">> " + sql.replaceAll("[\\{\\}\\(\\)(call)\\?\\,\\s]", ""), Level.INFO);
    }
    catch (Exception e)
    {
      SION.log(Modulo.VENTA, "No se encontró la propiedad: REDONDEO.USRVELIT.SPCONSULTAARTICULOSREDONDEO", Level.INFO);
    }
    if (existeStoreProcedurePackage(sql))
    {
      SION.log(Modulo.VENTA, "Store existe: " + sql.replaceAll("[\\{\\}\\(\\)(call)\\?\\,\\s]", ""), Level.INFO);
      int CANTIDAD_DEFAULT_REDONDEO = 1;
      articulosRedondeo = new HashSet();
      
      CallableStatement cs = null;
      ResultSet rs = null;
      Connection conn = null;
      try
      {
        conn = Conexion.obtenerConexion();
        cs = conn.prepareCall(sql, 1003, 1007);
        cs.setDouble(1, ((Double)venta).doubleValue());
        cs.registerOutParameter(2, -10);
        cs.registerOutParameter(3, 2);
        cs.registerOutParameter(4, 12);
        
        cs.execute();
        rs = (ResultSet)cs.getObject(2);
        if (cs.getInt(3) == 0) {
          while (rs.next())
          {
            byte[] barr = null;
            
            ModeloArticulo art = new ModeloArticulo();
            art.setCodBarras(rs.getString(1));
            art.setId(Long.valueOf(rs.getLong(2)));
            
            art.setNombre(rs.getString(3));
            art.setUnidad(Integer.valueOf(rs.getInt(4)));
            art.setIdFamilia(Integer.valueOf(rs.getInt(5)));
            art.setPrecio(Double.valueOf(rs.getDouble(6)));
            art.setCosto(Double.valueOf(rs.getDouble(7)));
            art.setIva(Double.valueOf(rs.getDouble(8)));
            art.setDescuento(Double.valueOf(rs.getDouble(9)));
            art.setAgranel(Double.valueOf(rs.getDouble(10)));
            art.setNormaempaque(Integer.valueOf(rs.getInt(11)));
            art.setCodBarraspadre(rs.getString(12));
            art.setIdIEPS(Integer.valueOf(rs.getInt(13)));
            art.setCantidad(1);
            
            art.setImporte(Double.valueOf(art.getPrecio().doubleValue() - art.getDescuento().doubleValue()));
            art.setTotal(Double.valueOf(((Double)venta).doubleValue() + art.getImporte().doubleValue()));
            
            Blob b = rs.getBlob(14);
            try
            {
              barr = b.getBytes(1L, (int)b.length());
            }
            catch (Exception e) {}
            art.setImagen(barr);
            articulosRedondeo.add(art);
          }
        }
      }
      catch (SQLException e)
      {
        redondeoActivo = Boolean.valueOf(false);
        esCandidata = false;
        SION.logearExcepcion(Modulo.VENTA, e, new String[] { sql });
      }
      catch (Exception e)
      {
        SION.logearExcepcion(Modulo.VENTA, e, new String[] { sql });
      }
      finally
      {
        Conexion.cerrarResultSet(rs);
        Conexion.cerrarCallableStatement(cs);
        Conexion.cerrarConexion(conn);
      }
    }
    else
    {
      SION.log(Modulo.VENTA, "No existe el objeto de BD: " + sql.replaceAll("[\\{\\}\\(\\)(call)\\?\\,\\s]", ""), Level.INFO);
    }
    esCandidata = articulosRedondeo.size() > 0;
    SION.log(Modulo.VENTA, "Es venta candidata redondeo: " + esCandidata + ", Articulos para redondeo: " + articulosRedondeo.size(), Level.INFO);
    
    return esCandidata;
  }
  
  public boolean existeStoreProcedurePackage(String sqlValidar)
  {
    boolean respuesta = false;
    if (sqlValidar == null) {
      return false;
    }
    String[] partes = sqlValidar.replaceAll("[\\{\\}\\(\\)(call)\\?\\,\\s]", "").split("[\\.]");
    
    Statement stmt = null;
    ResultSet rs = null;
    Connection conexion = null;
    String sql = null;
    try
    {
      sql = "SELECT COUNT(1) FROM USER_SOURCE WHERE UPPER(NAME) LIKE '%" + partes[1] + "%' " + "AND UPPER(TEXT) LIKE '%PROCEDURE%' and UPPER(TEXT) LIKE '%" + partes[2] + "%' " + "AND UPPER(TYPE) IN ('PACKAGE','PACKAGE BODY') ";
      
      conexion = Conexion.obtenerConexion();
      stmt = conexion.createStatement(1003, 1007);
      rs = stmt.executeQuery(sql);
      while (rs.next()) {
        respuesta = rs.getInt(1) == 2;
      }
    }
    catch (Exception ex)
    {
      SION.logearExcepcion(Modulo.VENTA, ex, new String[] { sql });
    }
    finally
    {
      Conexion.cerrarConexion(conexion);
      Conexion.cerrarResultSet(rs);
    }
    if (!respuesta) {
      SION.log(Modulo.VENTA, "Objeto no definido en BD: " + sqlValidar.replaceAll("[\\{\\}\\(\\)(call)\\?\\,\\s]", ""), Level.INFO);
    }
    return respuesta;
  }
  
  public static void main(String[] args)
  {
    RepositorioRedondeo r = new RepositorioRedondeo();
    r.existeStoreProcedurePackage("{call USRVELIT.PAVENTAS.SPCONSULTAARTICULOSREDONDEOS(?,?,?,?,?,?,?,?)}");
  }
  
  public static void leerImagenesRedondeo()
  {
    String folder = SION.obtenerParametro(Modulo.VENTA, "REDONDEO.IMAGENES");
    
    SION.log(Modulo.VENTA, "leerImagenesRedondeo :: folder: " + folder, Level.INFO);
    
    new Thread(new Runnable()
    {
      public void run()
      {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        
        boolean resultado = false;
        if ((val$folder != null) && (!"".equals(val$folder)))
        {
          File folderFile = new File(val$folder);
          if ((folderFile != null) && ((resultado = folderFile.exists())))
          {
            File[] files = folderFile.listFiles();
            for (File file : files) {
              if (!file.isDirectory()) {
                executor.execute(new ImagenCarga(val$folder, file));
              }
            }
          }
        }
      }
    }).start();
  }
  
  public boolean esPromocionesActivas()
  {
    if (redondeoActivo == null)
    {
      String activo = "0";
      try
      {
        activo = SION.obtenerParametro(Modulo.VENTA, "REDONDEO.PROMOCIONES.ACTIVO");
        if (activo == null)
        {
          activo = "0";
          SION.log(Modulo.VENTA, "Inactivo redondeo por inexistencia de parametro.", Level.INFO);
        }
      }
      catch (Exception e)
      {
        SION.logearExcepcion(Modulo.VENTA, e, new String[] { "Inactivo redondeo por inexistencia de parametro." });
      }
      redondeoActivo = Boolean.valueOf("1".equals(activo));
    }
    return redondeoActivo.booleanValue();
  }
}
