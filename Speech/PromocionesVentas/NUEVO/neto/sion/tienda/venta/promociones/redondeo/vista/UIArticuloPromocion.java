package neto.sion.tienda.venta.promociones.redondeo.vista;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyCombination.Modifier;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Text;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.promociones.redondeo.modelo.ModeloArticulo;

public class UIArticuloPromocion
  extends Pane
{
  final KeyCombination f2 = new KeyCodeCombination(KeyCode.F2, new KeyCombination.Modifier[0]);
  final KeyCombination f3 = new KeyCodeCombination(KeyCode.F3, new KeyCombination.Modifier[0]);
  final KeyCombination f4 = new KeyCodeCombination(KeyCode.F4, new KeyCombination.Modifier[0]);
  final KeyCombination f5 = new KeyCodeCombination(KeyCode.F5, new KeyCombination.Modifier[0]);
  final KeyCombination f6 = new KeyCodeCombination(KeyCode.F6, new KeyCombination.Modifier[0]);
  final KeyCombination f7 = new KeyCodeCombination(KeyCode.F7, new KeyCombination.Modifier[0]);
  final KeyCombination f8 = new KeyCodeCombination(KeyCode.F8, new KeyCombination.Modifier[0]);
  final KeyCombination f9 = new KeyCodeCombination(KeyCode.F9, new KeyCombination.Modifier[0]);
  public static Image check = null;
  private ModeloArticulo articulo;
  private Label altShortCut = new Label("");
  private Button btnAgregarArt = new Button("Seleccionar", altShortCut);
  private Pane paneContenido = new Pane();
  private Pane paneIndice = new Pane();
  private boolean marcado = false;
  private int indiceElemento;
  
  public ModeloArticulo getModelo()
  {
    return articulo;
  }
  
  public UIArticuloPromocion(ModeloArticulo _articulo)
  {
    try
    {
      InputStream inputStream = getClass().getResourceAsStream("/neto/sion/tienda/venta/promociones/redondeo/vista/circle-with-check-mark.png");
      check = new Image(inputStream);
      inputStream.close();
    }
    catch (IOException ex)
    {
      SION.logearExcepcion(Modulo.VENTA, ex, new String[] { "InputStream circle-with-check-mark" });
    }
    articulo = _articulo;
    
    setPrefWidth(250.0D);
    setPrefHeight(300.0D);
    try
    {
      String txt = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.btnseleccionar");
      btnAgregarArt.setText((txt == null) || ("".contains(txt)) ? "Seleccionar" : txt);
    }
    catch (Exception ex)
    {
      btnAgregarArt.setText("Seleccionar");
    }
    try
    {
      btnAgregarArt.focusedProperty().addListener(new ChangeListener()
      {
        public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldVal, Boolean newval)
        {
          if (newval.booleanValue()) {
            cambiarEstatusConFoco();
          } else {
            cambiarEstatusSinFoco();
          }
        }
      });
    }
    catch (Exception e)
    {
      SION.logearExcepcion(Modulo.VENTA, e, new String[] { "UIArticuloPromocion" });
    }
    String verShortCut = "";
    try
    {
      verShortCut = SION.obtenerParametro(Modulo.VENTA, "VENTA.REDONDEO.VERFS");
    }
    catch (Exception ex) {}
    if ("0".equals(verShortCut))
    {
      altShortCut.setText("");
      altShortCut.setVisible(false);
    }
    generarUIArticuloElementos();
  }
  
  public void setEventoBoton(EventHandler<ActionEvent> evento)
  {
    btnAgregarArt.setOnAction(evento);
  }
  
  public void cambiarEstatusSeleccionado()
  {
    marcado = true;
    
    btnAgregarArt.setDisable(true);
    btnAgregarArt.setVisible(false);
    btnAgregarArt.setFocusTraversable(true);
    btnAgregarArt.setStyle("-fx-background-color: #109F04; -fx-text-fill: #ffffff; -fx-font: 20pt System;");
    paneContenido.setStyle("-fx-background-color: #ffffff; -fx-border-color: #12AD2A;");
    paneIndice.setVisible(true);
  }
  
  public void cambiarEstatusNoSeleccionado()
  {
    marcado = false;
    btnAgregarArt.setDisable(false);
    btnAgregarArt.setVisible(true);
    paneIndice.setVisible(false);
    cambiarEstatusSinFoco();
  }
  
  public void cambiarEstatusConFoco()
  {
    if (!marcado)
    {
      btnAgregarArt.setStyle("-fx-background-color: #5DADE2; -fx-text-fill: #ffffff; -fx-font: 20pt System;");
      paneContenido.setStyle("-fx-background-color: #ffffff; -fx-border-color: #5DADE2;");
    }
  }
  
  public void cambiarEstatusSinFoco()
  {
    if (!marcado)
    {
      btnAgregarArt.setStyle("-fx-background-color: #0f3b84; -fx-text-fill: #ffffff; -fx-font: 20pt System;");
      paneContenido.setStyle("-fx-background-color: #ffffff; -fx-border-color: #cccccc;");
    }
  }
  
  private void generarUIArticuloElementos()
  {
    try
    {
      paneContenido.setPrefWidth(250.0D);
      paneContenido.setPrefHeight(320.0D);
      paneContenido.setStyle("-fx-background-color: #ffffff;-fx-border-color: #cccccc;");
      paneContenido.setLayoutX(10.0D);
      paneContenido.setLayoutY(5.0D);
      
      VBox cont = new VBox(5.0D);
      cont.setLayoutY(1.0D);
      cont.setPrefWidth(250.0D);
      cont.setPrefHeight(320.0D);
      cont.setAlignment(Pos.CENTER);
      ImageView imgV = new ImageView();
      if (articulo.getImagen() != null)
      {
        imgV.setImage(new Image(new ByteArrayInputStream(articulo.getImagen())));
        imgV.setCache(true);
      }
      imgV.setPreserveRatio(true);
      imgV.setFitHeight(120.0D);
      
      cont.getChildren().add(imgV);
      
      Label lblArtNombre = new Label(articulo.getNombre());
      lblArtNombre.setAlignment(Pos.CENTER);
      lblArtNombre.setPrefHeight(80.0D);
      lblArtNombre.setMaxHeight(80.0D);
      lblArtNombre.setPrefWidth(195.0D);
      lblArtNombre.setWrapText(true);
      lblArtNombre.setTextOverrun(OverrunStyle.CENTER_ELLIPSIS);
      lblArtNombre.setStyle(" -fx-text-fill: #292929; -fx-font: 14pt System; ");
      VBox.setVgrow(lblArtNombre, Priority.SOMETIMES);
      
      cont.getChildren().add(lblArtNombre);
      
      HBox c1 = new HBox();
      c1.setAlignment(Pos.TOP_CENTER);
      c1.setPrefWidth(245.0D);
      
      VBox v1 = new VBox();
      
      HBox cnt = new HBox(5.0D);
      cnt.setAlignment(Pos.TOP_CENTER);
      cnt.setPrefWidth(245.0D);
      
      Text tx1 = new Text("Precio regular:");
      tx1.setStyle(" -fx-font: 18pt System; ");
      Text tx2 = new Text(String.format("$%,.2f", new Object[] { articulo.getPrecio() }));
      tx2.setStyle("-fx-strikethrough: true; -fx-font: 18pt System; ");
      tx2.setFill(Color.web("#808080"));
      tx1.setFill(Color.web("#808080"));
      
      Text tx11 = new Text(String.format("($%,.2f)", new Object[] { articulo.getImporte() }));
      tx11.setStyle("-fx-font: 18pt System; -fx-font-weight: bold;");
      tx11.setFill(Color.web("#990000"));
      
      cnt.getChildren().add(tx1);
      cnt.getChildren().add(tx2);
      cnt.getChildren().add(tx11);
      
      v1.getChildren().add(cnt);
      
      HBox cnt1 = new HBox(5.0D);
      cnt1.setAlignment(Pos.TOP_CENTER);
      cnt1.setPrefWidth(245.0D);
      
      Text tx112 = new Text("Ahorra:");
      tx112.setStyle("-fx-font: 25pt System; -fx-font-weight: bold;");
      tx112.setFill(Color.web("#12AD2A"));
      Text tx122 = new Text(String.format("$ %,.2f", new Object[] { Double.valueOf(articulo.getAhorro()) }));
      tx122.setStyle("-fx-font: 25pt System; -fx-font-weight: bold;");
      tx122.setFill(Color.web("#12AD2A"));
      
      cnt1.getChildren().add(tx112);
      cnt1.getChildren().add(tx122);
      
      v1.getChildren().add(cnt1);
      
      HBox cnt2 = new HBox(5.0D);
      cnt2.setAlignment(Pos.TOP_CENTER);
      cnt2.setPrefWidth(245.0D);
      
      Text tx21 = new Text("Total:");
      tx21.setFill(Color.web("#990000"));
      tx21.setStyle(" -fx-font: 16pt System;");
      cnt2.getChildren().add(tx21);
      
      Text tx22 = new Text(String.format("$%,.2f", new Object[] { articulo.getTotal() }));
      tx22.setFill(Color.web("#990000"));
      tx22.setStyle(" -fx-font: 16pt System;");
      
      cnt2.getChildren().add(tx22);
      
      v1.getChildren().add(cnt2);
      
      c1.getChildren().add(v1);
      
      cont.getChildren().add(c1);
      
      btnAgregarArt.setStyle("-fx-background-color: #0f3b84; -fx-text-fill: #ffffff; -fx-font: 20pt System;");
      
      cont.getChildren().add(btnAgregarArt);
      
      paneContenido.getChildren().add(cont);
      
      getChildren().add(paneContenido);
    }
    catch (Exception e)
    {
      SION.logearExcepcion(Modulo.VENTA, e, new String[] { "generarUIArticuloElementos" });
    }
  }
  
  public void asignarIndice(final int indiceActual)
  {
    paneIndice.setPrefWidth(30.0D);
    paneIndice.setPrefHeight(30.0D);
    paneIndice.setLayoutX(0.0D);
    paneIndice.setLayoutY(0.0D);
    
    Circle c = new Circle(15.0D);
    
    c.setLayoutX(15.0D);
    c.setLayoutY(15.0D);
    c.setFill(Color.web("#12AD2A"));
    
    c.setStroke(Color.TRANSPARENT);
    c.setStrokeType(StrokeType.INSIDE);
    
    paneIndice.getChildren().add(c);
    
    paneIndice.setVisible(false);
    paneIndice.getChildren().add(new ImageView(check));
    indiceElemento = indiceActual;
    
    l = new EventHandler()
    {
      public void handle(KeyEvent ke)
      {
        if ((f2.match(ke)) && (indiceActual == 1)) {
          btnAgregarArt.fire();
        } else if ((f3.match(ke)) && (indiceActual == 2)) {
          btnAgregarArt.fire();
        } else if ((f4.match(ke)) && (indiceActual == 3)) {
          btnAgregarArt.fire();
        } else if ((f5.match(ke)) && (indiceActual == 4)) {
          btnAgregarArt.fire();
        } else if ((f6.match(ke)) && (indiceActual == 5)) {
          btnAgregarArt.fire();
        } else if ((f7.match(ke)) && (indiceActual == 6)) {
          btnAgregarArt.fire();
        } else if ((f8.match(ke)) && (indiceActual == 7)) {
          btnAgregarArt.fire();
        } else if ((f9.match(ke)) && (indiceActual == 8)) {
          btnAgregarArt.fire();
        }
      }
    };
    getChildren().add(paneIndice);
    
    altShortCut.setStyle("-fx-font: 12pt System;");
    if (indiceActual < 10) {
      altShortCut.setText("F" + (indiceActual + 1));
    }
  }
  
  EventHandler<KeyEvent> l = null;
  
  public EventHandler getFiltroEventosF()
  {
    return l;
  }
  
  public EventHandler getFiltroEventosAltRelease()
  {
    new EventHandler()
    {
      public void handle(KeyEvent ke)
      {
        if (ke.getCode().equals(KeyCode.ALT)) {
          if (altShortCut.isVisible())
          {
            altShortCut.setVisible(false);
            altShortCut.setText("");
          }
          else
          {
            altShortCut.setVisible(true);
            if (indiceElemento < 10) {
              altShortCut.setText("F" + (indiceElemento + 1));
            }
          }
        }
      }
    };
  }
}
