package neto.sion.tienda.venta.promociones.mvp;

public abstract interface IComando
{
  public abstract void executar(Object... paramVarArgs);
}
