package neto.sion.tienda.venta.promociones.mvp;

public abstract interface IPromocionesPresentador
{
  public abstract boolean esVentaCandidata(Object paramObject);
  
  public abstract void iniciarVista();
  
  public abstract Object getVistaPromociones();
  
  public abstract void agregarPromocion(Object paramObject);
  
  public abstract void calculoCostoPromocionesSeleccionadas();
  
  public abstract void aceptarPromociones();
  
  public abstract void rechazarPromociones();
  
  public abstract void cerrarPromociones();
  
  public abstract void setComandoRechazarPromocion(IComando paramIComando);
  
  public abstract void setComandoAceptarPromocion(IComando paramIComando);
  
  public abstract void setComandoCerrarPromocion(IComando paramIComando);
}
