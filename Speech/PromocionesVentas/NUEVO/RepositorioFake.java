import java.util.HashSet;
import java.util.Set;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesRepositorio;
import neto.sion.tienda.venta.promociones.redondeo.modelo.ModeloArticulo;

public class RepositorioFake
  implements IPromocionesRepositorio
{
  Boolean redondeoActivo = null;
  final Object lock = new Object();
  Set<ModeloArticulo> articulosRedondeo = new HashSet();
  Boolean Existe_StoreProcedure = null;
  
  public RepositorioFake()
  {
    ModeloArticulo articuloFake = new ModeloArticulo();
    articuloFake.setAgranel(Double.valueOf(0.0D));
    articuloFake.setCantidad(1);
    articuloFake.setCodBarras("00000");
    articuloFake.setCodBarraspadre("0000");
    articuloFake.setCosto(Double.valueOf(10.0D));
    articuloFake.setDescuento(Double.valueOf(2.0D));
    articuloFake.setId(Long.valueOf(100001L));
    articuloFake.setIdFamilia(Integer.valueOf(5));
    articuloFake.setIdIEPS(Integer.valueOf(0));
    articuloFake.setImagen(null);
    articuloFake.setImporte(Double.valueOf(8.0D));
    articuloFake.setIva(Double.valueOf(0.0D));
    articuloFake.setNombre("Chocolate Carlos V Suizo 1 pza");
    articuloFake.setNormaempaque(Integer.valueOf(1));
    articuloFake.setPrecio(Double.valueOf(10.0D));
    articuloFake.setTotal(Double.valueOf(8.0D));
    articuloFake.setUnidad(Integer.valueOf(1));
    
    articulosRedondeo.add(articuloFake);
  }
  
  public Object getPromocionesDisponibles()
  {
    return articulosRedondeo;
  }
  
  public boolean esVentaCandidatoPromocion(Object venta)
  {
    return true;
  }
  
  public boolean esPromocionesActivas()
  {
    return true;
  }
}
