/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import neto.sion.tienda.venta.promociones.mvp.IComando;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesPresentador;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesRepositorio;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesVista;
import neto.sion.tienda.venta.promociones.redondeo.PresentadorPromociones;
import neto.sion.tienda.venta.promociones.redondeo.RepositorioRedondeo;
import neto.sion.tienda.venta.promociones.redondeo.VistaPromociones;

/**
 *
 * @author dramirezr
 */
public class NewFXMain extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(final Stage primaryStage) {
        primaryStage.setTitle("Hello World!");
        
        
        IPromocionesRepositorio repositorio = new RepositorioFake();
        IPromocionesVista vista = new VistaPromociones(repositorio);
        
        IPromocionesPresentador presentador = new PresentadorPromociones(vista, repositorio);
        vista.setPresentador(presentador);
                        
        StackPane root = new StackPane();
        
        if( presentador.esVentaCandidata(34) ){           
            root.getChildren().add((Node)presentador.getVistaPromociones());
            presentador.iniciarVista();
            
             vista.setImporteTotal( 100.00 );
             vista.setImporteRecibido( 100.0 );
        }
        
        presentador.setComandoCerrarPromocion(new IComando() {
            @Override
            public void executar(Object... parametro) {
                primaryStage.close();
            }
        });
        
       // primaryStage.getScene().getWindow().hide();
        
        primaryStage.setScene(new Scene(root, 900, 750));
        primaryStage.show();
    }
}
