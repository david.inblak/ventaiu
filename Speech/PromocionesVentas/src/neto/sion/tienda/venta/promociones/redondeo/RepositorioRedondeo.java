/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.redondeo;

import java.io.File;
import java.io.FileInputStream;
import java.sql.*;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesRepositorio;
import neto.sion.tienda.venta.promociones.redondeo.modelo.ModeloArticulo;
import oracle.jdbc.driver.OracleSQLException;
import oracle.jdbc.internal.OracleTypes;



/**
 *
 * @author dramirezr
 */
public class RepositorioRedondeo implements IPromocionesRepositorio{
    Boolean redondeoActivo = null;
    final Object lock = new Object();
    Set<ModeloArticulo> articulosRedondeo = new HashSet<ModeloArticulo>();
    Boolean Existe_StoreProcedure = null;
    private int ORA_CODE_WRONG_NUM_PARAMS = 6550;
   
    @Override
    public Object getPromocionesDisponibles() {        
        //synchronized( lock )
        {
            return articulosRedondeo;
        }        
    }

    @Override
    public boolean esVentaCandidatoPromocion(Object venta) {
        //synchronized( lock )
        //{
        
        SION.log(Modulo.VENTA, "esVentaCandidatoPromocion " , Level.INFO);
            boolean esCandidata=false;
            String sql = null;
            try{
                sql = SION.obtenerParametro(Modulo.VENTA, "REDONDEO.USRVELIT.SPCONSULTAARTICULOSREDONDEO");
                SION.log(Modulo.VENTA, ">> " + sql.replaceAll("[\\{\\}\\(\\)(call)\\?\\,\\s]","") , Level.INFO);
            }catch(Exception e){ 
                SION.log(Modulo.VENTA, "No se encontró la propiedad: REDONDEO.USRVELIT.SPCONSULTAARTICULOSREDONDEO" , Level.INFO);
            }
            
            if(  existeStoreProcedurePackage(sql) )
            {
                SION.log(Modulo.VENTA, "Store existe: " + sql.replaceAll("[\\{\\}\\(\\)(call)\\?\\,\\s]",""), Level.INFO);
                final int CANTIDAD_DEFAULT_REDONDEO = 1;
                articulosRedondeo = new HashSet<ModeloArticulo>(); 

                CallableStatement cs=null;
                ResultSet rs=null;
                Connection conn = null;
                try {
                    conn = Conexion.obtenerConexion();//PoolConexiones.getInstanciaPool().getPool().getConnection();
                    cs = conn.prepareCall(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY );
                    cs.setDouble(1, (Double)venta);
                    cs.registerOutParameter(2, OracleTypes.CURSOR);
                    cs.registerOutParameter(3, OracleTypes.NUMBER);
                    cs.registerOutParameter(4, OracleTypes.VARCHAR);

                    cs.execute();
                    rs = (ResultSet) cs.getObject(2);

                    if (cs.getInt(3)==0){
                        while (rs.next()) {
                            byte[] barr=null;

                            ModeloArticulo art = new ModeloArticulo();
                            art.setCodBarras(rs.getString(1));
                            art.setId(rs.getLong(2));

                            art.setNombre(rs.getString(3));
                            art.setUnidad(rs.getInt(4));
                            art.setIdFamilia(rs.getInt(5));
                            art.setPrecio( rs.getDouble(6) );
                            art.setCosto(rs.getDouble(7));
                            art.setIva(rs.getDouble(8));
                            art.setDescuento(rs.getDouble(9));
                            art.setAgranel(rs.getDouble(10));
                            art.setNormaempaque(rs.getInt(11));
                            art.setCodBarraspadre(rs.getString(12));
                            art.setIdIEPS(rs.getInt(13));
                            art.setCantidad(CANTIDAD_DEFAULT_REDONDEO);

                            art.setImporte( art.getPrecio() - art.getDescuento());
                            art.setTotal( ((Double)venta) + art.getImporte() );

                            Blob b=rs.getBlob(14);
                            try{
                                barr=b.getBytes(1,(int)b.length());
                            }catch(Exception e){
                            }

                            art.setImagen(barr);
                            articulosRedondeo.add(art);                   

                                              
                        }

                    }


                } catch (SQLException e) {
                    redondeoActivo = false;
                    esCandidata = false;
                    SION.logearExcepcion(Modulo.VENTA, e, sql);
                } catch (Exception e) {
                    SION.logearExcepcion(Modulo.VENTA, e, sql);
                } finally {
                    Conexion.cerrarResultSet(rs);
                    Conexion.cerrarCallableStatement(cs);
                    Conexion.cerrarConexion(conn);
                }

            }else{
                SION.log(Modulo.VENTA, "No existe el objeto de BD: " + sql.replaceAll("[\\{\\}\\(\\)(call)\\?\\,\\s]","") , Level.INFO);
            }
            
            
            esCandidata = (articulosRedondeo.size() > 0);
            SION.log(Modulo.VENTA, "Es venta candidata redondeo: "+esCandidata+", Articulos para redondeo: "+articulosRedondeo.size(), Level.INFO);

            return esCandidata;
        //}
    }
    
    /*
     public boolean existeStoreProcedurePackage(String sqlValidar) {        
        if( Existe_StoreProcedure != null ){
            return Existe_StoreProcedure;
        }else{
            if( sqlValidar == null ) Existe_StoreProcedure = false;

            String sql = sqlValidar.replaceAll("[\\?\\,]","");
            Connection conexion = null;
            CallableStatement cs = null;

            try {
                conexion = Conexion.obtenerConexion();//PoolConexiones.getInstanciaPool().getPool().getConnection();
                cs = conexion.prepareCall(sql);
                cs.execute();
            } catch (SQLException ex) {
                if( ex.getErrorCode() == ORA_CODE_WRONG_NUM_PARAMS ){
                    Existe_StoreProcedure = true;
                    SION.log(Modulo.VENTA, "Objeto existe en BD: "+sqlValidar.replaceAll("[\\{\\}\\(\\)(call)\\?\\,\\s]",""), Level.INFO); 
                }else{
                    Existe_StoreProcedure = false;
                    SION.log(Modulo.VENTA, "Objeto no definido en BD: "+sqlValidar.replaceAll("[\\{\\}\\(\\)(call)\\?\\,\\s]",""), Level.INFO);  
                }
                SION.logearExcepcion(Modulo.VENTA, ex, sql);
            } finally {
                Conexion.cerrarRecursos(conexion, cs, null);
            }

            return Existe_StoreProcedure;
        }
    }*/
    
    public boolean existeStoreProcedurePackage(String sqlValidar) {
        boolean respuesta = false;
        if( sqlValidar == null ) return false;
         
        String []partes = sqlValidar.replaceAll("[\\{\\}\\(\\)(call)\\?\\,\\s]","").split("[\\.]");
       
        Statement stmt = null;
        ResultSet rs = null;
        Connection conexion = null;
        String sql = null;
        
        try {
           sql = (   "SELECT COUNT(1) FROM USER_SOURCE "+
                     "WHERE UPPER(NAME) LIKE '%"+partes[1]+"%' "+
                     "AND UPPER(TEXT) LIKE '%PROCEDURE%' and UPPER(TEXT) LIKE '%"+partes[2]+"%' "+
                     "AND UPPER(TYPE) IN ('PACKAGE','PACKAGE BODY') ");
             
            conexion = Conexion.obtenerConexion();
            stmt = conexion.createStatement( ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY );            
            rs = stmt.executeQuery(sql);
            
            while(rs.next()){
               respuesta = ( rs.getInt(1) == 2 );
            }
           
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, sql);           
        } finally {
            Conexion.cerrarConexion(conexion);
            Conexion.cerrarResultSet(rs);
        }
        
        if( !respuesta )
             SION.log(Modulo.VENTA, "Objeto no definido en BD: "+sqlValidar.replaceAll("[\\{\\}\\(\\)(call)\\?\\,\\s]",""), Level.INFO);
        
        return respuesta;
    }
     
    public static void main(String[] args) {
        RepositorioRedondeo r = new RepositorioRedondeo();
        r.existeStoreProcedurePackage("{call USRVELIT.PAVENTAS.SPCONSULTAARTICULOSREDONDEOS(?,?,?,?,?,?,?,?)}");
    }
     
    public static final int CANT_HILOS_PROCESANDO_IMAGENES = 5;
    public static final int INSERTADO_CON_EXITO = 0;
    
    public static void leerImagenesRedondeo(){
        final String folder = SION.obtenerParametro(Modulo.VENTA, "REDONDEO.IMAGENES");
       
        SION.log(Modulo.VENTA, "leerImagenesRedondeo :: folder: " +folder, Level.INFO);
        
        new Thread(new Runnable() {

            @Override
            public void run() {
                
                ExecutorService executor = Executors.newFixedThreadPool(CANT_HILOS_PROCESANDO_IMAGENES);

                boolean resultado = false;
                if( folder != null && !"".equals(folder)){

                    File folderFile = new File(folder);
                    if (folderFile != null && (resultado = folderFile.exists())) {
                        File[] files = folderFile.listFiles();
                       
                        for (File file : files) {
                            if( !file.isDirectory() ){
                                executor.execute(new ImagenCarga(folder, file));
                            }
                        }
                    }

                }
            }
        }).start();
        
        
    }
    

    @Override
    public boolean esPromocionesActivas() {
        if( redondeoActivo == null){
            String activo = "0";
            try{
                activo = SION.obtenerParametro(Modulo.VENTA, "REDONDEO.PROMOCIONES.ACTIVO");
                if( activo == null ){
                    activo = "0";
                    SION.log(Modulo.VENTA, "Inactivo redondeo por inexistencia de parametro.", Level.INFO);
                }
            }catch(Exception e){
                SION.logearExcepcion(Modulo.VENTA, e, "Inactivo redondeo por inexistencia de parametro.");
            }
            redondeoActivo = "1".equals(activo);
        }
        
        return redondeoActivo;
    }   
    
}
class ImagenCarga implements Runnable{
    public static int INSERTADO_CON_EXITO = 0;
    File fileImagen;
    //CallableStatement cs;
    String raiz;
    
    public ImagenCarga(String _raiz, File _fileImagen){
        fileImagen = _fileImagen;
        raiz = _raiz;
    }
    
    @Override
    public void run() {
        final String fileName = fileImagen.getName();
        SION.log(Modulo.VENTA, "leerImagenesRedondeo :: fileName: " +fileName, Level.INFO);

        String artName = fileName.split("\\.")[0];
        Connection conexion = null;
        CallableStatement cs = null;
       // ResultSet rs=null;
        try{ 
            String sql = SION.obtenerParametro(Modulo.VENTA, "VENTA.SPINSERTAIMAGENES.REDONDEO");
                       // PoolConexiones.getInstanciaPool().getPool().getConnection();//
            conexion = Conexion.obtenerConexion();
            cs = conexion.prepareCall(sql);
            
            FileInputStream fin = new FileInputStream(raiz + fileName);  

            cs.setLong             (1, Long.parseLong(artName) );
            cs.setBinaryStream     (2,fin,fin.available());
            cs.registerOutParameter(3, OracleTypes.NUMBER);
            cs.registerOutParameter(4, OracleTypes.VARCHAR);

            cs.execute();

            fin.close();
            SION.log(Modulo.VENTA, "Inserta imagen BD :: Respuesta : " + artName+" - "+cs.getInt(3), Level.INFO);

            /// Eliminacion de archivos cargados
            if(  cs.getInt(3) == INSERTADO_CON_EXITO ){
                if( fileImagen.delete() ){
                    SION.log(Modulo.VENTA, "leerImagenesRedondeo :: Se eliminó el archivo insertado en base: " + artName, Level.INFO);
                }else{
                    SION.log(Modulo.VENTA, "leerImagenesRedondeo :: Error al eliminar el archivo: " + artName, Level.INFO);
                }
            }
        }catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, "leerImagenesRedondeo :: Error durante la carga de archivos para articulos en el redondeo.");
        } finally {
            Conexion.cerrarRecursos(conexion,cs, null);
        }
    }
    
}