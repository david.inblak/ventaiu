/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.mvp;

/**
 *
 * @author dramirezr
 */
public interface IPromocionesVista {
    void setPresentador(IPromocionesPresentador _presentador);
            
    Object obtenerVista();
    void cargarPromociones();
    void desactivarPromociones(Object promocionSeleccionada);
     
    void mostrarError(String mensaje);
    void ocultarError();

    void mostrarLoading();
    void ocultarLoading();
    
    void mostrarCostoPromocionesSeleccionadas(Object promocionesSeleccionadas );
    void aceptarPromociones();
    void rechazarPromociones();
    void cerrarPromociones();
    
    void setImporteRecibido(Double importe);
    Double getImporteRecibido();
    void setImporteTotal(Double importe);
}
