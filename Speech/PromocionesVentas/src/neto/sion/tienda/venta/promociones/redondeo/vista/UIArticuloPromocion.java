/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.redondeo.vista;

import java.awt.event.KeyEvent;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Text;
import javax.xml.stream.EventFilter;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;


import neto.sion.tienda.venta.promociones.redondeo.modelo.ModeloArticulo;

/**
 *
 * @author dramirezr
 */
public class UIArticuloPromocion extends Pane {
    
    //final KeyCombination alt = new KeyCodeCombination(KeyCode.ALT);
    final KeyCombination f2 = new KeyCodeCombination(KeyCode.F2);
    final KeyCombination f3 = new KeyCodeCombination(KeyCode.F3);
    final KeyCombination f4 = new KeyCodeCombination(KeyCode.F4);
    final KeyCombination f5 = new KeyCodeCombination(KeyCode.F5);
    final KeyCombination f6 = new KeyCodeCombination(KeyCode.F6);    
    final KeyCombination f7 = new KeyCodeCombination(KeyCode.F7);
    final KeyCombination f8 = new KeyCodeCombination(KeyCode.F8);    
    final KeyCombination f9 = new KeyCodeCombination(KeyCode.F9);
    
    public static Image check=null;
    private ModeloArticulo articulo;
    private Label altShortCut = new Label("");
    private Button btnAgregarArt = new Button("Seleccionar", altShortCut);
    private Pane paneContenido = new Pane();
    private Pane paneIndice = new Pane();
    private boolean marcado = false;
    private int indiceElemento;
    
            
    public ModeloArticulo getModelo(){
        return articulo;
    }
    
    public UIArticuloPromocion(ModeloArticulo _articulo){
        try { 
            InputStream inputStream = this.getClass().getResourceAsStream(("/neto/sion/tienda/venta/promociones/redondeo/vista/circle-with-check-mark.png"));
            check = new Image(inputStream);
            inputStream.close();  
        } catch (IOException ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "InputStream circle-with-check-mark");
        }/**/ 
        //SION.log(Modulo.VENTA, "Iniciando constructor...", Level.INFO);
        this.articulo = _articulo;
        //SION.log(Modulo.VENTA, "Modelo asignado al UIArticulo.", Level.INFO);
        this.setPrefWidth(250);
        this.setPrefHeight(300);
        
        //SION.log(Modulo.VENTA, "Asignando texto a boton agregar.", Level.INFO);
        try {
            String txt = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.btnseleccionar");
            btnAgregarArt.setText((txt==null||"".contains(txt))? "Seleccionar":txt); 
        } catch (Exception ex) {
            btnAgregarArt.setText("Seleccionar"); 
        }
        //SION.log(Modulo.VENTA, "Asignando evento a botón agregar.", Level.INFO);
         try{
            btnAgregarArt.focusedProperty().addListener(new ChangeListener<Boolean>(){
                @Override
                public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldVal, Boolean newval) {
                    if( newval ){
                        cambiarEstatusConFoco();
                    }else{
                        cambiarEstatusSinFoco();
                    }
                }        
            });            
        }catch(Exception e){
            SION.logearExcepcion(Modulo.VENTA, e, "UIArticuloPromocion");
        }
        // SION.log(Modulo.VENTA, "Determinando vista de F's del artículo.", Level.INFO);
       String verShortCut = ""; 
       try {  verShortCut = SION.obtenerParametro(Modulo.VENTA, "VENTA.REDONDEO.VERFS"); } catch (Exception ex) {}
            
        if( "0".equals(verShortCut) ){
            altShortCut.setText("");
            altShortCut.setVisible(false);
        }
        
        //SION.log(Modulo.VENTA, "Llamando generarUIArticuloElementos...", Level.INFO);
        this.generarUIArticuloElementos();
    }
    
    public void setEventoBoton(EventHandler<ActionEvent> evento){
        this.btnAgregarArt.setOnAction(evento);
    }
    
    public void cambiarEstatusSeleccionado(){
        marcado = true;
        
        this.btnAgregarArt.setDisable(true);
        this.btnAgregarArt.setVisible(false);
        this.btnAgregarArt.setFocusTraversable(true);
        this.btnAgregarArt.setStyle("-fx-background-color: #109F04; -fx-text-fill: #ffffff; -fx-font: 20pt System;");
        this.paneContenido.setStyle("-fx-background-color: #ffffff; -fx-border-color: #12AD2A;");
        this.paneIndice.setVisible(true);
    }
    public void cambiarEstatusNoSeleccionado(){
        marcado = false;
        this.btnAgregarArt.setDisable(false);
        this.btnAgregarArt.setVisible(true);
        this.paneIndice.setVisible(false);
        cambiarEstatusSinFoco();
    }
    public void cambiarEstatusConFoco(){
        if( !marcado ){
            this.btnAgregarArt.setStyle("-fx-background-color: #5DADE2; -fx-text-fill: #ffffff; -fx-font: 20pt System;");
            this.paneContenido.setStyle("-fx-background-color: #ffffff; -fx-border-color: #5DADE2;");
        }
    }
    public void cambiarEstatusSinFoco(){
        if( !marcado ){
            this.btnAgregarArt.setStyle("-fx-background-color: #0f3b84; -fx-text-fill: #ffffff; -fx-font: 20pt System;");
            this.paneContenido.setStyle("-fx-background-color: #ffffff; -fx-border-color: #cccccc;");
        }
    }
    
    
    // public static int CANT_HILOS_PROCESANDO_IMAGENES = 3;
    //final ExecutorService executor = Executors.newFixedThreadPool(CANT_HILOS_PROCESANDO_IMAGENES);
    private void generarUIArticuloElementos(){
        try{
            //SION.log(Modulo.VENTA, "Inicializacion de elementos de vista de articulo.", Level.INFO);
            //----- Contenido del UIArticuloPromocion

            paneContenido.setPrefWidth(250);
            paneContenido.setPrefHeight(320);
            paneContenido.setStyle("-fx-background-color: #ffffff;-fx-border-color: #cccccc;");
            paneContenido.setLayoutX(10);
            paneContenido.setLayoutY(5);

            VBox cont = new VBox(5);
            cont.setLayoutY(1);
            cont.setPrefWidth(250);
            cont.setPrefHeight(320);
            cont.setAlignment(Pos.CENTER);
            ImageView imgV = new ImageView();

            if( articulo.getImagen() != null ){
                /*Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {*/
                                imgV.setImage(new Image( new ByteArrayInputStream(articulo.getImagen()) )); 
                                imgV.setCache(true);                               
                            /*}
                        });
                    }
                });*/
            }

            imgV.setPreserveRatio(true);
            imgV.setFitHeight(120);

            cont.getChildren().add(imgV);


            //-- Nombre del articulo
            Label lblArtNombre = new Label(articulo.getNombre());
            lblArtNombre.setAlignment(Pos.CENTER);
            lblArtNombre.setPrefHeight(80);
            lblArtNombre.setMaxHeight(80);
            lblArtNombre.setPrefWidth(195);
            lblArtNombre.setWrapText(true);
            lblArtNombre.setTextOverrun(OverrunStyle.CENTER_ELLIPSIS);
            lblArtNombre.setStyle(" -fx-text-fill: #292929; -fx-font: 14pt System; ");
            VBox.setVgrow(lblArtNombre, Priority.SOMETIMES);

            cont.getChildren().add(lblArtNombre);

            //SION.log(Modulo.VENTA, "Por empezar precios.", Level.INFO);
            //-- Precios
            HBox c1 = new HBox();
            c1.setAlignment(Pos.TOP_CENTER);
            c1.setPrefWidth(245);

            VBox v1 = new VBox();
            //--
                HBox cnt = new HBox(5);
                cnt.setAlignment(Pos.TOP_CENTER);
                cnt.setPrefWidth(245);

                Text tx1 = new Text("Precio regular:");
                tx1.setStyle(" -fx-font: 18pt System; ");
                Text tx2 = new Text( String.format("$%,.2f", articulo.getPrecio()) );
                tx2.setStyle("-fx-strikethrough: true; -fx-font: 18pt System; ");
                tx2.setFill(Color.web("#808080"));
                tx1.setFill(Color.web("#808080"));

                Text tx11 = new Text(String.format("($%,.2f)", articulo.getImporte()));
                tx11.setStyle("-fx-font: 18pt System; -fx-font-weight: bold;");
                tx11.setFill(Color.web("#990000"));


                cnt.getChildren().add(tx1);
                cnt.getChildren().add(tx2);
                cnt.getChildren().add(tx11);

                v1.getChildren().add(cnt);

                
                // SION.log(Modulo.VENTA, "Segunda parte.", Level.INFO);
                //--
                HBox cnt1 = new HBox(5);
                cnt1.setAlignment(Pos.TOP_CENTER);
                cnt1.setPrefWidth(245);
                


                Text tx112 = new Text("Ahorra:");
                tx112.setStyle("-fx-font: 25pt System; -fx-font-weight: bold;");
                tx112.setFill(Color.web("#12AD2A"));
                Text tx122 = new Text(String.format("$ %,.2f", articulo.getAhorro()));
                tx122.setStyle("-fx-font: 25pt System; -fx-font-weight: bold;");
                tx122.setFill(Color.web("#12AD2A"));

                cnt1.getChildren().add(tx112);
                cnt1.getChildren().add(tx122);


                v1.getChildren().add(cnt1);

                //--
                HBox cnt2 = new HBox(5);
                cnt2.setAlignment(Pos.TOP_CENTER);
                cnt2.setPrefWidth(245);

                Text tx21 = new Text("Total:");
                tx21.setFill(Color.web("#990000"));
                tx21.setStyle(" -fx-font: 16pt System;");
                cnt2.getChildren().add(tx21);

                Text tx22 = new Text(String.format("$%,.2f", articulo.getTotal()));            
                tx22.setFill(Color.web("#990000"));            
                tx22.setStyle(" -fx-font: 16pt System;");            

                cnt2.getChildren().add(tx22);

                v1.getChildren().add(cnt2);

            c1.getChildren().add(v1);

            cont.getChildren().add(c1);

            //-- Boton aceptar/rechazar promo

            btnAgregarArt.setStyle("-fx-background-color: #0f3b84; -fx-text-fill: #ffffff; -fx-font: 20pt System;");


            cont.getChildren().add(btnAgregarArt);

            paneContenido.getChildren().add(cont);

            this.getChildren().add(paneContenido);
            
            
            //SION.log(Modulo.VENTA, "Se concluyo la generación del elemento UIArticuloElemento sin errores.", Level.INFO);
        }catch(Exception e){
            SION.logearExcepcion(Modulo.VENTA, e, "generarUIArticuloElementos");
        }
        
    }
    
    public void asignarIndice(final int indiceActual){
        //----- Contenido del UIArticuloPromocion
       
        paneIndice.setPrefWidth(30);
        paneIndice.setPrefHeight(30);
        paneIndice.setLayoutX(0);
        paneIndice.setLayoutY(0);
        
        Circle c = new Circle(15);
        
        c.setLayoutX(15);
        c.setLayoutY(15);
        c.setFill(Color.web("#12AD2A"));
        //c.setFill(Color.TRANSPARENT);
        c.setStroke(Color.TRANSPARENT);
        c.setStrokeType(StrokeType.INSIDE);
        
        paneIndice.getChildren().add(c);
        
        /*Label lblIndxComponente = new Label( "Seleccionado");//String.valueOf(indiceActual) );
        lblIndxComponente.setAlignment(Pos.CENTER);
        lblIndxComponente.setContentDisplay(ContentDisplay.CENTER);
        lblIndxComponente.setPrefSize(60, 60);
        lblIndxComponente.setLayoutX(0);
        lblIndxComponente.setLayoutY(0);
        lblIndxComponente.setStyle(" -fx-text-fill: transparent; -fx-font: 15pt System; ");
                */
        paneIndice.setVisible(false);
        paneIndice.getChildren().add(new ImageView(check));
        this.indiceElemento = indiceActual;
        
        l = new EventHandler<javafx.scene.input.KeyEvent>() {
            @Override
            public void handle(javafx.scene.input.KeyEvent ke) {
                if (f2.match(ke) && indiceActual == 1 ) {
                    btnAgregarArt.fire();
                }else if (f3.match(ke) && indiceActual == 2) {
                     btnAgregarArt.fire();
                }else if (f4.match(ke) && indiceActual == 3) {
                     btnAgregarArt.fire();
                }else if (f5.match(ke) && indiceActual == 4) {
                     btnAgregarArt.fire();
                }else if (f6.match(ke) && indiceActual == 5) {
                     btnAgregarArt.fire();
                }else if (f7.match(ke) && indiceActual == 6) {
                     btnAgregarArt.fire();
                }else if (f8.match(ke) && indiceActual == 7) {
                     btnAgregarArt.fire();
                }else if (f9.match(ke) && indiceActual == 8) {
                     btnAgregarArt.fire();
                }
            }
        };
        
        this.getChildren().add(paneIndice);        
        
        this.altShortCut.setStyle("-fx-font: 12pt System;");
        if( indiceActual < 10 )
            this.altShortCut.setText("F"+(indiceActual+1));
    }
    
    
    EventHandler<javafx.scene.input.KeyEvent> l = null;
    public EventHandler getFiltroEventosF(){ 
           return l;
    }
    
   
    public EventHandler getFiltroEventosAltRelease(){
        return new EventHandler<javafx.scene.input.KeyEvent>() {
            @Override
            public void handle(javafx.scene.input.KeyEvent ke) {
                if( ke.getCode().equals(KeyCode.ALT) ){
                   if( altShortCut.isVisible() ){
                       altShortCut.setVisible(false);
                       altShortCut.setText("");
                   }else{
                       altShortCut.setVisible(true);
                       if( indiceElemento < 10 )
                            altShortCut.setText("F"+(indiceElemento+1));
                   }
                }
            }
        };
    } 
    
    
    /*public EventHandler getFiltroEventosAltPressed(){
        return new EventHandler<javafx.scene.input.KeyEvent>() {
            @Override
            public void handle(javafx.scene.input.KeyEvent ke) {
                if( ke.isAltDown() && !altShortCut.isVisible() ){
                   // altShortCut.setVisible(true);
                }
            }
        };
    }*/
    
}
