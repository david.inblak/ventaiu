/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.mvp;


/**
 *
 * @author dramirezr
 */
public interface IPromocionesPresentador {
    boolean esVentaCandidata(Object venta);
    void iniciarVista();
    Object getVistaPromociones();
    void agregarPromocion(Object modelo);
    void calculoCostoPromocionesSeleccionadas();

    void aceptarPromociones();
    void rechazarPromociones();
    void cerrarPromociones();

    void setComandoRechazarPromocion(IComando tareaRechazado);
    void setComandoAceptarPromocion(IComando tareaAceptado);
    void setComandoCerrarPromocion(IComando tareaCerrar);
}
