/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.mvp;

/**
 *
 * @author dramirezr
 */
public interface IPromocionesRepositorio {
    Object getPromocionesDisponibles();
    boolean esVentaCandidatoPromocion(Object venta);
    boolean esPromocionesActivas();
}
