
import java.util.HashSet;
import java.util.Set;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesRepositorio;
import neto.sion.tienda.venta.promociones.redondeo.modelo.ModeloArticulo;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dramirezr
 */
public class RepositorioFake implements IPromocionesRepositorio{
     Boolean redondeoActivo = null;
    final Object lock = new Object();
    Set<ModeloArticulo> articulosRedondeo = new HashSet<ModeloArticulo>();
    Boolean Existe_StoreProcedure = null;

    public RepositorioFake() {
        ModeloArticulo articuloFake = new ModeloArticulo();
        articuloFake.setAgranel(0.0);
        articuloFake.setCantidad(1);
        articuloFake.setCodBarras("00000");
        articuloFake.setCodBarraspadre("0000");
        articuloFake.setCosto(10.0);
        articuloFake.setDescuento(2.0);
        articuloFake.setId(100001L);
        articuloFake.setIdFamilia(5);
        articuloFake.setIdIEPS(0);
        articuloFake.setImagen(null);
        articuloFake.setImporte(8.0);
        articuloFake.setIva(0.0);
        articuloFake.setNombre("Chocolate Carlos V Suizo 1 pza");
        articuloFake.setNormaempaque(1);
        articuloFake.setPrecio(10.0);
        articuloFake.setTotal(8.0);
        articuloFake.setUnidad(1);
        
        articulosRedondeo.add(articuloFake);
        
        ModeloArticulo articuloFake2 = new ModeloArticulo();
        articuloFake2.setAgranel(0.0);
        articuloFake2.setCantidad(1);
        articuloFake2.setCodBarras("000002");
        articuloFake2.setCodBarraspadre("00002");
        articuloFake2.setCosto(10.0);
        articuloFake2.setDescuento(2.0);
        articuloFake2.setId(100002L);
        articuloFake2.setIdFamilia(5);
        articuloFake2.setIdIEPS(0);
        articuloFake2.setImagen(null);
        articuloFake2.setImporte(8.0);
        articuloFake2.setIva(0.0);
        articuloFake2.setNombre("Chocolates Carlos V Suizo 2 pza");
        articuloFake2.setNormaempaque(1);
        articuloFake2.setPrecio(10.0);
        articuloFake2.setTotal(8.0);
        articuloFake2.setUnidad(1);
        
        articulosRedondeo.add(articuloFake2);
        
        ModeloArticulo articuloFake23 = new ModeloArticulo();
        articuloFake23.setAgranel(0.0);
        articuloFake23.setCantidad(1);
        articuloFake23.setCodBarras("000003");
        articuloFake23.setCodBarraspadre("00003");
        articuloFake23.setCosto(10.0);
        articuloFake23.setDescuento(2.0);
        articuloFake23.setId(100003L);
        articuloFake23.setIdFamilia(5);
        articuloFake23.setIdIEPS(0);
        articuloFake23.setImagen(null);
        articuloFake23.setImporte(8.0);
        articuloFake23.setIva(0.0);
        articuloFake23.setNombre("Chocolates Carlos V Suizo 3 pza");
        articuloFake23.setNormaempaque(1);
        articuloFake23.setPrecio(10.0);
        articuloFake23.setTotal(8.0);
        articuloFake23.setUnidad(1);
        
        articulosRedondeo.add(articuloFake23);
    }
    
    

    @Override
    public Object getPromocionesDisponibles() {
        return articulosRedondeo;
    }

    @Override
    public boolean esVentaCandidatoPromocion(Object venta) {
        return true;
    }

    @Override
    public boolean esPromocionesActivas() {
        return true;
    }
    
}
