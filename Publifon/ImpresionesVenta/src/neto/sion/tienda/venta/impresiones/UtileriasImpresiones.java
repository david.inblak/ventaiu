package neto.sion.tienda.venta.impresiones;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;
import neto.sion.impresion.ventanormal.dto.ArticuloTicketDto;
import neto.sion.impresion.ventanormal.dto.TipoPagoTicketDto;
import neto.sion.impresion.ventanormal.util.UtileriaTicket;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.venta.servicios.cliente.dto.ArticuloDto;
import neto.sion.venta.servicios.cliente.dto.TipoPagoDto;

/**
 * Clase de utilieria para el procesamiento de los datos a imprimir en los
 * tickets.
 *
 * @author Carlos V. Perez L.
 */
public class UtileriasImpresiones {

    private double totalLetras;
    private DecimalFormat formato;

    public UtileriasImpresiones() {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(new Locale("es","MX"));
        formato = new DecimalFormat("0.00", dfs);
    }

    /**
     * **
     * Procesamiento de los articulos de venta de t.a para su impresion en un
     * ticket
     *
     * @param articulo
     * @param compania
     * @return Un objeto con los datos del articulo , listo para la impresion.
     */
    public ArticuloTicketDto articulosTADtoaTicket(ArticuloDto articulo, String compania) {
        ArticuloTicketDto articuloA = new ArticuloTicketDto();

        if (articulo.getFiAgranel() != 0.0) {
            String formatoCantidad = formato.format(articulo.getFiAgranel());
            articuloA.setCantidad(String.valueOf(formatoCantidad));
        } else {
            articuloA.setCantidad(String.valueOf((int) articulo.getFnCantidad()));
        }
        articuloA.setCantidad(String.valueOf(articulo.getFnCantidad()));
        articuloA.setImporte(String.valueOf(articulo.getFnPrecio() * articulo.getFnCantidad()));
        articuloA.setIva(articulo.getFnIva());
        articuloA.setNombre(compania + " $" + articulo.getFnPrecio());
        articuloA.setPrecio(String.valueOf(articulo.getFnPrecio()));
        return articuloA;
    }

    /**
     * **
     * Procesa los articulos de venta que seran impresos en ticket.
     *
     * @param articulos
     * @return Un arreglo de objetos con los datos de los articulo s, listos
     * para la impresion.
     */
    public ArticuloTicketDto[] articulosDtoaTicket(ArrayList<ArticuloDto> articulos) {
        ArticuloTicketDto[] articulosA = new ArticuloTicketDto[articulos.size()];
        int cont = 0;
        for (ArticuloDto articulo : articulos) {
            ArticuloTicketDto articuloA = new ArticuloTicketDto();

            if (articulo.getFiAgranel() != 0.0) {
                String formatoCantidad = formato.format(articulo.getFiAgranel());
                articuloA.setCantidad(String.valueOf(formatoCantidad));
            } else {
                articuloA.setCantidad(String.valueOf((int) articulo.getFnCantidad()));
            }
            articuloA.setCantidad(String.valueOf(articulo.getFnCantidad()));
            articuloA.setImporte(String.valueOf( (articulo.getFnPrecio() - articulo.getFnDescuento() ) * articulo.getFnCantidad()));
            articuloA.setIva(articulo.getFnIva());
            articuloA.setNombre(articulo.getFcNombreArticulo());
            articuloA.setPrecio(String.valueOf(articulo.getFnPrecio()));
            articuloA.setDescuento(formato.format(articulo.getFnDescuento()));
            articuloA.setIepsID(articulo.getFnIepsId());
            articulosA[cont] = articuloA;
            cont++;
        }
        return articulosA;
    }
    
    
    /**
     * **
     * Procesa los tipos de pagos que seran impresos en los tickets.
     *
     * @param tipos
     * @param totalVenta
     * @param montoRecibido
     * @param efectivoCambio
     * @return Un arreglo de objetos con los tipos de pagos , listos para
     * imprimirse en los tickets.
     */
    public TipoPagoTicketDto[] tipoPagoDtoaTicket(ArrayList<TipoPagoDto> tipos, Double totalVenta, double montoRecibido, double efectivoCambio) {
        return tipoPagoDtoaTicket(tipos, totalVenta, montoRecibido, efectivoCambio, 0);
    }

    /**
     * **
     * Procesa los tipos de pagos que seran impresos en los tickets.
     *
     * @param tipos
     * @param totalVenta
     * @param montoRecibido
     * @param efectivoCambio
     * @param totalDescuento
     * @return Un arreglo de objetos con los tipos de pagos , listos para
     * imprimirse en los tickets.
     */
    public TipoPagoTicketDto[] tipoPagoDtoaTicket(ArrayList<TipoPagoDto> tipos, Double totalVenta, double montoRecibido, double efectivoCambio, double totalDescuento) {
        TipoPagoTicketDto[] tiposA;
        
        int tpoPagoDescuento = 0;
        if( totalDescuento > 0 ) tpoPagoDescuento = 1;
        
        if (efectivoCambio >= 0.0) {
            tiposA = new TipoPagoTicketDto[tipos.size() + 2 + tpoPagoDescuento];
        } else {
            tiposA = new TipoPagoTicketDto[tipos.size() + 1 + tpoPagoDescuento];
        }
        TipoPagoTicketDto tipoTotal = new TipoPagoTicketDto();
        tipoTotal.setDescripcionTipoPago(SION.obtenerMensaje(Modulo.VENTA, "ventas,ticket.tipopago.total"));
        tipoTotal.setMontoPago(String.valueOf(totalVenta));
        tipoTotal.setTipoPago(6);
        tiposA[0] = tipoTotal;
        int cont = 1;
        for (int i = 0; i < tipos.size(); i++) {
            TipoPagoTicketDto tipoA = new TipoPagoTicketDto();
            switch (tipos.get(i).getFiTipoPagoId()) {
                case 1:
                    tipoA.setDescripcionTipoPago(SION.obtenerMensaje(Modulo.VENTA, "ventas.ticket.tipopago1"));
                    tipoA.setMontoPago(String.valueOf(montoRecibido));
                    tipoA.setTipoPago(1);
                    tiposA[cont] = tipoA;
                    cont++;
                    break;
                case 2:
                    tipoA.setDescripcionTipoPago(SION.obtenerMensaje(Modulo.VENTA, "tipo.pago.dos"));
                    tipoA.setMontoPago(String.valueOf(tipos.get(i).getFnMontoPago()));
                    tipoA.setTipoPago(2);
                    tiposA[cont] = tipoA;
                    cont++;
                    break;
                case 3:
                    tipoA.setDescripcionTipoPago(SION.obtenerMensaje(Modulo.VENTA, "ventas.ticket.tipopago3"));
                    tipoA.setMontoPago(String.valueOf(tipos.get(i).getFnMontoPago()));
                    tipoA.setTipoPago(3);
                    tiposA[cont] = tipoA;
                    cont++;
                    break;
            }
        }
        
        if (efectivoCambio >= 0.0){
            TipoPagoTicketDto cambio = new TipoPagoTicketDto();
            cambio.setDescripcionTipoPago(SION.obtenerMensaje(Modulo.VENTA, "ventas.ticket.tipopago.cambio"));
            cambio.setMontoPago(String.valueOf(efectivoCambio));
            cambio.setTipoPago(5);
            tiposA[cont] = cambio;
            cont++;
        }
        
        if( totalDescuento > 0 ){
            TipoPagoTicketDto descuento = new TipoPagoTicketDto();
            descuento.setDescripcionTipoPago(SION.obtenerMensaje(Modulo.VENTA, "ventas.ticket.total.descuento"));
            descuento.setMontoPago(String.valueOf(totalDescuento));
            descuento.setTipoPago(7);
            tiposA[cont] = descuento;
        }
        
        //Sumamos la comision total
        UtileriaTicket util = new UtileriaTicket();
        totalLetras = util.quitarFormatoCantidad(tiposA[0].getMontoPago(), 1);
        tiposA[0].setMontoPago(String.valueOf(totalLetras));
        return tiposA;
    }

    /**
     * Devuelve el valor del total de la venta antes de ser convertido a cadena
     *
     * @return Un double
     */
    public double getTotalLetras() {
        return totalLetras;
    }
}
