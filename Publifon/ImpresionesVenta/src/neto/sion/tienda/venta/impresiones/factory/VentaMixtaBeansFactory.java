package neto.sion.tienda.venta.impresiones.factory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import neto.sion.impresion.ventanormal.dto.VentaMixtaTicket;

/**
 *
 * @author agomez
 */
public class VentaMixtaBeansFactory {

    private VentaMixtaTicket impresionVtaMixta;
    
    public Collection getCollection(){
        List<VentaMixtaTicket> list = new ArrayList<VentaMixtaTicket>();
        list.add(impresionVtaMixta);
        return list;
    }
    
    public void setVentaServiciosTicketDto(VentaMixtaTicket bean){
        impresionVtaMixta = bean;
    }
}
