package neto.sion.tienda.venta.impresion.bean;

import neto.sion.impresion.ventanormal.dto.VentaNormalTicketDto;
import neto.sion.impresion.ventanormal.dto.VentaTarjetaVoucher;
import neto.sion.venta.servicios.cliente.dto.ArticuloDto;
import neto.sion.venta.servicios.cliente.dto.ReimpresionTarjetaDto;

/**
 * Clase que represetan la respuesta de los datos de reimpresion de ticket y
 * voucher.
 *
 * @author Carlos V. Perez L.
 */
public class RespuestaReimpresionBean {

    private int paCdgError;
    private String paDescError;
    private int paCdgErrorVouchers;
    private String paDescErrorVouchers;
    private String nombreUsuario;
    private VentaNormalTicketDto ticket;
    private long paMovimientoId;
    private ArticuloDto[] articulos;
    private VentaTarjetaVoucher[] vouchers;
    private ReimpresionTarjetaDto[] voucherConfirmacion;

    public int getPaCdgError() {
        return paCdgError;
    }

    public void setPaCdgError(int paCdgError) {
        this.paCdgError = paCdgError;
    }

    public String getPaDescError() {
        return paDescError;
    }

    public void setPaDescError(String paDescError) {
        this.paDescError = paDescError;
    }

    public VentaNormalTicketDto getTicket() {
        return ticket;
    }

    public void setTicket(VentaNormalTicketDto ticket) {
        this.ticket = ticket;
    }

    public ArticuloDto[] getArticulos() {
        return articulos;
    }

    public void setArticulos(ArticuloDto[] articulos) {
        this.articulos = articulos;
    }

    public VentaTarjetaVoucher[] getVouchers() {
        return vouchers;
    }

    public void setVouchers(VentaTarjetaVoucher[] vouchers) {
        this.vouchers = vouchers;
    }

    public int getPaCdgErrorVouchers() {
        return paCdgErrorVouchers;
    }

    public void setPaCdgErrorVouchers(int paCdgErrorVouchers) {
        this.paCdgErrorVouchers = paCdgErrorVouchers;
    }

    public String getPaDescErrorVouchers() {
        return paDescErrorVouchers;
    }

    public void setPaDescErrorVouchers(String paDescErrorVouchers) {
        this.paDescErrorVouchers = paDescErrorVouchers;
    }

    public long getPaMovimientoId() {
        return paMovimientoId;
    }

    public void setPaMovimientoId(long paMovimientoId) {
        this.paMovimientoId = paMovimientoId;
    }

    public ReimpresionTarjetaDto[] getVoucherConfirmacion() {
        return voucherConfirmacion;
    }

    public void setVoucherConfirmacion(ReimpresionTarjetaDto[] voucherConfirmacion) {
        this.voucherConfirmacion = voucherConfirmacion;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    @Override
    public String toString() {
        return "RespuestaReimpresionBean{" + "paCdgError=" + paCdgError + ", paDescError=" + paDescError + ", paCdgErrorVouchers=" + paCdgErrorVouchers + ", paDescErrorVouchers=" + paDescErrorVouchers + ", nombreUsuario=" + nombreUsuario + ", ticket=" + ticket + ", paMovimientoId=" + paMovimientoId + ", articulos=" + articulos + ", vouchers=" + vouchers + ", voucherConfirmacion=" + voucherConfirmacion + '}';
    }
}
