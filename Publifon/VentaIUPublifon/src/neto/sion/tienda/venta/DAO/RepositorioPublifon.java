/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.modelo.ParametrosBean;
import oracle.jdbc.internal.OracleTypes;

/**
 *
 * @author dramirezr
 */
public class RepositorioPublifon {

    public static class PromoTAReq {
        private long idConciliacion;
        private double montoVenta;
        private long usuarioId;

        public PromoTAReq(long idConciliacion, double montoVenta, long usuarioId) {
            this.idConciliacion = idConciliacion;
            this.montoVenta = montoVenta;
            this.usuarioId = usuarioId;
        }
        
        

        public long getIdConciliacion() {
            return idConciliacion;
        }

        public void setIdConciliacion(long idConciliacion) {
            
        }

        public double getMontoVenta() {
            return montoVenta;
        }

        public void setMontoVenta(double montoVenta) {
            this.montoVenta = montoVenta;
        }

        public long getUsuarioId() {
            return usuarioId;
        }

        public void setUsuarioId(long usuarioId) {
            this.usuarioId = usuarioId;
        }

        @Override
        public String toString() {
            return "PromoTAReq{" + "idConciliacion=" + idConciliacion + ", montoVenta=" + montoVenta + ", usuarioId=" + usuarioId + '}';
        }
        
        
        
    }
    
    public static class PromoTAResp {
        private String codigoGanador;
        private int codigoError;
        private String mensaje;

        public int getCodigoError() {
            return codigoError;
        }

        public void setCodigoError(int codigoError) {
            this.codigoError = codigoError;
        }

        public String getCodigoGanador() {
            return codigoGanador;
        }

        public void setCodigoGanador(String codigoGanador) {
            this.codigoGanador = codigoGanador;
        }

        public String getMensaje() {
            return mensaje;
        }

        public void setMensaje(String mensaje) {
            this.mensaje = mensaje;
        }

        @Override
        public String toString() {
            return "PromoTAResp{" + "codigoGanador=" + codigoGanador + ", codigoError=" + codigoError + ", mensaje=" + mensaje + '}';
        }
        
        
              
    }
    
    public static PromoTAResp esVentaCandidatoPromoTA(PromoTAReq solicitud){
        PromoTAResp respuesta = null;      
                
        String sql = null;
        CallableStatement cs = null;
        Connection conn = null;

        respuesta  = new PromoTAResp();
        
        SION.log(Modulo.VENTA, "Solicitud >> " + solicitud, Level.INFO);

        try{

            sql = SION.obtenerParametro(Modulo.VENTA,"USRVELIT.PAWSCODIGOSPUBLIFON.SPCDGSPUBLIFON");
            SION.log(Modulo.VENTA,"SQL a ejecutar... " + sql, Level.FINE);
            conn = Conexion.obtenerConexion();//PoolConexiones.getInstanciaPool().getPool().getConnection();
            cs = Conexion.prepararLlamada(sql);	
            cs.setLong(1, solicitud.getIdConciliacion());
            cs.setDouble(2, solicitud.getMontoVenta());
            cs.setLong(3, solicitud.getUsuarioId());
            cs.registerOutParameter(4, OracleTypes.VARCHAR);
            cs.registerOutParameter(5, OracleTypes.NUMBER);
            cs.registerOutParameter(6, OracleTypes.VARCHAR);

            cs.execute();						

            respuesta.setCodigoGanador  (cs.getString(4));
            respuesta.setCodigoError    (cs.getInt   (5));
            respuesta.setMensaje        (cs.getString(6));

            SION.log(Modulo.VENTA, "respuesta BD Publifone >> " + respuesta, Level.INFO);
        } catch(Exception e){
                SION.logearExcepcion(Modulo.VENTA, e, sql);
        } finally {
                Conexion.cerrarRecursos(conn, cs, null);
        }
            
        return respuesta;
    }
}
