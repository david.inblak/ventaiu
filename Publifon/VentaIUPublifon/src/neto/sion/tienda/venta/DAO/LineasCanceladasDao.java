/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.modelo.PeticionCancelacionVentaBean;
import neto.sion.tienda.venta.modelo.RespuestaCancelacionVentaBean;
import oracle.jdbc.internal.OracleTypes;

/**
 *
 * @author fvega
 */
public class LineasCanceladasDao {

    private String sql;
    private CallableStatement cs;

    public LineasCanceladasDao() {
        this.sql = null;
        this.cs = null;
    }

    public RespuestaCancelacionVentaBean insertaCancelacionVenta(PeticionCancelacionVentaBean _peticion) {
        
        SION.log(Modulo.VENTA, "Peticion de cancelacion de venta: "+_peticion, Level.INFO);
        RespuestaCancelacionVentaBean respuesta = new RespuestaCancelacionVentaBean();
        Connection conn = null; 
        try {

            sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAVENTAS.SPCANCELACIONLINEASVENTA");
            SION.log(Modulo.VENTA, "SQL a ejecutar... " + sql, Level.INFO);
            conn = Conexion.obtenerConexion();//PoolConexiones.getInstanciaPool().getPool().getConnection();
            cs = conn.prepareCall(sql);
            cs.setLong(1, _peticion.getUsuarioCancela());
            cs.setString(2, _peticion.getFecha());
            cs.setLong(3, _peticion.getUsuarioAutoriza());
            cs.setLong(4, _peticion.getNumeroTransaccion());
            cs.setInt(5, _peticion.getLineasCanceladas());
            cs.setInt(6, _peticion.getVentasCanceladas());
            cs.registerOutParameter(7, OracleTypes.NUMBER);
            cs.registerOutParameter(8, OracleTypes.VARCHAR);
            
            cs.execute();

            respuesta.setCodigoError(cs.getInt(7));
            respuesta.setDescripcionError(cs.getString(8));
            
            SION.log(Modulo.VENTA, "Respuesta recibida: " + respuesta.toString(), Level.INFO);

        } catch (Exception e) {
            respuesta = null;
            SION.logearExcepcion(Modulo.VENTA, e, sql);
        } finally {
            Conexion.cerrarRecursos(conn, cs, null);
        }

        return respuesta;
    }
    
    public static void main(String[] args) {
        LineasCanceladasDao dao = new LineasCanceladasDao();
        
        PeticionCancelacionVentaBean peticion = new PeticionCancelacionVentaBean();
        
        peticion.setFecha("03/10/2012");
        peticion.setLineasCanceladas(21);
        peticion.setNumeroTransaccion(0);
        peticion.setUsuarioAutoriza(741346);
        peticion.setUsuarioCancela(651460);
        peticion.setVentasCanceladas(0);
        
        RespuestaCancelacionVentaBean respuesta = new RespuestaCancelacionVentaBean();
        
        respuesta = dao.insertaCancelacionVenta(peticion);
        
        
    }
    
}
