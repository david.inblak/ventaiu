package neto.sion.tienda.venta.DAO;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.modelo.DatosFormasPagoBean;
import oracle.jdbc.internal.OracleTypes;

public class TipoTarjetaDao {
    
        public int consultaTipoTarjeta(String numeroTarjeta, Double montoPago)
	{
                if (numeroTarjeta.length() >= 16) {
                    SION.log(Modulo.VENTA, "Consultando tarjeta " + numeroTarjeta.substring(0, 6).concat("XXXXXXXXXX"), Level.INFO);
                }
                int tipoTarjeta=0;
		String sql = null;
		CallableStatement cs = null;
		//ResultSet rs = null;
                Connection conn = null;
                
                DatosFormasPagoBean datos = new DatosFormasPagoBean(0, 0, 0, "", 0, 0, 0);
		try{
                   
			sql = SION.obtenerParametro(Modulo.VENTA,"USRVELIT.PAVENTAS.SPCONSULTATIPOTARJETA");
                        SION.log(Modulo.VENTA,"SQL a ejecutar... " + sql, Level.FINE);
                        conn = Conexion.obtenerConexion();//PoolConexiones.getInstanciaPool().getPool().getConnection();
                        cs = conn.prepareCall(sql);	
			cs.setString(1, numeroTarjeta);	
			cs.registerOutParameter(2, OracleTypes.NUMBER);
			cs.execute();						
                        
                        /*datos.setFitipopagoid(2);
                        datos.setFnmontopago(montoPago);
                        datos.setFnnumerovales(0);
                        datos.setFcnumerotarjeta(numeroTarjeta);
                        datos.setFitipotarjeta(cs.getInt(2));
                        datos.setTarjetaidbus(Long.valueOf(0));*/
                        
                        tipoTarjeta=cs.getInt(2);
                        
                        /*if(tipoTarjeta==1){
                            datos.setComisiontarjeta(montoPago+1.05);
                        }else if(tipoTarjeta==2){
                            datos.setComisiontarjeta(montoPago+(montoPago*2.05));
                        }*/
                        
                        SION.log(Modulo.VENTA,"Resultado... " + tipoTarjeta, Level.FINE);
		}
		catch(Exception e)
		{
			SION.logearExcepcion(Modulo.VENTA, e, sql);
		}
		finally
		{
			Conexion.cerrarCallableStatement(cs);
			Conexion.cerrarConexion(conn);
		}
            
                return tipoTarjeta;
                
        }
	
	
}
