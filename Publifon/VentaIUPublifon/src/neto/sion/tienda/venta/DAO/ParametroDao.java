/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.logging.Level;

import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import oracle.jdbc.internal.OracleTypes;
import neto.sion.tienda.venta.modelo.PeticionParametroBean;
import neto.sion.tienda.venta.modelo.RespuestaParametroBean;


/**
 *
 * @author fvega
 */
public class ParametroDao extends Conexion {

    private String sql;
    private CallableStatement cs;
    private Connection conn;

    public ParametroDao() {
        this.sql = null;
        this.cs = null;
    }

    public RespuestaParametroBean consultaParametroOperacion(PeticionParametroBean _peticion) {

        SION.log(Modulo.VENTA, "Petición de parámetro de operación: " + _peticion.toString(), Level.INFO);

        RespuestaParametroBean respuesta = new RespuestaParametroBean();

        try {

            sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS");
            
            conn = Conexion.obtenerConexion();//PoolConexiones.getInstanciaPool().getPool().getConnection();
            cs = conn.prepareCall(sql);//Conexion.prepararLlamada(sql);
            cs.setInt(1, _peticion.getPaisId());
            cs.setInt(2, _peticion.getSistemaId());
            cs.setInt(3, _peticion.getModuloId());
            cs.setInt(4, _peticion.getSubmoduloId());
            cs.setInt(5, _peticion.getConfiguracionId());
            cs.registerOutParameter(6, OracleTypes.NUMBER);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.NUMBER);
            cs.execute();

            respuesta.setCodigoError(cs.getInt(6));
            respuesta.setDescripcionError(cs.getString(7));
            respuesta.setValorConfiguracion(cs.getDouble(8));

            //SION.log(Modulo.VENTA, "Respuesta recibida: " + respuesta.toString(), Level.INFO);

        } catch (Exception e) {
            respuesta = null;
            SION.log(Modulo.VENTA, "SQL a ejecutar... " + sql, Level.INFO);
            SION.logearExcepcion(Modulo.VENTA, e, sql);
        } finally {
            Conexion.cerrarRecursos(conn, cs, null);
        }

        return respuesta;

    }

    public RespuestaParametroBean actualizaParametro(PeticionParametroBean _peticion, int _fnValor) {
        RespuestaParametroBean respuesta = null;
        this.sql = "";
        this.cs = null;
        this.conn = null;
        
        SION.log(Modulo.VENTA, "Petición de Actualización de parámetro: "+ _peticion.toString(), Level.INFO);
        
        try {
            this.sql = SION.obtenerParametro(Modulo.VENTA, "VENTAFL.ACTUALIZA.PARAMETRO");
            this.conn = Conexion.obtenerConexion();//PoolConexiones.getInstanciaPool().getPool().getConnection();//obtenerConexion();
            this.cs = this.conn.prepareCall(this.sql);

            this.cs.setInt(1, _peticion.getPaisId());
            this.cs.setInt(2, _peticion.getSistemaId());
            this.cs.setInt(3, _peticion.getModuloId());
            this.cs.setInt(4, _peticion.getSubmoduloId());
            this.cs.setInt(5, _peticion.getConfiguracionId());
            this.cs.setInt(6, _fnValor);
            this.cs.registerOutParameter(7, 2);
            this.cs.registerOutParameter(8, 12);

            this.cs.execute();

            respuesta = new RespuestaParametroBean();
            respuesta.setCodigoError(this.cs.getInt(7));
            respuesta.setDescripcionError(this.cs.getString(8));
        } catch (Exception e) {
            respuesta = null;
            SION.logearExcepcion(Modulo.MonitorLocalFL, e, new String[]{this.sql});
        } finally {
            cerrarRecursos(this.conn, this.cs, null);
            this.sql = "";
        }
        
        SION.log(Modulo.VENTA, "Respuesta de Actualización de parámetro: "+ respuesta.toString(), Level.INFO);
        
        return respuesta;
    }

    public RespuestaParametroBean insertaHistoricoCambioParametroDAO(PeticionParametroBean _peticion, int _fnValor) {
        RespuestaParametroBean respuesta = null;
        this.sql = "";
        this.cs = null;
        this.conn = null;
        
        SION.log(Modulo.VENTA, "Petición de inserción de histórico de FL: "+ _peticion.toString(), Level.INFO);
        
        try {
            this.sql = SION.obtenerParametro(Modulo.MonitorLocalFL, "MONITORFL.HISTORICO.PARAMETRO");
            this.conn = Conexion.obtenerConexion();//PoolConexiones.getInstanciaPool().getPool().getConnection();//obtenerConexion();
            this.cs = this.conn.prepareCall(this.sql);

            this.cs.setInt(1, _peticion.getPaisId());
            this.cs.setInt(2, _peticion.getSistemaId());
            this.cs.setInt(3, _peticion.getModuloId());
            this.cs.setInt(4, _peticion.getSubmoduloId());
            this.cs.setInt(5, _peticion.getConfiguracionId());
            this.cs.setDouble(6, _fnValor);

            this.cs.registerOutParameter(7, 2);
            this.cs.registerOutParameter(8, 12);

            this.cs.execute();

            respuesta = new RespuestaParametroBean();
            respuesta.setCodigoError(this.cs.getInt(7));
            respuesta.setDescripcionError(this.cs.getString(8));
        } catch (Exception e) {
            respuesta = null;
            SION.logearExcepcion(Modulo.MonitorLocalFL, e, new String[]{this.sql});
        } finally {
            cerrarRecursos(this.conn, this.cs, null);
            this.sql = "";
        }

        SION.log(Modulo.VENTA, "Respuesta de inserción de histórico de FL: "+ _peticion.toString(), Level.INFO);
        
        return respuesta;
    }
}
