/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.vista;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.bean.RespuestaLeerTarjetaDescuentoBean;
import neto.sion.tienda.controles.vista.Utilerias;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.bindings.VentaBindings;
import neto.sion.tienda.venta.controlador.ControladorTarjetasDescuento;
import neto.sion.tienda.bean.DatosTarjeta;
import neto.sion.tienda.pt.iso8583.descuentos.dto.RespuestaValidaDescuentoArticulosDto;
import neto.sion.tienda.venta.controlador.TarjetaDescuentos;
import neto.sion.tienda.venta.modelo.DatosFormasPagoBean;
import neto.sion.tienda.venta.modelo.OperacionTarjetaBean;
import neto.sion.tienda.venta.modelo.PeticionPagoTarjetaBean;
import neto.sion.tienda.venta.modelo.RespuestaPagoTarjetaBean;
//import neto.sion.tienda.venta.pinpad.bean.PeticionPagoTarjetaBean;
//import neto.sion.tienda.venta.pinpad.bean.RespuestaPagoTarjetaBean;
//import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;
import neto.sion.tienda.venta.vistamodelo.VentaVistaModelo;

/**
 *
 * @author fvega
 */
public class CobroTarjetaVista {

    //instancias de clase    
    private VentaBindings ventaBinding;
    private VentaVistaModelo ventaVistaModelo;
    private VentaVista ventaVista;
    private Utilerias utilerias;
    //contenedores
    public Stage stage;
    private Scene escena;
    private Group grupo;
    public BorderPane panel;
    private VBox cajaSuperior;
    private VBox cajaInferior;
    //cajaCabecero
    private VBox cajaCabecero;
    //cajaInferior
    private VBox cajaLogo;
    //cajaCabecero
    private Label etiquetaCabecero;
    //cajaLogo
    private Image imagenLogo;
    private ImageView vistaImagenLogo;
    //
    private boolean fuePagoExitoso;
    private boolean necesitaNIP;
    
    
    private StackPane paneContenedorTarjeta;
    private StackPane paneOverlayCajaNip;
    
    private VBox cajaNip;
    private VBox cajaNipEncabezado;
    private VBox cajaNipCuerpo;
    
    private Label lblTituloNip;
    private Label lblMsgNip;
    
    private VBox cajaTextNip;
    private PasswordField pwdNipTarjeta;
    private Label lblErrorNip;
    
    private HBox cajaBotonesNip;
    private Button btnAceptarNip;
    private Button btnCancelarNip;
    
    private int anchoCajaContenedor = 850;
    private int altoCajaContenedor = 550;
    

    public CobroTarjetaVista(VentaBindings _ventaBindings, VentaVistaModelo _ventaVistaModelo,
            VentaVista _ventaVista, Utilerias _utilerias) {

        SION.log(Modulo.VENTA, "Ingresando a constructor de vista de cobro con tarjetas", Level.INFO);

        ventaBinding = _ventaBindings;
        ventaVistaModelo = _ventaVistaModelo;
        ventaVista = _ventaVista;
        this.utilerias = _utilerias;
        
        
        

        this.grupo = new Group();
        this.panel = new BorderPane();

        //panel
        this.cajaSuperior = utilerias.getVBox(5, 850, 80);
        this.cajaInferior = utilerias.getVBox(5, 850, 550);

        //cajaSuperior
        this.cajaCabecero = utilerias.getVBox(0, 850, 80);

        //cajaCabecero
        this.etiquetaCabecero = utilerias.getLabel("Inserte chip o deslice tarjeta", 770, 70);
        
        this.paneContenedorTarjeta = new StackPane();
        this.paneContenedorTarjeta.setAlignment(Pos.CENTER);
        
        //PANE OVERLAY PARA VENTANA NIP
        this.paneOverlayCajaNip =  new StackPane();
        this.paneOverlayCajaNip.setAlignment(Pos.BOTTOM_CENTER);
        
        int anchoCajaNip = 600;
        int altoCajaNip = 320;
        
        this.cajaNip = utilerias.getVBox(0, anchoCajaNip, altoCajaNip);
        this.cajaNip.setAlignment(Pos.TOP_CENTER);
        this.cajaNipEncabezado = utilerias.getVBox(0, anchoCajaNip, 60);
        this.cajaNipCuerpo = utilerias.getVBox(20, anchoCajaNip, 260);
        
        
        //this.lblTituloNip = utilerias.getLabel("La tarjeta requiere NIP de seguridad",  800, 100);
        
        this.lblTituloNip = new Label("La tarjeta requiere NIP de seguridad");
        this.lblMsgNip = new Label("¡Ingresa el codigo NIP de seguridad!");
        
        
        this.cajaTextNip = utilerias.getVBox(20, anchoCajaNip, 140);
        this.pwdNipTarjeta = utilerias.getPwdField("0", 100, 50);
        this.pwdNipTarjeta.setId("nipTarjeta");
        ventaBinding.formatearCampoNip(pwdNipTarjeta, 4);
        
        lblErrorNip = new Label("¡El NIP se deguridad debe de ser de cuatro digitos!");
        
        this.cajaBotonesNip = utilerias.getHBox(50, anchoCajaNip, 90);
        this.btnAceptarNip = utilerias.getButton("Aceptar", 200, 50);
        this.btnCancelarNip = utilerias.getButton("Cancelar", 200, 50);
        

        //cajaInferior
        this.cajaLogo = utilerias.getVBox(0, anchoCajaContenedor, altoCajaContenedor);

        //cajaLogo
        this.imagenLogo = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/fondo_tarjetas.jpg");
        
        
        this.vistaImagenLogo = new ImageView(imagenLogo);

        //implementacion chip
        this.fuePagoExitoso = false;
        this.necesitaNIP = false;

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                
                

                escena = new Scene(grupo, 850, 550, Color.TRANSPARENT);
                
                escena.getStylesheets().addAll(CobroTarjetaVista.class.getResource("/neto/sion/tienda/venta/css/VistaTarjetas.bss").toExternalForm());
                
                stage = new Stage(StageStyle.UNDECORATED);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initStyle(StageStyle.TRANSPARENT);
                
                
            }
        });

        establecerEstilos();
        establecerDatosComponentes();
    }

    private void establecerEstilos() {

        SION.log(Modulo.VENTA, "Agregando estilos a stage de pago con tarjetas", Level.INFO);

        etiquetaCabecero.setStyle("-fx-font: bold 25pt Verdana; -fx-alignment: center;");

        cajaSuperior.setStyle("-fx-alignment: center;");
        cajaInferior.setStyle("-fx-alignment: center;");

        cajaCabecero.setStyle("-fx-alignment: center; -fx-border-color: white white gray white; -fx-border-width: 6;");

        cajaLogo.getStyleClass().add("cajaLogo");
        
        paneOverlayCajaNip.setStyle("-fx-background-color: transparent;");
        
        cajaNip.getStyleClass().add("cajaNip");
                
        paneContenedorTarjeta.setStyle("-fx-background-color: transparent;");
        cajaNipEncabezado.getStyleClass().add("cajaNipEncabezado");
        lblTituloNip.getStyleClass().add("lblTituloNip");
        lblTituloNip.setContentDisplay(ContentDisplay.CENTER);
        
        lblMsgNip.setContentDisplay(ContentDisplay.CENTER);
        lblMsgNip.getStyleClass().add("lblMsgNip");
        
        cajaNipCuerpo.getStyleClass().add("cajaNipCuerpo");
        
        cajaTextNip.setAlignment(Pos.CENTER);
        pwdNipTarjeta.getStyleClass().add("TextFieldFormasPago");
        lblErrorNip.getStyleClass().add("lblErrorNip");
        
        btnAceptarNip.getStyleClass().add("btnAceptar");
        btnCancelarNip.getStyleClass().add("btnCancelar");
        cajaBotonesNip.setAlignment(Pos.CENTER);
        
        panel.setStyle("-fx-background-color: transparent;");
        grupo.setStyle("-fx-background-color: transparent;");
        //panel.setStyle("-fx-background-repeat: no-repeat; -fx-background-color: white ; -fx-border-color: transparent, orange, transparent ; -fx-border-width: 10; -fx-border-radius: 10; -fx-border-insets:-7, -3, 0");
        
    }

    private void establecerDatosComponentes() {

        SION.log(Modulo.VENTA, "Ajustando elementode de Stage de tarjetas", Level.INFO);

        vistaImagenLogo.setFitHeight(500);
        vistaImagenLogo.setFitWidth(780);

        cajaCabecero.getChildren().addAll(etiquetaCabecero);

        cajaLogo.getChildren().addAll(vistaImagenLogo);
        
        cajaNipEncabezado.getChildren().addAll(lblTituloNip);
        
        lblErrorNip.setVisible(false);
        cajaTextNip.getChildren().addAll(lblMsgNip,pwdNipTarjeta,lblErrorNip);
        cajaBotonesNip.getChildren().addAll(btnAceptarNip,btnCancelarNip);
        
        cajaNipCuerpo.getChildren().addAll(cajaTextNip,cajaBotonesNip); 
        
        
        cajaNip.getChildren().addAll(cajaNipEncabezado,cajaNipCuerpo);
        //cajaNip.setVisible(false);
        
        
        //add vista logo a pane
        paneOverlayCajaNip.getChildren().addAll(cajaNip);
        
        
        
        paneOverlayCajaNip.setVisible(false);
        
        //cajaInferior.getChildren().addAll(cajaLogo,paneOverlayCajaNip);
        
        paneContenedorTarjeta.getChildren().addAll(cajaLogo,paneOverlayCajaNip);
        //cajaSuperior.getChildren().addAll(cajaCabecero);
        //cajaInferior.getChildren().addAll(cajaLogo);

        //panel.setTop(cajaSuperior);
        panel.setCenter(paneContenedorTarjeta);
        panel.setMinHeight(550);
        panel.setMaxHeight(550);
        panel.setMinWidth(850);
        panel.setMaxWidth(850);
        
        
        
        accionBtnAceptarNip(btnAceptarNip);
        accionBtnCancelaNip(btnCancelarNip);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                grupo.getChildren().addAll(panel);
            }
        });

    }

    public boolean estaStageMostrandose() {
        return stage.isShowing();
    }

    public Stage getStageTarjetas() {
        return stage;
    }
    
    private void mosntrarAlertaRetornaFoco(final String mensaje, final TipoMensaje tipo, final Control control){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                AdministraVentanas.mostrarAlertaRetornaFoco(mensaje, tipo, control);
            }
        });
    }
    
    public void leerTarjetaDescuento(){
        final Service servicio = new Service(){
            @Override
            protected Task createTask(){
                return new Task(){
                    @Override
                    protected Object call() throws Exception{
                        
                        RespuestaLeerTarjetaDescuentoBean respuestaLeerTarjeta = ventaVistaModelo.leerTarjeta();
                        
                        SION.log(Modulo.VENTA, "Obteniendo respuesta de lectura.", Level.WARNING);
                        if( respuestaLeerTarjeta != null ){
                            if( respuestaLeerTarjeta.isEsTarjetaDescuento() ){

                                if ( respuestaLeerTarjeta.getIdError() != 0 ){
                                    final String mensaje = respuestaLeerTarjeta.getMensajeError();
                                    Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            AdministraVentanas.mostrarAlertaRetornaFoco(
                                                    SION.obtenerMensaje(Modulo.VENTA,mensaje), TipoMensaje.ERROR, ventaVista.getTablaVentas());
                                        }
                                    });
                                }else{
                                    SION.log(Modulo.VENTA, "Sí es una tarjeta de descuento.", Level.WARNING);
                                    DatosTarjeta datosTarj = new DatosTarjeta(
                                            respuestaLeerTarjeta.getEmisorTarjetaDescuento(),
                                            respuestaLeerTarjeta.getTrack2() );

                                    //ventaVistaModelo.getCtrlTarjetasDescuento().creartarjetaDescuento(datosTarj);
                                    if( !ventaVistaModelo.creartarjetaDescuento(datosTarj) ){
                                        mosntrarAlertaRetornaFoco("Esta tarjeta de descuentos ya se encuentra agregada.", 
                                                TipoMensaje.ERROR, ventaVista.getTablaVentas());
                                    }
                                    
                                    ventaVistaModelo.actualizaArticulosXValidar( ventaVistaModelo.getListaArticulos() );
                                    RespuestaValidaDescuentoArticulosDto respValidacion =
                                            ventaVistaModelo.getCtrlTarjetasDescuento().validarDescuentoArticulos( datosTarj, true );//Forzar consulta
                                    
                                    if( respValidacion != null ){
                                        ventaVistaModelo.actualizarListaArticulos( ventaVistaModelo.getCtrlTarjetasDescuento().getArticulosValidados() );
                                        if( respValidacion.getErrorId() != 0 ){
                                            SION.log(Modulo.VENTA, "Respuesta de error: "+
                                                    respValidacion.getErrorId()+", "+ respValidacion.getErrorMensaje(), Level.WARNING);
                                            mosntrarAlertaRetornaFoco(respValidacion.getErrorMensaje(), 
                                                    TipoMensaje.ERROR, ventaVista.getTablaVentas());
                                        }
                                        Platform.runLater(new Runnable(){
                                            @Override
                                            public void run(){ ventaBinding.refrescarMontoVenta(); }
                                        });
                                        
                                    }else{
                                        SION.log(Modulo.VENTA, "Respuesta nula.", Level.WARNING);
                                         mosntrarAlertaRetornaFoco("Ocurrio un problema al consultar los descuentos para los artículos.", 
                                                 TipoMensaje.ERROR, ventaVista.getTablaVentas());
                                    }
                                }
                            }else{
                                SION.log(Modulo.VENTA, "No es una tarjeta de descuento.", Level.WARNING);
                                mosntrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.capital.social.error.noesdescuento"), 
                                        TipoMensaje.ERROR, ventaVista.getTablaVentas());
                            }
                        }else{
                            SION.log(Modulo.VENTA, "Respuesta nula.", Level.WARNING);
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco(
                                                SION.obtenerMensaje(Modulo.VENTA, "ventas.capital.social.error.resppinpadnula"), 
                                                TipoMensaje.ERROR, ventaVista.getTablaVentas());
                                    }
                                });
                        }
                        return "OK";
                    }
                };
            }
        };
        
        servicio.stateProperty().addListener(new ChangeListener(){
                @Override
                public void changed(ObservableValue o, Object o1, Object o2){
                    if( o2.equals(Worker.State.SUCCEEDED) ){
                        stage.close();
                        ventaBinding.removerLoading();
                    }else if( o2.equals(Worker.State.FAILED)){
                        stage.close();
                        ventaBinding.removerLoading();
                        SION.logearExcepcion(Modulo.VENTA, new Exception(servicio.getException()), getStackTrace( servicio.getException()) ) ;
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.capital.social.error.resppinpadnula"), TipoMensaje.ERROR, ventaVista.getCampoMontoTarjeta());
                            }
                        });
                    }
                }
        });
        
        servicio.start();
    }

    public void aplicaPagoTarjeta(final PeticionPagoTarjetaBean _peticion) {

        necesitaNIP = false;
        
        final Task tarea = new Task() {

            
            @Override
            protected Object call() throws Exception {
                if (_peticion.isSeGeneroPeticion()) {
                    RespuestaPagoTarjetaBean respuestaPagoTarjeta = ventaVistaModelo.aplicarPagoTarjeta(_peticion);
                    
                    /*if( respuestaPagoTarjeta != null &&
                        VentaVistaModelo.hayParametroFueraLineaActivo() &&
                        respuestaPagoTarjeta.getDescTarjeta() != null && 
                        respuestaPagoTarjeta.getDescTarjeta().equals("PAGATODO"))
                    {
                        respuestaPagoTarjeta = null;
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco(
                                        SION.obtenerMensaje(Modulo.VENTA, "venta.pagatodo.fueradelinea.nopermitida"), 
                                        TipoMensaje.ERROR, ventaBinding.getCampoTarjeta());
                            }
                        });
                    } else */
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    if (respuestaPagoTarjeta != null){
                        
                        if (respuestaPagoTarjeta.getCodigoError() == 0) {
                            
                            
                            if (respuestaPagoTarjeta.getSolicitudPagaTdo()!=null &&
                                respuestaPagoTarjeta.getSolicitudPagaTdo().getReferencia()!=null &&
                                utilerias.EsTarjetaNip(respuestaPagoTarjeta.getSolicitudPagaTdo().getReferencia()) && 
                                (respuestaPagoTarjeta.getSolicitudPagaTdo().getNip()==null ||
                                 respuestaPagoTarjeta.getSolicitudPagaTdo().getNip().equals(""))){
                                necesitaNIP =true;
                            }
                            
                            OperacionTarjetaBean operacion = new OperacionTarjetaBean();
                            operacion.setPeticionPago(_peticion);
                            operacion.setRespuestaPago(respuestaPagoTarjeta);
                           
                            //insertar registro de forma de pago
                            //SION.log(Modulo.VENTA, "TRACE_>>>>"+respuestaPagoTarjeta.getTrace(), Level.INFO);
                            DatosFormasPagoBean dfp = new DatosFormasPagoBean(
                                    2,//tipopagoid
                                    operacion.getPeticionPago().getMonto(),//monto de la operacion
                                    0,//cantidad de vales
                                    operacion.getRespuestaPago().getNumerotarjeta(),
                                    operacion.getRespuestaPago().getTipoTarjeta(),
                                    //Long.parseLong(operacion.getRespuestaPago().getPeticionReverso().getTrace()),//tarjetaidbus
                                    //respuestaPagoTarjeta.getTrace(),//tarjetaidbus
				    operacion.getRespuestaPago().getPeticionReverso().getPagoTarjetaId(), 
                                    operacion.getRespuestaPago().getPeticionReverso().getComision(),
                                    String.valueOf(operacion.getRespuestaPago().getEstatusLectura()),
                                    respuestaPagoTarjeta.getDescTarjeta().equals("PAGATODO"),
                                    respuestaPagoTarjeta.getDescTarjeta().equals("DESCUENTO")
                            );
                            
                            boolean tarjetaPTRepetida = false;
                            fuePagoExitoso = false;
                            
                            if( dfp.isEsTarjetaPagaTodo() ){
                                int contTarjetasPT = 0;
                                
                                //Verificar que la tarjeta no se este deslizando ds veces
                                for (int i = 0; i < ventaVistaModelo.getListaFormasPago().size(); i++) {
                                    if( ventaVistaModelo.getListaFormasPago().get(i).isEsTarjetaPagaTodo() ){
                                        contTarjetasPT++;
                                        tarjetaPTRepetida |= operacion.getRespuestaPago().getNumerotarjeta().equals(
                                                ventaVistaModelo.getListaFormasPago().get(i).getFcnumerotarjeta());
                                    }
                                }
                                
                                SION.log(Modulo.VENTA, "hay "+contTarjetasPT+" tarjetas pagatodo.", Level.WARNING);
                                if( tarjetaPTRepetida ){
                                    SION.log(Modulo.VENTA, "Tarjeta paga todo repetida", Level.WARNING);
                                    respuestaPagoTarjeta = null;
                                    Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            AdministraVentanas.mostrarAlertaRetornaFocoVenta(
                                                    SION.obtenerMensaje(Modulo.VENTA, "venta.pagatodo.tarjeta.repetida"), 
                                                    TipoMensaje.ERROR, ventaBinding.getCampoTarjeta());
                                        }
                                    });
                                }else if( contTarjetasPT > 0 ){
                                    SION.log(Modulo.VENTA, "Multiples tarjetas Paga Todo", Level.WARNING);
                                    respuestaPagoTarjeta = null;
                                    Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            AdministraVentanas.mostrarAlertaRetornaFocoVenta(
                                                    SION.obtenerMensaje(Modulo.VENTA, "venta.pagatodo.tarjeta.multiple"), 
                                                    TipoMensaje.ERROR, ventaBinding.getCampoTarjeta());
                                        }
                                    });
                                }else{
                                    fuePagoExitoso = true;
                                    dfp.setSolicitudPagaTdo( respuestaPagoTarjeta.getSolicitudPagaTdo());
                                    dfp.setPeticionPagoTarjeta(respuestaPagoTarjeta.getPeticionPagoTarjeta());
                                }
                            }else{
                                fuePagoExitoso = true;
                            }
                            
                            if( fuePagoExitoso )
                            {
                                SION.log(Modulo.VENTA, "Agregando pago exitoso a lista", Level.INFO);
                                //insertar registro de tarjeta
                                ventaVistaModelo.getListaPagosTarjeta().add(operacion);

                                ventaVistaModelo.getListaFormasPago().add(dfp);
                                //fuePagoExitoso = true;

                                //int contador = 0;
                                //for (OperacionTarjetaBean pago : ventaVistaModelo.getListaPagosTarjeta()) {
                                //  System.out.println("lista indice " + contador + ": " + pago.toString());
                                //contador++;
                                //}

                                final OperacionTarjetaBean op = operacion;

                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        actualizarNavegadorTarjetas(op);
                                        ventaVista.getCampoMontoTarjeta().setText("");
                                        ventaVista.getCampoMontoValesE().setText("");
                                        ventaVista.getCampoMontoTarjeta().requestFocus();
                                    }
                                });
                            }
                            
                        } else {
                            final String descError = respuestaPagoTarjeta.getDescError();
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {//respuestaPagoTarjeta.getCodigoError()
                                    /*if(respuestaPagoTarjeta.isSeCargoLlave()){
                                        SION.log(Modulo.VENTA, "Mostrando popup de error en pago con tarjeta", Level.WARNING);
                                        Platform.runLater(new Runnable() {

                                            @Override
                                            public void run() {
                                                AdministraVentanas.mostrarAlertaRetornaFoco(respuestaPagoTarjeta.getDescError(), TipoMensaje.INFO, ventaBinding.getCampoTarjeta());
                                            }
                                        });
                                        
                                    }else{*/
                                        SION.log(Modulo.VENTA, "Mostrando popup de error en pago con tarjeta", Level.WARNING);
                                        
                                        Platform.runLater(new Runnable() {
                                            @Override
                                            public void run() {
                                                //AdministraVentanas.mostrarAlertaRetornaFoco(descError, TipoMensaje.ERROR, ventaBinding.getCampoTarjeta());
                                                AdministraVentanas.mostrarAlertaRetornaFocoVenta(descError, TipoMensaje.ERROR, ventaBinding.getCampoTarjeta());
                                            }
                                        });                                        
                                    //}
                                }
                            });
                        }/**/
                    } else {
                        
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                SION.log(Modulo.VENTA, "Mostrando Popup de error ya que se recibe respuesta nula", Level.WARNING);
                                //AdministraVentanas.mostrarAlertaRetornaFoco("[10] Error de comunicación. Por favor reintente, si persiste el problema contacte a soporte.", TipoMensaje.ERROR, ventaBinding.getCampoTarjeta());
                            	AdministraVentanas.mostrarAlertaRetornaFocoVenta("Por el momento no se encuentra disponible el servicio. Por favor intente mas tarde.", TipoMensaje.ERROR, ventaBinding.getCampoTarjeta());
                            }
                        });
                    }
                } else {
                    
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            SION.log(Modulo.VENTA, "Mostrando Popup de error al no poderse generar peticion de pago", Level.WARNING);
                            AdministraVentanas.mostrarAlertaRetornaFocoVenta("[7]"+_peticion.getMensaje(), TipoMensaje.ERROR, ventaBinding.getCampoTarjeta());
                        }
                    });
                } 

//                try {
//                    Thread.sleep(10000);
//                } catch (InterruptedException ex) {
//                    Logger.getLogger(VentaVistaModelo.class.getName()).log(Level.SEVERE, null, ex);
//                }

                return "ok";
            }
        };

        final Service servicio = new Service() {
            @Override
            protected Task createTask() {
                return tarea;
            }
        };

        servicio.stateProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                if (arg2.equals(Worker.State.SUCCEEDED)) {
                    SION.log(Modulo.VENTA, "Tarea de pago con tarjeta termina correctamente", Level.INFO);
                    
                    
                    
                    if (necesitaNIP){
                        
                        if (fuePagoExitoso) {
                            SION.log(Modulo.VENTA, "La tarjeta necesita NIP", Level.WARNING);
                            muestraVentanaNip();
                        
                        }else{
                            stage.close();
                        ventaBinding.removerLoading();
                        }
                        
                    }else{
                        stage.close();
                        ventaBinding.removerLoading();
                        if (fuePagoExitoso) {
                        marcarVenta();
                        }
                    }
                    
                    
                    
                } else if (arg2.equals(Worker.State.FAILED)) {
                    //servicio.getException().printStackTrace();
                    StringWriter sw = new StringWriter();
                    servicio.getException().printStackTrace(new PrintWriter(sw));
                    String exceptionAsString = sw.toString();
                    SION.log(Modulo.VENTA, "Tarea de pago con tarjeta termina con la siguiente excepcion: : "+exceptionAsString, Level.SEVERE);
                    //servicio.getException().printStackTrace();
                    
                    stage.close();
                    ventaBinding.removerLoading();
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            AdministraVentanas.mostrarAlertaRetornaFocoVenta(SION.obtenerMensaje(Modulo.VENTA, "ventas.errorpagoTarjeta"), TipoMensaje.ERROR, ventaVista.getCampoMontoTarjeta());
                        }
                    });
                    
                }
            }
        });

        servicio.start();
    }

    public void actualizarNavegadorTarjetas(OperacionTarjetaBean _pago) {

        SION.log(Modulo.VENTA, "Tarjeta autorizada, llenando componentes de la vista.", Level.INFO);

        ventaVista.setValorEtiquetaCantidadTarjetasRecibidas(String.valueOf(ventaVistaModelo.getCantidadTarjetasRecibidas(1) + ventaVistaModelo.getCantidadTarjetasRecibidas(2)));
        ventaVista.setValorEtiquetaCantidadValesERecibidos(String.valueOf(ventaVistaModelo.getCantidadTarjetasRecibidas(3)));
        ventaVista.setValorMontoCantidadTarjetasRecibidas(String.valueOf(ventaVistaModelo.getSumaImporteTarjetas(1) + ventaVistaModelo.getSumaImporteTarjetas(2)));
        ventaVista.setValorMontoCantidadValesERecibidos(String.valueOf(ventaVistaModelo.getSumaImporteTarjetas(3)));

        ventaVista.setDescripcionTarjetaBancaria(_pago.getRespuestaPago().getDescTarjeta());
        ventaVista.setDescripcionNoTarjeta(_pago.getRespuestaPago().getNumerotarjeta());
        ventaVista.setDescripcionImporteTarjeta(utilerias.getNumeroFormateado(_pago.getPeticionPago().getMonto()));
        ventaVista.setDescripcionAutorizada(String.valueOf(_pago.getRespuestaPago().getNumeroAutorizacion()));

    }

    public void marcarVenta() {
        if (ventaBinding.getImporteRecibido() >= Double.parseDouble(ventaVista.getMontoVenta())) {

            SION.log(Modulo.VENTA, "El monto de la venta es mayor o igual a lo requerido para marcar la venta,"
                    + " se procede a consultar los parámetros necesarios para su marcación", Level.INFO);

            //1,1 parametros de ACCION_VENTA y AUTORIZACION_ELIMINACION_LINEA
            ventaBinding.consultarParametrosAutorizacion(1, 1);
            //System.out.println("marcando venta-------->>>>>");

        }    
    }

    public Stage getStageCobroTarjeta(PeticionPagoTarjetaBean _peticion) {

        SION.log(Modulo.VENTA, "Mostrando stage de pagos con tarjetas", Level.INFO);

        fuePagoExitoso = false;
        aplicaPagoTarjeta(_peticion);
        lblErrorNip.setVisible(false);
        stage.setScene(escena);
        stage.setResizable(false);
        stage.show();
        stage.centerOnScreen();

        return stage;

    }
    
    public Stage getStageLeerTarjeta(){
        
        SION.log(Modulo.VENTA, "Mostrando stage de lectura de tarjetas", Level.INFO);

        //fuePagoExitoso = false;
        leerTarjetaDescuento();

        stage.setScene(escena);
        stage.setResizable(false);
        stage.show();
        stage.centerOnScreen();

        return stage;
    }
    
    public static String getStackTrace(Throwable aThrowable) {
            Writer result = new StringWriter();
            PrintWriter printWriter = new PrintWriter(result);
            aThrowable.printStackTrace(printWriter);
            return result.toString();
    }
    
    public void muestraVentanaNip(){
        
        pwdNipTarjeta.setText("");
        btnAceptarNip.setDisable(false);
        pwdNipTarjeta.setDisable(false);
        cajaLogo.setVisible(false);
        paneOverlayCajaNip.setVisible(true);
        pwdNipTarjeta.requestFocus();
        
    }
    
    
    private void accionBtnAceptarNip (final Button boton){
        
        boton.setDisable(true);
        pwdNipTarjeta.setDisable(true);
        boton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                SION.log(Modulo.VENTA, "Botón aceptar presionado", Level.OFF);
                procesaPwdNip();
            }
            
        });
        
        pwdNipTarjeta.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.ENTER) {
                    procesaPwdNip();
                }
            }
        });
        
                
    }
    
    
    private void procesaPwdNip(){
        
         if (pwdNipTarjeta.getText().length()==4){
             ventaVistaModelo.obtenerTarjetaPagaTodo().getSolicitudPagaTdo().setNip(pwdNipTarjeta.getText());
             
             lblErrorNip.setVisible(false);
             stage.close();
             ventaBinding.removerLoading();
             
             reversaVistaTarjeta();
             
             
             if (fuePagoExitoso) {
                 pwdNipTarjeta.setText("");
                 marcarVenta();
             }
         }else{
             lblErrorNip.setVisible(true);
             pwdNipTarjeta.setText("");   
             btnAceptarNip.setDisable(false);
             pwdNipTarjeta.setDisable(false);
         }
        
    }
    
    
    private void accionBtnCancelaNip (final Button boton){
        boton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                pwdNipTarjeta.setText("");
                ventaVistaModelo.removerPagoPagaTodo();
                stage.close();
                ventaBinding.removerLoading();
                
                reversaVistaTarjeta();
                
            }
            
        });
                
    }
    
    
    private void reversaVistaTarjeta(){
        cajaLogo.setVisible(true);
        paneOverlayCajaNip.setVisible(false);
    }
    
    

    
}