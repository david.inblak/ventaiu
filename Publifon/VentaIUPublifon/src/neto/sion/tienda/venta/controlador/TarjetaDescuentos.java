package neto.sion.tienda.venta.controlador;

import neto.sion.tienda.bean.DatosTarjeta;
import java.util.HashSet;
import java.util.Set;
import neto.sion.tienda.pt.iso8583.descuentos.ctrl.IValidaDescuentoArticulos;
import neto.sion.tienda.pt.iso8583.descuentos.dto.RespuestaValidaDescuentoArticulosDto;
import neto.sion.tienda.pt.iso8583.descuentos.dto.SolicitudValidaTarjetaDescuentosDto;
import neto.sion.tienda.venta.modelo.ArticuloBean;

public class TarjetaDescuentos {
    private DatosTarjeta datosTarjeta;
    private Set<ArticuloBean> articulosValidados;
    private IValidaDescuentoArticulos validadorArticulos;

    public TarjetaDescuentos(DatosTarjeta _datosTarjeta){
        datosTarjeta = _datosTarjeta;
        articulosValidados = new HashSet<ArticuloBean>();
    }

    public DatosTarjeta getDatosTarjeta() {
        return datosTarjeta;
    }

    public void setDatosTarjeta(DatosTarjeta datosTarjeta) {
        this.datosTarjeta = datosTarjeta;
    }
    
    

    public IValidaDescuentoArticulos getValidadorArticulos() {
        return validadorArticulos;
    }

    public void setValidadorArticulos(IValidaDescuentoArticulos validadorArticulos) {
        this.validadorArticulos = validadorArticulos;
    }
    
    public Set<ArticuloBean> getArticulosValidados() {
        return articulosValidados;
    }

    public void setArticulosValidados(Set<ArticuloBean> articulosValidados) {
        this.articulosValidados = articulosValidados;
    }
  
    public RespuestaValidaDescuentoArticulosDto validarArticulos(SolicitudValidaTarjetaDescuentosDto solicitud) throws Exception{
        return validadorArticulos.validaDescuentoArticulos(solicitud);
    }
}
