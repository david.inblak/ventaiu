/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

/**
 *
 * @author 702382
 */
public class PeticionDetalleArticuloBean {
    
    private String codigoBarras;
    private double cantidad;
    private int ventana;

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public int getVentana() {
        return ventana;
    }

    public void setVentana(int ventana) {
        this.ventana = ventana;
    }

    @Override
    public String toString() {
        return "PeticionArticuloLocalBean{" + "codigoBarras=" + codigoBarras + ", cantidad=" + cantidad + ", ventana=" + ventana + '}';
    }
    
    
    
}
