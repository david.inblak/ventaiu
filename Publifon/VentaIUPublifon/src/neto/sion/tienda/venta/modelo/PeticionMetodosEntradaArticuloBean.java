/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

import java.util.Arrays;

/**
 *
 * @author fvegap
 */
public class PeticionMetodosEntradaArticuloBean {

    private MetodoEntradaArticuloBean[] metodosEntrada;

    public MetodoEntradaArticuloBean[] getMetodosEntrada() {
        return metodosEntrada;
    }

    public void setMetodosEntrada(MetodoEntradaArticuloBean[] metodosEntrada) {
        this.metodosEntrada = metodosEntrada;
    }

    @Override
    public String toString() {
        return "PeticionMetodosEntradaArticuloBean{" + "metodosEntrada=" + Arrays.toString(metodosEntrada) + '}';
    }
}
