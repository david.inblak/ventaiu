/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.bindings;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import neto.sion.tienda.controles.vista.Utilerias;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.modelo.ArticuloBean;
//import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;
import neto.sion.tienda.venta.vista.VentaVista;
import neto.sion.tienda.venta.vista.VerificadorVista;
import neto.sion.tienda.venta.vistamodelo.VentaVistaModelo;

/**
 *
 * @author fvega
 */
public class VerificadorBinding {

    //instancias
    private Utilerias utilerias;
    private VentaVista ventaVista;
    private VentaVistaModelo vistaModelo;
    private ArticuloBean datosArticulo;
    private VentaBindings ventaBindings;
    private VerificadorVista verificadorVista;
    private ValidadorVerificador validador;
    //herramientas
    //private DecimalFormat formatoDouble;
    //constantes
    private String codigoArticulo;
    private final KeyCombination enter;
    private final KeyCombination tab;
    private final KeyCombination f10;
    private final KeyCombination escape;// = new KeyCodeCombination(KeyCode.ESCAPE);
    private ArticuloBean articuloLocal;
    private String cadenabuscador;
    private SimpleBooleanProperty seIntrodujoArticuloPorBuscador;
    private boolean existeParametroValidador;

    public VerificadorBinding(VentaVista ventaVista, VentaVistaModelo vistaModelo,
            VentaBindings ventaBindings, Utilerias _utilerias, VerificadorVista _verificadorVista) {
        this.utilerias = _utilerias;
        this.ventaVista = ventaVista;
        this.vistaModelo = vistaModelo;
        this.ventaBindings = ventaBindings;
        this.datosArticulo = null;
        //this.formatoDouble = new DecimalFormat("#.00");
        this.codigoArticulo = new String();
        this.validador = new ValidadorVerificador();

        this.enter = new KeyCodeCombination(KeyCode.ENTER);
        this.tab = new KeyCodeCombination(KeyCode.TAB);
        this.f10 = new KeyCodeCombination(KeyCode.F10);
        this.escape = new KeyCodeCombination(KeyCode.ESCAPE);

        this.articuloLocal = null;

        this.cadenabuscador = "";
        this.seIntrodujoArticuloPorBuscador = new SimpleBooleanProperty(false);
        this.existeParametroValidador = false;
    }

    public void setExisteParametroValidador(boolean existeParametroValidador) {
        this.existeParametroValidador = existeParametroValidador;
    }
    
    public void setSeIntrodujoArticuloPorBuscador(boolean seIntrodujoArticuloPorBuscador) {
        this.seIntrodujoArticuloPorBuscador.set(seIntrodujoArticuloPorBuscador);
    }

    public SimpleBooleanProperty getSeIntrodujoArticuloPorBuscador() {
        return seIntrodujoArticuloPorBuscador;
    }

    public void operacionesbuscador(final BorderPane panel, final StringProperty propiedadCadena,
            final TextField campoBusqueda, final TextField campoPrecio,
            final Label etiquetaDescripcion, final Stage stage) {

        int cantidad = 1;
        int posicion = 0;

        SION.log(Modulo.VENTA, "Activando buscador de artículos", Level.INFO);

        cadenabuscador = propiedadCadena.getValue();

        capturarArticulo(campoBusqueda, campoPrecio, etiquetaDescripcion, cadenabuscador, true);

    }

    public void eventoEtiquetaCerrar(ImageView vistaImagen, final Stage stage) {

        vistaImagen.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {
                stage.close();
            }
        });

    }

    public void eventoBotonAgregar(final ImageView vistaImagenAgregar, final TextField campoPrecio,
            final Label etiquetaDescripcion, final Stage stage) {

        vistaImagenAgregar.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {

                validarInsercionArticulo(campoPrecio, etiquetaDescripcion, stage);

            }
        });
    }

    public void agregarArticulo(TextField campoPrecio, Label etiquetaDescripcion, Stage stage) {
        if ((vistaModelo.getImporteVenta() + (datosArticulo.getPaprecio())
                * datosArticulo.getPaunidad()) <= ventaBindings.getMontoMaximoPermitido()) {

            if (!"".equals(codigoArticulo)) {
                if (vistaModelo.getListaArticulos().size() > 0) {
                    if (datosArticulo.getPacdgbarras().equals(
                            vistaModelo.getListaArticulos().get(vistaModelo.getListaArticulos().size() - 1).getPacdgbarras())
                            && vistaModelo.getListaArticulos().get(vistaModelo.getListaArticulos().size() - 1).getImporte() > 0) {

                        vistaModelo.getListaArticulos().get(vistaModelo.getListaArticulos().size() - 1).setCantidad(
                                vistaModelo.getListaArticulos().get(vistaModelo.getListaArticulos().size() - 1).getCantidad()
                                + datosArticulo.getCantidad());

                        vistaModelo.getListaArticulos().get(vistaModelo.getListaArticulos().size() - 1).setImporte(
                                vistaModelo.getListaArticulos().get(vistaModelo.getListaArticulos().size() - 1).getImporte()
                                + datosArticulo.getImporte());

                        stage.close();

                    } else {
                        insertarRegistroArticulo(campoPrecio, etiquetaDescripcion, stage);

                    }

                    ventaBindings.refrescarMontoVenta();

                } else {
                    insertarRegistroArticulo(campoPrecio, etiquetaDescripcion, stage);

                }
            }
        } else {

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(
                    Modulo.VENTA, "ventas.montoVenta"), TipoMensaje.ADVERTENCIA, ventaVista.getTablaVentas());
                }
            });
            
        }
    }

    public void validarInsercionArticulo(TextField campoPrecio, Label etiquetaDescripcion, Stage stage) {

        if (existeParametroValidador) {
            if (!seIntrodujoArticuloPorBuscador.get()) {
                agregarArticulo(campoPrecio, etiquetaDescripcion, stage);
            } else {
                SION.log(Modulo.VENTA, "El artículo no se agrego a la venta ya que ha sido insertado por medio de buscador de articulos", Level.INFO);
            }
        }else{
            agregarArticulo(campoPrecio, etiquetaDescripcion, stage);
        }
    }

    public void insertarRegistroArticulo(TextField campoPrecio, Label etiquetaDescripcion, Stage stage) {

        vistaModelo.getListaArticulos().add(datosArticulo);

        stage.close();

        campoPrecio.setText("");
        etiquetaDescripcion.setText("NO HAY INFORMACION");
        codigoArticulo = "";

    }

    public void eventoCampoCodigo(final TextField campoCodigo, final TextField campoPrecio,
            final Label etiquetaDescripcion, final Stage stage) {

        EventHandler<KeyEvent> eventoCampoCodigo = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (enter.match(arg0)) {

                    SION.log(Modulo.VENTA, "Botón enter presionado", Level.INFO);
                    capturarArticulo(campoCodigo, campoPrecio, etiquetaDescripcion, cadenabuscador, false);
                } /*
                 * if(tab.match(arg0)){ Platform.runLater(new Runnable() {
                 *
                 * @Override public void run() { campoCodigo.requestFocus(); }
                 * }); }
                 */ else if (f10.match(arg0)) {

                    validarInsercionArticulo(campoPrecio, etiquetaDescripcion, stage);
                }

            }
        };

        campoCodigo.addEventFilter(KeyEvent.KEY_PRESSED, eventoCampoCodigo);



    }

    public void eventoCampoBuscador(final TextField campoCodigo, final TextField campoPrecio, final Label etiquetaDescripcion, final Stage stage) {

        EventHandler<KeyEvent> eventoCampoCodigo = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (f10.match(arg0)) {
                    validarInsercionArticulo(campoPrecio, etiquetaDescripcion, stage);
                }
            }
        };

        campoCodigo.addEventFilter(KeyEvent.KEY_PRESSED, eventoCampoCodigo);

    }

    public void eventoBotonBuscar(ImageView vistaImagenBuscar, final TextField campoCodigo, final TextField campoPrecio,
            final Label etiquetaDescripcion) {

        vistaImagenBuscar.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {
                SION.log(Modulo.VENTA, "Botón de búsqueda presionado", Level.INFO);

                if (!seIntrodujoArticuloPorBuscador.get()) {
                    capturarArticulo(campoCodigo, campoPrecio, etiquetaDescripcion, cadenabuscador, false);
                }
            }
        });
    }

    public void capturarArticulo(TextField campoCodigo, TextField campoPrecio, Label etiquetaDescripcion,
            String cadenaProperty, boolean esCampoBuscador) {

        articuloLocal = null;
        String cadena = "";

        if (esCampoBuscador) {
            seIntrodujoArticuloPorBuscador.set(true);
            cadena = cadenaProperty;
        } else {
            seIntrodujoArticuloPorBuscador.set(false);
            cadena = campoCodigo.getText();
        }

        if (!"".equals(cadena)) {


            Pattern p = Pattern.compile("^[0-9]+$");
            Matcher m = p.matcher(cadena);
            if (m.find()) {

                vistaModelo.llenarPeticionArticuloLocal(1, cadena.trim());

                if (vistaModelo.consultarArticuloLocal() != null) {///valida que respuesta de store no sea nula
                    articuloLocal = vistaModelo.getArticuloLocal();
                    articuloLocal.setMetodoEntrada(ventaBindings.getMetodoEntradaVerificador());
                }

                if (((articuloLocal.getPaprecio() > 0.0D ? 1 : 0) & (!"".equals(articuloLocal.getPanombrearticulo()) ? 1 : 0)) != 0){
				//if (articuloLocal.getPaprecio() > 0 & !"".equals(articuloLocal.getPanombrearticulo())) {
                    campoPrecio.setText(utilerias.getNumeroFormateado(articuloLocal.getPaprecio()));
                    etiquetaDescripcion.setText(articuloLocal.getPanombrearticulo());
                    codigoArticulo = articuloLocal.getPacdgbarras();
                    datosArticulo = articuloLocal;
                } else {
                    campoPrecio.setText("");
                    etiquetaDescripcion.setText("NO HAY INFORMACION");
                }
            } else {
                campoPrecio.setText("");
                etiquetaDescripcion.setText("NO HAY INFORMACION");
                
                final String msj = cadena;
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlerta(SION.obtenerMensaje(Modulo.VENTA, "ventas.noArticulo").replace("%1", msj), TipoMensaje.ERROR);
                    }
                });
                
            }

            campoCodigo.setText("");

        }
    }

    public void validarCampo(TextField campo) {

        validador.formatearCampoTexto(campo);

    }

    public void eventoESC(final BorderPane panel, final Stage stage) {

        panel.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (escape.match(arg0)) {
                    stage.close();
                }
            }
        });

    }

    public void efectoHoverEtiquetas(final ImageView vistaImagen, final Image imagen, final Image imagenHover) {

        vistaImagen.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {

                if (!arg1 && arg2) {
                    vistaImagen.setImage(imagenHover);
                } else if (arg1 && !arg2) {
                    vistaImagen.setImage(imagen);
                }
            }
        });

    }

    public void eventosCampoPrecio(TextField campoPrecio, final TextField campoCodigo, final Stage stage,
            final Label etiquetaDescripcion) {

        campoPrecio.focusedProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {

                if (!arg1 && arg2) {
                    campoCodigo.requestFocus();
                }
            }
        });

        campoPrecio.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                validarInsercionArticulo(campoCodigo, etiquetaDescripcion, stage);
            }
        });

    }
}
