/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.bindings;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author 702382
 */
public class ValidadorTicket {
    
    private String ultimaCadenaValida;
    private int ultimoTamanioValido;
    
    private String minuscula1;
    private String minuscula2;
    
    Pattern patron;
    Matcher m;
    
    final TextField campoTexto;

    public ValidadorTicket() {
        this.ultimaCadenaValida = "";
        this.ultimoTamanioValido = 0;
        this.minuscula1 = "";
        this.minuscula2 = "";
        this.patron = Pattern.compile("^[0-9]{1,18}(ioIO{0,1})?$");
        this.m = null;
        
        this.campoTexto = new TextField();
    }
    
    public TextField getTextFieldFormatoTicket(final String caracterMayuscula1, final String caracterMayuscula2,
            double ancho, double alto, String texto){
        
        minuscula1 = caracterMayuscula1.toLowerCase();
        minuscula2 = caracterMayuscula2.toLowerCase();
        
        campoTexto.setMaxSize(ancho, alto);
        campoTexto.setMinSize(ancho, alto);
        campoTexto.setPrefSize(ancho, alto);
        
        campoTexto.setPromptText(texto);
        
                
        /*campoTexto.caretPositionProperty().addListener(new ChangeListener<Object>() {

            @Override
            public void changed(ObservableValue<? extends Object> arg0, Object arg1, Object arg2) {
                if(arg2.equals(0)&&campoTexto.getText().length()>0){
                    campoTexto.selectPositionCaret(campoTexto.getText().length());
                }
            }
        });*/
        
        campoTexto.addEventFilter(javafx.scene.input.KeyEvent.KEY_TYPED , new EventHandler<javafx.scene.input.KeyEvent>() {

            @Override
            public void handle(javafx.scene.input.KeyEvent arg0) {
                char ar[] = arg0.getCharacter().toCharArray();
                char ch = ar[arg0.getCharacter().toCharArray().length - 1];
                
                if((ch >= '0' && ch <= '9')){///valida que sean numeros
                    
                    if((campoTexto.getText().endsWith(minuscula2)) || campoTexto.getText().endsWith(minuscula1) 
                            || campoTexto.getText().endsWith(caracterMayuscula1) || campoTexto.getText().endsWith(caracterMayuscula2)){// cadena no debe terminar en "I" u "O"
                        arg0.consume();
                    }
                                        
                }else{///si no son numeros
                    
                    if(caracterMayuscula2.equals(arg0.getCharacter()) || caracterMayuscula1.equals(arg0.getCharacter()) || 
                            minuscula2.equals(arg0.getCharacter()) || minuscula1.equals(arg0.getCharacter())){  ///valida que sea "I" u "O"
                       
                        if((campoTexto.getText().endsWith(minuscula2)) || campoTexto.getText().endsWith(minuscula1) 
                                || campoTexto.getText().endsWith(caracterMayuscula1) || campoTexto.getText().endsWith(caracterMayuscula2)){  //cadena no debe terminar en "I" u "O"
                            arg0.consume();
                        }else{  //si son minusculas cambia a myusculas
                            if(arg0.getCharacter().equals(minuscula1)){
                                arg0.consume();
                                campoTexto.setText(campoTexto.getText().concat(caracterMayuscula1));
                            }else if(arg0.getCharacter().equals(minuscula2)){
                                arg0.consume();
                                campoTexto.setText(campoTexto.getText().concat(caracterMayuscula2));
                            }
                        }
                        
                    }else{  ///si es cualquier otro caracter mata evento, backspace borra todo el campo, si pasa se setea ultima cadena valida
                        
                        arg0.consume();
                        campoTexto.selectEnd();
                        if(campoTexto.getText().length()==0 && ultimoTamanioValido>1){
                            if(ultimaCadenaValida.length()>0){
                                
                                campoTexto.setText(ultimaCadenaValida.substring(0, ultimaCadenaValida.length()-1));
                                
                            }else{
                                
                                campoTexto.setText("");
                            }
                        }
                        
                    }
                }
                ultimaCadenaValida=campoTexto.getText();
                ultimoTamanioValido=campoTexto.getText().length();
                
                campoTexto.selectEnd();
                campoTexto.deselect();
            }
        });
        
        
        return campoTexto;
    }
    
    public void limpiarValidador(){
        
        ultimaCadenaValida="";
        
    }
    
}
