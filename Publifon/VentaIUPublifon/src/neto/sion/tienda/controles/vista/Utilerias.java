/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package neto.sion.tienda.controles.vista;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;

public final class Utilerias
{
  private Dimension d;
  private InputStream inputStream;
  private String cadena;
  private NumberFormat nf;
  private DecimalFormat df;
  private Locale lugar = new Locale("es", "MX");
  private String patron2digitos;
  private String patron3digitos;
  private HBox cajaBotonesPopup;
  private Button botonAceptar;
  private Button botonCancelar;
  private Image imagenAceptar;
  private Image imagenCancelar;
  private Image imagenAceptarHover;
  private Image imagenCancelarHover;
  private ImageView vistaImagenAceptar;
  private ImageView vistaImagenCancelar;
  private SimpleIntegerProperty popupProperty;
  private final int ACEPTAR = 1;
  private final int CANCELAR = 2;
  
  public Utilerias()
  {
    d = Toolkit.getDefaultToolkit().getScreenSize();
    inputStream = null;
    cadena = "";
    nf = null;
    df = null;
    patron2digitos = "#.00";
    patron3digitos = "#.000";
    

        //popup
        this.imagenAceptar = getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_Aceptar.png");
        this.imagenCancelar = getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_Cancelar.png");
        this.imagenAceptarHover = getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_Aceptar_2.png");
        this.imagenCancelarHover = getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_Cancelar_2.png");
        this.vistaImagenAceptar = new ImageView(imagenAceptar);
        this.vistaImagenCancelar = new ImageView(imagenCancelar);
        this.botonAceptar = new Button("", vistaImagenAceptar);
        this.botonCancelar = new Button("", vistaImagenCancelar);
        this.cajaBotonesPopup = getHBox(10, 300, 100);
        this.popupProperty = new SimpleIntegerProperty(0);

        inicializaPopup();
    }

    public int getAnchoMonitor() {
        return d.width;
    }

    public int getAlturaMonitor() {
        return d.height;
    }

    public Image getImagen(String ruta) {

        inputStream = getClass().getResourceAsStream(ruta);
        Image imagen = new Image(inputStream);
        try {
            inputStream.close();
        } catch (IOException ex) {
            SION.log(Modulo.VENTA, "ocurrió un error al crear imagen " + ruta, Level.SEVERE);
        }

        return imagen;

    }

    public DropShadow getDropShadow(double xcoord, double ycoord, Color color) {
        DropShadow sombra = new DropShadow();
        sombra.setOffsetY(ycoord);
        sombra.setOffsetX(xcoord);
        sombra.setColor(color);

        return sombra;
    }

    public HBox getHBox(double espacio, double ancho, double altura) {
        HBox cajaHorizontal = new HBox();
        cajaHorizontal.setSpacing(espacio);
        cajaHorizontal.setMaxSize(ancho, altura);
        cajaHorizontal.setMinSize(ancho, altura);
        cajaHorizontal.setPrefSize(ancho, altura);
        return cajaHorizontal;
    }

    public VBox getVBox(double espacio, double ancho, double altura) {
        VBox cajaVertical = new VBox();
        cajaVertical.setSpacing(espacio);
        cajaVertical.setMaxSize(ancho, altura);
        cajaVertical.setMinSize(ancho, altura);
        cajaVertical.setPrefSize(ancho, altura);
        return cajaVertical;
    }

    public Label getLabelConImageView(String UrlImagen, double alturaimagen, double anchoImagen, Pos posicion) {

        Image imagen = getImagen(UrlImagen);
        ImageView vistaImagen = new ImageView(imagen);
        vistaImagen.setFitHeight((alturaimagen));
        vistaImagen.setFitWidth((anchoImagen));
        Label etiqueta = new Label("", vistaImagen);
        etiqueta.setAlignment(posicion);
        etiqueta.setMaxSize((anchoImagen), (alturaimagen));
        etiqueta.setMinSize((anchoImagen), (alturaimagen));
        etiqueta.setPrefSize((anchoImagen), (alturaimagen));


        return etiqueta;
    }

    public Label getLabel(String texto, double ancho, double altura) {
        final Label etiqueta = new Label(texto);
        etiqueta.setPrefSize(ancho, altura);
        etiqueta.setMaxSize(ancho, altura);
        etiqueta.setMinSize(ancho, altura);

        return etiqueta;
    }

    public TextField getTextField(String texto, double ancho, double altura) {
        final TextField campoTexto = new TextField();
        campoTexto.setMinSize(ancho, altura);
        campoTexto.setMaxSize(ancho, altura);
        campoTexto.setPrefSize(ancho, altura);
        campoTexto.setPromptText(texto);

        return campoTexto;
    }
    
   public PasswordField getPwdField(String texto, double ancho, double altura) {
        final PasswordField campoPwd = new PasswordField();
        campoPwd.setMinSize(ancho, altura);
        campoPwd.setMaxSize(ancho, altura);
        campoPwd.setPrefSize(ancho, altura);
        campoPwd.setPromptText(texto);

        return campoPwd;
    }
    
    public Separator getSeparador(double ancho, double altura){
        
        Separator separador = new Separator();
        separador.setPrefWidth(ancho);
        separador.setPrefHeight(altura);
        return separador;
        
    }

    public TableColumn getColumna(String nombre, double ancho) {
        final TableColumn columna = new TableColumn(nombre);
        columna.setPrefWidth((int) ancho);
        columna.setMaxWidth((int) ancho);
        columna.setMinWidth((int) ancho);
        columna.setEditable(false);
        columna.setResizable(false);
        columna.setSortable(false);
        columna.resizableProperty().set(false);

        return columna;
    }

    public Label getLabelConEstilos(String estilo, double ancho, double altura,
            Pos posicion, double xcoord, double ycoord) {
        final Label etiqueta = new Label();
        etiqueta.getStyleClass().add(estilo);
        etiqueta.setMinSize((ancho), (altura));
        etiqueta.setMaxSize((ancho), (altura));
        etiqueta.setPrefSize((ancho), (altura));
        etiqueta.setText("0.00");
        etiqueta.setAlignment(posicion);
        etiqueta.setLayoutX(xcoord);
        etiqueta.setLayoutY((ycoord));

        return etiqueta;
    }

    public Label creaEtiquetaConImagenEstilo(String UrlImagen, String estilo, String texto,
            double anchoImagen, double alturaImagen, double anchoEtiqueta, double alturaEtiqueta, Pos posicion) {
        Image imagen = new Image(getClass().getResourceAsStream(UrlImagen));
        final ImageView vistaImagen = new ImageView(imagen);
        vistaImagen.getStyleClass().add(estilo);
        vistaImagen.setFitWidth(anchoImagen);
        vistaImagen.setFitHeight(alturaImagen);

        Label etiqueta = new Label(texto, vistaImagen);
        etiqueta.setAlignment(posicion);
        etiqueta.setMaxSize((anchoEtiqueta), (alturaEtiqueta));
        etiqueta.setTextAlignment(TextAlignment.RIGHT);
        etiqueta.setOpacity(2);
        etiqueta.getStyleClass().add(estilo);

        return etiqueta;
    }

    public String getNumeroFormateado(double valor) {

        cadena = "";

        try {
            nf = NumberFormat.getNumberInstance(lugar);
            df = (DecimalFormat) nf;
            df.applyPattern(patron2digitos);
            cadena = df.format(valor);

        } catch (NumberFormatException e) {
            SION.logearExcepcion(Modulo.VENTA, e);
        }

        return cadena;
    }

    public double RedondearImporte(double _cantidad, double _precio, double _descuento) {
        double importe = 0;
        importe = Double.parseDouble(getNumero3Digitos(_cantidad * (_precio-_descuento)));
        String cadenaImporte = String.valueOf(importe);
        int indicePunto = 0;
        indicePunto = cadenaImporte.indexOf(".");
        String cadenaEnteros = cadenaImporte.substring(0, indicePunto);
        String cadenaDecimales = cadenaImporte.substring(indicePunto + 1);

        if (cadenaDecimales.length() > 2) {
            if (cadenaDecimales.charAt(2) == '5'
                    || cadenaDecimales.charAt(2) == '6'
                    || cadenaDecimales.charAt(2) == '7'
                    || cadenaDecimales.charAt(2) == '8'
                    || cadenaDecimales.charAt(2) == '9') {

                int digitoSegundaPosicion = Integer.parseInt(String.valueOf(cadenaDecimales.charAt(1)));
                if (digitoSegundaPosicion == 9) {
                    int digitoPrimerPosicion = Integer.parseInt(String.valueOf(cadenaDecimales.charAt(0)));
                    if (digitoPrimerPosicion == 9) {
                        importe = Double.parseDouble(cadenaEnteros);
                        importe++;
                    } else {
                        digitoPrimerPosicion++;
                        cadenaDecimales = cadenaEnteros.concat(".").concat(String.valueOf(digitoPrimerPosicion).concat("0"));
                        importe = Double.parseDouble(cadenaDecimales);
                    }
                } else {
                    digitoSegundaPosicion++;
                    cadenaDecimales = cadenaEnteros.concat(".").concat(String.valueOf(cadenaDecimales.charAt(0)))
                            .concat(String.valueOf(digitoSegundaPosicion));
                    importe = Double.parseDouble(cadenaDecimales);
                }
            }
        }

        return importe;
    }

    public String getNumero3Digitos(double valor) {
        cadena = "";
        try {
            nf = NumberFormat.getNumberInstance(lugar);
            df = (DecimalFormat) nf;
            df.applyPattern(patron3digitos);
            cadena = df.format(valor);

        } catch (NumberFormatException e) {
            SION.logearExcepcion(Modulo.VENTA, e);
        }
        return cadena;
    }

    ////popup aceptar/cancelar
    private void inicializaPopup() {
        vistaImagenAceptar.setFitHeight(50);
        vistaImagenAceptar.setFitWidth(145);
        vistaImagenCancelar.setFitHeight(50);
        vistaImagenCancelar.setFitWidth(145);
        cajaBotonesPopup.getChildren().addAll(botonAceptar, botonCancelar);
        cajaBotonesPopup.setStyle("-fx-alignment: center;");
        popupProperty.set(0);

        setImagenHover(vistaImagenAceptar, imagenAceptar, imagenAceptarHover);
        setImagenHover(vistaImagenCancelar, imagenCancelar, imagenCancelarHover);

        botonAceptar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                popupProperty.set(ACEPTAR);
                AdministraVentanas.cerrarAlerta();
            }
        });

        botonCancelar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                popupProperty.set(CANCELAR);
                AdministraVentanas.cerrarAlerta();
            }
        });

    }

    public SimpleIntegerProperty getEstatusPopup() {
        return popupProperty;
    }

    public void mostrarPopupPregunta(String _mensaje, TipoMensaje _tipoMensaje) {
        popupProperty.set(0);
        AdministraVentanas.mostrarAlerta(_mensaje, _tipoMensaje, cajaBotonesPopup);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                botonCancelar.requestFocus();
            }
        });
    }

    private void setImagenHover(final ImageView _vistaImagen, final Image _imagen, final Image _imagenHover) {
        _vistaImagen.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    _vistaImagen.setCursor(Cursor.HAND);
                    _vistaImagen.setImage(_imagenHover);
                } else if (arg1 && !arg2) {
                    _vistaImagen.setCursor(Cursor.NONE);
                    _vistaImagen.setImage(_imagen);
                }
            }
        });
    }

    public void enviarAlarma() {

        SION.log(Modulo.VENTA, "inicia proceso de envío de alarma de seguridad", Level.INFO);
        String URL = "";

        try {
            URL = SION.obtenerParametro(Modulo.VENTA, "neto.sion.alarma.seguridad.url");
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, "No se encontro el parametro de alarma de seguridad");
        }

        if (URL != null) {
            if (!URL.isEmpty()) {
                try {
                    //Process p = Runtime.getRuntime().exec("C:\\Program Files\\Internet Explorer\\iexplore.exe \"http://201.144.220.174:80/alarma/evento/prueba/4517/769e3c0c67d19985fc7dfab6951025d8d47d82ea\"");
                    Process p = Runtime.getRuntime().exec("\"C:\\Program Files\\Internet Explorer\\iexplore.exe\" \""+URL+"\"");
                    Thread.sleep(300);
                    p.destroy();
                    SION.log(Modulo.VENTA, "Alarma enviada correctamente", Level.INFO);
                } catch (Exception e) {
                    SION.logearExcepcion(Modulo.VENTA, e, "Ocurrió un error al intentar enviar alarma de seguridad: "+e.getLocalizedMessage());
                }
            }
        }



        /*
         * SION.log(Modulo.VENTA, "inicia proceso de envío de alarma de
         * seguridad", Level.INFO);
         *
         * try {
         *
         * URL url = new
         * URL("http://201.144.220.174:80/alarma/evento/prueba/4517/769e3c0c67d19985fc7dfab6951025d8d47d82ea");
         * URLConnection urlC = null; try { urlC = url.openConnection();
         * SION.log(Modulo.VENTA, "Conexión encontrada", Level.INFO); } catch
         * (IOException ex) { SION.logearExcepcion(Modulo.VENTA, ex, "Ha
         * ocurrido un error al intentar abir conexión con URL Alarma"); } try {
         * urlC.connect(); SION.log(Modulo.VENTA, "Conexion establecida",
         * Level.INFO); } catch (IOException ex) {
         * SION.logearExcepcion(Modulo.VENTA, ex, "Ha iocurrido un error al
         * conectar con URL Alarma"); }
         *
         * } catch (MalformedURLException ex) {
         * SION.logearExcepcion(Modulo.VENTA, ex, "Ocurrió un error en proceso
         * de envio de alrama de seguridad"); }
         */
    }
    
    public Button getButton(String texto, double ancho, double altura) {
        
        Button btn = new Button(texto);
        btn.setMaxSize(ancho, altura);
        btn.setMinSize(ancho, altura);
        btn.setPrefSize(ancho, altura);
        
        return btn;
    }
    
    public boolean EsTarjetaNip(String tarjeta){
        boolean esTarjetaNip = false;
        
        String cardsNip = SION.obtenerParametro(Modulo.VENTA, "VENTA.PAGATODO.TARJETAS.NIP");
        String[] arrayCardsNip = cardsNip.split(",");
        String card8Dig = tarjeta.substring(0,8);
        
        
        if (arrayCardsNip.length>0){
            for (String cardNip : arrayCardsNip) {
                if (card8Dig.equals(cardNip)){
                    esTarjetaNip = true;
                    break;
                }
            }
        }        
        return esTarjetaNip;
    }
    
}
