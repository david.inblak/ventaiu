/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.binding;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import neto.sion.tienda.caja.principal.VerificaVentana;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.dao.PagoServiciosDao;
import neto.sion.tienda.pago.servicios.vista.PagoServiciosVista;
import neto.sion.tienda.pago.servicios.vistamodelo.AntadVistaModelo;
import neto.sion.tienda.pago.servicios.vistamodelo.PagoServiciosVistaModelo;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;

/**
 *
 * @author fvegap
 */
public class AntadBinding {
    
    private PagoServiciosVista vista;
    private PagoServiciosBinding binding;
    private AntadVistaModelo antadVistaModelo;
    private VerificaVentana verificaVentana;
    private Image aceptarBloqueos;
    private ImageView vistaImagenAceptarBloqueos;
    private Button botonBloqueos;



    /**
     * Clase constructor
     * @param _binding 
     */
    public AntadBinding(PagoServiciosVista _vista, PagoServiciosBinding _binding, PagoServiciosVistaModelo _vistModelo, 
            UtileriasVenta _utilerias, VerificaVentana _verificaVentana) {
        this.vista = _vista;
        this.binding = _binding;
        this.antadVistaModelo = new AntadVistaModelo(_vistModelo, _utilerias,this);
        this.verificaVentana = _verificaVentana;
        
        this.aceptarBloqueos = new Image(getClass().getResourceAsStream("/neto/sion/tienda/pago/servicios/vista/btn_Aceptar.png"));
        this.vistaImagenAceptarBloqueos = new ImageView(aceptarBloqueos);
        this.botonBloqueos = new Button("", vistaImagenAceptarBloqueos);
        
        creaListenerBotonBloqueos();
    }
    
    //bloqueos
    
    public void creaListenerBotonBloqueos() {

        botonBloqueos.setMaxSize(vistaImagenAceptarBloqueos.getFitWidth(), vistaImagenAceptarBloqueos.getFitHeight());
        botonBloqueos.setMinSize(vistaImagenAceptarBloqueos.getFitWidth(), vistaImagenAceptarBloqueos.getFitHeight());
        botonBloqueos.setPrefSize(vistaImagenAceptarBloqueos.getFitWidth(), vistaImagenAceptarBloqueos.getFitHeight());

        botonBloqueos.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        evaluarBloqueosVenta();
                    }
                });
            }
        });

    }
    
    
    ///validaciones
    
    public boolean existePagoAntad(){
        return antadVistaModelo.esEmisorAntad();
    }
    
    
    public boolean existePagoPagatodo(){
        return antadVistaModelo.existePagoPagatodo();
    }
    
    public boolean esEmisorAntad (long _emisor){
        return antadVistaModelo.esEmisorAntad(_emisor);
    }
    
    /**
     * Se encarga de agregar una referencia capturada y validada en switch a lista con todos los pagos acumulados en la operacion
     * @param _index
     * @param _tipoPago
     * @param _importe
     * @param _referencia 
     */
    public void agregaReferenciaAntad(final int _index, final int _tipoPago, final double _importe, final String _referencia){
        
        antadVistaModelo.cargarReferenciaAntad(_index, _tipoPago, _importe, _referencia);
        
    }
    
    /**
     * 
     * @param _emisor
     * @param _importe
     * @param _referencia 
     */
    public void consultarReferencia(final long _emisor, final double _importe, final String _referencia){
                
        vista.cargarLoading();
        
        final Task tarea = new Task() {

            @Override
            protected Object call() throws Exception {
                
              
                SION.log(Modulo.PAGO_SERVICIOS, "Comienza tarea de envío de petición a método ConsultaSKU", Level.INFO);
                antadVistaModelo.consultaPagoAntad(false, _emisor, _referencia, _importe);
                
                return "ok";
            }
        };
        
        final Service servicio = new Service() {

            @Override
            protected Task createTask() {
                return tarea;
            }
        };
        
        servicio.stateProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                if (arg2.equals(Worker.State.SUCCEEDED)) {
                    boolean esConsultaValida = (antadVistaModelo.getRespuestaConsultaAntad().getCodigoError()==0)?true:false;
                    
                    /**
                     * Errores por validar:
                     * -1   No se genero conciliacion
                     * -2   Excepcion en comunicacion - se muestra mensaje generico
                     */
                    
                    vista.removerImagenLoading();
                    SION.log(Modulo.PAGO_SERVICIOS, "Tarea de consulta de pago termina correctamente, la consulta ha sido validada: " + esConsultaValida, Level.INFO);
                    
                    //muestra popup de error en consulta
                    if(!esConsultaValida){
                        
                        
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                
                            AdministraVentanas.mostrarAlertaRetornaFoco(antadVistaModelo.getRespuestaConsultaAntad().getDescError(), TipoMensaje.ERROR, vista.getTablaPagosAcumulados());
                            limpiarCamposReferencia();
                            antadVistaModelo.limpiar();
                            antadVistaModelo.limpiarAntad();
                                
                            //limpia campos y forza a regresar el foco a la tabla
                            vista.getCampoEmisor().setText("");
                            vista.getCampoReferencia().setText("");
                            vista.getCampoPago().setText("0.00");
                            vista.getTablaPagosAcumulados().requestFocus();
                            }
                            
                        });
                        
                    }else{
                        vista.getCajaOverlay().setVisible(true);
                    }
                    
                    Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                            //forza a regresar el foco a la tabla
                            vista.getTablaPagosAcumulados().requestFocus();
                            }
                            
                     });
                    
                    
                    
                }else if (arg2.equals(Worker.State.FAILED)) {
                    vista.removerImagenLoading();
                    SION.log(Modulo.PAGO_SERVICIOS, "Tarea de consulta de pago termina con la siguiente excepción: "+servicio.getException().getLocalizedMessage(), Level.INFO);
                    
                    StringWriter sw = new StringWriter();
                    servicio.getException().printStackTrace(new PrintWriter(sw));
                    String exceptionAsString = sw.toString();
                    SION.log(Modulo.PAGO_SERVICIOS, "Exception - Cliente AntadTda Método Consulta : "+exceptionAsString, Level.SEVERE);
                    servicio.getException().printStackTrace();
                    
                     Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            vista.getTablaPagosAcumulados().requestFocus();
                        }
                    });
                }
            }
        });
        
        servicio.start();
        
    }
    
    /**
     * 
     * @param _montoRecibido 
     */
    public void aplicarAutorizacion(final double _montoRecibido, final String _referencia){
        
        vista.cargarLoading();
        
        final Task tarea = new Task() {

            @Override
            protected Object call() throws Exception {
                
              
                SION.log(Modulo.PAGO_SERVICIOS, "Comienza tarea de envío de petición a método Autorizacion Antad", Level.INFO);
                
                antadVistaModelo.autorizaPagoAntad(_montoRecibido, _referencia);
                vista.getCajaOverlay().setVisible(false);
                return "ok";
            }
        };
        
        final Service servicio = new Service() {

            @Override
            protected Task createTask() {
                return tarea;
            }
        };
        
        servicio.stateProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                if (arg2.equals(Worker.State.SUCCEEDED)) {
                    //try
                    boolean seRecibioRespuestaAutorizacionExitosa = false;
                    
                    //termina la tarea y se verifica si la autorizacion fue exitosa
                    
                    if(antadVistaModelo.getRespuestaAutorizacionAntad().getCodigoError()==0){
                        
                        //caso exitoso
                        SION.log(Modulo.PAGO_SERVICIOS, "Tarea de autorización de pago termina correctamente, el pago ha sido aplicado en linea exitosamente", Level.INFO);
                        seRecibioRespuestaAutorizacionExitosa = true;
                    
                    }else if(antadVistaModelo.getRespuestaAutorizacionAntad().getCodigoError()==-3){
                        
                        //pago exitoso en antad, no se pudo marcar en central y sera enviado mediante demonio ventas fl
                        if(antadVistaModelo.getRespuestaPagoLocal()!=null){
                            if(antadVistaModelo.getRespuestaPagoLocal().getCodigoError()==0){
                                SION.log(Modulo.PAGO_SERVICIOS, "Tarea de autorización de pago termina correctamente, el pago ha sido aplicado fuera de linea exitosamente", Level.INFO);
                                seRecibioRespuestaAutorizacionExitosa = true;
                            }else{
                                SION.log(Modulo.PAGO_SERVICIOS, "Tarea de autorización de pago termina correctamente, se recibe codigo de error en bd local", Level.WARNING);
                            }
                        }else{
                            SION.log(Modulo.PAGO_SERVICIOS, "Tarea de autorización de pago termina correctamente, no pudo ser aplicado el pago fuera de lineas", Level.INFO);
                        }
                    
                    }
                       
                    //quitar el loading
                    vista.removerImagenLoading();
                    
                    if(seRecibioRespuestaAutorizacionExitosa){
                        //si la respuesta es exitosa se verifica el estado de la impresion para mostrar popup de error si procede
                        validaEstadoImpresion();
                    }else{
                        SION.log(Modulo.PAGO_SERVICIOS, "Validando codigo de error recibido", Level.INFO);
                        
                        //si se obtuvo un codigo de error se muestra popup con error ya sea haya sido originado en central o bd local
                        
                        if(antadVistaModelo.getRespuestaAutorizacionAntad().getCodigoError() == -7){
                            
                            //rechazo encontrado en la consulta
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: Time out al realizar el pago, se encuentra pago no exitoso", Level.INFO);
                            
                            Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco(antadVistaModelo.getRespuestaAutorizacionAntad().getDescError(), 
                                        TipoMensaje.ERROR, vista.getCampoReferencia());
                                    }
                                });
                            
                        }else if(antadVistaModelo.getRespuestaAutorizacionAntad().getCodigoError() == -6){
                            
                            //rechazo encontrado en la consulta
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: Time out al realizar el pago, se encuentra pago no exitoso", Level.INFO);
                            
                            Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.error.todos"), 
                                        TipoMensaje.ERROR, vista.getCampoReferencia());
                                    }
                                });
                            
                        }else if(antadVistaModelo.getRespuestaAutorizacionAntad().getCodigoError() == -5){
                            
                            //no se encontro pago en la consukta
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: Time out al realizar el pago, no se encontro en BD Central al realizar la consulta", Level.INFO);
                            
                            Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.error.todos"), 
                                        TipoMensaje.ERROR, vista.getCampoReferencia());
                                    }
                                });
                            
                        }else if(antadVistaModelo.getRespuestaAutorizacionAntad().getCodigoError() == -3){
                            
                            //rechazo encontrado en la consulta
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: Time out al realizar el pago,rechazo en la consulta de pago", Level.INFO);
                            
                            Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco(antadVistaModelo.getRespuestaConsultaBDCentral().getMensaejAntad(), 
                                        TipoMensaje.ERROR, vista.getCampoReferencia());
                                    }
                                });
                            
                        }else if(antadVistaModelo.getRespuestaAutorizacionAntad().getCodigoError() == -2){
                            
                            //error al marcar en bd local
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: BD Local", Level.INFO);
                            if(antadVistaModelo.getRespuestaAutorizacionAntad().getDescError().contains("|")){   
                                //error originado en  base de datos local
                                final String[] msj = antadVistaModelo.getRespuestaPagoLocal().getDescError().substring(1).split("\\|");
                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco(msj[0].concat(" Favor de llamar a Soporte Técnico."), TipoMensaje.ERROR, vista.getCampoReferencia());
                                    }
                                });
                                

                            }else{
                                //excepcion al conectar con bd local
                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco("Ocurrió un error al intentar aplicar el pago. Por favor contacte a Soporte Técnico.", 
                                        TipoMensaje.ERROR, vista.getCampoReferencia());
                                    }
                                });
                                
                            }
                        
                        }else if(antadVistaModelo.getRespuestaAutorizacionAntad().getCodigoError() == -1){
                        
                            //la venta fl se realizo ok, error al conectar con plataforma antad
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: WS NETO", Level.INFO);
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.error.todos"),
                                    TipoMensaje.ERROR, vista.getCampoReferencia());
                                }
                            });
                            
                            
                            
                        }else{
                            
                            //error de antad
                            if(antadVistaModelo.getRespuestaAutorizacionAntad().getRespuestaAutorizacionAntad()!=null){
                                //muestra mensaje de error obtenido en repuesta de autorizacion
                                SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: WS Antad", Level.INFO);
                                String mensaje = "";
                                if(antadVistaModelo.getRespuestaAutorizacionAntad().getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeTicket()!=null){
                                    mensaje = mensaje.concat(antadVistaModelo.getRespuestaAutorizacionAntad().getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeTicket()).concat("\n ");
                                }

                                if(antadVistaModelo.getRespuestaAutorizacionAntad().getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeCajero()!=null){
                                    mensaje = mensaje.concat(antadVistaModelo.getRespuestaAutorizacionAntad().getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeCajero());
                                }

                                ///valida que exista mensaje de antad, sino muestra msj especifico para tal caso
                                if(mensaje.isEmpty()){
                                    mensaje = "Ocurrió un error al recibir respuesta de pago. Por favor contacte a soporte técnico.";
                                }

                                final String msj = mensaje;
                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco(msj,TipoMensaje.ERROR, vista.getCampoReferencia());
                                    }
                                });
                                

                            }else{
                                
                                //la venta fl se realizo ok, error al conectar con plataforma antad
                                SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: WS NETO", Level.INFO);
                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco("Ocurrió un error al registrar su pago. Por favor intente más tarde.",
                                        TipoMensaje.ERROR, vista.getCampoReferencia());
                                    }
                                });
                                

                            }
                        }

                    }
                    
                    //se regresa focus a tabla 
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            antadVistaModelo.limpiarAntad();
                            antadVistaModelo.limpiar();
                            binding.limpiar();
                            binding.limpiarVista();
                            binding.regresarFoco();
                        }
                    });
                    /*}catch(Exception e){
                        StringWriter sw = new StringWriter();
                        e.printStackTrace(new PrintWriter(sw));
                        String exceptionAsString = sw.toString();
                        SION.log(Modulo.PAGO_SERVICIOS, "Exception - Cliente AntadTda Método Autorización : "+exceptionAsString, Level.SEVERE);
                    }*/
                    
                }else if (arg2.equals(Worker.State.FAILED)) {
                    vista.removerImagenLoading();
                    SION.log(Modulo.PAGO_SERVICIOS, "Tarea de autorización de pago termina con la siguiente excepción: "+servicio.getException().getLocalizedMessage(), Level.INFO);
                    
                    StringWriter sw = new StringWriter();
                    servicio.getException().printStackTrace(new PrintWriter(sw));
                    String exceptionAsString = sw.toString();
                    SION.log(Modulo.PAGO_SERVICIOS, "Exception - Cliente AntadTda Método Autorización : "+exceptionAsString, Level.SEVERE);
                    
                     Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            antadVistaModelo.limpiarAntad();
                            antadVistaModelo.limpiar();
                            binding.limpiar();
                            binding.limpiarVista();
                            binding.regresarFoco();
                            //muestra msj de error generico
                            AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.error.todos"),
                                    TipoMensaje.ERROR, vista.getCampoReferencia());
                        }
                    });
                }
            }
        });
        
        servicio.start();
        
    }
    
    /**
     * Se utiliza cuando se cancela el pago de servicio
     */
    public void cancelarConciliacionAntad(){
        
        antadVistaModelo.cancelarConciliacionVenta(true);
        
    }
    
    public void validaEstadoImpresion(){
        
        if (!antadVistaModelo.seImprimioTicket()) {

            int parametroVentana = 0;
            boolean estaVentanaAbierta = false;

            try {
                parametroVentana = Integer.parseInt(SION.obtenerParametro(Modulo.SION, "SEGURIDAD.VENTANADESBLOQUEO.EGRESOVALORES"));
            } catch (NumberFormatException e) {
                parametroVentana = 1;
            }

            if (verificaVentana.consultaEstatusVentana(parametroVentana)) {
                SION.log(Modulo.PAGO_SERVICIOS, "estatus de ventana de bloqueo = true, mostrando alerta de "
                        + "error de impresora y se continúa con la venta", Level.INFO);

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ventas.noimpresora").replace(
                        "%1", String.valueOf(antadVistaModelo.getRespuestaAutorizacionAntad().getTransaccionId()).concat("I")), TipoMensaje.INFO, vista.getTablaServicios());
                    }
                });
                

            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "estatus de ventana de bloquoe = false, mostrando alerta de "
                        + "error de impresora en impresión de ticket de venta", Level.INFO);

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlerta(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ventas.noimpresora").replace(
                        "%1", String.valueOf(antadVistaModelo.getRespuestaAutorizacionAntad().getTransaccionId()).concat("I")), TipoMensaje.INFO, botonBloqueos);
                    }
                });
                

            }

        }
    }
    
    public void limpiarCamposReferencia(){

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                vista.getCampoReferencia().clear();
                vista.getCampoPago().clear();
                vista.getCampoReferencia().requestFocus();
            }
        });
        
    }
    
    public void evaluarBloqueosVenta(){
        binding.evaluarBloqueosVenta();
    }
    
    
}
