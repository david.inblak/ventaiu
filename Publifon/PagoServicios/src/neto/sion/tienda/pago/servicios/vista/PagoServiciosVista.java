/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vista;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import com.sion.tienda.genericos.ventanas.Ventana;
import java.io.File;
import java.util.*;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.binding.AutoCompleteList;
import neto.sion.tienda.pago.servicios.binding.PagoServiciosBinding;
import neto.sion.tienda.pago.servicios.modelo.EmpresaBean;
import neto.sion.tienda.pago.servicios.modelo.ImagenBean;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;
import neto.sion.venta.pagatodo.ps.dto.ConsultaComisionDtoResp;
import neto.sion.venta.pagatodo.ps.dto.DetalleMontoArticuloDto;
import utileriasventa.ValidadorNumerico;

/**
 *
 * @author fvega
 */
public class PagoServiciosVista implements Ventana {

    
    private final int PAGOSERVTIPOPIN = 2;
    
    private UtileriasVenta utilerias;
    private PagoServiciosBinding binding;
    private ReferenciaAyudaVista vistaAyuda;
    private ValidadorCampoReferencias validador;
    //
    private double anchoMonitor;
    private double altoMonitor;
    
    String rutaImgsLoc = "/neto/sion/tienda/pago/servicios/vista/imgs/";
    
    //Caja root contiene al panel principal y agrega overlay para mostrar botones
    // de emisores en la carga de la vista
    private HBox cajaRoot;
    
    //cajas principales
    private Group root;
    private BorderPane panelPrincipal;
    
    //panelPrincipal
    private VBox cajaSuperior;
    private HBox cajaCentral;
    private HBox cajaInferior;
    ////////cajaSuperior
    private VBox cajaDatosEmisor;
    
    
    
    
    /*-------------------------------------------------------------------------*/
    // se inicializan componentes para overlay de todos los emisores
    
    private StackPane paneOverlayEmisores;
    // Caja boton mostrar emisores
    private HBox cajaBtnMuestraEmisores;
    
    // Caja Emisores
    
    private FlowPane fpaneTodosServicios;
    private FlowPane fPaneEmisores;
    private FlowPane fPanePines;
    
    
    private Button botonServicios;
    private Button botonPrepago;
    /*--------------------------------------------------------------------------*/
    
    ///cajaDatosEmisor
    private HBox cajaEmisoresFrecuentes;
    private HBox cajaCombosEmisor;
    
    
    private Image imgServicio;
    private Image imgServicioH;
    private ImageView vistaImgServicio;
    
    
    //cajaEmisoresFrecuentes
    private Image imagenFrecuente1;
    private ImageView vistaImagenFrecuente1;
    private Image imagenFrecuente2;
    private ImageView vistaImagenFrecuente2;
    private Image imagenFrecuente3;
    private ImageView vistaImagenFrecuente3;
    private Image imagenFrecuente4;
    private ImageView vistaImagenFrecuente4;
    private Image imagenFrecuente5;
    private ImageView vistaImagenFrecuente5;    
    private Image imagenFrecuente6;
    private ImageView vistaImagenFrecuente6;
    private Image imagenFrecuente7;
    private ImageView vistaImagenFrecuente7;
    private Image imagenFrecuente8;
    private ImageView vistaImagenFrecuente8;
    private Image imagenFrecuente9;
    private ImageView vistaImagenFrecuente9;
    private Image imagenFrecuente10;
    private ImageView vistaImagenFrecuente10;
    private Image imagenFrecuente1H;
    private Image imagenFrecuente2H;
    private Image imagenFrecuente3H;
    private Image imagenFrecuente4H;
    private Image imagenFrecuente5H;
    private Image imagenFrecuente6H;
    private Image imagenFrecuente7H;
    private Image imagenFrecuente8H;
    private Image imagenFrecuente9H;
    private Image imagenFrecuente10H;
    private Image imagenFrecuentaAnt;
    private ImageView vistaImagenFrecuenteAnt;
    private Button botonAnterior;
    private Image imagenFrecuentaSig;
    private ImageView vistaImagenFrecuenteSig;
    private Button botonSiguiente;
    //cajaCombosEmisor
    private VBox cajaFiltros1;
    private VBox cajaFiltros2;
    private HBox cajaFiltros3;
    //cajaFiltros1
    private Label etiquetaEmpresa;
    private Label etiquetaReferencia;
    //cajaFiltros2
    private AutoCompleteList<String> autocompletarLista;
    // Auto acompletar overlay
    private AutoCompleteList<String> autocompletarListaOverlay;
    
    private TextField campoReferencia;
    //cajaFiltros3
    private Image imagenAyudaReferencia;
    private ImageView vistaImagenAyudaReferencia;
    //private Image imagenPesos;
    //private ImageView vistaImagenPesos;
    private HBox espacioLimpio;
    private Label etiquetaPago;
    private TextField campoPago;
    private Image imagenValidarPago;
    private ImageView vistaImagenValidarPago;
    private Button botonValidarPago;
    ////////cajaCentral
    private VBox cajaTablaPagosAcumulados;
    private VBox cajaFormasPago;
    ////cajaTablaPagosAcumulados
    private TableView tablaPagosAcumulados;
    private TableColumn columnaLogo;
    private TableColumn columnaCheck;
    private TableColumn columnaEmisor;
    private TableColumn columnaReferencia;
    private TableColumn columnaComision;
    private TableColumn columnaImporte;
    private HBox cajaEtiquetasMontoPago;
    //cajaEtiquetasMontoPago
    //cajaFormasPago
    private Group grupoImagenesMontoVenta;
    private Label etiquetaFondoMontoVenta;
    private Label etiquetaMontoVenta;
    private Label etiquetaMontoRestante;
    private TextField campoMontoRestante;
    private Label etiquetaImporteRecibido;
    private TextField campoImporteRecibido;
    private Label etiquetaCambio;
    private TextField campoCambio;
    private VBox cajaEtiquetaFormasPago;
    private VBox cajaBotonesEfectivotarjeta;
    //cajaEtiquetaFormasPago
    private Label etiquetaFormasPago;
    //cajaBotonesEfectivotarjeta
    private Image imagenBotonEfectivo;
    private ImageView vistaImagenBotonEfectivo;
    private Button botonEfectivo;
    private Image imagenBotonTarjeta;
    private ImageView vistaImagenBotonTarjeta;
    private Button botonTarjeta;
    ////////cajaInferior
    private VBox cajaFooter;
    //cajaFooter
    private HBox cajaAclaraciones;
    //cajaAclaraciones
    private HBox cajaAclaracionesCliente;
    private HBox cajaAclaracionesCajero;
    //cajaAclaracionesCliente
    private Image imagenFooterCliente;
    private ImageView vistaImagenFooterCliente;
    //cajaAclaracionesCajero
    private Image imagenFooterCajero;
    private ImageView vistaImagenFooterCajero;
    ///
    private DoubleBinding bindingRestante;
    private DoubleBinding bindingCambio;
    //
    private final DoubleProperty montoVentaProperty = new SimpleDoubleProperty(0);
    private final DoubleProperty recibidoProperty = new SimpleDoubleProperty(0);
    ////
    private final KeyCombination TAB = new KeyCodeCombination(KeyCode.TAB);
    private final KeyCombination SHIFTTAB = new KeyCodeCombination(KeyCode.TAB, KeyCodeCombination.SHIFT_ANY);
    private final KeyCombination ENTER = new KeyCodeCombination(KeyCode.ENTER);
    private final KeyCombination menos = new KeyCodeCombination(KeyCode.MINUS);
    private final KeyCombination sustraer = new KeyCodeCombination(KeyCode.SUBTRACT);
    private final KeyCombination escape = new KeyCodeCombination(KeyCode.ESCAPE);
    private final KeyCombination f10 = new KeyCodeCombination(KeyCode.F10);
    private final KeyCombination f11 = new KeyCodeCombination(KeyCode.F10);
    private final KeyCombination f9 = new KeyCodeCombination(KeyCode.F9);
    private final KeyCombination f8 = new KeyCodeCombination(KeyCode.F8);
    private final KeyCombination f2 = new KeyCodeCombination(KeyCode.F2);
    private final KeyCombination f3 = new KeyCodeCombination(KeyCode.F3);
    private final KeyCombination f4 = new KeyCodeCombination(KeyCode.F4);
    private final KeyCombination f5 = new KeyCodeCombination(KeyCode.F5);
    private final KeyCombination MAS = new KeyCodeCombination(KeyCode.EQUALS);
    private final KeyCombination ADD = new KeyCodeCombination(KeyCode.ADD);
    private final KeyCombination MENOS = new KeyCodeCombination(KeyCode.MINUS);
    //loading
    private ImageView imagenLoading;
    private Screen screen;// = (Screen) Screen.getPrimary();
    //
    private final int EFECTIVO = 1;
    private final int TARJETA = 2;
    //
    private String carpeta;
    private List<ImagenBean> listaImagenesFrecuentes;
    private List<ImagenBean> listaLogosTabla;
    private List<ImagenBean> listaImagenesReferencia;
    private String RUTA_FRECUENTES;
    private String RUTA_ICONOS_TABLA;
    private String RUTA_REFERENCIAS;
    //
    private EventHandler<KeyEvent> eventoESCPagoServicios;
    //componentes AVON
    private HBox cajaVendedorEmisor;
    private HBox cajaPortadorReferencia;
    private Label etiquetaReferenciaAyudaAVON;
    private RadioButton radioBotonVendedor;
    private RadioButton radioBotonPortador;
    private ToggleGroup grupoBotonesAvon;
    //
    //componentes jafra
    private HBox cajaDigitoVerificadorJafra;
    private TextField campoDigitoVerificadorJafra;
    //
    //componentes televia
    private ChoiceBox<String> comboTelevia;
    private ObservableList<String> listaMontosTelevia = FXCollections.observableArrayList();
    private int montoPagoTelevia = 0;
    //Componentes Pines
    private ChoiceBox<String> comboPines;
    private ObservableList<String> listaMontosPines = FXCollections.observableArrayList();
    private int montoPagoPines = 0;
    
    
    
    //verificacion hora sistema
    private Calendar calendario;
    private int horaSistema;
    private int minutoSistema;
    //antad
    private final DoubleProperty referenciaAntadProperty = new SimpleDoubleProperty(0);
    private int movimientosPorRealizar = 2;
    private int imagenesEnMemoriaPorRecorrer = 0;
    
    private StackPane paneOverlay;
    private HBox cajaOverlay;
    private Label soloUnPagoServicio;
    
    //botonn muestra overlay todos los emisores disponibles
    
    private Button btnMuestraEmisores;
    
    //hbox muestra emisores en la carga de la pantalla
    private FlowPane cajaOverlayEmisores;
    
    
    private FlowPane cajaBuscadorOverlay;
    private FlowPane cajaEmisoresOverlay;
    
    
    
    
    
    
    
    

    public HBox getCajaOverlay() {
        return cajaOverlay;
    }
    
    public FlowPane getCajaOverlayEmisores() {
        return cajaOverlayEmisores;
    }
    
    public PagoServiciosVista() {

        SION.log(Modulo.PAGO_SERVICIOS, "PagoServicios.jar Version: ESL Cambio 543 - Se agrega proceso pagatodo", Level.INFO);

        this.utilerias = new UtileriasVenta();
        this.binding = new PagoServiciosBinding(utilerias, this);
        this.vistaAyuda = new ReferenciaAyudaVista(this, utilerias);
        this.validador = new ValidadorCampoReferencias();
        //
        this.anchoMonitor = (utilerias.getAnchoMonitor());
        this.altoMonitor = (utilerias.getAlturaMonitor());
        
        
        //cajas principales
        this.cajaRoot = new HBox();
        this.root = new Group();
        this.panelPrincipal = new BorderPane();
        paneOverlayEmisores = new StackPane();
        paneOverlayEmisores.setAlignment(Pos.TOP_CENTER);
        
        
        // overlay muestra emisores en carga de la vista
        cajaOverlayEmisores = new FlowPane();
        cajaOverlayEmisores.setPrefSize(anchoMonitor * .97, altoMonitor * .75);
        cajaOverlayEmisores.setAlignment(Pos.TOP_CENTER);
        cajaOverlayEmisores.getStyleClass().add("cajaEmisoresSPane");
        
        //----------------------AGREGA OBJETOS PARA BUSQUEDA DE EMISORES Y BOTONES DE CAMBIO DESDE OVERLAY-------
        cajaBuscadorOverlay = new FlowPane();
        cajaBuscadorOverlay.setPrefSize(anchoMonitor * .97, altoMonitor * .12);
        cajaBuscadorOverlay.setAlignment(Pos.TOP_CENTER);
        cajaBuscadorOverlay.setStyle("-fx-padding:25px;");
        //cajaBuscadorOverlay.setStyle("-fx-padding:15px; -fx-background-color : #cccccc;");
        
        //Boton muestra pines
        botonServicios = new Button();
        botonServicios.setText(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.BOTON.TEXT.SERVICIOS"));
        botonServicios.setAlignment(Pos.CENTER);
        botonServicios.getStyleClass().add("buttonTipoServicio");
        botonServicios.setPrefWidth(anchoMonitor * .35);
        botonServicios.setPrefHeight(37);
        
        cajaBuscadorOverlay.getChildren().add(botonServicios);
        
        HBox boxSpace = new HBox();
        boxSpace.setPrefWidth(25);
        boxSpace.setPrefHeight(37);  
        cajaBuscadorOverlay.getChildren().add(boxSpace);
        
        //Boton muestra pines
        botonPrepago = new Button();
        botonPrepago.setText(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.BOTON.TEXT.PINES"));
        botonPrepago.setAlignment(Pos.CENTER);
        botonPrepago.getStyleClass().add("buttonTipoServicio");
        botonPrepago.setPrefWidth(anchoMonitor * .35);
        botonPrepago.setPrefHeight(37);
        
        cajaBuscadorOverlay.getChildren().add(botonPrepago);
        
        
        HBox hBoxSpaceW = new HBox();
        hBoxSpaceW.setPrefSize(anchoMonitor * .97, 10);
        cajaBuscadorOverlay.getChildren().add(hBoxSpaceW);
        
        // Caja buscador autoacompletar lista
       
         autocompletarListaOverlay = null;
        
        
        
        
        
        //----------------------AGREGA BOTONES DE SERVICIOS-------
        
        
        cajaEmisoresOverlay = new FlowPane();
        cajaEmisoresOverlay.setPrefSize(anchoMonitor * .97, altoMonitor * .63);
        
        
        
        fpaneTodosServicios = new FlowPane();
        fpaneTodosServicios.setPrefSize(anchoMonitor * .97, altoMonitor * .63);
        fpaneTodosServicios.setAlignment(Pos.TOP_CENTER);
        
        
        
        
        fPaneEmisores = new FlowPane();
        fPaneEmisores.setVgap(10);
        fPaneEmisores.setHgap(10);
        fPaneEmisores.setPrefWidth(anchoMonitor * .97);
        fPaneEmisores.setPrefHeight(altoMonitor * .63);
        fPaneEmisores.setAlignment(Pos.TOP_CENTER);
        
        
        
        fPanePines = new FlowPane();
        fPanePines.setVgap(10);
        fPanePines.setHgap(10);
        fPanePines.setPrefWidth(anchoMonitor * .97);
        fPanePines.setPrefHeight(altoMonitor * .63);
        fPanePines.setAlignment(Pos.TOP_CENTER);
        
        fpaneTodosServicios.getChildren().add(fPaneEmisores);
        
        
        cajaEmisoresOverlay.getChildren().add(fpaneTodosServicios);
        
        cajaOverlayEmisores.getChildren().addAll(cajaBuscadorOverlay, cajaEmisoresOverlay);
        



        //panelPrincipal
        this.cajaSuperior = utilerias.getVBox(0, anchoMonitor * .97, altoMonitor * .25);
        this.cajaCentral = utilerias.getHBox(0, anchoMonitor * .97, altoMonitor * .52);
        this.cajaInferior = utilerias.getHBox(0, anchoMonitor * .97, altoMonitor * .03);
        
        paneOverlay = new StackPane();
        cajaOverlay = utilerias.getHBox(0, anchoMonitor * .97, altoMonitor * .25);
        cajaOverlay.setStyle("-fx-background-color: #000000");
        cajaOverlay.setOpacity(.7);
        
        soloUnPagoServicio = new Label("Recuerda que solo se puede agregar una referencia de pago de servicio a la vez");
        cajaOverlay.getChildren().add(soloUnPagoServicio);
        soloUnPagoServicio.setStyle("-fx-font: 20pt Verdana; -fx-text-fill: #E9D460;");
        cajaOverlay.setAlignment(Pos.CENTER);
        
        paneOverlay.getChildren().addAll(cajaSuperior,cajaOverlay);
        
        
        
        this.btnMuestraEmisores = new Button();
        this.btnMuestraEmisores.setText("Mostrar todos los emisores");
        
        
        ///////////cajaSuperior
        this.cajaDatosEmisor = utilerias.getVBox(0, anchoMonitor * .97, altoMonitor * .25);
        
        ///////Caja para mostrar el boton que muestra todos los emisores disponibles
        this.cajaBtnMuestraEmisores = utilerias.getHBox(0, anchoMonitor * .97, altoMonitor * .05);
        
        ///////cajaDatosEmisor
        this.cajaEmisoresFrecuentes = utilerias.getHBox(5, anchoMonitor * .97, altoMonitor * .1);
        this.cajaCombosEmisor = utilerias.getHBox(0, anchoMonitor * .97, altoMonitor * .1);
        
        
        //imagenes y vista para muestra todos los servicios
        this.imgServicio = null;
        this.imgServicioH = null;
        this.vistaImgServicio = null;
        
        
        
        //cajaEmisoresFrecuentes
        this.imagenFrecuente1 = null;
        this.vistaImagenFrecuente1 = null;
        this.imagenFrecuente2 = null;
        this.vistaImagenFrecuente2 = null;
        this.imagenFrecuente3 = null;
        this.vistaImagenFrecuente3 = null;
        this.imagenFrecuente4 = null;
        this.vistaImagenFrecuente4 = null;
        this.imagenFrecuente5 = null;
        this.vistaImagenFrecuente5 = null;
        this.imagenFrecuente6 = null;
        this.vistaImagenFrecuente6 = null;
        this.imagenFrecuente7 = null;
        this.vistaImagenFrecuente7 = null;
        this.imagenFrecuente8 = null;
        this.vistaImagenFrecuente8 = null;
        this.imagenFrecuente9 = null;
        this.vistaImagenFrecuente9 = null;
        this.imagenFrecuente10 = null;
        this.vistaImagenFrecuente10 = null;
        this.imagenFrecuente1H = null;
        this.imagenFrecuente2H = null;
        this.imagenFrecuente3H = null;
        this.imagenFrecuente4H = null;
        this.imagenFrecuente5H = null;
        this.imagenFrecuente6H = null;
        this.imagenFrecuente7H = null;
        this.imagenFrecuente8H = null;
        this.imagenFrecuente9H = null;
        this.imagenFrecuente10H = null;
        this.imagenFrecuentaAnt = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/Venta_anterior.png");
        this.vistaImagenFrecuenteAnt = new ImageView(imagenFrecuentaAnt);
        this.botonAnterior = new Button("", vistaImagenFrecuenteAnt);
        this.imagenFrecuentaSig = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/Venta_siguiente.png");
        this.vistaImagenFrecuenteSig = new ImageView(imagenFrecuentaSig);
        this.botonSiguiente = new Button("", vistaImagenFrecuenteSig);
        //cajaCombosEmisor
        this.cajaFiltros1 = utilerias.getVBox(0, anchoMonitor * .1, altoMonitor * .1);
        this.cajaFiltros2 = utilerias.getVBox(2, anchoMonitor * .32, altoMonitor * .1);
        this.cajaFiltros3 = utilerias.getHBox(5, anchoMonitor * .55, altoMonitor * .1);
        //cajaFiltros1
        this.etiquetaEmpresa = utilerias.getLabel("Empresa", anchoMonitor * .1, altoMonitor * .05);
        this.etiquetaReferencia = utilerias.getLabel("Referencia", anchoMonitor * .1, altoMonitor * .05);
        //cajaFiltros2
        this.autocompletarLista = null;
        this.campoReferencia = utilerias.getTextField("Referencia de pago", anchoMonitor * .32, altoMonitor * .045);
        //cajaFiltros3
        //this.imagenAyudaReferencia = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/icon_Info.png");
        this.imagenAyudaReferencia = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/imgs/ico_ayuda.png");
        this.vistaImagenAyudaReferencia = new ImageView(imagenAyudaReferencia);
        //this.imagenPesos = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/icon_Pesos.png");
        //this.vistaImagenPesos = new ImageView(imagenPesos);
        this.etiquetaPago = utilerias.getLabel("Importe de Pago", anchoMonitor * .19, altoMonitor * .06);
        //this.campoPago.setStyle("-fx-font: normal 14px sans-serif;");
        this.campoPago = utilerias.getTextField("", anchoMonitor * .12, altoMonitor * .05);
        //this.campoPago.setStyle("-fx-font: normal 14px sans-serif;");
        //this.imagenValidarPago = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/btn_ValidarPago.png");
        this.imagenValidarPago = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/imgs/ico_valida_pago.png");
        
        this.vistaImagenValidarPago = new ImageView(imagenValidarPago);
        this.vistaImagenValidarPago.setFitHeight(25);
        this.vistaImagenValidarPago.setFitWidth(25);
        
        
        this.espacioLimpio = new HBox();
        this.botonValidarPago = new Button();

        /////////cajaCentral
        this.cajaTablaPagosAcumulados = utilerias.getVBox(0, anchoMonitor * .7, altoMonitor * .5);
        this.cajaFormasPago = utilerias.getVBox(0, anchoMonitor * .27, altoMonitor * .5);
        ////cajaTablaPagosAcumulados
        this.tablaPagosAcumulados = new TableView();
        this.columnaCheck = utilerias.getColumna("", anchoMonitor * .03);
        //this.columnaLogo = utilerias.getColumna("", anchoMonitor * .07);
        this.columnaLogo = utilerias.getColumna("", anchoMonitor * .07);
        this.columnaEmisor = utilerias.getColumna("Emisor", anchoMonitor * .2);
        this.columnaReferencia = utilerias.getColumna("Referencia", anchoMonitor * .2);
        this.columnaComision = utilerias.getColumna("Comision", anchoMonitor * .07);
        this.columnaImporte = utilerias.getColumna("Importe", anchoMonitor * .07);
        this.cajaEtiquetasMontoPago = utilerias.getHBox(15, anchoMonitor * .7, altoMonitor * .05);
        //cajaEtiquetasMontoPago

        ////cajaFormasPago
        this.grupoImagenesMontoVenta = new Group();
        this.etiquetaFondoMontoVenta = utilerias.getLabelConImageView("/neto/sion/tienda/pago/servicios/vista/Venta_montoventa.png", altoMonitor * .125, anchoMonitor * .25, Pos.CENTER_LEFT);
        this.etiquetaMontoVenta = utilerias.getLabelConEstilos("EtiquetaMontoVenta", anchoMonitor * .24, altoMonitor * .1, Pos.CENTER_RIGHT, 0, altoMonitor * .03);
        this.etiquetaMontoRestante = utilerias.getLabel("Importe restante", anchoMonitor * .25, altoMonitor * .03);
        this.campoMontoRestante = utilerias.getTextField("", anchoMonitor * .25, altoMonitor * .05);
        this.etiquetaImporteRecibido = utilerias.getLabel("Importe Recibido", anchoMonitor * .25, altoMonitor * .03);
        this.campoImporteRecibido = utilerias.getTextField("", anchoMonitor * .25, altoMonitor * .05);
        campoImporteRecibido.setEditable(false);
        this.etiquetaCambio = utilerias.getLabel("Cambio", anchoMonitor * .25, altoMonitor * .03);
        this.campoCambio = utilerias.getTextField("", anchoMonitor * .25, altoMonitor * .05);
        this.cajaEtiquetaFormasPago = utilerias.getVBox(0, anchoMonitor * .25, altoMonitor * .03);
        this.cajaBotonesEfectivotarjeta = utilerias.getVBox(10, anchoMonitor * .25, altoMonitor * .135);
        //cajaEtiquetaFormasPago
        this.etiquetaFormasPago = utilerias.getLabel("Formas de pago", anchoMonitor * .25, altoMonitor * .03);
        //cajaBotonesEfectivotarjeta
        this.imagenBotonEfectivo = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/btn_Efectivo.png");
        this.vistaImagenBotonEfectivo = new ImageView(imagenBotonEfectivo);
        this.botonEfectivo = new Button();
        this.imagenBotonTarjeta = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/btn_Tarjeta.png");
        this.vistaImagenBotonTarjeta = new ImageView(imagenBotonTarjeta);
        this.botonTarjeta = new Button();

        ////////cajaInferior
        this.cajaFooter = utilerias.getVBox(0, anchoMonitor * .97, altoMonitor * .11);
        //cajaFooter
        this.cajaAclaraciones = utilerias.getHBox(0, anchoMonitor * .97, altoMonitor * .11);


        //cajaAclaraciones
        this.cajaAclaracionesCliente = utilerias.getHBox(0, anchoMonitor * .485, altoMonitor * .11);
        this.cajaAclaracionesCajero = utilerias.getHBox(0, anchoMonitor * .485, altoMonitor * .11);
        //cajaAclaracionesCliente
        this.imagenFooterCliente = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/footer_cliente.png");
        this.vistaImagenFooterCliente = new ImageView(imagenFooterCliente);
        //cajaAclaracionesCajero
        this.imagenFooterCajero = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/footer_cajero.png");
        this.vistaImagenFooterCajero = new ImageView(imagenFooterCajero);
        //cajaComision

        /////loading 
        this.screen = (Screen) Screen.getPrimary();
        this.imagenLoading = new ImageView(utilerias.getImagen("/com/sion/tienda/genericos/images/popup/loading.gif"));

        //
        this.carpeta = "";
        this.RUTA_FRECUENTES = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "RUTA.IMAGENES.FRECUENTES");
        this.RUTA_ICONOS_TABLA = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "RUTA.IMAGENES.TABLA");
        this.RUTA_REFERENCIAS = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "RUTA.IMAGENES.REFERENCIAS");

        this.listaImagenesFrecuentes = obtieneImagenes(RUTA_FRECUENTES);
        this.listaLogosTabla = obtieneImagenes(RUTA_ICONOS_TABLA);
        this.listaImagenesReferencia = obtieneImagenes(RUTA_REFERENCIAS);

        //
        this.eventoESCPagoServicios = null;

        //componentes AVON
        this.cajaVendedorEmisor = utilerias.getHBox(5, anchoMonitor * .32, altoMonitor * .05);
        this.cajaPortadorReferencia = utilerias.getHBox(5, anchoMonitor * .32, altoMonitor * .05);
        this.etiquetaReferenciaAyudaAVON = utilerias.getLabel("B.003528", anchoMonitor * .08, altoMonitor * .044);
        this.radioBotonVendedor = new RadioButton("Vendedor");
        this.radioBotonPortador = new RadioButton("Portador");
        this.grupoBotonesAvon = new ToggleGroup();
        //

        //componentes jafra
        this.cajaDigitoVerificadorJafra = utilerias.getHBox(5, anchoMonitor * .32, altoMonitor * .044);
        this.campoDigitoVerificadorJafra = utilerias.getTextField("", anchoMonitor * .055, altoMonitor * .044);
        //

        //componentes televia
        this.comboTelevia = new ChoiceBox<String>();
        
        this.comboPines = new ChoiceBox<String>();
        
        //


        this.calendario = null;
        this.horaSistema = 0;
        this.minutoSistema = 0;
        
        

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                autocompletarLista = new AutoCompleteList<String>(anchoMonitor * .28, altoMonitor * .045);
                
                autocompletarListaOverlay = new AutoCompleteList<String>(anchoMonitor * .28, altoMonitor * .045);
        
                agregarEstilos();
                ajustarTamaniosComponentes();
                cargarServiciosFrecuentes();
                cargarTodosLosServicios();
                cargarEmisoresPines();
                creaListeners();
                creaHandlers();
                actualizarCantidades();
                SION.log(Modulo.PAGO_SERVICIOS, "Formateando campo de pago", Level.INFO);
                formatearCampo(campoPago);
                formatearCampo(campoImporteRecibido);
                binding();
                revisarServiciosFrecuentes();
                cargarComponentesDefault();
                configuraBuscardorOverlay();
                
            }
        });
        
        
        
        
        cajaOverlay.setVisible(false);
        cajaOverlayEmisores.setVisible(true);
        
        
    }
    
    public void configuraBuscardorOverlay(){
        
        autocompletarListaOverlay.setMaxSize(anchoMonitor * .35, altoMonitor * .045);
        autocompletarListaOverlay.setMinSize(anchoMonitor * .35, altoMonitor * .045);
        autocompletarListaOverlay.setPrefSize(anchoMonitor * .35, altoMonitor * .045);
        cajaBuscadorOverlay.getChildren().add(autocompletarListaOverlay);
        autocompletarListaOverlay.getTextField().requestFocus();
        
    }
    
    public void ocultaOverlayEmisores(){
        cajaOverlayEmisores.setVisible(false);
    }

    public TableView getTablaPagosAcumulados() {
        return tablaPagosAcumulados;
    }

    public void setTablaPagosAcumulados(TableView tablaPagosAcumulados) {
        this.tablaPagosAcumulados = tablaPagosAcumulados;
    }

    public AutoCompleteList<String> getAutocompletarLista() {
        return autocompletarLista;
    }
    
    
    public void setEmisoraBuscador(String emisor){
    
        autocompletarLista.setText(emisor);
        if (autocompletarLista.isPopupShowing()) {
                            
            autocompletarLista.ocultarPopup();
        }
        campoReferencia.setText("");
        campoReferencia.requestFocus();
    }
    
    /*public void setEmisoraBuscadorOverlay(String emisor){
    
        autocompletarListaOverlay.getTextField().setText(emisor);
    }*/
    
     public void limpiarEmisorOverlay(){
    
        autocompletarListaOverlay.getTextField().clear();
    }
    

    private int obtenerHoraSistema() {
        calendario = new GregorianCalendar();
        return calendario.get(calendario.HOUR_OF_DAY);
    }

    private int obtenerMinutoSistema() {
        calendario = new GregorianCalendar();
        return calendario.get(calendario.MINUTE);
    }

    private void agregarEstilos() {


        cajaRoot.setStyle("-fx-alignment: top-center;");
        
        
        root.setStyle("-fx-alignment: top-center;");

        panelPrincipal.setStyle("-fx-alignment: top-center;");

        cajaSuperior.setStyle("-fx-background-color: white; -fx-alignment: top-center;");
        cajaCentral.setStyle("-fx-background-color: white; -fx-alignment: center;");
        cajaInferior.setStyle("-fx-background-color: white; -fx-alignment: top-center;");
        cajaFooter.setStyle("-fx-background-color: white; -fx-alignment: top-center;");
        
        /* estilos caja emisor muestra todos los emisores
        cajaEmisoresSP.getStyleClass().add("CajaTodosEmisores");*/
        
        //cajaEmisoresSPane.getStyleClass().add("cajaEmisoresSPane");
        fPaneEmisores.getStyleClass().add("fPaneEmisores");
        
        //Estilos caja contenedor boton muestra todos los emisores
        cajaBtnMuestraEmisores.setStyle("-fx-alignment: top-center;");
        btnMuestraEmisores.getStyleClass().add("buttonMuestraEmisores");
        
        
        //avon
        etiquetaReferenciaAyudaAVON.setStyle("-fx-alignment: center-left; -fx-font: 20pt Verdana; -fx-text-fill: #084B8A;");
        radioBotonVendedor.setStyle("-fx-alignment: center-left; -fx-font: 20pt Verdana; -fx-text-fill: #084B8A;");
        radioBotonPortador.setStyle("-fx-alignment: center-left; -fx-font: 20pt Verdana; -fx-text-fill: #084B8A;");

        cajaVendedorEmisor.setStyle("-fx-alignment: bottom-center;");
        cajaPortadorReferencia.setStyle("-fx-alignment: bottom-center;");
        //

        //jafra
        campoDigitoVerificadorJafra.getStyleClass().add("TextField");
        //

        //televia
        comboTelevia.getStyleClass().add("Combo");
        
        comboPines.getStyleClass().add("Combo");

        //list.getTextField().getStyleClass().add("TextField");
        autocompletarLista.getList().getStyleClass().add("scrollResumen");
        autocompletarLista.getPanelPrincipal().getStyleClass().add("CampoAutoCompletar");
        
        
        autocompletarListaOverlay.getList().getStyleClass().add("scrollResumen");
        autocompletarListaOverlay.getPanelPrincipal().getStyleClass().add("CampoAutoCompletar");

        //cajaDatosEmisor
        cajaEmisoresFrecuentes.getStyleClass().add("CajaEmisoresFrecuentes");
        cajaCombosEmisor.setStyle("-fx-background-color: white; -fx-alignment: center;");

        //caja central
        cajaFiltros1.setStyle("-fx-alignment: center-right bottom-left;");
        cajaFiltros2.setStyle("-fx-alignment: center;");
        cajaFiltros3.setStyle("-fx-alignment: center;");
        etiquetaEmpresa.setStyle("-fx-alignment: center-left; -fx-font: 20pt Verdana; -fx-text-fill: #084B8A;");

        etiquetaReferencia.setStyle("-fx-alignment: center-left; -fx-font: 20pt Verdana; -fx-text-fill: #084B8A;");
        campoReferencia.getStyleClass().add("TextField");

        etiquetaPago.setStyle("-fx-alignment: center; -fx-font: 20pt Verdana; -fx-text-fill: #084B8A;");
        campoPago.getStyleClass().add("TextField");

        cajaFormasPago.setStyle("-fx-alignment: center-left;");

        cajaTablaPagosAcumulados.getStyleClass().add("CajaTablaPagosAcumulados");
        cajaEtiquetasMontoPago.getStyleClass().add("CajaEtiquetasMontoPago");

        //inferior
        cajaBotonesEfectivotarjeta.setStyle("-fx-alignment: center;");

        etiquetaMontoRestante.setStyle("-fx-alignment: center-left; -fx-font: 22pt Verdana; -fx-text-fill: #084B8A;");
        etiquetaImporteRecibido.setStyle("-fx-alignment: center-left; -fx-font: 22pt Verdana; -fx-text-fill: #084B8A;");
        campoMontoRestante.getStyleClass().add("TextField");
        campoImporteRecibido.getStyleClass().add("TextField");

        etiquetaCambio.setStyle("-fx-alignment: center-left; -fx-font: 22pt Verdana; -fx-text-fill: #084B8A;");
        campoCambio.getStyleClass().add("TextField");

        cajaEtiquetaFormasPago.setStyle("-fx-alignment: center;");
        etiquetaFormasPago.setStyle("-fx-alignment: center-left; -fx-font: 22pt Verdana; -fx-text-fill: #084B8A;");

        cajaAclaracionesCliente.setStyle("-fx-background-color: white; -fx-alignment: center;");
        cajaAclaracionesCajero.setStyle("-fx-background-color: white; -fx-alignment: center;");
        
        
        
        
        
    }

    private void ajustarTamaniosComponentes() {
        
        
        
        
        // boton muestra overlay todos los emisores disponibles
        btnMuestraEmisores.setMaxSize(anchoMonitor * .22, altoMonitor * .04);
        btnMuestraEmisores.setPrefSize(anchoMonitor * .22, altoMonitor * .04);
        btnMuestraEmisores.setMinSize(anchoMonitor * .22, altoMonitor * .04);
        

    
        

        //avon
        grupoBotonesAvon.getToggles().addAll(radioBotonVendedor, radioBotonPortador);
        radioBotonVendedor.setMaxSize(anchoMonitor * .1, altoMonitor * .045);
        radioBotonVendedor.setMinSize(anchoMonitor * .1, altoMonitor * .045);
        radioBotonVendedor.setPrefSize(anchoMonitor * .1, altoMonitor * .045);
        radioBotonPortador.setMaxSize(anchoMonitor * .1, altoMonitor * .045);
        radioBotonPortador.setMinSize(anchoMonitor * .1, altoMonitor * .045);
        radioBotonPortador.setPrefSize(anchoMonitor * .1, altoMonitor * .045);
        //

        //jafra
        campoDigitoVerificadorJafra.setVisible(false);
        //

        //televia
        comboTelevia.setDisable(true);
        comboTelevia.setMaxSize(campoPago.getMaxWidth(), campoPago.getMaxHeight());
        comboTelevia.setMinSize(campoPago.getMinWidth(), campoPago.getMinHeight());
        comboTelevia.setPrefSize(campoPago.getMaxWidth(), campoPago.getMaxHeight());
        comboTelevia.setItems(listaMontosTelevia);
        
        //
        
        
        //Pines
        comboPines.setDisable(true);
        comboPines.setMaxSize(campoPago.getMaxWidth(), campoPago.getMaxHeight());
        comboPines.setMinSize(campoPago.getMinWidth(), campoPago.getMinHeight());
        comboPines.setPrefSize(campoPago.getMaxWidth(), campoPago.getMaxHeight());
        comboPines.setItems(listaMontosPines);
        //
        
        
        
        //
        
        
        
        autocompletarLista.setMaxSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setMinSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setPrefSize(anchoMonitor * .32, altoMonitor * .045);


        cajaSuperior.getChildren().addAll(cajaDatosEmisor);
        cajaDatosEmisor.getChildren().addAll(cajaEmisoresFrecuentes,cajaBtnMuestraEmisores, cajaCombosEmisor);
        cajaBtnMuestraEmisores.getChildren().add(btnMuestraEmisores);
        
        cajaCombosEmisor.getChildren().addAll(cajaFiltros1, cajaFiltros2, cajaFiltros3);
        cajaFiltros1.getChildren().addAll(etiquetaEmpresa, etiquetaReferencia);
        cajaFiltros2.getChildren().addAll(autocompletarLista, campoReferencia);
        //cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia, vistaImagenPesos,etiquetaPago, campoPago, botonValidarPago);
        cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia,etiquetaPago, campoPago, botonValidarPago);
        //
        //vistaImagenPesos.setFitWidth(anchoMonitor * .05);
        //vistaImagenPesos.setFitHeight(altoMonitor * .05);
        //botonValidarPago.setGraphic(vistaImagenValidarPago);
        
        espacioLimpio.setPrefWidth(anchoMonitor * .22);
        espacioLimpio.setPrefHeight(altoMonitor * .05);
        
        
        botonValidarPago.getStyleClass().add("buttonValidarPago");
        botonValidarPago.setText("Validar Pago");
        
        botonValidarPago.setMaxSize(anchoMonitor * .12, altoMonitor * .05);
        botonValidarPago.setMinSize(anchoMonitor * .12, altoMonitor * .05);
        botonValidarPago.setPrefSize(anchoMonitor * .12, altoMonitor * .05);
        
        
        
        /*vistaImagenValidarPago.setFitWidth(anchoMonitor * .12);
        vistaImagenValidarPago.setFitHeight(altoMonitor * .05);*/

        /*vistaImagenAyudaReferencia.setFitHeight(altoMonitor * .05);
        vistaImagenAyudaReferencia.setFitWidth(anchoMonitor * .05);*/
        
        vistaImagenAyudaReferencia.setFitHeight(50);
        vistaImagenAyudaReferencia.setFitWidth(50);

        //
        tablaPagosAcumulados.setMaxSize(anchoMonitor * .65, altoMonitor * .4);
        tablaPagosAcumulados.setMinSize(anchoMonitor * .65, altoMonitor * .4);
        tablaPagosAcumulados.setPrefSize(anchoMonitor * .65, altoMonitor * .4);
        tablaPagosAcumulados.getColumns().addAll(columnaCheck, columnaLogo, columnaEmisor,
                columnaComision, columnaReferencia, columnaImporte);
        cajaEtiquetasMontoPago.getChildren().addAll();
        cajaTablaPagosAcumulados.getChildren().addAll(tablaPagosAcumulados, cajaEtiquetasMontoPago);

        //
        cajaCentral.getChildren().addAll(cajaTablaPagosAcumulados, cajaFormasPago);


        cajaAclaracionesCliente.getChildren().addAll(vistaImagenFooterCliente);
        cajaAclaracionesCajero.getChildren().addAll(vistaImagenFooterCajero);
        cajaEtiquetaFormasPago.getChildren().addAll(etiquetaFormasPago);
        cajaBotonesEfectivotarjeta.getChildren().addAll(botonEfectivo/*
                 * , botonTarjeta
                 */);
        grupoImagenesMontoVenta.getChildren().addAll(etiquetaFondoMontoVenta, etiquetaMontoVenta);
        cajaFormasPago.getChildren().addAll(grupoImagenesMontoVenta, etiquetaMontoRestante, campoMontoRestante, etiquetaImporteRecibido,
                campoImporteRecibido, etiquetaCambio, campoCambio/*
                 * , cajaEtiquetaFormasPago,
                 */, cajaBotonesEfectivotarjeta);

        etiquetaFormasPago.setWrapText(true);
        campoMontoRestante.setEditable(false);
        campoCambio.setEditable(false);

        vistaImagenBotonEfectivo.setFitWidth(anchoMonitor * .25);
        vistaImagenBotonEfectivo.setFitHeight(altoMonitor * .09);

        vistaImagenBotonTarjeta.setFitWidth(anchoMonitor * .25);
        vistaImagenBotonTarjeta.setFitHeight(altoMonitor * .045);

        //botonEfectivo.setGraphic(vistaImagenBotonEfectivo);
        botonEfectivo.getStyleClass().add("buttonEfectivo");
        botonEfectivo.setText("$ Efectivo");
        
        botonTarjeta.setGraphic(vistaImagenBotonTarjeta);

        botonEfectivo.setMaxSize(anchoMonitor * .22, altoMonitor * .09);
        botonEfectivo.setPrefSize(anchoMonitor * .22, altoMonitor * .09);
        botonEfectivo.setMinSize(anchoMonitor * .22, altoMonitor * .09);

        botonTarjeta.setMaxSize(anchoMonitor * .25, altoMonitor * .045);
        botonTarjeta.setPrefSize(anchoMonitor * .25, altoMonitor * .045);
        botonTarjeta.setMinSize(anchoMonitor * .25, altoMonitor * .045);

        vistaImagenFooterCliente.setFitHeight(altoMonitor * .1);
        vistaImagenFooterCliente.setFitWidth(anchoMonitor * .33);
        vistaImagenFooterCajero.setFitHeight(altoMonitor * .1);
        vistaImagenFooterCajero.setFitWidth(anchoMonitor * .33);
        cajaAclaraciones.getChildren().addAll(cajaAclaracionesCajero, cajaAclaracionesCliente);
        cajaFooter.getChildren().addAll(cajaAclaraciones);
        //cajaInferior.getChildren().addAll(cajaFooter);


        panelPrincipal.setTop(paneOverlay);
        panelPrincipal.setCenter(cajaCentral);
        panelPrincipal.setBottom(cajaInferior);
        
        paneOverlayEmisores.getChildren().addAll(panelPrincipal,cajaOverlayEmisores);
        
        cajaRoot.getChildren().addAll(paneOverlayEmisores);
        root.getChildren().add(cajaRoot);
    }

    public void setRadioVendedorAvon() {
        radioBotonVendedor.setSelected(true);
    }

    public void cargarComponentesTelevia() {
        SION.log(Modulo.PAGO_SERVICIOS, "Cargando componentes de vista de emisor Televia", Level.INFO);

        this.vistaImagenValidarPago.setFitHeight(25);
        this.vistaImagenValidarPago.setFitWidth(25);
        botonValidarPago.setGraphic(this.vistaImagenValidarPago);
        botonValidarPago.setContentDisplay(ContentDisplay.LEFT);
        botonValidarPago.setWrapText(true);
        botonValidarPago.getStyleClass().add("buttonValidarPago");
        botonValidarPago.setText("Validar Pago");
        
        botonValidarPago.setMaxSize(anchoMonitor * .12, altoMonitor * .05);
        botonValidarPago.setMinSize(anchoMonitor * .12, altoMonitor * .05);
        botonValidarPago.setPrefSize(anchoMonitor * .12, altoMonitor * .05);

        campoPago.setVisible(true);

        comboTelevia.setDisable(false);
        comboPines.setDisable(true);
        
        montoPagoTelevia = 0;

        campoDigitoVerificadorJafra.setVisible(false);
        campoDigitoVerificadorJafra.setText("");

        cajaFiltros3.getChildren().clear();
        
        cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia,
                etiquetaPago, comboTelevia, botonValidarPago);

        autocompletarLista.setMaxSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setMinSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setPrefSize(anchoMonitor * .32, altoMonitor * .045);

        campoReferencia.setMaxSize(anchoMonitor * .32, altoMonitor * .045);
        campoReferencia.setMinSize(anchoMonitor * .32, altoMonitor * .045);
        campoReferencia.setPrefSize(anchoMonitor * .32, altoMonitor * .045);

        cajaFiltros2.getChildren().clear();
        cajaFiltros2.getChildren().addAll(autocompletarLista, campoReferencia);

        validador.setMaXCaracteres(39);

        cargarMontosTelevia();

    }
    
     public void cargarComponentesPines() {
         
        SION.log(Modulo.PAGO_SERVICIOS, "Cargando componentes de vista Prepago Combo Dinamico", Level.INFO);


        comboPines.getSelectionModel().clearSelection();
        comboPines.getItems().clear();
        
        this.vistaImagenValidarPago.setFitHeight(25);
        this.vistaImagenValidarPago.setFitWidth(25);
        botonValidarPago.setGraphic(this.vistaImagenValidarPago);
        botonValidarPago.setContentDisplay(ContentDisplay.LEFT);
        botonValidarPago.setWrapText(true);
        botonValidarPago.getStyleClass().add("buttonValidarPago");
        botonValidarPago.setText("Validar Pago");
        
        botonValidarPago.setMaxSize(anchoMonitor * .12, altoMonitor * .05);
        botonValidarPago.setMinSize(anchoMonitor * .12, altoMonitor * .05);
        botonValidarPago.setPrefSize(anchoMonitor * .12, altoMonitor * .05);

        campoPago.setVisible(true);

        comboTelevia.setDisable(true);
        comboPines.setDisable(false);
        
        montoPagoPines = 0;

        campoDigitoVerificadorJafra.setVisible(false);
        campoDigitoVerificadorJafra.setText("");

        cajaFiltros3.getChildren().clear();
        
        
        cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia,
                etiquetaPago, comboPines, botonValidarPago);

        autocompletarLista.setMaxSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setMinSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setPrefSize(anchoMonitor * .32, altoMonitor * .045);

        campoReferencia.setMaxSize(anchoMonitor * .32, altoMonitor * .045);
        campoReferencia.setMinSize(anchoMonitor * .32, altoMonitor * .045);
        campoReferencia.setPrefSize(anchoMonitor * .32, altoMonitor * .045);

        cajaFiltros2.getChildren().clear();
        cajaFiltros2.getChildren().addAll(autocompletarLista, campoReferencia);

        validador.setMaXCaracteres(40);

        cargarMontosPines();

    }

    public int getMontoTelevia() {
        return montoPagoTelevia;
    }
    
    public int getMontoPines() {
        return montoPagoPines;
    }

    private void cargarMontosTelevia() {
        SION.log(Modulo.PAGO_SERVICIOS, "Cargando montos disponibles para emisor Televia", Level.INFO);

        campoPago.setDisable(false);

        String cadenaMontos = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "MONTOS.TELEVIA");
        if (!cadenaMontos.isEmpty()) {
            String montos[] = cadenaMontos.split(",");

            for (int i = 0; i < montos.length; i++) {
                listaMontosTelevia.add(montos[i]);
            }
        }
    }
    
    
     ConsultaComisionDtoResp montosDet = null;
    private void cargarMontosPines() {
        SION.log(Modulo.PAGO_SERVICIOS, "Cargando montos disponibles para el Pin Seleccionado", Level.INFO);

        campoPago.setDisable(false);
        
        final String strEmisor = binding.buscaIdXNombre(getCampoEmisor().getText());
        
        
        cargarLoading();
        
        Service serviceTmp = new Service() {

                       @Override
                        protected Task createTask() {
                            return new Task() {

                               @Override
                                protected Object call() {

                                   montosDet = binding.consultaComisionXidEmisor(Long.valueOf(strEmisor));
                                   return "exitoso2";
                               }
                            };
                        }
                    };
        
        

        
        
        
        
        serviceTmp.stateProperty().addListener(new ChangeListener<Worker.State>() {

                       @Override
                        public void changed(ObservableValue<? extends Worker.State> state,
                                Worker.State oldValue, Worker.State newValue) {
                            if (newValue == Worker.State.SUCCEEDED) {

                              

                                    if (montosDet!=null){
            
                                        for(DetalleMontoArticuloDto dto : montosDet.getArrayDetalleArticulos()){
                                            listaMontosPines.add(dto.getFcCobroFront());
                                        }
                                        removerImagenLoading();
           
                                    }else {
                                        removerImagenLoading();
                                        AdministraVentanas.mostrarAlertaRetornaFoco("Ocurrio un error al obtener los montos", TipoMensaje.ERROR,comboPines);
                                        
                                       

                                   }

                               
                                


                           } else if (newValue == Worker.State.FAILED) {
                                //InicioPagoFaltantesVista.ocultaLoading();
            removerImagenLoading();
                               AdministraVentanas.mostrarAlertaRetornaFoco("Ocurrio un error al obtener los montos", TipoMensaje.ERROR,comboPines);

                           }

                       }
                    });
                    serviceTmp.start();
        
        

    }
    
    

    public void cargarComponentesAvon() {
        SION.log(Modulo.PAGO_SERVICIOS, "Cargando componentes de vista de emisor AVON", Level.INFO);

        this.vistaImagenValidarPago.setFitHeight(25);
        this.vistaImagenValidarPago.setFitWidth(25);
        botonValidarPago.setGraphic(this.vistaImagenValidarPago);
        botonValidarPago.setContentDisplay(ContentDisplay.LEFT);
        botonValidarPago.setWrapText(true);
        botonValidarPago.getStyleClass().add("buttonValidarPago");
        botonValidarPago.setText("Validar Pago");
        
        
        botonValidarPago.setMaxSize(anchoMonitor * .12, altoMonitor * .05);
        botonValidarPago.setMinSize(anchoMonitor * .12, altoMonitor * .05);
        botonValidarPago.setPrefSize(anchoMonitor * .12, altoMonitor * .05);



        campoPago.setVisible(true);

        cajaFiltros3.getChildren().clear();
        //cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia, vistaImagenPesos,
         //       etiquetaPago, campoPago, botonValidarPago);
        
        cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia,
                etiquetaPago, campoPago, botonValidarPago);
        
        
        
        comboTelevia.setDisable(true);
        comboPines.setDisable(true);

        campoDigitoVerificadorJafra.setVisible(false);
        campoDigitoVerificadorJafra.setText("");

        cajaFiltros2.getChildren().clear();
        cajaVendedorEmisor.getChildren().clear();
        cajaPortadorReferencia.getChildren().clear();

        autocompletarLista.setMaxSize(anchoMonitor * .22, altoMonitor * .044);
        autocompletarLista.setMinSize(anchoMonitor * .22, altoMonitor * .044);
        autocompletarLista.setPrefSize(anchoMonitor * .22, altoMonitor * .044);

        campoReferencia.setMaxSize(anchoMonitor * .13, altoMonitor * .044);
        campoReferencia.setMinSize(anchoMonitor * .13, altoMonitor * .044);
        campoReferencia.setPrefSize(anchoMonitor * .13, altoMonitor * .044);

        cajaFiltros2.getChildren().addAll(cajaVendedorEmisor, cajaPortadorReferencia);

        cajaVendedorEmisor.getChildren().addAll(radioBotonVendedor, autocompletarLista);
        cajaPortadorReferencia.getChildren().addAll(radioBotonPortador, etiquetaReferenciaAyudaAVON, campoReferencia);

        cajaFiltros2.setStyle("-fx-alignment: bottom-center;");
    }

    public void cargarComponentesJafra() {
        SION.log(Modulo.PAGO_SERVICIOS, "Cargando componentes de vista de emisor JAFRA", Level.INFO);

        this.vistaImagenValidarPago.setFitHeight(25);
        this.vistaImagenValidarPago.setFitWidth(25);
        botonValidarPago.setGraphic(this.vistaImagenValidarPago);
        botonValidarPago.setContentDisplay(ContentDisplay.LEFT);
        botonValidarPago.setWrapText(true);
        botonValidarPago.getStyleClass().add("buttonValidarPago");
        botonValidarPago.setText("Validar Pago");
        
        
        
        botonValidarPago.setMaxSize(anchoMonitor * .12, altoMonitor * .05);
        botonValidarPago.setMinSize(anchoMonitor * .12, altoMonitor * .05);
        botonValidarPago.setPrefSize(anchoMonitor * .12, altoMonitor * .05);
        
        

        campoPago.setVisible(true);

        cajaFiltros3.getChildren().clear();
        //cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia, vistaImagenPesos,
        //        etiquetaPago, campoPago, botonValidarPago);
        
        cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia,
                etiquetaPago, campoPago, botonValidarPago);
        
        
        comboTelevia.setDisable(true);
        comboPines.setDisable(true);
        
        cajaFiltros2.getChildren().clear();

        campoReferencia.setMaxSize(anchoMonitor * .26, altoMonitor * .045);
        campoReferencia.setMinSize(anchoMonitor * .26, altoMonitor * .045);
        campoReferencia.setPrefSize(anchoMonitor * .26, altoMonitor * .045);

        autocompletarLista.setMaxSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setMinSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setPrefSize(anchoMonitor * .32, altoMonitor * .045);

        campoDigitoVerificadorJafra.setVisible(true);
        campoDigitoVerificadorJafra.setText("");

        cajaDigitoVerificadorJafra.getChildren().clear();
        cajaDigitoVerificadorJafra.getChildren().addAll(campoReferencia, campoDigitoVerificadorJafra);

        cajaFiltros2.getChildren().addAll(autocompletarLista, cajaDigitoVerificadorJafra);

        cajaFiltros2.setStyle("-fx-alignment: bottom-center;");

    }

    public void cargarComponentesOriflame() {
        SION.log(Modulo.PAGO_SERVICIOS, "Cargando componentes de vista de emisor Oriflame", Level.INFO);

        
        this.vistaImagenValidarPago.setFitHeight(25);
        this.vistaImagenValidarPago.setFitWidth(25);
        botonValidarPago.setGraphic(this.vistaImagenValidarPago);
        botonValidarPago.setContentDisplay(ContentDisplay.LEFT);
        botonValidarPago.setWrapText(true);
        botonValidarPago.getStyleClass().add("buttonValidarPago");
        botonValidarPago.setText("Validar Pago");
        
        botonValidarPago.setMaxSize(anchoMonitor * .16, altoMonitor * .05);
        botonValidarPago.setMinSize(anchoMonitor * .16, altoMonitor * .05);
        botonValidarPago.setPrefSize(anchoMonitor * .16, altoMonitor * .05);

        //Limpiar campos 
        cajaFiltros3.getChildren().clear();
        cajaFiltros2.getChildren().clear();
        campoDigitoVerificadorJafra.setVisible(false);
        campoDigitoVerificadorJafra.setText("");
        comboTelevia.setDisable(true);
        comboPines.setDisable(true);
        
        autocompletarLista.setMaxSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setMinSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setPrefSize(anchoMonitor * .32, altoMonitor * .045);

        campoReferencia.setMaxSize(anchoMonitor * .32, altoMonitor * .045);
        campoReferencia.setMinSize(anchoMonitor * .32, altoMonitor * .045);
        campoReferencia.setPrefSize(anchoMonitor * .32, altoMonitor * .045);
        cajaFiltros2.getChildren().addAll(autocompletarLista, campoReferencia);
        cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia, espacioLimpio,botonValidarPago);
        

        campoPago.setVisible(false);
        
        cajaFiltros2.setStyle("-fx-alignment: center;");

    }
    
    
    public void cargarComponentesDineroExpress() {
        SION.log(Modulo.PAGO_SERVICIOS, "Cargando componentes de vista de emisor Dinero Express", Level.INFO);

        
        this.vistaImagenValidarPago.setFitHeight(25);
        this.vistaImagenValidarPago.setFitWidth(25);
        botonValidarPago.setGraphic(this.vistaImagenValidarPago);
        botonValidarPago.setContentDisplay(ContentDisplay.LEFT);
        botonValidarPago.setWrapText(true);
        botonValidarPago.getStyleClass().add("buttonValidarPago");
        botonValidarPago.setText("Validar Pago");
        
        botonValidarPago.setMaxSize(anchoMonitor * .16, altoMonitor * .05);
        botonValidarPago.setMinSize(anchoMonitor * .16, altoMonitor * .05);
        botonValidarPago.setPrefSize(anchoMonitor * .16, altoMonitor * .05);

        
        //Limpiar campos 
        cajaFiltros3.getChildren().clear();
        cajaFiltros2.getChildren().clear();
        campoDigitoVerificadorJafra.setVisible(false);
        campoDigitoVerificadorJafra.setText("");
        comboTelevia.setDisable(true);
        comboPines.setDisable(true);
        
        
        //cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia, vistaImagenPesos,
        //        botonValidarPago);
        
        autocompletarLista.setMaxSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setMinSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setPrefSize(anchoMonitor * .32, altoMonitor * .045);

        campoReferencia.setMaxSize(anchoMonitor * .32, altoMonitor * .045);
        campoReferencia.setMinSize(anchoMonitor * .32, altoMonitor * .045);
        campoReferencia.setPrefSize(anchoMonitor * .32, altoMonitor * .045);
        
        cajaFiltros2.getChildren().addAll(autocompletarLista, campoReferencia);
        cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia, espacioLimpio,botonValidarPago);
        

        campoPago.setVisible(false);
        
        cajaFiltros2.setStyle("-fx-alignment: center;");

    }

        public void cargarComponentesDefault() {
        SION.log(Modulo.PAGO_SERVICIOS, "Cargando componentes de vista por default", Level.INFO);

        this.vistaImagenValidarPago.setFitHeight(25);
        this.vistaImagenValidarPago.setFitWidth(25);
        botonValidarPago.setGraphic(this.vistaImagenValidarPago);
        botonValidarPago.setContentDisplay(ContentDisplay.LEFT);
        botonValidarPago.setWrapText(true);
        botonValidarPago.getStyleClass().add("buttonValidarPago");
        botonValidarPago.setText("Validar Pago");
        
        
        botonValidarPago.setMaxSize(anchoMonitor * .14, altoMonitor * .05);
        botonValidarPago.setMinSize(anchoMonitor * .14, altoMonitor * .05);
        botonValidarPago.setPrefSize(anchoMonitor * .14, altoMonitor * .05);
        /*vistaImagenValidarPago.setFitWidth(anchoMonitor * .12);
        vistaImagenValidarPago.setFitHeight(altoMonitor * .05);*/

        campoPago.setVisible(true);

        campoDigitoVerificadorJafra.setVisible(false);
        campoDigitoVerificadorJafra.setText("");

        cajaFiltros3.getChildren().clear();
        //cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia, vistaImagenPesos,etiquetaPago, campoPago, botonValidarPago);
        
        cajaFiltros3.getChildren().addAll(vistaImagenAyudaReferencia,etiquetaPago, campoPago, botonValidarPago);
        comboTelevia.setDisable(true);
        comboPines.setDisable(true);

        autocompletarLista.setMaxSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setMinSize(anchoMonitor * .32, altoMonitor * .045);
        autocompletarLista.setPrefSize(anchoMonitor * .32, altoMonitor * .045);

        campoReferencia.setMaxSize(anchoMonitor * .32, altoMonitor * .045);
        campoReferencia.setMinSize(anchoMonitor * .32, altoMonitor * .045);
        campoReferencia.setPrefSize(anchoMonitor * .32, altoMonitor * .045);

        cajaFiltros2.getChildren().clear();
        cajaFiltros2.getChildren().addAll(autocompletarLista, campoReferencia);

        validador.setMaXCaracteres(39);

        cajaFiltros2.setStyle("-fx-alignment: center;");
    }

    public List<ImagenBean> getListaImagenesReferencia() {
        return listaImagenesReferencia;
    }

    public String getRutaReferencias() {
        return RUTA_REFERENCIAS;
    }

    private void renuevaListenersImagenesCarrusel() {
        listenerImagenes(vistaImagenFrecuente1, imagenFrecuente1, imagenFrecuente1H);
        listenerImagenes(vistaImagenFrecuente2, imagenFrecuente2, imagenFrecuente2H);
        listenerImagenes(vistaImagenFrecuente3, imagenFrecuente3, imagenFrecuente3H);
        listenerImagenes(vistaImagenFrecuente4, imagenFrecuente4, imagenFrecuente4H);
        listenerImagenes(vistaImagenFrecuente5, imagenFrecuente5, imagenFrecuente5H);
        listenerImagenes(vistaImagenFrecuente6, imagenFrecuente6, imagenFrecuente6H);
        listenerImagenes(vistaImagenFrecuente7, imagenFrecuente7, imagenFrecuente7H);
        listenerImagenes(vistaImagenFrecuente8, imagenFrecuente8, imagenFrecuente8H);
        listenerImagenes(vistaImagenFrecuente9, imagenFrecuente9, imagenFrecuente9H);
        listenerImagenes(vistaImagenFrecuente10, imagenFrecuente10, imagenFrecuente10H);
    }

    private void recorrerAnteriorFrecuente() {

        //valida que haya mas de 10 emisores para poder hacer un recorrido
        if (binding.getListaEmpresasCabecera().size() > 10) {

            //SION.log(Modulo.PAGO_SERVICIOS, "Lista de cabeceros a evaluar: " + binding.getListaEmpresasCabecera().toString(), Level.INFO);

            //se toman Id de los extremos de la lista
            String imagenIdIni = vistaImagenFrecuente10.getId();
            String imagenIdFin = vistaImagenFrecuente1.getId();
            //SION.log(Modulo.PAGO_SERVICIOS, "ID inicial: " + imagenIdIni, Level.INFO);
            //SION.log(Modulo.PAGO_SERVICIOS, "Id final: " + imagenIdFin, Level.INFO);

            ArrayList<EmpresaBean> listaCabecerosTemporal = new ArrayList<EmpresaBean>();

            boolean seDebeAgregarEmisoraImagen = false;
            boolean seDebeCargarNuevoEmisor = false;
            boolean seHanCompletadoNuevosEmisores = false;
            int contadorEmisoresNuevosCargados = 0;

            if (movimientosPorRealizar == 0 || movimientosPorRealizar > 10) {
                imagenesEnMemoriaPorRecorrer = 1;
            }

            //se valida si los movimeinto a realizar son  menores a 10, en este caso primero se recorren las imagenes que ya setienen en memoria para ahorrar tiempo
            if (movimientosPorRealizar < 10) {
                imagenesEnMemoriaPorRecorrer = 10 - movimientosPorRealizar;
                //SION.log(Modulo.PAGO_SERVICIOS, "Imagenes por recorrer: " + imagenesEnMemoriaPorRecorrer, Level.INFO);
            }

            //recorre las imagenes antes de comenzar a cargar nuevas
            if (imagenesEnMemoriaPorRecorrer > 0) {
                for (int i = 0; i < movimientosPorRealizar; i++) {
                    //SION.log(Modulo.PAGO_SERVICIOS, "Recorriendo: "+i, Level.INFO);
                    
                    imagenFrecuente10 = imagenFrecuente9;
                    imagenFrecuente10H = imagenFrecuente9H;
                    imagenFrecuente9 = imagenFrecuente8;
                    imagenFrecuente9H = imagenFrecuente8H;
                    imagenFrecuente8 = imagenFrecuente7;
                    imagenFrecuente8H = imagenFrecuente7H;
                    imagenFrecuente7 = imagenFrecuente6;
                    imagenFrecuente7H = imagenFrecuente6H;
                    imagenFrecuente6 = imagenFrecuente5;
                    imagenFrecuente6H = imagenFrecuente5H;
                    imagenFrecuente5 = imagenFrecuente4;
                    imagenFrecuente5H = imagenFrecuente4H;
                    imagenFrecuente4 = imagenFrecuente3;
                    imagenFrecuente4H = imagenFrecuente3H;
                    imagenFrecuente3 = imagenFrecuente2;
                    imagenFrecuente3H = imagenFrecuente2H;
                    imagenFrecuente2 = imagenFrecuente1;
                    imagenFrecuente2H = imagenFrecuente1H;

                    vistaImagenFrecuente10.setImage(imagenFrecuente10);
                    vistaImagenFrecuente10.setId(vistaImagenFrecuente9.getId());
                    vistaImagenFrecuente9.setImage(imagenFrecuente9);
                    vistaImagenFrecuente9.setId(vistaImagenFrecuente8.getId());
                    vistaImagenFrecuente8.setImage(imagenFrecuente8);
                    vistaImagenFrecuente8.setId(vistaImagenFrecuente7.getId());
                    vistaImagenFrecuente7.setImage(imagenFrecuente7);
                    vistaImagenFrecuente7.setId(vistaImagenFrecuente6.getId());
                    vistaImagenFrecuente6.setImage(imagenFrecuente6);
                    vistaImagenFrecuente6.setId(vistaImagenFrecuente5.getId());
                    vistaImagenFrecuente5.setImage(imagenFrecuente5);
                    vistaImagenFrecuente5.setId(vistaImagenFrecuente4.getId());
                    vistaImagenFrecuente4.setImage(imagenFrecuente4);
                    vistaImagenFrecuente4.setId(vistaImagenFrecuente3.getId());
                    vistaImagenFrecuente3.setImage(imagenFrecuente3);
                    vistaImagenFrecuente3.setId(vistaImagenFrecuente2.getId());
                    vistaImagenFrecuente2.setImage(imagenFrecuente2);
                    vistaImagenFrecuente2.setId(vistaImagenFrecuente1.getId());
                    
                }
            }
            
            Collections.reverse(binding.getListaEmpresasCabecera());
            //SION.log(Modulo.PAGO_SERVICIOS, "Lista inversa: "+binding.getListaEmpresasCabecera().toString(), Level.INFO);

            for (EmpresaBean empresa : binding.getListaEmpresasCabecera()) {

                //validar bandera de carga de imagen de emisor, la primera vez nunca entra
                if (seDebeAgregarEmisoraImagen) {
                    //SION.log(Modulo.PAGO_SERVICIOS, "nuevo emisor a cargar: " + empresa.toString(), Level.INFO);
                    listaCabecerosTemporal.add(empresa);
                    //SION.log(Modulo.PAGO_SERVICIOS, "nuevo tamaño lista temporal: " + listaCabecerosTemporal.size(), Level.INFO);
                    contadorEmisoresNuevosCargados++;
                }

                //primero localizar ultima imagen dentro de la lista de cabeceros
                if (imagenIdFin.equals(String.valueOf(empresa.getEmpresaId()))) {
                    //una vez encontrado el ultimo emisor del cual existe imagen cargada podremos empezar a cargas las nueva
                    //SION.log(Modulo.PAGO_SERVICIOS, "Ultimo emisor encontrado: " + empresa.toString(), Level.INFO);
                    seDebeAgregarEmisoraImagen = true;
                }

                //valida que la cantidad de emisores cargados 
                if (contadorEmisoresNuevosCargados == movimientosPorRealizar) {

                    seHanCompletadoNuevosEmisores = true;

                }

                //bandera para salir del bucle
                if (seHanCompletadoNuevosEmisores) {
                    System.out.println("saliendo del bucle");
                    break;
                }
            }

            //validar que se hayan obtenido los ID de las nuevas 10 imagenes a cargar
            //sino es asi tomar del principio de la lista
            if (!seHanCompletadoNuevosEmisores) {
                //SION.log(Modulo.PAGO_SERVICIOS, "no se completaron emisores, cargando del principio de la lista", Level.INFO);
                for (EmpresaBean empresa : binding.getListaEmpresasCabecera()) {
                    //SION.log(Modulo.PAGO_SERVICIOS, "cargando emisor: " + empresa.toString(), Level.INFO);
                    listaCabecerosTemporal.add(empresa);
                    if (listaCabecerosTemporal.size() == 10) {
                        //SION.log(Modulo.PAGO_SERVICIOS, "emisores completos ahora si, saliendo del bucle", Level.INFO);
                        break;
                    }
                }
            }
            
            Collections.reverse(binding.getListaEmpresasCabecera());
            
            //SION.log(Modulo.PAGO_SERVICIOS, "Lista de cabeceros: "+listaCabecerosTemporal.toString(), Level.INFO);

            //cargar id de las imagenes con la lista temporal creada

            int indiceImagenesNuevas = 0;

            if (imagenesEnMemoriaPorRecorrer < 1) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Seteando "+listaCabecerosTemporal.get(indiceImagenesNuevas).getDescrpcion()+" en posicion 10", Level.INFO);
                imagenFrecuente10 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente10H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente10.setImage(imagenFrecuente10);
                vistaImagenFrecuente10.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 2) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Seteando "+listaCabecerosTemporal.get(indiceImagenesNuevas).getDescrpcion()+" en posicion 9", Level.INFO);
                imagenFrecuente9 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente9H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente9.setImage(imagenFrecuente9);
                vistaImagenFrecuente9.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 3) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Seteando "+listaCabecerosTemporal.get(indiceImagenesNuevas).getDescrpcion()+" en posicion 8", Level.INFO);
                imagenFrecuente8 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente8H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente8.setImage(imagenFrecuente8);
                vistaImagenFrecuente8.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 4) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Seteando "+listaCabecerosTemporal.get(indiceImagenesNuevas).getDescrpcion()+" en posicion 7", Level.INFO);
                imagenFrecuente7 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente7H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente7.setImage(imagenFrecuente7);
                vistaImagenFrecuente7.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 5) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Seteando "+listaCabecerosTemporal.get(indiceImagenesNuevas).getDescrpcion()+" en posicion 6", Level.INFO);
                imagenFrecuente6 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente6H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente6.setImage(imagenFrecuente6);
                vistaImagenFrecuente6.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 6) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Seteando "+listaCabecerosTemporal.get(indiceImagenesNuevas).getDescrpcion()+" en posicion 5", Level.INFO);
                imagenFrecuente5 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente5H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente5.setImage(imagenFrecuente5);
                vistaImagenFrecuente5.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 7) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Seteando "+listaCabecerosTemporal.get(indiceImagenesNuevas).getDescrpcion()+" en posicion 4", Level.INFO);
                imagenFrecuente4 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente4H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente4.setImage(imagenFrecuente4);
                vistaImagenFrecuente4.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 8) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Seteando "+listaCabecerosTemporal.get(indiceImagenesNuevas).getDescrpcion()+" en posicion 3", Level.INFO);
                imagenFrecuente3 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente3H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente3.setImage(imagenFrecuente3);
                vistaImagenFrecuente3.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 9) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Seteando "+listaCabecerosTemporal.get(indiceImagenesNuevas).getDescrpcion()+" en posicion 2", Level.INFO);
                imagenFrecuente2 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente2H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente2.setImage(imagenFrecuente2);
                vistaImagenFrecuente2.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 10) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Seteando "+listaCabecerosTemporal.get(indiceImagenesNuevas).getDescrpcion()+" en posicion 1", Level.INFO);
                imagenFrecuente1 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente1H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente1.setImage(imagenFrecuente1);
                vistaImagenFrecuente1.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            
            vistaImagenFrecuente1.requestFocus();
            vistaImagenFrecuente2.requestFocus();
            vistaImagenFrecuente3.requestFocus();
            vistaImagenFrecuente4.requestFocus();
            vistaImagenFrecuente5.requestFocus();
            vistaImagenFrecuente6.requestFocus();
            vistaImagenFrecuente7.requestFocus();
            vistaImagenFrecuente8.requestFocus();
            vistaImagenFrecuente9.requestFocus();
            vistaImagenFrecuente10.requestFocus();
            botonAnterior.requestFocus();
            
            renuevaListenersImagenesCarrusel();

        }

    }

    private void recorrerSiguienteFrecuente  () {

        //valida que haya mas de 10 emisores para poder hacer un recorrido
        if (binding.getListaEmpresasCabecera().size() > 10) {

            SION.log(Modulo.PAGO_SERVICIOS, "Lista de cabeceros a evaluar: " + binding.getListaEmpresasCabecera().toString(), Level.INFO);

            //se toman Id de los extremos de la lista
            String imagenIdIni = vistaImagenFrecuente1.getId();
            String imagenIdFin = vistaImagenFrecuente10.getId();
            //SION.log(Modulo.PAGO_SERVICIOS, "ID inicial: " + imagenIdIni, Level.INFO);
            //SION.log(Modulo.PAGO_SERVICIOS, "Id final: " + imagenIdFin, Level.INFO);

            ArrayList<EmpresaBean> listaCabecerosTemporal = new ArrayList<EmpresaBean>();

            boolean seDebeAgregarEmisoraImagen = false;
            boolean seDebeCargarNuevoEmisor = false;
            boolean seHanCompletadoNuevosEmisores = false;
            int contadorEmisoresNuevosCargados = 0;

            if (movimientosPorRealizar == 0 || movimientosPorRealizar > 10) {
                imagenesEnMemoriaPorRecorrer = 1;
            }

            //se valida si los movimeinto a realizar son  menores a 10, en este caso primero se recorren las imagenes que ya setienen en memoria para ahorrar tiempo
            if (movimientosPorRealizar < 10) {
                imagenesEnMemoriaPorRecorrer = 10 - movimientosPorRealizar;
                //SION.log(Modulo.PAGO_SERVICIOS, "Imagenes por recorrer: " + imagenesEnMemoriaPorRecorrer, Level.INFO);
            }

            //recorre las imagenes antes de comenzar a cargar nuevas
            if (imagenesEnMemoriaPorRecorrer > 0) {
                for (int i = 0; i < movimientosPorRealizar; i++) {
                    imagenFrecuente1 = imagenFrecuente2;
                    imagenFrecuente1H = imagenFrecuente2H;
                    imagenFrecuente2 = imagenFrecuente3;
                    imagenFrecuente2H = imagenFrecuente3H;
                    imagenFrecuente3 = imagenFrecuente4;
                    imagenFrecuente3H = imagenFrecuente4H;
                    imagenFrecuente4 = imagenFrecuente5;
                    imagenFrecuente4H = imagenFrecuente5H;
                    imagenFrecuente5 = imagenFrecuente6;
                    imagenFrecuente5H = imagenFrecuente6H;
                    imagenFrecuente6 = imagenFrecuente7;
                    imagenFrecuente6H = imagenFrecuente7H;
                    imagenFrecuente7 = imagenFrecuente8;
                    imagenFrecuente7H = imagenFrecuente8H;
                    imagenFrecuente8 = imagenFrecuente9;
                    imagenFrecuente8H = imagenFrecuente9H;
                    imagenFrecuente9 = imagenFrecuente10;
                    imagenFrecuente9H = imagenFrecuente10H;

                    vistaImagenFrecuente1.setImage(imagenFrecuente1);
                    vistaImagenFrecuente1.setId(vistaImagenFrecuente2.getId());
                    vistaImagenFrecuente2.setImage(imagenFrecuente2);
                    vistaImagenFrecuente2.setId(vistaImagenFrecuente3.getId());
                    vistaImagenFrecuente3.setImage(imagenFrecuente3);
                    vistaImagenFrecuente3.setId(vistaImagenFrecuente4.getId());
                    vistaImagenFrecuente4.setImage(imagenFrecuente4);
                    vistaImagenFrecuente4.setId(vistaImagenFrecuente5.getId());
                    vistaImagenFrecuente5.setImage(imagenFrecuente5);
                    vistaImagenFrecuente5.setId(vistaImagenFrecuente6.getId());
                    vistaImagenFrecuente6.setImage(imagenFrecuente6);
                    vistaImagenFrecuente6.setId(vistaImagenFrecuente7.getId());
                    vistaImagenFrecuente7.setImage(imagenFrecuente7);
                    vistaImagenFrecuente7.setId(vistaImagenFrecuente8.getId());
                    vistaImagenFrecuente8.setImage(imagenFrecuente8);
                    vistaImagenFrecuente8.setId(vistaImagenFrecuente9.getId());
                    vistaImagenFrecuente9.setImage(imagenFrecuente9);
                    vistaImagenFrecuente9.setId(vistaImagenFrecuente10.getId());
                }
            }

            for (EmpresaBean empresa : binding.getListaEmpresasCabecera()) {

                //validar bandera de carga de imagen de emisor, la primera vez nunca entra
                if (seDebeAgregarEmisoraImagen) {
                    //SION.log(Modulo.PAGO_SERVICIOS, "nuevo emisor a cargar: " + empresa.toString(), Level.INFO);
                    listaCabecerosTemporal.add(empresa);
                    //SION.log(Modulo.PAGO_SERVICIOS, "nuevo tamaño lista temporal: " + listaCabecerosTemporal.size(), Level.INFO);
                    contadorEmisoresNuevosCargados++;
                }

                //primero localizar ultima imagen dentro de la lista de cabeceros
                if (imagenIdFin.equals(String.valueOf(empresa.getEmpresaId()))) {
                    //una vez encontrado el ultimo emisor del cual existe imagen cargada podremos empezar a cargas las nueva
                    //SION.log(Modulo.PAGO_SERVICIOS, "Ultimo emisor encontrado: " + empresa.toString(), Level.INFO);
                    seDebeAgregarEmisoraImagen = true;
                }

                //valida que la cantidad de emisores cargados 
                if (contadorEmisoresNuevosCargados == movimientosPorRealizar) {

                    seHanCompletadoNuevosEmisores = true;

                }

                //bandera para salir del bucle
                if (seHanCompletadoNuevosEmisores) {
                    //System.out.println("saliendo del bucle");
                    break;
                }
            }

            //validar que se hayan obtenido los ID de las nuevas 10 imagenes a cargar
            //sino es asi tomar del principio de la lista
            if (!seHanCompletadoNuevosEmisores) {
                //SION.log(Modulo.PAGO_SERVICIOS, "no se completaron emisores, cargando del principio de la lista", Level.INFO);
                for (EmpresaBean empresa : binding.getListaEmpresasCabecera()) {
                    //SION.log(Modulo.PAGO_SERVICIOS, "cargando emisor: " + empresa.toString(), Level.INFO);
                    listaCabecerosTemporal.add(empresa);
                    if (listaCabecerosTemporal.size() == 10) {
                        //SION.log(Modulo.PAGO_SERVICIOS, "emisores completos ahora si, saliendo del bucle", Level.INFO);
                        break;
                    }
                }
            }

            //cargar id de las imagenes con la lista temporal creada

            int indiceImagenesNuevas = 0;

            if (imagenesEnMemoriaPorRecorrer < 1) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Recorriendo 1", Level.INFO);
                imagenFrecuente1 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente1H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente1.setImage(imagenFrecuente1);
                vistaImagenFrecuente1.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 2) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Recorriendo 2", Level.INFO);
                imagenFrecuente2 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente2H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente2.setImage(imagenFrecuente2);
                vistaImagenFrecuente2.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 3) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Recorriendo 3", Level.INFO);
                imagenFrecuente3 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente3H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente3.setImage(imagenFrecuente3);
                vistaImagenFrecuente3.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 4) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Recorriendo 4", Level.INFO);
                imagenFrecuente4 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente4H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente4.setImage(imagenFrecuente4);
                vistaImagenFrecuente4.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 5) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Recorriendo 5", Level.INFO);
                imagenFrecuente5 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente5H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente5.setImage(imagenFrecuente5);
                vistaImagenFrecuente5.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 6) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Recorriendo 6", Level.INFO);
                imagenFrecuente6 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente6H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente6.setImage(imagenFrecuente6);
                vistaImagenFrecuente6.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 7) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Recorriendo 7", Level.INFO);
                imagenFrecuente7 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente7H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente7.setImage(imagenFrecuente7);
                vistaImagenFrecuente7.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 8) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Recorriendo 8", Level.INFO);
                imagenFrecuente8 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente8H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente8.setImage(imagenFrecuente8);
                vistaImagenFrecuente8.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 9) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Recorriendo 9", Level.INFO);
                imagenFrecuente9 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente9H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente9.setImage(imagenFrecuente9);
                vistaImagenFrecuente9.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            if (imagenesEnMemoriaPorRecorrer < 10) {
                //SION.log(Modulo.PAGO_SERVICIOS, "Recorriendo 10", Level.INFO);
                imagenFrecuente10 = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), false);
                imagenFrecuente10H = importarImagenLoc(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()), true);
                vistaImagenFrecuente10.setImage(imagenFrecuente10);
                vistaImagenFrecuente10.setId(String.valueOf(listaCabecerosTemporal.get(indiceImagenesNuevas).getEmpresaId()));
                indiceImagenesNuevas++;
            }
            
            
            
            
            renuevaListenersImagenesCarrusel();

        }

    }

    private void creaHandlers() {

        SION.log(Modulo.PAGO_SERVICIOS, "Creando handlers", Level.INFO);

        botonAnterior.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {

                SION.log(Modulo.PAGO_SERVICIOS, "Boton anterior presionado", Level.INFO);

                recorrerAnteriorFrecuente();

            }
        });

        botonSiguiente.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {

                SION.log(Modulo.PAGO_SERVICIOS, "Boton siguiente presionado", Level.INFO);

                recorrerSiguienteFrecuente();

            }
        });

        panelPrincipal.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (f3.match(arg0)) {
                    SION.log(Modulo.PAGO_SERVICIOS, "F3 presionado", Level.INFO);
                    recorrerAnteriorFrecuente();
                } else if (f4.match(arg0)) {
                    SION.log(Modulo.PAGO_SERVICIOS, "F4 presionado", Level.INFO);
                    recorrerSiguienteFrecuente();
                } else if (f5.match(arg0)) {
                    try {
                        /*
                         * SION.log(Modulo.PAGO_SERVICIOS,"F5 presionado",
                         * Level.INFO); OriflameVista ori = new
                         * OriflameVista(utilerias, binding.getVistaModelo(),
                         * EFECTIVO); ori.abreStageOriflame("<PagosCliente>" +
                         * "<Cabecero>No.Pedido|Monto|Fecha</Cabecero>" +
                         * "<General>2|3|1|2|1</General>"
                         * +"<Pagos>7099051695|711.5|27/05/2011,7099052431|848.74|10/06/2011</Pagos></PagosCliente>",
                         * 0, null);
                         *
                         */
                        /*
                         * RespuestaReferenciaDto r = new
                         * RespuestaReferenciaDto(); r.setNombreGrupo("Los
                         * superchonicos"); MAZVista maz = new
                         * MAZVista(utilerias, EFECTIVO,
                         * binding.getVistaModelo()); maz.abreStageMAZ(r, 0);
                         */
                        /*
                         * ServicioBean[] servicios = new ServicioBean[1];
                         *
                         * ServicioBean servicio = new ServicioBean(true, "", 1,
                         * "TELMEX", EFECTIVO, 0, 150, "7123786600004123", 0,
                         * ""); servicios[0] = servicio;
                         *
                         * ConfirmacionPagoVista confirmacion = new
                         * ConfirmacionPagoVista(binding,utilerias);
                         * confirmacion.abreStageConfirmacion(servicios,EFECTIVO);
                         */
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

    }
    

    
    private void cargarTodosLosServicios(){
        
        SION.log(Modulo.PAGO_SERVICIOS, "Se construyen los botones de todos los servicios disponibles", Level.INFO);
        if (binding.getListaEmpresas()!=null){
            
            int count = 0;

            for (EmpresaBean bean : binding.getListaEmpresas()) {
                if (bean.getEmpresaId()!=26 && bean.getTipoServicio()!=2){
                    
                    count ++;
                    imgServicio = null;
                    imgServicioH = null;
                    vistaImgServicio= null;
                    
                    
                    imgServicio = importarImagenLoc(String.valueOf(bean.getEmpresaId()), false);
                    imgServicioH = importarImagenLoc(String.valueOf(bean.getEmpresaId()), true);
                    
                    
                    vistaImgServicio = new ImageView(imgServicio);
                    vistaImgServicio.setId(String.valueOf(bean.getEmpresaId()));
                    ajustarImagen(vistaImgServicio, anchoMonitor * .078, altoMonitor * .053);
                    listenerImagenes(vistaImgServicio, imgServicio, imgServicioH);
            
                
                    fPaneEmisores.getChildren().add(vistaImgServicio);
                
                
                    if (count == 11){
                        Separator separador = new Separator();
                        //lblSeparador.setText(" ");
                        separador.setPrefWidth(anchoMonitor * .96);
                        separador.setPrefHeight(20);
                        fPaneEmisores.getChildren().add(separador);
                    }   
                }
                
            }
            
            
            
        }
    }
    
    
    
    private void cargarEmisoresPines(){
        
        SION.log(Modulo.PAGO_SERVICIOS, "Se construyen los botones de los Emisores de Pines", Level.INFO);
        if (binding.getListaEmpresas()!=null){
            
            int count = 0;

            for (EmpresaBean bean : binding.getListaEmpresas()) {
                if (bean.getEmpresaId()!=26 && bean.getTipoServicio()!=1){
                    
                    count ++;
                    imgServicio = null;
                    imgServicioH = null;
                    vistaImgServicio= null;
                    
                    
                    imgServicio = importarImagenLoc(String.valueOf(bean.getEmpresaId()), false);
                    imgServicioH = importarImagenLoc(String.valueOf(bean.getEmpresaId()), true);
                    
                    
                    vistaImgServicio = new ImageView(imgServicio);
                    vistaImgServicio.setId(String.valueOf(bean.getEmpresaId()));
                    ajustarImagen(vistaImgServicio, anchoMonitor * .078, altoMonitor * .053);
                    listenerImagenes(vistaImgServicio, imgServicio, imgServicioH);
            
                
                    fPanePines.getChildren().add(vistaImgServicio);
                
                
                    if (count == 11){
                        Separator separador = new Separator();
                        //lblSeparador.setText(" ");
                        separador.setPrefWidth(anchoMonitor * .96);
                        separador.setPrefHeight(20);
                        fPanePines.getChildren().add(separador);
                    }   
                }
                
            }
            
            
            
        }
    }
    
    
    
    
    
    

    private void cargarServiciosFrecuentes() {

        binding.cargarListaServiciosFrecuentes();

        if (binding.getListaNombresServiciosfrecuentes() != null) {

            int contador = 0;
            for (String s : binding.getListaNombresServiciosfrecuentes()) {
                Image imagen = null;
                boolean seCargoImagen = false;
                if (s != null) {
                    switch (contador) {
                        case 0:
                            imagenFrecuente1 = importarImagenLoc(s, false);
                            imagenFrecuente1H = importarImagenLoc(s, true);
                            vistaImagenFrecuente1 = new ImageView(imagenFrecuente1);
                            vistaImagenFrecuente1.setId(s);
                            ajustarImagen(vistaImagenFrecuente1, anchoMonitor * .085, altoMonitor * .06);
                            break;
                        case 1:
                            imagenFrecuente2 = importarImagenLoc(s, false);
                            imagenFrecuente2H = importarImagenLoc(s, true);
                            vistaImagenFrecuente2 = new ImageView(imagenFrecuente2);
                            vistaImagenFrecuente2.setId(s);
                            ajustarImagen(vistaImagenFrecuente2, anchoMonitor * .085, altoMonitor * .06);
                            break;
                        case 2:
                            imagenFrecuente3 = importarImagenLoc(s, false);
                            imagenFrecuente3H = importarImagenLoc(s, true);
                            vistaImagenFrecuente3 = new ImageView(imagenFrecuente3);
                            vistaImagenFrecuente3.setId(s);
                            ajustarImagen(vistaImagenFrecuente3, anchoMonitor * .085, altoMonitor * .06);
                            break;
                        case 3:
                            imagenFrecuente4 = importarImagenLoc(s, false);
                            imagenFrecuente4H = importarImagenLoc(s, true);
                            vistaImagenFrecuente4 = new ImageView(imagenFrecuente4);
                            vistaImagenFrecuente4.setId(s);
                            ajustarImagen(vistaImagenFrecuente4, anchoMonitor * .085, altoMonitor * .06);
                            break;
                        case 4:
                            imagenFrecuente5 = importarImagenLoc(s, false);
                            imagenFrecuente5H = importarImagenLoc(s, true);
                            vistaImagenFrecuente5 = new ImageView(imagenFrecuente5);
                            vistaImagenFrecuente5.setId(s);
                            ajustarImagen(vistaImagenFrecuente5, anchoMonitor * .085, altoMonitor * .06);
                            break;
                        case 5:
                            imagenFrecuente6 = importarImagenLoc(s, false);
                            imagenFrecuente6H = importarImagenLoc(s, true);
                            vistaImagenFrecuente6 = new ImageView(imagenFrecuente6);
                            vistaImagenFrecuente6.setId(s);
                            ajustarImagen(vistaImagenFrecuente6, anchoMonitor * .085, altoMonitor * .06);
                            break;
                        case 6:
                            imagenFrecuente7 = importarImagenLoc(s, false);
                            imagenFrecuente7H = importarImagenLoc(s, true);
                            vistaImagenFrecuente7 = new ImageView(imagenFrecuente7);
                            vistaImagenFrecuente7.setId(s);
                            ajustarImagen(vistaImagenFrecuente7, anchoMonitor * .085, altoMonitor * .06);
                            break;
                        case 7:
                            imagenFrecuente8 = importarImagenLoc(s, false);
                            imagenFrecuente8H = importarImagenLoc(s, true);
                            vistaImagenFrecuente8 = new ImageView(imagenFrecuente8);
                            vistaImagenFrecuente8.setId(s);
                            ajustarImagen(vistaImagenFrecuente8, anchoMonitor * .085, altoMonitor * .06);
                            break;
                        case 8:
                            imagenFrecuente9 = importarImagenLoc(s, false);
                            imagenFrecuente9H = importarImagenLoc(s, true);
                            vistaImagenFrecuente9 = new ImageView(imagenFrecuente9);
                            vistaImagenFrecuente9.setId(s);
                            ajustarImagen(vistaImagenFrecuente9, anchoMonitor * .085, altoMonitor * .06);
                            break;
                        case 9:
                            imagenFrecuente10 = importarImagenLoc(s, false);
                            imagenFrecuente10H = importarImagenLoc(s, true);
                            vistaImagenFrecuente10 = new ImageView(imagenFrecuente10);
                            vistaImagenFrecuente10.setId(s);
                            ajustarImagen(vistaImagenFrecuente10, anchoMonitor * .085, altoMonitor * .06);
                            break;
                    }

                }
                contador++;
            }

            vistaImagenFrecuenteAnt.setFitWidth(anchoMonitor * .03);
            vistaImagenFrecuenteAnt.setFitHeight(altoMonitor * .03);

            vistaImagenFrecuenteSig.setFitWidth(anchoMonitor * .03);
            vistaImagenFrecuenteSig.setFitHeight(altoMonitor * .03);

            botonAnterior.setMaxSize(anchoMonitor * .03, altoMonitor * .03);
            botonAnterior.setMinSize(anchoMonitor * .03, altoMonitor * .03);
            botonAnterior.setPrefSize(anchoMonitor * .03, altoMonitor * .03);

            botonSiguiente.setMaxSize(anchoMonitor * .03, altoMonitor * .03);
            botonSiguiente.setMinSize(anchoMonitor * .03, altoMonitor * .03);
            botonSiguiente.setPrefSize(anchoMonitor * .03, altoMonitor * .03);

            cajaEmisoresFrecuentes.getChildren().clear();

            cajaEmisoresFrecuentes.getChildren().clear();


            cajaEmisoresFrecuentes.getChildren().add(botonAnterior);

            cargaImagen(vistaImagenFrecuente1);
            cargaImagen(vistaImagenFrecuente2);
            cargaImagen(vistaImagenFrecuente3);
            cargaImagen(vistaImagenFrecuente4);
            cargaImagen(vistaImagenFrecuente5);
            cargaImagen(vistaImagenFrecuente6);
            cargaImagen(vistaImagenFrecuente7);
            cargaImagen(vistaImagenFrecuente8);
            cargaImagen(vistaImagenFrecuente9);
            cargaImagen(vistaImagenFrecuente10);

            cajaEmisoresFrecuentes.getChildren().add(botonSiguiente);
        }

    }

    private void cargaImagen(ImageView _imagen) {
        if (_imagen != null) {
            cajaEmisoresFrecuentes.getChildren().add(_imagen);
        }
    }

    private List<ImagenBean> obtieneImagenes(String _ruta) {

        SION.log(Modulo.PAGO_SERVICIOS, "Cargando imagernes en ruta: " + _ruta, Level.INFO);
        List<ImagenBean> imagenes = new ArrayList<ImagenBean>();
        File arc;

        try {
            arc = new File(_ruta);
            String[] archivos = arc.list();
            if (archivos != null && archivos.length > 0) {
                for (String archivo : archivos) {
                    File file = new File(_ruta + File.separator + archivo);
                    if (file.isDirectory()) {
                        carpeta = archivo;
                        imagenes.addAll(obtieneImagenes(_ruta + File.separator + archivo));
                    } else {
                        imagenes.add(new ImagenBean(carpeta, archivo, file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator)).concat(File.separator)));
                    }
                }
            }
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex);
        }

        return imagenes;
    }
    
    
   

    public Image getImagen(String path) {

        String loadedPath = "";
        Image img = null;
        File imageFile;
        ImageView imagen = new ImageView();

        System.gc();
        if (!loadedPath.equals(path)) {
            img = null;
            imageFile = null;
            System.gc();
            try {
                imageFile = new File(path);
                loadedPath = path;
            } catch (Exception ex) {
                SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex);
            } catch (Error err) {
                SION.log(Modulo.PAGO_SERVICIOS, "ocurrio un error al crear imagen: " + err.toString(), Level.SEVERE);
            }
            img = new Image(imageFile.toURI().toString());
        }

        return img;
    }
    
    
    private Image importarImagenLoc(String _nombre, boolean esHover) {
        boolean seCargoImagen = false;
        Image imagen = null;
        String r = "";

        try {
            /*if (esHover) {
                for (ImagenBean bean : listaImagenesFrecuentes) {
                    if (bean != null) {
                        r = _nombre.concat("_B.png");
                        if (bean.getNombre().trim().equals(r.trim())) {
                            seCargoImagen = true;
                            //imagen = getImagen(RUTA_FRECUENTES.concat(_nombre).concat("_B.png"));
                            imagen = utilerias.getImagen(rutaImgsLoc.concat(_nombre).concat("_B.png"));
                        }
                    }
                }
            } else {
                for (ImagenBean bean : listaImagenesFrecuentes) {
                    if (bean != null) {
                        r = _nombre.concat("_A.png");
                        if (bean.getNombre().trim().equals(r.trim())) {
                            seCargoImagen = true;
                            //imagen = getImagen(RUTA_FRECUENTES.concat(_nombre).concat("_A.png"));
                            imagen = utilerias.getImagen(rutaImgsLoc.concat(_nombre).concat("_A.png"));
                        }
                    }
                }
            }*/
            
            
            if (esHover) {
                r = _nombre.concat("_B.png");
                imagen = utilerias.getImagen(rutaImgsLoc.concat(_nombre).concat("_B.png"));
                seCargoImagen = true;
                    
            } else {
                r = _nombre.concat("_A.png");
                imagen = utilerias.getImagen(rutaImgsLoc.concat(_nombre).concat("_A.png"));
                seCargoImagen = true;
            }
            
            
            
        } catch (Exception e) {
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un error al importar imagen en JAR", Level.SEVERE);
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
            
            
        }

        if (!seCargoImagen) {
            
            //imagen = importarImagenFrecuente(_nombre, esHover);
            
            imagen = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/logoNeto.png");
        }
        return imagen;
    }

    private Image importarImagenFrecuente(String _nombre, boolean esHover) {
        boolean seCargoImagen = false;
        Image imagen = null;
        String r = "";

        try {
            if (esHover) {
                for (ImagenBean bean : listaImagenesFrecuentes) {
                    if (bean != null) {
                        r = _nombre.concat("_B.png");
                        if (bean.getNombre().trim().equals(r.trim())) {
                            seCargoImagen = true;
                            imagen = getImagen(RUTA_FRECUENTES.concat(_nombre).concat("_B.png"));
                        }
                    }
                }
            } else {
                for (ImagenBean bean : listaImagenesFrecuentes) {
                    if (bean != null) {
                        r = _nombre.concat("_A.png");
                        if (bean.getNombre().trim().equals(r.trim())) {
                            seCargoImagen = true;
                            imagen = getImagen(RUTA_FRECUENTES.concat(_nombre).concat("_A.png"));
                        }
                    }
                }
            }
        } catch (Exception e) {
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un error al importar imagen", Level.SEVERE);
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
        }

        if (!seCargoImagen) {
            imagen = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/logoNeto.png");
        }
        return imagen;
    }

    private void ajustarImagen(ImageView _vistaImagen, double _width, double _height) {
        _vistaImagen.setFitWidth(_width);
        _vistaImagen.setFitHeight(_height);
        _vistaImagen.setStyle("-fx-background-color : #000000;");
    }

    private void revisarServiciosFrecuentes() {

        quitarImagenFrecuente(vistaImagenFrecuente1);
        quitarImagenFrecuente(vistaImagenFrecuente2);
        quitarImagenFrecuente(vistaImagenFrecuente3);
        quitarImagenFrecuente(vistaImagenFrecuente4);
        quitarImagenFrecuente(vistaImagenFrecuente5);
        quitarImagenFrecuente(vistaImagenFrecuente6);
        quitarImagenFrecuente(vistaImagenFrecuente7);
        quitarImagenFrecuente(vistaImagenFrecuente8);
        quitarImagenFrecuente(vistaImagenFrecuente9);
        quitarImagenFrecuente(vistaImagenFrecuente10);

    }

    private void quitarImagenFrecuente(ImageView _imagen) {
        if (!binding.esEmisorActivo(Long.parseLong(_imagen.getId()))) {
            cajaEmisoresFrecuentes.getChildren().remove(_imagen);
        }
    }

    private void binding() {
        SION.log(Modulo.PAGO_SERVICIOS, "Asignando propiedades a columnas", Level.INFO);
        binding.asignaPropiedadBean(columnaEmisor, String.class, "nombreEmisor");
        binding.asignaPropiedadBean(columnaReferencia, String.class, "referencia");
        binding.asignaPropiedadBean(columnaComision, Double.class, "comision");
        binding.asignaPropiedadBean(columnaImporte, String.class, "importe");
        binding.setCallbackImagen(columnaLogo);
        binding.setCallbackCheckBox(columnaCheck, etiquetaMontoVenta);

        binding.setCellFactory(tablaPagosAcumulados, columnaEmisor, String.class, Pos.CENTER_LEFT, true);
        binding.setCellFactory(tablaPagosAcumulados, columnaReferencia, String.class, Pos.CENTER_LEFT, true);
        binding.setCellFactory(tablaPagosAcumulados, columnaImporte, String.class, Pos.CENTER_LEFT, true);

        binding.crearListaAutocompletar(autocompletarLista);
        binding.crearListaAutocompletarOverlay(autocompletarListaOverlay);
        binding.cargarListaPagosAcumulados(tablaPagosAcumulados);
        binding.creaListenersListas(etiquetaMontoVenta, tablaPagosAcumulados);
        binding.creaListenerEventoESC();

    }

    public void validarReferenciasDuplicadas(int _index, String _referencia, boolean esTelevia) {

        //se valida que no exista la misma referencia en la tabla 
        if (binding.esNumeroReferenciaValida(_index, campoReferencia.getText().trim(), esTelevia)) {
            
            /*botonValidarPago.setDisable(true);
            campoReferencia.setDisable(true);
            */
            
            
            if (esTelevia) {
                binding.agregarReferencia(_index, 1, montoPagoTelevia, _referencia);
            } else if (binding.buscaIdXNombre(autocompletarLista.getTextField().getText().trim()).equals(
                    SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.ORIFLAME"))) {
                binding.agregarReferencia(_index, 1, 1, _referencia);
            } else if (binding.buscaIdXNombre(autocompletarLista.getTextField().getText().trim()).equals(
                    SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.DINEROEXP"))) {
                binding.agregarReferencia(_index, 1, 1, _referencia);
            } else {
                binding.agregarReferencia(_index, 1, Double.parseDouble(campoPago.getText()), _referencia);
            }

            campoReferencia.setText("");
            campoPago.setText("");
            autocompletarLista.getTextField().setText("");

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    tablaPagosAcumulados.requestFocus();
                }
            });

        } else {
            /*
             * if( binding.esMontoConDecimales()){
             *
             * AdministraVentanas.mostrarAlertaRetornaFoco(
             * SION.obtenerMensaje(Modulo.PAGO_SERVICIOS,
             * "monto.decimales.maz").trim(), TipoMensaje.ADVERTENCIA,
             * autocompletarLista.getTextField());
             *
             * }else
             */ if (binding.sobrepasaMontoValidoEmisor()) {
                 
                 Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(
                        SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "monto.novalidado").trim(), TipoMensaje.ADVERTENCIA,
                        autocompletarLista.getTextField());
                    }
                });
                 
                
            } else {
                 
                 Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(
                        "Ya existe un pago del Emisor con la referencia: "
                        + campoReferencia.getText().trim(), TipoMensaje.ADVERTENCIA, autocompletarLista.getTextField());
                    }
                });
                 
                
            }
        }
    }

    private void creaListeners() {

        SION.log(Modulo.PAGO_SERVICIOS, "Creando listeners", Level.INFO);

        botonValidarPago.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                
                SION.log(Modulo.PAGO_SERVICIOS, "Botón validar pago presionado", Level.INFO);

                boolean existePagoAntad = false;
                boolean existePagoPagatodo = false;
                
                boolean esEmisorAntad = false;
                boolean esEmisorPagatodo = false;

                int index = binding.getIndiceServicio(autocompletarLista.getTextField().getText().trim());
                String referencia = "";
                
                if (!campoReferencia.getText().isEmpty()) {

                    //validar pago duplicado en bd local
                    //if (!binding.existePagobotonaLinea()) {
                    //se revisa que no exista el pago en bd local para evitar duplicados

                    //valida que no existe algun pago por aplicar en antad
                    SION.log(Modulo.PAGO_SERVICIOS, "Validando existencia de pagos con plataforma Antad", Level.INFO);
                    existePagoAntad = binding.existePagoAntad();
                    
                    
                    //valida que no existe algun pago por aplicar en antad
                    SION.log(Modulo.PAGO_SERVICIOS, "Validando existencia de pagos con plataforma Pagatodo", Level.INFO);
                    existePagoPagatodo = binding.existePagoPagatodo();
                    

                    if (!existePagoAntad && !existePagoPagatodo) {
                        SION.log(Modulo.PAGO_SERVICIOS, "No existen pagos antad cargados en la operacion en curso", Level.INFO);
                        //continua con el proceso normal siempre y cuando no existe vcargado en la tabla un pago de antad, ya que estos solo se eprmite enviar uno a la vez
                        
                        
                        

                        //valida si el pago se envia al Switch Elektra o antad
                        esEmisorAntad = binding.esEmisorAntad(Long.parseLong(binding.buscaIdXNombre(autocompletarLista.getTextField().getText().trim())));
                        
                        
                        //valida si el pago se envia al Switch Elektra o antad
                        esEmisorPagatodo = binding.esEmisorPagatodo(Long.parseLong(binding.buscaIdXNombre(autocompletarLista.getTextField().getText().trim())));


                        if (esEmisorAntad) {

                            SION.log(Modulo.PAGO_SERVICIOS, "Se valida que el emisor ingresado se enviara a Plataforma Antad, iniciando proceso.", Level.INFO);

                            //abre validaodr de antad para capturar referencia nuevamente y manda contenido del campo de referencia para comparar asi como emisor y monto
                            binding.abreStageAntad();

                        }else if(esEmisorPagatodo){
                            
                           SION.log(Modulo.PAGO_SERVICIOS, "Se valida que el emisor ingresado se enviara a Plataforma Pagatodo, iniciando proceso.", Level.INFO);

                            //abre validaodr de antad para capturar referencia nuevamente y manda contenido del campo de referencia para comparar asi como emisor y monto
                            binding.abreStagePagatodo();
                            
                        } else {
                            
                            SION.log(Modulo.PAGO_SERVICIOS, "Se valida que el emisor ingresado se enviara a Plataforma Switch EKT, iniciando proceso.", Level.INFO);

                            if (!comboTelevia.isDisable()) {
                                //valida televia
                                if (montoPagoTelevia > 0) {
                                    referencia = campoReferencia.getText().trim();
                                    //validarReferenciasDuplicadas(index, referencia.trim(), true);
                                    binding.abreStageSwitch(index, referencia.trim(), true);
                                    
                                }
                            } else {
                                ///valida oriflame
                                if (binding.buscaIdXNombre(autocompletarLista.getTextField().getText().trim()).equals(
                                        SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.ORIFLAME"))) {
                                    referencia = campoReferencia.getText().trim();
                                    //validarReferenciasDuplicadas(index, referencia.trim(), false);
                                    binding.abreStageSwitch(index, referencia.trim(), false);
                                }else if(binding.buscaIdXNombre(autocompletarLista.getTextField().getText().trim()).equals(
                                        SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.DINEROEXP"))) {
                                    referencia = campoReferencia.getText().trim();
                                    //validarReferenciasDuplicadas(index, referencia.trim(), false);
                                    binding.abreStageSwitch(index, referencia.trim(), false);
                                
                                }else {
                                    //continua validando demas emisores
                                    if (!campoPago.getText().isEmpty()) {
                                        if (Double.parseDouble(campoPago.getText()) > 0) {
                                            if (binding.getCantidadPagosTotal() < Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "MAXIMO.PAGOS"))) {
                                                ///continua validacion jafra
                                                if (binding.buscaIdXNombre(autocompletarLista.getTextField().getText().trim()).equals(
                                                        SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.JAFRA"))) {
                                                    if (campoDigitoVerificadorJafra.getText().length() == 2) {

                                                        referencia = campoReferencia.getText().trim().concat(campoDigitoVerificadorJafra.getText().trim());
                                                        //validarReferenciasDuplicadas(index, referencia.trim(), false);
                                                        binding.abreStageSwitch(index, referencia.trim(), false);

                                                    } else {
                                                        
                                                        Platform.runLater(new Runnable() {

                                                            @Override
                                                            public void run() {
                                                                AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.campos.vacios"), TipoMensaje.ADVERTENCIA, campoReferencia);
                                                                cargarComponentesJafra();
                                                            }
                                                        });
                                                    }
                                                } //todos los demas emisores
                                                else {
                                                    referencia = campoReferencia.getText().trim();
                                                    //validarReferenciasDuplicadas(index, referencia.trim(), false);
                                                    binding.abreStageSwitch(index, referencia.trim(), false);
                                                }
                                            } else {
                                                Platform.runLater(new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        AdministraVentanas.mostrarAlertaRetornaFoco(
                                                        SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.maximo.pagos"), TipoMensaje.ADVERTENCIA, tablaPagosAcumulados);
                                                    }
                                                });
                                                
                                            }
                                        }
                                    }

                                }
                            }
                        }

                    } else {
                        

                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                
                                //muestra advertencia que indica que si existe cargado un pago de antad primero hay que concluirlo
                        AdministraVentanas.mostrarAlertaRetornaFoco("No se permiten agregar más pagos a la lista, para continuar termine con el pago actual.", TipoMensaje.ADVERTENCIA, campoReferencia);
                                
                                campoPago.setText("0.00");
                                campoReferencia.setText("");
                                autocompletarLista.getTextField().setText("");
                            }
                        });
                    }
                    //}else{
                    //    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.fueralinea.encontrado"), TipoMensaje.ADVERTENCIA, campoReferencia);
                    //}

                } else {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.campos.vacios"), TipoMensaje.ADVERTENCIA, campoReferencia);
                        }
                    });
                    
                }

                if (index==-1)
                {
                    cargarComponentesDefault();
                }else{
                    
                   String strIdEmisor = binding.buscaIdXNombre(autocompletarLista.getTextField().getText().trim());
                   
                   
                   if (strIdEmisor.equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.AVON"))) {
                        cargarComponentesAvon();
                        setRadioVendedorAvon();
                    } else if (strIdEmisor.equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.JAFRA"))) {
                        cargarComponentesJafra();
                    } else if (strIdEmisor.equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.TELEVIA"))) {
                        cargarComponentesTelevia();
                    } else if (strIdEmisor.equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.ORIFLAME"))) {
                        cargarComponentesOriflame();
                    }else if (strIdEmisor.equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.DINEROEXP"))) {
                        cargarComponentesDineroExpress();
                    }else if (binding.buscaTipoServicioXNombre(getCampoEmisor().getText()) == PAGOSERVTIPOPIN){
                        cargarComponentesPines();
                    } else {
                        cargarComponentesDefault();
                    }
                   
                   
           
                    
                }
                
                

            }
        });
        
        
        
        btnMuestraEmisores.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {

                SION.log(Modulo.PAGO_SERVICIOS, "Clic en boton muestra todos los emisores" , Level.INFO);
                //autocompletarListaOverlay.setText("");
                autocompletarListaOverlay.getTextField().clear();
                autocompletarListaOverlay.getTextField().requestFocus();
                cajaOverlayEmisores.setVisible(true);
                
            }
        });
        
        
        
        
        
        
        botonServicios.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {

                SION.log(Modulo.PAGO_SERVICIOS, "Clic en boton muestra emisores servicios" , Level.INFO);
                fpaneTodosServicios.getChildren().clear();
                
               fpaneTodosServicios.getChildren().add(fPaneEmisores);
            }
        });
        
        
        
        
        
        botonPrepago.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {

                SION.log(Modulo.PAGO_SERVICIOS, "Clic en boton muestra emisores pines" , Level.INFO);
                 fpaneTodosServicios.getChildren().clear();
                fpaneTodosServicios.getChildren().add(fPanePines);
                
            }
        });

       
                

        validador.setMaXCaracteres(39);
        validador.formatearCampoTexto(campoReferencia);

        radioBotonVendedor.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                validador.setMaXCaracteres(13);
                campoReferencia.setText("");
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        campoReferencia.requestFocus();
                    }
                });
            }
        });

        radioBotonPortador.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                validador.setMaXCaracteres(9);
                campoReferencia.setText("");
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        campoReferencia.requestFocus();
                    }
                });
            }
        });

        campoDigitoVerificadorJafra.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (ENTER.match(arg0)) {
                    if (!campoReferencia.getText().isEmpty()) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                campoPago.requestFocus();
                            }
                        });
                    }
                }
            }
        });

        campoDigitoVerificadorJafra.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (campoDigitoVerificadorJafra.getText().length() > 1) {
                    arg0.consume();
                }
            }
        });

        EventHandler<KeyEvent> eventosAtajos = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (f2.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            autocompletarLista.getTextField().requestFocus();
                        }
                    });
                } else if (f8.match(arg0)) {
                    if (montoVentaProperty.get() > 0) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                campoImporteRecibido.requestFocus();
                            }
                        });
                    }
                }
            }
        };
        panelPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, eventosAtajos);

        autocompletarLista.getTextField().focusedProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    campoReferencia.setText("");
                    campoPago.setText("");
                    autocompletarLista.getTextField().setText("");
                }
            }
        });

        comboTelevia.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (ENTER.match(arg0)) {
                    if (montoPagoTelevia > 0) {
                        botonValidarPago.requestFocus();
                    }
                }
            }
        });
        
        

        comboTelevia.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                if (comboTelevia.getSelectionModel().getSelectedItem() != null) {
                    montoPagoTelevia = Integer.parseInt(comboTelevia.getSelectionModel().getSelectedItem());
                    botonValidarPago.requestFocus();
                }
            }
        });
        
        
        comboPines.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (ENTER.match(arg0)) {
                    if (montoPagoPines > 0) {
                        botonValidarPago.requestFocus();
                    }
                }
            }
        });
        
        
        comboPines.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                if (comboPines.getSelectionModel().getSelectedItem() != null) {
                    montoPagoPines = Integer.parseInt(comboPines.getSelectionModel().getSelectedItem());
                    botonValidarPago.requestFocus();
                }
            }
        });

        EventHandler<KeyEvent> eventosCampoReferencia = new EventHandler<KeyEvent>() {

            @Override
            public void handle(final KeyEvent arg0) {
                if (!autocompletarLista.getTextField().getText().isEmpty()) {
                    if (ENTER.match(arg0)) {
                        if (!campoReferencia.getText().isEmpty()) {
                            if (!comboTelevia.isDisable()) {
                                comboTelevia.requestFocus();
                            }else if(!comboPines.isDisable()) {
                                comboPines.requestFocus();
                            }else {
                                if (campoDigitoVerificadorJafra.isVisible()) {
                                    campoDigitoVerificadorJafra.requestFocus();
                                } else {

                                    if (campoPago.isVisible()) {
                                        campoPago.requestFocus();
                                    } else {
                                        botonValidarPago.requestFocus();
                                    }
                                }
                            }
                        }
                    }
                } else {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            arg0.consume();
                            campoReferencia.setText("");
                        }
                    });
                }

            }
        };
        campoReferencia.addEventFilter(KeyEvent.KEY_PRESSED, eventosCampoReferencia);

        campoReferencia.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (campoReferencia.getText().length() > 40) {
                    arg0.consume();
                }
            }
        });

        campoReferencia.focusedProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                campoPago.setText("");
            }
        });

        campoPago.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (ENTER.match(arg0)) {
                    if (!campoPago.getText().isEmpty()) {
                        botonValidarPago.requestFocus();
                    }
                }
            }
        });

        EventHandler<KeyEvent> eventosTabla = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (ENTER.match(arg0)) {
                    SION.log(Modulo.PAGO_SERVICIOS, "Boton enter presionado sobre tabla de servicios", Level.INFO);
                    if (montoVentaProperty.get() > 0) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                campoImporteRecibido.requestFocus();
                            }
                        });
                    }
                }
            }
        };
        tablaPagosAcumulados.addEventFilter(KeyEvent.KEY_PRESSED, eventosTabla);

        botonEfectivo.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {

                if (esHoraValida()) {
                    if (!campoImporteRecibido.getText().isEmpty()) {
                        if (Double.parseDouble(campoImporteRecibido.getText()) >= montoVentaProperty.get()) {

                            binding.validarMontoPagoServicio(EFECTIVO);

                        }
                    }
                } else {
                    muestraMsjHoraNoValida();
                }
            }
        });

        botonTarjeta.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                binding.validarMontoPagoServicio(TARJETA);
            }
        });

        botonTarjeta.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (TAB.match(arg0) || SHIFTTAB.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    autocompletarLista.setFocusCampoTexto();
                                }
                            });
                        }
                    });
                }
            }
        });

        tablaPagosAcumulados.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (TAB.match(arg0) || SHIFTTAB.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            campoImporteRecibido.requestFocus();
                        }
                    });
                }
            }
        });

        campoMontoRestante.focusedProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    campoImporteRecibido.requestFocus();
                }
            }
        });

        campoImporteRecibido.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (TAB.match(arg0) || SHIFTTAB.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            botonEfectivo.requestFocus();
                        }
                    });
                } else if (ENTER.match(arg0) || f10.match(arg0)) {

                    if (esHoraValida()) {
                        if (!campoImporteRecibido.getText().isEmpty()) {
                            if (Double.parseDouble(campoImporteRecibido.getText()) >= montoVentaProperty.get()) {

                                binding.validarMontoPagoServicio(EFECTIVO);

                            }
                        }
                    } else {
                        muestraMsjHoraNoValida();
                    }
                } else if (MAS.match(arg0) || ADD.match(arg0)) {
                    campoImporteRecibido.setText(utilerias.getNumeroFormateado(montoVentaProperty.get()));
                }
            }
        });

        eventoESCPagoServicios = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (escape.match(arg0)) {
                    binding.ejecutarAutorizador();
                }
            }
        };
        //panelPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, eventoESCPagoServicios);

        EventHandler<KeyEvent> eventosBotonEfectivo = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (TAB.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            autocompletarLista.getTextField().requestFocus();
                        }
                    });
                }
            }
        };
        botonEfectivo.addEventFilter(KeyEvent.KEY_PRESSED, eventosBotonEfectivo);

        setCursor(botonEfectivo);
        setCursor(botonValidarPago);
        setCursor(botonTarjeta);
        setCursor(vistaImagenAyudaReferencia);

        listenerImagenes(vistaImagenFrecuente1, imagenFrecuente1, imagenFrecuente1H);
        listenerImagenes(vistaImagenFrecuente2, imagenFrecuente2, imagenFrecuente2H);
        listenerImagenes(vistaImagenFrecuente3, imagenFrecuente3, imagenFrecuente3H);
        listenerImagenes(vistaImagenFrecuente4, imagenFrecuente4, imagenFrecuente4H);
        listenerImagenes(vistaImagenFrecuente5, imagenFrecuente5, imagenFrecuente5H);
        listenerImagenes(vistaImagenFrecuente6, imagenFrecuente6, imagenFrecuente6H);
        listenerImagenes(vistaImagenFrecuente7, imagenFrecuente7, imagenFrecuente7H);
        listenerImagenes(vistaImagenFrecuente8, imagenFrecuente8, imagenFrecuente8H);
        listenerImagenes(vistaImagenFrecuente9, imagenFrecuente9, imagenFrecuente9H);
        listenerImagenes(vistaImagenFrecuente10, imagenFrecuente10, imagenFrecuente10H);

    }

    public void agregarEventoESC() {
        //SION.log(Modulo.PAGO_SERVICIOS, "Agregando ESC pago servicios", Level.INFO);
        panelPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, eventoESCPagoServicios);
    }

    public void removerEventoESC() {
        //SION.log(Modulo.PAGO_SERVICIOS, "Removiendo ESC pago servicios", Level.INFO);
        panelPrincipal.removeEventFilter(KeyEvent.KEY_PRESSED, eventoESCPagoServicios);
    }

    public void actualizarCantidades() {

        SION.log(Modulo.PAGO_SERVICIOS, "Creando eventos de actualizacion de cantidades", Level.INFO);

        montoVentaProperty.addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
                etiquetaMontoVenta.setText(utilerias.getNumeroFormateado(arg2.doubleValue()));

                if (montoVentaProperty.get() > 0) {
                    if (!campoImporteRecibido.isEditable()) {
                        campoImporteRecibido.setEditable(true);
                    }
                } else if (montoVentaProperty.get() == 0) {
                    if (campoImporteRecibido.isEditable()) {
                        campoImporteRecibido.setEditable(false);
                    }
                }
            }
        });

        //binding que actualiza el monto restante al actualizar propiedades de monto de venta o de monto recibido 
        bindingRestante = new DoubleBinding() {

            {
                super.bind(montoVentaProperty, recibidoProperty);
            }

            @Override
            protected double computeValue() {
                if ((montoVentaProperty.get() - recibidoProperty.get() > 0)) {
                    return (montoVentaProperty.get() - recibidoProperty.get());
                } else {
                    return 0;
                }
            }
        };


        bindingCambio = new DoubleBinding() {

            {
                super.bind(montoVentaProperty, recibidoProperty);
            }

            @Override
            protected double computeValue() {
                if ((recibidoProperty.get() - montoVentaProperty.get()) > 0) {
                    return (recibidoProperty.get() - montoVentaProperty.get());
                } else {
                    return 0;
                }
            }
        };

        campoImporteRecibido.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {

                if (campoImporteRecibido.getText().isEmpty()) {
                    recibidoProperty.set(0);
                } else {
                    recibidoProperty.set(Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(campoImporteRecibido.getText()))));
                }

                setMontoVentaProperty(binding.getImportePago());

                if (bindingRestante.get() > 0) {
                    campoMontoRestante.setText(utilerias.getNumeroFormateado(bindingRestante.get()));
                } else {
                    campoMontoRestante.setText("0.00");
                }
                if (bindingCambio.get() > 0) {
                    campoCambio.setText(utilerias.getNumeroFormateado(bindingCambio.get()));
                } else {
                    campoCambio.setText("0.00");
                }

            }
        });

        vistaImagenAyudaReferencia.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {


                try {

                    if (!autocompletarLista.getTextField().getText().isEmpty()) {
                        String nombre = binding.buscaIdXNombre(autocompletarLista.getTextField().getText().trim());
                        if (!nombre.isEmpty()) {
                            vistaAyuda.mostrarStageAyuda(nombre.trim());
                        }
                    }

                } catch (Exception e) {
                    SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
                } catch (Error e) {
                    SION.log(Modulo.PAGO_SERVICIOS, "error en vista ps", Level.SEVERE);
                }
            }
        });
    }

    public void setMontoVentaProperty(double _monto) {
        montoVentaProperty.set(Double.parseDouble(utilerias.getNumeroFormateado(_monto)));
    }

    public DoubleProperty getMontoVentaProperty() {
        return montoVentaProperty;
    }

    public void setReferenciaAntadProperty(double _monto) {
        referenciaAntadProperty.set(Double.parseDouble(utilerias.getNumeroFormateado(_monto)));
    }

    public DoubleProperty getReferenciaAntadProperty() {
        return referenciaAntadProperty;
    }

    public double getBindingMontoRestante() {
        return Double.parseDouble(utilerias.getNumeroFormateado(bindingRestante.get()));
    }

    public double getBindingMontoCambio() {
        return Double.parseDouble(utilerias.getNumeroFormateado(bindingCambio.get()));
    }

    public TextField getCampoMontoRestante() {
        return campoMontoRestante;
    }

    public TextField getCampoMontoCambio() {
        return campoCambio;
    }

    public TextField getCampoMontoRecibido() {
        return campoImporteRecibido;
    }

    public TextField getCampoReferencia() {
        return campoReferencia;
    }

    public TextField getCampoPago() {
        return campoPago;
    }

    public TableView getTablaServicios() {
        return tablaPagosAcumulados;
    }

    public TextField getCampoEmisor() {
        return autocompletarLista.getTextField();
    }
    
    public TextField getCampoOverlay() {
        return autocompletarListaOverlay.getTextField();
    }

    private void setCursor(final Node _nodo) {

        _nodo.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {

                if (arg2 && !arg1) {
                    _nodo.setCursor(Cursor.HAND);

                } else if (arg1 && !arg2) {
                    _nodo.setCursor(Cursor.NONE);

                }


            }
        });

    }

    private void cambiarImagen(ImageView _vistaImagen, Image _imagen) {
        _vistaImagen.setImage(_imagen);
    }

    private void listenerImagenes(final ImageView _vistaImagen, final Image _imagen, final Image _imagenHover) {

        _vistaImagen.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {

                EmpresaBean bean = new EmpresaBean();
                int indiceCompania = -1;
                
                cajaOverlayEmisores.setVisible(false);

                bean = binding.getIndiceServicioFrecuente(_vistaImagen.getId());

                autocompletarLista.setText(bean.getDescrpcion());
                
                SION.log(Modulo.PAGO_SERVICIOS, "Click en boton id " + bean.getEmpresaId(), Level.INFO);

                if (String.valueOf(bean.getEmpresaId()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.AVON"))) {
                    cargarComponentesAvon();
                    setRadioVendedorAvon();
                } else if (String.valueOf(bean.getEmpresaId()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.JAFRA"))) {
                    cargarComponentesJafra();
                } else if (String.valueOf(bean.getEmpresaId()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.TELEVIA"))) {
                    cargarComponentesTelevia();
                } else if (String.valueOf(bean.getEmpresaId()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.ORIFLAME"))) {
                    cargarComponentesOriflame();
                }else if (String.valueOf(bean.getEmpresaId()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.DINEROEXP"))) {
                    cargarComponentesDineroExpress();
                }else if (binding.buscaTipoServicioXNombre(getCampoEmisor().getText()) == PAGOSERVTIPOPIN ){
                        cargarComponentesPines();
                } else {
                        cargarComponentesDefault();
                }

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        if (autocompletarLista.isPopupShowing()) {
                            autocompletarLista.ocultarPopup();
                        }
                        campoReferencia.setText("");
                        campoReferencia.requestFocus();

                    }
                });

            }
        });


        _vistaImagen.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    _vistaImagen.setImage(_imagenHover);
                    _vistaImagen.setCursor(Cursor.HAND);
                } else if (arg1 && !arg2) {
                    _vistaImagen.setImage(_imagen);
                    _vistaImagen.setCursor(Cursor.NONE);

                }
            }
        });
        
        

    }

    public void cargarLoading() {

        imagenLoading.setFitHeight(100);
        imagenLoading.setFitWidth(100);
        imagenLoading.setCache(true);
        imagenLoading.setStyle("-fx-background-color:transparent;");
        imagenLoading.setLayoutX((screen.getBounds().getMaxX() / 2) - imagenLoading.getFitHeight());
        imagenLoading.setLayoutY((screen.getBounds().getMaxY() / 2) - imagenLoading.getFitHeight());

        panelPrincipal.getChildren().add(panelPrincipal.getChildren().size(), imagenLoading);
        panelPrincipal.setDisable(true);

        SION.log(Modulo.PAGO_SERVICIOS, "Loading cargado", Level.INFO);

    }

    public void removerImagenLoading() {

        panelPrincipal.getChildren().remove(imagenLoading);
        panelPrincipal.setDisable(false);
    }

    public void formatearCampo(TextField _campo) {
        //SION.log(Modulo.PAGO_SERVICIOS, "entra validador", Level.INFO);
        ValidadorNumerico validadorNumerico = new ValidadorNumerico();
        validadorNumerico.validadorCampoTarjetas(_campo);
        //SION.log(Modulo.PAGO_SERVICIOS, "sale validador", Level.INFO);
    }

    private void validarHoraSistema() {

        if (!esHoraValida()) {
            muestraMsjHoraNoValida();
            panelPrincipal.setDisable(true);
        } else {
            if (panelPrincipal.isDisable()) {
                panelPrincipal.setDisable(false);
            }
        }
    }

    public void muestraMsjHoraNoValida() {
        
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                AdministraVentanas.mostrarAlertaRetornaFoco(
                SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.hora.novalida").
                replace("%1", SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "hora.limite.dia")).
                replace("%2", SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "hora.limite.noche")).
                replace("%3", SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "minuto.limite")),
                TipoMensaje.ADVERTENCIA, tablaPagosAcumulados);
            }
        });
        
        
    }

    public boolean esHoraValida() {
        boolean esValida = false;

        SION.log(Modulo.PAGO_SERVICIOS, "Validando hora de sistema", Level.INFO);

        horaSistema = 0;
        minutoSistema = 0;
        horaSistema = obtenerHoraSistema();
        minutoSistema = obtenerMinutoSistema();

        SION.log(Modulo.PAGO_SERVICIOS, "Hora consultada: " + horaSistema + ":" + minutoSistema, Level.INFO);

        if (horaSistema >= Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "hora.limite.dia"))) {

            if (horaSistema < Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "hora.limite.noche"))) {

                esValida = true;
            } else {
                if (horaSistema == Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "hora.limite.noche"))) {

                    if (minutoSistema <= Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "minuto.limite"))) {

                        esValida = true;
                    }
                }
            }
        }

        /*
         * if (horaSistema >=
         * Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS,
         * "hora.limite.dia")) || horaSistema <=
         * Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS,
         * "hora.limite.noche"))) { System.out.println("pasa 1"); if
         * (minutoSistema <=
         * Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS,
         * "minuto.limite"))) { System.out.println("pasa 2"); esValida = true; }
         * }
         */

        return esValida;
    }

    @Override
    public Parent obtenerEscena(Object... os) {

        
        //try{
        
        AdministraVentanas.agregarF12EventHandler();
        validarHoraSistema();

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                VistaBarraUsuario.setTituloVentana("Pago de Servicios");

                
                SION.log(Modulo.PAGO_SERVICIOS, "poblando atajos", Level.INFO);

                try {
                    VistaBarraUsuario.poblarAtajos(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "ps.sistema")),
                            Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "ps.modulo")),
                            Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "ps.submodulo")));
                } catch (Exception e) {
                    SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e, "Ocurrió un error al invocar método de atajos ");
                }

                //consulta parametro para ver cuantas veces se debe recorrer las imagenes
                neto.sion.tienda.venta.utilerias.bean.RespuestaParametroBean parametroRecorridoImagenes = utilerias.consultaParametro(UtileriasVenta.Accion.RECORRIDO_EMISORES_PS, Modulo.PAGO_SERVICIOS, Modulo.PAGO_SERVICIOS);
                if (parametroRecorridoImagenes.getCodigoError() == 0) {
                    movimientosPorRealizar = (int) parametroRecorridoImagenes.getValorConfiguracion();
                }





            }
        });
        
        /*}catch(Exception e){
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            SION.log(Modulo.PAGO_SERVICIOS,"Excepcion al mostrar escena: "+exceptionAsString, Level.SEVERE);
            
            
            //e.printStackTrace();
        }*/

        return root;
    }

    @Override
    public void limpiar() {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                SION.log(Modulo.PAGO_SERVICIOS, "Limpiando variables y componentes de la vista", Level.INFO);
                binding.limpiar();
                autocompletarLista.getTextField().setText("");
                if (autocompletarLista.isPopupShowing()) {
                    autocompletarLista.ocultarPopup();
                }
                campoReferencia.setText("");
                campoPago.setText("");
                etiquetaMontoVenta.setText("0.00");
                campoMontoRestante.setText("0.00");
                campoImporteRecibido.setText("0.00");
                campoCambio.setText("0.00");
                montoVentaProperty.set(0);
                recibidoProperty.set(0);
                referenciaAntadProperty.set(0);
            }
        });

    }

    @Override
    public String[] getUserAgentStylesheet() {
        return new String[]{"/neto/sion/tienda/pago/servicios/vista/EstilosPagoServicios.css"};
    }
}