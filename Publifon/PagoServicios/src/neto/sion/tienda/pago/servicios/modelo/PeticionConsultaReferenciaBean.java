package neto.sion.tienda.pago.servicios.modelo;

import java.util.Date;

/**
 *
 * @author jjsanchezs
 */
public class PeticionConsultaReferenciaBean {

    private String paReferencia;
    private double paMontoPago;
    private Long paEmisorId;
    private String paFechaConc;
    private int paPaisId;
    private int paTiendaId;

    public String getPaReferencia() {
        return paReferencia;
    }

    public void setPaReferencia(String paReferencia) {
        this.paReferencia = paReferencia;
    }

    public double getPaMontoPago() {
        return paMontoPago;
    }

    public void setPaMontoPago(double paMontoPago) {
        this.paMontoPago = paMontoPago;
    }

    public Long getPaEmisorId() {
        return paEmisorId;
    }

    public void setPaEmisorId(Long paEmisorId) {
        this.paEmisorId = paEmisorId;
    }

    public String getPaFechaConc() {
        return paFechaConc;
    }

    public void setPaFechaConc(String paFechaConc) {
        this.paFechaConc = paFechaConc;
    }

    public int getPaPaisId() {
        return paPaisId;
    }

    public void setPaPaisId(int paPaisId) {
        this.paPaisId = paPaisId;
    }

    public int getPaTiendaId() {
        return paTiendaId;
    }

    public void setPaTiendaId(int paTiendaId) {
        this.paTiendaId = paTiendaId;
    }

    @Override
    public String toString() {
        return "PeticionConsultaReferenciaBean{" + "paReferencia=" + paReferencia + ", paMontoPago=" + paMontoPago + ", paEmisorId=" + paEmisorId + ", paFechaConc=" + paFechaConc + ", paPaisId=" + paPaisId + ", paTiendaId=" + paTiendaId + '}';
    }
}
