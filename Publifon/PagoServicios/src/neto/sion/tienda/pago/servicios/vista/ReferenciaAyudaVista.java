/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vista;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.modelo.ImagenBean;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;

/**
 *
 * @author fvega
 */
public class ReferenciaAyudaVista {

    private UtileriasVenta utilerias;
    private PagoServiciosVista pagoServiciosVista;
    //
    private Stage stage;
    private Group grupo;
    private Scene escena;
    private BorderPane panel;
    //panel
    private VBox cajaCabecera;
    private VBox cajaImagen;
    //cajaCabecera
    private Group grupoCabecera;
    //grupoCabecera
    private VBox cajaRelleno;
    private Image imagenCerrar;
    private Image imagenCerrarHover;
    private ImageView vistaImagenCerrar;
    //cajaImagen
    private Image imagenAyuda;
    private ImageView vistaImagenAyuda;

    public ReferenciaAyudaVista(final PagoServiciosVista _pagoServiciosVista, final UtileriasVenta _utilerias) {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {

                //
                pagoServiciosVista = _pagoServiciosVista;
                utilerias = _utilerias;
                //
                panel = new BorderPane();
                grupo = new Group();
                escena = new Scene(grupo, 550, 650, Color.WHITE);
                stage = new Stage(StageStyle.UNDECORATED);
                stage.initModality(Modality.APPLICATION_MODAL);

                //panel
                cajaCabecera = utilerias.getVBox(0, 550, 70);
                cajaImagen = utilerias.getVBox(0, 550, 580);

                //cajaCabecera
                grupoCabecera = new Group();

                //grupoCabecera
                cajaRelleno = utilerias.getVBox(0, 550, 70);
                imagenCerrar = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/btnCerrar.png");
                imagenCerrarHover = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/btn_Cerrar_2.png");
                vistaImagenCerrar = new ImageView(imagenCerrar);

                //cajaImagen
                imagenAyuda = null;
                vistaImagenAyuda = null;

                aplicarEstilos();
                ajustarTamanios();
                creaListeners();
                setCursor(vistaImagenCerrar);

            }
        });

    }

    private void aplicarEstilos() {

        cajaRelleno.getStyleClass().add("CajaCabecera");
        cajaImagen.setStyle("-fx-background-color: white; -fx-border-color: orange; -fx-border-width: 0 2 2 2;"
                + "-fx-alignment: center;");
    }

    private void ajustarTamanios() {

        //cajaCabecera
        cajaCabecera.getChildren().addAll(grupoCabecera);
        grupoCabecera.setBlendMode(BlendMode.MULTIPLY);
        grupoCabecera.getChildren().add(cajaRelleno);
        grupoCabecera.getChildren().add(vistaImagenCerrar);
        vistaImagenCerrar.setFitHeight(25);
        vistaImagenCerrar.setFitWidth(25);
        vistaImagenCerrar.setX(500);
        vistaImagenCerrar.setY(12);

        panel.setTop(cajaCabecera);
        panel.setCenter(cajaImagen);
        panel.setMaxSize(550, 650);
        panel.setMinSize(550, 650);
        panel.setPrefSize(550, 650);
        grupo.getChildren().addAll(panel);

    }

    private Image importarImagen(String _nombre) {
        SION.log(Modulo.PAGO_SERVICIOS, "buscando imagen de ayuda referencia con id: " + _nombre, Level.INFO);
        boolean seCargoImagen = false;
        Image imagen = null;

        try {

            for (ImagenBean bean : pagoServiciosVista.getListaImagenesReferencia()) {
                if (bean != null) {
                    if (bean.getNombre().trim().equals(_nombre.concat(".png").trim())) {
                        seCargoImagen = true;
                        String ruta = pagoServiciosVista.getRutaReferencias().concat(_nombre).concat(".png").trim();
                        
                        SION.log(Modulo.PAGO_SERVICIOS, "Ruta de imagen de ayuda a buscar: " + ruta, Level.INFO);
                        imagen = pagoServiciosVista.getImagen(ruta);
                        SION.log(Modulo.PAGO_SERVICIOS, "Imagen encontrada", Level.INFO);
                    }
                }
            }

        } catch (Exception e) {
            SION.log(Modulo.PAGO_SERVICIOS, "ocurrio una excepcion al cargar imagen: " + e.toString(), Level.SEVERE);
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
        } catch (Error e) {
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un error al cargar imagen: " + e.toString(), Level.SEVERE);
        }

        if (!seCargoImagen) {
            imagen = null;
        }

        return imagen;
    }

    private void creaListeners() {

        vistaImagenCerrar.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {
                cerrarStage();
            }
        });

        vistaImagenCerrar.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    vistaImagenCerrar.setImage(imagenCerrarHover);
                } else if (arg1 && !arg2) {
                    vistaImagenCerrar.setImage(imagenCerrar);
                }
            }
        });

    }

    private void setCursor(final Node _nodo) {

        _nodo.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    _nodo.setCursor(Cursor.HAND);
                } else if (arg1 && !arg2) {
                    _nodo.setCursor(Cursor.NONE);
                }
            }
        });

    }

    public void mostrarStageAyuda(String _nombre) {
        SION.log(Modulo.PAGO_SERVICIOS, "Mostrando stage de ayuda de referencias", Level.INFO);
        escena.getStylesheets().addAll(PagoServiciosVista.class.getResource("/neto/sion/tienda/pago/servicios/vista/EstilosPagoServicios.css").toExternalForm());
        stage.setScene(escena);

        try {
            
            imagenAyuda = importarImagen(_nombre);
            
            if (imagenAyuda != null) {
                
                if (vistaImagenAyuda == null) {
                    
                    vistaImagenAyuda = new ImageView(imagenAyuda);
                } else {
                    
                    vistaImagenAyuda.setImage(imagenAyuda);
                }
                
                cajaImagen.getChildren().clear();
                cajaImagen.getChildren().add(vistaImagenAyuda);
                
                vistaImagenAyuda.setFitWidth(420);
                vistaImagenAyuda.setFitHeight(530);
                stage.show();
                stage.centerOnScreen();
            } else {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.error.imagen.ayuda"), TipoMensaje.ADVERTENCIA, pagoServiciosVista.getCampoReferencia());
                    }
                });
                
            }

        } catch (Exception e) {
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
        } catch (Error e) {
            SION.log(Modulo.PAGO_SERVICIOS, "Error al mostrar stage", Level.SEVERE);

        }

    }

    public void cerrarStage() {
        SION.log(Modulo.PAGO_SERVICIOS, "Cerrando stage de ayuda de referencias", Level.INFO);
        stage.close();
    }
}
