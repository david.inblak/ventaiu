/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author fvega
 */
public class RespuestaServiciosFrecuentesBean {

    private ServicioFrecuenteBean[] servicioFrecuenteBeans;
    private int codigoError;
    private String descError;

    public int getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    public ServicioFrecuenteBean[] getServicioFrecuenteBeans() {
        return servicioFrecuenteBeans;
    }

    public void setServicioFrecuenteBeans(ServicioFrecuenteBean[] servicioFrecuenteBeans) {
        this.servicioFrecuenteBeans = servicioFrecuenteBeans;
    }

    @Override
    public String toString() {
        return "RespuestaServiciosFrecuentesBean{" + "servicioFrecuenteBeans=" + servicioFrecuenteBeans + ", codigoError=" + codigoError + ", descError=" + descError + '}';
    }
}
