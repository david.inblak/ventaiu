/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author jjsanchezs
 */
public class RespuestaConsultaReferenciaBean {
    private int paCdgError;
    private String paDescError;

    public int getPaCdgError() {
        return paCdgError;
    }

    public void setPaCdgError(int paCdgError) {
        this.paCdgError = paCdgError;
    }

    public String getPaDescError() {
        return paDescError;
    }

    public void setPaDescError(String paDescError) {
        this.paDescError = paDescError;
    }

    @Override
    public String toString() {
        return "RespuestaConsultaReferenciaBean{" + "paCdgError=" + paCdgError + ", paDescError=" + paDescError + '}';
    }
    
}
