/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author fvega
 */
public class ImagenBean {

    private String carpeta;
    private String nombre;
    private String path;

    public ImagenBean(String carpeta, String nombre, String path) {
        this.carpeta = carpeta;
        this.nombre = nombre;
        this.path = path;
    }

    public String getCarpeta() {
        return carpeta;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPath() {
        return path;
    }

    public void setCarpeta(String carpeta) {
        this.carpeta = carpeta;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "ImagenBean{" + "carpeta=" + carpeta + ", nombre=" + nombre + ", path=" + path + '}';
    }
}
