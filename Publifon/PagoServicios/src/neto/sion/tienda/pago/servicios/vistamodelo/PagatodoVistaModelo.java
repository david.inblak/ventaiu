/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vistamodelo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import neto.sion.impresion.ventanormal.dto.ServicioTicketDto;
import neto.sion.impresion.ventanormal.dto.VentaServiciosTicketDto;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.binding.PagatodoBinding;
import neto.sion.tienda.pago.servicios.dao.PagoServiciosDao;
import neto.sion.tienda.pago.servicios.modelo.*;
import neto.sion.tienda.venta.cliente.servicios.antad.dto.*;
import neto.sion.tienda.venta.conciliacion.bean.PeticionActTransaccionLocalBean;
import neto.sion.tienda.venta.conciliacion.bean.RespuestaActualizacionLocalBean;
import neto.sion.tienda.venta.conciliacion.controlador.ConciliacionLocalControlador;
import neto.sion.tienda.venta.fueralinea.bean.PagoServicioBean;
import neto.sion.tienda.venta.fueralinea.bean.PeticionPagoServicioLocalBean;
import neto.sion.tienda.venta.fueralinea.bean.RespuestaPagoServicioLocalBean;
import neto.sion.tienda.venta.fueralinea.bean.TipoPagoBean;
import neto.sion.tienda.venta.fueralinea.controlador.VentaFueraLineaControlador;
import neto.sion.tienda.venta.impresiones.ImpresionVenta;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;
import neto.sion.venta.pagatodo.ps.dto.*;
import neto.sion.tienda.venta.utilerias.bean.RespuestaParametroBean;

import neto.sion.venta.pagatodo.ps.facade.ConsumirPagaTodoPSImp;
/**
 *
 * @author esantiago
 */
public class PagatodoVistaModelo {

    private PagoServiciosVistaModelo vm;
    private UtileriasVenta utilerias;
    private final int AGRUPACION_ANTAD = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.IDAGRUPACION.ANTAD").trim());
    private final int AGRUPACION_PAGATODO = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.IDAGRUPACION.PAGATODO").trim());
    private final int CATEGORIA_ANTAD_CONSULTASKU = 2;
    //private PeticionConsultaDto peticionConsultaAntad;

    //private PeticionAutorizacionDto peticionAutorizacionAntad;

    //private RespuestaAutorizacionDto respuestaAutorizacionAntad;
    //private ConsumirAntadPagosServicioImp clienteAntad;
    private PagatodoBinding pagatodoBinding;
    private VentaFueraLineaControlador pagoFueraLinea;
    private PeticionPagoServicioLocalBean peticionPagoLocal;
    private RespuestaPagoServicioLocalBean respuestaPagoLocal;
    private PeticionActualizacionPagoFueraLineaBean peticionActualizacionPagoFL;
    private RespuestaActualizacionPagoFueraLineaBean respuestaActualizacionPagoFL;
    boolean existeProblemaComunicacion = false;
    boolean seImprimioTicketExitoso = false;
    private final int PAGO_EXITOSO_CENTRAL = 3;
    private final int PAGO_POR_CONFIRMAR_CENTRAL = 9;

    private final int PAGOSERVTIPOPIN = 2;

    private ConsultaPagoPTDtoResp respuestaConsultaBDCentral;



    //Pagatodo
    private ConsumirPagaTodoPSImp clientePagaTodo;
    private AutorizacionPagaTodoDtoReq autorizacionPagaTodoReq;
    private AutorizacionPagaTodoDtoResp autorizacionPagaTodoResp;
    private PeticionPagaTodoBean peticionPagaTodoBean;








    public PagatodoVistaModelo(PagoServiciosVistaModelo _vm, UtileriasVenta _utilerias, PagatodoBinding _pagatodoBinding) {

        this.vm = _vm;
        this.utilerias = _utilerias;
        this.pagatodoBinding = _pagatodoBinding;


        this.clientePagaTodo = new ConsumirPagaTodoPSImp();

        ///fuera de linea
        this.pagoFueraLinea = new VentaFueraLineaControlador();
        this.peticionPagoLocal = new PeticionPagoServicioLocalBean();
        this.respuestaPagoLocal = new RespuestaPagoServicioLocalBean();
        this.peticionActualizacionPagoFL = new PeticionActualizacionPagoFueraLineaBean();
        this.respuestaActualizacionPagoFL = new RespuestaActualizacionPagoFueraLineaBean();


        this.peticionPagaTodoBean = new PeticionPagaTodoBean();


    }


    //getters
    /*public PeticionAutorizacionDto getPeticionAutorizacionAntad() {
        return peticionAutorizacionAntad;
    }*/

    public AutorizacionPagaTodoDtoReq getaAutorizacionPagaTodoReq(){
        return autorizacionPagaTodoReq;
    }


    public AutorizacionPagaTodoDtoResp getAutorizacionPagaTodoResp() {
        return autorizacionPagaTodoResp;
    }

    public PeticionPagoServicioLocalBean getPeticionPagoLocal() {
        return peticionPagoLocal;
    }

    public RespuestaPagoServicioLocalBean getRespuestaPagoLocal() {
        return respuestaPagoLocal;
    }

    public ConsultaPagoPTDtoResp getRespuestaConsultaBDCentral() {
        return respuestaConsultaBDCentral;
    }



    //consulta
    public void consultaPagoPagatodo(boolean _esConsultaSKU, long _emisorId, String _referencia, double _monto, double _comision, int tipoServicio, ConsultaComisionDtoResp detallesComision) {

        boolean seGeneroConciliacion = false;

        //se genera numero de conciliacion
        vm.generarConciliacion(
                Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SISTEMA")),//1
                Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.MODULO")),//2
                Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SUBMODULO.PAGATODO")));//13

        if (vm.getRespuestaConciliacionLocalBean() != null) {
            SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de conciliacion: " + vm.getRespuestaConciliacionLocalBean().toString(), Level.INFO);
            if (vm.getRespuestaConciliacionLocalBean().getPaCdgError() == 0
                    && vm.getRespuestaConciliacionLocalBean().getPaConciliacionId() > 0) {
                seGeneroConciliacion = true;
            }
        }

        if (seGeneroConciliacion) {

            String strCdgBarras ="";

            strCdgBarras = buscaSKUxId((int) _emisorId);




            this.peticionPagaTodoBean.setComision(String.valueOf(_comision));

            this.peticionPagaTodoBean.setEmisor(String.valueOf(_emisorId));
            this.peticionPagaTodoBean.setMonto(String.valueOf(_monto));
            this.peticionPagaTodoBean.setCodigoBarras(strCdgBarras);

            if (tipoServicio == PAGOSERVTIPOPIN){

                double dMontoDetalle = 0;
                String skuPin = "";

                for(DetalleMontoArticuloDto dto : detallesComision.getArrayDetalleArticulos()){
                    dMontoDetalle = Double.valueOf(dto.getFcCobroFront());

                    if (_monto == dMontoDetalle)
                    {
                       skuPin = dto.getFcCodigoBarras();
                       break;
                    }
                }


                this.peticionPagaTodoBean.setSku(skuPin);

            }else{

                if (peticionPagaTodoBean.getEmisor().equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.TELMEX.PAGATODO"))){

                    if (_referencia.length()==10){
                        this.peticionPagaTodoBean.setSku(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGATODO.TELMEX.SKU.MANUAL"));

                    }

                    if (_referencia.length()==20){
                        this.peticionPagaTodoBean.setSku(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGATODO.TELMEX.SKU.RECIBO"));
                    }

                }else{
                    this.peticionPagaTodoBean.setSku(strCdgBarras);
                }

            }





            this.peticionPagaTodoBean.setConciliacion(String.valueOf(vm.getRespuestaConciliacionLocalBean().getPaConciliacionId()));
            this.peticionPagaTodoBean.setSucursal(String.valueOf(VistaBarraUsuario.getSesion().getTiendaId()));
            this.peticionPagaTodoBean.setCajaId(String.valueOf(VistaBarraUsuario.getSesion().getCajaId()));
            this.peticionPagaTodoBean.setPaisId(String.valueOf(VistaBarraUsuario.getSesion().getPaisId()));


            agregaReferenciaPagatodo(_emisorId, _referencia, _monto, _comision);


        }else{

            SION.log(Modulo.PAGO_SERVICIOS, "EL proceso de consulta de pago se detendra debido a que no se logro generar un numero de conciliacion", Level.INFO);

            autorizacionPagaTodoResp=null;
            autorizacionPagaTodoResp = new AutorizacionPagaTodoDtoResp();

            autorizacionPagaTodoResp.setRespuestaPagatodoDto(null);
            autorizacionPagaTodoResp.setCodigoError(-1);
            autorizacionPagaTodoResp.setDescError("En estos momentos no es posible realizar el pago, por favor contacte a Soporte Técnico");
        }


    }

    public void agregaReferenciaPagatodo(long _emisorId, String _referencia, double _monto, double _comision) {

        SION.log(Modulo.PAGO_SERVICIOS, "Agregando a lista de referencias validadas", Level.INFO);



        autorizacionPagaTodoResp = new AutorizacionPagaTodoDtoResp();

        autorizacionPagaTodoResp.setCodigoError(0);
        autorizacionPagaTodoResp.setDescError("OK");


        ReferenciaValidaBean referenciaValida = new ReferenciaValidaBean();
        referenciaValida.setEmisorId((int) _emisorId);
        referenciaValida.setReferencia(_referencia);
        referenciaValida.setSubReferencia("");

        vm.getListaReferenciasValidadas().add(referenciaValida);

        SION.log(Modulo.PAGO_SERVICIOS, "Lista de referencias actualizada: " + vm.getListaReferenciasValidadas(), Level.INFO);


        SION.log(Modulo.PAGO_SERVICIOS, "Agregando registro a lista de pagos acumulados", Level.INFO);


        double comision = 0;
        double monto = 0;

        comision = _comision;



        monto = _monto;

        vm.getListaPagosAcumulados().add(new ServicioBean(
                true, //check en tabla
                String.valueOf(_emisorId), //ruta del logo
                (int) _emisorId, //emisor
                buscaNombreEmisorXId((int) _emisorId), //nombre del emisor
                1, //tipopago
                comision,//comision
                monto, //importe
                _referencia, //referencia
                0.0, //impuesto
                ""));  //deposito
        SION.log(Modulo.PAGO_SERVICIOS, "Lista de pagos actualizada: " + vm.getListaPagosAcumulados().toString(), Level.INFO);

    }



    //autorizacion
    public void autorizaPagoPagatodo(double _montoRecibido,double comision ,String _referencia) {

        SION.log(Modulo.PAGO_SERVICIOS, "Paso 1/4: Generar peticion de Autorizacion", Level.INFO);

        autorizacionPagaTodoReq = null;
        autorizacionPagaTodoReq = new AutorizacionPagaTodoDtoReq();

        autorizacionPagaTodoReq.setAutorizacion(null);


        String t = VistaBarraUsuario.getSesion().getIpEstacion();

         t = t.concat("|CAJA");
        t = t.concat(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));

        autorizacionPagaTodoReq.setAutorizacion(generaAutorizacion(_referencia));
        autorizacionPagaTodoReq.setFecha(obtenerFecha());
        autorizacionPagaTodoReq.setFueraDeLinea(0);
        autorizacionPagaTodoReq.setMontoTotal(Double.parseDouble(utilerias.getNumeroFormateado(vm.getListaPagosAcumulados().get(0).getImporte()
                + vm.getListaPagosAcumulados().get(0).getComision())));
        autorizacionPagaTodoReq.setNumTransaccion(0);
        autorizacionPagaTodoReq.setPaConciliacionId(vm.getRespuestaConciliacionLocalBean().getPaConciliacionId());
        autorizacionPagaTodoReq.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
        autorizacionPagaTodoReq.setTerminal(t);
        autorizacionPagaTodoReq.setTiendaId(Long.valueOf(peticionPagaTodoBean.getSucursal()));
        autorizacionPagaTodoReq.setTipoMovto(7);
        autorizacionPagaTodoReq.setUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());
        autorizacionPagaTodoReq.setArregloTiposPago(generaArregloTiposPago(_montoRecibido));


        SION.log(Modulo.PAGO_SERVICIOS, "Paso 1/4 completo.", Level.INFO);

        //grabar pago fl antes de enviar a central, sirve par validar duplicados antes de enviar a antad
        boolean seGraboRegistroFL = false;

        int intentosPagoFL = 3;
        int intento = 1;

        SION.log(Modulo.PAGO_SERVICIOS, "Paso 2/4: Grabar registro localmente", Level.INFO);
        do {
            SION.log(Modulo.PAGO_SERVICIOS, "Intento No. " + intento + " de pago fuera de linea", Level.INFO);
            grabarPagoFueraLinea();

        } while (intento <= intentosPagoFL && respuestaPagoLocal == null);

        if (respuestaPagoLocal != null) {

            if (respuestaPagoLocal.getCodigoError() == 0) {

                seGraboRegistroFL = true;
                SION.log(Modulo.PAGO_SERVICIOS, "Paso 2/4 completo.", Level.INFO);
            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "Paso 2/4 incompleto, termina proceso de aplicación de pago.", Level.WARNING);
            }

        }


        if (seGraboRegistroFL) {

            autorizacionPagaTodoResp = null;
            autorizacionPagaTodoResp = new AutorizacionPagaTodoDtoResp();



            existeProblemaComunicacion = false;

            try {
                SION.log(Modulo.PAGO_SERVICIOS, "Paso 3/4: Envío de pago a Central.", Level.INFO);
                autorizacionPagaTodoResp =  clientePagaTodo.aplicarPagoPT(autorizacionPagaTodoReq);


                /*autorizacionPagaTodoResp.setCodigoError(0);
                autorizacionPagaTodoResp.setDescError("El pago de servicios se registro exitosamente.");
                
                RespuestaPagaTodoDto pagatodoDtoResp = new RespuestaPagaTodoDto();
                
                
                
                pagatodoDtoResp.setrCodigoRespuesta("00");
                pagatodoDtoResp.setrDescRespuesta("Autorizado.");
                pagatodoDtoResp.setrDescMsgObligatorio("Para dudas o aclaraciones marca a PagaTodo al 5249 5000. Proporciona tu número de autorización  izzi");
                pagatodoDtoResp.setrIdGrupo("1");
                pagatodoDtoResp.setrIdSucursal("7228");
                pagatodoDtoResp.setrNoTicket(null);
                pagatodoDtoResp.setrReferencia("22383573");
                pagatodoDtoResp.setrNoConfirmacion("171438");
                pagatodoDtoResp.setrNoSecUnicoPT("0009243809115");
                pagatodoDtoResp.setrNoMsgObligatorio("00");
                pagatodoDtoResp.setrS110("D029=-58f1920c.15f12a4ed62.-309b");
                
                autorizacionPagaTodoResp.setRespuestaPagatodoDto(pagatodoDtoResp);
                
                autorizacionPagaTodoResp.setTransaccionId(Long.parseLong("20171012357310325"));
                
                

                BloqueoPagatodoDto[] arregloBloqueos = new BloqueoPagatodoDto[3];

                BloqueoPagatodoDto b1 = new BloqueoPagatodoDto();
                b1.setFiTipoPagoId(1);
                b1.setFiNumAvisos(1);
                b1.setFiEstatusBloqueoId(0);
                b1.setFiAvisosFalt(2);
                arregloBloqueos[0] = b1;
                
                
                BloqueoPagatodoDto b2 = new BloqueoPagatodoDto();
                b2.setFiTipoPagoId(2);
                b2.setFiNumAvisos(0);
                b2.setFiEstatusBloqueoId(0);
                b2.setFiAvisosFalt(0);
                arregloBloqueos[1] = b2;
                
                
                BloqueoPagatodoDto b3 = new BloqueoPagatodoDto();
                b3.setFiTipoPagoId(1);
                b3.setFiNumAvisos(1);
                b3.setFiEstatusBloqueoId(0);
                b3.setFiAvisosFalt(2);
                arregloBloqueos[2] = b3;

                autorizacionPagaTodoResp.setArregloBloqueos(arregloBloqueos);*/




                SION.log(Modulo.PAGO_SERVICIOS, "Paso 3/4 completo, respuesta de central recibida, validando codigo de respuesta.", Level.INFO);
                //throw new Exception("error de prueba");
            } catch (Exception ex) {
                existeProblemaComunicacion = true;
                SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex, "Paso 3/4 incompleto, termina proceso de aplicacion del pago: " + ex.getLocalizedMessage());
            }

            //existeProblemaComunicacion = true;

            if (!existeProblemaComunicacion) {
                //valida si la autorizacion fue exitosa
                SION.log(Modulo.PAGO_SERVICIOS, "Paso 4/4: Validacion de codigo de respuesta, actualizacion de pago bd local e impresion de ticket.", Level.INFO);


                if (autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrCodigoRespuesta()!=null &&
                    (autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrCodigoRespuesta().equals("PA") ||
                    autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrCodigoRespuesta().equals("22"))){


                     SION.log(Modulo.PAGO_SERVICIOS, "El pago que se intenta ya esta pagado", Level.INFO);
                    cancelarConciliacionVenta(true);

                    //se cancela operacion debido a que no se pudo guardar venta en bd local

                    //valida si existe respuesta de local lo cual indica un error generado en bd, caso contrario ocurrio una exceocion al conectar

                    autorizacionPagaTodoResp.setDescError("No se pudo realizar el pago: " + autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrDescRespuesta());

                    autorizacionPagaTodoResp.setCodigoError(-7);
                    autorizacionPagaTodoResp.setMovimientoId(0);
                    autorizacionPagaTodoResp.setArregloBloqueos(null);
                    autorizacionPagaTodoResp.setRespuestaPagatodoDto(null);
                    autorizacionPagaTodoResp.setTransaccionId(0);


                }else{

                    if (autorizacionPagaTodoResp.getCodigoError() == 0 || autorizacionPagaTodoResp.getCodigoError()==34) {

                         //respuesta exitosa
                        SION.log(Modulo.PAGO_SERVICIOS, "Codigo de respuesta exitoso, actualizando folio central", Level.INFO);

                        //actualizar el registro de pago fl creado al principio
                        actualizarEstatusLocal(PAGO_EXITOSO_CENTRAL, true, false);

                        //impresion del ticket
                        double montoCambio = _montoRecibido - Double.parseDouble(utilerias.getNumeroFormateado(autorizacionPagaTodoReq.getMontoTotal()));

                        imprimirTicketPagatodo(_montoRecibido, montoCambio, false, true, false);

                        //actualizar sesion
                        vm.agregarObjetoSesion(arrayBloqueosAntad2Venta(), autorizacionPagaTodoResp.getCodigoError());

                        //guardar venta del dia
                        guardarVentaDia();

                        //evalua bloqueos
                        pagatodoBinding.evaluarBloqueosVenta();

                    }else if (autorizacionPagaTodoResp.getCodigoError() == -4) {
                        //no se recibio respuesta de wssionjax

                        completarVentaExitosaPagoPendiente(_montoRecibido, true);

                    }

                    else {

                        //error en la respuesta

                        SION.log(Modulo.PAGO_SERVICIOS, "Se recibe codigo de error, se limpian listas y se muestra msj de error", Level.INFO);

                        //actualizar mensaje error
                        PagoServiciosDao dao = new PagoServiciosDao();
                        peticionActualizacionPagoFL = new PeticionActualizacionPagoFueraLineaBean();
                        peticionActualizacionPagoFL.setConciliacionId(autorizacionPagaTodoReq.getPaConciliacionId());
                        peticionActualizacionPagoFL.setEstatusId(5);
                        if (autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrDescRespuesta() != null) {
                            peticionActualizacionPagoFL.setRespuestaAntad(autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrDescRespuesta());
                        } else {
                            peticionActualizacionPagoFL.setRespuestaAntad("Ocurrió un error al aplicar el pago en WS PagaTodo, no se obtuvo mensaje de respuesta.");
                        }

                        peticionActualizacionPagoFL.setTransaccionId(0);
                        respuestaActualizacionPagoFL = new RespuestaActualizacionPagoFueraLineaBean();
                        respuestaActualizacionPagoFL = dao.actualizarPagoFL(peticionActualizacionPagoFL, 5);

                        cancelarConciliacionVenta(false);
                        vm.limpiaParcial();

                    }

                }
            } else {

                //si existe problema de comunicacion se actualizara a estatus 9 para ser tomada por demonio y ser conciliada posteriormente
                SION.log(Modulo.PAGO_SERVICIOS, "Paso 4 incompleto, se procede a consultar pago en Central ya que no se obtuvo respuesta.", Level.INFO);

                int numeroIntentos = 3;
                int intentoConsulta = 1;
                boolean seRealizoConsulta = false;
                boolean seEncontroPago = false;
                boolean elPagoFueExitoso = false;

                do{

                    SION.log(Modulo.PAGO_SERVICIOS, "Intento de consulta "+intentoConsulta+"/"+numeroIntentos, Level.INFO);
                    SION.log(Modulo.PAGO_SERVICIOS, "Peticicion de consulta de pago a BD Central: "+autorizacionPagaTodoReq.toString(), Level.INFO);
                    try {

                        ConsultaPagoPTDtoReq consultaPagoPTDtoReq = new ConsultaPagoPTDtoReq();



                        consultaPagoPTDtoReq.setEmisorId(Integer.valueOf(peticionPagaTodoBean.getEmisor()));
                        consultaPagoPTDtoReq.setFechaOperacion(autorizacionPagaTodoReq.getFecha());
                        consultaPagoPTDtoReq.setMontoPago(autorizacionPagaTodoReq.getMontoTotal());
                        consultaPagoPTDtoReq.setPaisId(autorizacionPagaTodoReq.getPaPaisId());
                        consultaPagoPTDtoReq.setReferencia(autorizacionPagaTodoReq.getAutorizacion().getReferencia());

                        String strTienda = String.valueOf(autorizacionPagaTodoReq.getTiendaId());
                        consultaPagoPTDtoReq.setTiendaId(Integer.valueOf(strTienda));


                        respuestaConsultaBDCentral = clientePagaTodo.consultaPagoPTCentral(consultaPagoPTDtoReq);


                        seRealizoConsulta = true;
                        SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de consulta de pago: "+respuestaConsultaBDCentral.toString(), Level.INFO);
                    } catch (Exception ex) {
                        seRealizoConsulta = false;
                        SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex, "Ocurrio un error a realizar consulta de pago: "+ex.getLocalizedMessage());
                    }

                    intentoConsulta++;

                }while(intentoConsulta<=numeroIntentos && !seRealizoConsulta);

                if(seRealizoConsulta){
                    if(respuestaConsultaBDCentral.getCodigoError()==1){
                        //pago encontrado en central
                        seEncontroPago = true;
                        if(respuestaConsultaBDCentral.getFolioPagatodo()>0 && respuestaConsultaBDCentral.getTransaccionId()>0){
                            elPagoFueExitoso = true;
                        }
                    }
                }else{
                    completarVentaExitosaPagoPendiente(_montoRecibido, false);
                }

                if(seEncontroPago){

                    if(elPagoFueExitoso){

                        //consulta exitosa con pago autorizado
                        SION.log(Modulo.PAGO_SERVICIOS, "Consulta de pago exitosa, se encuentra pago aplicado en BD Central", Level.INFO);

                        //actualizar el registro de pago fl creado al principio
                        actualizarEstatusLocal(PAGO_EXITOSO_CENTRAL, true, true);

                        //impresion del ticket
                        double montoCambio = _montoRecibido - Double.parseDouble(utilerias.getNumeroFormateado(autorizacionPagaTodoReq.getMontoTotal()));

                        imprimirTicketPagatodo(_montoRecibido, montoCambio, false, true, true);

                        //actualizar sesion - no se actualizan objetos de sesion ya q consultade pago no trae bloqueos
                        //vm.agregarObjetoSesion(arrayBloqueosAntad2Venta(), respuestaConsultaBDCentral.getCodigoError());

                        //guardar venta del dia
                        guardarVentaDia();

                        //evalua bloqueos
                        //antadBinding.evaluarBloqueosVenta();

                    }else{

                        //consulta exitosa con pago rechazado
                        SION.log(Modulo.PAGO_SERVICIOS, "El pago se encuentra pago rechazado en la consulta a BD Central", Level.INFO);

                        //actualizar mensaje error
                        PagoServiciosDao dao = new PagoServiciosDao();
                        peticionActualizacionPagoFL = new PeticionActualizacionPagoFueraLineaBean();
                        peticionActualizacionPagoFL.setConciliacionId(autorizacionPagaTodoReq.getPaConciliacionId());
                        peticionActualizacionPagoFL.setEstatusId(5);
                        peticionActualizacionPagoFL.setRespuestaAntad(respuestaConsultaBDCentral.getMensajePagatodo());

                        //llenar respuesta autorizacion q se valida en binding
                        autorizacionPagaTodoResp.setCodigoError(-6);
                        autorizacionPagaTodoResp.setMovimientoId(0);
                        autorizacionPagaTodoResp.setArregloBloqueos(null);
                        autorizacionPagaTodoResp.setRespuestaPagatodoDto(null);
                        autorizacionPagaTodoResp.setTransaccionId(0);

                        peticionActualizacionPagoFL.setTransaccionId(0);
                        respuestaActualizacionPagoFL = new RespuestaActualizacionPagoFueraLineaBean();
                        respuestaActualizacionPagoFL = dao.actualizarPagoFL(peticionActualizacionPagoFL, 5);

                        cancelarConciliacionVenta(false);
                        vm.limpiaParcial();

                    }

                }else{

                    //consulta exitosa no se encontro el pago
                    SION.log(Modulo.PAGO_SERVICIOS, "El pago no se encontro", Level.INFO);


                    //actualizar mensaje error
                    PagoServiciosDao dao = new PagoServiciosDao();
                    peticionActualizacionPagoFL = new PeticionActualizacionPagoFueraLineaBean();
                    peticionActualizacionPagoFL.setConciliacionId(autorizacionPagaTodoReq.getPaConciliacionId());
                    peticionActualizacionPagoFL.setEstatusId(5);
                    
                    if (respuestaConsultaBDCentral!=null && respuestaConsultaBDCentral.getMensajePagatodo()!=null){
                         peticionActualizacionPagoFL.setRespuestaAntad(respuestaConsultaBDCentral.getMensajePagatodo());
                    }else{
                        peticionActualizacionPagoFL.setRespuestaAntad("No se pudo consultar el pago");
                    }
                   

                    autorizacionPagaTodoResp.setCodigoError(-5);
                    autorizacionPagaTodoResp.setMovimientoId(0);
                    autorizacionPagaTodoResp.setArregloBloqueos(null);
                    autorizacionPagaTodoResp.setRespuestaPagatodoDto(null);
                    autorizacionPagaTodoResp.setTransaccionId(0);

                    peticionActualizacionPagoFL.setTransaccionId(0);
                    respuestaActualizacionPagoFL = new RespuestaActualizacionPagoFueraLineaBean();
                    respuestaActualizacionPagoFL = dao.actualizarPagoFL(peticionActualizacionPagoFL, 5);

                    cancelarConciliacionVenta(false);
                    vm.limpiaParcial();

                }



            }


        } else {

            cancelarConciliacionVenta(true);

            //se cancela operacion debido a que no se pudo guardar venta en bd local
            autorizacionPagaTodoResp = new AutorizacionPagaTodoDtoResp();

            //valida si existe respuesta de local lo cual indica un error generado en bd, caso contrario ocurrio una exceocion al conectar
            if (respuestaPagoLocal != null) {
                if (respuestaPagoLocal.getDescError() != null) {

                    autorizacionPagaTodoResp.setDescError(respuestaPagoLocal.getDescError());
                } else {
                    autorizacionPagaTodoResp.setDescError("Ocurrió un error al intentar aplicar el pago. Por favor, contacte a soporte técnico con el Folio de Conciliacion : "
                            + vm.getRespuestaConciliacionLocalBean().getPaConciliacionId());
                }
            }



            autorizacionPagaTodoResp.setCodigoError(-2);
            autorizacionPagaTodoResp.setMovimientoId(0);
            autorizacionPagaTodoResp.setArregloBloqueos(null);
            autorizacionPagaTodoResp.setRespuestaPagatodoDto(null);
            autorizacionPagaTodoResp.setTransaccionId(0);
        }


    }

    /*
     * public boolean seCompletoAutorizacion(){ return seCompletoAutorizacion; }
     */
    public void completarVentaExitosaPagoPendiente(double _montoRecibido, final boolean seRecibioRespuesta) {

        int intentosActualizacionFL = 3;
        int intento = 1;

        do {
            SION.log(Modulo.PAGO_SERVICIOS, "Intento No. " + intento + " de actualizacion del pago fuera de linea", Level.INFO);
            //intento de actualizacion del pago en nuevo estatus
            //en caso de no lograr actualizar no se hace nada en front
            actualizarEstatusLocal(PAGO_POR_CONFIRMAR_CENTRAL, seRecibioRespuesta, false);

        } while (intento <= intentosActualizacionFL && respuestaActualizacionPagoFL == null);

        if (respuestaPagoLocal != null) {

            if (respuestaPagoLocal.getCodigoError() == 0) {
                //pago fl exitoso

                //imprimirTicketPagatodo(_montoRecibido, _montoRecibido - autorizacionPagaTodoReq.getMontoTotal(), true, seRecibioRespuesta, false);

                //guardarVentaDia();

                pagatodoBinding.evaluarBloqueosVenta();

            }

        }

    }

    public void actualizarEstatusLocal(int _estatus, boolean _seRecibioRespuestaPagaTodo, boolean _seConsultoPago) {

        peticionActualizacionPagoFL = null;
        peticionActualizacionPagoFL = new PeticionActualizacionPagoFueraLineaBean();
        respuestaActualizacionPagoFL = null;
        respuestaActualizacionPagoFL = new RespuestaActualizacionPagoFueraLineaBean();
        PagoServiciosDao dao = new PagoServiciosDao();

        peticionActualizacionPagoFL.setConciliacionId(Long.valueOf(peticionPagaTodoBean.getConciliacion()));


        if (_estatus == PAGO_EXITOSO_CENTRAL) {
            if(_seConsultoPago){
                SION.log(Modulo.PAGO_SERVICIOS, "Actualizando transaccion: " + respuestaConsultaBDCentral.getTransaccionId(), Level.INFO);
                peticionActualizacionPagoFL.setTransaccionId(respuestaConsultaBDCentral.getTransaccionId());
            }else{
                SION.log(Modulo.PAGO_SERVICIOS, "Actualizando transaccion: " + autorizacionPagaTodoResp.getTransaccionId(), Level.INFO);
                peticionActualizacionPagoFL.setTransaccionId(autorizacionPagaTodoResp.getTransaccionId());
            }
        } else {
            //peticionActualizacionPagoFL.setTransaccionId(0);
        }
        peticionActualizacionPagoFL.setEstatusId(_estatus);

        String respuestaPagaTodo = "";

        if (_seRecibioRespuestaPagaTodo) {

            //entra este caso cuando se recibe una respuesta del ws antad y sera considerado por el demonio
            //en este caso solo falta ser aplicado en bd central pero se tiene certeza que el pago fue aplicado en antad

            boolean existeMensajeCajero = false;
            String mensajeRespuestaPagaodo = "";

            if(_seConsultoPago){

                mensajeRespuestaPagaodo = respuestaConsultaBDCentral.getMensajePagatodo();

                //agrega numero de autorizacion antad
                respuestaPagaTodo = String.valueOf(respuestaConsultaBDCentral.getFolioPagatodo()).concat("||").concat(mensajeRespuestaPagaodo);

            }else{

                if (autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrDescRespuesta() != null) {
                    if (!autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrDescRespuesta().isEmpty()) {
                        mensajeRespuestaPagaodo = autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrDescRespuesta();
                        existeMensajeCajero = true;
                    }
                }

                if (autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrDescRespuesta() != null) {
                    if (!autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrDescRespuesta().isEmpty()) {

                        if (existeMensajeCajero) {
                            mensajeRespuestaPagaodo = mensajeRespuestaPagaodo.concat("|");
                        }

                        mensajeRespuestaPagaodo = mensajeRespuestaPagaodo.concat(autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrDescRespuesta());
                    }
                }

                //agrega numero de autorizacion antad
                respuestaPagaTodo = String.valueOf(autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrNoConfirmacion()).concat("||").concat(mensajeRespuestaPagaodo);

            }

        } else {

            //entra cuando ocurre una excepcion donde no se pudo applicar el pago, se marca como fuera de linea para que la tome el demonio
            //puede ocurrir que sea tomada y marcada por demonio sin haber aplicado en antad, en este caso entra en la conciliacion

            //se concatena 0 como folio antad y msj generico

            respuestaPagaTodo = "0||".concat("Pago de Servicio PagaTodo registrado Fuera de linea. ");

        }



        //agrega campo operacion
        respuestaPagaTodo = String.valueOf(autorizacionPagaTodoReq.getAutorizacion().getCodigoBarras()).concat("||").concat(respuestaPagaTodo);

        //agrega campo SKU
        //respuestaAntad = String.valueOf(peticionAutorizacionAntad.getAutorizacion().getSKU()).concat("||").concat(respuestaAntad);
        //Se cambia 11/08/2015 por campo folio transaccion XCD para q se guarde en lugar de sku
        if(_seConsultoPago){
            respuestaPagaTodo = String.valueOf(respuestaConsultaBDCentral.getFolioPagatodo()).concat("||").concat(respuestaPagaTodo);
        }else{

            respuestaPagaTodo = String.valueOf(autorizacionPagaTodoReq.getAutorizacion().getReferencia()).concat("||").concat(respuestaPagaTodo);
        }
        //agrega campo modo ingreso
        respuestaPagaTodo = String.valueOf(autorizacionPagaTodoReq.getAutorizacion().getModoIngreso()).concat("||").concat(respuestaPagaTodo);

        peticionActualizacionPagoFL.setRespuestaAntad(respuestaPagaTodo);

        SION.log(Modulo.PAGO_SERVICIOS, "Peticion de actualizacion de pago: " + peticionActualizacionPagoFL.toString(), Level.INFO);

        respuestaActualizacionPagoFL = dao.actualizarPagoFL(peticionActualizacionPagoFL, _estatus);

        SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de actualizacion de pago: " + respuestaActualizacionPagoFL.toString(), Level.INFO);
    }

    /*public WSPagoServiciosPTServiceStub.AutorizacionBeanReq generaAutorizacionPT(String _referencia){
        AutorizacionBeanReq


    }*/



    public AutorizacionPTDto generaAutorizacion(String _referencia) {

        AutorizacionPTDto autorizacion = new AutorizacionPTDto();

         String t = VistaBarraUsuario.getSesion().getIpEstacion();

        String[] ipPartes = t.split("\\.");

        autorizacion.setCaja(ipPartes[3]);
        autorizacion.setCajero(String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()));
        autorizacion.setCodigoBarras(this.peticionPagaTodoBean.getCodigoBarras());


        /*comercio = Long.parseLong(respuestaConsultaPagatodo.getRespuestaConsultaSKUAntad().getConsultaResult().getComercio());

        autorizacion.setComercio(comercio);*/


        autorizacion.setComision(Double.valueOf(utilerias.getNumeroFormateado(Double.parseDouble(this.peticionPagaTodoBean.getComision()))));


        autorizacion.setEmisor(this.peticionPagaTodoBean.getEmisor());
        //autorizacion.setFolioComercio(Long.parseLong(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "antad.foliocomercio.neto").trim()));
        autorizacion.setModoIngreso("01");


        autorizacion.setMonto(Double.valueOf(this.peticionPagaTodoBean.getMonto()));



        //autorizacion.setOperacion(respuestaConsultaPagatodo.getRespuestaConsultaSKUAntad().getConsultaResult().getOperacion());
        autorizacion.setReferencia(_referencia);
        autorizacion.setReferencia2("");
        autorizacion.setReferencia3("");
        autorizacion.setReintento(0);
        autorizacion.setSKU(this.peticionPagaTodoBean.getSku());
        autorizacion.setSucursal(this.peticionPagaTodoBean.getSucursal());
        autorizacion.setPaisId(VistaBarraUsuario.getSesion().getPaisId());

        autorizacion.setTicket(String.valueOf(peticionPagaTodoBean.getConciliacion()).substring(0, 12));
        //autorizacion.setTicket(String.valueOf(peticionConsultaAntad.getPaConciliacionId()));

        return autorizacion;
    }

    public void grabarPagoFueraLinea() {

        SION.log(Modulo.PAGO_SERVICIOS, "Comienza proceso de pago Fuera de Linea", Level.INFO);

        peticionPagoLocal = null;
        peticionPagoLocal = new PeticionPagoServicioLocalBean();

        //inicializar

        String t = VistaBarraUsuario.getSesion().getIpEstacion();
        t = t.concat("|CAJA");
        t = t.concat(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));


        peticionPagoLocal.setArrayPagos(tipoPagoDto2Bean(autorizacionPagaTodoReq.getArregloTiposPago()));
        peticionPagoLocal.setArrayServicios(autorizacion2PagoServicioFL());
        peticionPagoLocal.setConciliacionId(autorizacionPagaTodoReq.getPaConciliacionId());
        peticionPagoLocal.setIndicador(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "pago.local.indicador")));//7
        peticionPagoLocal.setModuloId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.MODULO")));//2
        peticionPagoLocal.setPais(autorizacionPagaTodoReq.getPaPaisId());
        peticionPagoLocal.setSistemaId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SISTEMA")));//1
        peticionPagoLocal.setSubModuloId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SUBMODULO.PAGATODO")));//11
        peticionPagoLocal.setTerminal(t);
        peticionPagoLocal.setTiendaId(autorizacionPagaTodoReq.getTiendaId());
        peticionPagoLocal.setTotalVenta(autorizacionPagaTodoReq.getMontoTotal());
        peticionPagoLocal.setUsuarioId(autorizacionPagaTodoReq.getUsuarioId());

        respuestaPagoLocal = null;
        respuestaPagoLocal = new RespuestaPagoServicioLocalBean();

        SION.log(Modulo.PAGO_SERVICIOS, "Peticion de registro de PS Pagatodo fuera de linea: " + peticionPagoLocal.toString(), Level.INFO);

        respuestaPagoLocal = pagoFueraLinea.spPagoServicioLocal(peticionPagoLocal);

        SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de registro de PS Pagatodo: " + respuestaPagoLocal.toString(), Level.INFO);

    }

    public TipoPagoPTDto[] generaArregloTiposPago(double _montoRecibido) {

        TipoPagoPTDto[] arreglo = new TipoPagoPTDto[1];

        TipoPagoPTDto tipoPago = new TipoPagoPTDto();

        tipoPago.setFnMontoPago(autorizacionPagaTodoReq.getMontoTotal());
        tipoPago.setImporteAdicional(_montoRecibido);
        tipoPago.setFnNumeroVales(0);
        tipoPago.setPaPagoTarjetaIdBus(0);
        tipoPago.setFiTipoPagoId(1);

        arreglo[0] = tipoPago;

        return arreglo;
    }

    private PagoServicioBean[] autorizacion2PagoServicioFL() {
        ArrayList<PagoServicioBean> pagos = new ArrayList<PagoServicioBean>();

        PagoServicioBean pago = new PagoServicioBean();

        pago.setAdicionales(autorizacionPagaTodoReq.getAutorizacion().getSKU());
        pago.setArticuloId(Long.parseLong(autorizacionPagaTodoReq.getAutorizacion().getEmisor()));
        pago.setCodigoBarras(peticionPagaTodoBean.getCodigoBarras());
        pago.setUid(String.valueOf(autorizacionPagaTodoReq.getUId()));
        pago.setComision(autorizacionPagaTodoReq.getAutorizacion().getComision());
        pago.setImpuesto(0);
        pago.setMonto(autorizacionPagaTodoReq.getAutorizacion().getMonto());
        pago.setParametros("");
        pago.setReferencia(autorizacionPagaTodoReq.getAutorizacion().getReferencia());

        pagos.add(pago);

        return pagos.toArray(new PagoServicioBean[0]);
    }

    private TipoPagoBean[] tipoPagoDto2Bean(TipoPagoPTDto[] _tiposPago) {
        ArrayList<TipoPagoBean> tiposPago = new ArrayList<TipoPagoBean>();

        for (TipoPagoPTDto dto : _tiposPago) {
            TipoPagoBean bean = new TipoPagoBean();

            bean.setFcReferenciaPagoTarjetas(String.valueOf(dto.getPaPagoTarjetaIdBus()));
            bean.setFiTipoPagoId(dto.getFiTipoPagoId());
            bean.setFnImporteAdicional(dto.getImporteAdicional());
            bean.setFnMontoPago(dto.getFnMontoPago());
            bean.setFnNumeroVales(dto.getFnNumeroVales());

            tiposPago.add(bean);
        }

        return tiposPago.toArray(new TipoPagoBean[0]);
    }

    public neto.sion.pago.servicios.cliente.dto.BloqueoDto[] bloqueosAntad2Switch(BloqueoDto[] _bloqueosAntad) {

        ArrayList<neto.sion.pago.servicios.cliente.dto.BloqueoDto> listaBloqueos = new ArrayList<neto.sion.pago.servicios.cliente.dto.BloqueoDto>();

        for (BloqueoDto bloqueoAntad : _bloqueosAntad) {
            neto.sion.pago.servicios.cliente.dto.BloqueoDto bloqueo = new neto.sion.pago.servicios.cliente.dto.BloqueoDto();

            bloqueo.setFiAvisosFalt(bloqueoAntad.getFiAvisosFalt());
            bloqueo.setFiEstatusBloqueoId(bloqueoAntad.getFiEstatusBloqueoId());
            bloqueo.setFiNumAvisos(bloqueoAntad.getFiNumAvisos());
            bloqueo.setFiTipoPagoId(bloqueoAntad.getFiTipoPagoId());
            bloqueo.setTiendaId(bloqueoAntad.getTiendaId());

            listaBloqueos.add(bloqueo);
        }

        return listaBloqueos.toArray(new neto.sion.pago.servicios.cliente.dto.BloqueoDto[0]);
    }

    public void actualizarTransacciónLocal(int sistema, int modulo, int submodulo) {

        SION.log(Modulo.PAGO_SERVICIOS, "Actualizando conciliacion en BD local", Level.INFO);

        PeticionActTransaccionLocalBean peticionActTransaccionLocalBean = new PeticionActTransaccionLocalBean();
        RespuestaActualizacionLocalBean respuestaActualizacionLocalBean = new RespuestaActualizacionLocalBean();


        peticionActTransaccionLocalBean.setPaConciliacionId(autorizacionPagaTodoReq.getPaConciliacionId());
        peticionActTransaccionLocalBean.setPaTransaccionVenta(autorizacionPagaTodoResp.getTransaccionId());

        SION.log(Modulo.PAGO_SERVICIOS, "Petición de actualización de transacción local : " + peticionActTransaccionLocalBean.toString(), Level.INFO);

        ConciliacionLocalControlador conciliacionLocalControlador = new ConciliacionLocalControlador();

        respuestaActualizacionLocalBean = conciliacionLocalControlador.actualizarTransaccionLocal(peticionActTransaccionLocalBean);

        int contador = 0;

        while (true && contador < 3) {
            if (respuestaActualizacionLocalBean.getPaCdgError() == 0) {
                break;
            } else {
                respuestaActualizacionLocalBean = conciliacionLocalControlador.actualizarTransaccionLocal(peticionActTransaccionLocalBean);
            }

            contador++;
        }

        SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de la petición de actualización de la transacción local : " + respuestaActualizacionLocalBean.toString(), Level.INFO);

    }

    public void cancelarConciliacionVenta(boolean _soloSeTieneConciliacion) {

        SION.log(Modulo.PAGO_SERVICIOS, "Cancelando conciliacion " + peticionPagaTodoBean.getConciliacion() + " de venta en BD local", Level.INFO);

        PeticionActTransaccionLocalBean peticionActTransaccionLocalBean = new PeticionActTransaccionLocalBean();

        peticionActTransaccionLocalBean.setPaConciliacionId(Long.valueOf(peticionPagaTodoBean.getConciliacion()));

        if (_soloSeTieneConciliacion) {
            peticionActTransaccionLocalBean.setPaTransaccionVenta(0);
        } else {
            peticionActTransaccionLocalBean.setPaTransaccionVenta(autorizacionPagaTodoResp.getTransaccionId());
        }

        ConciliacionLocalControlador conciliacionLocalControlador = new ConciliacionLocalControlador();
        conciliacionLocalControlador.updCancelacionConciliacion(peticionActTransaccionLocalBean);


    }

    //validaciones
    private String getCodigoBarrasFromListaEmpresas(long _emisor, RespuestaEmpresasBean _respuestaEmpresasBean) {
        String codigoBarras = "";

        for (EmpresaBean empresaBean : _respuestaEmpresasBean.getEmpresaBeans()) {
            if (empresaBean != null) {
                if (_emisor == empresaBean.getEmpresaId()) {
                    codigoBarras = empresaBean.getCodigoBarras();
                    break;
                }
            }
        }

        return codigoBarras;
    }

    public boolean esEmisorPagatodo(long _emisor, RespuestaEmpresasBean _respuestaEmpresasBean) {
        boolean esPagatodo = false;

        for (EmpresaBean empresa : _respuestaEmpresasBean.getEmpresaBeans()) {
            if (empresa != null) {
                if (_emisor == empresa.getEmpresaId()) {
                    if (empresa.getAgrupacionId() == AGRUPACION_PAGATODO) {
                        esPagatodo = true;
                        break;
                    }
                }
            }
        }

        return esPagatodo;
    }







     public boolean existePagoPagatodo() {
        boolean existePago = false;

        for (ServicioBean servicio : vm.getListaPagosAcumulados()) {
            if (esEmisorPagatodo(servicio.getEmisorId(), vm.getRespuestaEmpresasBean())) {
                existePago = true;
                break;
            }
        }

        return existePago;
    }


    public boolean esEmisorPagatodo(long _emisor) {
        return esEmisorPagatodo(_emisor, vm.getRespuestaEmpresasBean());
    }

    //impresion ticket
    public void imprimirTicketPagatodo(double _montoRecibido, double _montoCambio, boolean _esFueraLinea, boolean _seRecibioRespuestaPagatodo, boolean _seConsultoPago) {

        ImpresionVenta impresionControlador = new ImpresionVenta();
        VentaServiciosTicketDto ticketPagatodo = generarTicketPagatodo(_montoRecibido, _montoCambio, impresionControlador, _esFueraLinea, _seRecibioRespuestaPagatodo, _seConsultoPago);

        seImprimioTicketExitoso = false;

        if (ticketPagatodo != null) {

            SION.log(Modulo.PAGO_SERVICIOS, "Ticket a imprimir: " + ticketPagatodo.toString(), Level.INFO);

            //se valida que solo se imprima ticket si hubo algun pago exitoso

            if (ticketPagatodo.getServicios().length > 0) {

                try {
                    ticketPagatodo.setIvaTotal();

                    SION.log(Modulo.PAGO_SERVICIOS, "TotalLetras : " + ticketPagatodo.getTotalLetras(), Level.INFO);


                    impresionControlador.imprimirTicketServicios(ticketPagatodo);
                    seImprimioTicketExitoso = true;
                    SION.log(Modulo.PAGO_SERVICIOS, "El ticket se imprimio correctamente", Level.INFO);
                } catch (Exception ex) {
                    SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex);
                    SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió una excepcion al imprimir ticket de pago", Level.SEVERE);

                } catch (Error err) {
                    SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un error al imprimir el ticket de pago", Level.SEVERE);

                }
            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "No se encontraron servicios exitosos, no se imprime ticket", Level.WARNING);
                seImprimioTicketExitoso = true;
            }
        } else {

            //Error al construir ticket
            SION.log(Modulo.PAGO_SERVICIOS, "Instancia de ticket nula, se continua con el proceso y se muestra msj de error en la impresion", Level.WARNING);

        }

    }

    public boolean seImprimioTicket() {
        return seImprimioTicketExitoso;
    }

    private VentaServiciosTicketDto generarTicketPagatodo(double _montoRecibido, double _montoCambio, ImpresionVenta _impresionControlador, boolean _esFueraLinea, boolean _seRecibioRespuestaPagatodo, boolean _seConsultoPago) {

        VentaServiciosTicketDto ticket = new VentaServiciosTicketDto();

        SION.log(Modulo.PAGO_SERVICIOS, "Construyendo ticket de pago de servicios Pagatodo", Level.INFO);
        String cadenaDireccion = "";
        String cadenaDelegacion = "";

        boolean esSuperPrecio = false;

        RespuestaParametroBean parametro = new RespuestaParametroBean();
        
        parametro = utilerias.consultaParametro(UtileriasVenta.Accion.ENCABEZADO_TICKET, Modulo.VENTA, Modulo.PAGO_SERVICIOS);
        if (parametro != null) {
            if (parametro.getCodigoError() == 0 && parametro.getValorConfiguracion() != 1) {
                esSuperPrecio = true;
            }
        }

        try {

            cadenaDireccion = cadenaDireccion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getCalle()));
            cadenaDireccion = cadenaDireccion.concat(" ");
            cadenaDireccion = cadenaDireccion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroExterior()));
            if (!"".equals(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior())
                    && !"0".equals(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior())
                    && VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior() != null) {
                cadenaDireccion = cadenaDireccion.concat("-");
                cadenaDireccion = cadenaDireccion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior()));
            }

            cadenaDelegacion = cadenaDelegacion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getMunicipio()).concat(" ").concat(
                    String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getEstado())).concat(" ").concat(
                    String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getCodigoPostal())));


            ticket.setCaja(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));
            ticket.setCajero(String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()));
            ticket.setCalleNumeroTienda(cadenaDireccion);
            ticket.setColoniaTienda(VistaBarraUsuario.getSesion().getDatosTienda().getColonia());
            ticket.setDelegacionCiudadEstadoCP(cadenaDelegacion);
            if (esSuperPrecio) {
                ticket.setEncabezado(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ENCABEZADO.TICKET.SUPR"));
            } else {
                ticket.setEncabezado(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ENCABEZADO.TICKET"));
            }
            ticket.setFecha(obtenerFecha().substring(0, obtenerFecha().indexOf(" ")));
            ticket.setHora(obtenerFecha().substring(10));
            ticket.setNombreTienda(VistaBarraUsuario.getSesion().getNombreTienda());
            if (_esFueraLinea) {
                ticket.setFolio(String.valueOf(respuestaPagoLocal.getConciliacionId()).concat("O"));
                ticket.setTransaccion(String.valueOf(respuestaPagoLocal.getConciliacionId()).concat("O"));

            } else {
                if(_seConsultoPago){
                    ticket.setFolio(String.valueOf(respuestaConsultaBDCentral.getTransaccionId()).concat("I"));
                    ticket.setTransaccion(String.valueOf(respuestaConsultaBDCentral.getTransaccionId()).concat("I"));
                }else{

                    ticket.setFolio(String.valueOf(autorizacionPagaTodoResp.getTransaccionId()).concat("I"));
                    ticket.setTransaccion(String.valueOf(autorizacionPagaTodoResp.getTransaccionId()).concat("I"));
                }
            }
            ticket.setServicios(arrayAutorizacionAntad2ServicioTicketDto(_seRecibioRespuestaPagatodo, _seConsultoPago));

            ticket.setTiendaId(String.valueOf(VistaBarraUsuario.getSesion().getTiendaId()));
            ticket.setTotalComision(utilerias.getNumeroFormateado(autorizacionPagaTodoReq.getAutorizacion().getComision()));
            ticket.setTiposPago(_impresionControlador.tipoPagoDtoaTicket(arrayTiposPagoServicioAntad2Venta(), autorizacionPagaTodoReq.getMontoTotal(), _montoRecibido, _montoCambio));
            ticket.setTotalServicios();
            ticket.setTotalLetras(String.valueOf(_impresionControlador.getTotalLetras()));

        } catch (Exception e) {
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió una excepción al construir el ticket", Level.SEVERE);
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e, "Ocurrió una excepción al construir el ticket");
            ticket = null;
        } catch (Error e){
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió un error al construir el ticket", Level.SEVERE);
            
            SION.log(Modulo.PAGO_SERVICIOS, "Error al generar el ticket : " + e.getMessage()+ " " + e.getCause() + " " + e.getLocalizedMessage() , Level.SEVERE);
            
            ticket = null;
        }

        return ticket;
    }

    //venta dia
    public void guardarVentaDia() {

        PeticionVentaDiaBean peticionVentaDiaBean = new PeticionVentaDiaBean();
        RespuestaVentaDiaBean respuestaVentaDiaBean = new RespuestaVentaDiaBean();

        VentaDiaBean[] pagosDia = cargarPagosDia();

        if (pagosDia != null) {

            peticionVentaDiaBean.setVentaDiaBeans(pagosDia);

            PagoServiciosDao dao = new PagoServiciosDao();
            respuestaVentaDiaBean = dao.guardarVentaDia(peticionVentaDiaBean);

            if (respuestaVentaDiaBean != null) {
                if (respuestaVentaDiaBean.getCodigoError() == 0) {
                    SION.log(Modulo.PAGO_SERVICIOS, "La venta se guardo exitosamente", Level.INFO);
                } else {
                    SION.log(Modulo.PAGO_SERVICIOS, respuestaVentaDiaBean.getDescripcionError(), Level.SEVERE);
                }
            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de inserción de venta del día nula", Level.SEVERE);
            }
        } else {
            SION.log(Modulo.PAGO_SERVICIOS, "No hay pagos que registrar como venta del día en esta operación", Level.WARNING);
        }
    }

    private VentaDiaBean[] cargarPagosDia() {

        ArrayList<VentaDiaBean> listaVentas = new ArrayList<VentaDiaBean>();

        int iepsId = 0;

        try {
            iepsId = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "servicio.iepsId").trim());
        } catch (NumberFormatException e) {
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e, "Ocurrio un error al consultar iepsId desde archivo de propiedades, se asiga 69 default");
            iepsId = 69;
        }

        VentaDiaBean pagoDia = new VentaDiaBean();
        pagoDia.setArticuloId(Long.parseLong(autorizacionPagaTodoReq.getAutorizacion().getEmisor()));
        pagoDia.setCantidad(1);
        pagoDia.setCodigoBarras(autorizacionPagaTodoReq.getAutorizacion().getCodigoBarras());
        pagoDia.setCosto(autorizacionPagaTodoReq.getMontoTotal());
        pagoDia.setDescuento(0);
        pagoDia.setIva(0);
        pagoDia.setPrecio(0);
        pagoDia.setIepsId(iepsId);

        listaVentas.add(pagoDia);

        return listaVentas.toArray(new VentaDiaBean[0]);
    }

    //utilerias
    private neto.sion.pago.servicios.cliente.dto.BloqueoDto[] arrayBloqueosAntad2Venta() {
        ArrayList<neto.sion.pago.servicios.cliente.dto.BloqueoDto> listaBloqueos = new ArrayList<neto.sion.pago.servicios.cliente.dto.BloqueoDto>();


        for (BloqueoPagatodoDto bloqueoPT : autorizacionPagaTodoResp.getArregloBloqueos()) {
            neto.sion.pago.servicios.cliente.dto.BloqueoDto bloqueoVenta = new neto.sion.pago.servicios.cliente.dto.BloqueoDto();
            bloqueoVenta.setFiAvisosFalt(bloqueoPT.getFiAvisosFalt());
            bloqueoVenta.setFiEstatusBloqueoId(bloqueoPT.getFiEstatusBloqueoId());
            bloqueoVenta.setFiNumAvisos(bloqueoPT.getFiNumAvisos());
            bloqueoVenta.setFiTipoPagoId(bloqueoPT.getFiTipoPagoId());
            listaBloqueos.add(bloqueoVenta);
        }

        return listaBloqueos.toArray(new neto.sion.pago.servicios.cliente.dto.BloqueoDto[0]);
    }

    private ArrayList<neto.sion.venta.servicios.cliente.dto.TipoPagoDto> arrayTiposPagoServicioAntad2Venta() {
        ArrayList<neto.sion.venta.servicios.cliente.dto.TipoPagoDto> listaTiposPago = new ArrayList<neto.sion.venta.servicios.cliente.dto.TipoPagoDto>();

        for (TipoPagoPTDto tipoPago : autorizacionPagaTodoReq.getArregloTiposPago()) {
            neto.sion.venta.servicios.cliente.dto.TipoPagoDto tipoPagoDto = new neto.sion.venta.servicios.cliente.dto.TipoPagoDto();
            tipoPagoDto.setImporteAdicional(tipoPago.getImporteAdicional());
            tipoPagoDto.setFnMontoPago(tipoPago.getFnMontoPago());
            tipoPagoDto.setFnNumeroVales(tipoPago.getFnNumeroVales());
            tipoPagoDto.setPaPagoTarjetaIdBus(tipoPago.getPaPagoTarjetaIdBus());
            tipoPagoDto.setFiTipoPagoId(tipoPago.getFiTipoPagoId());
            listaTiposPago.add(tipoPagoDto);
        }

        return listaTiposPago;
    }

    public ServicioTicketDto[] arrayAutorizacionAntad2ServicioTicketDto(boolean _seRecibioRespuesta, boolean _seConsultoPago) {
        ArrayList<ServicioTicketDto> listaServicios = new ArrayList<ServicioTicketDto>();

        ServicioTicketDto servicioTicket = new ServicioTicketDto();

        servicioTicket.setComision(String.valueOf(autorizacionPagaTodoReq.getAutorizacion().getComision()));
        servicioTicket.setConceptoImpuesto("0");
        
        //servicioTicket.setEmisora(String.valueOf(peticionAutorizacionAntad.getAutorizacion().getEmisor()));
        servicioTicket.setEmisora(buscaNombreEmisorXId(Integer.parseInt(autorizacionPagaTodoReq.getAutorizacion().getEmisor())));
        servicioTicket.setEsConceptoImpuesto(false);
        servicioTicket.setImporte(utilerias.getNumeroFormateado(autorizacionPagaTodoReq.getAutorizacion().getMonto()));
        servicioTicket.setIva(0);

        if (_seRecibioRespuesta) {
            if(_seConsultoPago){
                servicioTicket.setLeyenda(autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrDescMsgObligatorio());

                servicioTicket.setNoAutorizacion(String.valueOf(respuestaConsultaBDCentral.getFolioPagatodo()));

                servicioTicket.setFolioXCD("");

            }else{
                servicioTicket.setLeyenda(autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrDescMsgObligatorio());
                servicioTicket.setNoAutorizacion(autorizacionPagaTodoResp.getRespuestaPagatodoDto().getrNoConfirmacion());
                servicioTicket.setFolioXCD("");
            }
        } else {
            //cuando se recibe excepcion y se marca fl
            servicioTicket.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.ANTAD.MENSAJE.TICKETFL"));
            servicioTicket.setNoAutorizacion("0");
            servicioTicket.setFolioXCD("");
        }

        servicioTicket.setEsConceptoImpuesto(false);
        servicioTicket.setConceptoImpuesto("");

        servicioTicket.setReferencia(autorizacionPagaTodoReq.getAutorizacion().getReferencia());

        listaServicios.add(servicioTicket);

        return listaServicios.toArray(new ServicioTicketDto[0]);
    }

    public String buscaNombreEmisorXId(int _emisor) {
        String nombre = "";

        for (EmpresaBean bean : vm.getListaEmpresas()) {
            if (bean != null) {
                if (bean.getEmpresaId() == _emisor) {
                    nombre = bean.getDescrpcion();
                    break;
                }
            }
        }

        return nombre;
    }

    public String buscaSKUxId(int _emisor) {
        String sku = "";

        for (EmpresaBean bean : vm.getListaEmpresas()) {
            if (bean != null) {
                if (bean.getEmpresaId() == _emisor) {
                    sku = bean.getCodigoBarras();
                    break;
                }
            }
        }

        return sku;
    }





    public String obtenerFecha() {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar Cal = Calendar.getInstance();
        return dateFormat.format(new Date());
    }

    public void limpiarAntad() {
        SION.log(Modulo.PAGO_SERVICIOS, "Limpiando variables Pagatodo", Level.INFO);
        //peticionPagaTodoBean = null;
        autorizacionPagaTodoResp = null;
        autorizacionPagaTodoReq = null;

    }

    public void limpiar() {
        vm.limpiar();
    }
  
    
    public ConsultaComisionDtoResp consultaComisionXEmisorId(long emisorId)
  {
    ConsultaComisionDtoResp consultaComisionDto = new ConsultaComisionDtoResp();
    try
    {
      consultaComisionDto = this.clientePagaTodo.consultaComisionXIdArticulo(String.valueOf(emisorId));
    }
    catch (Exception ex)
    {
      SION.log(Modulo.PAGO_SERVICIOS, ex.getMessage(), Level.SEVERE);
      
      consultaComisionDto.setCodigoError(1);
      consultaComisionDto.setArrayDetalleArticulos(null);
      consultaComisionDto.setDescError("Ocurrio un error en la consulta de la comision del emisor el favor de intentarlo de nuevo");
    }
    return consultaComisionDto;
  }

}
