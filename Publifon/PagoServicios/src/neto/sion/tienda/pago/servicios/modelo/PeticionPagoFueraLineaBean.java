/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

import java.util.Arrays;
import neto.sion.pago.servicios.cliente.dto.PeticionPagoServicioDto;
import neto.sion.pago.servicios.cliente.dto.TipoPagoServicioDto;
import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;

/**
 *
 * @author fvega
 */
public class PeticionPagoFueraLineaBean extends PeticionBaseVentaDto{
    
    private long usuarioId;
    private String paFechaOper;
    private int paTipoMovto;
    private double paMontoTotalVta;
    private TipoPagoServicioDto[] arrayTiposPago;
    private PeticionPagoServicioDto[] arrayPagosservicio;

    public PeticionPagoServicioDto[] getArrayPagosservicio() {
        return arrayPagosservicio;
    }

    public void setArrayPagosservicio(PeticionPagoServicioDto[] arrayPagosservicio) {
        this.arrayPagosservicio = arrayPagosservicio;
    }

    public TipoPagoServicioDto[] getArrayTiposPago() {
        return arrayTiposPago;
    }

    public void setArrayTiposPago(TipoPagoServicioDto[] arrayTiposPago) {
        this.arrayTiposPago = arrayTiposPago;
    }

    public String getPaFechaOper() {
        return paFechaOper;
    }

    public void setPaFechaOper(String paFechaOper) {
        this.paFechaOper = paFechaOper;
    }

    public double getPaMontoTotalVta() {
        return paMontoTotalVta;
    }

    public void setPaMontoTotalVta(double paMontoTotalVta) {
        this.paMontoTotalVta = paMontoTotalVta;
    }

    public int getPaTipoMovto() {
        return paTipoMovto;
    }

    public void setPaTipoMovto(int paTipoMovto) {
        this.paTipoMovto = paTipoMovto;
    }

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public String toString() {
        return "PeticionPagoFueraLineaBean{" + "usuarioId=" + usuarioId + 
                ", paFechaOper=" + paFechaOper + ", paTipoMovto=" + paTipoMovto + 
                ", paMontoTotalVta=" + paMontoTotalVta + ", arrayTiposPago=" + Arrays.toString(arrayTiposPago) + 
                ", arrayPagosservicio=" + Arrays.toString(arrayPagosservicio) + '}';
    }
       
}
