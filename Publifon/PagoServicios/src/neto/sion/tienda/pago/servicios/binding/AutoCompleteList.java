/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.binding;

/**
 *
 * @author fvega
 */
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Popup;
import javafx.util.Callback;
import neto.sion.tienda.pago.servicios.vistamodelo.NodeFactory;

public class AutoCompleteList<ItemType> extends BorderPane {

    private TextField textField;
    private Popup popup;
    private ListView<ItemType> availableList;
    private ObservableList<ItemType> chosenItems;
    private ObservableList<Cell> chosenItemCells;
    private FlowPane content;
    private NodeFactory<ItemType> nodeFactory;
    private ScrollPane scrollPane;
    private StringBuilder builder;

    public AutoCompleteList(double _width, double _height) {
        buildView(_width, _height);
        this.builder = new StringBuilder();
    }

    public TextField getTextField() {
        return textField;
    }

    public void setFocusCampoTexto() {
        textField.requestFocus();
    }

    public ListView<ItemType> getList() {
        return availableList;
    }

    public void setText(String _text) {
        textField.setText(_text);
    }

    public Property<String> inputText() {
        return textField.textProperty();
    }

    public void setCellFactory(Callback<ListView<ItemType>, ListCell<ItemType>> callback) {
        availableList.setCellFactory(callback);
    }

    public ObservableList<ItemType> getAvailableItems() {
        return availableList.getItems();
    }

    public ObservableList<ItemType> getChosenItems() {
        return chosenItems;
    }

    public MultipleSelectionModel<ItemType> getAvailableItemsSelectionModel() {
        return availableList.getSelectionModel();
    }

    public void setNodeFactory(NodeFactory<ItemType> nodeFactory) {
        this.nodeFactory = nodeFactory;
    }

    public BorderPane getPanelPrincipal(){
        return this;
    }
    
    private void buildView(double _width, double _height) {
        getStyleClass().add("text-area");

        StringBuilder builder = new StringBuilder().append("-fx-border-color: #E6E6E6; -fx-border-style: "
                + "solid; -fx-border-width: 4; -fx-alignment:center;" + "-fx-background-color: white; "
                + "-fx-border-radius:10px;-fx-background-radius: 11px;");
        setStyle(builder.toString());


        chosenItems = FXCollections.observableArrayList();
        chosenItemCells = FXCollections.observableArrayList();
        content = new FlowPane(Orientation.HORIZONTAL, 3, 3);


        textField = new TextField();
        textField.setPromptText("Escriba el nombre de la empresa");
//        inputField.getStyleClass().add("auto-complete-list-field");
        textField.setStyle("-fx-background-color: white; -fx-font: 22pt Verdana;"
               + "-fx-border-color: transparent; -fx-border-width: 0; -fx-alignment: center;");
        //textField.getStyleClass().add("TextField");
        textField.setMaxSize(_width + 100, _height);
        textField.setMinSize(_width + 100, _height);
        textField.setPrefSize(_width + 100, _height);

        /*
         * inputField.textProperty().addListener(new ChangeListener<String>() {
         * @Override public void changed(ObservableValue<? extends String>
         * observableValue, String s, String s1) { int cols = s1 != null ?
         * Math.max(2, s1.length() + 1) : 2; inputField.setPrefWidth(cols * 8);
         * } });
         */

        textField.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> source, String oldValue, String newValue) {
                if (newValue != null && newValue.trim().length() > 0 && !popup.isShowing()) {
                    showPopup();
                }
            }
        });
        content.getChildren().add(textField);

        scrollPane = new ScrollPane();
        scrollPane.setFitToWidth(true);
        scrollPane.setContent(content);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        //content.prefWrapLengthProperty().bind(scrollPane.prefWidthProperty().subtract(10));
        setCenter(scrollPane);

        scrollPane.setStyle("-fx-border-color: transparent;-fx-border-width: 0; -fx-alignment: center;");
        scrollPane.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                textField.requestFocus();
            }
        });


        content.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent mouseEvent) {
                textField.requestFocus();
            }
        });

        createPopup(_width, _height);
    }

    protected void showPopup() {
        double screenX = textField.getScene().getWindow().getX() + textField.getScene().getX() + textField.localToScene(0, 0).getX();
        double screenY = textField.getScene().getWindow().getY() + textField.getScene().getY() + textField.localToScene(0, 0).getY();
        popup.show(textField, screenX, screenY + textField.getHeight());
    }

    protected void listItemChosen() {
        ObservableList<ItemType> selectedItems = availableList.getSelectionModel().getSelectedItems();
        if (selectedItems != null && selectedItems.size() > 0 && selectedItems.get(0) != null) {
            ItemType selected = selectedItems.get(0);
            Cell cell = new Cell(selected);
            chosenItemCells.add(cell);
            chosenItems.add(selected);
            content.getChildren().add(content.getChildren().size() - 1, cell);
            scrollPane.setVvalue(scrollPane.getVmax());
            layout();
            popup.hide();
        }
    }

    public boolean isPopupShowing() {
        return popup.showingProperty().get();
    }

    public void ocultarPopup() {
        popup.hide();
    }

    protected void createPopup(double _width, double _height) {
        popup = new Popup();
        availableList = new ListView<ItemType>();
        availableList.setMaxSize(_width + 100, _height * 5);
        availableList.setMinSize(_width + 100, _height * 5);
        availableList.setPrefSize(_width + 100, _height * 5);
        availableList.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent mouseEvent) {
                listItemChosen();
            }
        });

        availableList.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode().equals(KeyCode.ENTER)) {
                    listItemChosen();
                } else if (keyEvent.getCode().equals(KeyCode.ESCAPE)) {
                    ocultarPopup();
                }
            }
        });

        /*
         * popup.showingProperty().addListener(new ChangeListener<Boolean>() {
         * @Override public void changed(ObservableValue<? extends Boolean>
         * source, Boolean oldValue, Boolean newValue) { if (!newValue) {
         * inputField.setText("");
         *
         * }
         * }
         * });
         */

        popup.getContent().add(availableList);
        popup.setAutoHide(true);
    }

    //-------------------------------------------------------------------------
    private class Cell extends HBox {

        private Hyperlink deleteButton;

        private Cell(final ItemType item) {
            setSpacing(6);

            StringBuilder builder = new StringBuilder().append("-fx-border-color: #cccccc;").append("-fx-border-radius: 5;").append("-fx-padding: 2 5 2 5;").append("-fx-background-radius: 5;").append("-fx-background-color: #ffffdd;");
            setStyle(builder.toString());

            Node node;
            if (nodeFactory != null) {
                node = nodeFactory.createNode(item);
            } else {
                node = new Label(String.valueOf(item));
            }
            //getChildren().add(node);

            deleteButton = new Hyperlink("X");
            deleteButton.setStyle("-fx-padding: 0; -fx-text-fill: blue");
            deleteButton.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    chosenItemCells.remove(Cell.this);
                    chosenItems.remove(item);
                    content.getChildren().remove(Cell.this);
                    layout();
                }
            });
            //getChildren().add(deleteButton);
        }
    }
}