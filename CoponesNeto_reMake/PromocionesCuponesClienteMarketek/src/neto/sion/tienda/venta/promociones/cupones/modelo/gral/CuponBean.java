/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.gral;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import neto.sion.tienda.venta.promociones.cupones.modelo.addcupon.CuponBeanResp;

/**
 *
 * @author dramirezr
 */
public class CuponBean {
    public SimpleStringProperty idCupon = new SimpleStringProperty();
    public SimpleBooleanProperty activo = new SimpleBooleanProperty(false);
    public SimpleBooleanProperty automatico = new SimpleBooleanProperty(false);
    public SimpleStringProperty mensaje = new SimpleStringProperty();
    private CuponBeanResp respCupon = new CuponBeanResp();
    //public SimpleDoubleProperty importeDescuento = new SimpleDoubleProperty();
    
    public CuponBean(){
        this("", false, "", false, null);
    }

    public CuponBean(String idCupon, boolean activo, String mensaje, boolean automatico, CuponBeanResp respCupon) {
        this.idCupon.set(idCupon);
        this.activo.set(activo);
        this.mensaje.set(mensaje);
        this.automatico.set(automatico);
        this.respCupon = respCupon;
    }
    
    

    public boolean isActivo() {
        return activo.get();
    }

    public void setActivo(boolean activo) {
        this.activo.set(activo);
    }


    public String getIdCupon() {
        return idCupon.get();
    }

    public void setIdCupon(String idCupon) {
        this.idCupon.set(idCupon);
    }

    public String getMensaje() {
        return mensaje.get();
    }

    public void setMensaje(String mensaje) {
        this.mensaje.set(mensaje);
    }

    public boolean getAutomatico() {
        return automatico.get();
    }

    public void setAutomatico(boolean automatico) {
        this.automatico.set(automatico);
    }

    public CuponBeanResp getRespCupon() {
        return respCupon;
    }

    public void setRespCupon(CuponBeanResp respCupon) {
        this.respCupon = respCupon;
    }
    
    

    @Override
    public String toString() {
        return "CuponBean{" + "idCupon=" + idCupon + ", activo=" + activo + ", automatico=" + automatico + ", mensaje=" + mensaje + '}';
    }
      
}
