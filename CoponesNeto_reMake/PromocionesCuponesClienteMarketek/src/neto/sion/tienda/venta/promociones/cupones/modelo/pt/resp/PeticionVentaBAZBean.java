package neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp;


/**
 * Clase que representa un mensaje200 de solicitud de venta a BAZ, en comentario
 * info del campo en contrato de interfaz, tama�o, nombre y no. de campo
 * 
 * @author fvegap
 * 
 */
public class PeticionVentaBAZBean {

	private String longitudMensaje200; // 4 Inicio de cadena
	private String tipoMensaje; // 4-MSG_TYPE-MTI
	private String bitmap; // 16-PRIMARY_BIT_MAP-000
	private String codigoProceso; // 6-PROC_CODE-003
	private String montoOperacion; // 12-TRANS_AMT-004
	private String fecha; // 10-XMT_DATE_TIME-007
	private String trace; // 6_STAN-011
	private String modoEntrada; // 3-POS_ENTRY_MODE-022
	private String terminal; // 8-CARD_ACCPT_TRMNL _ID-041
	private String nombreComercio; // 40-ACCEPTORNAME-043
	private String tipoMoneda; // 3-CURRENCY CODE-049
	private String longitudC55; // 3-IC card system related data_Len-055
	private String C55_TAGS_EMV_FULL; // Variable,indicada en:(longitudC55)- IC
	// card system related data-055
	private String longitudC63; // 3-TOKEN_LEN-063
	private String C63_desc_token_ES_EZ; // Varibale, definida en:

	// (longitudC63)-TOKEN_DATA-063

	public String getLongitudMensaje200() {
		return longitudMensaje200;
	}

	public void setLongitudMensaje200(String longitudMensaje200) {
		this.longitudMensaje200 = longitudMensaje200;
	}

	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	public String getBitmap() {
		return bitmap;
	}

	public void setBitmap(String bitmap) {
		this.bitmap = bitmap;
	}

	public String getCodigoProceso() {
		return codigoProceso;
	}

	public void setCodigoProceso(String codigoProceso) {
		this.codigoProceso = codigoProceso;
	}

	public String getMontoOperacion() {
		return montoOperacion;
	}

	public void setMontoOperacion(String montoOperacion) {
		this.montoOperacion = montoOperacion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	public String getModoEntrada() {
		return modoEntrada;
	}

	public void setModoEntrada(String modoEntrada) {
		this.modoEntrada = modoEntrada;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getNombreComercio() {
		return nombreComercio;
	}

	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}

	public String getTipoMoneda() {
		return tipoMoneda;
	}

	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	public String getLongitudC55() {
		return longitudC55;
	}

	public void setLongitudC55(String longitudC55) {
		this.longitudC55 = longitudC55;
	}

	public String getC55_TAGS_EMV_FULL() {
		return C55_TAGS_EMV_FULL;
	}

	public void setC55_TAGS_EMV_FULL(String c55TAGSEMVFULL) {
		C55_TAGS_EMV_FULL = c55TAGSEMVFULL;
	}

	public String getLongitudC63() {
		return longitudC63;
	}

	public void setLongitudC63(String longitudC63) {
		this.longitudC63 = longitudC63;
	}

	public String getC63_desc_token_ES_EZ() {
		return C63_desc_token_ES_EZ;
	}

	public void setC63_desc_token_ES_EZ(String c63DescTokenESEZ) {
		C63_desc_token_ES_EZ = c63DescTokenESEZ;
	}



	@Override
	public String toString() {
		return "PeticionVentaBAZBean [C55_TAGS_EMV_FULL=" + C55_TAGS_EMV_FULL
				+ ", C63_desc_token_ES_EZ=" + C63_desc_token_ES_EZ
				+ ", bitmap=" + bitmap + ", codigoProceso=" + codigoProceso
				+ ", fecha=" + fecha + ", longitudC55=" + longitudC55
				+ ", longitudC63=" + longitudC63 + ", longitudMensaje200="
				+ longitudMensaje200 + ", modoEntrada=" + modoEntrada
				+ ", montoOperacion=" + montoOperacion + ", nombreComercio="
				+ nombreComercio + ", terminal=" + terminal + ", tipoMensaje="
				+ tipoMensaje + ", tipoMoneda=" + tipoMoneda + ", trace="
				+ trace + "]";
	}

}
