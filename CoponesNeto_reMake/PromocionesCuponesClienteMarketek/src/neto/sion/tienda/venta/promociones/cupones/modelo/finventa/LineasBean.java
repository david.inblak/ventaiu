package neto.sion.tienda.venta.promociones.cupones.modelo.finventa;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonProperty;

public class LineasBean {
	
	@JsonProperty("orden") private int orden;
	@JsonProperty("texto") private String texto;
	@JsonProperty("size") private int size;
	@JsonProperty("alineacion") private String alineacion;
	@JsonProperty("inverso") private String inverso;
	@JsonProperty("estilo") private String estilo;

        public LineasBean(){}
    public LineasBean(int orden, String texto, int size, String alineacion, String inverso, String estilo) {
        this.orden = orden;
        this.texto = texto;
        this.size = size;
        this.alineacion = alineacion;
        this.inverso = inverso;
        this.estilo = estilo;
    }
	
	
        
        
    
	public int getOrden() {
		return orden;
	}
    	public void setOrden(int orden) {
		this.orden = orden;
	}
    
	public String getTexto() {
		return texto;
	}
    
	public void setTexto(String texto) {
		this.texto = texto;
	}
    
	public int getSize() {
		return size;
	}
    
	public void setSize(int size) {
		this.size = size;
	}
    
	public String getAlineacion() {
		return alineacion;
	}
    
	public void setAlineacion(String alineacion) {
		this.alineacion = alineacion;
	}
    
	public String getInverso() {
		return inverso;
	}
    
	public void setInverso(String inverso) {
		this.inverso = inverso;
	}
    
	public String getEstilo() {
		return estilo;
	}
    
	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}
	
	
	@Override
	public String toString() {
		return "LineasBean :[orden=" + orden
							 + ", texto=" + texto 
							 + ", size=" + size 
							 + ", alineacion=" + alineacion 
							 + ", inverso=" + inverso 
							 + ", estilo=" + estilo + "]";
	}
	
	
	
}
