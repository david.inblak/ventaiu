package neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp;


public class RespuestaVentaTarjetaBean {

	private PeticionVentaBAZBean peticionBAZ;
	private RespuestaVentaBAZBean respuestaBAZ;
	private RespuestaReversoBAZBean respuestaReversoBAZ;
	private RespuestaPagatodoNipBean respuestaPagaTodo;
	private boolean seEnviaReverso;
	private String modoEntrada;
	private long trace;
	private int codigoError;
	private String descError;
	private long uId;
	private boolean pagoExitoso;
	
	public RespuestaVentaBAZBean getRespuestaBAZ() {
		return respuestaBAZ;
	}

	public void setRespuestaBAZ(RespuestaVentaBAZBean respuestaBAZ) {
		this.respuestaBAZ = respuestaBAZ;
	}

	public RespuestaPagatodoNipBean getRespuestaPagaTodo() {
		return this.respuestaPagaTodo;
	}

	public void setRespuestaPagaTodo(RespuestaPagatodoNipBean respuestaPagaTodo) {
		this.respuestaPagaTodo = respuestaPagaTodo;
	}

	public int getCodigoError() {
		return this.codigoError;
	}

	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}

	public String getDescError() {
		return this.descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	public RespuestaReversoBAZBean getRespuestaReversoBAZ() {
		return this.respuestaReversoBAZ;
	}

	public void setRespuestaReversoBAZ(
			RespuestaReversoBAZBean respuestaReversoBAZ) {
		this.respuestaReversoBAZ = respuestaReversoBAZ;
	}

	public String getModoEntrada() {
		return this.modoEntrada;
	}

	public void setModoEntrada(String modoEntrada) {
		this.modoEntrada = modoEntrada;
	}

	public PeticionVentaBAZBean getPeticionBAZ() {
		return this.peticionBAZ;
	}

	public void setPeticionBAZ(PeticionVentaBAZBean peticionBAZ) {
		this.peticionBAZ = peticionBAZ;
	}

	public boolean isSeEnviaReverso() {
		return this.seEnviaReverso;
	}

	public void setSeEnviaReverso(boolean seEnviaReverso) {
		this.seEnviaReverso = seEnviaReverso;
	}

	public long getTrace() {
		return this.trace;
	}

	public void setTrace(long trace) {
		this.trace = trace;
	}

	public long getuId() {
		return this.uId;
	}

	public void setuId(long uId) {
		this.uId = uId;
	}
	
	public boolean isPagoExitoso() {
		return this.pagoExitoso;
	}

	public void setPagoExitoso(boolean pagoExitoso) {
		this.pagoExitoso = pagoExitoso;
	}

    @Override
    public String toString() {
        return "RespuestaVentaTarjetaBean{" + "peticionBAZ=" + peticionBAZ + ", respuestaBAZ=" + respuestaBAZ + ", respuestaReversoBAZ=" + respuestaReversoBAZ + ", respuestaPagaTodo=" + respuestaPagaTodo + ", seEnviaReverso=" + seEnviaReverso + ", modoEntrada=" + modoEntrada + ", trace=" + trace + ", codigoError=" + codigoError + ", descError=" + descError + ", uId=" + uId + ", pagoExitoso=" + pagoExitoso + '}';
    }

        
}
