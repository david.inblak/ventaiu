/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.beginaddcupon;

import neto.sion.tienda.venta.promociones.cupones.modelo.preview.ValidaVentaDescResp;

/**
 *
 * @author dramirezr
 */
public class BeginAddCuponResp {
    private ValidaVentaDescResp respuesta;

    public ValidaVentaDescResp getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(ValidaVentaDescResp respuesta) {
        this.respuesta = respuesta;
    }
    
}
