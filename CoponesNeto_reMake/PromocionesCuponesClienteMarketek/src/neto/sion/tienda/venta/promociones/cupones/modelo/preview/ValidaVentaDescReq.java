/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.preview;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.util.ConversionBeanes;
import neto.sion.tienda.venta.cupones.util.FormateoNumero;
import org.codehaus.jackson.annotate.JsonProperty;

public class ValidaVentaDescReq {
        @JsonProperty("pedido") private long pedido;
        /*** Numero de caja*/
	@JsonProperty("pos") private long pos;
	@JsonProperty("sucursal") private long sucursal;
	/*** El consecutivo del idConciliación más el numero de intento*/
	@JsonProperty("ticket") private String ticket;
        /*** Solo el consecutivo del idConciliación */
	@JsonProperty("ticketOriginal") private String ticketOriginal;
        /*** idConciliación */
        @JsonProperty("ticketCompleto") private String ticketCompleto;
	@JsonProperty("ticketAnterior")  private String ticketAnterior;
	@JsonProperty("cajero") private long cajero;
	@JsonProperty("fecha") private String fecha;//Fecha de venta
	@JsonProperty("fechapedido") private String fechapedido;//siempre null
	@JsonProperty("hora") private String hora; //HHmmss
	@JsonProperty("cantidadArticulos") private int cantidadArticulos;
	@JsonProperty("montototal") private double montototal;
	@JsonProperty("montoimp") private double montoimp;
	@JsonProperty("identidad") private IdentidadBeginVentaBean identidad;//siempre null
	@JsonProperty("plu")  private List<PluBeginVentaBean> plu = null;
        
        DecimalFormat formatter = new DecimalFormat("#0.00");

    public String getTicketOriginal() {
        return ticketOriginal;
    }

    public void setTicketOriginal(String ticketOriginal) {
        this.ticketOriginal = ticketOriginal;
    }
        
        

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

        
    public String getTicketCompleto() {
        return ticketCompleto;
    }

    public void setTicketCompleto(String ticketCompleto) {
        this.ticketCompleto = ticketCompleto;
    }
        
        

    public String getTicketAnterior() {
        return ticketAnterior;
    }

    public void setTicketAnterior(String ticketAnterior) {
        this.ticketAnterior = ticketAnterior;
    }
	
	public long getPedido() {
		return pedido;
	}
	public void setPedido(long pedido) {
		this.pedido = pedido;
	}
	public long getPos() {
		return pos;
	}
	public void setPos(long pos) {
		this.pos = pos;
	}
	public long getSucursal() {
		return sucursal;
	}
	public void setSucursal(long sucursal) {
		this.sucursal = sucursal;
	}
	
	
	public long getCajero() {
		return cajero;
	}
	public void setCajero(long cajero) {
		this.cajero = cajero;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getFechapedido() {
		return fechapedido;
	}
	public void setFechapedido(String fechapedido) {
		this.fechapedido = fechapedido;
	}
	
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public int getCantidadArticulos() {
		return cantidadArticulos;
	}
	public void setCantidadArticulos(int cantidadArticulos) {
		this.cantidadArticulos = cantidadArticulos;
	}
	
	public double getMontototal() {
		return montototal;
	}
	public void setMontototal(double montototal) {
            this.montototal = Double.parseDouble(String.format(FormateoNumero.local, "%.3f", montototal));
            //this.montototal = Double.parseDouble(FormateoNumero.porPatron(FormateoNumero.PATRON_DOS_DECIMALES, montototal));
	}
	public double getMontoimp() {
		return montoimp;
	}
	public void setMontoimp(double montoimp) {
		this.montoimp = montoimp;
	}
	
	public IdentidadBeginVentaBean getIdentidad() {
		return identidad;
	}
	public void setIdentidad(IdentidadBeginVentaBean identidad) {
		this.identidad = identidad;
	}
	
	public List<PluBeginVentaBean> getPlu() {
		return plu;
	}

	public void setPlu(List<PluBeginVentaBean> plu) {
		this.plu = plu;
	}
	
        @Override
        public String toString() {
            try {
                return ConversionBeanes.convertir(this, String.class).toString();
            } catch (Exception ex) {
                SION.logearExcepcion(Modulo.VENTA, ex, "toString");
            }
            return null;
        }
         /*return new StringBuilder().
                            append(",plu:").append(plu).
                            append(",fecha:").append( fecha).
                            append(",fechapedido:").append(fechapedido).
                            append(",hora:").append(hora).
                            append(",cantidadArticulos:").append( cantidadArticulos).
                            append(",montototal:").append( montototal).
                            append(",montoimp:").append( montoimp).
                            append(",identidad:").append(identidad).
                            append(",pos:").append( pos).
                            append(",ticket:").append(ticket).
                            append(",pedido:").append(pedido).
                            append(",sucursal:").append( sucursal).toString();*/
}
