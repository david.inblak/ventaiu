package neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/***
 * Clase que representa un los datos de un bloqueo venta.
 * 
 * @author Carlos V. Perez L.
 * 
 */

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "fiTipoPagoId",
    "fiNumAvisos",
    "fiEstatusBloqueoId",
    "fiAvisosFalt"
})


public class BloqueoBean {
	@JsonProperty("fiTipoPagoId") private int fiTipoPagoId;
	@JsonProperty("fiNumAvisos") private int fiNumAvisos;
	@JsonProperty("fiEstatusBloqueoId") private int fiEstatusBloqueoId;
	@JsonProperty("fiAvisosFalt") private int fiAvisosFalt;

	public int getFiTipoPagoId() {
		return fiTipoPagoId;
	}

	public void setFiTipoPagoId(int fiTipoPagoId) {
		this.fiTipoPagoId = fiTipoPagoId;
	}

	public int getFiNumAvisos() {
		return fiNumAvisos;
	}

	public void setFiNumAvisos(int fiNumAvisos) {
		this.fiNumAvisos = fiNumAvisos;
	}

	public int getFiEstatusBloqueoId() {
		return fiEstatusBloqueoId;
	}

	public void setFiEstatusBloqueoId(int fiEstatusBloqueoId) {
		this.fiEstatusBloqueoId = fiEstatusBloqueoId;
	}

	public int getFiAvisosFalt() {
		return fiAvisosFalt;
	}

	public void setFiAvisosFalt(int fiAvisosFalt) {
		this.fiAvisosFalt = fiAvisosFalt;
	}

	@Override
	public String toString() {
		return "BloqueoBean [fiTipoPagoId=" + fiTipoPagoId + ", fiNumAvisos="
				+ fiNumAvisos + ", fiEstatusBloqueoId=" + fiEstatusBloqueoId
				+ ", fiAvisosFalt=" + fiAvisosFalt + "]";
	}

}
