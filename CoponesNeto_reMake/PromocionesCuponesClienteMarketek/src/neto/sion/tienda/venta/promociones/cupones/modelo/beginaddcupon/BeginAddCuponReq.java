/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.beginaddcupon;

import neto.sion.tienda.venta.promociones.cupones.modelo.preview.ValidaVentaDescReq;

/**
 *
 * @author dramirezr
 */
public class BeginAddCuponReq {
    private ValidaVentaDescReq solicitud;

    public ValidaVentaDescReq getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(ValidaVentaDescReq solicitud) {
        this.solicitud = solicitud;
    }
    
}
