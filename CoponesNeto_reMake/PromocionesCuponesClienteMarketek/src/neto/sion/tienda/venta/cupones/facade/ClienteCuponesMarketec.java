/* 
 * To change this template, choose Tools | Templates 
 * and open the template in the editor. 
 */ 
package neto.sion.tienda.venta.cupones.facade; 
 
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import javax.activation.DataHandler;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.proxy.WSVentaCuponesMarketecServiceStub;
import neto.sion.tienda.venta.cupones.proxy.WSVentaCuponesMarketecServiceStub.ObtenerImagenResp;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.ServiceClient;
 
/** 
 * 
 * @author dramirezr 
 */ 
public class ClienteCuponesMarketec implements ClienteCupones { 
    private WSVentaCuponesMarketecServiceStub stub; 
    private ServiceClient sc; 
    private int CODIGO_EXITO = 0; 
     
    public ClienteCuponesMarketec() { 
        if( stub == null ){
            try {         
                this.stub = new WSVentaCuponesMarketecServiceStub(SION.obtenerConfigurationContext(), SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE")); 
                sc = stub._getServiceClient(); 
                sc.engageModule("rampart");
            } catch (AxisFault ex) { 
                SION.logearExcepcion(Modulo.VENTA, ex, "Error al intentar crear el cliente: ClienteMarketec"); 
            } catch (Exception ex) { 
                SION.logearExcepcion(Modulo.VENTA, ex, "Error al intentar crear el cliente: ClienteMarketec"); 
            }catch (Error ex) { 
                SION.logearExcepcion(Modulo.VENTA, new Exception(ex), "Error al intentar crear el cliente: ClienteMarketec"); 
            }
        }
    } 
     
     
    @Override
    public Object validaDesceuntosAutomaticos(Object solicitud) throws Exception{ 
        sc.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE.TIMEOUT.CONNECT.AUTOMATICO"))); 
        SION.log(Modulo.VENTA, "MARKETEC :: Solicitud > validaDesceuntosAutomaticos :: validaVentaDescuento " + solicitud.toString(), Level.INFO); 
        Object resp =this.stub.inicializaVentaMktc(solicitud.toString()); 
        SION.log(Modulo.VENTA, "MARKETEC :: Respuesta > validaDesceuntosAutomaticos :: validaVentaDescuento " + resp.toString(), Level.INFO); 
        return resp; 
    } 
     
    @Override
    public Object calculaDescuentoCupones(Object solicitud) throws Exception{ 
        sc.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE.TIMEOUT.CONNECT.ADDCUPONES"))); 
        SION.log(Modulo.VENTA, "MARKETEC :: Solicitud > calculaDescuentoCupones: " + solicitud.toString(), Level.INFO); 
        Object resp = this.stub.addCupon(solicitud.toString()); 
        SION.log(Modulo.VENTA,"MARKETEC :: Respuesta > calculaDescuentoCupones: " + resp.toString(), Level.INFO); 
        return resp; 
    } 
     
    @Override
    public Object finalizaVenta(Object solicitud) throws Exception{ 
        sc.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE.TIMEOUT.CONNECT.FINALIZAVENTA"))); 
        SION.log(Modulo.VENTA, "MARKETEC :: Solicitud > finalizaVenta: " + solicitud.toString(), Level.INFO); 
        Object resp = this.stub.finalizaVentaCupones(solicitud.toString()); 
        SION.log(Modulo.VENTA, "MARKETEC :: Respuesta > finalizaVenta: " + resp.toString(), Level.INFO); 
        return resp; 
    } 
     
    @Override
    public Object cancelarVenta(Object solicitud) throws Exception{ 
        sc.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE.TIMEOUT.CONNECT.CANCELARVENTA"))); 
        SION.log(Modulo.VENTA, "MARKETEC :: Solicitud > cancelarVenta: " + solicitud.toString(), Level.INFO); 
        Object resp = this.stub.cancelaVentaMktc(solicitud.toString()); 
        SION.log(Modulo.VENTA, "MARKETEC :: Respuesta > cancelarVenta: " + resp.toString(), Level.INFO); 
        return resp; 
    } 
     
    @Override
    public byte[] desgargaImagen(String nombreImagen) throws Exception{ 
        ObtenerImagenResp resp = null; 
        byte[] imagenBytes = null; 
         
        try 
        { 
            sc.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE.TIMEOUT.CONNECT.DESCARGAIMAGEN"))); 
            SION.log(Modulo.VENTA, "MARKETEC :: Solicitud > desgargaImagen: " + nombreImagen, Level.INFO); 
             
            resp = this.stub.obtenerImagen(nombreImagen); 
            if( resp.getCodigoError() == CODIGO_EXITO ){ 
                DataHandler handler = resp.getImagen(); 
                ByteArrayOutputStream output = new ByteArrayOutputStream();           
                handler.writeTo(output);           
                imagenBytes = output.toByteArray(); 
            } 
             
            SION.log(Modulo.VENTA, "MARKETEC :: Respuesta > desgargaImagen: " + resp.getCodigoError() + " >> " + resp.getMsgError(), Level.INFO); 
             
        } catch (IOException e) { 
            SION.logearExcepcion(Modulo.VENTA, e, "MARKETEC :: Ocurrio un error al obtener la imagen"); 
        } catch (Exception e) { 
            SION.logearExcepcion(Modulo.VENTA, e, "MARKETEC :: Ocurrio un error al obtener la imagen"); 
        } catch (Error e) { 
            SION.logearExcepcion(Modulo.VENTA, new Exception(e), "MARKETEC :: Ocurrio un error al obtener la imagen"); 
        } 
         
        return imagenBytes; 
    } 
     
} 
