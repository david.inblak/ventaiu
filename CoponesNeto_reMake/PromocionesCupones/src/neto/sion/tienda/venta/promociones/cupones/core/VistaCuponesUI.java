/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.util.Excepciones;
import neto.sion.tienda.venta.cupones.util.FormateoNumero;
import neto.sion.tienda.venta.promociones.cupones.modelo.gral.ArticuloCupon;
import neto.sion.tienda.venta.promociones.cupones.modelo.gral.CuponBean;
import neto.sion.tienda.venta.promociones.cupones.vista.CuponUI;

/**
 *
 * @author dramirezr
 */
public abstract class VistaCuponesUI {

    public static final String PromptInputCodigoCupon = "Introduzca un código de cupón";
    public static final Locale local = new Locale("es", "MX");
    public ObservableList<ArticuloCupon> artiulosVenta = FXCollections.observableArrayList();
    SimpleDoubleProperty importeTotalAPagat = new SimpleDoubleProperty();
    TextField inputCodigoCupon;
    Button btnAgregaCupon;
    Button btnEliminaCupon;
    Button btnContinuar;
    Pane nodoPrincipal;

    public VistaCuponesUI() {
        artiulosVenta.addListener(listener);
    }
    
    @Override
    protected void finalize(){
        try {
            super.finalize();
            artiulosVenta.removeListener(listener);
        } catch (Throwable ex) {
            System.out.println(""+ex.getMessage());
        }
    }
    
    public void resetVista(){
        artiulosVenta.clear();
    }

    public Node getVista() {
        int alto = 630, largo = 1120;
        nodoPrincipal = new Pane();
        nodoPrincipal.setPrefSize(largo, alto);
        nodoPrincipal.setStyle("-fx-padding: 10;"
                + "-fx-border-style: solid inside;"
                + "-fx-border-width: 1;"
                + "-fx-border-radius: 5;"
                + "-fx-border-color: grey;");
        nodoPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent evento) {
                if (evento.getCode().equals(KeyCode.ESCAPE) || evento.getCode().equals(KeyCode.F9)) {
                    presenter_cerrarVentana();
                    evento.consume();
                }
                if (evento.getCode().equals(KeyCode.F8)) {
                    presenter_continuar(artiulosVenta);
                    evento.consume();
                }
            }
        });

        VBox vista = new VBox(10);
        nodoPrincipal.getChildren().add(vista);
        vista.getChildren().addAll(getBuscador(), getTabla(), getFinal());

        nodoPrincipal.getChildren().add(getBloqueo(largo, alto));
        nodoPrincipal.getChildren().add(getMensaje(largo, alto));

        return nodoPrincipal;
    }
    Pane bloqueo;
    HBox contenido = new HBox(20);
    Label mensaje = new Label();

    public Node getBloqueo(int largo, int alto) {
        bloqueo = new Pane();
        bloqueo.setPrefSize(largo, alto);
        bloqueo.setStyle("-fx-background-color: rgba(0,0,0,0.7);");
        bloqueo.setVisible(false);

        HBox centrador = new HBox();
        centrador.setAlignment(Pos.CENTER);
        centrador.setPrefSize(largo, alto);
        bloqueo.getChildren().add(centrador);

        contenido.setAlignment(Pos.CENTER);
        centrador.getChildren().add(contenido);

        InputStream inputStream = this.getClass().getResourceAsStream("/neto/sion/tienda/venta/promociones/cupones/vista/Loading.gif");
        Image loading = new Image(inputStream);

        try {
            inputStream.close();
        } catch (IOException ex) {
            SION.log(Modulo.VENTA, "Error al crear imagen ", Level.SEVERE);
        }

        ImageView imageView = new ImageView(loading);
        imageView.setCache(true);
        imageView.setFitWidth(100);
        imageView.setFitHeight(100);
        contenido.getChildren().add(imageView);

        mensaje.setFont(Font.font(null, FontWeight.NORMAL, 25));
        mensaje.setStyle(" -fx-text-fill: white;");
        contenido.getChildren().add(mensaje);

        return bloqueo;
    }
    Pane bloqueoMensaje;
    HBox contenidoMensaje = new HBox(20);
    Label mensajeSimple = new Label();

    public Node getMensaje(int largo, int alto) {
        bloqueoMensaje = new Pane();
        bloqueoMensaje.setPrefSize(largo, alto);
        bloqueoMensaje.setStyle("-fx-background-color: rgba(0,0,0,0.7);");
        bloqueoMensaje.setVisible(false);

        HBox centrador = new HBox();
        centrador.setAlignment(Pos.CENTER);
        centrador.setPrefSize(largo, alto);
        bloqueoMensaje.getChildren().add(centrador);

        HBox.setHgrow(contenidoMensaje, Priority.ALWAYS);
        contenidoMensaje.setAlignment(Pos.CENTER);
        centrador.getChildren().add(contenidoMensaje);

        mensajeSimple.setMaxWidth(600);
        mensajeSimple.setWrapText(true);
        HBox.setHgrow(mensajeSimple, Priority.ALWAYS);
        mensajeSimple.setFont(Font.font(null, FontWeight.NORMAL, 25));
        mensajeSimple.setAlignment(Pos.CENTER);
        mensajeSimple.setStyle(" -fx-text-fill: orange;");
        contenidoMensaje.getChildren().add(mensajeSimple);

        return bloqueoMensaje;
    }

    private Node getBuscador() {
        VBox vista = new VBox(10);
        vista.setPadding(new Insets(10));

        HBox contScanaCupones = new HBox(10);
        contScanaCupones.setAlignment(Pos.CENTER_RIGHT);

        vista.getChildren().add(contScanaCupones);

        inputCodigoCupon = new TextField() {

            int maxChar = 20;
            String soloNumeros = "[+a-zA-Z0-9]*";

            @Override
            public void replaceText(int start, int end, String text) {
                if (matchTest(text)) {
                    super.replaceText(start, end, text);
                }
            }

            @Override
            public void replaceSelection(String text) {
                if (matchTest(text)) {
                    super.replaceSelection(text);
                }
            }

            private boolean matchTest(String text) {
                return text.isEmpty() || (text.matches(soloNumeros) && getText().length() < maxChar);
            }
        };
        inputCodigoCupon.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent ev) {
                if (ev.getCode().equals(KeyCode.ENTER)) {
                    agregarCuponDescuento(null);
                }
            }
        });

        inputCodigoCupon.setStyle("-fx-font: 20pt System;");
        inputCodigoCupon.setPromptText(PromptInputCodigoCupon);
        inputCodigoCupon.setPrefSize(320, 35);
        contScanaCupones.getChildren().add(inputCodigoCupon);

        btnAgregaCupon = new Button("Agregar Cupón");
        btnAgregaCupon.disableProperty().bind(inputCodigoCupon.textProperty().isEqualTo(""));
        btnAgregaCupon.setStyle("-fx-background-color: green; -fx-text-fill: white; -fx-font: 20pt System;");
        btnAgregaCupon.getLabelPadding();
        btnAgregaCupon.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                agregarCuponDescuento(null);
            }
        });
        contScanaCupones.getChildren().add(btnAgregaCupon);

        Platform.runLater(new Runnable() {

            public void run() {
                inputCodigoCupon.requestFocus();
            }
        });


        return vista;
    }

    private Node getTabla() {
        HBox vista = new HBox(10);
        vista.setPadding(new Insets(10));

        TableView tabla = new TableView();
        tabla.addEventFilter(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
            public void handle(MouseEvent arg0) {
                arg0.consume();
            }
        });
        vista.setAlignment(Pos.CENTER_RIGHT);

        TableColumn colCantidad = new TableColumn("Cantidad");
        colCantidad.setCellValueFactory(new PropertyValueFactory<ArticuloCupon, Double>("cantidad"));
        colCantidad.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn param) {
                return new TableCell<ArticuloCupon, Double>() {

                    @Override
                    public void updateItem(Double item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!this.isEmpty()) {
                            this.textProperty().setValue(String.format(local, "%.0f", item));
                            this.setStyle("-fx-text-fill: black; -fx-font: 16pt System;");
                            this.alignmentProperty().set(Pos.CENTER);
                        }
                    }
                };
            }
        });
        colCantidad.setMinWidth(75);

        TableColumn colProducto = new TableColumn("Producto");
        colProducto.setCellValueFactory(new PropertyValueFactory<ArticuloCupon, String>("nombre"));
        colProducto.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn param) {
                return new TableCell<ArticuloCupon, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!this.isEmpty()) {
                            this.textProperty().setValue(item);
                            this.setStyle("-fx-text-fill: black; -fx-font: 16pt Verdana;");
                            this.alignmentProperty().set(Pos.CENTER_LEFT);
                        }
                    }
                };
            }
        });
        colProducto.setMinWidth(500);

        TableColumn colDescuento = new TableColumn("Descuento");
        colDescuento.setCellValueFactory(new PropertyValueFactory<ArticuloCupon, Double>("descuento"));
        colDescuento.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn param) {
                return new TableCell<ArticuloCupon, Double>() {

                    @Override
                    public void updateItem(Double item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!this.isEmpty()) {
                            this.textProperty().bind(Bindings.format(FormateoNumero.local, "%.2f", artiulosVenta.get(getIndex()).descuento));
                            this.alignmentProperty().set(Pos.CENTER_RIGHT);
                            if (item.doubleValue() > 0) {
                                this.setStyle("-fx-text-fill: blue; -fx-font: 16pt Verdana;");
                            } else {
                                this.setStyle("-fx-text-fill: gray; -fx-font: 16pt Verdana;");
                            }

                        }
                    }
                };
            }
        });
        colDescuento.setMinWidth(90);

        TableColumn colPrecio = new TableColumn("Precio");
        colPrecio.setCellValueFactory(new PropertyValueFactory<ArticuloCupon, Double>("precio"));
        colPrecio.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn param) {
                return new TableCell<ArticuloCupon, Double>() {

                    @Override
                    public void updateItem(Double item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!this.isEmpty()) {
                            this.textProperty().setValue(String.format(local, "%.2f", item));
                            this.setStyle("-fx-text-fill: black; -fx-font: 16pt Verdana;");
                            this.alignmentProperty().set(Pos.CENTER_RIGHT);
                        }
                    }
                };
            }
        });
        colPrecio.setMinWidth(90);

        TableColumn colImporte = new TableColumn("Importe");
        colImporte.setCellValueFactory(new PropertyValueFactory<ArticuloCupon, Double>("importe"));
        colImporte.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn param) {
                return new TableCell<ArticuloCupon, Double>() {

                    @Override
                    public void updateItem(Double item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!this.isEmpty()) {
                            this.textProperty().bind(Bindings.format(FormateoNumero.local, "%.2f", artiulosVenta.get(getIndex()).importe));
                            this.setStyle("-fx-text-fill: black; -fx-font: 16pt Verdana;");
                            this.alignmentProperty().set(Pos.CENTER_RIGHT);
                        }
                    }
                };
            }
        });
        colImporte.setMinWidth(90);

        TableColumn colCupones = new TableColumn("Cupones");
        colCupones.setCellValueFactory(new PropertyValueFactory<ArticuloCupon, CuponBean>("cupon"));
        colCupones.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn param) {
                return new TableCell<ArticuloCupon, CuponBean>() {

                    @Override
                    public void updateItem(CuponBean item, boolean empty) {
                        super.updateItem(item, empty);

                        if (!empty) {
                            final VBox graf = new VBox();
                            graf.setAlignment(Pos.CENTER);
                            graf.setFillWidth(true);
                            this.setGraphic(graf);
                            Label msg = new Label();
                            msg.textProperty().bind(artiulosVenta.get(this.getIndex()).getCupon().mensaje);
                            msg.visibleProperty().bind(artiulosVenta.get(this.getIndex()).getCupon().activo);
                            graf.getChildren().add(CuponUI.crearCupon(artiulosVenta.get(this.getIndex()).getCupon(), artiulosVenta.get(this.getIndex())).getVista());
                            graf.getChildren().add(msg);
                        }
                    }
                };
            }
        });
        colCupones.setMinWidth(230);


        tabla.getColumns().addAll(colCantidad, colProducto, colDescuento, colPrecio, colImporte, colCupones);


        tabla.setItems(artiulosVenta);

        tabla.setEditable(false);
        vista.getChildren().add(tabla);

        return vista;
    }

    private Node getFinal() {
        HBox vista = new HBox(10);
        vista.setAlignment(Pos.CENTER_RIGHT);
        vista.setPadding(new Insets(10));

        VBox vb = new VBox(10);
        vista.getChildren().add(vb);

        HBox hb1 = new HBox(10);
        hb1.setAlignment(Pos.CENTER_RIGHT);
        vb.getChildren().add(hb1);
        Label lblImporteTotalMensaje = new Label("Importe total a pagar");
        lblImporteTotalMensaje.setFont(Font.font(null, FontWeight.NORMAL, 36));
        lblImporteTotalMensaje.setStyle(" -fx-text-fill: green;");

        Label lblImporteTotalCifra = new Label("$ 205.50");
        lblImporteTotalCifra.setFont(Font.font(null, FontWeight.BOLD, 48));
        lblImporteTotalCifra.setStyle(" -fx-text-fill: green;");
        lblImporteTotalCifra.textProperty().bind(Bindings.format(FormateoNumero.local, "$ %.2f", importeTotalAPagat));
        hb1.getChildren().addAll(lblImporteTotalMensaje, lblImporteTotalCifra);


        HBox hb2 = new HBox(10);
        vb.getChildren().add(hb2);
        Label lblDescuentoTotalMensaje = new Label("Descuentos válidos sólo con sistema de descuentos");//Suma total de descuentos
        lblDescuentoTotalMensaje.setFont(Font.font(null, FontWeight.NORMAL, 30));
        lblDescuentoTotalMensaje.setStyle(" -fx-text-fill: #CD0000;");

        Label lblDescuentoTotalCifra = new Label("");
        lblDescuentoTotalCifra.setFont(Font.font(null, FontWeight.NORMAL, 35));
        lblDescuentoTotalCifra.setStyle(" -fx-text-fill: #CD0000;");
        hb2.getChildren().addAll(lblDescuentoTotalMensaje, lblDescuentoTotalCifra);

        btnContinuar = new Button(" Continuar ");
        btnContinuar.setStyle("-fx-background-color: green; -fx-text-fill: white; -fx-font: 32pt System;");
        btnContinuar.setMinSize(100, 100);
        btnContinuar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                try {
                    presenter_continuar(artiulosVenta);
                } catch (Exception ex) {
                    SION.logearExcepcion(Modulo.VENTA, ex, "Finalizar flujo cupones :: " + Excepciones.getStackTrace(ex));
                }
            }
        });

        vista.getChildren().add(btnContinuar);

        btnEliminaCupon = new Button("Eliminar\n y Salir");
        btnEliminaCupon.setAlignment(Pos.CENTER);
        btnEliminaCupon.setStyle("-fx-background-color: #CD0000; -fx-text-fill: white; -fx-font: 20pt System;");
        btnEliminaCupon.setMinSize(80, 100);
        btnEliminaCupon.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                try {
                    presenter_cerrarVentana();
                } catch (Exception ex) {
                    SION.logearExcepcion(Modulo.VENTA, ex, "Accion De Boton" + Excepciones.getStackTrace(ex));
                }
            }
        });
        vista.getChildren().add(btnEliminaCupon);

        return vista;
    }
    
    ListChangeListener<ArticuloCupon> listener = new ListChangeListener<ArticuloCupon>() {

        @Override
        public void onChanged(ListChangeListener.Change<? extends ArticuloCupon> change) {
            boolean huboCambios = false;
            while (change.next()) {
                huboCambios = change.wasAdded() || change.wasRemoved();
            }
            if (huboCambios) {
                presenter_calculaPrecioTotalConDescuentos();
            }
        }
    };

    abstract void presenter_calculaPrecioTotalConDescuentos();
    abstract void presenter_cerrarVentana();
    abstract void presenter_continuar(Object solicitud);
    abstract void agregarCuponDescuento(Object solicitud);
}
