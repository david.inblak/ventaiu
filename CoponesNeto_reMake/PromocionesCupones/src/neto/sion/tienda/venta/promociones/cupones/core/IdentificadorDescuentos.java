/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.core;

import neto.sion.tienda.venta.cupones.facade.TipoCupon;

/**
 *
 * @author dramirezr
 */
public class IdentificadorDescuentos {
    public static int ID_MARKETEC = 3;
    public static int ID_CUPON_NETO = 4;
            
    public static boolean esDescuentoCupones(int tipoDescuento){
        
        if( tipoDescuento == ID_MARKETEC || tipoDescuento == ID_CUPON_NETO)
            return true;
        
        return false;
    }
    
    public static int getTipoCuponAgregado( TipoCupon tipo){
        if( tipo == TipoCupon.Marketec)
            return ID_MARKETEC;
        else if( tipo == TipoCupon.Neto )
            return ID_CUPON_NETO;
        else return -1;
    }
}
