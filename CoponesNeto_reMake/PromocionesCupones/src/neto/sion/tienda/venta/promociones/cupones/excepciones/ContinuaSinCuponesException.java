/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.excepciones;

/**
 *
 * @author dramirezr
 */
public class ContinuaSinCuponesException extends Exception{

    public ContinuaSinCuponesException(String mensaje) {
        super(mensaje);
    }
    
}
