/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mensajeserror;

import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author dramirezr
 */
public abstract class InterpreteMensajesError {    
    protected static final ObjectMapper mapper = new ObjectMapper();
    public InterpreteMensajesError siguiente= null;

    public InterpreteMensajesError(InterpreteMensajesError siguiente) {
        this.siguiente = siguiente;
    }
       
    
    public abstract Object interpretaMensaje(final MensajeXInterpretar mensaje);
    
    public void setSiguienteInterprete(InterpreteMensajesError nterprete){
        this.siguiente = nterprete;
    }
    public InterpreteMensajesError getSiguienteInterprete(){
        return siguiente;
    }
}
