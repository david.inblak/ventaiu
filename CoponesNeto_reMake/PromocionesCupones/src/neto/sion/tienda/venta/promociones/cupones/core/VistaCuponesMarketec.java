/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.core;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.promociones.cupones.modelo.gral.ArticuloCupon;
import neto.sion.tienda.venta.promociones.cupones.core.IPresentadorCupones;
import neto.sion.tienda.venta.promociones.cupones.core.IVistaCupones;


/**
 * @author dramirezr
 */
public class VistaCuponesMarketec extends VistaCuponesUI implements IVistaCupones{

    IPresentadorCupones presenter;
    
    public VistaCuponesMarketec(){
        super();
    }
    
        
    public void setPresenter(IPresentadorCupones _presenter){
        presenter = _presenter;
    }
    

    @Override
    public void calculaPrecioTotalConDescuentos() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                Double suma = new Double(0);
                for (ArticuloCupon articuloCupon : artiulosVenta) {
                    suma += articuloCupon.getCantidad()* (articuloCupon.getPrecio() - articuloCupon.getDescuento());
                }
                
                importeTotalAPagat.set(suma);
            }
        });
        
    } 
   
    
    
    @Override
    public void presenter_calculaPrecioTotalConDescuentos(){
         presenter.calculaPrecioTotalConDescuentos();
    }
    
    private ChangeListener<Number> cambioDescuento = new ChangeListener<Number>() {
        @Override
        public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
            calculaPrecioTotalConDescuentos();
        }
    };
    
    @Override
    public Object mostrarArticulosDeVenta(Object solcitud) {

        final List<ArticuloCupon> articulosVenta = (List<ArticuloCupon>)solcitud;
                
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                for (ArticuloCupon articuloCupon : articulosVenta)
                    articuloCupon.descuento.removeListener(cambioDescuento);
                
                artiulosVenta.clear();
                artiulosVenta.addAll(articulosVenta);
                
                for (ArticuloCupon articuloCupon : articulosVenta)
                    articuloCupon.descuento.addListener(cambioDescuento);
                
            }
        });
        
        return null;
    }

    @Override
    public void mostrarCargando(final Object msg) {
         Platform.runLater(new Runnable() {
            @Override
            public void run() {
                bloqueo.setVisible(true);
                mensaje.setText(msg.toString());
                inputCodigoCupon.setVisible(false);
            }
        });
    }
    
    @Override
    public void ocultarCargando(Object msg) {
         Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.currentThread().sleep(500L);
                } catch (InterruptedException ex) {}
                
                bloqueo.setVisible(false);
                mensaje.setText("");
                inputCodigoCupon.setVisible(true);
                inputCodigoCupon.requestFocus();
            }
        });
    }
    
    
    @Override
    public void mostrarMensaje(final String msg, final String color) {
        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                bloqueoMensaje.setVisible(true);
                mensajeSimple.setText(msg.toString());
                mensajeSimple.setStyle(" -fx-text-fill: "+color+";");
                inputCodigoCupon.setDisable(true);
            }
        });
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.schedule(new Runnable() {
            @Override
            public void run() {
                bloqueoMensaje.setVisible(false);
                inputCodigoCupon.setDisable(false);
                inputCodigoCupon.requestFocus();
            }
        }, 6, TimeUnit.SECONDS);
         
    }

    @Override
    public Object actualizarDescuentosArticulosDeVenta(Object solicitud) {
        //Recibir los articulos con descuentos.
        final List<ArticuloCupon> articulosConDescuentos = (List<ArticuloCupon>)solicitud;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
               for (ArticuloCupon articuloDescuento : articulosConDescuentos) {
                   if( articuloDescuento.getId() == ArticuloCupon.ID_ARTICULO_DESCTO_TOTAL ){
                       int econtradoEnIndice = artiulosVenta.indexOf(articuloDescuento);
                       boolean esCuponExistenteIgualActual = false;
                       if( econtradoEnIndice >= 0 ){
                           System.out.println("Se encontró en el indice: " + econtradoEnIndice);
                           esCuponExistenteIgualActual = artiulosVenta.get(econtradoEnIndice).cupon.get().getIdCupon().equals(articuloDescuento.cupon.get().getIdCupon());
                       }
                                              
                       if( econtradoEnIndice < 0 || !esCuponExistenteIgualActual ){
                           artiulosVenta.add(articuloDescuento);
                       }
                   }else{
                        for (ArticuloCupon articuloCupon : artiulosVenta)
                        {
                                
                            if( articuloCupon.getId() == articuloDescuento.getId() )
                            {
                                    articuloCupon.setDescuento( 
                                            articuloCupon.getDescuento() + (
                                                articuloDescuento.getDescuento()/(articuloCupon.getCantidad() + articuloCupon.getCantidadDevueltos())
                                            ));//TODO: Arreglar el caso para un artículo con más de un descuento
                                    articuloCupon.setTipoDescuento(articuloDescuento.getTipoDescuento());
                                    articuloCupon.getCupon().setIdCupon   (articuloDescuento.getCupon().getIdCupon());
                                    articuloCupon.getCupon().setMensaje   (articuloDescuento.getCupon().getMensaje());
                                    articuloCupon.getCupon().setActivo    (articuloDescuento.getCupon().isActivo());
                                    articuloCupon.getCupon().setAutomatico(articuloDescuento.getCupon().getAutomatico());
                                    articuloDescuento.setId(articuloCupon.getId());
                                    break;
                             }
                        }
                   }
                }
            }
        });
        
        
        
        return null;
    }

    @Override
    public void eliminarDescuentosArticulos(Object solicitud) {
         Platform.runLater(new Runnable() {
            @Override
            public void run() {
                for (ArticuloCupon articuloCupon : artiulosVenta) {
                    articuloCupon.setTipoDescuento(ArticuloCupon.ID_SIN_DESCUENTO);
                    articuloCupon.setDescuento(0.0);
                    articuloCupon.getCupon().setActivo(false);
                    if( articuloCupon.getId() ==  ArticuloCupon.ID_ARTICULO_DESCTO_TOTAL ){
                        artiulosVenta.remove(articuloCupon);
                    }
                }
            }
         });
    }

    @Override
    public void agregarCuponDescuento(Object solicitud) {      
        try {
            String texto = inputCodigoCupon.textProperty().get().toString();
            if( !"".equals(texto) || texto != null ){
                boolean cuponValido = (Boolean) presenter.esCuponValido(texto);
                if( cuponValido )
                    presenter.calculaDescuentosXCupon(inputCodigoCupon.textProperty().get().toString());
            }            
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "agregarCuponDescuento");
        }finally{
            inputCodigoCupon.textProperty().set("");
        }
    }

    @Override
    public void eliminarCuponDescuento(Object solicitud) {
        final String idCupon = solicitud.toString();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
               for (ArticuloCupon articulo : artiulosVenta) {
                    if( !articulo.getCupon().getAutomatico() && articulo.getCupon().getIdCupon().equals(idCupon)){
                        articulo.setDescuento(0.0);
                        articulo.setTipoDescuento(ArticuloCupon.ID_SIN_DESCUENTO);
                        articulo.getCupon().setActivo(false);
                        if( articulo.getId() ==  ArticuloCupon.ID_ARTICULO_DESCTO_TOTAL ){
                           artiulosVenta.remove(articulo);
                           return ;
                        }
                    }
                }              
               
                presenter.calculaPrecioTotalConDescuentos();
                inputCodigoCupon.textProperty().set("");
            }
        });
    }

    @Override
    void presenter_cerrarVentana() {
        presenter.cerrarVentana();
    }
    
    @Override
    void presenter_continuar(Object solicitud) {
        presenter.continuar(artiulosVenta);
    }

    @Override
    public Object obtenerArticulosVista() {
        return artiulosVenta;
    }

    @Override
    public void actualizaIdCupon(String codigoNto) {
        inputCodigoCupon.textProperty().setValue(codigoNto);
    }
    
    
    
}
