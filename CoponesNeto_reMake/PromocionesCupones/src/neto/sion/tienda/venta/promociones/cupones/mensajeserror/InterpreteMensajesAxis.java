/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mensajeserror;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.sql.SQLException;
import javax.xml.stream.XMLStreamException;
import org.apache.axiom.om.OMException;
import org.apache.axis2.AxisFault;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.ConnectionPoolTimeoutException;

/**
 *
 * @author dramirezr
 */
public class InterpreteMensajesAxis extends InterpreteMensajesError{

    public InterpreteMensajesAxis(InterpreteMensajesError interprete ) {
        super(interprete);
    }


    @Override
    public Object interpretaMensaje(final MensajeXInterpretar mensaje) {
        if( mensaje == null || mensaje.data == null )
            return "Axis > Se recibió un mensaje vacío";
        
         StringBuilder strBuilder = new StringBuilder();
        
        if( mensaje.data instanceof AxisFault ){
            AxisFault error = (AxisFault) mensaje.data;
            if( error.getCause() == null ){
                strBuilder.append("Se presentó un error en central: ").append(error.getMessage());
            }else if( error.getCause() instanceof ConnectTimeoutException || 
                      error.getCause() instanceof SocketTimeoutException ){
                strBuilder.append("Se agotó el tiempo de espera para la respuesta: ").append(error.getMessage());
            }else if( error.getCause() instanceof ConnectionPoolTimeoutException ){
                strBuilder.append("Se agotó el tiempo en espera de conexión: ").append(error.getMessage());
            }else if( error.getCause() instanceof SQLException ){
                strBuilder.append("Error de BD en central: ").append(error.getMessage());
            }else if( error.getCause() instanceof SocketException ){
                strBuilder.append("Se presentó un problema con la red: ").append(error.getMessage());
            }else if( error.getCause() instanceof XMLStreamException ||
                      error.getCause() instanceof OMException){
                strBuilder.append("El servicio está temporalmente fuera de línea. ").append(error.getMessage());
            }else{
                String msj = error.getMessage();
                strBuilder.append("Se presentó un error inesperado: ").append(error.getMessage().substring(0, msj.length() > 100? 100 : msj.length()));
            }
            
            return strBuilder.toString();  
        }else{
            return this.getSiguienteInterprete().interpretaMensaje(mensaje);
        }
    }
    
}