/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.core;


import java.sql.CallableStatement;
import java.sql.Connection;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.genericos.utilidades.Utilerias;
import neto.sion.tienda.venta.cupones.facade.ClienteCupones;
import neto.sion.tienda.venta.cupones.facade.ClienteCuponesFactory;
import neto.sion.tienda.venta.cupones.facade.TipoCupon;
import neto.sion.tienda.venta.promociones.cupones.core.IRepositorioCupones;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author dramirezr
 */
public class RepositorioCuponesNetoMarketec implements IRepositorioCupones{
    private static final int NUMERO_INTENTOS = 1;
    private ClienteCupones cliente = null;
    private static int EXITO = 0;
    private static int ACTIVADO = 1;
        
    public RepositorioCuponesNetoMarketec() {
        cliente = ClienteCuponesFactory.getClienteCupones(TipoCupon.MarketecNeto);
    }
    
    @Override
    public Object calculaDescuentosAutomaticos(Object solicitud) throws Exception {
        ClienteCupones tmpCliente = ClienteCuponesFactory.getClienteCupones(TipoCupon.Marketec);
        return tmpCliente.validaDesceuntosAutomaticos(solicitud);
    }

    @Override
    public Object calculaDescuentosCupon(Object solicitud, Object tipoCliente) throws Exception {
       ClienteCupones tmpCliente = ClienteCuponesFactory.getClienteCupones((TipoCupon)tipoCliente);
       return tmpCliente.calculaDescuentoCupones(solicitud);
    }
    
    @Override
    public Object cancelaVentaCupones(Object solicitud) throws Exception {
        return cliente.cancelarVenta(solicitud);
    }

    @Override
    public Object hayComunicacion(Object solicitud) throws Exception {
        return Utilerias.evaluarConexionCentral(solicitud.toString(), NUMERO_INTENTOS);
    }

    @Override
    public Object debeAbrirVentanaCupones() throws Exception {
        Boolean resultado = true;
        Boolean fueraLineaComunicacion = false;
        Boolean fueraLineaForzado = false;
        Boolean marketecActivo = false;
        Boolean cuponesNetoActivo = false;
        
        CallableStatement cs=null;
        Connection conn = null;       
        String sql = null;
        try{
            conn = Conexion.obtenerConexion();
            
            sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS");
        
            cs = conn.prepareCall(sql);
            cs.setInt(1, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONETO.CUPONES.ACTIVOS.PAIS")));
            cs.setInt(2, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.sistema")));
            cs.setInt(3, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.modulo")));
            cs.setInt(4, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.submodulo")));
            cs.setInt(5, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.configuracion")));
            cs.registerOutParameter(6, OracleTypes.NUMBER);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.NUMBER);
            cs.execute();
            
            double resOperacionFlComunicacion = cs.getDouble(6);
            double activadoFlComunicacion = cs.getDouble(8);
            
            if( resOperacionFlComunicacion  == EXITO )
                fueraLineaComunicacion = (activadoFlComunicacion == ACTIVADO);
            System.out.println( " fueraLineaComunicacion: " + fueraLineaComunicacion );
            
            
            cs.setInt(1, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.PAIS")));
            cs.setInt(2, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "forzadoVentaFueraLinea.sistema")));
            cs.setInt(3, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "forzadoVentaFueraLinea.modulo")));
            cs.setInt(4, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "forzadoVentaFueraLinea.submodulo")));
            cs.setInt(5, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "forzadoVentaFueraLinea.configuracion")));
            cs.registerOutParameter(6, OracleTypes.NUMBER);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.NUMBER);
            cs.execute();
            
            double resOperacionFlForzado = cs.getDouble(6);
            double activadoFlForzado = cs.getDouble(8);
            
            if( resOperacionFlForzado  == EXITO )
                fueraLineaForzado = (activadoFlForzado == ACTIVADO);
            System.out.println( " fueraLineaForzado: " + fueraLineaForzado );
            
            
            
            cs.setInt(1, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.PAIS")));
            cs.setInt(2, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.SISTEMA")));
            cs.setInt(3, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.MODULO")));
            cs.setInt(4, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.SUBMODULO")));
            cs.setInt(5, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.CONFIGURACIONID")));
            cs.registerOutParameter(6, OracleTypes.NUMBER);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.NUMBER);
            cs.execute();
            
            double resOperacionMareketecActivo = cs.getDouble(6);
            double activadoMareketecActivo = cs.getDouble(8);
            
            if( resOperacionMareketecActivo  == EXITO )
                marketecActivo = (activadoMareketecActivo == ACTIVADO);
            System.out.println( " Marketec activo: " + marketecActivo );
            
                        
            cs.setInt(1, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONETO.CUPONES.ACTIVOS.PAIS")));
            cs.setInt(2, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONETO.CUPONES.ACTIVOS.SISTEMA")));
            cs.setInt(3, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONETO.CUPONES.ACTIVOS.MODULO")));
            cs.setInt(4, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONETO.CUPONES.ACTIVOS.SUBMODULO")));
            cs.setInt(5, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONETO.CUPONES.ACTIVOS.CONFIGURACIONID")));
            cs.registerOutParameter(6, OracleTypes.NUMBER);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.NUMBER);
            cs.execute();
            
            double resOperacionCuponesNetoActivo = cs.getDouble(6);
            double activadoCuponesNeto = cs.getDouble(8);
            if( resOperacionCuponesNetoActivo  == EXITO ){
                cuponesNetoActivo = (activadoCuponesNeto == ACTIVADO);
                System.out.println( " CuponesNeto activo: " + cuponesNetoActivo );
            }
            
            if( fueraLineaForzado == true || fueraLineaComunicacion == true || (cuponesNetoActivo == false && marketecActivo == false ) ){
                resultado = false;
            }else{
                resultado = true;
            }
                
            return resultado;
            
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, sql);
            throw new Exception("Local: Ocurrio un error al intentar obtener la configuración de los cupones.\n" + e.getMessage());//throw e;
        } finally {
            System.out.println( "debeAbrirVentanaCupones: " + resultado );
            Conexion.cerrarRecursos(conn,cs, null);
        }
    }
    
    
    @Override
    public Object esVentaConProveedorExterno(Object solicitud) throws Exception {
       
        Boolean marketecActivo = false;
        
        CallableStatement cs=null;
        Connection conn = null;       
        String sql = null;
        try{
            conn = Conexion.obtenerConexion();
            
            sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS");
        
            cs = conn.prepareCall(sql);
            
            cs.setInt(1, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.PAIS")));
            cs.setInt(2, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.SISTEMA")));
            cs.setInt(3, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.MODULO")));
            cs.setInt(4, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.SUBMODULO")));
            cs.setInt(5, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.CONFIGURACIONID")));
            cs.registerOutParameter(6, OracleTypes.NUMBER);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.NUMBER);
            cs.execute();
            
            double resOperacionMareketecActivo = cs.getDouble(6);
            double activadoMareketecActivo = cs.getDouble(8);
            if( resOperacionMareketecActivo  == EXITO ){
                marketecActivo = (activadoMareketecActivo == ACTIVADO);
                System.out.println( "Es venta con proveedor externo: " + marketecActivo );
            }
            
            return marketecActivo;
            
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, sql);
            throw e;
        } finally {
            Conexion.cerrarRecursos(conn,cs, null);
        }
    }

    @Override
    public Object finalizaVenta(Object solicitud) throws Exception {
        return cliente.finalizaVenta(solicitud);
    }
    
    
    public static void main(String[] args) throws Exception {
        RepositorioCuponesNetoMarketec r = new RepositorioCuponesNetoMarketec();
        System.out.println( " Debe: " + r.debeAbrirVentanaCupones() );
    }


}
