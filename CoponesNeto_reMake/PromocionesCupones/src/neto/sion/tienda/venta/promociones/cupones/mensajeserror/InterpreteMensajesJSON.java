/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mensajeserror;

import java.io.IOException;
import java.util.Map;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.MapType;

/**
 *
 * @author dramirezr
 */
public class InterpreteMensajesJSON extends InterpreteMensajesError {
    private static final String INICIO_OBJETO_JSON = "{", FIN_OBJETO_JSON = "}";    

    public InterpreteMensajesJSON() {
        super(null);
    }
    
    @Override
    public Object interpretaMensaje(MensajeXInterpretar mensaje) {
        if( mensaje == null || mensaje.data == null | "".equals(mensaje.data.toString()) )
            return "JSON > Se recibió un mensaje vacío";
        
        if( mensaje.data.toString().contains(INICIO_OBJETO_JSON) && mensaje.data.toString().contains(FIN_OBJETO_JSON))
        {
            StringBuilder strBuilder = new StringBuilder();
            try {
                MapType type = mapper.getTypeFactory().constructMapType(Map.class, String.class, Object.class);
                Map<String, Object> campos = mapper.readValue(mensaje.data.toString(), type);
                
                for (Object valor : campos.values()) {
                    if( valor instanceof String )
                        strBuilder.append(valor.toString()).append("\n");
                }               
            } catch (IOException ex) {
                SION.logearExcepcion(Modulo.VENTA, ex, "obtenerMensaje");
            }
            
            if( strBuilder.length() == 0 )
                strBuilder.append("El mensaje no contiene información");
            
            return strBuilder.toString();
        }else{
            return mensaje;
        }
    }
}
