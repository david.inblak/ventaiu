package promocionescupone.test;


import java.util.logging.Level;
import java.util.logging.Logger;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.util.ConversionBeanes;
import neto.sion.tienda.venta.promociones.cupones.modelo.preview.ValidaVentaDescReq;
import neto.sion.tienda.venta.promociones.cupones.modelo.preview.ValidaVentaDescResp;
import neto.sion.tienda.venta.promociones.cupones.core.RepositorioCuponesMarketec;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dramirezr
 */
public class TestVentaPreview {
    public static void main(String[] args) {
        String sol = "{										  "+
"    \"pedido\": 20191028161200,          "  +
"    \"pos\": 1234,                       "  +
"    \"sucursal\": 3725,                  "  +
"    \"ticket\": 20191028161200,          "  +
"    \"cajero\": 152886,                  "  +
"    \"fecha\": \"20191028\",             "  +
"    \"fechapedido\": \"20191028\",       "  +
"    \"hora\": \"161500\",                "  +
"    \"cantidadArticulos\": 2,            "  +
"    \"montototal\": 21,                  "  +
"    \"montoimp\": 0,                     "  +
"    \"identidad\": null,                 "  +
"    \"plu\": [                           "  +
"        {                                "  +
"            \"codigo\": \"1234567\",     "  +
"            \"estructura\": \"\",        "  +
"            \"cantidad\": 1,             "  +
"            \"monto\": 10,               "  +
"            \"montoTotal\": 10           "  +
"        },                               "  +
"        {                                "  +
"            \"codigo\": \"1234568\",     "  +
"            \"estructura\": \"\",        "  +
"            \"cantidad\": 1,             "  +
"            \"monto\": 11,               "  +
"            \"montoTotal\": 11           "  +
"        }                                "  +
"    ]                                    "  +
"}                                        " ;
        try {
           

            ValidaVentaDescReq req = (ValidaVentaDescReq)ConversionBeanes.convertir(sol, ValidaVentaDescReq.class);
            System.out.println(">" + req);
            
            FakeRepositorioCupones repositorio = new FakeRepositorioCupones();//new MarketecRepositorioCupones();
            Object desctosStr = repositorio.calculaDescuentosAutomaticos(req);
            
            ValidaVentaDescResp descBean = (ValidaVentaDescResp) ConversionBeanes.convertir(desctosStr, ValidaVentaDescResp.class);
            System.out.println(">" + descBean);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "TestPreview");
        }
        
    }
}
