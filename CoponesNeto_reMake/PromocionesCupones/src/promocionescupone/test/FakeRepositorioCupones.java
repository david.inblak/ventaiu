/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package promocionescupone.test;

import java.util.ArrayList;
import java.util.List;
import neto.sion.tienda.venta.promociones.cupones.core.IRepositorioCupones;

/**
 *
 * @author dramirezr
 */
public class FakeRepositorioCupones implements IRepositorioCupones{
    List<String> respAddCupon = new ArrayList<String>();
    int indice =0;
    
    public FakeRepositorioCupones(){
        respAddCupon.add("{"+
        "\"Mensaje\":\"OK\","+
        "\"codigoError\":0,"+
        "\"cupon\":{"+
        "    \"estado\":0,"+
        "    \"codigo\":\"4600330110828\","+
        "    \"estadoDesc\":\"Aplicado\","+
        "    \"linea1\":\"Aplicado \","+
        "    \"linea2\":\" \","+
        "    \"mensajeOperacion\":\"Mensaje de operación para mostrar\","+
        "    \"descuentoTotal\":{"+
        "        \"codigo\":\"4600330110828\","+
        "        \"monto\":\"5.00\","+
        "        \"oferta\":324960,"+
        "        \"descripcionOferta\":\"$5 compra mínima $50 con exclusión\","+
        "        \"prefijo\":\"\","+
        "        \"articuloId\":1000001,"+
        "        \"codigoBarras\":\"1000001\""+
        "    }"+
        "  }"+
        "}");
        
        /*respAddCupon.add("{\"Mensaje\":\"OK\","
                + "\"codigoError\":0,"
                + "\"cupon\":"
                    + "{\"estado\":0,"
                    + "\"codigo\":\"4600060006347\","
                    + "\"estadoDesc\":\"Aplicado\","
                    + "\"linea1\":\"Aplicado\","
                    + "\"linea2\":\" \","
                    + "\"descuentoTotal\":{"
                        + "\"codigo\":\"4600060006347\","
                        + "\"monto\":\"25.00\","
                        + "\"oferta\":300434,"
                        + "\"descripcionOferta\":\"Descuento directo a la venta\","
                        + "\"prefijo\":\"\","
                        + "\"articuloId\":1000001,"
                        + "\"codigoBarras\":\"1000001\"}}}");
        respAddCupon.add("{\"Mensaje\":\"OK\","
                + "\"codigoError\":0,"
                + "\"cupon\":"
                    + "{\"estado\":0,"
                    + "\"codigo\":\"4600060006349\","
                    + "\"estadoDesc\":\"Aplicado\","
                    + "\"linea1\":\"Aplicado\","
                    + "\"linea2\":\" \","
                    + "\"descuentoTotal\":{"
                        + "\"codigo\":\"4600060006349\","
                        + "\"monto\":\"25.00\","
                        + "\"oferta\":300434,"
                        + "\"descripcionOferta\":\"Descuento directo a la venta\","
                        + "\"prefijo\":\"\","
                        + "\"articuloId\":1000001,"
                        + "\"codigoBarras\":\"1000001\"}}}");
        
        respAddCupon.add(" {\"Mensaje\":\"OK\","
                + "\"codigoError\":0,"
                + "\"cupon\":"
                + "{\"estado\":0,"
                + "\"codigo\":\"4600060003094\","
                + "\"estadoDesc\":\"Aplicado\","
                + "\"linea1\":\"Aplicado\","
                + "\"linea2\":\" \","
                + "\"articulos\":["
                    + "{\"cantidad\":2.0,"
                    + "\"articuloId\":6070005204,"
                    + "\"cupon\":4600060003094,"
                    + "\"descripcionOferta\":\"Descuento en cocas\","
                    + "\"montoDesc\":5.0,"
                    + "\"puntosCanje\":\"0\"}"
                    + "]}}");*/
    }     
    
    

    @Override
    public Object calculaDescuentosAutomaticos(Object solicitud) throws Exception {
        Thread.sleep(500);
        //return "{\"Mensaje\":\"OK\",\"codigoError\":0,\"registroMktcId\":200116000000466}";
        return "{\"codigoError\":0,\"registroMktcId\":200116000000466}";
    }

    @Override
    public Object calculaDescuentosCupon(Object solicitud, Object tipoCliente) throws Exception {
        System.out.println("FAKE :: solicitud > " + solicitud);
        Thread.sleep(500);
        if( indice == 1 ) indice=0;
        
        return respAddCupon.get(indice++);
    }

    @Override
    public Object hayComunicacion(Object solicitud) throws Exception {
        return true;
    }

    @Override
    public Object cancelaVentaCupones(Object solicitud) throws Exception {
        System.out.println("RepoFake : cancelaVentaCupones");
        return null;
    }

    @Override
    public Object debeAbrirVentanaCupones() throws Exception {
        return true;
    }

    @Override
    public Object finalizaVenta(Object solicitud) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object esVentaConProveedorExterno(Object solicitud) throws Exception {
        return false;
    }
}
