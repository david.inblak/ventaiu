/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package promocionescupone.test;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import neto.sion.tienda.venta.promociones.cupones.modelo.gral.ArticuloCupon;
import neto.sion.tienda.venta.promociones.cupones.modelo.gral.CuponBean;
import neto.sion.tienda.venta.promociones.cupones.modelo.addcupon.CuponBeanResp;
import neto.sion.tienda.venta.promociones.cupones.modelo.addcupon.DescuentoTotalBean;
import neto.sion.tienda.venta.promociones.cupones.vista.CuponUI;

/**
 *
 * @author dramirezr
 */
public class TestCuponUI extends Application {
   public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Hello World!");
        VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        
        CuponBeanResp cuponResponse = new CuponBeanResp();
        cuponResponse.setCodigo("1234567890123");
        cuponResponse.setDescuentoTotal( new DescuentoTotalBean());
        cuponResponse.getDescuentoTotal().setCodigo("1234567890123");
        cuponResponse.getDescuentoTotal().setDescripcionOferta("Descripcion de oferta");
        cuponResponse.getDescuentoTotal().setMonto("100");
        cuponResponse.getDescuentoTotal().setOferta("XD");
        cuponResponse.getDescuentoTotal().setPrefijo("YY");
        cuponResponse.setEstado(1);
        cuponResponse.setLinea1("Linea 1");
        cuponResponse.setLinea2("Linea 2");
        CuponBean cupon = new CuponBean("1234567890123", true, "OK", false, cuponResponse) ;
        ArticuloCupon articulo = null;
        vbox.getChildren().add( CuponUI.crearCupon(cupon, articulo).getVista());
                
        StackPane root = new StackPane();
        root.getChildren().addAll(vbox);
        
        int alto=700, largo=1120;
        
        

        primaryStage.setScene(new Scene(root, largo, alto));
        primaryStage.show();
    }

}
