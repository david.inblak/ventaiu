/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package promocionescupone.test;

import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesAxis;
import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesError;
import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesException;
import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesJSON;
import neto.sion.tienda.venta.promociones.cupones.core.IConfiguracionCupones;
import neto.sion.tienda.venta.promociones.cupones.core.IRepositorioCupones;

/**
 *
 * @author dramirezr
 */
public class ConfiguracionCuponesFake implements IConfiguracionCupones {
    IRepositorioCupones repo = null;
    private InterpreteMensajesError conv = null;
    
    @Override
    public IRepositorioCupones elegirRepositorio() throws Exception {
        repo = new FakeRepositorioCupones();
       // repo = new RepositorioCuponesDesactivados();
        return repo;
    }

    @Override
    public Object debeIntentarDescuentoCupones() throws Exception {
        return repo.debeAbrirVentanaCupones();
    }
    
    @Override
    public Object cuponesExternosActivos() throws Exception {
        return repo.esVentaConProveedorExterno(null);
    }

    @Override
    public void resetRepositorio() {
        
    }

    @Override
    public InterpreteMensajesError getConvertidorMensajes() {
        if( conv == null)
            conv = new InterpreteMensajesAxis(new InterpreteMensajesException(new InterpreteMensajesJSON()));
        return conv;
    }
}
