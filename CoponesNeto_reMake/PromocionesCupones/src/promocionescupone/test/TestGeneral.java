package promocionescupone.test;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dramirezr
 */
public class TestGeneral {
    static final SimpleDateFormat sdfNumFecha = new SimpleDateFormat("yyyyMMdd");
    static final SimpleDateFormat sdfNumHora = new SimpleDateFormat("hhmmss");
    static final SimpleDateFormat sdfNum = new SimpleDateFormat("yyyyMMddhhmmss");
    static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    
    public static BigDecimal calculo(Double x, Double y, Double z){
        BigDecimal cant = new BigDecimal(x);
        BigDecimal precio = new BigDecimal(y);
        BigDecimal desct = new BigDecimal(z);
        
        return desct.subtract(precio).multiply(cant);
    }
    
    public static void main(String[] args) throws InterruptedException {
       
        System.out.println("> " +  Long.parseLong( 
                "4135620191107".replace(
                    sdfNumFecha.format(new Date()),"")
                + 3 )
                );
        /*String da = "paTerminal=10.2.32.68|CAJA3";
        System.out.println("> " + da.substring(da.length()-1, da.length()) );*/
               
        if(true)
            return ;
        /*SimpleFloatProperty cant = new SimpleFloatProperty(1.0f);        
        SimpleFloatProperty desc = new SimpleFloatProperty(1.0f);  
        SimpleFloatProperty presio = new SimpleFloatProperty(22.5f);
       SimpleFloatProperty importe = new SimpleFloatProperty(0.0f);
       
       System.out.println(">" + presio.subtract(desc));
       
       importe.bind(cant.multiply(presio.subtract(desc)));
        
        System.out.println(">" + importe.get());*/
        
        
        final ObjectProperty<BigDecimal> cant = new SimpleObjectProperty<BigDecimal> (BigDecimal.ZERO);        
        final ObjectProperty<BigDecimal>  desc = new SimpleObjectProperty<BigDecimal> (BigDecimal.ZERO);  
        final ObjectProperty<BigDecimal>  precio = new SimpleObjectProperty<BigDecimal> (BigDecimal.ZERO);
        final ObjectProperty<BigDecimal>  importe = new SimpleObjectProperty<BigDecimal> (BigDecimal.ZERO);

        ChangeListener<BigDecimal> listener = new ChangeListener<BigDecimal>(){
            @Override
            public void changed(ObservableValue<? extends BigDecimal> arg0, BigDecimal arg1, BigDecimal arg2) {
                    importe.set( calculo(cant.get().doubleValue(), desc.get().doubleValue(), precio.get().doubleValue()) );
            }                                       
        };
        
        cant.addListener(listener);
        desc.addListener(listener);
        precio.addListener(listener);
       
       cant.set(new BigDecimal(1.0));
       desc.set(new BigDecimal(1.0));
       precio.set(new BigDecimal(22.50));
       
       System.out.println(">" + importe.toString());
       
        
        System.out.println(">" + importe.get());
        /*
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddhhmmss");
        Date fechaInicio = new Date(sdf.format(new Date()));
        
        Thread.sleep(5000);
        System.out.println(     fechaInicio.compareTo(new Date(sdf.format(new Date()))) );
        
        System.out.println(     new Date("2019/10/29") );
        
       Date hoy = new Date();
                
        int idTienda = 3725;
        int idCaja   =    3;
                //  03725
                //       3
                //        9999999999999
        //long x = (long)(Math.random()*((9999999999999L-1)+1))+1;
        long dato = Long.parseLong( String.format("%d%d%s", idTienda, idCaja, sdf1.format(new Date())).trim() );
                        
        System.out.println( ">" + dato );*/
    }
}
