/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

/**
 *
 * @author fvegap
 */
public class MetodoEntradaArticuloBean {

    private int paisId;
    private long tiendaId;
    private String fecha;
    private long usuarioId;
    private long articuloId;
    private String codigoBarras;
    private int metodoEntradaId;
    private int cantidad;

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public long getArticuloId() {
        return articuloId;
    }

    public void setArticuloId(long articuloId) {
        this.articuloId = articuloId;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getMetodoEntradaId() {
        return metodoEntradaId;
    }

    public void setMetodoEntradaId(int metodoEntradaId) {
        this.metodoEntradaId = metodoEntradaId;
    }

    public int getPaisId() {
        return paisId;
    }

    public void setPaisId(int paisId) {
        this.paisId = paisId;
    }

    public long getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(long tiendaId) {
        this.tiendaId = tiendaId;
    }

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public String toString() {
        return "MetodoEntradaArticuloBean{" + "paisId=" + paisId + ", tiendaId=" + tiendaId + ", fecha=" + fecha + ", usuarioId=" + usuarioId + ", articuloId=" + articuloId + ", codigoBarras=" + codigoBarras + ", metodoEntradaId=" + metodoEntradaId + ", cantidad=" + cantidad + '}';
    }
}
