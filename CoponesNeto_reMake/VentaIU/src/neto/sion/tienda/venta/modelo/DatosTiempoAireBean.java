/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author fvega
 */
public class DatosTiempoAireBean {
    
    public SimpleStringProperty codigobarras;
    public SimpleLongProperty articuloid;
    public SimpleIntegerProperty idcompania;
    public SimpleStringProperty nombrecompania;
    public SimpleDoubleProperty precio;
    public SimpleDoubleProperty costo;
    public SimpleDoubleProperty descuento;
    public SimpleDoubleProperty iva;
    
    
    public DatosTiempoAireBean(String acodigobarras, long aarticuloid, int aidcompania, String anombrecompania, 
            int aprecio, double acosto, double adescuento, double aiva){
        
        this.codigobarras = new ReadOnlyStringWrapper(acodigobarras);
        this.articuloid = new SimpleLongProperty(aarticuloid);
        this.idcompania = new SimpleIntegerProperty(aidcompania);
        this.nombrecompania = new SimpleStringProperty(anombrecompania);
        this.precio = new SimpleDoubleProperty(aprecio);
        this.costo = new SimpleDoubleProperty(acosto);
        this.descuento = new SimpleDoubleProperty(adescuento);
        this.iva = new SimpleDoubleProperty(aiva);
        
    }

    
    public SimpleStringProperty getCodigobarras() {
        return codigobarras;
    }
    public void setCodigobarras(SimpleStringProperty codigobarras) {
        this.codigobarras = codigobarras;
    }
    public SimpleStringProperty codigoBarrasProperty(){
        return codigobarras;
    }

    public SimpleDoubleProperty getCosto() {
        return costo;
    }
    public void setCosto(SimpleDoubleProperty costo) {
        this.costo = costo;
    }
    public SimpleDoubleProperty costoProperty(){
        return costo;
    }

    public SimpleDoubleProperty getDescuento() {
        return descuento;
    }
    public void setDescuento(SimpleDoubleProperty descuento) {
        this.descuento = descuento;
    }
    public SimpleDoubleProperty descuentoProperty(){
        return descuento;
    }

    public SimpleDoubleProperty getIva() {
        return iva;
    }
    public void setIva(SimpleDoubleProperty iva) {
        this.iva = iva;
    }
    public SimpleDoubleProperty ivaProperty(){
        return iva;
    }

    public SimpleLongProperty getArticuloid() {
        return articuloid;
    }
    public void setArticuloid(SimpleLongProperty articuloid) {
        this.articuloid = articuloid;
    }
    public SimpleLongProperty articuloIdProperty(){
        return articuloid;
    }

    public SimpleIntegerProperty getIdcompania() {
        return idcompania;
    }
    public void setIdcompania(SimpleIntegerProperty idcompania) {
        this.idcompania = idcompania;
    }
    public SimpleIntegerProperty idCompaniaProperty(){
        return idcompania;
    }

    public SimpleDoubleProperty getPrecio() {
        return precio;
    }
    public void setPrecio(SimpleDoubleProperty aprecio) {
        this.precio = aprecio;
    }
    public SimpleDoubleProperty precioProperty(){
        return precio;
    }

    public SimpleStringProperty getNombrecompania() {
        return nombrecompania;
    }
    public void setNombrecompania(SimpleStringProperty nombrecompania) {
        this.nombrecompania = nombrecompania;
    }
    public SimpleStringProperty nombreCompaniaProperty(){
        return nombrecompania;
    }
    
    
}
