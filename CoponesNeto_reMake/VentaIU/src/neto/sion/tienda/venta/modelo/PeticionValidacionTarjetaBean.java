/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

/**
 *
 * @author fvega
 */
public class PeticionValidacionTarjetaBean {

    private String numerotarjeta;

    public String getNumerotarjeta() {
        return numerotarjeta;
    }

    public void setNumerotarjeta(String numerotarjeta) {
        this.numerotarjeta = numerotarjeta;
    }

    @Override
    public String toString() {
        return "PeticionValidacionTarjetaBean{" + "numerotarjeta=" + numerotarjeta.substring(0, 6).concat("XXXXXXXXXX") + '}';
    }
}
