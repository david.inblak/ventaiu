/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//package neto.sion.tienda.venta.utilerias.bean;
package neto.sion.tienda.venta.modelo;

/**
 *
 * @author fvega
 */
public class PeticionParametroBean {
    
    private int paisId;
    private int sistemaId;
    private int moduloId;
    private int submoduloId;
    private int configuracionId;

    public int getConfiguracionId() {
        return configuracionId;
    }

    public void setConfiguracionId(int configuracionId) {
        this.configuracionId = configuracionId;
    }

    public int getModuloId() {
        return moduloId;
    }

    public void setModuloId(int moduloId) {
        this.moduloId = moduloId;
    }

    public int getPaisId() {
        return paisId;
    }

    public void setPaisId(int paisId) {
        this.paisId = paisId;
    }

    public int getSistemaId() {
        return sistemaId;
    }

    public void setSistemaId(int sistemaId) {
        this.sistemaId = sistemaId;
    }

    public int getSubmoduloId() {
        return submoduloId;
    }

    public void setSubmoduloId(int submoduloId) {
        this.submoduloId = submoduloId;
    }

    @Override
    public String toString() {
        return "PeticionParametroBean{" + "paisId=" + paisId + ", sistemaId=" + sistemaId + ", moduloId=" + moduloId + ", submoduloId=" + submoduloId + ", configuracionId=" + configuracionId + '}';
    }
    
        
}
