/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

/**
 *
 * @author fvega
 */
public class RespuestaCancelacionVentaBean {

    private int codigoError;
    private String descripcionError;

    public int getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }

    public String getDescripcionError() {
        return descripcionError;
    }

    public void setDescripcionError(String descripcionError) {
        this.descripcionError = descripcionError;
    }

    @Override
    public String toString() {
        return "RespuestaCancelacionVenta{" + "codigoError=" + codigoError + ", descripcionError=" + descripcionError + '}';
    }
}
