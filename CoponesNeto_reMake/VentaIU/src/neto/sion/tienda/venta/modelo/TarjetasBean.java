/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author fvega
 */
public class TarjetasBean {

    SimpleStringProperty track1;
    SimpleStringProperty track2;
    SimpleStringProperty ingreso;
    SimpleStringProperty canal;
    SimpleStringProperty terminal;
    SimpleStringProperty sucursal;
    SimpleStringProperty afiliacion;
    SimpleStringProperty tienda;
    SimpleStringProperty idreferencia;
    SimpleStringProperty tarjetaidbus;
    SimpleDoubleProperty monto;
    SimpleDoubleProperty comision;
    SimpleStringProperty autorizacion;
    SimpleStringProperty mensaje;
    SimpleStringProperty trace;
    SimpleStringProperty fechalocal;
    SimpleStringProperty horalocal;
    SimpleStringProperty codigo;
    SimpleIntegerProperty autorizada;
    SimpleStringProperty desctarjeta;
    SimpleStringProperty numerotarjeta;
    SimpleStringProperty nombreUsuario;
    SimpleIntegerProperty tipoTarjeta;
    int contador = 0;

    public TarjetasBean(String atrack1, String atrack2, String aingreso, String acanal, String aterminal, String asucursal,
            String aafiliacion, String atienda, String aidreferencia, String atarjetaidbus, double amonto, double acomision,
            String aautorizacion, String amensaje, String atrace, String afechalocal, String ahoralocal, String acodigo,
            int aautorizada, String adescTarjeta, int aTipoTarjeta, String anumerotarjeta, String aNombreUsuario) {
        this.track1 = new SimpleStringProperty(atrack1);
        this.track2 = new SimpleStringProperty(atrack2);
        this.ingreso = new SimpleStringProperty(aingreso);
        this.canal = new SimpleStringProperty(acanal);
        this.terminal = new SimpleStringProperty(aterminal);
        this.sucursal = new SimpleStringProperty(asucursal);
        this.afiliacion = new SimpleStringProperty(aafiliacion);
        this.tienda = new SimpleStringProperty(atienda);
        this.idreferencia = new SimpleStringProperty(aidreferencia);
        this.tarjetaidbus = new SimpleStringProperty(atarjetaidbus);
        this.monto = new SimpleDoubleProperty(amonto);
        this.comision = new SimpleDoubleProperty(acomision);
        this.autorizacion = new SimpleStringProperty(aautorizacion);
        this.mensaje = new SimpleStringProperty(amensaje);
        this.trace = new SimpleStringProperty(atrace);
        this.fechalocal = new SimpleStringProperty(afechalocal);
        this.horalocal = new SimpleStringProperty(ahoralocal);
        this.codigo = new SimpleStringProperty(acodigo);
        this.autorizada = new SimpleIntegerProperty(aautorizada);
        this.desctarjeta = new SimpleStringProperty(adescTarjeta);
        this.tipoTarjeta = new SimpleIntegerProperty(aTipoTarjeta);
        this.numerotarjeta = new SimpleStringProperty(anumerotarjeta);
        this.nombreUsuario = new SimpleStringProperty(aNombreUsuario);
    }

    /*public void setnull() {
        throw new NullPointerException();
    }*/

    public Integer getTipoTarjeta() {
        return tipoTarjeta.get();
    }

    public void setTipoTarjeta(Integer aTipoTarjeta) {
        tipoTarjeta.set(aTipoTarjeta);
    }

    public SimpleIntegerProperty tipoTarjetaProperty() {
        return tipoTarjeta;
    }

    public String getNombreUsuario() {
        return nombreUsuario.get();
    }

    public void setNombreUsuario(String aNombreUsuario) {
        nombreUsuario.set(aNombreUsuario);
    }

    public SimpleStringProperty nombreUsuarioProperty() {
        return nombreUsuario;
    }

    public String getNumerotarjeta() {
        return numerotarjeta.get();
    }

    public void setNumerotarjeta(String anumerotarjeta) {
        numerotarjeta.set(anumerotarjeta);
    }

    public SimpleStringProperty numeroTarjetaProperty() {
        return numerotarjeta;
    }

    public String getDesctarjeta() {
        return desctarjeta.get();
    }

    public void setDesctarjeta(String adesctarjeta) {
        desctarjeta.set(adesctarjeta);
    }

    public SimpleStringProperty descTarjetaOProperty() {
        return desctarjeta;
    }

    public Integer getAutorizada() {
        return autorizada.get();
    }

    public void setAutorizada(int aautorizada) {
        autorizada.set(aautorizada);
    }

    public SimpleIntegerProperty autorizadaProperty() {
        return autorizada;
    }
    //////    

    public String getAutorizacion() {
        return autorizacion.get();
    }

    public void setAutorizacion(String aautorizacion) {
        autorizacion.set(aautorizacion);
    }

    public SimpleStringProperty autorizacionProperty() {
        return autorizacion;
    }
    ///////

    public String getCodigo() {
        return codigo.get();
    }

    public void setCodigo(String acodigo) {
        codigo.set(acodigo);
    }

    public SimpleStringProperty codigoProperty() {
        return codigo;
    }
    ///////

    public String getFechalocal() {
        return fechalocal.get();
    }

    public void setFechalocal(String afechalocal) {
        fechalocal.set(afechalocal);
    }

    public SimpleStringProperty fechalocalProperty() {
        return fechalocal;
    }
    //////

    public String getHoralocal() {
        return horalocal.get();
    }

    public void setHoralocal(String ahoralocal) {
        horalocal.set(ahoralocal);
    }

    public SimpleStringProperty horalocalProperty() {
        return horalocal;
    }
    //////

    public String getMensaje() {
        return mensaje.get();
    }

    public void setMensaje(String amensaje) {
        mensaje.set(amensaje);
    }

    public SimpleStringProperty mensajeProperty() {
        return mensaje;
    }
    //////

    public String getTrace() {
        return trace.get();
    }

    public void setTrace(String atrace) {
        trace.set(atrace);
    }

    public SimpleStringProperty traceProperty() {
        return trace;
    }
    //////

    public Double getMonto() {
        return monto.get();
    }

    public void setMonto(double amonto) {
        monto.set(amonto);
    }

    public SimpleDoubleProperty montoProperty() {
        return monto;
    }
    /////

    public Double getComision() {
        return comision.get();
    }

    public void setComision(double acomision) {
        comision.set(acomision);
    }

    public SimpleDoubleProperty comisionProperty() {
        return comision;
    }
    //////

    public String getAfiliacion() {
        return afiliacion.get();
    }

    public void setAfiliacion(String aafiliacion) {
        afiliacion.set(aafiliacion);
    }

    public SimpleStringProperty afiliacionProperty() {
        return afiliacion;
    }
    /////

    public String getCanal() {
        return canal.get();
    }

    public void setCanal(String acanal) {
        canal.set(acanal);
    }

    public SimpleStringProperty canalProperty() {
        return canal;
    }
    /////

    public String getIdreferencia() {
        return idreferencia.get();
    }

    public void setIdreferencia(String aidreferencia) {

        idreferencia.set(aidreferencia);
    }

    public SimpleStringProperty idreferenciaProperty() {
        return idreferencia;
    }
    /////

    public String getIngreso() {
        return ingreso.get();
    }

    public void setIngreso(String aingreso) {
        ingreso.set(aingreso);
    }

    public SimpleStringProperty ingresoProperty() {
        return ingreso;
    }
    /////

    public String getSucursal() {
        return sucursal.get();
    }

    public void setSucursal(String asucursal) {
        sucursal.set(asucursal);
    }

    public SimpleStringProperty sucursalProperty() {
        return sucursal;
    }
    /////

    public String getTarjetaidbus() {
        return tarjetaidbus.get();
    }

    public void setTarjetaidbus(String atarjetaidbus) {
        tarjetaidbus.set(atarjetaidbus);
    }

    public SimpleStringProperty tarjetaidbusProperty() {
        return tarjetaidbus;
    }
    /////

    public String getTerminal() {
        return terminal.get();
    }

    public void setTerminal(String aterminal) {
        terminal.set(aterminal);
    }

    public SimpleStringProperty terminalProperty() {
        return terminal;
    }
    //////

    public String getTienda() {
        return tienda.get();
    }

    public void setTienda(String atienda) {
        tienda.set(atienda);
    }

    public SimpleStringProperty tiendaProperty() {
        return tienda;
    }

    public String getTrack1() {
        return track1.get();
    }

    public void setTrack1(String atrack1) {
        track1.set(atrack1);
    }

    public SimpleStringProperty track1Property() {
        return track1;
    }
    //////

    public String getTrack2() {
        return track2.get();
    }

    public void setTrack2(String atrack2) {
        track2.set(atrack2);
    }

    public SimpleStringProperty track2Property() {
        return track2;
    }

    @Override
    public String toString() {
        return "TarjetasBean{" + "track1=" + track1.get() + ", track2=" + track2.get() + 
                ", ingreso=" + ingreso.get() + ", canal=" + canal.get() + ", terminal=" + terminal.get() + 
                ", sucursal=" + sucursal.get() + ", afiliacion=" + afiliacion.get() + 
                ", tienda=" + tienda.get() + ", idreferencia=" + idreferencia.get() + 
                ", tarjetaidbus=" + tarjetaidbus.get() + ", monto=" + monto.get() + 
                ", comision=" + comision.get() + ", autorizacion=" + autorizacion.get() + 
                ", mensaje=" + mensaje.get() + ", trace=" + trace.get() + 
                ", fechalocal=" + fechalocal.get() + ", horalocal=" + horalocal.get() + 
                ", codigo=" + codigo.get() + ", autorizada=" + autorizada.get() + 
                ", desctarjeta=" + desctarjeta.get() + ", numerotarjeta=" + numerotarjeta.get() + 
                ", nombreUsuario=" + nombreUsuario.get() + ", tipoTarjeta=" + tipoTarjeta.get() + 
                ", contador=" + contador + '}';
    }
    
    
}
