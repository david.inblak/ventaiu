/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.vista;

import com.sion.tienda.genericos.ventanas.Ventana;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author dramirezr
 */
public class VentanaCupones implements Ventana{
            
    Group root;  
    
    public VentanaCupones(){
        root=new Group();
        root.getStyleClass().add("Root");
    }
    
    @Override
    public Parent obtenerEscena(Object... os) {
        
        root.getChildren().clear();
        root.getChildren().addAll((Pane)os[0]); 
        return root;
        
    }
    
    @Override
    public void limpiar() {
    }

    @Override
    public String[] getUserAgentStylesheet() {
        return new String[]{
            //"/neto/sion/tienda/venta/imagenes/inicio/Venta.css", "/neto/sion/tienda/venta/promociones/redondeo/vista/estilosPromociones.css"
        };
    }
    
}
