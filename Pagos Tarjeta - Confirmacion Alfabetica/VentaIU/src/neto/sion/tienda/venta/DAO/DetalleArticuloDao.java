/**
 *
 * @author fvega
 */
package neto.sion.tienda.venta.DAO;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.logging.Level;
import neto.sion.tienda.controles.vista.Utilerias;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.modelo.ArticuloBean;
import neto.sion.tienda.venta.modelo.PeticionDetalleArticuloBean;
import neto.sion.tienda.venta.modelo.RespuestaDetalleArticuloBean;
//import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;
import oracle.jdbc.internal.OracleTypes;

public class DetalleArticuloDao {

    private String sql;
    private CallableStatement cs;
    private ResultSet rs;
    private NumberFormat formato;
    private DecimalFormatSymbols simbolo;
    private Locale lugar = new Locale("es", "MX");
    private ArticuloBean articulo;
    private NumberFormat nf;
    private DecimalFormat df;
    private final int VENTANA_VENTA = 0;
    private final int VENTANA_DEVOLUCIONES = 1;
    private String cadena;
    private Utilerias utilerias;
    
    public DetalleArticuloDao(Utilerias _utilerias) {
        this.utilerias = _utilerias;
        this.simbolo = new DecimalFormatSymbols();
        simbolo.setDecimalSeparator('.');
        this.sql = null;
        this.cs = null;
        this.rs = null;
        this.formato = new DecimalFormat("#.00", simbolo);
        this.nf = null;
        this.df = null;
        this.cadena = "";
    }

    public RespuestaDetalleArticuloBean consultaArticulo(PeticionDetalleArticuloBean _peticion) {

        if (_peticion != null) {
            RespuestaDetalleArticuloBean respuesta = new RespuestaDetalleArticuloBean();

            articulo = null;
            articulo = new ArticuloBean();
            try {

                sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAVENTAS.SPCONSULTAARTICULO");
                cs = Conexion.prepararLlamada(sql);
                cs.setString(1, _peticion.getCodigoBarras());
                cs.registerOutParameter(2, OracleTypes.NUMBER);
                cs.registerOutParameter(3, OracleTypes.VARCHAR);
                cs.registerOutParameter(4, OracleTypes.NUMBER);
                cs.registerOutParameter(5, OracleTypes.NUMBER);
                cs.registerOutParameter(6, OracleTypes.NUMBER);
                cs.registerOutParameter(7, OracleTypes.NUMBER);
                cs.registerOutParameter(8, OracleTypes.NUMBER);
                cs.registerOutParameter(9, OracleTypes.NUMBER);
                cs.registerOutParameter(10, OracleTypes.NUMBER);
                cs.registerOutParameter(11, OracleTypes.NUMBER);
                cs.registerOutParameter(12, OracleTypes.NUMBER);
                cs.registerOutParameter(13, OracleTypes.NUMBER);
                cs.registerOutParameter(14, OracleTypes.NUMBER);
                cs.registerOutParameter(15, OracleTypes.VARCHAR);
                cs.execute();

                articulo.setCantidad(Double.parseDouble(utilerias.getNumeroFormateado(_peticion.getCantidad() * cs.getInt(11))));
                articulo.setPacdgbarras(_peticion.getCodigoBarras());
                articulo.setPaarticuloid(cs.getLong(2));
                articulo.setPanombrearticulo(cs.getString(3));
                articulo.setPaUnidadMedida(cs.getInt(4));
                articulo.setPafamiliaid(cs.getInt(5));
                articulo.setPaprecio(Double.parseDouble(utilerias.getNumeroFormateado(cs.getDouble(6))));
                articulo.setPacosto(cs.getDouble(7));
                articulo.setPaiva(cs.getDouble(8));
                articulo.setPadescuento(cs.getInt(9));
                articulo.setPagranel(cs.getDouble(10));
                articulo.setPaunidad(cs.getInt(11));
                articulo.setPaCdgBarrasPadre(cs.getLong(12));
                articulo.setPaIepsId(cs.getInt(13));
                articulo.setPacdgerror(cs.getInt(14));
                articulo.setPadescerror(cs.getString(15));
                articulo.setImporte(utilerias.RedondearImporte(articulo.getCantidad(), cs.getDouble(6), articulo.getPadescuento() ));
                articulo.setDevcantidad(0);
                articulo.setDevimporte(0);

                respuesta.setCodigoError(articulo.getPacdgerror());
                respuesta.setDescripcionError(articulo.getPadescerror());
                respuesta.setArticulos(articulo);
            } catch (Exception e) {
                SION.logearExcepcion(Modulo.VENTA, e, "Se generó un error al consultar peticion");
                respuesta = null;
            } finally {
                SION.log(Modulo.VENTA, "\n"+
                        "SQL a ejecutar... " + sql + "\n"+
                        "Peticion de articulo: " + _peticion +"\n"+
                        "Respuesta obtenida: " + respuesta.toString(), Level.INFO);
                
                Conexion.cerrarResultSet(rs);
                Conexion.cerrarCallableStatement(cs);
            }

            return respuesta;
        } else {
            return null;
        }
    }
    
}
