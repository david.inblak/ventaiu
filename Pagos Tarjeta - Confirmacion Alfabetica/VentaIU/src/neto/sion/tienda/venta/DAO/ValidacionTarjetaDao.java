/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.DAO;

import neto.sion.tienda.venta.bean.PeticionActualizaConciliacionPendienteBean;
import java.sql.CallableStatement;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.bean.RespuestaActualizaConciliacionPendienteBean;
import neto.sion.tienda.venta.modelo.PeticionValidacionTarjetaBean;
import neto.sion.tienda.venta.modelo.RespuestaValidacionTarjetaBean;
import oracle.jdbc.internal.OracleTypes;

/**
 *
 * @author fvega
 */
public class ValidacionTarjetaDao {
    
    private String sql;
    private CallableStatement cs;
    
    public ValidacionTarjetaDao() {
        this.sql = null;
        this.cs = null;
    }
    
    public RespuestaValidacionTarjetaBean validartarjeta (PeticionValidacionTarjetaBean _peticion){
        RespuestaValidacionTarjetaBean respuesta = new RespuestaValidacionTarjetaBean();
        
        SION.log(Modulo.VENTA, "Petición de validación de tarjeta: "+_peticion.toString(), Level.INFO);
        
        try {

            sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAVENTAS.SPVALIDATARJETA");
            SION.log(Modulo.VENTA, "SQL a ejecutar... " + sql, Level.INFO);
            cs = Conexion.prepararLlamada(sql);
            cs.setString(1, _peticion.getNumerotarjeta());
            cs.registerOutParameter(2, OracleTypes.NUMBER);
            cs.registerOutParameter(3, OracleTypes.VARCHAR);
            cs.execute();

            respuesta.setCodigoError(cs.getInt(2));
            respuesta.setDescError(cs.getString(3));
                        
            SION.log(Modulo.VENTA, "Respuesta recibida: "+respuesta.toString(), Level.INFO);

        } catch (Exception e) {
            respuesta=null;
            SION.logearExcepcion(Modulo.VENTA, e, sql);
        } finally {
            Conexion.cerrarCallableStatement(cs);
        }

        return respuesta;
    }
    
    public RespuestaActualizaConciliacionPendienteBean actualizaConciliacionPendiente (PeticionActualizaConciliacionPendienteBean _peticion){
        RespuestaActualizaConciliacionPendienteBean respuesta = new RespuestaActualizaConciliacionPendienteBean();
        
        SION.log(Modulo.VENTA, "Petición de actualizacion de conciliación: "+_peticion.toString(), Level.INFO);
        
        try {

            sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAVENTAS.SPACTCONCILIACIONLOCALPEND");
            SION.log(Modulo.VENTA, "SQL a ejecutar... " + sql, Level.INFO);
            cs = Conexion.prepararLlamada(sql);
            cs.setInt  (1, _peticion.getPaPaisId());
            cs.setInt  (2, _peticion.getPaTiendaId());
            cs.setLong (3, _peticion.getPaConciliacionId());
            cs.registerOutParameter(4, OracleTypes.NUMBER);
            cs.registerOutParameter(5, OracleTypes.VARCHAR);
            cs.execute();

            respuesta.setCodigoError(cs.getInt(4));
            respuesta.setDescripcionError(cs.getString(5));
                        
            SION.log(Modulo.VENTA, "Respuesta de actualizacion de conciliación: "+respuesta.toString(), Level.INFO);

        } catch (Exception e) {
            respuesta=null;
            SION.logearExcepcion(Modulo.VENTA, e, sql);
        } finally {
            Conexion.cerrarCallableStatement(cs);
        }

        return respuesta;
    }
    
}
