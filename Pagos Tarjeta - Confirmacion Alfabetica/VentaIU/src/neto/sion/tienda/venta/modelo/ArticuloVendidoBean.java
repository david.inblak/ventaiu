/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

/**
 *
 * @author fvega
 */
public class ArticuloVendidoBean {

    private long articuloId;
    private String codigoBarras;
    private double cantidad;
    private double precio;
    private double costo;
    private double descuento;
    private double iva;
    private int ieps;

    public long getArticuloId() {
        return articuloId;
    }

    public void setArticuloId(long articuloId) {
        this.articuloId = articuloId;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getIeps() {
        return ieps;
    }

    public void setIeps(int ieps) {
        this.ieps = ieps;
    }

    @Override
    public String toString() {
        return "ArticuloVendidoBean{" + "articuloId=" + articuloId + ", codigoBarras=" + codigoBarras + ", cantidad=" + cantidad + ", precio=" + precio + ", costo=" + costo + ", descuento=" + descuento + ", iva=" + iva + ", ieps=" + ieps + '}';
    }
}
