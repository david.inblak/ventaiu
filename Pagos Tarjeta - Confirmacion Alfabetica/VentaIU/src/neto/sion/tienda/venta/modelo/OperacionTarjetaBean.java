/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

public class OperacionTarjetaBean
{
  private PeticionPagoTarjetaBean peticionPago;
  private RespuestaPagoTarjetaBean respuestaPago;
  
  public PeticionPagoTarjetaBean getPeticionPago()
  {
    return peticionPago;
  }
  
  public void setPeticionPago(PeticionPagoTarjetaBean peticionPago)
  {
    this.peticionPago = peticionPago;
  }
  
  public RespuestaPagoTarjetaBean getRespuestaPago()
  {
    return respuestaPago;
  }
  
  public void setRespuestaPago(RespuestaPagoTarjetaBean respuestaPago)
  {
    this.respuestaPago = respuestaPago;
  }
  
  public String toString()
  {
    return "OperacionTarjetaBean{peticionPago=" + peticionPago + ", respuestaPago=" + respuestaPago + '}';
  }
}
