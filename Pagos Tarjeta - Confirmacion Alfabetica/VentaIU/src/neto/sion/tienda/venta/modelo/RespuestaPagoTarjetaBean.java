/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

import neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto;
import neto.sion.tienda.venta.wssion.dto.PeticionPagatodoDto;


public class RespuestaPagoTarjetaBean
{
  private int codigoError;
  private String descError;
  private String numeroAutorizacion;
  private int tipoTarjeta;
  private String numerotarjeta;
  private String descTarjeta;
  private String nombreUsuario;
  private PeticionReversoTarjetaBean peticionReverso;
  private String estatusLectura;
  
  private PeticionPagatodoDto solicitudPagaTdo;
  private PeticionPagoTarjetaDto peticionPagoTarjeta;

    public PeticionPagoTarjetaDto getPeticionPagoTarjeta() {
        return peticionPagoTarjeta;
    }

    public void setPeticionPagoTarjeta(PeticionPagoTarjetaDto peticionPagoTarjeta) {
        this.peticionPagoTarjeta = peticionPagoTarjeta;
    }

    public PeticionPagatodoDto getSolicitudPagaTdo() {
        return solicitudPagaTdo;
    }

    public void setSolicitudPagaTdo(PeticionPagatodoDto solicitudPagaTdo) {
        this.solicitudPagaTdo = solicitudPagaTdo;
    }
  
  public int getCodigoError()
  {
    return codigoError;
  }
  
  public void setCodigoError(int codigoError)
  {
    this.codigoError = codigoError;
  }
  
  public String getDescError()
  {
    return descError;
  }
  
  public void setDescError(String descError)
  {
    this.descError = descError;
  }
  
  public String getNumeroAutorizacion()
  {
    return numeroAutorizacion;
  }
  
  public void setNumeroAutorizacion(String numeroAutorizacion)
  {
    this.numeroAutorizacion = numeroAutorizacion;
  }
  
  public int getTipoTarjeta()
  {
    return tipoTarjeta;
  }
  
  public void setTipoTarjeta(int tipoTarjeta)
  {
    this.tipoTarjeta = tipoTarjeta;
  }
  
  public PeticionReversoTarjetaBean getPeticionReverso()
  {
    return peticionReverso;
  }
  
  public void setPeticionReverso(PeticionReversoTarjetaBean peticionReverso)
  {
    this.peticionReverso = peticionReverso;
  }
  
  public String getDescTarjeta()
  {
    return descTarjeta;
  }
  
  public void setDescTarjeta()
  {
    switch (tipoTarjeta)
    {
    case 1: 
      descTarjeta = "DEBITO";
      break;
    case 2: 
      descTarjeta = "CREDITO";
      break;
    case 3: 
      descTarjeta = "VALE ELECTRONICO";
    }
  }
  public void setDescTarjeta(String desc)
  {
    descTarjeta = desc;
    if( descTarjeta == null || descTarjeta.isEmpty() )
        setDescTarjeta();
  }
  
  public String getNumerotarjeta()
  {
    return numerotarjeta;
  }
  
  public void setNumerotarjeta(String numerotarjeta)
  {
    this.numerotarjeta = numerotarjeta;
  }
  
  public String getNombreUsuario()
  {
    return nombreUsuario;
  }
  
  public void setNombreUsuario(String nombreUsuario)
  {
    this.nombreUsuario = nombreUsuario;
  }
  
  public String _toString()
  {
    return "RespuestaPagoTarjetaBean{codigoError=" + codigoError + ", descError=" + descError + '}';
  }
  
  public String getEstatusLectura()
  {
    return estatusLectura;
  }
  
  public void setEstatusLectura(String estatusLectura)
  {
    this.estatusLectura = estatusLectura;
  }
  
  public String toString()
  {
    return "RespuestaPagoTarjetaBean{codigoError=" + codigoError + ", descError=" + descError + ", numeroAutorizacion=" + numeroAutorizacion + ", tipoTarjeta=" + tipoTarjeta + ", numerotarjeta=" + numerotarjeta + ", descTarjeta=" + descTarjeta + ", nombreUsuario=" + nombreUsuario + ", peticionReverso=" + peticionReverso + ", estatusLectura=" + estatusLectura + '}';
  }
}
