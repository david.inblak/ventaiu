/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

/**
 *
 * @author fvega
 */
public class BloqueoLocalBean {

    private int paTipoPago;
    private int paNumAvisos;
    private int paNumAvisosRestantes;
    private int paEstatusBloqueo;

    public int getPaEstatusBloqueo() {
        return paEstatusBloqueo;
    }

    public void setPaEstatusBloqueo(int paEstatusBloqueo) {
        this.paEstatusBloqueo = paEstatusBloqueo;
    }

    public int getPaNumAvisos() {
        return paNumAvisos;
    }

    public void setPaNumAvisos(int paNumAvisos) {
        this.paNumAvisos = paNumAvisos;
    }

    public int getPaNumAvisosRestantes() {
        return paNumAvisosRestantes;
    }

    public void setPaNumAvisosRestantes(int paNumAvisosRestantes) {
        this.paNumAvisosRestantes = paNumAvisosRestantes;
    }

    public int getPaTipoPago() {
        return paTipoPago;
    }

    public void setPaTipoPago(int paTipoPago) {
        this.paTipoPago = paTipoPago;
    }

    @Override
    public String toString() {
        return "BloqueoLocalBean{" + "paTipoPago=" + paTipoPago + ", paNumAvisos=" + paNumAvisos + ", paNumAvisosRestantes=" + paNumAvisosRestantes + ", paEstatusBloqueo=" + paEstatusBloqueo + '}';
    }
}
