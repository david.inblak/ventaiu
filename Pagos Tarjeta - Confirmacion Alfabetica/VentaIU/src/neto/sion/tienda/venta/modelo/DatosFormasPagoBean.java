/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package neto.sion.tienda.venta.modelo;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto;
import neto.sion.tienda.venta.wssion.dto.PeticionPagatodoDto;

/**
 *
 * @author fvega
 */
public class DatosFormasPagoBean {

    public SimpleIntegerProperty fitipopagoid;
    public SimpleDoubleProperty fnmontopago;
    public SimpleIntegerProperty fnnumerovales;
    public SimpleStringProperty fcnumerotarjeta;
    public SimpleIntegerProperty fitipotarjeta;
    public SimpleLongProperty tarjetaidbus;
    public SimpleDoubleProperty importeAdicional;
    public SimpleStringProperty estatusLectura;
    public SimpleBooleanProperty esTarjetaPagaTodo;
    public SimpleBooleanProperty esTarjetaDescuentos;
    
    private PeticionPagatodoDto solicitudPagaTdo;
    private PeticionPagoTarjetaDto peticionPagoTarjeta;

    public PeticionPagoTarjetaDto getPeticionPagoTarjeta() {
        return peticionPagoTarjeta;
    }

    public void setPeticionPagoTarjeta(PeticionPagoTarjetaDto peticionPagoTarjeta) {
        this.peticionPagoTarjeta = peticionPagoTarjeta;
    }

    public PeticionPagatodoDto getSolicitudPagaTdo() {
        return solicitudPagaTdo;
    }

    public void setSolicitudPagaTdo(PeticionPagatodoDto solicitudPagaTdo) {
        this.solicitudPagaTdo = solicitudPagaTdo;
    }


    public DatosFormasPagoBean(int afitipopagoid, double afnmontopago, int afnnumerovales, String afcnumerotarjeta,
            int afitipotarjeta, long atarjetaidbus, double acomisiontarjeta, String aEstatusLectura) {
        this.fitipopagoid = new SimpleIntegerProperty(afitipopagoid);
        this.fnmontopago = new SimpleDoubleProperty(afnmontopago);
        this.fnnumerovales = new SimpleIntegerProperty(afnnumerovales);
        this.fcnumerotarjeta = new SimpleStringProperty(afcnumerotarjeta);
        this.fitipotarjeta = new SimpleIntegerProperty(afitipotarjeta);
        this.tarjetaidbus = new SimpleLongProperty(atarjetaidbus);
        this.importeAdicional = new SimpleDoubleProperty(acomisiontarjeta);
        this.estatusLectura = new SimpleStringProperty(aEstatusLectura);
        this.esTarjetaPagaTodo = new SimpleBooleanProperty(false);
    }
    
    public DatosFormasPagoBean(int afitipopagoid, double afnmontopago, int afnnumerovales, String afcnumerotarjeta,
            int afitipotarjeta, long atarjetaidbus, double acomisiontarjeta, String aEstatusLectura, boolean estarjetapagatodo) {
        this.fitipopagoid = new SimpleIntegerProperty(afitipopagoid);
        this.fnmontopago = new SimpleDoubleProperty(afnmontopago);
        this.fnnumerovales = new SimpleIntegerProperty(afnnumerovales);
        this.fcnumerotarjeta = new SimpleStringProperty(afcnumerotarjeta);
        this.fitipotarjeta = new SimpleIntegerProperty(afitipotarjeta);
        this.tarjetaidbus = new SimpleLongProperty(atarjetaidbus);
        this.importeAdicional = new SimpleDoubleProperty(acomisiontarjeta);
        this.estatusLectura = new SimpleStringProperty(aEstatusLectura);
        this.esTarjetaPagaTodo = new SimpleBooleanProperty(estarjetapagatodo);
    }
    
    public DatosFormasPagoBean(int afitipopagoid, double afnmontopago, int afnnumerovales, String afcnumerotarjeta,
            int afitipotarjeta, long atarjetaidbus, double acomisiontarjeta, String aEstatusLectura, boolean estarjetapagatodo, 
            boolean estarjetadescuentos) {
        this.fitipopagoid = new SimpleIntegerProperty(afitipopagoid);
        this.fnmontopago = new SimpleDoubleProperty(afnmontopago);
        this.fnnumerovales = new SimpleIntegerProperty(afnnumerovales);
        this.fcnumerotarjeta = new SimpleStringProperty(afcnumerotarjeta);
        this.fitipotarjeta = new SimpleIntegerProperty(afitipotarjeta);
        this.tarjetaidbus = new SimpleLongProperty(atarjetaidbus);
        this.importeAdicional = new SimpleDoubleProperty(acomisiontarjeta);
        this.estatusLectura = new SimpleStringProperty(aEstatusLectura);
        this.esTarjetaPagaTodo = new SimpleBooleanProperty(estarjetapagatodo);
        this.esTarjetaDescuentos = new SimpleBooleanProperty(estarjetadescuentos);
    }
    
    public DatosFormasPagoBean(int afitipopagoid, double afnmontopago, int afnnumerovales, String afcnumerotarjeta,
            int afitipotarjeta, long atarjetaidbus, double acomisiontarjeta, String aEstatusLectura, boolean estarjetapagatodo,
            PeticionPagatodoDto solicitudPagaTdo, PeticionPagoTarjetaDto peticionPagoTarjeta) {
        this.fitipopagoid = new SimpleIntegerProperty(afitipopagoid);
        this.fnmontopago = new SimpleDoubleProperty(afnmontopago);
        this.fnnumerovales = new SimpleIntegerProperty(afnnumerovales);
        this.fcnumerotarjeta = new SimpleStringProperty(afcnumerotarjeta);
        this.fitipotarjeta = new SimpleIntegerProperty(afitipotarjeta);
        this.tarjetaidbus = new SimpleLongProperty(atarjetaidbus);
        this.importeAdicional = new SimpleDoubleProperty(acomisiontarjeta);
        this.estatusLectura = new SimpleStringProperty(aEstatusLectura);
        this.esTarjetaPagaTodo = new SimpleBooleanProperty(estarjetapagatodo);
        this.solicitudPagaTdo = solicitudPagaTdo;
        this.peticionPagoTarjeta = peticionPagoTarjeta;
    }

    public DatosFormasPagoBean(int afitipopagoid, double afnmontopago, int afnnumerovales, String afcnumerotarjeta,
            int afitipotarjeta, long atarjetaidbus, double acomisiontarjeta) {
        this.fitipopagoid = new SimpleIntegerProperty(afitipopagoid);
        this.fnmontopago = new SimpleDoubleProperty(afnmontopago);
        this.fnnumerovales = new SimpleIntegerProperty(afnnumerovales);
        this.fcnumerotarjeta = new SimpleStringProperty(afcnumerotarjeta);
        this.fitipotarjeta = new SimpleIntegerProperty(afitipotarjeta);
        this.tarjetaidbus = new SimpleLongProperty(atarjetaidbus);
        this.importeAdicional = new SimpleDoubleProperty(acomisiontarjeta);
        this.estatusLectura = new SimpleStringProperty("");
        this.esTarjetaPagaTodo = new SimpleBooleanProperty(false);
    }
    
    
   
    public double getComisiontarjeta() {
        return importeAdicional.get();
    }

    public void setComisiontarjeta(double acomisiontarjeta) {
        importeAdicional.set(acomisiontarjeta);
    }

    SimpleDoubleProperty comisiontarjetaProperty() {
        return importeAdicional;
    }
    /////

    public Long getTarjetaidbus() {
        return tarjetaidbus.get();
    }

    public void setTarjetaidbus(Long atarjetaidbus) {
        tarjetaidbus.set(atarjetaidbus);
    }

    SimpleLongProperty tarjetaidbusProperty() {
        return tarjetaidbus;
    }
    ////

    public int getFitipopagoid() {
        return fitipopagoid.get();
    }

    public void setFitipopagoid(int afitipopagoid) {
        fitipopagoid.set(afitipopagoid);
    }

    public SimpleIntegerProperty fitipopagoidProperty() {
        return fitipopagoid;
    }
    /////

    public double getFnmontopago() {
        return fnmontopago.get();
    }

    public void setFnmontopago(double afnmontopago) {
        fnmontopago.set(afnmontopago);
    }

    public SimpleDoubleProperty fnmontopagoProperty() {
        return fnmontopago;
    }
    /////

    public int getFnnumerovales() {
        return fnnumerovales.get();
    }

    public void setFnnumerovales(int afnnumerovales) {
        fnnumerovales.set(afnnumerovales);
    }

    public SimpleIntegerProperty fnnumerovalesProperty() {
        return fnnumerovales;
    }
    /////

    public String getFcnumerotarjeta() {
        return fcnumerotarjeta.get();
    }

    public void setFcnumerotarjeta(String afcnumerotarjeta) {
        fcnumerotarjeta.set(afcnumerotarjeta);
    }

    public SimpleStringProperty fcnumerotarjetaProperty() {
        return fcnumerotarjeta;
    }
    /////

    public int getFitipotarjeta() {
        return fitipotarjeta.get();
    }

    public void setFitipotarjeta(int afitipotarjeta) {
        fitipopagoid.set(afitipotarjeta);
    }

    public SimpleIntegerProperty fitipotarjetaProperty() {
        return fitipotarjeta;
    }

    public SimpleStringProperty getEstatusLectura() {
        return estatusLectura;
    }

    public void setEstatusLectura(SimpleStringProperty estatusLectura) {
        this.estatusLectura = estatusLectura;
    }
    
    
    public boolean isEsTarjetaPagaTodo() {
        return esTarjetaPagaTodo.getValue();
    }

    public void setEsTarjetaPagaTodo(boolean esTarjetaPagaTodo) {
        this.esTarjetaPagaTodo.set( esTarjetaPagaTodo );
    }

    @Override
    public String toString() {
        return "DatosFormasPagoBean{" + "fitipopagoid=" + fitipopagoid.get() + ", fnmontopago=" + fnmontopago.get()
                + ", fnnumerovales=" + fnnumerovales.get() + ", fcnumerotarjeta=" + fcnumerotarjeta.get()
                + ", fitipotarjeta=" + fitipotarjeta.get() + ", tarjetaidbus=" + tarjetaidbus.get()
                + ", importeAdicional=" + importeAdicional.get()
                + ", estatusLectura=" + estatusLectura.get() 
                + ", esTarjetaPagaTodo=" + esTarjetaPagaTodo +'}';
    }
}
