/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

/**
 *
 * @author 702382
 */
public class RespuestaDetalleArticuloBean {
    
    private ArticuloBean articulos;
    private int codigoError;
    private String descripcionError;

    public int getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }

    public ArticuloBean getArticulos() {
        return articulos;
    }

    public void setArticulos(ArticuloBean datosArticulosBean) {
        this.articulos = datosArticulosBean;
    }

    public String getDescripcionError() {
        return descripcionError;
    }

    public void setDescripcionError(String descripcionError) {
        this.descripcionError = descripcionError;
    }

    @Override
    public String toString() {
        return "RespuestaArticuloLocalBean{Articulos=" + articulos + ", codigoError=" + codigoError + ", descripcionError=" + descripcionError + '}';
    }
    
    
    
    
}
