/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package neto.sion.tienda.venta.modelo;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author fvega
 */
public class ArticuloBean {

    private SimpleDoubleProperty cantidad;
    private SimpleStringProperty pacdgbarras;
    private SimpleDoubleProperty paprecio;
    private SimpleLongProperty paarticuloid;
    private SimpleStringProperty panombrearticulo;
    private SimpleIntegerProperty pafamiliaid;
    private SimpleDoubleProperty pacosto;
    private SimpleDoubleProperty paiva;
    private SimpleDoubleProperty padescuento;
    private SimpleDoubleProperty pagranel;
    private SimpleIntegerProperty paunidadmedida;
    private SimpleLongProperty paCdgBarrasPadre;
    private SimpleIntegerProperty pacdgerror;
    private SimpleStringProperty padescerror;
    private SimpleDoubleProperty importe;
    private SimpleIntegerProperty companiaid;
    private SimpleIntegerProperty proveedorTAid;
    private SimpleStringProperty descripcioncompania;
    private SimpleDoubleProperty devcantidad;
    private SimpleDoubleProperty devimporte;
    private SimpleDoubleProperty devueltos;
    private SimpleIntegerProperty paunidad;
    private SimpleIntegerProperty paIepsId;
    private SimpleIntegerProperty metodoEntrada;
    
    @Override
    public boolean equals(Object o){
        if( o == null)
            return false;
        if( !(o instanceof ArticuloBean) )
            return false;
        return this.hashCode() == o.hashCode();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.pacdgbarras != null ? this.pacdgbarras.hashCode() : 0);
        hash = 23 * hash + (this.paarticuloid != null ? this.paarticuloid.hashCode() : 0);
        return hash;
    }

    public ArticuloBean() {
        
        this.cantidad = new SimpleDoubleProperty(0);
        ////
        this.pacdgbarras = new SimpleStringProperty("");
        this.paarticuloid = new SimpleLongProperty(0);
        this.panombrearticulo = new SimpleStringProperty("");
        this.paCdgBarrasPadre = new SimpleLongProperty(0);
        this.paunidadmedida = new SimpleIntegerProperty(0);
        this.pafamiliaid = new SimpleIntegerProperty(0);
        this.paprecio = new SimpleDoubleProperty(0);
        this.pacosto = new SimpleDoubleProperty(0);
        this.paiva = new SimpleDoubleProperty(0);
        this.padescuento = new SimpleDoubleProperty(0);
        this.pagranel = new SimpleDoubleProperty(0);
        this.paCdgBarrasPadre = new SimpleLongProperty(0);
        this.pacdgerror = new SimpleIntegerProperty(0);
        this.padescerror = new SimpleStringProperty("");
        this.importe = new SimpleDoubleProperty(0);
        this.companiaid = new SimpleIntegerProperty(0);
        this.proveedorTAid = new SimpleIntegerProperty(0);
        this.descripcioncompania = new SimpleStringProperty("");
        this.devcantidad = new SimpleDoubleProperty(0);
        this.devimporte = new SimpleDoubleProperty(0);
        this.devueltos = new SimpleDoubleProperty(0);
        this.paunidad = new SimpleIntegerProperty(0);
        this.paIepsId = new SimpleIntegerProperty(0);
        this.metodoEntrada = new SimpleIntegerProperty(0);
    }
    
    public ArticuloBean(double acantidad, String apacdgbarras, double apaprecio, long apaarticuloid, String apanombrearticulo,
            int apafamiliaid, double apacosto, double apaiva, double apadescuento, double apagranel, int apaunidadmedida,
            long apaCdgBarrasPadre, int apacdgerror, String apadescerror, double aimporte, int acompaniaid, 
            String adescripcioncompania, double adevcantidad, double adevimporte, double adevueltos, int apaunidad, int aiepsid,
            int aMetodoEntrada) {
        this( acantidad,  apacdgbarras,  apaprecio,  apaarticuloid,  apanombrearticulo,
             apafamiliaid,  apacosto,  apaiva,  apadescuento,  apagranel,  apaunidadmedida,
             apaCdgBarrasPadre,  apacdgerror,  apadescerror,  aimporte,  acompaniaid, 
             adescripcioncompania,  adevcantidad,  adevimporte,  adevueltos,  apaunidad,  aiepsid,
             aMetodoEntrada, 0);
    }

    public ArticuloBean(double acantidad, String apacdgbarras, double apaprecio, long apaarticuloid, String apanombrearticulo,
            int apafamiliaid, double apacosto, double apaiva, double apadescuento, double apagranel, int apaunidadmedida,
            long apaCdgBarrasPadre, int apacdgerror, String apadescerror, double aimporte, int acompaniaid, 
            String adescripcioncompania, double adevcantidad, double adevimporte, double adevueltos, int apaunidad, int aiepsid,
            int aMetodoEntrada, int proveedorTAid) {
        this.cantidad = new SimpleDoubleProperty(acantidad);
        ////
        this.pacdgbarras = new SimpleStringProperty(apacdgbarras);
        this.paarticuloid = new SimpleLongProperty(apaarticuloid);
        this.panombrearticulo = new SimpleStringProperty(apanombrearticulo);
        this.paCdgBarrasPadre = new SimpleLongProperty(apaCdgBarrasPadre);
        this.paunidadmedida = new SimpleIntegerProperty(apaunidadmedida);
        this.pafamiliaid = new SimpleIntegerProperty(apafamiliaid);//divisionid
        this.paprecio = new SimpleDoubleProperty(apaprecio);
        this.pacosto = new SimpleDoubleProperty(apacosto);
        this.paiva = new SimpleDoubleProperty(apaiva);
        this.padescuento = new SimpleDoubleProperty(apadescuento);
        this.pagranel = new SimpleDoubleProperty(apagranel);
        this.paCdgBarrasPadre = new SimpleLongProperty(apaCdgBarrasPadre);
        this.pacdgerror = new SimpleIntegerProperty(apacdgerror);
        this.padescerror = new SimpleStringProperty(apadescerror);
        ////
        this.importe = new SimpleDoubleProperty(aimporte);
        this.companiaid = new SimpleIntegerProperty(acompaniaid);
        this.proveedorTAid = new SimpleIntegerProperty(proveedorTAid);
        this.descripcioncompania = new SimpleStringProperty(adescripcioncompania);
        this.devcantidad = new SimpleDoubleProperty(adevcantidad);
        this.devimporte = new SimpleDoubleProperty(adevimporte);
        this.devueltos = new SimpleDoubleProperty(adevueltos);
        this.paunidad = new SimpleIntegerProperty(apaunidad);
        this.paIepsId = new SimpleIntegerProperty(aiepsid);
        this.metodoEntrada = new SimpleIntegerProperty(aMetodoEntrada);
    }
    
    public int getMetodoEntrada() {
        return metodoEntrada.get();
    }

    public void setMetodoEntrada(int ametodoEntrada) {
        metodoEntrada.set(ametodoEntrada);
    }
    
    public SimpleIntegerProperty metodoEntradaProperty(){
        return metodoEntrada;
    }

    public Integer getPaunidad() {
        return paunidad.get();
    }

    public void setPaunidad(int apaunidad) {
        paunidad.set(apaunidad);
    }

    SimpleIntegerProperty paunidadproperty() {
        return paunidad;
    }

    public double getDevueltos() {
        return devueltos.get();
    }

    public void setDevueltos(double adevueltos) {
        devueltos.set(adevueltos);
    }

    public SimpleDoubleProperty devueltosProperty() {
        return devueltos;
    }
    /////

    public Integer getCompaniaid() {
        return companiaid.get();
    }

    public void setCompaniaid(int acompaniaid) {
        companiaid.set(acompaniaid);
    }

    SimpleIntegerProperty companiaidProperty() {
        return companiaid;
    }
    
    
    /////

    public String getDescripcioncompania() {
        return descripcioncompania.get();
    }

    public void setDescripcioncompania(String adescripcioncompania) {
        descripcioncompania.set(adescripcioncompania);

    }

    SimpleStringProperty descripcioncompaniaproperty() {
        return descripcioncompania;
    }
    /////

    public double getCantidad() {
        return cantidad.get();
    }

    public void setCantidad(double acantidad) {
        cantidad.set(acantidad);
    }

    public SimpleDoubleProperty cantidadProperty() {
        return cantidad;
    }
    /////

    public String getPacdgbarras() {

        return pacdgbarras.get();
    }

    public void setPacdgbarras(String apacdgbarras) {
        pacdgbarras.set(apacdgbarras);
    }

    public SimpleStringProperty PacdgbarrasProperty() {
        return pacdgbarras;
    }
    /////

    public double getPaprecio() {
        return paprecio.get();
    }

    public void setPaprecio(double apaprecio) {
        paprecio.set(apaprecio);
    }

    public SimpleDoubleProperty paprecioProperty() {
        return paprecio;
    }
    /////

    public long getPaarticuloid() {
        return paarticuloid.get();
    }

    public void setPaarticuloid(long apaarticuloid) {
        paarticuloid.set(apaarticuloid);
    }

    public SimpleLongProperty paarticuloidProperty() {
        return paarticuloid;
    }
    /////

    public String getPanombrearticulo() {
        return panombrearticulo.get();
    }

    public void setPanombrearticulo(String apanombrearticulo) {
        panombrearticulo.set(apanombrearticulo);
    }

    public SimpleStringProperty PanombrearticuloProperty() {
        return panombrearticulo;
    }
    ////

    public int getPafamiliaid() {
        return pafamiliaid.get();
    }

    public void setPafamiliaid(int apafamiliaid) {
        pafamiliaid.set(apafamiliaid);
    }

    public SimpleIntegerProperty pafamiliaidProperty() {
        return pafamiliaid;
    }
    ////

    public double getPacosto() {
        return pacosto.get();
    }

    public void setPacosto(double apacosto) {
        pacosto.set(apacosto);
    }

    public SimpleDoubleProperty pacostoproperty() {
        return pacosto;
    }
    ////

    public double getPaiva() {
        return paiva.get();
    }

    public void setPaiva(double apaiva) {
        paiva.set(apaiva);
    }

    public SimpleDoubleProperty paivaProperty() {
        return paiva;
    }
    ////

    public double getPadescuento() {
        return padescuento.get();
    }

    public void setPadescuento(double apadescuento) {
        padescuento.set(apadescuento);
    }

    public SimpleDoubleProperty padescuentoProperty() {
        return padescuento;
    }
    ////

    public double getPagranel() {
        return pagranel.get();
    }

    public void setPagranel(double apagranel) {
        pagranel.set(apagranel);
    }

    public SimpleDoubleProperty pagranelProperty() {
        return pagranel;
    }
    ////

    public int getPaUnidadMedida() {
        return paunidadmedida.get();
    }

    public void setPaUnidadMedida(int apaunidadmedida) {
        paunidadmedida.set(apaunidadmedida);
    }

    public SimpleIntegerProperty paUnidadMedidaProperty() {
        return paunidadmedida;
    }
    ////

    public long getPaCdgBarrasPadre() {
        return paCdgBarrasPadre.get();
    }

    public void setPaCdgBarrasPadre(long paCdgBarrasPadre) {
        this.paCdgBarrasPadre.set(paCdgBarrasPadre);
    }
    
    public SimpleLongProperty getPaCdgBarrasPadreProperty(){
        return paCdgBarrasPadre;
    }
    
    public int getPacdgerror() {
        return pacdgerror.get();
    }

    public void setPacdgerror(int apacdgerror) {
        pacdgerror.set(apacdgerror);
    }

    public SimpleIntegerProperty pacdgerrorProperty() {
        return pacdgerror;
    }
    ////

    public String getPadescerror() {
        return padescerror.get();
    }

    public void setPadescerror(String apadescerror) {
        padescerror.set(apadescerror);
    }

    public SimpleStringProperty padescerrorProperty() {
        return padescerror;
    }
    ////

    public double getImporte() {
        return importe.get();
    }

    public void setImporte(double aimporte) {
        importe.set(aimporte);
    }

    public SimpleDoubleProperty importeProperty() {
        return importe;
    }
    ////

    public double getDevcantidad() {
        return devcantidad.get();
    }

    public void setDevcantidad(double adevcantidad) {
        devcantidad.set(adevcantidad);
    }

    public SimpleDoubleProperty devCantidadProperty() {
        return devcantidad;
    }
    ////

    public double getDevimporte() {
        return devimporte.get();
    }

    public void setDevimporte(double adevimporte) {
        devimporte.set(adevimporte);
    }

    public SimpleDoubleProperty devImporteProperty() {
        return devimporte;
    }

    public int getPaIepsId() {
        return paIepsId.get();
    }

    public void setPaIepsId(int paIepsId) {
        this.paIepsId.set(paIepsId);
    }
    
    public SimpleIntegerProperty paIepsIdProperty(){
        return paIepsId;
    }

    public int getProveedorTAid() {
        return proveedorTAid.get();
    }

    public void setProveedorTAid(int proveedorTAid) {
        this.proveedorTAid.set(proveedorTAid);
    }
    
        
    @Override
    public String toString() {
        return "ArticuloBean{ hashcode="+hashCode()+
                ", cantidad=" + cantidad.get() + 
                ", pacdgbarras=" + pacdgbarras.get() + 
                ", paprecio=" + paprecio.get() + 
                ", paarticuloid=" + paarticuloid.get() + 
                ", panombrearticulo=" + panombrearticulo.get() + 
                ", pafamiliaid=" + pafamiliaid.get() + 
                ", pacosto=" + pacosto.get() + 
                ", paiva=" + paiva.get() + 
                ", paIeps=" + paIepsId.get() + 
                ", padescuento=" + padescuento.get() + 
                ", pagranel=" + pagranel.get() + 
                ", paunidadmedida=" + paunidadmedida.get() + 
                ", paCdgBarrasPadre=" + paCdgBarrasPadre.get() + 
                ", pacdgerror=" + pacdgerror.get() + 
                ", padescerror=" + padescerror.get() + 
                ", importe=" + importe.get() + 
                ", companiaid=" + companiaid.get() + 
                ", proveedorTAid=" + proveedorTAid.get() + 
                ", descripcioncompania=" + descripcioncompania.get() + 
                ", devcantidad=" + devcantidad.get() + 
                ", devimporte=" + devimporte.get() + 
                ", devueltos=" + devueltos.get() + 
                ", paunidad=" + paunidad.get() + 
                ", metodoEntrada=" + metodoEntrada.get() + '}';
    }
    
   
    public ArticuloBean clonar(){
        ArticuloBean a = new ArticuloBean(
                this.getCantidad(),  
                this.getPacdgbarras(),  
                this.getPaprecio(),  
                this.getPaarticuloid(),  
                this.getPanombrearticulo(),
                this.getPafamiliaid(),  
                this.getPacosto(),  
                this.getPaiva(),  
                this.getPadescuento(),  
                this.getPagranel(),  
                this.getPaUnidadMedida(),
                this.getPaCdgBarrasPadre(),  
                this.getPacdgerror(),  
                this.getPadescerror(),  
                this.getImporte(),  
                this.getCompaniaid(), 
                this.getDescripcioncompania(),  
                this.getDevcantidad(),  
                this.getDevimporte(),  
                this.getDevueltos(),  
                this.getPaunidad(),
                this.getPaIepsId(), 
                this.getMetodoEntrada(),
                this.getProveedorTAid());
        
        return a;
    } /**/
               
}
