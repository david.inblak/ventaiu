/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.bindings;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;

/**
 *
 * @author fvega
 */
public class ValidadorNumerico {

    private int longitudAnterior;
    private DecimalFormat formateador;
    private double ultimoValorValido;
    private int id;
    private String numeroFormato;
    private DecimalFormatSymbols simbolos;
    private static ChangeListener<Number> changeListener;
    private Pattern p;
    private Matcher m;
    private String cadena;
    private String dato;
    private Double prueba;
    private Double cantidad;
    private boolean existeExcepcion;
    private String valor;
    private final Pattern patronCampoTarjetas = Pattern.compile("^[0-9]{1,6}(\\.[0-9]{0,2})?$");
    private char ar[];
    private char ch;

    public ValidadorNumerico() {
        this.simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');

        this.id = 1;

        this.p = null;
        this.m = null;

        this.cadena = null;
        this.dato = null;
        this.prueba = 0.0;
        this.cantidad = 0.0;
        this.existeExcepcion = false;
        this.valor = "";
    }

    
    
    public void validadorCampoVales(final TextField campo, final int cantidadDigitosPermitidos) {

        campo.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent t) {
                
                ar = t.getCharacter().toCharArray();
                ch = ar[t.getCharacter().toCharArray().length - 1];
                
                if((ch >= '0' && ch <= '9')){///valida que sean numeros
                    if(campo.getText().length()>=cantidadDigitosPermitidos){
                        t.consume();
                    }                                                            
                }
                else{
                    t.consume();
                }
                
            }
        });

    }

    public void validadorCampoTarjetas(final TextField campo) {

        campo.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent t) {
                if (!campo.getText().contains(".")
                        && campo.getText().length() > 6) {
                    if (campo.getText().length() == 8) {
                        String parte1 =
                                campo.getText().substring(0, 6);
                        String parte2 =
                                campo.getText().substring(6, 8);
                        campo.setText(parte1 + "."
                                + parte2);
                        t.consume();
                        campo.positionCaret(campo.getText().length());
                    } else if (campo.getText().length() == 7) {
                        String parte1 = campo.getText().substring(0, 6);
                        String parte2 =
                                campo.getText().substring(6, 7);
                        campo.setText(parte1 + "."
                                + parte2);
                        t.consume();
                        campo.positionCaret(campo.getText().length());
                    } else {
                        campo.positionCaret(campo.getText().length());
                    }
                } else {
                    if( campo.getText().length() == 1 &&
                        campo.getText().startsWith("0") && 
                            t.getCharacter().equals("0") )
                    {
                        t.consume();
                    }else{
                       String parte1 = campo.getText().substring(0, campo.getCaretPosition());
                        String parte2 = campo.getText().substring(campo.getCaretPosition(), campo.getText().length());
                        String cadena = parte1 + t.getCharacter() + parte2;
                        try {
                            if (!(patronCampoTarjetas.matcher(cadena).matches())) {
                                t.consume();
                            }
                        } catch (Exception e) {
                            SION.logearExcepcion(Modulo.VENTA, e);
                        } 
                    }
                    
                }
            }
        });

    }
    
}