
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.bindings;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import neto.sion.tienda.autorizador.controlador.ControladorValidacion;
import neto.sion.tienda.autorizador.controlador.RespuestaValidacion;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.exception.SionException;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.seguridad.modelo.UsuarioSesionBean;
import neto.sion.tienda.venta.modelo.ArticuloBean;
import neto.sion.tienda.venta.modelo.DatosFormasPagoBean;
import neto.sion.tienda.controles.vista.Utilerias;
import neto.sion.tienda.venta.vista.DevolucionVista;
import neto.sion.tienda.venta.vistamodelo.DevolucionesVistaModelo;
import neto.sion.venta.servicios.cliente.dto.PeticionDetalleVentaDto;
import neto.sion.venta.servicios.cliente.dto.PeticionVentaDto;
import neto.sion.venta.servicios.cliente.dto.RespuestaDetalleVentaDto;
import neto.sion.venta.servicios.cliente.serviceFacade.ConsumirVenta;
import neto.sion.venta.servicios.cliente.serviceFacade.ConsumirVentaImp;

/**
 *
 * @author fvega
 */
public class DevolucionesBindings {

    private Utilerias utilerias;
    private DevolucionVista vista;
    private DevolucionesVistaModelo vistaModelo;
    private ArticuloBean articuloLocal;
    private ValidadorTicket validadorTicket;
    private ValidadorNumerico validador;
    private final NumberFormat formatoInt = new DecimalFormat("#");
    //private final NumberFormat formatoDouble = new DecimalFormat("#.00");
    private boolean esDevolucionExitosa;
    private double montoDevolucion;
    private double montoMaximoDevolucion;
    private long transaccion;
    private int movimientoId;
    private int index;
    private ArrayList listaCeldas;
    private ConsumirVenta comsumirVentaFachada;
    private PeticionVentaDto peticionVentaDTO;
    private PeticionDetalleVentaDto peticionDetalleVentaDTO;
    private RespuestaDetalleVentaDto respuestaDetalleVentaDTO;
    private String[] MsjDevolucion;
    private ObservableList<ArticuloBean> temporalObv;
    private boolean hayComunicacion;
    private boolean esDevolucionFueraLineaExitosa;
    private boolean seMarcoVentaFueraLinea;
    private boolean existeParametroFueraLinea;
    private boolean esAutorizadorConsultado;
    private boolean estaTicketCancelacionSeleccionado;
    private final int ACTUALIZACION_LOCAL;
    private final int ACTUALIZACION_CENTRAL;
    private DateFormat dateFormat;
    private final int PANTALLA_POLITICAS = 0;
    private final int PANTALLA_TICKET = 1;
    ///////////////////variables de cell factory
    private String cadenaDouble;
    private int indicePunto;
    private String cadenaDecimal;
    //////////////dev fuera de linea
    private int intentosMarcacionFueraDeLinea;
    /////////////dev en linea 
    private double efectivoDevolucion;
    private String fechaDevolucion;
    private double montoRecibidoDevolucion;
    private double cantidadArticulosDevolucion;
    private long conciliacionDevolucion;
    private long codigoErrorDevolucionFueraLinea;
    private long numeroConciliacion = 0;
    private int banderaDevolucion = 0;
    /////////////captura de aticulos
    private double cantidadArticulosCapturados;
    private String cadenaArticuloCapturado;
    private double idGranelArticuloCapturado;
    private Pattern patronArticuloGranel;
    private Matcher matcherPatronArticuloGranel;
    private Pattern patronMuchosArticulos;
    private Matcher matcherMuchosArticulos;
    //////dev por transaccion
    private boolean seEncontroArticuloTablaTicket;
    private int indexSeleccionadoTablaTicket;
    /////consultar comunicacion
    boolean existeComunicacion = false;
    ////mensajes
    private String[] mensajeVentana;
    
    public DevolucionesBindings(DevolucionVista devolucionVista, Utilerias _utilerias) {

        SION.log(Modulo.VENTA, "Entrando a constructor en binding de devolución", Level.INFO);

        this.utilerias = _utilerias;
        this.vista = devolucionVista;
        this.vistaModelo = new DevolucionesVistaModelo(_utilerias);
        this.articuloLocal = null;

        try {
            comsumirVentaFachada = new ConsumirVentaImp();
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "ocurrió un error al instanciar objeto de ConsumirVentafachada: " + ex.toString(), Level.SEVERE);
        }

        this.validadorTicket = new ValidadorTicket();

        this.peticionVentaDTO = new PeticionVentaDto();
        this.peticionDetalleVentaDTO = new PeticionDetalleVentaDto();
        this.respuestaDetalleVentaDTO = new RespuestaDetalleVentaDto();

        this.validador = new ValidadorNumerico();

        this.listaCeldas = new ArrayList();
        this.index = 0;
        this.esDevolucionExitosa = false;

        this.montoDevolucion = 0;
        this.montoMaximoDevolucion = 0;
        this.transaccion = 0;
        this.movimientoId = 0;

        this.MsjDevolucion = null;
        this.hayComunicacion = false;
        this.esDevolucionFueraLineaExitosa = false;
        this.seMarcoVentaFueraLinea = false;
        this.existeParametroFueraLinea = false;
        this.ACTUALIZACION_LOCAL = 0;
        this.ACTUALIZACION_CENTRAL = 1;
        this.dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        this.esAutorizadorConsultado = false;
        this.estaTicketCancelacionSeleccionado = false;
        ////cell factory
        this.cadenaDouble = "";
        this.indicePunto = 0;
        this.cadenaDecimal = "";
        ///dev fuera de linea
        this.intentosMarcacionFueraDeLinea = 0;
        ///dev en linea
        this.efectivoDevolucion = 0;
        this.fechaDevolucion = "";
        this.montoRecibidoDevolucion = 0;
        this.cantidadArticulosDevolucion = 0;
        this.conciliacionDevolucion = 0;
        this.codigoErrorDevolucionFueraLinea = 0;
        this.numeroConciliacion = 0;
        this.banderaDevolucion = 0;
        ////captura de articulos
        this.cantidadArticulosCapturados = 1;
        this.cadenaArticuloCapturado = "";
        this.idGranelArticuloCapturado = 0;
        //this.patronArticuloGranel = Pattern.compile("^.(.).[0-9].*.[0-9]$");
        this.patronArticuloGranel = Pattern.compile("^[0-9]*.(.).[0-9]*.*$");
        this.matcherPatronArticuloGranel = null;
        this.patronMuchosArticulos = Pattern.compile("^[0-9].*.[0-9]$");
        this.matcherMuchosArticulos = null;
        /////dev por transaccion
        this.seEncontroArticuloTablaTicket = false;
        this.indexSeleccionadoTablaTicket = 0;
        ////consultar transaccion
        this.existeComunicacion = false;
        ////mensajes
        this.mensajeVentana = null;
        
    }
    ////generales

    public void setCasillaCancelacionSeleccionada() {
        estaTicketCancelacionSeleccionado = true;
    }

    public void setCasillaCancelacionNoSeleccionada() {
        estaTicketCancelacionSeleccionado = false;
    }

    /*
     * public int getPantallaId() { return idPantalla; }
     */
    public void ejecutarAutorizador() {

        SION.log(Modulo.VENTA, "Abriendo autorizador", Level.INFO);

        final ControladorValidacion controladorValidacion = new ControladorValidacion();

//        SION.log(Modulo.VENTA, "datos a enviar al autorizador:", Level.INFO);
//        SION.log(Modulo.VENTA, "Usuario:" + String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()), Level.INFO);
//        SION.log(Modulo.VENTA, "Sistema:" + Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "devolucion.sistema"))), Level.INFO);
//        SION.log(Modulo.VENTA, "Módulo:" + Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "devolucion.modulo"))), Level.INFO);
//        SION.log(Modulo.VENTA, "Submódulo:" + Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "devolucion.submodulo"))), Level.INFO);
//        SION.log(Modulo.VENTA, "Opción:" + Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "devolucion.opcion"))), Level.INFO);

        controladorValidacion.autorizarEmpleadoAccion(String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()),
                Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "devolucion.sistema"))),
                Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "devolucion.modulo"))),
                Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "devolucion.submodulo"))),
                Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "devolucion.opcion"))));

        controladorValidacion.getValidacionFinalizada().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observado, Boolean valorViejo, Boolean valorNuevo) {
                if (valorNuevo == true) {

                    esAutorizadorConsultado = true;

                    RespuestaValidacion respuesta = controladorValidacion.getRespuestaValidacion();
                    if ((respuesta != RespuestaValidacion.NO_AUTORIZADO) ||
					 (respuesta == RespuestaValidacion.AUTENTICADO)) 
					 {
                        
                        String[] msjAutorizador = null;
                        msjAutorizador = String.valueOf(controladorValidacion.getUsuariosAutorizados().get(0)).substring(0).split("-");
                        String autorizador = msjAutorizador[0];

                        aplicarDevolucion(Long.parseLong(autorizador));
                        vista.regresarFoco();

                    }
                    if ((respuesta != RespuestaValidacion.ERROR_PARAMETRIZACION_SIN_AUTORIZADORES) || (
                    
                      (respuesta != RespuestaValidacion.ERROR_CRITICO) ||
                    
                     (respuesta == RespuestaValidacion.VALIDACION_ABORTADA)))
					{
                        vista.setAutorizadorConsumido(true);
                        vista.regresarFoco();
                    }

                }
            }
        });
    }

    public void operacionesbuscador(final BorderPane panel, final StringProperty propiedadCadena,
             final TextField campoBusqueda, TextField campoProductosDevolucion, TextField campoImporteDevolver) {

        vistaModelo.llenarPeticionArticuloLocal(1, propiedadCadena.getValue());

        if (vistaModelo.consultarArticuloLocal() != null) {
            articuloLocal = vistaModelo.getArticuloLocal();
            if (articuloLocal.getPaCdgBarrasPadre() > 0) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA,
                        "venta.envase.invalido").replaceAll("%i", articuloLocal.getPanombrearticulo()), TipoMensaje.ADVERTENCIA, vista.getTablaDevolucionPorPoliticas());
                    }
                });
                
            } else {
                agregarArticuloaTabla(campoBusqueda);
            }
        }
    }

    private void agregarArticuloaTabla(final TextField campoBusqueda) {

        if (vistaModelo.consultarArticuloLocal() != null)
		{
            articuloLocal = vistaModelo.getArticuloLocal();

            if (articuloLocal.getPacdgerror() == 0 && articuloLocal.getPacdgbarras() != null
                    && !"".equals(articuloLocal.getPacdgbarras()) && articuloLocal.getPaprecio() != 0) {

                vistaModelo.consultaParametrosOperacion(VistaBarraUsuario.getSesion().getPaisId(),
                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.sistema")),
                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.modulo")),
                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.submodulo")),
                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.configuracion")));

                if ((vistaModelo.getImporteDevolucionPoliticas() + articuloLocal.getImporte()) < vistaModelo.consultaValorConfiguracionParametro()) {

                    if (articuloLocal.getPafamiliaid() == 4) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFocoVenta(SION.obtenerMensaje(Modulo.VENTA, "ventas.escaneoServicio"), TipoMensaje.ADVERTENCIA, campoBusqueda);
                            }
                        });
                        

                    } else {

                        if (vistaModelo.getTamanioListaArticulosPoliticas() > 0) {

                            if (articuloLocal.getPacdgbarras().equals(vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getTamanioListaArticulosPoliticas() - 1).getPacdgbarras())
                                    && vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getTamanioListaArticulosPoliticas() - 1).getDevueltos() == 0) {

                                if ((vistaModelo.getImporteDevolucionPoliticas() + vistaModelo.getArticuloImporte(vistaModelo.getTamanioListaArticulosPoliticas() - 1)) < 999999
                                        && (vistaModelo.getArticuloCantidad(vistaModelo.getTamanioListaArticulosPoliticas() - 1)
                                        + (vistaModelo.getArticuloUnidad(vistaModelo.getTamanioListaArticulosPoliticas() - 1) * vistaModelo.getArticuloPrecio(vistaModelo.getTamanioListaArticulosPoliticas() - 1))) < 999) {

                                    vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getTamanioListaArticulosPoliticas() - 1).setCantidad(articuloLocal.getCantidad() + vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getTamanioListaArticulosPoliticas() - 1).getCantidad());
                                    vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getTamanioListaArticulosPoliticas() - 1).setImporte(Double.parseDouble(utilerias.getNumeroFormateado(vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getTamanioListaArticulosPoliticas() - 1).getPaprecio() * vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getTamanioListaArticulosPoliticas() - 1).getCantidad())));

                                    setearMontosDevolucionPoliticas();
                                } else {
                                    Platform.runLater(new Runnable() {

                                        @Override
                                        public void run() {
                                            AdministraVentanas.mostrarAlertaRetornaFocoVenta(SION.obtenerMensaje(Modulo.VENTA, "ventas.montoVenta"), TipoMensaje.INFO, vista.getTablaDevolucionPorPoliticas());
                                        }
                                    });
                                    

                                }
                            } else {

                                if ((vistaModelo.getImporteDevolucionPoliticas() + articuloLocal.getImporte()) < 999999
                                        && articuloLocal.getCantidad() + (articuloLocal.getPaunidad() * articuloLocal.getPaprecio()) < 999) {

                                    vistaModelo.getListaArticulosPoliticas().add(articuloLocal);

                                } else {
                                    Platform.runLater(new Runnable() {

                                        @Override
                                        public void run() {
                                            AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.montoVenta"), TipoMensaje.INFO, vista.getTablaDevolucionPorPoliticas());
                                        }
                                    });
                                    
                                }
                            }
                        } else {
                            vistaModelo.getListaArticulosPoliticas().add(articuloLocal);
                        }

                        vista.setFocusTablaArticulosDevolucionPorPoliticas();

                        if (vistaModelo.getTamanioListaArticulosPoliticas() > 1) {
                            vista.ajustarScrollTablaPoliticas(vistaModelo.getTamanioListaArticulosPoliticas());
                        }

                    }
                } else {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA,
                            "ventas.montoDevolucion"), TipoMensaje.ADVERTENCIA, vista.getTablaDevolucionPorPoliticas());
                        }
                    });
                    
                }
            } else {
                if (!"".equals(articuloLocal.getPacdgbarras())) {
                    if (articuloLocal.getPacdgbarras() == null) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFocoVenta(SION.obtenerMensaje(
                                        Modulo.VENTA, "ventas.errorBDLocal"), TipoMensaje.ERROR, vista.getTablaDevolucionPorPoliticas());
                            }
                        });

                        SION.log(Modulo.VENTA, "Ocurrió un error al intentar hacer una conculta en BD local", Level.INFO);
                    } else {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(
                                        Modulo.VENTA, "ventas.noArticulo").replace("%1", articuloLocal.getPacdgbarras()),
                                        TipoMensaje.ERROR, vista.getTablaDevolucionPorPoliticas());
                            }
                        });

                        SION.log(Modulo.VENTA, "No se encontró el artículo en BD", Level.INFO);
                    }
                }
            }
        } else {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(
                    Modulo.VENTA, "ventas.errorBDLocal"), TipoMensaje.ERROR, vista.getTablaDevolucionPorPoliticas());
                }
            });
            
        }

    }

    public <E> void asignaPropiedadBean(TableColumn columna, E e, final String id) {
        columna.setCellValueFactory(new PropertyValueFactory<ArticuloBean, E>(id));
    }

    public String obtenerFecha() {
        return dateFormat.format(new Date());
    }

    public void insertaDatosDevolucionPoliticas(TableView tabla) {
        tabla.setItems(vistaModelo.getListaArticulosPoliticas());
    }

    public void insertaDatosDevolucionTicket(TableView tabla) {
        tabla.setItems(vistaModelo.getListaArticulosTicket());
    }

    public void insertaDatosDevolucionSeleccionados(TableView tabla) {
        tabla.setItems(vistaModelo.getListaArticulosSeleccionados());
    }

    public void InicializarDevoluciones() {
        SION.log(Modulo.VENTA, "Inicializando variables de devoluciones", Level.INFO);

        SION.log(Modulo.VENTA, "Consultando parámetros de devolución", Level.INFO);

        vistaModelo.consultaParametrosOperacion(VistaBarraUsuario.getSesion().getPaisId(),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.sistema")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.modulo")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.submodulo")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.configuracion")));
        AdministraVentanas.agregarESCEventHandler();

        SION.log(Modulo.VENTA, "Parametro de monto máximo de devolución obtenido en BD: " + vistaModelo.consultaValorConfiguracionParametro(), Level.INFO);

    }

    public <E> void actualizarColumnas(TableColumn columna, E e, final javafx.geometry.Pos posicion, final boolean esDouble) {

        columna.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn arg0) {
                return new TableCell<ArticuloBean, E>() {

                    @Override
                    public void updateItem(E item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!this.isEmpty()) {

                            this.alignmentProperty().set(posicion);

                            if (esDouble) {
                                if (Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(item.toString()))) > 0) {
                                    this.setText(utilerias.getNumeroFormateado(Double.parseDouble(item.toString())));
                                } else {
                                    this.setText("0.00");
                                }
                            } else {
                                this.setText(String.valueOf(item));
                            }

                            if (listaCeldas.contains(this.getIndex())) {
                                this.setTextFill(Color.RED);
                            }


                            try {
                                this.setStyle("-fx-font: 20pt Verdana;");
                            } catch (Exception e) {
                                SION.log(Modulo.VENTA, "Ocurrió un error al intentar aplicar los estilos en las celdas", Level.INFO);
                            }

                        }
                    }
                };
            }
        });

    }

    ////politicas  
    public void llenarColumnaConFormato(TableColumn columna, final javafx.geometry.Pos posicion) {

        columna.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn param) {

                return new TableCell<ArticuloBean, Double>() {

                    @Override
                    public void updateItem(Double item, boolean empty) {

                        //NumberFormat formatoDecimal = new DecimalFormat("#");

                        super.updateItem(item, empty);

                        if (!isEmpty()) {

                            cadenaDouble = String.valueOf(item);
                            indicePunto = cadenaDouble.indexOf(".");
                            cadenaDecimal = cadenaDouble.substring(indicePunto + 1);

                            this.setTextFill(Color.BLACK);
                            this.alignmentProperty().set(posicion);

                            if (Double.parseDouble(cadenaDecimal) > 0) {
                                this.setText(utilerias.getNumeroFormateado(Double.parseDouble(cadenaDouble)));
                            } else {
                                this.setText(cadenaDouble.substring(0, indicePunto));
                            }

                            if (listaCeldas.contains(this.getIndex())) {
                                this.setTextFill(Color.RED);
                            }

                            this.setStyle("-fx-font: 20pt Verdana;");

                        }
                    }
                };
            }
        });

    }

    public void actualizarComponentesPoliticas(final TableView tabla, final TextField campoProductosDevolucion,
            final TextField campoImporteDevolver) {

        vistaModelo.getListaArticulosPoliticas().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable arg0) {

                //scroll
                if (vistaModelo.getTamanioListaArticulosPoliticas() > 0) {

                    try {
                        SION.log(Modulo.VENTA, "Refrescando tabla políticas", Level.OFF);
                        tabla.setItems(vistaModelo.getListaArticulosTemp());
                        tabla.setItems(vistaModelo.getListaArticulosPoliticas());
                    } catch (Exception e) {
                        SION.log(Modulo.VENTA, "Error al actualizar tablas", Level.INFO);
                    }


                    tabla.getSelectionModel().clearSelection();
                    tabla.getSelectionModel().selectLast();

                    if (vistaModelo.getTamanioListaArticulosPoliticas() > 1) {

                        tabla.scrollTo(vistaModelo.getTamanioListaArticulosPoliticas() - 1);

                        //tabla.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
                    }
                    if (vistaModelo.getListaArticulosPoliticas().size() == 1) {
                        AdministraVentanas.removerESCEventHandler();

                    }
                } else {
                    AdministraVentanas.agregarESCEventHandler();

                }
                //etiquetas

                if (vistaModelo.getCantidadArticulosPoliticas() > 0) {

                    campoProductosDevolucion.setText(utilerias.getNumeroFormateado(vistaModelo.getCantidadArticulosPoliticas()));
                } else {

                    campoProductosDevolucion.setText("0");
                }

                if (vistaModelo.getImporteDevolucionPoliticas() > 0) {
                    campoImporteDevolver.setText(utilerias.getNumeroFormateado(vistaModelo.getImporteDevolucionPoliticas()));
                } else {
                    campoImporteDevolver.setText("0.00");
                }

            }
        });

    }

    public int getTamanioListaArticulosPoliticas() {
        return vistaModelo.getTamanioListaArticulosPoliticas();
    }

    public int getTamanioListaArticulosTicket() {
        return vistaModelo.getTamanioListaArticulosTicket();
    }

    public int getTamanioListaArticulosSeleccionadosTicket() {
        return vistaModelo.getTamanioListaArticulosSeleccionados();
    }

    public double getImporteArticulosDevolucionOPorPoliticas() {
        return vistaModelo.getImporteDevolucionPoliticas();
    }

    public double getImporteArticulosDevolucionPorTicket() {
        return vistaModelo.getImporteDevolucionTicket();
    }

    public void marcarDevolucion(long conciliacion) {

        try {//se intenta marcar la devolcuion

            SION.log(Modulo.VENTA, "Comienza proceso de devolución", Level.INFO);
            vistaModelo.registrarDevolucion();

            if (vistaModelo.getErrorDevolucion() == 0) {//devolucion satisfactoria

                SION.log(Modulo.VENTA, "El proceso de devolución ha sido satisfactorio, "
                        + "se procede a actualizar conciliación en BD local y posteriormente imprimir ticket", Level.INFO);

                vistaModelo.actualizarTransacciónLocal(conciliacion, 1, 2, 2, ACTUALIZACION_LOCAL);

                transaccion = vistaModelo.getTransaccionDevolucion();

                try {
                    if (vista.esDevolucionPorTicket()) {
                        vistaModelo.imprimirTicket(String.valueOf(transaccion).trim(), false);
                    } else {///devolucion por politicas neto
                        vistaModelo.imprimirTicket(String.valueOf(transaccion).trim(), true);
                    }
                } catch (Exception e) {

                    SION.log(Modulo.VENTA, "Ocurrió un problema al imprimir el ticket de devolución : " + e.getLocalizedMessage(), Level.SEVERE);

                }

                if (!vistaModelo.seImprimioTicket()) {
                    if (vista.esDevolucionPorTicket()) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco(
                                SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresoraDev").replace(
                                "%1", String.valueOf(vistaModelo.getTransaccionDevolucion()).concat("I")),
                                TipoMensaje.INFO, vista.getTablaDevolucionPorTicket());
                            }
                        });
                        
                    } else {///devolucion por politicas neto
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco(
                                SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresoraDev").replace(
                                "%1", String.valueOf(vistaModelo.getTransaccionDevolucion()).concat("I")),
                                TipoMensaje.INFO, vista.getTablaDevolucionPorPoliticas());
                            }
                        });
                        
                    }
                }

                vistaModelo.agregarObjetoSesion();

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        vista.setCadenaCampoTicket("");
                    }
                });

                esDevolucionExitosa = true;
                ///
                //int tipoTicket = 0;

            } else {//error en devolucion

                SION.log(Modulo.VENTA, vistaModelo.getDescErrorDevolucion() + " "
                        + vistaModelo.getErrorDevolucion(), Level.INFO);
                //SION.log(Modulo.VENTA, devolucionesVistaModelo.getDescErrorDevolucion(), Level.INFO);

                vistaModelo.cancelarConciliacionDevolucion(conciliacion);

                if (vistaModelo.getDescErrorDevolucion().contains("|")) {
                    MsjDevolucion = vistaModelo.getDescErrorDevolucion().substring(1).split("\\|");

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            AdministraVentanas.mostrarAlertaRetornaFocoVenta(MsjDevolucion[0], TipoMensaje.ERROR,
                            vista.getTablaDevolucionPorPoliticas());
                        }
                    });
                    
                } else {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            AdministraVentanas.mostrarAlertaRetornaFocoVenta(vistaModelo.getDescErrorDevolucion(),
                            TipoMensaje.ERROR, vista.getTablaDevolucionPorPoliticas());
                        }
                    });
                    
                }



                vistaModelo.limpiarRegistrosPago();

            }
        } catch (SionException e) {
            SION.log(Modulo.VENTA, "Ocurrió un error al intentar registrar la petición de la devolución: " + e.getLocalizedMessage(), Level.SEVERE);


            SION.log(Modulo.VENTA, "Se detectó error de comunicación: " + e.getLocalizedMessage() + " " + e.getCodigoAccion(), Level.SEVERE);
            hayComunicacion = false;

        }

    }

    public void marcarDevolucionFueraLinea(int sistema, int modulo, int submodulo, long conciliacion, 
            boolean _esDevolucionTicket, long _usuarioAutoriza) {

        SION.log(Modulo.VENTA, "Entrando a método de intentos de marcación de devolución fuera de línea", Level.INFO);
        ///
        intentosMarcacionFueraDeLinea = 0;

        do {

            SION.log(Modulo.VENTA, "Intento: " + (int) intentosMarcacionFueraDeLinea + 1, Level.INFO);

            esDevolucionFueraLineaExitosa = vistaModelo.marcarDevolucionFueraDeLinea(
                    montoDevolucion, sistema, modulo, submodulo, conciliacion, 
                    estaTicketCancelacionSeleccionado, _esDevolucionTicket, _usuarioAutoriza);

            intentosMarcacionFueraDeLinea++;
        } while (intentosMarcacionFueraDeLinea < 5 && esDevolucionFueraLineaExitosa == false);

        if (esDevolucionFueraLineaExitosa) {
            SION.log(Modulo.VENTA, "la devolución se realizó en " + (int) intentosMarcacionFueraDeLinea + 1 + " intentos", Level.INFO);
        } else {
            SION.log(Modulo.VENTA, "El número de intentos de marcado de devolución se terminó, "
                    + "la devolución fuera de línea no se pudo completar", Level.SEVERE);

            if (existeParametroFueraLinea) {
                SION.log(Modulo.VENTA, "No se generó conciliación, se continúa con el proceso", Level.INFO);
            } else {
                SION.log(Modulo.VENTA, "Se procede a cancelar conciliación generada", Level.INFO);
                vistaModelo.cancelarConciliacionDevolucion(conciliacion);
            }

            if (vista.getPantalla() == PANTALLA_POLITICAS) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFocoVenta(SION.obtenerMensaje(
                        Modulo.VENTA, "ventas.errorBDLocal"), TipoMensaje.ERROR, vista.getTablaDevolucionPorPoliticas());
                    }
                });
                
            } else {////pantalla de devolucion por ticket
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFocoVenta(SION.obtenerMensaje(
                        Modulo.VENTA, "ventas.errorBDLocal"), TipoMensaje.ERROR, vista.getTablaDevolucionPorTicket());
                    }
                });
                
            }
        }

    }

    public void aplicarDevolucion(final long _usuarioAutoriza) {

        SION.log(Modulo.VENTA, "Entrando a método de marcado de devolución", Level.INFO);

        vista.cargarLoading();

        if (vista.esDevolucionPorTicket()) {
            montoDevolucion = vistaModelo.getImporteDevolucionTicket();
        } else {////devolucion por politicas neto
            montoDevolucion = vistaModelo.getImporteDevolucionPoliticas();
        }

        final Task tarea = new Task() {

            @Override
            protected Object call() throws Exception {

                SION.log(Modulo.VENTA, "Entrando a task de marcado de devolución", Level.INFO);

                efectivoDevolucion = 0;
                fechaDevolucion = "";
                montoRecibidoDevolucion = 0;
                cantidadArticulosDevolucion = 0;
                conciliacionDevolucion = 0;
                codigoErrorDevolucionFueraLinea = 0;
                seMarcoVentaFueraLinea = false;
                esDevolucionFueraLineaExitosa = false;
                existeParametroFueraLinea = false;

                vistaModelo.limpiarListasDto();

                if (vista.esDevolucionPorTicket()) {
                    vistaModelo.insertarRegistroFormaPago(new DatosFormasPagoBean(1, Double.parseDouble(utilerias.getNumeroFormateado(
                            vistaModelo.getImporteDevolucionTicket())), 0, "", 0, 0, 0));
                } else {////devolucion por politicas neto
                    vistaModelo.insertarRegistroFormaPago(new DatosFormasPagoBean(1, Double.parseDouble(utilerias.getNumeroFormateado(
                            vistaModelo.getImporteDevolucionPoliticas())), 0, "", 0, 0, 0));
                }

                vistaModelo.agregarTipoPago();

                if (!VistaBarraUsuario.getSesion().getIpEstacion().isEmpty()
                        && VistaBarraUsuario.getSesion().getIpEstacion() != null) {
                    
                    ////fuera de linea directo
                    vistaModelo.consultaParametrosOperacion(VistaBarraUsuario.getSesion().getPaisId(),
                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.sistema")),
                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.modulo")),
                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.submodulo")),
                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.configuracion")));
                    
                    if(vistaModelo.consultaErrorParametro()==0){
                        if(vistaModelo.consultaValorConfiguracionParametro()==1){
                            existeParametroFueraLinea = true;
                            SION.log(Modulo.VENTA, "se ha encontrado parametro de devoluciones fuera de linea activo", Level.INFO);
                        }
                    }
                    //
                    

                    SION.log(Modulo.VENTA, "Es devolución ticket? " + vista.esDevolucionPorTicket(), Level.INFO);

                    SION.log(Modulo.VENTA, "Se procede a verificar si hay conexión intentando con dirección wsdl:"
                            + String.valueOf(SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice")), Level.INFO);
                    hayComunicacion = neto.sion.tienda.genericos.utilidades.Utilerias.evaluarConexionCentral(SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice"), 3);
                    SION.log(Modulo.VENTA, "Hay comunicación? " + hayComunicacion, Level.INFO);

                    //ppoliticas 1 2 2
                    if (hayComunicacion && !existeParametroFueraLinea) {

                        if (vista.esDevolucionPorTicket()) {
                            vistaModelo.insertarArticulosDevolucionTicket();
                            vistaModelo.insertaPeticionConciliacion(
                                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacionDevTicket.1")),
                                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacionDevTicket.2")),
                                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacionDevTicket.3")));
                        } else {/////devolucion por politicas neto
                            vistaModelo.insertarArticulosDevolucion();
                            vistaModelo.insertaPeticionConciliacion(
                                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacionDevPoliticas.1")),
                                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacionDevPoliticas.2")),
                                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacionDevPoliticas.3")));
                        }

                        conciliacionDevolucion = vistaModelo.obtenerRespuestaConciliacion();

                        if (vista.esDevolucionPorTicket()) {
                            vistaModelo.insertapeticionDevolucionDto(
                                    montoDevolucion, obtenerFecha(), 2, transaccion,
                                    vistaModelo.getRespuestaDetalleVentaDto().getPaMovimientoId(), 
                                    conciliacionDevolucion, estaTicketCancelacionSeleccionado, _usuarioAutoriza);
                        } else {//////devolucion por politicas neto
                            vistaModelo.insertapeticionDevolucionDto(
                                    montoDevolucion, obtenerFecha(), 1, 0, 0, conciliacionDevolucion, 
                                    estaTicketCancelacionSeleccionado, _usuarioAutoriza);
                        }

                        marcarDevolucion(conciliacionDevolucion);

                    } else {//fuera de linea directo

                        if (vista.esDevolucionPorTicket()) {
                            vistaModelo.insertarArticulosDevolucionTicket();
                        } else {////devolucion x politicas neto
                            vistaModelo.insertarArticulosDevolucion();
                        }

                        existeParametroFueraLinea = true;

                        marcarDevolucionFueraLinea(
                                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacionDevFueraLinea.1")),
                                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacionDevFueraLinea.2")),
                                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacionDevFueraLinea.3")),
                                conciliacionDevolucion, vista.esDevolucionPorTicket(), _usuarioAutoriza);

                        seMarcoVentaFueraLinea = true;

                    }

                    if (!hayComunicacion && !seMarcoVentaFueraLinea) {///entra aqui si truena x timed out

                        SION.log(Modulo.VENTA, "Se procede a marcar fuera de línea al no encontrar comunicación con central", Level.INFO);
                        marcarDevolucionFueraLinea(1, 2, 5, conciliacionDevolucion, vista.esDevolucionPorTicket(), _usuarioAutoriza);

                    }

                    if (!hayComunicacion) {
                        SION.log(Modulo.VENTA, "Comprobando estatus de la respuesta a la petición de devolución fuera de línea, "
                                + " Devolucion fuera de línea exitosa? " + esDevolucionFueraLineaExitosa, Level.INFO);
                    }

                    if (esDevolucionFueraLineaExitosa) {

                        try {
                            if (vista.esDevolucionPorTicket()) {
                                vistaModelo.imprimirTicket(String.valueOf(
                                        vistaModelo.getConciliacionIdFueraLinea()).trim(), false);
                            } else {
                                vistaModelo.imprimirTicket(String.valueOf(
                                        vistaModelo.getConciliacionIdFueraLinea()).trim(), true);
                            }

                            limpiarDevolucion();
                        } catch (Exception e) {

                            SION.log(Modulo.VENTA, "Ocurrió un error al intentar imprimir el ticket de devolución fuera de línea conciliación: "
                                    + String.valueOf(vistaModelo.getConciliacionIdFueraLinea()).concat("O") + " : " + e.toString(), Level.SEVERE);

                        }

                    }

                    SION.log(Modulo.VENTA, "Ticket de venta completado? " + vistaModelo.seImprimioTicket(), Level.INFO);
                    if (!vistaModelo.seImprimioTicket() && esDevolucionFueraLineaExitosa) {

                        if (vista.getPantalla() == PANTALLA_POLITICAS) {
                            SION.log(Modulo.VENTA, "Mostrando alerta de fallo en impresión en el ticket de devolución fuera de línea", Level.INFO);
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresoraDev").replace(
                                    "%1", String.valueOf(vistaModelo.getConciliacionIdFueraLinea()).concat("O")), TipoMensaje.INFO, vista.getTablaDevolucionPorPoliticas());
                                }
                            });
                            
                        } else {////pantalla de devolucion por ticket
                            SION.log(Modulo.VENTA, "Mostrando alerta de fallo en impresión en el ticket de devolución fuera de línea", Level.INFO);
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresoraDev").replace(
                                    "%1", String.valueOf(vistaModelo.getConciliacionIdFueraLinea()).concat("O")), TipoMensaje.INFO, vista.getTablaDevolucionPorPoliticas());
                                }
                            });
                            
                        }

                        limpiarDevolucion();
                    }


                } else {
                    SION.log(Modulo.VENTA, "Ocurrió un error al intentar validar la IP del equipo", Level.INFO);

                    if (vista.getPantalla() == PANTALLA_POLITICAS) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.ip"),
                                TipoMensaje.ERROR, vista.getTablaDevolucionPorPoliticas());
                            }
                        });
                        
                    } else {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.ip"),
                                TipoMensaje.ERROR, vista.getTablaDevolucionPorTicket());
                            }
                        });
                        
                    }
                }

                return "ok";
            }
        };

        final Service servicio = new Service() {

            @Override
            protected Task createTask() {
                return tarea;
            }
        };

        servicio.stateProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {

                if (arg2.equals(Worker.State.SUCCEEDED)) {
                    SION.log(Modulo.VENTA, "Tarea de marcado de devoluciòn concluyó sin errores. Devolución exitosa? " + esDevolucionExitosa, Level.INFO);

                    vista.removerLoading();

                    if (esDevolucionExitosa) {

                        limpiarDevolucion();

                        esDevolucionExitosa = false;
                        if (vista.esDevolucionPorTicket()) {

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    vista.setFocusTablaArticulosDevolucionPorTicket();
                                }
                            });
                        } else {

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    vista.setFocusTablaArticulosDevolucionPorPoliticas();
                                }
                            });
                        }

                    }



                } else if (arg2.equals(Worker.State.FAILED)) {

                    SION.log(Modulo.VENTA, "Tarea de marcado de devolución terminó con una excepción: " + servicio.getException(), Level.SEVERE);

                    vista.removerLoading();

                    if (esDevolucionExitosa) {

                        limpiarDevolucion();
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                vista.setCadenaCampoTicket("");
                            }
                        });

                        esDevolucionExitosa = false;
                    }

                    if (vista.esDevolucionPorTicket()) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                vista.setFocusCampoTicket();
                            }
                        });
                    } else {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                vista.setFocusTablaArticulosDevolucionPorPoliticas();
                            }
                        });
                    }
                }
            }
        });
        servicio.start();


    }

    public void capturarArticulo() {
        cantidadArticulosCapturados = 1;
        cadenaArticuloCapturado = "";
        idGranelArticuloCapturado = 0;
        
        SION.log(Modulo.VENTA, "Cadena ingresada por usuario: "+vista.getCadenaArticulo(), Level.INFO);

        if (vista.getCadenaArticulo().contains("*")) {
            
            if (vista.getCadenaArticulo().contains(".")) {
                
                matcherPatronArticuloGranel = patronArticuloGranel.matcher(vista.getCadenaArticulo());

                if (matcherPatronArticuloGranel.find()) {
                    
                    int pos = vista.getCadenaArticulo().indexOf("*");
                    int i = 0;
                    while (i < pos) {
                        cadenaArticuloCapturado += vista.getCadenaArticulo().charAt(i);
                        i++;
                    }
                    try {
                        cantidadArticulosCapturados = Double.parseDouble(cadenaArticuloCapturado);
                    } catch (NumberFormatException e) {
                        AdministraVentanas.mostrarAlerta(e.toString(), TipoMensaje.ERROR);
                    }
                    vista.setCadenaArticulo(vista.getCadenaArticulo().substring(pos + 1));
                    //cadena = cadena.substring(pos + 1);
                    idGranelArticuloCapturado = 1;
                }
            } else {
                matcherMuchosArticulos = patronMuchosArticulos.matcher(vista.getCadenaArticulo());
                if (matcherMuchosArticulos.find()) {

                    int pos = vista.getCadenaArticulo().indexOf("*");

                    int i = 0;
                    while (i < pos) {
                        cadenaArticuloCapturado += vista.getCadenaArticulo().charAt(i);
                        i++;
                    }
                    try {
                        cantidadArticulosCapturados = Double.parseDouble(cadenaArticuloCapturado);
                    } catch (NumberFormatException e) {
                        AdministraVentanas.mostrarAlerta(e.toString(), TipoMensaje.ERROR);

                    }
                    vista.setCadenaArticulo(vista.getCadenaArticulo().substring(pos + 1));
                    //cadena = cadena.substring(pos + 1);
                } else {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            AdministraVentanas.mostrarAlertaRetornaFoco("Código no válido", TipoMensaje.ERROR,
                            vista.getTablaDevolucionPorPoliticas());
                        }
                    });
                    
                }
            }
        }

        vistaModelo.llenarPeticionArticuloLocal(cantidadArticulosCapturados, vista.getCadenaArticulo().trim());
        if (vistaModelo.consultarArticuloLocal() != null) {

            articuloLocal = vistaModelo.getArticuloLocal();

            if (articuloLocal.getPacdgerror() == 0 && !"".equals(articuloLocal.getPacdgbarras())
                    && !"".equals(articuloLocal.getPacdgbarras()) && articuloLocal.getPaprecio() != 0) {

                vistaModelo.consultaParametrosOperacion(VistaBarraUsuario.getSesion().getPaisId(),
                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.sistema")),
                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.modulo")),
                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.submodulo")),
                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.configuracion")));

                //SION.log(Modulo.VENTA, "Monto máximo de la devolución obtenido: "+devolucionesVistaModelo.consultaValorConfiguracionParametro(), Level.INFO);
                if ((vistaModelo.getImporteDevolucionPoliticas() + articuloLocal.getImporte())
                        < vistaModelo.consultaValorConfiguracionParametro()) {

                    if (articuloLocal.getPafamiliaid() == 4 && ((vistaModelo.getImporteDevolucionPoliticas())
                            + articuloLocal.getImporte()) <= vistaModelo.consultaValorConfiguracionParametro()) {
                        if (articuloLocal.getPafamiliaid() == 4) {
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    AdministraVentanas.mostrarAlerta("El código de barras " + articuloLocal.getPacdgbarras()
                                    + " es un servicio, \"" + articuloLocal.getPanombrearticulo() + "\"", TipoMensaje.ADVERTENCIA);
                                }
                            });
                            
                        }

                        if (((vistaModelo.getImporteDevolucionPoliticas())
                                + articuloLocal.getImporte()) <= vistaModelo.consultaValorConfiguracionParametro()) {
                        }

                    } else {
                        if (idGranelArticuloCapturado == 1) {
                            if (articuloLocal.getPagranel() != 0) {
                                if (vistaModelo.getListaArticulosPoliticas().size() > 0) {

                                    if (articuloLocal.getPacdgbarras().equals(vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).getPacdgbarras())
                                            && vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).getDevueltos() == 0) {
                                        vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).setCantidad(
                                                articuloLocal.getCantidad() + vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).getCantidad());
                                        vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).setImporte(
                                                vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).getPaprecio() * vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).getCantidad());


                                        setearMontosDevolucionPoliticas();
                                    } else {
                                        vistaModelo.getListaArticulosPoliticas().add(articuloLocal);
                                    }
                                } else {

                                    vistaModelo.getListaArticulosPoliticas().add(articuloLocal);
                                }
                            } else {
                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlerta("El artículo " + articuloLocal.getPanombrearticulo() + " no es un articulo que se venda a granel", TipoMensaje.ADVERTENCIA);
                                    }
                                });
                                
                            }
                        } else {
                            if (vistaModelo.getListaArticulosPoliticas().size() > 0) {

                                if (articuloLocal.getPacdgbarras().equals(vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).getPacdgbarras())
                                        && vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).getDevueltos() == 0) {
                                    vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).setCantidad(articuloLocal.getCantidad() + vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).getCantidad());
                                    vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).setImporte(vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).getPaprecio() * vistaModelo.getListaArticulosPoliticas().get(vistaModelo.getListaArticulosPoliticas().size() - 1).getCantidad());

                                    setearMontosDevolucionPoliticas();
                                } else {
                                    vistaModelo.getListaArticulosPoliticas().add(articuloLocal);


                                }
                            } else {
                                vistaModelo.getListaArticulosPoliticas().add(articuloLocal);

                            }
                        }
                    }
                } else {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA,
                            "ventas.montoDevolucion"), TipoMensaje.ADVERTENCIA, vista.getTablaDevolucionPorPoliticas());
                        }
                    });
                    
                }
            } else {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(
                        Modulo.VENTA, "ventas.noArticulo").replace("%1", vista.getCadenaArticulo()),
                        TipoMensaje.INFO, vista.getTablaDevolucionPorPoliticas());
                    }
                });
                
            }

            vista.setCadenaArticulo("");
            //cadena = "";


        } else {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(
                    Modulo.VENTA, "ventas.errorBDLocal"), TipoMensaje.ERROR, vista.getTablaDevolucionPorPoliticas());
                }
            });
            
        }


    }

    public void sumarArticuloTablaDevolucionPorPoliticas(int index) {
        if (index > 0) {
            //index = tabla.getSelectionModel().getSelectedIndex();
        } else {
            index = 0;
        }

        vistaModelo.consultaParametrosOperacion(VistaBarraUsuario.getSesion().getPaisId(),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.sistema")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.modulo")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.submodulo")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteDevolucion.configuracion")));

        SION.log(Modulo.VENTA, "Monto máximo de la devolución obtenido: " + vistaModelo.consultaValorConfiguracionParametro(), Level.INFO);

        if ((vistaModelo.getImporteDevolucionPoliticas()
                + vistaModelo.getListaArticulosPoliticas().get(index).getPaprecio())
                <= vistaModelo.consultaValorConfiguracionParametro()) {
            if (index >= 0) {//valida indice 

                if ((vistaModelo.getImporteDevolucionPoliticas() + vistaModelo.getArticuloPrecio(index)) <= 999999
                        && //(vistamodelo.getArticuloCantidad(index)+
                        (vistaModelo.getArticuloUnidad(index) + vistaModelo.getArticuloCantidad(index)) <= 999) {

                    insertarRegistroArticulo(index, 1);
                    setearMontosDevolucionPoliticas();
                } else {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            AdministraVentanas.mostrarAlertaRetornaFocoVenta(SION.obtenerMensaje(Modulo.VENTA,
                            "ventas.excedeCantidadArticulos"), TipoMensaje.INFO, vista.getTablaDevolucionPorPoliticas());
                        }
                    });
                    

                }
            }
        } else {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA,
                    "ventas.montoDevolucion"), TipoMensaje.ADVERTENCIA, vista.getTablaDevolucionPorPoliticas());
                }
            });
            
        }
    }

    public void eventosTablaPoliticas(final TableView tabla, final TextField campoCantidadProductos, final TextField campoImporte) {
    }

    public void setearMontosDevolucionPoliticas() {

        if (vistaModelo.getImporteDevolucionPoliticas() > 0) {
            vista.setImporteArticulosDevolucionPorPoliticas(
                    utilerias.getNumeroFormateado(vistaModelo.getImporteDevolucionPoliticas()));
        } else {
            vista.setImporteArticulosDevolucionPorPoliticas("");
        }

        if (vistaModelo.getCantidadArticulosPoliticas() > 0) {
            vista.setCantidadArticulosDevolucionPorPoliticas(
                    utilerias.getNumeroFormateado(vistaModelo.getCantidadArticulosPoliticas()));
        } else {
            vista.setCantidadArticulosDevolucionPorPoliticas("");
        }
    }

    public void revisarValoresNegativos(final TableView tabla, final TableColumn columna) {

        columna.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn arg0) {
                return new TableCell<ArticuloBean, Double>() {

                    @Override
                    public void updateItem(Double item, boolean empty) {
                        super.updateItem(item, empty);

                        if (!this.isEmpty()) {
                            if (item <= 0.0) {

                                listaCeldas.add(this.getIndex());

                                this.setTextFill(Color.RED);

                            } else {
                                this.setTextFill(Color.BLACK);
                            }

                            this.alignmentProperty().set(Pos.CENTER_RIGHT);
                            this.setText(utilerias.getNumeroFormateado(item));
                            this.setStyle("-fx-font: 20pt Verdana;");

                        }
                    }
                };
            }
        });

    }

    public void insertarRegistroArticulo(int indice, int id) {

        if (indice == -1) {
            indice = vistaModelo.getListaArticulosPoliticas().size() - 1;
        }

        vistaModelo.modificarListaArticulos(indice, id);

    }

    public void limpiarDevolucion() {

        SION.log(Modulo.VENTA, "Limpiando variables", Level.INFO);

        listaCeldas.clear();

        vista.getCampoTicket().clear();
        vista.removerCasillaSeleccionada();

        vistaModelo.getListaArticulosTicket().clear();
        vistaModelo.getListaArticulosPoliticas().clear();
        vistaModelo.getListaArticulosSeleccionados().clear();
        vistaModelo.getListaFormasPago().clear();
        vistaModelo.limpiarListasDto();

        validadorTicket.limpiarValidador();

        SION.log(Modulo.VENTA, "Variables inicializadas", Level.INFO);
    }

    //////tickets
    //public void validarCampoTicket(TextField campoTicket) {
    //validador.establecerFormato(18);
    //validador.formatearCampoTexto(campoTicket);
    //validador.validadorCampoTarjetas(campoTicket);
    //}
    public void actualizarComponentesTicket(final TableView tablaSeleccionados, final TextField campoProductosDevolucion,
            final TextField campoImporteDevolver, final TableView tabla) {

        vistaModelo.getListaArticulosTicket().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable arg0) {
                if (vista.getPantalla() == PANTALLA_TICKET) {
                    if (vistaModelo.getListaArticulosTicket().size() > 0) {
                        if (vistaModelo.getListaArticulosTicket().size() == 1) {
                            AdministraVentanas.removerESCEventHandler();
                        }
                    } else {
                        AdministraVentanas.agregarESCEventHandler();
                    }

                    if (vistaModelo.getListaArticulosTicket().size() > 1) {
                        SION.log(Modulo.VENTA, "Recorriendo selección tabla políticas: "
                                + tabla.getItems().size(), Level.INFO);

                        tabla.getSelectionModel().clearSelection();
                        tabla.getSelectionModel().selectLast();
                        if (vistaModelo.getListaArticulosTicket().size() > 13) {
                            tabla.scrollTo(vistaModelo.getTamanioListaArticulosTicket() - 1);
                        }
                    }
                }
            }
        });

        vistaModelo.getListaArticulosSeleccionados().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable arg0) {

                //scroll
                if (vistaModelo.getTamanioListaArticulosSeleccionados() > 1) {
                    tablaSeleccionados.getSelectionModel().clearSelection();
                    tablaSeleccionados.getSelectionModel().selectLast();
                    tablaSeleccionados.scrollTo(vistaModelo.getTamanioListaArticulosSeleccionados() - 1);
                }
                //etiquetas

                if (vistaModelo.getCantidadArticulosTicket() > 0) {
                    campoProductosDevolucion.setText(utilerias.getNumeroFormateado(vistaModelo.getCantidadArticulosTicket()));
                } else {
                    campoProductosDevolucion.setText("");
                }

                if (vistaModelo.getImporteDevolucionTicket() > 0) {
                    campoImporteDevolver.setText(utilerias.getNumeroFormateado(vistaModelo.getImporteDevolucionTicket()));
                } else {
                    campoImporteDevolver.setText("");
                }
            }
        });

    }

    public void actualizarMontos() {

        if (vistaModelo.getImporteDevolucionTicket() > 0) {
            vista.setImporteArticulosDevolucionPorTicket(
                    utilerias.getNumeroFormateado(vistaModelo.getImporteDevolucionTicket()));
        } else {
            vista.setImporteArticulosDevolucionPorTicket("0.00");
        }

        if (vistaModelo.getCantidadArticulosTicket() > 0) {
            vista.setCantidadArticulosDevolucionPorTicket(
                    String.valueOf(vistaModelo.getCantidadArticulosTicket()));
        } else {
            vista.setCantidadArticulosDevolucionPorTicket("0");
        }

    }

    public void seleccionarArticulo(int index) {//este metodo se usa para pasar articulos de la tabla de tickets a seleccionados

        montoMaximoDevolucion = vistaModelo.consultaValorConfiguracionParametro();
        if(montoMaximoDevolucion<10){
            montoMaximoDevolucion=500;
            SION.log(Modulo.VENTA, "Ocurrió un error al establecer monto máximo de devolución, asigando default:"
                    + " "+ montoMaximoDevolucion, Level.INFO);
        }
        
        seEncontroArticuloTablaTicket = false;
        indexSeleccionadoTablaTicket = 0;

        if (index >= 0) {///valida el indice

            if (vistaModelo.getListaArticulosTicket().get(index).getCantidad() > 0) {
                
                if ((vistaModelo.getImporteDevolucionTicket()
                        + vistaModelo.getListaArticulosTicket().get(index).getPaprecio())
                        + vistaModelo.getRespuestaDetalleVentaDto().getPaMontoDev()
                        <= montoMaximoDevolucion) {

                    if (vistaModelo.getListaArticulosSeleccionados().size() > 0) {

                        for (int i = 0; i < vistaModelo.getListaArticulosSeleccionados().size(); i++) {

                            if (vistaModelo.getListaArticulosTicket().get(index).getPacdgbarras().equals(
                                    vistaModelo.getListaArticulosSeleccionados().get(i).getPacdgbarras())) {

                                seEncontroArticuloTablaTicket = true;
                                indexSeleccionadoTablaTicket = i;

                                if (vistaModelo.getListaArticulosTicket().get(index).getCantidad() > 0) {

                                    vistaModelo.getListaArticulosSeleccionados().get(i).setCantidad(
                                            vistaModelo.getListaArticulosSeleccionados().get(i).getCantidad() + 1);
                                    vistaModelo.getListaArticulosSeleccionados().get(i).setImporte(
                                            vistaModelo.getListaArticulosSeleccionados().get(i).getPaprecio()
                                            * vistaModelo.getListaArticulosSeleccionados().get(i).getCantidad());

                                    vistaModelo.getListaArticulosTicket().get(index).setCantidad(
                                            vistaModelo.getListaArticulosTicket().get(index).getCantidad() - 1);
                                    vistaModelo.getListaArticulosTicket().get(index).setImporte(
                                            vistaModelo.getListaArticulosTicket().get(index).getPaprecio()
                                            * vistaModelo.getListaArticulosTicket().get(index).getCantidad());
                                    vistaModelo.getListaArticulosTicket().get(index).setDevueltos(
                                            vistaModelo.getListaArticulosTicket().get(index).getDevueltos() + 1);

                                    actualizarMontos();
                                }
                            }
                        }

                        if (!seEncontroArticuloTablaTicket) {

                            vistaModelo.insertarArticuloSeleccionado(index, vistaModelo.getListaArticulosSeleccionados().size());
                            actualizarMontos();
                        }
                    } else {//no hay registros anteriores solo inserta

                        vistaModelo.insertarArticuloSeleccionado(index, 0);
                        actualizarMontos();
                    }
                } else {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            AdministraVentanas.mostrarAlertaRetornaFocoVenta(SION.obtenerMensaje(Modulo.VENTA,
                            "ventas.montoDevolucion"), TipoMensaje.ADVERTENCIA, vista.getTablaDevolucionPorTicket());
                        }
                    });
                    
                }
            } else {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco("El artículo seleccionado se ha devuelto en su totalidad.", TipoMensaje.INFO, vista.getTablaDevolucionPorTicket());
                    }
                });
                
            }
        }

    }

    public void suprimirLinea(int index) {
        if (vistaModelo.getListaArticulosSeleccionados().size() > 0) {
            for (int i = 0; i < vistaModelo.getListaArticulosTicket().size(); i++) {
                if (vistaModelo.getListaArticulosTicket().get(i).getPacdgbarras().equals(
                        vistaModelo.getListaArticulosSeleccionados().get(index).getPacdgbarras())) {

                    vistaModelo.getListaArticulosTicket().get(i).setDevueltos(
                            vistaModelo.getListaArticulosTicket().get(i).getDevueltos()
                            - vistaModelo.getListaArticulosSeleccionados().get(index).getCantidad());

                    vistaModelo.getListaArticulosTicket().get(i).setCantidad(
                            vistaModelo.getListaArticulosTicket().get(i).getCantidad()
                            + vistaModelo.getListaArticulosSeleccionados().get(index).getCantidad());

                    vistaModelo.getListaArticulosTicket().get(i).setImporte(
                            vistaModelo.getListaArticulosTicket().get(i).getCantidad()
                            * vistaModelo.getListaArticulosTicket().get(i).getPaprecio());

                    vistaModelo.getListaArticulosSeleccionados().remove(index);

                    actualizarMontos();
                    break;

                }
            }
        }
    }

    public void restarArticulo(int index) {
        if (vistaModelo.getListaArticulosSeleccionados().size() > 0) {
            if (vistaModelo.getListaArticulosSeleccionados().size() > 0) {
                if (vistaModelo.getListaArticulosSeleccionados().get(index).getCantidad() > 0) {
                    for (int i = 0; i < vistaModelo.getListaArticulosTicket().size(); i++) {
                        if (vistaModelo.getListaArticulosTicket().get(i).getPacdgbarras().equals(
                                vistaModelo.getListaArticulosSeleccionados().get(index).getPacdgbarras())) {

                            vistaModelo.getListaArticulosTicket().get(i).setDevueltos(
                                    vistaModelo.getListaArticulosTicket().get(i).getDevueltos() - 1);

                            vistaModelo.getListaArticulosTicket().get(i).setCantidad(
                                    vistaModelo.getListaArticulosTicket().get(i).getCantidad() + 1);

                            vistaModelo.getListaArticulosTicket().get(i).setImporte(
                                    vistaModelo.getListaArticulosTicket().get(i).getCantidad()
                                    * vistaModelo.getListaArticulosTicket().get(i).getPaprecio());

                            vistaModelo.getListaArticulosSeleccionados().get(index).setCantidad(
                                    vistaModelo.getListaArticulosSeleccionados().get(index).getCantidad() - 1);

                            vistaModelo.getListaArticulosSeleccionados().get(index).setImporte(
                                    vistaModelo.getListaArticulosSeleccionados().get(index).getCantidad()
                                    * vistaModelo.getListaArticulosSeleccionados().get(index).getPaprecio());

                            if (vistaModelo.getListaArticulosSeleccionados().get(index).getCantidad() == 0) {
                                vistaModelo.getListaArticulosSeleccionados().remove(index);
                            }

                            actualizarMontos();
                            break;

                        }
                    }
                }
            }
        }
    }

    public int validarIndexTablaArticulosSeleccionadosDevolucionPorTicket(int _index) {

        if (_index > 0 && vistaModelo.getListaArticulosSeleccionados().size() > 0) {
            if (_index >= vistaModelo.getListaArticulosSeleccionados().size()) {
                _index = vistaModelo.getListaArticulosSeleccionados().size() - 1;
            }
        }

        return _index;

    }

    public void iniciarProcesoDevolucion(final String cadenaTransaccion) {

        hayComunicacion = false;
        hayComunicacion = consultarComunicacion();

        if (vistaModelo.getListaArticulosTicket().size() <= 0
                && !cadenaTransaccion.isEmpty() && hayComunicacion) {

            vista.cargarLoading();

            final Task tarea = new Task() {

                @Override
                protected Object call() throws Exception {

                    SION.log(Modulo.VENTA, "Entrando a tarea de busqueda de detalle de venta para devolución por número de ticket", Level.INFO);

                    numeroConciliacion = 0;
                    banderaDevolucion = 0;
                    transaccion = 0;

                    try {
                        transaccion = Long.parseLong(cadenaTransaccion.trim());
                    } catch (NumberFormatException e) {
                        transaccion = 0;
                    }

                    vistaModelo.registrarPeticionDetalleVenta(cadenaTransaccion);

                    banderaDevolucion = vistaModelo.obtenerRespuestaDetalleVenta();

                    if (banderaDevolucion != 0) {
                        SION.log(Modulo.VENTA, "Ocurrió un error al obtener el detalle de la venta", Level.INFO);
                        vista.setFocusCampoTicket();
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFocoVenta(SION.obtenerMensaje(
                                Modulo.VENTA, "ventas.devTicket"), TipoMensaje.ERROR, vista.getCampoTicket());
                            }
                        });
                        
                    } else {

                        if (vistaModelo.getRespuestaDetalleVentaDto().getPaCdgError() != 0) {
                            SION.log(Modulo.VENTA, "Ocurrió un error al intentar consultar el ticket con transacción " + cadenaTransaccion + ", " + vistaModelo.getRespuestaDetalleVentaDto().getPaDescError(), Level.INFO);

                            if (vistaModelo.getRespuestaDetalleVentaDto().getPaDescError().contains("|")) {
                                mensajeVentana = vistaModelo.getRespuestaDetalleVentaDto().getPaDescError().substring(1).split("\\|");
                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFocoVenta(mensajeVentana[0], TipoMensaje.ERROR, vista.getCampoTicket());
                                    }
                                });
                                
                            } else {
                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFocoVenta(
                                        vistaModelo.getRespuestaDetalleVentaDto().getPaDescError(), TipoMensaje.ERROR, vista.getCampoTicket());
                                    }
                                });
                                
                            }

                        } else {
                            vistaModelo.insertarArticuloDetalle();
                            vista.setFocusTablaArticulosDevolucionPorTicket();
                        }

                    }

                    return "ok";

                }
            };

            final Service servicio = new Service() {

                @Override
                protected Task createTask() {
                    return tarea;
                }
            };

            servicio.stateProperty().addListener(new ChangeListener() {

                @Override
                public void changed(ObservableValue arg0, Object arg1, Object arg2) {

                    if (arg2.equals(Worker.State.SUCCEEDED)) {
                        SION.log(Modulo.VENTA, "Tarea de consulta de detalle de ticket de venta concluyó sin errores", Level.INFO);

                        vista.removerLoading();
                        vista.setFocusTablaArticulosDevolucionPorTicket();

                    } else if (arg2.equals(Worker.State.FAILED)) {
                        SION.log(Modulo.VENTA, "Tarea de consulta de detalle de ticket de venta terminó con una excepción: " + servicio.getException(), Level.SEVERE);

                        vista.removerLoading();
                        vista.setFocusTablaArticulosDevolucionPorTicket();

                    }
                }
            });
            servicio.start();

        } else {
            if (!hayComunicacion) {

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        SION.log(Modulo.VENTA, "No se detectó comunicación, la petición del detalle de la venta no se llevó a cabo.", Level.INFO);
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco("El servicio no se encuentra disponible temporalmente, intente más tarde",
                                TipoMensaje.INFO, vista.getTablaDevolucionPorTicket());
                            }
                        });
                        
                        limpiarDevolucion();
                    }
                });
            }

        }
    }

    public boolean consultarComunicacion() {

        existeComunicacion = false;

        SION.log(Modulo.VENTA, "Se procede a verificar si hay conexión intentando con dirección wsdl:"
                + String.valueOf(SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice")), Level.INFO);
        existeComunicacion = neto.sion.tienda.genericos.utilidades.Utilerias.evaluarConexionCentral(SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice"), 3);
        SION.log(Modulo.VENTA, "Hay comunicación? " + hayComunicacion, Level.INFO);

        return existeComunicacion;
    }

    public void consultarParametroOperacion(int _pais, int _sistema, int _modulo, int _submodulo, int _configuracion) {
        vistaModelo.consultaParametrosOperacion(_pais, _sistema, _modulo, _submodulo, _configuracion);
    }

    public int getCodigoErrorParametro() {
        return vistaModelo.consultaErrorParametro();
    }

    public String getDescErrorParametro() {
        return vistaModelo.consultaDescErrorParametro();
    }

    public double getValorConfiguracionParametro() {
        return vistaModelo.consultaValorConfiguracionParametro();
    }
}
