package neto.sion.tienda.venta.constantes;

/**
 *
 * @author dramirezr
 */
public class TiposImpresion {
    public static final int TICKET_NORMAL=0, TICKET_TIEMPO_AIRE=1, TICKET_PAGOS_ELETRONICOS=2;
}
