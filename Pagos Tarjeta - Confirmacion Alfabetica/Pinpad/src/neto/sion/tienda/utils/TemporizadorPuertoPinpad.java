/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.utils;

import java.util.logging.Level;
import neto.sion.genericos.pinpad.RecuperaPinPad;
import neto.sion.tarjeta.chip.utilerias.Temporizador;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;


public class TemporizadorPuertoPinpad extends Temporizador
{
    public TemporizadorPuertoPinpad(int length) {
        super(length);
    }

    @Override
    public void timeout()
    {
        RecuperaPinPad recuperarPinpad = new RecuperaPinPad();
        SION.log(Modulo.VENTA, "Terminando thread...\n", Level.FINE);
        recuperarPinpad.inhabilitaPinpad();
        recuperarPinpad.habilitaPinpad();
        stopThread();
    }

    public void stopThread()
    {
        SION.log(Modulo.VENTA, "Terminando thread...\n", Level.FINE);
        Thread.currentThread().stop();
    }
}
