package neto.sion.tienda.bean;

public class RespuestaLeerTarjetaDescuentoBean {
    private String nombrePropietario;
    private String numeroTarjeta;
    private int tipoLectura;
    private int tipoTarjeta;
    private String track1;
    private String track2;
    private boolean esTarjetaDescuento;
    private String emisorTarjetaDescuento;
    private int idError;
    private String MensajeError;

    public String getEmisorTarjetaDescuento() {
        return emisorTarjetaDescuento;
    }

    public void setEmisorTarjetaDescuento(String emisorTarjetaDescuento) {
        this.emisorTarjetaDescuento = emisorTarjetaDescuento;
    }
    
    public String getMensajeError() {
        return MensajeError;
    }

    public void setMensajeError(String MensajeError) {
        this.MensajeError = MensajeError;
    }

    public int getIdError() {
        return idError;
    }

    public void setIdError(int idError) {
        this.idError = idError;
    }
    
    public boolean isEsTarjetaDescuento() {
        return esTarjetaDescuento;
    }

    public void setEsTarjetaDescuento(boolean esTarjetaDescuento) {
        this.esTarjetaDescuento = esTarjetaDescuento;
    }

    public String getNombrePropietario() {
        return nombrePropietario;
    }

    public void setNombrePropietario(String nombrePropietario) {
        this.nombrePropietario = nombrePropietario;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public int getTipoLectura() {
        return tipoLectura;
    }

    public void setTipoLectura(int tipoLectura) {
        this.tipoLectura = tipoLectura;
    }

    public int getTipoTarjeta() {
        return tipoTarjeta;
    }

    public void setTipoTarjeta(int tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    public String getTrack1() {
        return track1;
    }

    public void setTrack1(String track1) {
        this.track1 = track1;
    }

    public String getTrack2() {
        return track2;
    }

    public void setTrack2(String track2) {
        this.track2 = track2;
    }
    
    
}
