package neto.sion.tienda.bean;

public class RespuestaReversoTarjetaFrontBean{

    private String descError;
    private int codError;

    public RespuestaReversoTarjetaFrontBean() {}

    public int getCodError() {
        return codError;
    }

    public void setCodError(int codError) {
        this.codError = codError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }
}