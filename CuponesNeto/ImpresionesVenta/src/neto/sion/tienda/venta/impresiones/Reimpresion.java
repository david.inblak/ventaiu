package neto.sion.tienda.venta.impresiones;

import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.exception.SionException;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.venta.servicios.cliente.dto.PeticionActReimpTicketDto;
import neto.sion.venta.servicios.cliente.dto.PeticionReimpresionDto;
import neto.sion.venta.servicios.cliente.dto.RespuestaActReimpTicketDto;
import neto.sion.venta.servicios.cliente.dto.RespuestaActReimpVoucherDto;
import neto.sion.venta.servicios.cliente.serviceFacade.ConsumirVenta;
import neto.sion.venta.servicios.cliente.serviceFacade.ConsumirVentaImp;

/**
 * Clase utilizada para la reimpresion de tickets y vouchers.
 *
 * @author Carlos V. Perez L.
 */
public class Reimpresion {
    
    private ConsumirVenta clienteWSVenta;
    private UtileriasReimpresiones utileriasReimpresion;
    
    public Reimpresion() {
        try {
            clienteWSVenta = new ConsumirVentaImp();
            utileriasReimpresion = new UtileriasReimpresiones();
        } catch (Exception ex) {
            System.out.println("Hubo una excepcion al momento de instancias la implementacion del WS:" + ex);
        }
    }

    /**
     * **
     * Metodo utilizado para reimprimir un ticket.
     *
     * @param peticion
     * @param caja
     * @param cajero
     * @param calleNumeroTienda
     * @param coloniaTienda
     * @param delegacionCiudadEstadoCp
     * @param nombreTienda
     * @throws Exception
     */
    /*public RespuestaReimpresionBean reimprimirTicket(PeticionReimpresionDto peticion, String caja, String calleNumeroTienda, String coloniaTienda, String delegacionCiudadEstadoCp, String nombreTienda) throws Exception {
        double montoRecibido = 0.0;
        double efectivoCambio = 0.0;
        double sumaImporteVenta = 0.0;
        RespuestaReimpresionBean respuesta = new RespuestaReimpresionBean();
        RespuestaReimpresionDto respuestaWS = null;
        try {
            respuestaWS = clienteWSVenta.obtenerReimpresionTicket(peticion);
        } catch (SionException ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "Hubo una excepcion al momento de ejecutar la reimpresion de un ticket");
        }
        if (respuestaWS.getReimpresionTicketBean().getPaCdgError() != 0) {
            respuesta.setPaCdgError(respuestaWS.getReimpresionTicketBean().getPaCdgError());
            respuesta.setPaDescError(respuestaWS.getReimpresionTicketBean().getPaDescError());
        } else if (respuestaWS.getReimpresionVoucherBean().getCdgError() != 0) {
            respuesta.setPaCdgErrorVouchers(respuestaWS.getReimpresionVoucherBean().getCdgError());
            respuesta.setPaDescErrorVouchers(respuestaWS.getReimpresionVoucherBean().getDescError());
        } else {
            respuesta.setPaCdgError(respuestaWS.getReimpresionTicketBean().getPaCdgError());
            respuesta.setPaDescError(respuestaWS.getReimpresionTicketBean().getPaDescError());
        }
        if (respuestaWS.getReimpresionTicketBean().getPaCdgError() == 0) {
            respuesta.setArticulos(respuestaWS.getReimpresionTicketBean().getPaArtReimpresiones());
            for (TipoPagoDto tipo : respuestaWS.getReimpresionTicketBean().getPaTPReimpresiones()) {
                if (tipo.getFiTipoPagoId() == 1) {
                    montoRecibido = tipo.getFiMontoRecibido();
                    efectivoCambio = tipo.getFiMontoRecibido() - tipo.getFnMontoPago();
                }
                sumaImporteVenta += tipo.getFnMontoPago();
            }
            VentaNormalTicketDto ticket = new VentaNormalTicketDto();
            ticket.setCaja(caja);
            ticket.setCajero(String.valueOf(respuestaWS.getReimpresionTicketBean().getUsuarioId()));
            ticket.setCalleNumeroTienda(calleNumeroTienda);
            ticket.setColoniaTienda(coloniaTienda);
            ticket.setDelegacionCiudadEstadoCP(delegacionCiudadEstadoCp);
            String fechaD = respuestaWS.getReimpresionTicketBean().getPaFechaCreacion();
            ticket.setFecha(fechaD.substring(0, 10));
            ticket.setHora(fechaD.substring(10));
            ticket.setFolio(String.valueOf(peticion.getPaNumTransaccion()));
            ticket.setNombreTienda(nombreTienda);
            ticket.setTiendaId(String.valueOf(peticion.getTiendaId()));
            ticket.setTransaccion(String.valueOf(peticion.getPaNumTransaccion()));
            ticket.setArticulos(utileriasReimpresion.reimpresionarticulosDtoaTicket(respuestaWS.getReimpresionTicketBean().getPaArtReimpresiones()));
            ticket.setTiposPago(utileriasReimpresion.reimpresiontipoPagoDtoaTicket(respuestaWS.getReimpresionTicketBean().getPaTPReimpresiones(), sumaImporteVenta, montoRecibido, efectivoCambio));
            ticket.setTotalArticulos();
            ticket.setTotalLetras(String.valueOf(utileriasReimpresion.getTotalLetras()));
            ticket.setIvaTotal();
            respuesta.setPaMovimientoId(respuestaWS.getReimpresionTicketBean().getPaMovimientoId());
            respuesta.setNombreUsuario(respuestaWS.getReimpresionTicketBean().getNombreUsuario());
            respuesta.setTicket(ticket);
        }
        if (respuestaWS.getReimpresionVoucherBean().getCdgError() == 0) {
            int cont = 0;
            VentaTarjetaVoucher[] vouchers = new VentaTarjetaVoucher[respuestaWS.getReimpresionVoucherBean().getPaReimpresionesTar().length];
            if (respuesta.getPaCdgError() == 0) {
                for (ReimpresionTarjetaDto tar : respuestaWS.getReimpresionVoucherBean().getPaReimpresionesTar()) {
                    VentaTarjetaVoucher voucher = new VentaTarjetaVoucher();
                    voucher.setCaja(caja);
                    voucher.setCajero(String.valueOf(respuestaWS.getReimpresionTicketBean().getUsuarioId()));
                    voucher.setCalleNumeroTienda(calleNumeroTienda);
                    voucher.setColoniaTienda(coloniaTienda);
                    voucher.setDelegacionCiudadEstadoCP(delegacionCiudadEstadoCp);
                    String fechaD = respuestaWS.getReimpresionVoucherBean().getPaFechaCreacion();
                    voucher.setFecha(fechaD.substring(0, 10));
                    voucher.setHora(fechaD.substring(10));
                    voucher.setFolio(String.valueOf(peticion.getPaNumTransaccion()));
                    voucher.setNombreTienda(nombreTienda);
                    voucher.setTiendaId(String.valueOf(peticion.getTiendaId()));
                    voucher.setTipoTarjeta(tar.getTipoTarjeta());
                    voucher.setNumeroTarjeta(tar.getFcNumTarjeta().substring(tar.getFcNumTarjeta().length() - 4));
                    voucher.setAfiliacion(String.valueOf(tar.getFiAfiliacion()));
                    voucher.setAutorizacion(tar.getFcNumeroAutorizacion());
                    voucher.setImporte(String.valueOf(tar.getImporteComision()));
                    voucher.setTotalLetras(String.valueOf(tar.getImporteComision()));
                    voucher.setTitularCuenta(null);
                    vouchers[cont] = voucher;
                    cont++;
                }
            }
            respuesta.setVouchers(vouchers);
            respuesta.setVoucherConfirmacion(respuestaWS.getReimpresionVoucherBean().getPaReimpresionesTar());
        }
        return respuesta;
    }*/

    /**
     * *
     * Metodo utilizado para actualizar la informacion de impresion de voucher.
     *
     * @param peticion
     */
    public RespuestaActReimpVoucherDto actualizarReimpresionVoucher(PeticionReimpresionDto peticion) throws SionException {
        RespuestaActReimpVoucherDto respuesta = null;
        try {
            respuesta = clienteWSVenta.actualizarReimpresionVoucher(peticion);
        } catch (SionException ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "Hubo una excepcion al momento de confirmar las reimpresiones de los vouchers");
            throw ex;
        }
        return respuesta;
        
    }

    /***
     * Metodo utilizado para confirmar la reimpresion de un ticket.
     * @param peticion
     * @return Un objeto con los detalles de la confirmacion de la reimpresion del ticket.
     */
    public RespuestaActReimpTicketDto confirmarReimpresionTicket(PeticionActReimpTicketDto peticion) throws SionException {
        RespuestaActReimpTicketDto respuestaConfirmacion = new RespuestaActReimpTicketDto();
        try {
            respuestaConfirmacion = clienteWSVenta.confirmarReimpresionTicket(peticion);
        } catch (SionException ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "Hubo una excepcion al momento de confirmar la reimpresion del voucher");
            throw ex;
        }
        return respuestaConfirmacion;
    }
}
