package neto.sion.tienda.venta.impresiones;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Level;
import neto.sion.impresion.ventanormal.dto.*;
import neto.sion.impresion.ventanormal.util.UtileriaTicket;
import neto.sion.tienda.controlimpresiontickets.ControlImpresionIReport;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.impresiones.factory.VentaServiciosBeansFactory;
import neto.sion.venta.servicios.cliente.dto.ArticuloDto;
import neto.sion.venta.servicios.cliente.dto.TipoPagoDto;

/**
 * Clase encargada de la impresion de los diferentes tickets de venta.
 *
 * @author Carlos V. Perez L.
 */
public class ImpresionVenta {

    private UtileriasImpresiones util;

    public ImpresionVenta() {
        util = new UtileriasImpresiones();
    }

    public ArticuloTicketDto articulosTADtoaTicket(ArticuloDto articulo, String compania) {
        return util.articulosTADtoaTicket(articulo, compania);
    }

    public ArticuloTicketDto[] articulosDtoaTicket(ArrayList<ArticuloDto> articulos) {
        return util.articulosDtoaTicket(articulos);
    }

    public TipoPagoTicketDto[] tipoPagoDtoaTicket(ArrayList<TipoPagoDto> tipos, Double totalVenta, double montoRecibido, double efectivoCambio) {
        return util.tipoPagoDtoaTicket(tipos, totalVenta, montoRecibido, efectivoCambio);
    }
    
    public TipoPagoTicketDto[] tipoPagoDtoaTicket(ArrayList<TipoPagoDto> tipos, Double totalVenta, double montoRecibido, double efectivoCambio, double importeDescuento) {
        return util.tipoPagoDtoaTicket(tipos, totalVenta, montoRecibido, efectivoCambio, importeDescuento);
    }

    public void imprimirTicketVenta(VentaNormalTicketDto ticket) throws Exception {
        imprimirTicketVenta(ticket, new HashMap<Object, Object>());
    }
    /**
     * **
     * Impresion de un ticket de venta normal.
     *
     * @param ticket
     */
    public void imprimirTicketVenta(VentaNormalTicketDto ticket, HashMap<Object, Object> _hs) throws Exception {
        HashMap<Object, Object> hs = new HashMap<Object, Object>();
        //if (SION.ENCRIPTADO) {
            hs.put("rutaImagenes", SION.obtenerParametro(Modulo.SION, "rutaImagenesGenerica"));
        /*} else {
            hs.put("rutaImagenes", System.getProperty("user.home").concat(File.separator.concat(SION.obtenerParametro(Modulo.SION, "rutaImagenes"))));
        }*/

        if (ticket.getTotalLetras().length() > 40) {
            hs.put("pequenoDescripcionTotal", true);
        } else {
            hs.put("pequenoDescripcionTotal", false);
        }
        
        hs.putAll(_hs);
        
        Collection<Object> coleccion = new ArrayList<Object>();
        //System.out.println("ticket antes: "+ticket.toString());
        coleccion.add(ticket);
        ControlImpresionIReport impresion = new ControlImpresionIReport("TicketVenta.jasper", hs, coleccion);
        try {
            impresion.generarDocumentoImprimir();
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error durante la impresion", Level.SEVERE);
            SION.logearExcepcion(Modulo.VENTA, ex, getStackTrace(ex));
            throw ex;
        }

    }

    /**
     * **
     * Impresion de un ticket de venta normal.
     *
     * @param ticket
     */
    public void reImprimirTicketVenta(VentaNormalTicketDto ticket) throws Exception {
        HashMap<Object, Object> hs = new HashMap<Object, Object>();
        //if (SION.ENCRIPTADO) {
            hs.put("rutaImagenes", SION.obtenerParametro(Modulo.SION, "rutaImagenesGenerica"));
        /*} else {
            hs.put("rutaImagenes", System.getProperty("user.home").concat(File.separator.concat(SION.obtenerParametro(Modulo.SION, "rutaImagenes"))));
        }*/

        if (ticket.getTotalLetras().length() > 40) {
            hs.put("pequenoDescripcionTotal", true);
        } else {
            hs.put("pequenoDescripcionTotal", false);
        }
        Collection<Object> coleccion = new ArrayList<Object>();
        coleccion.add(ticket);
        ControlImpresionIReport impresion = new ControlImpresionIReport("TicketVenta.jasper", hs, coleccion);
        try {
            impresion.generarDocumentoImprimir();
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error al durante la impresion", Level.SEVERE);
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw ex;
        }

    }

    /**
     * **
     * Impresion de un ticket de venta de tiempo aire telcel.
     *
     * @param ticketTA
     */
    public void imprimirTicketTATelcel(VentaTATicket ticketTA) throws Exception {
        HashMap<Object, Object> hs = new HashMap<Object, Object>();
        //if (SION.ENCRIPTADO) {
            hs.put("rutaImagenes", SION.obtenerParametro(Modulo.SION, "rutaImagenesGenerica"));
        /*} else {
            hs.put("rutaImagenes", System.getProperty("user.home").concat(File.separator.concat(SION.obtenerParametro(Modulo.SION, "rutaImagenes"))));
        }*/

        UtileriaTicket utilTicket = new UtileriaTicket();
        if (ticketTA.getTotalDescripcion().length() > 40) {
            hs.put("pequenoDescripcion", true);
        } else {
            hs.put("pequenoDescripcion", false);
        }
        if (Double.valueOf(utilTicket.quitarFormatoCantidad(
                ticketTA.getCambio(), 1)) > 0.0) {
            hs.put("existeCambio", true);
        } else {
            hs.put("existeCambio", false);
        }
        Collection<Object> coleccion = new ArrayList<Object>();
        coleccion.add(ticketTA);
        ControlImpresionIReport impresion = new ControlImpresionIReport("TiempoAireTelcel.jasper", hs, coleccion);
        try {
            impresion.generarDocumentoImprimir();
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error al durante la impresion", Level.SEVERE);
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw ex;
        }

    }

    /**
     * **
     * Impresion de un ticket de venta de tiempo aire.
     *
     * @param ticketTA
     */
    public void imprimirTicketTA(VentaTATicket ticketTA) throws Exception {
        HashMap<Object, Object> hs = new HashMap<Object, Object>();
        //if (SION.ENCRIPTADO) {
            hs.put("rutaImagenes", SION.obtenerParametro(Modulo.SION, "rutaImagenesGenerica"));
        /*} else {
            hs.put("rutaImagenes", System.getProperty("user.home").concat(File.separator.concat(SION.obtenerParametro(Modulo.SION, "rutaImagenes"))));
        }*/

        UtileriaTicket utilTicket = new UtileriaTicket();
        if (ticketTA.getTotalDescripcion().length() > 40) {
            hs.put("pequenoDescripcion", true);
        } else {
            hs.put("pequenoDescripcion", false);
        }
        if (Double.valueOf(utilTicket.quitarFormatoCantidad(
                ticketTA.getCambio(), 1)) > 0.0) {
            hs.put("existeCambio", true);
        } else {
            hs.put("existeCambio", false);
        }
        Collection<Object> coleccion = new ArrayList<Object>();
        coleccion.add(ticketTA);
        ControlImpresionIReport impresion = new ControlImpresionIReport("TiempoAire.jasper", hs, coleccion);
        try {
            impresion.generarDocumentoImprimir();
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error al durante la impresion", Level.SEVERE);
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw ex;
        }

    }

    /**
     * **
     * Impresion de un voucher de pago con tarjeta.
     *
     * @param voucher
     */
    public void imprimirVoucher(VentaTarjetaVoucher voucher) throws Exception {
        HashMap<Object, Object> hs = new HashMap<Object, Object>();
        //if (SION.ENCRIPTADO) {
            hs.put("rutaImagenes", SION.obtenerParametro(Modulo.SION, "rutaImagenesGenerica"));
        /*} else {
            hs.put("rutaImagenes", System.getProperty("user.home").concat(File.separator.concat(SION.obtenerParametro(Modulo.SION, "rutaImagenes"))));
        }*/

        if (voucher.getTotalLetras().length() > 40) {
            hs.put("pequenoDescripcionTotal", true);
        } else {
            hs.put("pequenoDescripcionTotal", false);
        }
        if (voucher.getTitularCuenta() != null && !(voucher.getTitularCuenta().trim().equals(""))) {
            hs.put("existeTitular", true);
        } else {
            hs.put("existeTitular", false);
        }

        Collection<Object> coleccion = new ArrayList<Object>();
        coleccion.add(voucher);
        ControlImpresionIReport impresion = new ControlImpresionIReport("TicketTarjetas.jasper", hs, coleccion);
        try {
            impresion.generarDocumentoImprimir();
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error al durante la impresion", Level.SEVERE);
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw ex;
        }

    }

    /**
     * **
     * Impresion de un ticket de devolucion.
     *
     * @param voucher
     */
    public void imprimirTicketDevolucion(DevolucionTicketDto ticket, boolean esDevolucion) throws Exception {
        HashMap<Object, Object> hs = new HashMap<Object, Object>();
        //if (SION.ENCRIPTADO) {
            hs.put("rutaImagenes", SION.obtenerParametro(Modulo.SION, "rutaImagenesGenerica"));
        /*} else {
            hs.put("rutaImagenes", System.getProperty("user.home").concat(File.separator.concat(SION.obtenerParametro(Modulo.SION, "rutaImagenes"))));
        }*/
        if(esDevolucion)
            ticket.setCabecero("DEVOLUCIÓN");
        else
            ticket.setCabecero("CANCELACIÓN");
        Collection<Object> coleccion = new ArrayList<Object>();
        coleccion.add(ticket);
        ControlImpresionIReport impresion = new ControlImpresionIReport("TicketDevolucion.jasper", hs, coleccion);
        try {
            impresion.generarDocumentoImprimir();
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error al durante la impresion", Level.SEVERE);
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw ex;
        }
    }
    
    public void imprimirTicketServicios(VentaServiciosTicketDto ticket) throws Exception{
        HashMap<Object, Object> hs = new HashMap<Object, Object>();
        //if (SION.ENCRIPTADO) {
            hs.put("rutaImagenes", SION.obtenerParametro(Modulo.SION, "rutaImagenesGenerica"));
        /*} else {
            hs.put("rutaImagenes", System.getProperty("user.home").concat(File.separator.concat(SION.obtenerParametro(Modulo.SION, "rutaImagenes"))));
        }*/
        
        if (ticket.getTotalLetras().length() > 40) {
            hs.put("pequenoDescripcionTotal", true);
        } else {
            hs.put("pequenoDescripcionTotal", false);
        }
        VentaServiciosBeansFactory factory = new VentaServiciosBeansFactory();
        factory.setVentaServiciosTicketDto(ticket);
        ControlImpresionIReport impresion = new ControlImpresionIReport("TicketVentaServicios.jasper", hs, factory.getCollection());
        try {
            impresion.generarDocumentoImprimir();
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error al durante la impresion", Level.INFO);
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw ex;
        }
    }

    public void imprimirTicketVentaMixta(VentaMixtaTicket ticket, boolean existeTelcel, 
            boolean existeMovistar, boolean existeUnefon, boolean existeIusacell) throws Exception {
        HashMap<Object, Object> hs = new HashMap<Object, Object>();
        //if (SION.ENCRIPTADO) {
            hs.put("rutaImagenes", SION.obtenerParametro(Modulo.SION, "rutaImagenesGenerica"));
        /*} else {
            hs.put("rutaImagenes", System.getProperty("user.home").concat(File.separator.concat(SION.obtenerParametro(Modulo.SION, "rutaImagenes"))));
        }*/

        if (ticket.getTotalLetras().length() > 40) {
            hs.put("pequenoDescripcionTotal", true);
        } else {
            hs.put("pequenoDescripcionTotal", false);
        }
            
        hs.put("ventaArticulos", true);
        hs.put("recargaTelcel", existeTelcel);
        hs.put("recargaMovistar", existeMovistar);
        hs.put("recargaUnefon", existeUnefon);
        hs.put("recargaIusacell", existeIusacell);
        
        Collection<Object> coleccion = new ArrayList<Object>();
        coleccion.add(ticket);
        ControlImpresionIReport impresion = new ControlImpresionIReport("TicketVentaMixta.jasper", hs, coleccion);
        try {
            System.out.println();
            impresion.generarDocumentoImprimir();
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error al durante la impresion", Level.INFO);
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw ex;
        }

    }

    private VentaMixtaTicket getCollection(){
        VentaMixtaTicket t = new VentaMixtaTicket();
        t.setEncabezado("TIENDAS SUPER PRECIO, S.A. de C.V. AV. INSURGENTES SUR 3155 COL. JARDINES DEL PEDREGAL DEL. COYOACÁN C.P. 04500 MÉXICO, DF. RFC: TSP0004051I4");
        t.setTiendaId("903");
        t.setNombreTienda("Las Torres");
        t.setCalleNumeroTienda("Insurgentes Sur 3155");
        t.setColoniaTienda("Amp. San Angel");
        t.setDelegacionCiudadEstadoCP("Coyoacan");
        t.setCajero("835077");
        t.setCaja("69");
        t.setFolio("2013061845689I");
        t.setFecha("18/06/2013");
        t.setHora("18:24:00");
        
        ArticuloTicketDto[] articulos = new ArticuloTicketDto[3];
        
        ArticuloTicketDto a1 = new ArticuloTicketDto();
        a1.setCantidad("1");
        a1.setImporte("20.00");
        a1.setIva(0);
        a1.setNombre("Articulo 1 no recarga");
        a1.setPrecio("20.00");
        
        articulos[0] = a1;
        
        ArticuloTicketDto a2 = new ArticuloTicketDto();
        a2.setCantidad("2");
        a2.setImporte("40.00");
        a2.setIva(0);
        a2.setNombre("Telcel $20");
        a2.setPrecio("20.00");
        
        articulos[1] = a2;
        
        ArticuloTicketDto a3 = new ArticuloTicketDto();
        a3.setCantidad("1");
        a3.setImporte("30.00");
        a3.setIva(0);
        a3.setNombre("Iusacell $30");
        a3.setPrecio("30.00");
        
        articulos[2] = a3;
        
        t.setArticulos(articulos);
        
        TipoPagoTicketDto[] tipos = new TipoPagoTicketDto[2];
        
        TipoPagoTicketDto t1 = new TipoPagoTicketDto();
        t1.setDescripcionTipoPago("Efectivo");
        t1.setMontoPago("100.00");
        t1.setTipoPago(1);
        
        tipos[0] = t1;
        
        TipoPagoTicketDto t2 = new TipoPagoTicketDto();
        t2.setDescripcionTipoPago("Cambio");
        t2.setMontoPago("10.00");
        t2.setTipoPago(5);
        
        tipos[1] = t2;
        
        t.setTiposPago(tipos);
        
        t.setTotalArticulos();
        t.setIvaTotal();
        t.setTotalLetras("90.00");
        t.setTransaccion("2013061845689I");
        
        RecargaTA[] telcelTA = new RecargaTA[2];
        
        RecargaTA tcl1 = new RecargaTA();
        tcl1.setNumeroTelefono("5528904857");
        tcl1.setNumeroOperacion("45632");
        tcl1.setNumeroReferencia("11689");
        
        telcelTA[0] = tcl1;
        
        RecargaTA tcl2 = new RecargaTA();
        tcl2.setNumeroTelefono("5528904857");
        tcl2.setNumeroOperacion("45632");
        tcl2.setNumeroReferencia("11689");
        
        telcelTA[1] = tcl2;
        
        t.setTelcelTA(telcelTA);
        
        t.setMensajeTelcel("Mensaje de prueba Telcel");
        
        RecargaTA[] iusaTA = new RecargaTA[2];
        
        RecargaTA iusa1 = new RecargaTA();
        iusa1.setNumeroTelefono("5528904857");
        iusa1.setNumeroOperacion("45632");
        iusa1.setNumeroReferencia("11689");
        
        iusaTA[0] = iusa1;
        
        RecargaTA iusa2 = new RecargaTA();
        iusa2.setNumeroTelefono("5528904857");
        iusa2.setNumeroOperacion("45632");
        iusa2.setNumeroReferencia("11689");
        
        iusaTA[1] = iusa2;
        
        t.setIusacellTA(iusaTA);
        
        t.setMensajeIusacell("Mensaje de prueba iusacell");
        return t;
    }
    
        /**
     * **
     * Impresion de un ticket de venta normal.
     *
     * @param ticket
     */
    public void imprimir(String ticket, HashMap<Object, Object> _hs) throws Exception {
        HashMap<Object, Object> hs = new HashMap<Object, Object>();
        //if (SION.ENCRIPTADO) {
            hs.put("rutaImagenes", SION.obtenerParametro(Modulo.SION, "rutaImagenesGenerica"));
        /*} else {
            hs.put("rutaImagenes", System.getProperty("user.home").concat(File.separator.concat(SION.obtenerParametro(Modulo.SION, "rutaImagenes"))));
        }*/

        hs.putAll(_hs);
        
        Collection<Object> coleccion = new ArrayList<Object>();
        //System.out.println("ticket antes: "+ticket.toString());
        ////coleccion.add(ticket);
        ControlImpresionIReport impresion = new ControlImpresionIReport("TicketVenta.jasper", hs, coleccion);
        try {
            impresion.generarDocumentoImprimir();
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error durante la impresion", Level.SEVERE);
            SION.logearExcepcion(Modulo.VENTA, ex, getStackTrace(ex));
            throw ex;
        }

    }

    public static void main(String[] args) {
        ImpresionVenta i = new ImpresionVenta();
        try {
            i.imprimirTicketVentaMixta(i.getCollection(), true, false, false, true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
        
    /**
     *
     * Devuelve el valor del total de la venta antes de ser convertido a cadena
     *
     * @return Un double
     */
    public double getTotalLetras() {
        return util.getTotalLetras();
    }
    
    public static String getStackTrace(Throwable aThrowable) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }
}
