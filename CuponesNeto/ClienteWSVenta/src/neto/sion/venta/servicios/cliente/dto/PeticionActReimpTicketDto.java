package neto.sion.venta.servicios.cliente.dto;

import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;

public class PeticionActReimpTicketDto extends PeticionBaseVentaDto {

    private long paMovimientoId;

    public long getPaMovimientoId() {
        return paMovimientoId;
    }

    public void setPaMovimientoId(long paMovimientoId) {
        this.paMovimientoId = paMovimientoId;
    }

    @Override
    public String toString() {
        return "PeticionActReimpTicketDto{" + "paMovimientoId=" + paMovimientoId + '}';
    }
}
