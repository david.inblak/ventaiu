/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvega
 */
public class PeticionTicketReimpresionDto {

    private int paPaisId;
    private long paTiendaId;
    private long paNumTransaccion;
    private int paParamTipoTrans;

    public int getPaPaisId() {
        return paPaisId;
    }

    public void setPaPaisId(int paPaisId) {
        this.paPaisId = paPaisId;
    }

    public long getPaTiendaId() {
        return paTiendaId;
    }

    public void setPaTiendaId(long paTiendaId) {
        this.paTiendaId = paTiendaId;
    }

    public long getPaNumTransaccion() {
        return paNumTransaccion;
    }

    public void setPaNumTransaccion(long paNumTransaccion) {
        this.paNumTransaccion = paNumTransaccion;
    }

    public int getPaParamTipoTrans() {
        return paParamTipoTrans;
    }

    public void setPaParamTipoTrans(int paParamTipoTrans) {
        this.paParamTipoTrans = paParamTipoTrans;
    }

    @Override
    public String toString() {
        return "PeticionTicketReimpresionDto [paPaisId=" + paPaisId
                + ", paTiendaId=" + paTiendaId + ", paNumTransaccion="
                + paNumTransaccion + ", paParamTipoTrans=" + paParamTipoTrans
                + "]";
    }
}
