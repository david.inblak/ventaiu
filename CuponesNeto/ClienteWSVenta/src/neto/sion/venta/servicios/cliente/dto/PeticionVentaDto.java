package neto.sion.venta.servicios.cliente.dto;

import java.util.Arrays;
import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * *
 * Clase que representa la peticion de una venta.
 *
 * @author Carlos V. Perez L.
 *
 */
public class PeticionVentaDto extends PeticionBaseVentaDto {
    @JsonProperty("paTerminal") private String paTerminal;
    @JsonProperty("paUsuarioId") private long paUsuarioId;
    @JsonProperty("paUsuarioAutorizaId") private long paUsuarioAutorizaId;
    @JsonProperty("paFechaOper") private String paFechaOper;
    @JsonProperty("paTipoMovto") private int paTipoMovto;
    @JsonProperty("paMontoTotalVta") private double paMontoTotalVta;
    @JsonProperty("articulos") private ArticuloDto[] articulos;
    @JsonProperty("tiposPago") private TipoPagoDto[] tiposPago;
    @JsonProperty("fueraLinea") private int fueraLinea;

    public String getPaTerminal() {
        return paTerminal;
    }

    public void setPaTerminal(String paTerminal) {
        this.paTerminal = paTerminal;
    }

    public long getPaUsuarioId() {
        return paUsuarioId;
    }

    public void setPaUsuarioId(long paUsuarioId) {
        this.paUsuarioId = paUsuarioId;
    }

    public long getPaUsuarioAutorizaId() {
        return paUsuarioAutorizaId;
    }

    public void setPaUsuarioAutorizaId(long paUsuarioAutorizaId) {
        this.paUsuarioAutorizaId = paUsuarioAutorizaId;
    }
    
    public String getPaFechaOper() {
        return paFechaOper;
    }

    public void setPaFechaOper(String paFechaOper) {
        this.paFechaOper = paFechaOper;
    }

    public int getPaTipoMovto() {
        return paTipoMovto;
    }

    public void setPaTipoMovto(int paTipoMovto) {
        this.paTipoMovto = paTipoMovto;
    }

    public double getPaMontoTotalVta() {
        return paMontoTotalVta;
    }

    public void setPaMontoTotalVta(double paMontoTotalVta) {
        this.paMontoTotalVta = paMontoTotalVta;
    }

    public ArticuloDto[] getArticulos() {
        return articulos;
    }

    public void setArticulos(ArticuloDto[] articulos) {
        this.articulos = articulos;
    }

    public TipoPagoDto[] getTiposPago() {
        return tiposPago;
    }

    public void setTiposPago(TipoPagoDto[] tiposPago) {
        this.tiposPago = tiposPago;
    }

    public int getFueraLinea() {
        return fueraLinea;
    }

    public void setFueraLinea(int fueraLinea) {
        this.fueraLinea = fueraLinea;
    }

    @Override
    public String toString() {
        return "PeticionVentaDto{" + "paTerminal=" + paTerminal 
        + ", paPaisId=" + getPaPaisId()
        + ", paTiendaId=" + getTiendaId()
        + ", paUsuarioId=" + paUsuarioId 
        + ", paUsuarioAutorizaId=" + paUsuarioAutorizaId   +
                ", paFechaOper=" + paFechaOper 
                + ", paTipoMovto=" + paTipoMovto 
                + ", paMontoTotalVta=" + paMontoTotalVta + ", articulos=" + Arrays.toString(articulos) 
                + ", tiposPago=" + Arrays.toString(tiposPago) 
                + ", paConciliacionId=" + getPaConciliacionId()        
                + ", fueraLinea=" + fueraLinea + '}';
    }
}
