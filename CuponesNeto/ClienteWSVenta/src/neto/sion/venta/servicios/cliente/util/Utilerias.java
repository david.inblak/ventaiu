/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.util;

import neto.sion.venta.servicios.cliente.dto.*;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.ArticuloDevReimpresionBean;
//import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.ArticuloPeticionVentaBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.ArticuloReimpresionTicketBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.ArticuloTAReimpresionBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.PeticionTicketReimpresionBean;
//import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.PeticionVentaArticulosBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.RespuestaTicketReimpresionBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.ServicioReimpresionBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.TarjetaReimpresionBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.TipoPagoReimpresionBean;
import neto.sion.ventared.servicios.cliente.proxy.VentaSeparacionServiceStub;

/**
 *
 * @author fvega
 */
public class Utilerias {

    /**
     * *
     * Acoplamiento de una Peticion Venta Dto utilizada por el front y un Bean
     * utilzado por el WS.
     *
     * @param _peticion
     * @return Un objeto de peticion del tipo esperado por el WS.
     */
    public VentaSeparacionServiceStub.PeticionVentaArticulosBean peticionDtoaBean(PeticionVentaDto _peticion) {
        VentaSeparacionServiceStub.PeticionVentaArticulosBean peticion = new VentaSeparacionServiceStub.PeticionVentaArticulosBean();
        peticion.setPaTerminal(_peticion.getPaTerminal());
        peticion.setPaPaisId(_peticion.getPaPaisId());
        peticion.setTiendaId(_peticion.getTiendaId());
        peticion.setPaUsuarioId(_peticion.getPaUsuarioId());
        peticion.setPaUsuarioAutorizaId(_peticion.getPaUsuarioAutorizaId());
        peticion.setPaFechaOper(_peticion.getPaFechaOper());
        peticion.setPaTipoMovto(_peticion.getPaTipoMovto());
        peticion.setPaMontoTotalVta(_peticion.getPaMontoTotalVta());
        peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
        if (_peticion.getArticulos() == null) {
            peticion.setArticulos(null);
        } else {
            peticion.setArticulos((articulosDtoaBean(_peticion.getArticulos())));
        }
        if (_peticion.getTiposPago() == null) {
            peticion.setTiposPago(null);
        } else {
            peticion.setTiposPago(tiposPagoDtoaBean(_peticion.getTiposPago()));
        }
        peticion.setUId(_peticion.getUId());
        peticion.setIpTerminal(_peticion.getIpTerminal());
        return peticion;
    }

    /**
     * *
     * Acoplamiento entre tipos de pago Dto y Bean
     *
     * @param tiposPagoDto
     * @return Arreglo de tipos de pago tipo Bean.
     */
    public VentaSeparacionServiceStub.TipoPagoBean[] tiposPagoDtoaBean(TipoPagoDto[] tiposPagoDto) {
        if (tiposPagoDto != null) {
            VentaSeparacionServiceStub.TipoPagoBean[] tipos = new VentaSeparacionServiceStub.TipoPagoBean[tiposPagoDto.length];
            int contador = 0;
            for (TipoPagoDto dto : tiposPagoDto) {
                if (dto != null) {
                    VentaSeparacionServiceStub.TipoPagoBean tipo = new VentaSeparacionServiceStub.TipoPagoBean();
                    tipo.setFiTipoPagoId(dto.getFiTipoPagoId());
                    tipo.setFnMontoPago(dto.getFnMontoPago());
                    tipo.setFnNumeroVales(dto.getFnNumeroVales());
                    tipo.setPaPagoTarjetaIdBus(dto.getPaPagoTarjetaIdBus());
                    tipo.setImporteAdicional(dto.getImporteAdicional());
                    tipos[contador] = tipo;
                    contador++;
                } else {
                    return null;
                }
            }
            return tipos;
        } else {
            return null;
        }
    }
    
    /**
     * *
     * Acoplamiento entre tipos de pago Dto y Bean
     *
     * @param tiposPagoDto
     * @return Arreglo de tipos de pago tipo Bean.
     */
    public VentaServiceStub.TipoPagoBean[] tiposPagoDtoaBeanAnt(TipoPagoDto[] tiposPagoDto) {
        if (tiposPagoDto != null) {
            VentaServiceStub.TipoPagoBean[] tipos = new VentaServiceStub.TipoPagoBean[tiposPagoDto.length];
            int contador = 0;
            for (TipoPagoDto dto : tiposPagoDto) {
                if (dto != null) {
                    VentaServiceStub.TipoPagoBean tipo = new VentaServiceStub.TipoPagoBean();
                    tipo.setFiTipoPagoId(dto.getFiTipoPagoId());
                    tipo.setFnMontoPago(dto.getFnMontoPago());
                    tipo.setFnNumeroVales(dto.getFnNumeroVales());
                    tipo.setPaPagoTarjetaIdBus(dto.getPaPagoTarjetaIdBus());
                    tipo.setImporteAdicional(dto.getImporteAdicional());
                    tipos[contador] = tipo;
                    contador++;
                } else {
                    return null;
                }
            }
            return tipos;
        } else {
            return null;
        }
    }

    /**
     * *
     * Acoplamiento entre articulos Dto y Bean.
     *
     * @param articulosDto
     * @return Arreglo de articulos de tipo Bean.
     */
    public VentaSeparacionServiceStub.ArticuloPeticionVentaBean[] articulosDtoaBean(ArticuloDto[] articulosDto) {
        if (articulosDto != null) {
            VentaSeparacionServiceStub.ArticuloPeticionVentaBean[] articulos = new VentaSeparacionServiceStub.ArticuloPeticionVentaBean[articulosDto.length];
            int contador = 0;
            for (ArticuloDto dto : articulosDto) {
                if (dto != null) {
                    VentaSeparacionServiceStub.ArticuloPeticionVentaBean articulo = new VentaSeparacionServiceStub.ArticuloPeticionVentaBean();
                    articulo.setFiArticuloId(dto.getFiArticuloId());
                    articulo.setFcCdGbBarras(dto.getFcCdGbBarras());
                    articulo.setFnCantidad(dto.getFnCantidad());
                    articulo.setFnCosto(dto.getFnCosto());
                    articulo.setFnDescuento(dto.getFnDescuento());
                    articulo.setFnIva(dto.getFnIva());
                    articulo.setFnPrecio(dto.getFnPrecio());
                    articulo.setFiIepsId(dto.getFnIepsId());
                    articulo.setFiTipoDescuento(dto.getTipoDescuento());
                    articulos[contador] = articulo;
                    contador++;
                } else {
                    return null;
                }
            }
            return articulos;
        } else {
            return null;
        }
    }
    
    /**
     * *
     * Acoplamiento entre articulos Dto y Bean.
     *
     * @param articulosDto
     * @return Arreglo de articulos de tipo Bean.
     */
    public VentaServiceStub.ArticuloPeticionVentaBean[] articulosDtoaBeanAnt(ArticuloDto[] articulosDto) {
        if (articulosDto != null) {
            VentaServiceStub.ArticuloPeticionVentaBean[] articulos = new VentaServiceStub.ArticuloPeticionVentaBean[articulosDto.length];
            int contador = 0;
            for (ArticuloDto dto : articulosDto) {
                if (dto != null) {
                    VentaServiceStub.ArticuloPeticionVentaBean articulo = new VentaServiceStub.ArticuloPeticionVentaBean();
                    articulo.setFiArticuloId(dto.getFiArticuloId());
                    articulo.setFcCdGbBarras(dto.getFcCdGbBarras());
                    articulo.setFnCantidad(dto.getFnCantidad());
                    articulo.setFnCosto(dto.getFnCosto());
                    articulo.setFnDescuento(dto.getFnDescuento());
                    articulo.setFnIva(dto.getFnIva());
                    articulo.setFnPrecio(dto.getFnPrecio());
                    articulo.setFiIepsId(dto.getFnIepsId());
                    articulos[contador] = articulo;
                    contador++;
                } else {
                    return null;
                }
            }
            return articulos;
        } else {
            return null;
        }
    }

    /**
     * ***reimpresion devoluciones 21 mayo 2014***
     */
    public RespuestaTicketReimpresionDto respuestaTicketReimpresionBeanaDto(RespuestaTicketReimpresionBean _respuesta) {

        RespuestaTicketReimpresionDto respuesta = new RespuestaTicketReimpresionDto();
        int contador = 0;

        respuesta.setFiUsuarioId(_respuesta.getFiUsuarioId());
        respuesta.setPaCdgError(_respuesta.getPaCdgError());
        respuesta.setPaDescError(_respuesta.getPaDescError());
        respuesta.setPaEstacionTrabajo(_respuesta.getPaEstacionTrabajo());
        respuesta.setPaFechaCreacion(_respuesta.getPaFechaCreacion());
        respuesta.setPaMoviminetoId(_respuesta.getPaMoviminetoId());
        respuesta.setPaNombreUsuario(_respuesta.getPaNombreUsuario());
        respuesta.setPaPermiteTicket(_respuesta.getPaPermiteTicket());
        respuesta.setPaTipoMovimiento(_respuesta.getPaTipoMovimiento());

        //valida codigo de error para llenar cursores
        if (respuesta.getPaCdgError() == 0) {

            TipoPagoReimpresionDto[] arregloTiposPago = new TipoPagoReimpresionDto[_respuesta.getTipoPagoReimpresionBeans().length];
            for (TipoPagoReimpresionBean tipoPago : _respuesta.getTipoPagoReimpresionBeans()) {
                TipoPagoReimpresionDto dto = new TipoPagoReimpresionDto();
                dto.setFiTipoPagoId(tipoPago.getFiTipoPagoId());
                dto.setFnMontoRecibido(tipoPago.getFnMontoRecibido());
                dto.setMontoPago(tipoPago.getMontoPago());
                arregloTiposPago[contador] = dto;
                contador++;
            }
            respuesta.setTipoPagoReimpresionDtos(arregloTiposPago);

            contador = 0;
            switch (respuesta.getPaTipoMovimiento()) {
                case 9://venta de articulos

                    ArticuloReimpresionTicketDto[] arregloArticulos = new ArticuloReimpresionTicketDto[_respuesta.getArticuloReimpresionBeans().length];
                    for (ArticuloReimpresionTicketBean articulo : _respuesta.getArticuloReimpresionBeans()) {
                        ArticuloReimpresionTicketDto dto = new ArticuloReimpresionTicketDto();
                        dto.setFcNombreArticulo(articulo.getFcNombreArticulo());
                        dto.setFiIepsId(articulo.getFiIepsId());
                        dto.setFnCantidad(articulo.getFnCantidad());
                        dto.setFnDescuento(articulo.getFnDescuento());
                        dto.setFnIva(articulo.getFnIva());
                        dto.setFnPrecio(articulo.getFnPrecio());
                        arregloArticulos[contador] = dto;
                        contador++;
                    }
                    respuesta.setArticuloReimpresionDtos(arregloArticulos);

                    contador = 0;
                    TarjetaReimpresionDto[] arregloPagosTarjeta = new TarjetaReimpresionDto[_respuesta.getTarjetaReimpresionBeans().length];
                    for (TarjetaReimpresionBean pagoTarjeta : _respuesta.getTarjetaReimpresionBeans()) {
                        TarjetaReimpresionDto dto = new TarjetaReimpresionDto();
                        if (pagoTarjeta != null) {
                            if (pagoTarjeta.getFcDescripcion() != null && pagoTarjeta.getFcNumeroAutorizacion() != null) {
                                dto.setFcDescripcion(pagoTarjeta.getFcDescripcion());
                                dto.setFcNumeroAutorizacion(pagoTarjeta.getFcNumeroAutorizacion());
                                dto.setFcNumeroTarjeta(pagoTarjeta.getFcNumeroTarjeta());
                                dto.setFiAfiliacion(pagoTarjeta.getFiAfiliacion());
                                dto.setFiPagoTarjetasId(pagoTarjeta.getFiPagoTarjetasId());
                                dto.setImporteComision(pagoTarjeta.getImporteComision());
                                arregloPagosTarjeta[contador] = dto;
                                contador++;
                            }
                        }
                    }
                    respuesta.setTarjetaReimpresionDtos(arregloPagosTarjeta);

                    respuesta.setArregloArticulosDevolucion(null);
                    respuesta.setArticuloTAReimpresionDtos(null);
                    respuesta.setServiciosReimpresionDtos(null);
                    break;
                case 10://recarga de tiempo aire

                    contador = 0;
                    ArticuloTAReimpresionDto[] arregloRecargas = new ArticuloTAReimpresionDto[_respuesta.getArticuloTAReimpresionBeans().length];
                    for (ArticuloTAReimpresionBean recarga : _respuesta.getArticuloTAReimpresionBeans()) {
                        ArticuloTAReimpresionDto dto = new ArticuloTAReimpresionDto();
                        dto.setCompania(recarga.getCompania());
                        dto.setFcNombreArticulo(recarga.getFcNombreArticulo());
                        dto.setFiEmpresaId(recarga.getFiEmpresaId());
                        dto.setNumOperacion(recarga.getNumOperacion());
                        dto.setNumReferencia(recarga.getNumReferencia());
                        dto.setNumero(recarga.getNumero());
                        arregloRecargas[contador] = dto;
                        contador++;
                    }
                    respuesta.setArticuloTAReimpresionDtos(arregloRecargas);

                    respuesta.setArregloArticulosDevolucion(null);
                    respuesta.setArticuloReimpresionDtos(null);
                    respuesta.setServiciosReimpresionDtos(null);
                    respuesta.setTarjetaReimpresionDtos(null);

                    break;
                case 19://pago de servicio

                    contador = 0;
                    ServicioReimpresionDto[] arregloServicios = new ServicioReimpresionDto[_respuesta.getServiciosReimpresionBeans().length];
                    for (ServicioReimpresionBean servicio : _respuesta.getServiciosReimpresionBeans()) {
                        ServicioReimpresionDto dto = new ServicioReimpresionDto();
                        dto.setComision(servicio.getComision());
                        dto.setImpuesto(servicio.getImpuesto());
                        dto.setMensaje(servicio.getMensaje());
                        dto.setMonto(servicio.getMonto());
                        dto.setNomArticulo(servicio.getNomArticulo());
                        dto.setNumAutorizacion(servicio.getNumAutorizacion());
                        dto.setReferencia(servicio.getReferencia());
                        arregloServicios[contador] = dto;
                        contador++;
                    }
                    respuesta.setServiciosReimpresionDtos(arregloServicios);

                    respuesta.setArregloArticulosDevolucion(null);
                    respuesta.setArticuloReimpresionDtos(null);
                    respuesta.setArticuloTAReimpresionDtos(null);
                    respuesta.setTarjetaReimpresionDtos(null);
                    break;
                case 58://devolucion

                    contador = 0;
                    ArticuloDevReimpresionDto[] arregloDevoluciones = new ArticuloDevReimpresionDto[_respuesta.getArregloArticulosDevolucion().length];
                    for (ArticuloDevReimpresionBean articuloDev : _respuesta.getArregloArticulosDevolucion()) {
                        ArticuloDevReimpresionDto dto = new ArticuloDevReimpresionDto();
                        dto.setCantidad(articuloDev.getCantidad());
                        dto.setIepsId(articuloDev.getIepsId());
                        dto.setIva(articuloDev.getIva());
                        dto.setNombre(articuloDev.getNombre());
                        dto.setPrecio(articuloDev.getPrecio());
                        arregloDevoluciones[contador] = dto;
                        contador++;
                    }
                    respuesta.setArregloArticulosDevolucion(arregloDevoluciones);

                    respuesta.setArticuloReimpresionDtos(null);
                    respuesta.setArticuloTAReimpresionDtos(null);
                    respuesta.setServiciosReimpresionDtos(null);
                    respuesta.setTarjetaReimpresionDtos(null);
                    break;
            }

        } else {
            respuesta.setArticuloReimpresionDtos(null);
            respuesta.setArticuloTAReimpresionDtos(null);
            respuesta.setServiciosReimpresionDtos(null);
            respuesta.setTarjetaReimpresionDtos(null);
            respuesta.setArregloArticulosDevolucion(null);
        }

        return respuesta;
    }

    public PeticionTicketReimpresionBean peticionTicketReimpresionDtoaBean(PeticionTicketReimpresionDto _peticion) {
        PeticionTicketReimpresionBean peticionBean = new PeticionTicketReimpresionBean();

        peticionBean.setPaNumTransaccion(_peticion.getPaNumTransaccion());
        peticionBean.setPaPaisId(_peticion.getPaPaisId());
        peticionBean.setPaParamTipoTrans(_peticion.getPaParamTipoTrans());
        peticionBean.setPaTiendaId(_peticion.getPaTiendaId());

        return peticionBean;
    }
}
