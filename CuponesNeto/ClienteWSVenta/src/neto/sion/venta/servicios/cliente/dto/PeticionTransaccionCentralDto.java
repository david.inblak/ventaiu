package neto.sion.venta.servicios.cliente.dto;

import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;

/**
 * *
 * Clase que representa los datos necesarios para una consulta de transaccion en
 * central
 *
 * @author Carlos V. Perez L.
 *
 */
public class PeticionTransaccionCentralDto extends PeticionBaseVentaDto {

    private long paUsuarioId;
    private int paSistemaId;
    private int paModuloId;
    private int paSubModuloId;

    public long getPaUsuarioId() {
        return paUsuarioId;
    }

    public void setPaUsuarioId(long paUsuarioId) {
        this.paUsuarioId = paUsuarioId;
    }

    public int getPaSistemaId() {
        return paSistemaId;
    }

    public void setPaSistemaId(int paSistemaId) {
        this.paSistemaId = paSistemaId;
    }

    public int getPaModuloId() {
        return paModuloId;
    }

    public void setPaModuloId(int paModuloId) {
        this.paModuloId = paModuloId;
    }

    public int getPaSubModuloId() {
        return paSubModuloId;
    }

    public void setPaSubModuloId(int paSubModuloId) {
        this.paSubModuloId = paSubModuloId;
    }

    @Override
    public String toString() {
        return "PeticionTransaccionCentralDto [paUsuarioId=" + paUsuarioId
                + ", paSistemaId=" + paSistemaId + ", paModuloId=" + paModuloId
                + ", paSubModuloId=" + paSubModuloId + super.toString() + "]";
    }
}
