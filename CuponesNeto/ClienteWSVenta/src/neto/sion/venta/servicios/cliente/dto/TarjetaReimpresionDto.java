/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvega
 */
public class TarjetaReimpresionDto {

    private long fiPagoTarjetasId;
    private double importeComision;
    private String fcNumeroTarjeta;
    private String fcNumeroAutorizacion;
    private long fiAfiliacion;
    private String fcDescripcion;

    public long getFiPagoTarjetasId() {
        return fiPagoTarjetasId;
    }

    public void setFiPagoTarjetasId(long fiPagoTarjetasId) {
        this.fiPagoTarjetasId = fiPagoTarjetasId;
    }

    public double getImporteComision() {
        return importeComision;
    }

    public void setImporteComision(double importeComision) {
        this.importeComision = importeComision;
    }

    public String getFcNumeroTarjeta() {
        return fcNumeroTarjeta;
    }

    public void setFcNumeroTarjeta(String fcNumeroTarjeta) {
        this.fcNumeroTarjeta = fcNumeroTarjeta;
    }

    public String getFcNumeroAutorizacion() {
        return fcNumeroAutorizacion;
    }

    public void setFcNumeroAutorizacion(String fcNumeroAutorizacion) {
        this.fcNumeroAutorizacion = fcNumeroAutorizacion;
    }

    public long getFiAfiliacion() {
        return fiAfiliacion;
    }

    public void setFiAfiliacion(long fiAfiliacion) {
        this.fiAfiliacion = fiAfiliacion;
    }

    public String getFcDescripcion() {
        return fcDescripcion;
    }

    public void setFcDescripcion(String fcDescripcion) {
        this.fcDescripcion = fcDescripcion;
    }

    @Override
    public String toString() {
        return "TarjetaReimpresionDto{" + "fiPagoTarjetasId=" + fiPagoTarjetasId + ", importeComision=" + importeComision + ", fcNumeroTarjeta=" + fcNumeroTarjeta + ", fcNumeroAutorizacion=" + fcNumeroAutorizacion + ", fiAfiliacion=" + fiAfiliacion + ", fcDescripcion=" + fcDescripcion + '}';
    }
}
