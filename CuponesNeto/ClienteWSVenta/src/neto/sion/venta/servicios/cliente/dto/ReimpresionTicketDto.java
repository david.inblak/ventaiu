package neto.sion.venta.servicios.cliente.dto;

import neto.sion.tienda.genericos.dto.Base;

/**
 * *
 * Clase que representa todos los datos necesarios para la reimpresion de un
 * ticket.
 *
 * @author Carlos V. Perez L.
 *
 */
public class ReimpresionTicketDto extends Base {

    private int paCdgError;
    private String paDescError;
    private long usuarioId;
    private String nombreUsuario;
    private String paFechaCreacion;
    private String paEstacionTrabajo;
    private long paMovimientoId;
    private ArticuloDto[] paArtReimpresiones;
    private TipoPagoDto[] paTPReimpresiones;

    public int getPaCdgError() {
        return paCdgError;
    }

    public void setPaCdgError(int paCdgError) {
        this.paCdgError = paCdgError;
    }

    public String getPaDescError() {
        return paDescError;
    }

    public void setPaDescError(String paDescError) {
        this.paDescError = paDescError;
    }

    public ArticuloDto[] getPaArtReimpresiones() {
        return paArtReimpresiones;
    }

    public void setPaArtReimpresiones(ArticuloDto[] paArtReimpresiones) {
        this.paArtReimpresiones = paArtReimpresiones;
    }

    public TipoPagoDto[] getPaTPReimpresiones() {
        return paTPReimpresiones;
    }

    public void setPaTPReimpresiones(TipoPagoDto[] paTPReimpresiones) {
        this.paTPReimpresiones = paTPReimpresiones;
    }

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getPaFechaCreacion() {
        return paFechaCreacion;
    }

    public void setPaFechaCreacion(String paFechaCreacion) {
        this.paFechaCreacion = paFechaCreacion;
    }

    public String getPaEstacionTrabajo() {
        return paEstacionTrabajo;
    }

    public void setPaEstacionTrabajo(String paEstacionTrabajo) {
        this.paEstacionTrabajo = paEstacionTrabajo;
    }

    public long getPaMovimientoId() {
        return paMovimientoId;
    }

    public void setPaMovimientoId(long paMovimientoId) {
        this.paMovimientoId = paMovimientoId;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    @Override
    public String toString() {
        return "ReimpresionTicketDto{" + "paCdgError=" + paCdgError + ", paDescError=" + paDescError + ", usuarioId=" + usuarioId + ", nombreUsuario=" + nombreUsuario + ", paFechaCreacion=" + paFechaCreacion + ", paEstacionTrabajo=" + paEstacionTrabajo + ", paMovimientoId=" + paMovimientoId + ", paArtReimpresiones=" + paArtReimpresiones + ", paTPReimpresiones=" + paTPReimpresiones + '}';
    }
}
