/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

import java.util.Arrays;

/**
 *
 * @author fvega
 */
public class RespuestaTarjetaDto {

    private TarjetaDto[] tarjetaDtos;
    private int cdgError;
    private String descError;

    public TarjetaDto[] getTarjetaDtos() {
        return tarjetaDtos;
    }

    public void setTarjetaDtos(TarjetaDto[] tarjetaDtos) {
        this.tarjetaDtos = tarjetaDtos;
    }

    public int getCdgError() {
        return cdgError;
    }

    public void setCdgError(int cdgError) {
        this.cdgError = cdgError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    @Override
    public String toString() {
        return "RespuestaTarjetaDto [tarjetaDtos="
                + Arrays.toString(tarjetaDtos) + ", cdgError=" + cdgError
                + ", descError=" + descError + "]";
    }
}
