package neto.sion.venta.servicios.cliente.dto;

public class RespuestaConsultaTransaccionTADto {

    private long numAutorizacion;
    private long numTransaccion;
    private int cdgError;
    private String descError;

    public long getNumAutorizacion() {
        return numAutorizacion;
    }

    public void setNumAutorizacion(long numAutorizacion) {
        this.numAutorizacion = numAutorizacion;
    }

    public int getCdgError() {
        return cdgError;
    }

    public void setCdgError(int cdgError) {
        this.cdgError = cdgError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    public long getNumTransaccion() {
        return numTransaccion;
    }

    public void setNumTransaccion(long numTransaccion) {
        this.numTransaccion = numTransaccion;
    }

    @Override
    public String toString() {
        return "RespuestaConsultaTransaccionTADto [numAutorizacion="
                + numAutorizacion + ", numTransaccion=" + numTransaccion
                + ", cdgError=" + cdgError + ", descError=" + descError + "]";
    }
}
