package neto.sion.venta.servicios.cliente.dto;

import neto.sion.tienda.genericos.dto.Base;

/**
 * *
 * Clase que representa un Bloqueo de venta.
 *
 * @author Carlos V. Perez L.
 *
 */
public class RespuestaBloqueoVentaDto extends Base {

    private BloqueoDto[] mensajesBloqueo;
    private int cdgError;
    private String descError;

    public BloqueoDto[] getMensajesBloqueo() {
        return mensajesBloqueo;
    }

    public void setMensajesBloqueo(BloqueoDto[] mensajesBloqueo) {
        this.mensajesBloqueo = mensajesBloqueo;
    }

    public int getCdgError() {
        return cdgError;
    }

    public void setCdgError(int cdgError) {
        this.cdgError = cdgError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    @Override
    public String toString() {
        return "RespuestaBloqueoVentaDto[mensajesBloqueo.length=" + (mensajesBloqueo == null ? null : mensajesBloqueo.length) + ",cdgError=" + cdgError + ",descError=" + descError + "]";
    }
}
