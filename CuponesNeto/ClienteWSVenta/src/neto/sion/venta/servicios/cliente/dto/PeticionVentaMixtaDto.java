package neto.sion.venta.servicios.cliente.dto;

import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;

public class PeticionVentaMixtaDto extends PeticionBaseVentaDto {

    private String paTerminal;
    private long paUsuarioId;
    private String paFechaOper;
    private int paTipoMovto;
    private double paMontoTotalVta;
    private ArticuloDto[] articulos;
    private TipoPagoDto[] tiposPago;
    private int fueraLinea;
    private PeticionVentaTADto[] recargasTA;

    public String getPaTerminal() {
        return paTerminal;
    }

    public void setPaTerminal(String paTerminal) {
        this.paTerminal = paTerminal;
    }

    public long getPaUsuarioId() {
        return paUsuarioId;
    }

    public void setPaUsuarioId(long paUsuarioId) {
        this.paUsuarioId = paUsuarioId;
    }

    public String getPaFechaOper() {
        return paFechaOper;
    }

    public void setPaFechaOper(String paFechaOper) {
        this.paFechaOper = paFechaOper;
    }

    public int getPaTipoMovto() {
        return paTipoMovto;
    }

    public void setPaTipoMovto(int paTipoMovto) {
        this.paTipoMovto = paTipoMovto;
    }

    public double getPaMontoTotalVta() {
        return paMontoTotalVta;
    }

    public void setPaMontoTotalVta(double paMontoTotalVta) {
        this.paMontoTotalVta = paMontoTotalVta;
    }

    public ArticuloDto[] getArticulos() {
        return articulos;
    }

    public void setArticulos(ArticuloDto[] articulos) {
        this.articulos = articulos;
    }

    public TipoPagoDto[] getTiposPago() {
        return tiposPago;
    }

    public void setTiposPago(TipoPagoDto[] tiposPago) {
        this.tiposPago = tiposPago;
    }

    public int getFueraLinea() {
        return fueraLinea;
    }

    public void setFueraLinea(int fueraLinea) {
        this.fueraLinea = fueraLinea;
    }

    public PeticionVentaTADto[] getRecargasTA() {
        return recargasTA;
    }

    public void setRecargasTA(PeticionVentaTADto[] recargasTA) {
        this.recargasTA = recargasTA;
    }

    @Override
    public String toString() {
        return "PeticionVentaMixtaDto{" + "paTerminal=" + paTerminal + ", paUsuarioId=" + paUsuarioId + ", paFechaOper=" + paFechaOper + ", paTipoMovto=" + paTipoMovto + ", paMontoTotalVta=" + paMontoTotalVta + ", articulos=" + articulos + ", tiposPago=" + tiposPago + ", fueraLinea=" + fueraLinea + ", recargasTA=" + recargasTA + '}';
    }
}
