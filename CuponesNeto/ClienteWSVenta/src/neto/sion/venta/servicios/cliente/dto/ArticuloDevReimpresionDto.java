/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvegap
 */
public class ArticuloDevReimpresionDto {

    private double precio;
    private double cantidad;
    private double iva;
    private int iepsId;
    private String nombre;

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public int getIepsId() {
        return iepsId;
    }

    public void setIepsId(int iepsId) {
        this.iepsId = iepsId;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "ArticuloDevReimpresionDto{" + "precio=" + precio + ", cantidad=" + cantidad + ", iva=" + iva + ", iepsId=" + iepsId + ", nombre=" + nombre + '}';
    }
}
