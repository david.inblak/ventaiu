package neto.sion.venta.servicios.cliente.dto;

import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;

/**
 * *
 * Clase que representa la peticion del detalle de una venta.
 *
 * @author Carlos V. Perez L.
 *
 */
public class PeticionDetalleVentaDto extends PeticionBaseVentaDto {

    private String paNumTransaccion;
    private int paParamTipoTrans;

    public int getPaParamTipoTrans() {
        return paParamTipoTrans;
    }

    public void setPaParamTipoTrans(int paParamTipoTrans) {
        this.paParamTipoTrans = paParamTipoTrans;
    }

    public String getPaNumTransaccion() {
        return paNumTransaccion;
    }

    public void setPaNumTransaccion(String paNumTransaccion) {
        this.paNumTransaccion = paNumTransaccion;
    }

    @Override
    public String toString() {
        return "PeticionDetalleVentaDto [paNumTransaccion=" + paNumTransaccion
                + ", paParamTipoTrans=" + paParamTipoTrans + super.toString()
                + "]";
    }
}
