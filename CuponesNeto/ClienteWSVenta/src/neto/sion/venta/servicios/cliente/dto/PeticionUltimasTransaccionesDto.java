/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvega
 */
public class PeticionUltimasTransaccionesDto {

    private int paPaisId;
    private long paTiendaId;

    public int getPaPaisId() {
        return paPaisId;
    }

    public void setPaPaisId(int paPaisId) {
        this.paPaisId = paPaisId;
    }

    public long getPaTiendaId() {
        return paTiendaId;
    }

    public void setPaTiendaId(long paTiendaId) {
        this.paTiendaId = paTiendaId;
    }

    @Override
    public String toString() {
        return "PeticionUltimasTransaccionesDto{" + "paPaisId=" + paPaisId + ", paTiendaId=" + paTiendaId + '}';
    }
}
