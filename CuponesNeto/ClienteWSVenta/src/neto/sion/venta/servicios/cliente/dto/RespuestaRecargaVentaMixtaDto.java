/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvega
 */
public class RespuestaRecargaVentaMixtaDto {

    private int codigoError;
    private String descError;
    private String autorizacion;

    public int getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    public String getAutorizacion() {
        return autorizacion;
    }

    public void setAutorizacion(String autorizacion) {
        this.autorizacion = autorizacion;
    }

    @Override
    public String toString() {
        return "RespuestaRecargaVentaMixtaBean [codigoError=" + codigoError
                + ", descError=" + descError + ", autorizacion=" + autorizacion
                + "]";
    }
}
