/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.redondeo.modelo;

/**
 *
 * @author dramirezr
 */
public class ModeloArticulo {
    Long id;
    int cantidad;
    String nombre;
    Double precio;
    Double importe;    
    Double total;
    
    Double costo;
    Double descuento;
    Double iva;
    //Double ieps;
    Double agranel;
    Integer idFamilia;
    Integer idIEPS;
    Integer unidad;
    Integer normaempaque;
    String codBarras;
    String codBarraspadre;
    byte[] imagen;

    public ModeloArticulo(){}

    public ModeloArticulo(
            Long id, String nombre, Double precio, Double total, Double costo, 
            Double descuento, Double iva, Double agranel, Integer idFamilia, 
            Integer idIEPS, Integer unidad, Integer normaempaque, String codBarras, 
            String codBarraspadre, byte[] imagen) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.total = total;
        this.costo = costo;
        this.descuento = descuento;
        this.iva = iva;
        //this.ieps = ieps;
        this.agranel = agranel;
        this.idFamilia = idFamilia;
        this.idIEPS = idIEPS;
        this.unidad = unidad;
        this.normaempaque = normaempaque;
        this.codBarras = codBarras;
        this.codBarraspadre = codBarraspadre;
        this.imagen = imagen;
        
        //this.setPrecioRegular(this.getPrecio()+this.getDescuento());
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
       
    public Double getAgranel() {
        return agranel;
    }

    public void setAgranel(Double agranel) {
        this.agranel = agranel;
    }

    public String getCodBarras() {
        return codBarras;
    }

    public void setCodBarras(String codBarras) {
        this.codBarras = codBarras;
    }
    public String getCodBarraspadre() {
        return codBarraspadre;
    }

    public void setCodBarraspadre(String codBarraspadre) {
        this.codBarraspadre = codBarraspadre;
    }

    public Double getCosto() {
        return costo;
    }

    public void setCosto(Double costo) {
        this.costo = costo;
    }

    public Double getDescuento() {
        return descuento;
    }

    public void setDescuento(Double descuento) {
        this.descuento = descuento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdFamilia() {
        return idFamilia;
    }

    public void setIdFamilia(Integer idFamilia) {
        this.idFamilia = idFamilia;
    }

    public Integer getIdIEPS() {
        return idIEPS;
    }

    public void setIdIEPS(Integer idIEPS) {
        this.idIEPS = idIEPS;
    }

    /*public Double getIeps() {
        return ieps;
    }

    public void setIeps(Double ieps) {
        this.ieps = ieps;
    }*/

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public Double getIva() {
        return iva;
    }

    public void setIva(Double iva) {
        this.iva = iva;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }

    
    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Integer getUnidad() {
        return unidad;
    }

    public void setUnidad(Integer unidad) {
        this.unidad = unidad;
    }

    public Integer getNormaempaque() {
        return normaempaque;
    }

    public void setNormaempaque(Integer normaempaque) {
        this.normaempaque = normaempaque;
    }

    @Override
    public String toString() {
        return "ModeloArticulo{" + "id=" + id + ", nombre=" + nombre + ", precio=" + precio + ", importe=" + importe + ", total=" + total + ", costo=" + costo + ", descuento=" + descuento + ", iva=" + iva + /*", ieps=" + ieps + */", agranel=" + agranel + ", idFamilia=" + idFamilia + ", idIEPS=" + idIEPS + ", unidad=" + unidad + ", normaempaque=" + normaempaque + ", codBarras=" + codBarras + ", codBarraspadre=" + codBarraspadre  + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ModeloArticulo other = (ModeloArticulo) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if ((this.codBarras == null) ? (other.codBarras != null) : !this.codBarras.equals(other.codBarras)) {
            return false;
        }
        return true;
    }
    
    public double getAhorro(){
        return getPrecio() - getImporte();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 67 * hash + (this.codBarras != null ? this.codBarras.hashCode() : 0);
        return hash;
    }

    
    
    
}
