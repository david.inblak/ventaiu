/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.redondeo;


import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.promociones.mvp.IComando;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesPresentador;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesRepositorio;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesVista;
import neto.sion.tienda.venta.promociones.redondeo.modelo.ModeloArticulo;
 
/**
 *
 * @author dramirezr
 */
public class PresentadorPromociones implements IPromocionesPresentador{
    public IPromocionesVista vista;
    public IPromocionesRepositorio repositorio;
    
    IComando tareaRechazado;
    IComando tareaAceptado;
    IComando tareaCerrar;
    
    private Set<ModeloArticulo> promocionesSeleccionadas;
    
    private Boolean esVentaCandidata = null;
    
    public PresentadorPromociones(IPromocionesVista _vista, IPromocionesRepositorio _modelo){
        this.vista = _vista;
        this.repositorio =_modelo;
        
        promocionesSeleccionadas = new HashSet<ModeloArticulo>();
    }
    
    @Override
    public void iniciarVista(){        
        vista.mostrarLoading();
        vista.cargarPromociones();
    }    
    

    @Override
    public boolean esVentaCandidata(Object venta) {
        SION.log(Modulo.VENTA, "esVentaCandidatoPromocion " , Level.INFO);
        return repositorio.esVentaCandidatoPromocion(venta);
    }
    
    @Override
    public Object getVistaPromociones(){
        return vista.obtenerVista();
    }

    @Override
    public void agregarPromocion(Object modelo) {
        ModeloArticulo modeloArt = (ModeloArticulo)modelo;
        promocionesSeleccionadas.removeAll(promocionesSeleccionadas);
        promocionesSeleccionadas.add(modeloArt);
        
        vista.desactivarPromociones(modeloArt);
        calculoCostoPromocionesSeleccionadas();
        SION.log(Modulo.VENTA, "Se selecciona artículo de la promocion :: " + modeloArt, Level.INFO);
    }

    @Override
    public void calculoCostoPromocionesSeleccionadas() {
        vista.mostrarCostoPromocionesSeleccionadas(promocionesSeleccionadas);
    }

    @Override
    public void aceptarPromociones() {
        SION.log(Modulo.VENTA, "Finalizar promociones (Aceptadas) "+promocionesSeleccionadas.size()+" Promociones seleccionadas.", Level.INFO);
        SION.log(Modulo.VENTA, "Importe recibido : "+vista.getImporteRecibido(), Level.INFO);
        if( this.tareaAceptado != null )
            this.tareaAceptado.executar(promocionesSeleccionadas, vista.getImporteRecibido());
    }
    
    @Override
    public void rechazarPromociones() {
        SION.log(Modulo.VENTA, "Finalizar promociones (Rechazadas) "+promocionesSeleccionadas.size()+" Promociones seleccionadas.", Level.INFO);
        if( this.tareaRechazado != null ) 
                this.tareaRechazado.executar(null, null);
    }
    
    @Override
    public void cerrarPromociones() {
        SION.log(Modulo.VENTA, "Cierra promociones (Escape) ", Level.INFO);
        if( this.tareaCerrar != null ) 
                this.tareaCerrar.executar();
    }

    @Override
    public void setComandoRechazarPromocion(IComando _tareaRechazado) {
        this.tareaRechazado = _tareaRechazado;
    }

    @Override
    public void setComandoAceptarPromocion(IComando _tareaAceptado) {
        this.tareaAceptado = _tareaAceptado;
    }
    
    @Override
    public void setComandoCerrarPromocion(IComando _tareaCerrar) {
        this.tareaCerrar = _tareaCerrar;
    }
    
    
}
