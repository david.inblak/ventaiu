
package neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto;

import neto.sion.clientews.tarjeta.dto.RespuestaPagoTarjetaDto;
import neto.sion.venta.servicios.cliente.dto.RespuestaVentaDto;


public class RespuestaCombinadaVentaDto {
    private neto.sion.venta.servicios.cliente.dto.RespuestaVentaDto respuestaVentaNormal;
    private neto.sion.clientews.tarjeta.dto.RespuestaPagoTarjetaDto respuestaVentaTarjeta;
    private RespuestaPagatodoDto respuestaTarjetaPTDto;

    public RespuestaVentaDto getRespuestaVentaNormal() {
        return respuestaVentaNormal;
    }

    public void setRespuestaVentaNormal(RespuestaVentaDto respuestaVentaNormal) {
        this.respuestaVentaNormal = respuestaVentaNormal;
    }

    public RespuestaPagoTarjetaDto getRespuestaVentaTarjeta() {
        return respuestaVentaTarjeta;
    }

    public void setRespuestaVentaTarjeta(RespuestaPagoTarjetaDto respuestaVentaTarjeta) {
        this.respuestaVentaTarjeta = respuestaVentaTarjeta;
    }

	public RespuestaPagatodoDto getRespuestaTarjetaPTDto() {
		return respuestaTarjetaPTDto;
	}

	public void setRespuestaTarjetaPTDto(RespuestaPagatodoDto respuestaTarjetaPTDto) {
		this.respuestaTarjetaPTDto = respuestaTarjetaPTDto;
	}
    
    
}
