package neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.facade;

import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto.RespuestaCombinadaVentaDto;
import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto.RespuestaPagatodoDto;
import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto.PeticionPagatodoNipDto;
import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto.PeticionTransaccionCentralDto;
import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto.WSException;

public interface ConsumirTarjetaPT_ISO8583NIP {
	public RespuestaPagatodoDto consultaSaldoPTdo ( 
            PeticionPagatodoNipDto _peticionConSaldo) throws WSException;

public RespuestaCombinadaVentaDto registrarVentaTarjetaPTdo(
		neto.sion.tienda.venta.promociones.cupones.modelo.pt.PeticionVentaBean _peticionVenta,
		neto.sion.tienda.venta.promociones.cupones.modelo.finventa.FinalizaVentaCuponesBeanRequest _peticionVentaN, 
		neto.sion.tienda.venta.promociones.cupones.modelo.pt.PeticionPagatodoNipBean _requestPT) throws WSException;

public RespuestaCombinadaVentaDto registrarVentaTarjetaPagaTodo(
            neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto _peticionVenta, 
            neto.sion.tienda.venta.promociones.cupones.modelo.finventa.FinalizaVentaCuponesBeanRequest _peticionVentaN,
            PeticionPagatodoNipDto _requestPT)
                            throws WSException;

public long consultarTransaccionCentral(
	PeticionTransaccionCentralDto _peticion)
			throws WSException;

}
