package neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc;

import neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto;
import neto.sion.tienda.venta.cupones.util.ConversionBeanes;
import neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp.FinalizaVentaCuponesPTBeanResp;
import neto.sion.venta.servicios.cliente.dto.ArticuloDto;
import neto.sion.venta.servicios.cliente.dto.PeticionVentaDto;
import neto.sion.venta.servicios.cliente.dto.TipoPagoDto;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.TipoPagoBean;
import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto.PeticionPagatodoNipDto;
import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.facade.ConsumirTarjetaPT_ISO8583NIP;
import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.facade.ConsumirTarjetaPT_ISO8583NIPImp;

public class Test {

	public static void main(String[] args) {
            try{
              FinalizaVentaCuponesPTBeanResp  finVtaCupResponse = 
	    	    		(neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp.FinalizaVentaCuponesPTBeanResp)
	    	    		ConversionBeanes.convertir(
                                "{"
                                    + "\"respuestaVentaNormal\":{\"paTransaccionId\":0,"
                                    + "\"paCdgError\":300,"
                                    + "\"paDescError\":\"Ocurrio un error con el o los cupones no se pudieron redimir.\","
                                    + "\"paTaNumOperacion\":0}}\"", 
                                    neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp.FinalizaVentaCuponesPTBeanResp.class);
              
                System.out.println(" >> " + finVtaCupResponse.toString());
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
            
            if(true)
                return;
            
		try {
			ConsumirTarjetaPT_ISO8583NIP clientePagatodoNip = new ConsumirTarjetaPT_ISO8583NIPImp();
			
			PeticionPagoTarjetaDto _peticionVenta = new PeticionPagoTarjetaDto();
			PeticionVentaDto _peticionVentaN = new PeticionVentaDto();
			PeticionPagatodoNipDto _requestPT = new PeticionPagatodoNipDto();
			
			
			
			
			_peticionVentaN.setPaTerminal("10.2.0.139|CAJA5");
			_peticionVentaN.setPaPaisId(1);
			_peticionVentaN.setTiendaId(3725);
			_peticionVentaN.setPaUsuarioId(152886);
			_peticionVentaN.setPaFechaOper("13/06/2019 15:29:10");
			_peticionVentaN.setPaTipoMovto(1);
			_peticionVentaN.setPaMontoTotalVta(2.0);
			
			ArticuloDto[] arts = new ArticuloDto[1];
			arts[0] = new ArticuloDto();
			arts[0].setFiArticuloId(1020003432);
			arts[0].setFcCdGbBarras("75051884");
			arts[0].setFnCantidad(1.0);
			arts[0].setFnPrecio(2.0);
			arts[0].setFnCosto(0.7);
			arts[0].setFnDescuento(0.0);
			arts[0].setFnIva(0.16);
			arts[0].setFcNombreArticulo("CHICLE CLORETS 4S 1 PZ");
			arts[0].setFiAgranel(0.0);
			arts[0].setFnIepsId(69);
			_peticionVentaN.setArticulos(arts);
			
			
			TipoPagoDto[] tiposPago = new TipoPagoDto[2];
			tiposPago[0] = new TipoPagoDto();
			tiposPago[0].setFiTipoPagoId(2);
			tiposPago[0].setFnMontoPago(1.0);
			tiposPago[0].setFnNumeroVales(0);
			tiposPago[0].setPaPagoTarjetaIdBus(0);
			tiposPago[0].setImporteAdicional(0);
			tiposPago[0].setFiMontoRecibido(0.0);
			tiposPago[1] = new TipoPagoDto();
			tiposPago[1].setFiTipoPagoId(1);
			tiposPago[1].setFnMontoPago(1.0);
			tiposPago[1].setFnNumeroVales(0);
			tiposPago[1].setPaPagoTarjetaIdBus(0);
			tiposPago[1].setImporteAdicional(1.0);
			tiposPago[1].setFiMontoRecibido(0.0);
			_peticionVentaN.setTiposPago(tiposPago);
			_peticionVentaN.setPaConciliacionId(new Long("4396320190631"));
			_peticionVentaN.setFueraLinea(0);
			
			
			
			_peticionVenta.setPaPaisId(1);
			_peticionVenta.setC55("N/A");
			_peticionVenta.setC63("PROSA");
			_peticionVenta.setComision(0.0);
			_peticionVenta.setChip(false);
			_peticionVenta.setFalloLecturaChip(false);
			_peticionVenta.setMonto(1.0);
			_peticionVenta.setNumTerminal("05");
			_peticionVenta.setTrack1("6395091390018645 /VALE ELECTRONICO        26055501000000000000000613000000");
			_peticionVenta.setTrack2("6395091390018645=260550100000613");
			_peticionVenta.setCanalPago("01");
			
			
			
			
			_requestPT.setIdSucursal("3725");
			_requestPT.setIdTerminal("139");
			_requestPT.setIdOperador("152886");
			_requestPT.setNoTicket("0");
			_requestPT.setReferencia("6395091390018645=260550100000613");
			_requestPT.setMonto("1.00");
			_requestPT.setTipoTarjetaId(2);
			_requestPT.setNip("1234");
			
			
			//System.out.println(clientePagatodoNip.registrarVentaTarjetaPagaTodo(_peticionVenta, _peticionVentaN, _requestPT)); 
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		

	}

}
