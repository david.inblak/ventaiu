/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author dramirezr
 */
public class CuponesDto {
        @JsonProperty("codigo")         private String codigo;
        @JsonProperty("codigoOferta")   private int codigoOferta;
	@JsonProperty("informacionAdicional") private String informacionAdicional;
	@JsonProperty("logoCadena")     private String logoCadena;
	@JsonProperty("logoProducto")   private String logoProducto;
	@JsonProperty("lineas")         private LineasDto[] lineas;
        @JsonProperty("origen")         private int origen;
	@JsonProperty("vecesAImprimir") private int vecesAImprimir;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

        
    public int getCodigoOferta() {
        return codigoOferta;
    }

    public void setCodigoOferta(int codigoOferta) {
        this.codigoOferta = codigoOferta;
    }

    public String getInformacionAdicional() {
        return informacionAdicional;
    }

    public void setInformacionAdicional(String informacionAdicional) {
        this.informacionAdicional = informacionAdicional;
    }

    public LineasDto[] getLineas() {
        return lineas;
    }

    public void setLineas(LineasDto[] lineas) {
        this.lineas = lineas;
    }

    public String getLogoCadena() {
        return logoCadena;
    }

    public void setLogoCadena(String logoCadena) {
        this.logoCadena = logoCadena;
    }

    public String getLogoProducto() {
        return logoProducto;
    }

    public void setLogoProducto(String logoProducto) {
        this.logoProducto = logoProducto;
    }
    
    public int getOrigen() {
        return origen;
    }

    public void setOrigen(int origen) {
        this.origen = origen;
    }

    public int getVecesAImprimir() {
        return vecesAImprimir;
    }

    public void setVecesAImprimir(int vecesAImprimir) {
        this.vecesAImprimir = vecesAImprimir;
    }

    @Override
    public String toString() {
        return "CuponesDto{" + "codigoOferta=" + codigoOferta + ", informacionAdicional=" + informacionAdicional + ", logoCadena=" + logoCadena + ", logoProducto=" + logoProducto + ", lineas=" + lineas + ", vecesAImprimir=" + vecesAImprimir + ", origen=" + origen + '}';
    }
        
        
}
