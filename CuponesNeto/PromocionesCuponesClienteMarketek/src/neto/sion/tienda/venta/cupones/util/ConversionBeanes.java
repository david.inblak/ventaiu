/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.cupones.util;

import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author dramirezr
 */
public class ConversionBeanes {
    public static final ObjectMapper mapper;
    static{
        mapper = new ObjectMapper();
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
    
    public static Object convertir(Object dato, Class ATipo) throws Exception{
        /*SION.log(Modulo.VENTA, "Se intenta convertir del tipo: \n"
                + dato.getClass()+ "\n al tipo "
                + ":" + ATipo, Level.INFO);*/
        if( ATipo == String.class ){
            String res = mapper.writeValueAsString(dato);
            ///System.out.println("objGenerado > "+res);
            return res;
        }else if( dato.getClass() == String.class ){
            Object res = mapper.readValue(dato.toString(), ATipo);
            ///System.out.println("objGenerado > "+res);
            return res;
        }else{
            String strDato = mapper.writeValueAsString(dato);
            //System.out.println("Conversion de "+dato.getClass().getName() +" a "+ ATipo.getName());
            ///System.out.println("strDato > "+strDato);
            Object res = mapper.readValue(strDato, ATipo);
            ///System.out.println("objGenerado > "+res);
            return res;
        }
    }

}
