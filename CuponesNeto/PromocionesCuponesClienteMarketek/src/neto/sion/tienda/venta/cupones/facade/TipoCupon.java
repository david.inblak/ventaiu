/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.cupones.facade;

/**
 *
 * @author dramirezr
 */
public enum TipoCupon {
    Marketec(0, "MARKETEC"), Neto(1, "NETO"), MarketecNeto(3, "MARKETEC-NETO");
    private int id;
    private String nombre;
    
    private TipoCupon(int id, String nombre){
        this.id = id;
        this.nombre = nombre;
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public static TipoCupon convierteIntTipoCupon(int ordinal){
        if( 0 == ordinal) return TipoCupon.Marketec;
        if( 1 == ordinal) return TipoCupon.Neto;
        if( 3 == ordinal) return TipoCupon.MarketecNeto;
        return null;
    }
    
    @Override
    public String toString(){
        return id + " - " +nombre;
    }
}
