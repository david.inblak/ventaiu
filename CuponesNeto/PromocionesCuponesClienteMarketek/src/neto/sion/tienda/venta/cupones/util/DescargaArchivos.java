/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.cupones.util;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.facade.ClienteCupones;
import neto.sion.tienda.venta.cupones.facade.ClienteCuponesMarketec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author dramirezr
 */
public class DescargaArchivos {
    public static final int CODIGO_EXITO = 0;
    //private static ClienteCuponesMarketec cli = null;
    
    public static String verificarExistenciaArchivo(ClienteCupones cli, String _nombreArchivo){
        
        if( _nombreArchivo == null || "".equals(_nombreArchivo))
            return SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPON.IMAGENES.BLANCO");
        
        String [] nombreArchivo = _nombreArchivo.split("[\\/\\.]");
        
        String fileName = nombreArchivo[nombreArchivo.length-2];
        String fileExt = nombreArchivo[nombreArchivo.length-1];
        
        String ubicacionImagenesCupones = SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPON.IMAGENES.UBICACION");
        
        SION.log(Modulo.VENTA, "validando el archivo >> "+_nombreArchivo, Level.INFO);
        SION.log(Modulo.VENTA, "directorio: "+ubicacionImagenesCupones+", archivo: "+fileName+", extension: "+fileExt, Level.INFO);
        
        File file = new File(ubicacionImagenesCupones + fileName+"."+ fileExt);
        //Buscar el archivo en la ubicacion
        if( !file.exists() ){
            SION.log(Modulo.VENTA, "Archivo ["+file.getAbsolutePath()+"] no existe se descargará desde central...", Level.INFO);
            try {                
                SION.log(Modulo.VENTA, "Instanciando cliente...", Level.INFO);
                
                SION.log(Modulo.VENTA, "Consumiendo metodo de descarga de imagen...", Level.INFO);
                byte[] bytes = cli.desgargaImagen( fileName+"."+ fileExt);
                if( bytes != null ){        
                    SION.log(Modulo.VENTA, "Escribiendo archivo...", Level.INFO);
                    FileOutputStream fos = new FileOutputStream(ubicacionImagenesCupones + fileName + "." +fileExt);
                    fos.write(bytes);
                    fos.close();
                    SION.log(Modulo.VENTA, "Se descargó archivo de central.", Level.INFO);
                }else{
                    SION.log(Modulo.VENTA, "El archivo descargado contiene 0 bytes", Level.INFO);
                    fileName = "blanco";
                    fileExt = "png";
                }
            } catch (IOException ex) {
                SION.logearExcepcion(Modulo.VENTA, ex, "Error al descargar el archivo.");
                return null;
            }catch (Exception ex) {
                SION.logearExcepcion(Modulo.VENTA, ex, "Error al descargar el archivo.");
                return null;
            }catch (Error ex) {
                SION.logearExcepcion(Modulo.VENTA, new Exception(ex), "Error al descargar el archivo.");
                return null;
            }finally{
                SION.log(Modulo.VENTA, "Termina descarga de imagen desde central...", Level.INFO);
            }
        }else{
            SION.log(Modulo.VENTA, "Archivo ["+file.getAbsolutePath()+" :: "+file.length()+"] ya existe...", Level.INFO);
        }
         return fileName + "." + fileExt ;
    }
    
    public static void main(String[] args) throws MalformedURLException, IOException {
        DescargaArchivos.verificarExistenciaArchivo(new ClienteCuponesMarketec(), "LogMkt.bmp");
    }
}
