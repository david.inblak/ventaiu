/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.cupones.facade;

/**
 *
 * @author dramirezr
 */
public class ClienteCuponesFactory {
    private static ClienteCupones marketec;
    private static ClienteCupones neto;
    private static ClienteCupones marketecneto;
    
    static{
        marketec= new ClienteCuponesMarketec();
        neto= new ClienteCuponesNeto();
        marketecneto = new ClienteCuponesNetoMarketec();
    }
    
    public static ClienteCupones getClienteCupones(TipoCupon tipo){
        ClienteCupones respuesta=null;
        switch(tipo){
            case Marketec: 
                respuesta=marketec;
                break;
            case Neto: 
                respuesta= neto;
                break;
            case MarketecNeto: 
                respuesta= marketecneto;
                break;
        }
        return respuesta;
    }
}
