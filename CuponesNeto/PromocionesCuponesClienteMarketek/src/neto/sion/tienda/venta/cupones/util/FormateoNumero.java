/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.cupones.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;

/**
 *
 * @author dramirezr
 */
public class FormateoNumero {
    public static Locale local = new Locale("es", "MX");
    public static final String PATRON_DOS_DECIMALES = "#.00";
    public static final String PATRON_TRES_DECIMALES = "#.000";
    
    public static String porPatron(Double dato){
        return porPatron(null, dato);
    }
    
    public static String porPatron(String patron, Double dato){
        String cadena = "";

        try {
            NumberFormat nf = NumberFormat.getNumberInstance(local);
            DecimalFormat df = (DecimalFormat) nf;
            if(patron!=null)
                df.applyPattern(patron);
            cadena = df.format(dato);
        } catch (NumberFormatException e) {
            SION.logearExcepcion(Modulo.VENTA, e);
        }

        return cadena;
    }
}
