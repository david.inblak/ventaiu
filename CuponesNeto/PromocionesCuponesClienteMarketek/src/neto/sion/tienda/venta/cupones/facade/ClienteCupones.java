/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.cupones.facade;

/**
 *
 * @author dramirezr
 */
public interface ClienteCupones {

    Object calculaDescuentoCupones(Object solicitud) throws Exception;

    Object cancelarVenta(Object solicitud) throws Exception;

    byte[] desgargaImagen(String nombreImagen) throws Exception;

    Object finalizaVenta(Object solicitud) throws Exception;

    Object validaDesceuntosAutomaticos(Object solicitud) throws Exception;
    
}
