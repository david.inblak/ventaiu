package neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp;

import java.io.Serializable;
import java.util.Arrays;

import org.codehaus.jackson.annotate.JsonProperty;

public class CuponesBean implements Serializable{

	private static final long serialVersionUID = 5543529686367009717L;
	
	@JsonProperty("codigo") private String codigo;
	@JsonProperty("codigoOferta") private int codigoOferta;
	@JsonProperty("informacionAdicional") private String informacionAdicional;
	@JsonProperty("logoCadena") private String logoCadena;
	@JsonProperty("logoProducto") private String logoProducto;
	@JsonProperty("lineas") private LineasBean[] lineas;
        @JsonProperty("origen") private int origen;
	@JsonProperty("vecesAImprimir") private int vecesAImprimir;

        public String getCodigo() {
            return codigo;
        }

        public void setCodigo(String codigo) {
            this.codigo = codigo;
        }
	
	
	public int getCodigoOferta() {
		return codigoOferta;
	}
	public void setCodigoOferta(int codigoOferta) {
		this.codigoOferta = codigoOferta;
	}
	public String getInformacionAdicional() {
		return informacionAdicional;
	}
	public void setInformacionAdicional(String informacionAdicional) {
		this.informacionAdicional = informacionAdicional;
	}
	public String getLogoCadena() {
		return logoCadena;
	}
	public void setLogoCadena(String logoCadena) {
		this.logoCadena = logoCadena;
	}
	public String getLogoProducto() {
		return logoProducto;
	}
	public void setLogoProducto(String logoProducto) {
		this.logoProducto = logoProducto;
	}
	public LineasBean[] getLineas() {
		return lineas;
	}
	public void setLineas(LineasBean[] lineas) {
		this.lineas = lineas;
	}
                public int getOrigen() {
            return origen;
        }

        public void setOrigen(int origen) {
            this.origen = origen;
        }
	
	public int getVecesAImprimir() {
		return vecesAImprimir;
	}
	public void setVecesAImprimir(int vecesAImprimir) {
		this.vecesAImprimir = vecesAImprimir;
	}
	@Override
	public String toString() {
		return "CuponesBean :[codigoOferta=" + codigoOferta
							 + ", informacionAdicional=" + informacionAdicional 
							 + ", logoCadena=" + logoCadena 
							 + ", logoProducto=" + logoProducto
							 + ", vecesAImprimir=" + vecesAImprimir
							 + ", lineas=" + Arrays.toString(lineas)+ "]";
	}
	

}
