package neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp;

import java.io.Serializable;




public class RespuestaPagatodoNipBean implements Serializable {
	final String CODIGO_EXITO = "00";
	final String CODIGO_ERROR = "-2";
	
	private static final long serialVersionUID = 1L;
	private String rCodigoRespuesta;
	private String rDescRespuesta;
	private String rDescMsgObligatorio;
	private String rIdGrupo;
	private String rIdSucursal;
	private String rIdTerminal;
	private String rIdOperador;
	private String rNoTicket;
	private String rReferencia;
	private String rNoConfirmacion;
	private String rNoSecUnicoPT;
	private String rNoMsgObligatorio;
	private String rNoMsgPromocion;
	private String rSaldoDisponibleTarjeta;
	private String rMontoAbonado;
	private String rS110;
	
	public RespuestaPagatodoNipBean(){
		super();
	}
	

	
	public String getrCodigoRespuesta() {
		return rCodigoRespuesta;
	}
	public void setrCodigoRespuesta(String rCodigoRespuesta) {
		this.rCodigoRespuesta = rCodigoRespuesta;
	}
	public String getrDescRespuesta() {
		return rDescRespuesta;
	}
	public void setrDescRespuesta(String rDescRespuesta) {
		this.rDescRespuesta = rDescRespuesta;
	}
	public String getrDescMsgObligatorio() {
		return rDescMsgObligatorio;
	}
	public void setrDescMsgObligatorio(String rDescMsgObligatorio) {
		this.rDescMsgObligatorio = rDescMsgObligatorio;
	}
	public String getrIdGrupo() {
		return rIdGrupo;
	}
	public void setrIdGrupo(String rIdGrupo) {
		this.rIdGrupo = rIdGrupo;
	}
	public String getrIdSucursal() {
		return rIdSucursal;
	}
	public void setrIdSucursal(String rIdSucursal) {
		this.rIdSucursal = rIdSucursal;
	}
	public String getrIdTerminal() {
		return rIdTerminal;
	}
	public void setrIdTerminal(String rIdTerminal) {
		this.rIdTerminal = rIdTerminal;
	}
	public String getrIdOperador() {
		return rIdOperador;
	}
	public void setrIdOperador(String rIdOperador) {
		this.rIdOperador = rIdOperador;
	}
	public String getrNoTicket() {
		return rNoTicket;
	}
	public void setrNoTicket(String rNoTicket) {
		this.rNoTicket = rNoTicket;
	}
	public String getrReferencia() {
		return rReferencia;
	}
	public void setrReferencia(String rReferencia) {
		this.rReferencia = rReferencia;
	}
	public String getrNoConfirmacion() {
		return rNoConfirmacion;
	}
	public void setrNoConfirmacion(String rNoConfirmacion) {
		this.rNoConfirmacion = rNoConfirmacion;
	}
	public String getrNoSecUnicoPT() {
		return rNoSecUnicoPT;
	}
	public void setrNoSecUnicoPT(String rNoSecUnicoPT) {
		this.rNoSecUnicoPT = rNoSecUnicoPT;
	}
	public String getrNoMsgObligatorio() {
		return rNoMsgObligatorio;
	}
	public void setrNoMsgObligatorio(String rNoMsgObligatorio) {
		this.rNoMsgObligatorio = rNoMsgObligatorio;
	}
	public String getrNoMsgPromocion() {
		return rNoMsgPromocion;
	}
	public void setrNoMsgPromocion(String rNoMsgPromocion) {
		this.rNoMsgPromocion = rNoMsgPromocion;
	}
	public String getrSaldoDisponibleTarjeta() {
		return rSaldoDisponibleTarjeta;
	}
	public void setrSaldoDisponibleTarjeta(String rSaldoDisponibleTarjeta) {
		this.rSaldoDisponibleTarjeta = rSaldoDisponibleTarjeta;
	}
	public String getrMontoAbonado() {
		return rMontoAbonado;
	}
	public void setrMontoAbonado(String rMontoAbonado) {
		this.rMontoAbonado = rMontoAbonado;
	}
	public String getrS110() {
		return rS110;
	}
	public void setrS110(String rS110) {
		this.rS110 = rS110;
	}

	@Override
	public String toString() {
		return "RespuestaPagatodoBean [rCodigoRespuesta=" + rCodigoRespuesta
				+ ", rDescRespuesta=" + rDescRespuesta
				+ ", rDescMsgObligatorio=" + rDescMsgObligatorio
				+ ", rIdGrupo=" + rIdGrupo + ", rIdSucursal=" + rIdSucursal
				+ ", rIdTerminal=" + rIdTerminal + ", rIdOperador="
				+ rIdOperador + ", rNoTicket=" + rNoTicket + ", rReferencia="
				+ rReferencia + ", rNoConfirmacion=" + rNoConfirmacion
				+ ", rNoSecUnicoPT=" + rNoSecUnicoPT + ", rNoMsgObligatorio="
				+ rNoMsgObligatorio + ", rNoMsgPromocion=" + rNoMsgPromocion
				+ ", rSaldoDisponibleTarjeta=" + rSaldoDisponibleTarjeta
				+ ", rMontoAbonado=" + rMontoAbonado + ", rS110=" + rS110 + "]";
	}

	public boolean isEsPagoExitoso() {
		return CODIGO_EXITO.equals(getrCodigoRespuesta());
	}
	public boolean isEsPagoError() {
		return CODIGO_ERROR.equals(getrCodigoRespuesta());
	}
	
}
