package neto.sion.tienda.venta.promociones.cupones.modelo.finventa;

import org.codehaus.jackson.annotate.JsonProperty;

/***
 * Clase que representa un tipo de pago.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class TipoPagoBean {
        @JsonProperty("fiTipoPagoId") 
	private int fiTipoPagoId;
        @JsonProperty("fnMontoPago")
	private double fnMontoPago;
        @JsonProperty("fnNumeroVales")
	private int fnNumeroVales;
        @JsonProperty("paPagoTarjetaIdBus")
	private long paPagoTarjetaIdBus;
        @JsonProperty("importeAdicional")
	private double importeAdicional;
        @JsonProperty("esPagaTodo")
	private boolean esPagaTodo;
        

	public double getImporteAdicional() {
		return importeAdicional;
	}

	public void setImporteAdicional(double importeAdicional) {
		this.importeAdicional = importeAdicional;
	}

	public long getPaPagoTarjetaIdBus() {
		return paPagoTarjetaIdBus;
	}

	public void setPaPagoTarjetaIdBus(long paPagoTarjetaIdBus) {
		this.paPagoTarjetaIdBus = paPagoTarjetaIdBus;
	}

	public int getFiTipoPagoId() {
		return fiTipoPagoId;
	}

	public void setFiTipoPagoId(int fiTipoPagoId) {
		this.fiTipoPagoId = fiTipoPagoId;
	}

	public double getFnMontoPago() {
		return fnMontoPago;
	}

	public void setFnMontoPago(double fnMontoPago) {
		this.fnMontoPago = fnMontoPago;
	}

	public int getFnNumeroVales() {
		return fnNumeroVales;
	}

	public void setFnNumeroVales(int fnNumeroVales) {
		this.fnNumeroVales = fnNumeroVales;
	}
        
        public boolean isEsPagaTodo() {
            return esPagaTodo;
	}

	public void setEsPagaTodo(boolean esPagaTodo) {
		this.esPagaTodo = esPagaTodo;
	}

    @Override
    public String toString() {
        return "TipoPagoBean{" + "fiTipoPagoId=" + fiTipoPagoId + ", fnMontoPago=" + fnMontoPago + ", fnNumeroVales=" + fnNumeroVales + ", paPagoTarjetaIdBus=" + paPagoTarjetaIdBus + ", importeAdicional=" + importeAdicional + ", esPagaTodo=" + esPagaTodo + '}';
    }

	

}
