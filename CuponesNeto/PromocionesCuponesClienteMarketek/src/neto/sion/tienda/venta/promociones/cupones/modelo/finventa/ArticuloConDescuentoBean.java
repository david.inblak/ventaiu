/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.finventa;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "cantidad",
    "articuloId",
    "codigoBarras",
    "cupon",
    "descripcionOferta",
    "montoDesc",
    "puntosCanje"
})
public class ArticuloConDescuentoBean {
	
    @JsonProperty("cantidad")
    private float cantidad;
    @JsonProperty("articuloId")
    private long articuloId;
    @JsonProperty("codigoBarras")
    private String codigoBarras;
    @JsonProperty("cupon")
    private long cupon;
    @JsonProperty("descripcionOferta")
    private String descripcionOferta;
    @JsonProperty("montoDesc")
    private float montoDesc;
    @JsonProperty("puntosCanje")
    private String puntosCanje;
    @JsonProperty("cantidad")
    public float getCantidad() {
        return cantidad;
    }

    @JsonProperty("cantidad")
    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }
    public long getArticuloId() {
	return articuloId;
    }
    public void setArticuloId(long articuloId) {
            this.articuloId = articuloId;
    }
    @JsonProperty("codigoBarras")
    public String getCodigoBarras() {
        return codigoBarras;
    }
    @JsonProperty("codigoBarras")
    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }
    @JsonProperty("cupon")
    public long getCupon() {
        return cupon;
    }

    @JsonProperty("cupon")
    public void setCupon(long cupon) {
        this.cupon = cupon;
    }

    @JsonProperty("descripcionOferta")
    public String getDescripcionOferta() {
        return descripcionOferta;
    }

    @JsonProperty("descripcionOferta")
    public void setDescripcionOferta(String descripcionOferta) {
        this.descripcionOferta = descripcionOferta;
    }

    @JsonProperty("montoDesc")
    public float getMontoDesc() {
        return montoDesc;
    }

    @JsonProperty("montoDesc")
    public void setMontoDesc(float montoDesc) {
        this.montoDesc = montoDesc;
    }

    @JsonProperty("puntosCanje")
    public String getPuntosCanje() {
        return puntosCanje;
    }

    @JsonProperty("puntosCanje")
    public void setPuntosCanje(String puntosCanje) {
        this.puntosCanje = puntosCanje;
    }

    @Override
    public String toString() {
        return new StringBuilder().
                append("cantidad:").append(cantidad).
                append(",articuloId:").append(articuloId).
                append(",codigoBarras:").append(codigoBarras).
                append(",cupon:").append(cupon).
                append(",descripcionOferta:").append(descripcionOferta).
                append(",montoDesc:").append(montoDesc).
                append(",puntosCanje:").append(puntosCanje).toString();
    }
}