package neto.sion.tienda.venta.promociones.cupones.modelo.pt;

import neto.sion.tienda.venta.promociones.cupones.modelo.finventa.FinalizaVentaCuponesBeanRequest;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "_peticionVenta",
    "finalizaVentaMktcReq",
    "_requestPT"
})


public class PeticionVentaPTMktc {
	
	
	
	@JsonProperty("_peticionVenta") PeticionVentaBean _peticionVenta; 
	@JsonProperty("finalizaVentaMktcReq") FinalizaVentaCuponesBeanRequest finalizaVentaMktcReq;
	@JsonProperty("_requestPT") PeticionPagatodoNipBean _requestPT;
	
	
	public PeticionVentaBean get_peticionVenta() {
		return _peticionVenta;
	}
	public void set_peticionVenta(PeticionVentaBean _peticionVenta) {
		this._peticionVenta = _peticionVenta;
	}
	public FinalizaVentaCuponesBeanRequest getFinalizaVentaMktcReq() {
		return finalizaVentaMktcReq;
	}
	public void setFinalizaVentaMktcReq(FinalizaVentaCuponesBeanRequest finalizaVentaMktcReq) {
		this.finalizaVentaMktcReq = finalizaVentaMktcReq;
	}
	public PeticionPagatodoNipBean get_requestPT() {
		return _requestPT;
	}
	public void set_requestPT(PeticionPagatodoNipBean _requestPT) {
		this._requestPT = _requestPT;
	}
	

	

}
