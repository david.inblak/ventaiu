package neto.sion.tienda.venta.promociones.cupones.modelo.finventa;

import java.io.Serializable;
import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;

public class FinTicketMktcBeanReq implements Serializable{
	
	/*@JsonProperty("pedido") private long pedido;//vacio
	@JsonProperty("email") private String email;//vacio
	@JsonProperty("cadena") private long cadena;//vacio*/
	@JsonProperty("pos") private int pos;//NoCaja
	@JsonProperty("sucursal") private int sucursal;
        /*** El consecutivo del idConciliación más el numero de intento*/
	@JsonProperty("ticket") private String ticket;//idConciliacion 
        @JsonProperty("ticketAnterior") private String ticketAnterior; 
        /*** Solo el consecutivo del idConciliación */
	@JsonProperty("ticketOriginal") private String ticketOriginal;
        /*** idConciliación */
        @JsonProperty("ticketCompleto") private String ticketCompleto;
	@JsonProperty("confirmaCupon") private List<ConfirmaCuponBean> confirmaCupon;
	/*@JsonProperty("tipoDePago") private TipoDePagoMktcBean[] tipoDePago;//No requerido
	@JsonProperty("puntosSuma") private String puntosSuma;//vacio
	@JsonProperty("puntosCanje") private int puntosCanje;//vacio*/
	private int cajeroId;

    public int getCajeroId() {
        return cajeroId;
    }

    public void setCajeroId(int cajeroId) {
        this.cajeroId = cajeroId;
    }
        
        
	
	/*
	public long getPedido() {
		return pedido;
	}
	public void setPedido(long pedido) {
		this.pedido = pedido;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getCadena() {
		return cadena;
	}
	public void setCadena(long cadena) {
		this.cadena = cadena;
	}*/
	public long getPos() {
		return pos;
	}
	public void setPos(int pos) {
		this.pos = pos;
	}
	public long getSucursal() {
		return sucursal;
	}
	public void setSucursal(int sucursal) {
		this.sucursal = sucursal;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
            this.ticket = ticket;
	}

    public String getTicketAnterior() {
        return ticketAnterior;
    }

    public void setTicketAnterior(String ticketAnterior) {
        this.ticketAnterior = ticketAnterior;
    }
        
        
	public List<ConfirmaCuponBean> getConfirmaCupon() {
		return this.confirmaCupon;
	}
	public void setConfirmaCupon(List<ConfirmaCuponBean> confirmaCupon) {
		this.confirmaCupon = confirmaCupon;
	}
	/*public TipoDePagoMktcBean[] getTipoDePago() {
		return tipoDePago;
	}
	public void setTipoDePago(TipoDePagoMktcBean[] tipoDePago) {
		this.tipoDePago = tipoDePago;
	}
	public String getPuntosSuma() {
		return puntosSuma;
	}
	public void setPuntosSuma(String puntosSuma) {
		this.puntosSuma = puntosSuma;
	}
	public int getPuntosCanje() {
		return puntosCanje;
	}
	public void setPuntosCanje(int puntosCanje) {
		this.puntosCanje = puntosCanje;
	}*/
	public String getTicketOriginal() {
		return ticketOriginal;
	}
	public void setTicketOriginal(String ticketOriginal) {
		this.ticketOriginal = ticketOriginal;
	}
        
        
	
	/*@Override
	public String toString() {
		return "DescuentoBean :[pedido=" + pedido
							 + ", email=" + email 
							 + ", cadena=" + cadena 
							 + ", pos=" + pos 
							 + ", sucursal=" + sucursal 
							 + ", ticket=" + ticket 
							 + ", confirmaCupon=" + Arrays.toString(confirmaCupon) 
							 + ", tipoDePago=" + Arrays.toString(tipoDePago) 
							 + ", puntosSuma=" + puntosSuma
							 + ", puntosCanje=" + puntosCanje
							 + ", estadoCupon=" + ticketOriginal+ "]";
	}*/

    public String getTicketCompleto() {
        return ticketCompleto;
    }

    public void setTicketCompleto(String ticketCompleto) {
        this.ticketCompleto = ticketCompleto;
    }

}
