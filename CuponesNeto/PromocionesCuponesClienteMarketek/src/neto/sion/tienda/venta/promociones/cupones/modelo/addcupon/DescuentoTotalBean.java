package neto.sion.tienda.venta.promociones.cupones.modelo.addcupon;

import org.codehaus.jackson.annotate.JsonProperty;

public class DescuentoTotalBean {
	
	@JsonProperty("codigo") private String codigo;//Codigo de barras
	@JsonProperty("monto") private String monto;
	@JsonProperty("oferta") private String oferta;
        @JsonProperty("descripcionOferta") private String descripcionOferta;
	@JsonProperty("prefijo") private String prefijo;
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getOferta() {
		return oferta;
	}
	public void setOferta(String oferta) {
		this.oferta = oferta;
	}
	public String getPrefijo() {
		return prefijo;
	}
	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}

    public String getDescripcionOferta() {
        return descripcionOferta;
    }

    public void setDescripcionOferta(String descripcionOferta) {
        this.descripcionOferta = descripcionOferta;
    }
	
	

	
	@Override
	public String toString() {
		return "DescuentoTotalBean :"
                        + "[codigo=" + codigo
                            + ", monto=" + monto 
                            + ", oferta=" + oferta 
                            + ", descripcionOferta=" + descripcionOferta 
                            + ", prefijo=" + prefijo
                        + "]";
		
	}

}
