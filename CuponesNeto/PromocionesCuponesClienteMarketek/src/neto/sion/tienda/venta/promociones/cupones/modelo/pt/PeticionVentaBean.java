package neto.sion.tienda.venta.promociones.cupones.modelo.pt;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "uId",
    "paisId",
    "tiendaId",
    "usuarioId",
    "numeroTarjeta",
    "estatusLecturaId",
    "comision",
    "metodoLectura",
    "pinEntryCapability",
    "numTerminal",
    "c55",
    "c63"
    
})

public class PeticionVentaBean {

	@JsonProperty("uId") private long uId;
	@JsonProperty("paisId") private int paisId;
	@JsonProperty("tiendaId") private int tiendaId;
	@JsonProperty("usuarioId") private long usuarioId;
	@JsonProperty("numeroTarjeta") private String numeroTarjeta;
	@JsonProperty("estatusLecturaId") private int estatusLecturaId;
	@JsonProperty("comision") private double comision;
	@JsonProperty("metodoLectura") private int metodoLectura;
	@JsonProperty("monto") private double monto;
	@JsonProperty("pinEntryCapability") private int pinEntryCapability;
	@JsonProperty("numTerminal") private String numTerminal;
	@JsonProperty("c55") private String c55;
	@JsonProperty("c63") private String c63;

	public long getuId() {
		return uId;
	}

	public void setuId(long uId) {
		this.uId = uId;
	}

	public int getPaisId() {
		return paisId;
	}

	public void setPaisId(int paisId) {
		this.paisId = paisId;
	}

	public int getTiendaId() {
		return tiendaId;
	}

	public void setTiendaId(int tiendaId) {
		this.tiendaId = tiendaId;
	}

	public long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public int getEstatusLecturaId() {
		return estatusLecturaId;
	}

	public void setEstatusLecturaId(int estatusLecturaId) {
		this.estatusLecturaId = estatusLecturaId;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public int getMetodoLectura() {
		return metodoLectura;
	}

	public void setMetodoLectura(int metodoLectura) {
		this.metodoLectura = metodoLectura;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public int getPinEntryCapability() {
		return pinEntryCapability;
	}

	public void setPinEntryCapability(int pinEntryCapability) {
		this.pinEntryCapability = pinEntryCapability;
	}

	public String getNumTerminal() {
		return numTerminal;
	}

	public void setNumTerminal(String numTerminal) {
		this.numTerminal = numTerminal;
	}

	public String getC55() {
		return c55;
	}

	public void setC55(String c55) {
		this.c55 = c55;
	}

	public String getC63() {
		return c63;
	}

	public void setC63(String c63) {
		this.c63 = c63;
	}

	@Override
	public String toString() {
		return "PeticionVentaBean [uId=" + uId + ", paisId=" + paisId
				+ ", tiendaId=" + tiendaId + ", usuarioId=" + usuarioId
				+ ", numeroTarjeta=" + numeroTarjeta + ", estatusLecturaId="
				+ estatusLecturaId + ", comision=" + comision
				+ ", metodoLectura=" + metodoLectura + ", monto=" + monto
				+ ", pinEntryCapability=" + pinEntryCapability
				+ ", numTerminal=" + numTerminal + ", c55=" + c55 + ", c63="
				+ c63 + "]";
	}



	

}
