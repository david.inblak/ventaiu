/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.addcupon;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author dramirezr
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "articuloId",
    "cantidad",
    "precio",
    "descuento"
})
public class ArticuloAddCuponBean {

    @JsonProperty("articuloId")
    private long articuloId;
    @JsonProperty("cantidad")
    private double cantidad;
    @JsonProperty("precio")
    private double precio;
    @JsonProperty("descuento")
    private double descuento;

    public ArticuloAddCuponBean() {
    }

    public ArticuloAddCuponBean(long articuloId, double cantidad, double precio, double descuento) {
        this.articuloId = articuloId;
        this.cantidad = cantidad;
        this.precio = precio;
        this.descuento = descuento;
    }
   

    public long getArticuloId() {
        return articuloId;
    }

    public void setArticuloId(long articuloId) {
        this.articuloId = articuloId;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }
    
    

    @Override
    public String toString() {
        return new StringBuilder("ArticuloCuponBean > ")
                .append("articuloId:").append(articuloId)
                .append(",cantidad:").append(cantidad)
                .append(",precio:").append(precio)
                .append(",descuento:").append(descuento).toString();
    }
}