package neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp;

import org.codehaus.jackson.annotate.JsonProperty;

public class FinalizaVentaCuponesPTBeanResp {
    @JsonProperty("respuestaVentaNormal") 
	private RespuestaVentaBean respuestaVentaNormal;
    @JsonProperty("respuestaTarjeta")
	private RespuestaVentaTarjetaBean respuestaTarjeta;
	
	public RespuestaVentaBean getRespuestaVentaNormal() {
		return respuestaVentaNormal;
	}
	public void setRespuestaVentaNormal(
			RespuestaVentaBean respuestaVentaNormal) {
		this.respuestaVentaNormal = respuestaVentaNormal;
	}
	public RespuestaVentaTarjetaBean getRespuestaTarjeta() {
		return respuestaTarjeta;
	}
	public void setRespuestaTarjeta(
			RespuestaVentaTarjetaBean respuestaTarjeta) {
		this.respuestaTarjeta = respuestaTarjeta;
	}

    @Override
    public String toString() {
        return "FinalizaVentaCuponesPTBeanResp{" + "respuestaVentaNormal=" + respuestaVentaNormal + ", respuestaTarjeta=" + respuestaTarjeta + '}';
    }
        
        
}
