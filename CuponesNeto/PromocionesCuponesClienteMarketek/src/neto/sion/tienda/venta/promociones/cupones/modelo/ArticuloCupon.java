/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo;

import java.math.BigDecimal;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author dramirezr
 */
public class ArticuloCupon {
    public static final Integer ID_SIN_DESCUENTO = 0;
    
    public static final Integer ID_SIN_IVA = 66;    
    public static final Integer ID_SIN_IEPS = 69;
    //public static final Integer ID_TIPODESCUENTO_CUPONES = 3;
    
    public static final Long ID_ARTICULO_DESCTO_TOTAL = 1000001L;
    public static final int ID_METODO_ENTRADA_DESC_INSERTADO = 4;
    
    public SimpleLongProperty id = new SimpleLongProperty();    
    public SimpleDoubleProperty cantidad = new SimpleDoubleProperty();
    public SimpleDoubleProperty cantidadDevueltos = new SimpleDoubleProperty();
    public SimpleStringProperty nombre = new SimpleStringProperty();
    public SimpleDoubleProperty precio = new SimpleDoubleProperty();
    public SimpleDoubleProperty importe = new SimpleDoubleProperty();
    public SimpleDoubleProperty costo = new SimpleDoubleProperty();
    public SimpleDoubleProperty descuento = new SimpleDoubleProperty();
    public SimpleDoubleProperty iva = new SimpleDoubleProperty();
    public SimpleDoubleProperty agranel = new SimpleDoubleProperty();
    public SimpleIntegerProperty idFamilia = new SimpleIntegerProperty();
    public SimpleIntegerProperty idIEps = new SimpleIntegerProperty();
    public SimpleIntegerProperty unidad = new SimpleIntegerProperty();
    public SimpleIntegerProperty normaEmpaque = new SimpleIntegerProperty();
    public ObjectProperty<CuponBean>  cupon = new SimpleObjectProperty<CuponBean>();
    public SimpleStringProperty codigoBarras = new SimpleStringProperty();
    public SimpleStringProperty codigoBarrasPadre = new SimpleStringProperty();
    public SimpleIntegerProperty tipoDescuento = new SimpleIntegerProperty();
    
    ChangeListener<Number> listener = null;
    public Double calculo(Double x, Double y, Double z){
        BigDecimal cant = new BigDecimal(x);
        BigDecimal prec = new BigDecimal(y);
        BigDecimal desct = new BigDecimal(z);
        
        return desct.subtract(prec).multiply(cant).doubleValue();
    }
    
     public ArticuloCupon(){
        listener = new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
                //if( cantidad.get() > 0 && descuento.get() > 0 && precio.get() > 0)
                    importe.set( calculo(cantidad.doubleValue(), descuento.doubleValue(), precio.getValue()) );
                
            }
        };
        
        if( cupon == null )
            cupon.set( new CuponBean() );
        
        cantidad.addListener(listener);
        descuento.addListener(listener);
        precio.addListener(listener);
     }
     
    public ArticuloCupon(
            long _id, final Double _cantidad, final Double _descuento, 
            CuponBean cupon, String codBarras, Integer tipoDescuento, String nmbre) 
    {
        this();
        this.id.set(_id);
        this.cantidad.set(_cantidad);
        this.descuento.set(_descuento);
        
        this.cupon.set(cupon);
        this.codigoBarras.set(codBarras);     
        this.tipoDescuento.set(tipoDescuento);
        
        this.nombre.set(nmbre);
    }
    
    public ArticuloCupon(
            Long id, Double cantidad, String nombre, Double precio, 
            Double costo, Double descuento, Double iva, Double agranel,
            Integer idFamilia, Integer idIEPS, Integer unidad, Integer normaempaque,
            CuponBean cupon, String codBarras, String codBarraspadre, Integer tipoDescuento,
            Double devueltos) 
    {
       this();
                
        this.id.set(id);
        this.cantidad.set(cantidad);
        this.cantidadDevueltos.set(devueltos);
        this.nombre.set(nombre);
        this.precio.set(precio);
             
        this.costo.set(costo);
        this.descuento.set(descuento);
        this.iva.set(iva);        
        this.agranel.set(agranel);
        
        this.idFamilia.set(idFamilia);
        this.idIEps.set(idIEPS);
        this.unidad.set(unidad);        
        this.normaEmpaque.set(normaempaque);
        
        this.cupon.set(cupon);
        this.codigoBarras.set(codBarras);
        this.codigoBarrasPadre.set(codBarraspadre);        
        this.tipoDescuento.set(tipoDescuento);
    }
    
    
    public double getCantidadDevueltos() {
        return cantidadDevueltos.getValue();
    }

    public void setCantidadDevueltos(double cantDevueltos) {
        this.cantidadDevueltos.setValue(cantDevueltos);
    }

    public double getCantidad() {
        return cantidad.getValue();
    }

    public void setCantidad(double cantidad) {
        this.cantidad.setValue(cantidad);
    }

    public CuponBean getCupon() {
        return cupon.get();
    }

    public void setCupon(CuponBean cupon) {
        this.cupon.get().setActivo(cupon.isActivo());
        this.cupon.get().setIdCupon(cupon.getIdCupon());
        this.cupon.get().setMensaje(cupon.getMensaje());
        this.cupon.get().setAutomatico(cupon.getAutomatico());
    }

    public String getNombre() {
        return nombre.getValue();
    }

    public void setNombre(String nombre) {
        this.nombre.setValue(nombre);
    }

    public Double getDescuento() {
        return this.descuento.get();
    }

    public void setDescuento(Double descuento) {
        this.descuento.set(descuento);
        //this.descuento.
    }

    public Double getPrecio() {
        return precio.get();
    }

    public void setPrecio(Double precio) {
        this.precio.set(precio);
    }

    public double getAgranel() {
        return agranel.get();
    }

    public void setAgranel(double agranel) {
        this.agranel.get();
    }

    public String getCodigoBarras() {
        return codigoBarras.get();
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras.set(codigoBarras);
    }

    public String getCodigoBarrasPadre() {
        return codigoBarrasPadre.get();
    }

    public void setCodigoBarrasPadre(String codigoBarrasPadre) {
        this.codigoBarrasPadre.set(codigoBarrasPadre);
    }

    public long getId() {
        return id.get();
    }

    public void setId(long id) {
        this.id.set(id);
    }

    public int getIdFamilia() {
        return idFamilia.get();
    }

    public void setIdFamilia(int idFamilia) {
        this.idFamilia.set(idFamilia);
    }

    public int getIdIEps() {
        return idIEps.get();
    }

    public void setIdIEps(int idIEps) {
        this.idIEps.set(idIEps);
    }

    public double getIva() {
        return iva.get();
    }

    public void setIva(double iva) {
        this.iva.set(iva);
    }

    public int getNormaEmpaque() {
        return normaEmpaque.get();
    }

    public void setNormaEmpaque(int normaEmpaque) {
        this.normaEmpaque.set(normaEmpaque);
    }

    public int getUnidad() {
        return unidad.get();
    }

    public void setUnidad(int unidad) {
        this.unidad.set(unidad);
    }

    public int getTipoDescuento() {
        return tipoDescuento.get();
    }

    public void setTipoDescuento(int tipoDescuento) {
        this.tipoDescuento.set(tipoDescuento);
    }

    public double getImporte() {
        return importe.get();
    }

    private void setImporte(double importe) {
        this.importe.set(importe);
    }

    public double getCosto() {
        return costo.get();
    }

    public void setCosto(double costo) {
        this.costo.set(costo);
    }
    
    

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ArticuloCupon other = (ArticuloCupon) obj;
        if (this.id.get() == other.id.get()) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        /*int hash = 3;
        hash = 53 * hash + Integer.getInteger(this.id+"");*/
        int res = (int) (this.id.get() ^ (this.id.get() >>> 32));
        return res;//return this.id.getValue().intValue();
    }

    @Override
    public String toString() {
        return "ArticuloCupon{" + "id=" + id + ", cantidad=" + cantidad + ", nombre=" + nombre + ", precio=" + precio + ", importe=" + importe + ", costo=" + costo + ", descuento=" + descuento + ", iva=" + iva + ", agranel=" + agranel + ", idFamilia=" + idFamilia + ", idIEps=" + idIEps + ", unidad=" + unidad + ", normaEmpaque=" + normaEmpaque + ", cupon=" + cupon + ", codigoBarras=" + codigoBarras + ", codigoBarrasPadre=" + codigoBarrasPadre + ", tipoDescuento=" + tipoDescuento + '}';
    }

    
}
