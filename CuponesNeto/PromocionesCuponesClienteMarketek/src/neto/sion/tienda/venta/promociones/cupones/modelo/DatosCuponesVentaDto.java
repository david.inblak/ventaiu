/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import neto.sion.tienda.venta.promociones.cupones.modelo.ArticuloCupon;
import neto.sion.tienda.venta.promociones.cupones.modelo.addcupon.CuponBeanResp;
import neto.sion.tienda.venta.promociones.cupones.modelo.finventa.ConfirmaCuponBean;
import neto.sion.tienda.venta.promociones.cupones.modelo.finventa.FinTicketMktcBeanReq;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author dramirezr
 */
public class DatosCuponesVentaDto {
    int intentos;
    
    private boolean esVentaInicializadaMktc = false;//+++    
    private boolean esVentaSinMarketec = false;//+++
    private String motivoSinCupones;//+++
    private FinTicketMktcBeanReq finTicketMktc = new FinTicketMktcBeanReq();//+++
     /**     Lista de cupones obtenidos de la respuesta de agregar cupones*/
    private Set<CuponBeanResp> cuponesValidados = new HashSet<CuponBeanResp>();//+++
    private long registroMktcId;//+++
    private String registroPeticionNeto;
    /** Lista de articulos con cupon */
    List<ArticuloCupon> articulosCupon = new ArrayList<ArticuloCupon>();
    
    public int incrementeIntentos(){
        return ++intentos;
    }

    public List<ArticuloCupon> getArticulosCupon() {
        return articulosCupon;
    }

    public void setArticulosCupon(List<ArticuloCupon> articulosCupon) {
        this.articulosCupon = articulosCupon;
    }

    public int getIntentos() {
        return intentos;
    }

    public void setIntentos(int intentos) {
        this.intentos = intentos;
    }
    
    public List<ConfirmaCuponBean> getCuponesConfirmados(){
        List<ConfirmaCuponBean> confirmados = new ArrayList<ConfirmaCuponBean>();
        Set<CuponBeanResp> cupVal = getCuponesValidados();
        if( cupVal != null ){
            for (Iterator<CuponBeanResp> confirmaCuponBean = cupVal.iterator(); confirmaCuponBean.hasNext();) {
                CuponBeanResp cuponBeanResp = confirmaCuponBean.next();
                if( cuponBeanResp != null ){
                    ConfirmaCuponBean cnf = new ConfirmaCuponBean();
                    cnf.setCodigo(cuponBeanResp.getCodigo());
                    cnf.setAplicaDescuento(cuponBeanResp.getEstado()== -1? false: true);
                    confirmados.add(cnf);
                }                
            }
        }
                
        return confirmados;
    }

    public Set<CuponBeanResp> getCuponesValidados() {
        return cuponesValidados;
    }

    public void setCuponesValidados(Set<CuponBeanResp> cuponesValidados) {
        this.cuponesValidados = cuponesValidados;
    }

    public boolean isEsVentaInicializadaMktc() {
        return esVentaInicializadaMktc;
    }

    public void setEsVentaInicializadaMktc(boolean esVentaInicializadaMktc) {
        this.esVentaInicializadaMktc = esVentaInicializadaMktc;
    }

   


    public FinTicketMktcBeanReq getFinTicketMktc() {
        return finTicketMktc;
    }

    public void setFinTicketMktc(FinTicketMktcBeanReq finTicketMktc) {
        this.finTicketMktc = finTicketMktc;
    }

    public String getMotivoSinCupones() {
        return motivoSinCupones;
    }

    public void setMotivoSinCupones(String motivoSinCupones) {
        this.motivoSinCupones = motivoSinCupones;
    }

    public long getRegistroMktcId() {
        return registroMktcId;
    }

    public void setRegistroMktcId(long registroMktcId) {
        this.registroMktcId = registroMktcId;
    }
    
    public String getRegistroPeticionNeto() {
        return this.registroPeticionNeto;
    }

    public void setRegistroPeticionNeto(String registroPeticionNeto) {
        this.registroPeticionNeto = registroPeticionNeto;
    }

    public boolean isEsVentaSinMarketec() {
        return esVentaSinMarketec;
    }

    public void setEsVentaSinMarketec(boolean esVentaSinMarketec) {
        this.esVentaSinMarketec = esVentaSinMarketec;
    }
    
    public DatosCuponesVentaDto clonar(){
        /*DatosCuponesVentaDto clon = new DatosCuponesVentaDto();
        
        clon.setRegistroMktcId(this.getRegistroMktcId());
        clon.setMotivoSinCupones(this.getMotivoSinCupones());
        clon.setIntentos(this.getIntentos());
        clon.setEsVentaSinMarketec(this.isEsVentaSinMarketec());
        clon.setEsVentaInicializadaMktc(this.isEsVentaInicializadaMktc());
        
        clon.setCuponesValidados(new HashSet<CuponBeanResp>());
        for (Iterator<CuponBeanResp> it = cuponesValidados.iterator(); it.hasNext();) {
            CuponBeanResp cuponBeanResp = it.next();
            
        }
        
        
        clon.setArticulosCupon(+);
        clon.setFinTicketMktc(+);*/
        
        return Collections.nCopies(1, this).get(0);
    }
}
