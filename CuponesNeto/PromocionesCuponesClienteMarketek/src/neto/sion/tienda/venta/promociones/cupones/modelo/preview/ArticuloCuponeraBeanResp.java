package neto.sion.tienda.venta.promociones.cupones.modelo.preview;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "cantidad",
    "codigoBarras",
    "descripcionOferta",
    "monto"
})
public class ArticuloCuponeraBeanResp {
    @JsonProperty("cantidad")
    private double cantidad;//Cantidad de artículos a los que afecta el cupon
    @JsonProperty("codigoBarras")
    private String codigoBarras;
    @JsonProperty("descripcionOferta")
    private String descripcionOferta;
    @JsonProperty("monto")
    private double monto;

    @JsonProperty("cantidad")
    public double getCantidad() {
        return cantidad;
    }

    @JsonProperty("cantidad")
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @JsonProperty("codigoBarras")
    public String getCodigoBarras() {
        return codigoBarras;
    }

    @JsonProperty("codigoBarras")
    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    @JsonProperty("descripcionOferta")
    public String getDescripcionOferta() {
        return descripcionOferta;
    }

    @JsonProperty("descripcionOferta")
    public void setDescripcionOferta(String descripcionOferta) {
        this.descripcionOferta = descripcionOferta;
    }

    @JsonProperty("monto")
    public double getMonto() {
        return monto;
    }

    @JsonProperty("monto")
    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String toString() {
        return new StringBuilder().
                append(",cantidad:").append( cantidad).
                append(",codigoBarras:").append(  codigoBarras).
                append(",descripcionOferta:").append(  descripcionOferta).
                append(",monto:").append(  monto).toString();
    }

}