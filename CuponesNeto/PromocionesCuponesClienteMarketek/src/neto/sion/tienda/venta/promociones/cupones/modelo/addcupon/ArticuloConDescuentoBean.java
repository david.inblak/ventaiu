
package neto.sion.tienda.venta.promociones.cupones.modelo.addcupon;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "articuloId",
    "cantidad",
    "codigoBarras",
    "cupon",
    "descripcionOferta",
    "montoDesc",
    "puntosCanje"
})
public class ArticuloConDescuentoBean {
    @JsonProperty("articuloId")
    private long articuloId;
    @JsonProperty("cantidad")
    private double cantidad;
    @JsonProperty("codigoBarras")
    private String codigoBarras;
    @JsonProperty("cupon")
    private String cupon;
    @JsonProperty("descripcionOferta")
    private String descripcionOferta;
    @JsonProperty("montoDesc")
    private double montoDesc;
    @JsonProperty("puntosCanje")
    private String puntosCanje;

    @JsonProperty("articuloId")
    public long getArticuloId() {
        return this.articuloId;
    }

    @JsonProperty("articuloId")
    public void setArticuloId(long articuloId) {
        this.articuloId = articuloId;
    }
    
     @JsonProperty("cantidad")
    public double getCantidad() {
        return cantidad;
    }

    @JsonProperty("cantidad")
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    @JsonProperty("codigoBarras")
    public String getCodigoBarras() {
        return codigoBarras;
    }

    @JsonProperty("codigoBarras")
    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    @JsonProperty("cupon")
    public String getCupon() {
        return cupon;
    }

    @JsonProperty("cupon")
    public void setCupon(String cupon) {
        this.cupon = cupon;
    }

    @JsonProperty("descripcionOferta")
    public String getDescripcionOferta() {
        return descripcionOferta;
    }

    @JsonProperty("descripcionOferta")
    public void setDescripcionOferta(String descripcionOferta) {
        this.descripcionOferta = descripcionOferta;
    }

    @JsonProperty("montoDesc")
    public double getMontoDesc() {
        return montoDesc;
    }

    @JsonProperty("montoDesc")
    public void setMontoDesc(float montoDesc) {
        this.montoDesc = montoDesc;
    }

    @JsonProperty("puntosCanje")
    public String getPuntosCanje() {
        return puntosCanje;
    }

    @JsonProperty("puntosCanje")
    public void setPuntosCanje(String puntosCanje) {
        this.puntosCanje = puntosCanje;
    }

    @Override
    public String toString() {
        return new StringBuilder("ArticuloConDescuentoBean > ")
                .append("cantidad:").append(cantidad)
                .append(", codigoBarras:").append(codigoBarras)
                .append(", cupon:").append(cupon)
                .append(", descripcionOferta:").append(descripcionOferta)
                .append(", montoDesc:").append(montoDesc)
                .append(", puntosCanje:").append(puntosCanje).toString();
    }

}
