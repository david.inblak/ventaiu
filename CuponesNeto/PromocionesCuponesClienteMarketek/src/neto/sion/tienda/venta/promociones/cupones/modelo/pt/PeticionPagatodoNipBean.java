/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.pt;


import java.io.Serializable;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.util.ConversionBeanes;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;



@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "fechaHoraTransaccion",
    "idSucursal",
    "idTerminal",
    "idOperador",
    "noTicket",
    "referencia",
    "monto",
    "tipoTarjetaId",
    "nip"
})


public class PeticionPagatodoNipBean {
	
	@JsonProperty("fechaHoraTransaccion") private String fechaHoraTransaccion;
	@JsonProperty("idSucursal") private String idSucursal;
	@JsonProperty("idTerminal") private String idTerminal;
	@JsonProperty("idOperador") private String idOperador;
	@JsonProperty("noTicket") private String noTicket;
	@JsonProperty("referencia") private String referencia;
	@JsonProperty("monto") private String monto;
	@JsonProperty("tipoTarjetaId") private int tipoTarjetaId;
	@JsonProperty("nip") private String nip;
	  
	
	public int getTipoTarjetaId() {
		return tipoTarjetaId;
	}
	public void setTipoTarjetaId(int tipoTarjetaId) {
		this.tipoTarjetaId = tipoTarjetaId;
	}
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	
	public String getFechaHoraTransaccion() {
		return fechaHoraTransaccion;
	}
	public void setFechaHoraTransaccion(String fechaHoraTransaccion) {
		this.fechaHoraTransaccion = fechaHoraTransaccion;
	}
	
	public String getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(String idSucursal) {
		this.idSucursal = idSucursal;
	}
	public String getIdTerminal() {
		return idTerminal;
	}
	public void setIdTerminal(String idTerminal) {
		this.idTerminal = idTerminal;
	}
	public String getIdOperador() {
		return idOperador;
	}
	public void setIdOperador(String idOperador) {
		this.idOperador = idOperador;
	}
	public String getNoTicket() {
		return noTicket;
	}
	public void setNoTicket(String noTicket) {
		this.noTicket = noTicket;
	}
	
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	
	
	public int getTipoTarjataId() {
		return tipoTarjetaId;
	}
	public void setTipoTarjataId(int _tipoTarjataId) {
		this.tipoTarjetaId = _tipoTarjataId;
	}

            @Override
    public String toString() {
        try {
            return ConversionBeanes.convertir(this, String.class).toString();
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "toString");
        }
        return null;
    }
}
