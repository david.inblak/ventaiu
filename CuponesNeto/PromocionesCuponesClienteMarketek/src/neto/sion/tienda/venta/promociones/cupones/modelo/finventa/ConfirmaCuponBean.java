package neto.sion.tienda.venta.promociones.cupones.modelo.finventa;



import org.codehaus.jackson.annotate.JsonProperty;

public class ConfirmaCuponBean {

	
	@JsonProperty("codigo") private String codigo;
	@JsonProperty("aplicaDescuento") private String aplicaDescuento;
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getAplicaDescuento() {
		return aplicaDescuento;
	}
	public void setAplicaDescuento(Boolean aplicoDescuento) {
		this.aplicaDescuento = aplicoDescuento.toString();
	}
	
	
	@Override
    public String toString() {
        return new StringBuilder().append("aplicaDescuento:").append(aplicaDescuento).append(",codigo:").append( codigo).toString();
    }
	

}
