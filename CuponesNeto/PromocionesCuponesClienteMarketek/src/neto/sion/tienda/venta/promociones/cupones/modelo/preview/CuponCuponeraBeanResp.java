/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.preview;


import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;




@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({    "articulosCuponera",    "codigoCupon"})
public class CuponCuponeraBeanResp {
    	
    @JsonProperty("articulosCuponera")
    private List<ArticuloCuponeraBeanResp> articulosCuponera = null;
    @JsonProperty("codigoCupon")
    private String codigoCupon;

    @JsonProperty("articulosCuponera")
    public List<ArticuloCuponeraBeanResp> getArticulosCuponera(){
        return articulosCuponera;
    }

    @JsonProperty("arrayArticulosCuponera")
    public void setArticulosCuponera(List<ArticuloCuponeraBeanResp> articulosCuponera) {
        this.articulosCuponera = articulosCuponera;
    }

    @JsonProperty("codigoCupon")
    public String getCodigoCupon() {
        return codigoCupon;
    }

    @JsonProperty("codigoCupon")
    public void setCodigoCupon(String codigoCupon) {
        this.codigoCupon = codigoCupon;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(",articulosCuponera:").append( articulosCuponera)
                .append(",codigoCupon:").append( codigoCupon).toString();
    }
}
