package neto.sion.tienda.utils;

import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;

public class OperacionesPinpad {

    ConstantesTarjeta constantes;
    String exeInterop;
    String exeDevcon;
    String idPort;
    int timeout;

    public OperacionesPinpad()
    {
        constantes = new ConstantesTarjeta();
        idPort = SION.obtenerParametro(Modulo.SION, "ID.PINPAD");
        exeDevcon = SION.obtenerParametro(Modulo.VENTA, "pinpad.bin.devcon");
        exeInterop = SION.obtenerParametro(Modulo.VENTA, "pinpad.bin.chip");
        timeout = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "pinpad.lectura.tiempo.espera").trim());
    }
    public boolean restartPortPinpad()
    {
        String comando = "cmd /c " + exeDevcon + constantes.opcionReiniciarDevcon + idPort;
        boolean resultado = true;
        try
        {
            Process process = Runtime.getRuntime().exec(comando);
            String mensaje = "";
            ComunicacionPinpad pinpad = new ComunicacionPinpad();
            
            mensaje = pinpad.lecturaDatosPinpad(process, timeout);
            
            if(mensaje.toString().toLowerCase().matches(".*no.*restarted.*"))
                resultado = false;
            else if(mensaje.toString().toLowerCase().contains("restarted"))
                resultado = true;
            else
                resultado = false;
            process.destroy();
        }
        catch (Exception e)
        {
            resultado = false;
            SION.logearExcepcion(Modulo.VENTA, e, e.getMessage());
        }
        catch (Error e)
        {
            resultado = false;
            SION.logearExcepcion(Modulo.VENTA, new Exception(e), e.getMessage());
        }
        return resultado;
    }

    public boolean enablePortPinpad()
    {
        String comando = "cmd /c " + exeDevcon + constantes.opcionHabilitarDevcon + idPort;
        boolean resultado = true;
        try
        {
            Process process = Runtime.getRuntime().exec(comando);
            String mensaje = "";
            ComunicacionPinpad pinpad = new ComunicacionPinpad();
            
            mensaje = pinpad.lecturaDatosPinpad(process, timeout);
            
            if(mensaje.toString().toLowerCase().contains("enable"))
                resultado = true;
            else
                resultado = false;
            process.destroy();
        }
        catch (Exception e)
        {
            resultado = false;
            SION.logearExcepcion(Modulo.VENTA, e, e.getMessage());
        }
        catch (Error e)
        {
            resultado = false;
            SION.logearExcepcion(Modulo.VENTA, new Exception(e), e.getMessage());
        }
        return resultado;
    }

    public boolean disablePortPinpad()
    {
        String comando = "cmd /c " + exeDevcon + constantes.opcionDeshabilitarDevcon + idPort;
        boolean resultado = true;
        try
        {
            Process process = Runtime.getRuntime().exec(comando);
            String mensaje = "";
            ComunicacionPinpad pinpad = new ComunicacionPinpad();
            
            mensaje = pinpad.lecturaDatosPinpad(process, timeout);
            
            if(mensaje.toString().toLowerCase().contains("disable"))
                resultado = true;
            else
                resultado = false;
            process.destroy();
        }
        catch (Exception e)
        {
            resultado = false;
            SION.logearExcepcion(Modulo.VENTA, e, e.getMessage());
        }
        catch (Error e)
        {
            resultado = false;
            SION.logearExcepcion(Modulo.VENTA, new Exception(e), e.getMessage());
        }
        return resultado;
    }
    
    public boolean isRunningInterop()
    {
        String comando = "cmd /c " + constantes.buscaProcesoPinpadInterop + " " + exeInterop + "\"";
        boolean resultado = true;
        try
        {
            Process process = Runtime.getRuntime().exec(comando);
            String mensaje = "";
            ComunicacionPinpad pinpad = new ComunicacionPinpad();
            
            mensaje = pinpad.lecturaDatosPinpad(process, timeout);
            
            if(mensaje.toString().toLowerCase().contains(exeInterop.toLowerCase()))
                resultado = true;
            else
                resultado = false;
            process.destroy();
        }
        catch (Exception e)
        {
            resultado = false;
            SION.logearExcepcion(Modulo.VENTA, e, e.getMessage());
        }
        catch (Error e)
        {
            resultado = false;
            SION.logearExcepcion(Modulo.VENTA, new Exception(e), e.getMessage());
        }
        return resultado;
    }
    
    public boolean killInterop()
    {
        String tmp[];
        String nameExe = exeInterop;
////////////////////
//        SION.log(Modulo.VENTA, "if(exeInterop.matches(\"[a-zA-Z0-9_\\.\\s\\-]+\\.[a-zA-Z0-9_]+\"))" + exeInterop.matches("[a-zA-Z0-9_\\.\\s\\-]+\\.[a-zA-Z0-9_]+"), Level.INFO);
////////////////////
        if(exeInterop.matches("[a-zA-Z0-9_\\.\\s\\-]+\\.[a-zA-Z0-9_]+"))
        {
            tmp = exeInterop.split("\\.");
//            SION.log(Modulo.VENTA, tmp.toString(), Level.INFO);
            nameExe = tmp[0];
//            SION.log(Modulo.VENTA, nameExe, Level.INFO);
        }

        String comando = "cmd /c " + constantes.matarProcesoWindows + " " + nameExe+ "*" ;
        boolean resultado = true;
        try
        {
            Process process = Runtime.getRuntime().exec(comando);
            SION.log(Modulo.VENTA, comando, Level.INFO);
            process.destroy();
        }
        catch (Exception e)
        {
            resultado = false;
            SION.logearExcepcion(Modulo.VENTA, e, e.getMessage());
        }
        catch (Error e)
        {
            resultado = false;
            SION.logearExcepcion(Modulo.VENTA, new Exception(e), e.getMessage());
        }
        return resultado;
    }
    
    public boolean runInstallBAT()
    {
        String comando = "cmd /c \"D:\\DESTEC\\InstalacionPinpad\\InstalacionManualCompleta.bat\"" ;
        boolean resultado = true;
        try
        {
            Process process = Runtime.getRuntime().exec(comando);
            SION.log(Modulo.VENTA, comando, Level.INFO);
            process.destroy();
        }
        catch (Exception e)
        {
            resultado = false;
            SION.logearExcepcion(Modulo.VENTA, e, e.getMessage());
        }
        catch (Error e)
        {
            resultado = false;
            SION.logearExcepcion(Modulo.VENTA, new Exception(e), e.getMessage());
        }
        return resultado;
    }
}