package neto.sion.tienda.utils;

import java.util.logging.Level;
import neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto;
import neto.sion.tarjeta.chip.utilerias.UtileriasCadenas;
import neto.sion.tarjeta.utilidades.OrdenaTracks;
import neto.sion.tarjeta.utilidades.Tracks;
import neto.sion.tarjeta.utilidades.Validacion;
import neto.sion.tienda.bean.PeticionPagoTarjetaFrontBean;
import neto.sion.tienda.dao.PorcentajeComisionDao;
import neto.sion.tienda.dao.TipoTarjetaDao;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;

public class UtileriasTarjeta
{

    UtileriasCadenas util = new UtileriasCadenas();
    ConstantesTarjeta constantes = new ConstantesTarjeta();

    public boolean operacionPermitida(boolean sePermitenBancarias, boolean sePermitenVales, int tipoTarjeta)
    {
        ConstantesTarjeta constantesTarjeta = new ConstantesTarjeta();
        if((tipoTarjeta == constantesTarjeta.tarjetaCredito || tipoTarjeta == constantesTarjeta.tarjetaDebito) && sePermitenBancarias)
            return true;
        else if(tipoTarjeta == constantesTarjeta.tarjetaVales && sePermitenVales)
            return true;
        return false;
    }

    public String obtieneNumeroTarjeta(String track)
    {
        track = track.replaceAll("\\D", "");
        return track.length() >= 16
                ? track.trim().substring(0, 16)
                : "ERROR: Track erroneo";
    }

    public int obtieneTipoTarjeta (String respuestaPinpad)
    {
        SION.log(Modulo.VENTA, "obtieneTipoTarjeta: " + respuestaPinpad, Level.INFO);
        String valor = "";

        int codTipoTarjeta = -1;

        if((valor = util.obtieneValorBuscaPropiedad("Track", respuestaPinpad)).contains("ERROR"))
        {
            SION.log(Modulo.VENTA, "Tarjeta chip: " + valor, Level.INFO);
            if(!(valor = util.obtieneValorBuscaPropiedad("NombreTipoTarjeta", respuestaPinpad)).contains("ERROR"))
            {
                SION.log(Modulo.VENTA, "Tarjeta chip [obtener tipo]: " + valor, Level.INFO);
                if(valor.matches(".*CREDIT.*"))
                    codTipoTarjeta = constantes.tarjetaCredito;
                else
                    codTipoTarjeta = constantes.tarjetaDebito;
            }
        }
        else
        {
            valor = this.obtieneNumeroTarjeta(valor);
            SION.log(Modulo.VENTA, "Tarjeta banda: " + valor, Level.INFO);
            TipoTarjetaDao tipoTarjetaDao = new TipoTarjetaDao();
            codTipoTarjeta = tipoTarjetaDao.obtieneTipoTarjeta(valor);
        }
        SION.log(Modulo.VENTA, "Resultado obtieneTipoTarjeta: " + codTipoTarjeta, Level.INFO);
        return codTipoTarjeta;
    }
    
    public boolean esTarjetaPagaTodo (String respuestaPinpad)
    {
        SION.log(Modulo.VENTA, "esTarjetaPagaTodo: " + respuestaPinpad, Level.INFO);
        String valor = "";

        boolean esPagaTodo=false;

        if(!(valor = util.obtieneValorBuscaPropiedad("Track", respuestaPinpad)).contains("ERROR"))
        {
            valor = this.obtieneNumeroTarjeta(valor);
            SION.log(Modulo.VENTA, "Tarjeta banda: " + valor, Level.INFO);
            TipoTarjetaDao tipoTarjetaDao = new TipoTarjetaDao();
            esPagaTodo = tipoTarjetaDao.esTarjetaPagaTodo(valor);
        }
        SION.log(Modulo.VENTA, "Resultado obtieneTipoTarjeta: " + esPagaTodo, Level.INFO);
        return esPagaTodo;
    }
    
    public boolean esTarjetaDescuento(String respuestaPinpad)
    {
        SION.log(Modulo.VENTA, "esTarjetaDescuento: " + respuestaPinpad, Level.INFO);
        String valor = "";

        boolean esTarjetaDescuento=false;

        if(!(valor = util.obtieneValorBuscaPropiedad("Track", respuestaPinpad)).contains("ERROR"))
        {
            valor = this.obtieneNumeroTarjeta(valor);
            SION.log(Modulo.VENTA, "Tarjeta banda: " + valor, Level.INFO);
            TipoTarjetaDao tipoTarjetaDao = new TipoTarjetaDao();
            esTarjetaDescuento = tipoTarjetaDao.esTarjetaDescuento(valor);
        }
        SION.log(Modulo.VENTA, "Resultado esTarjetaDescuento: " + esTarjetaDescuento, Level.INFO);
        return esTarjetaDescuento;
    }
   

    public int obtieneTipoLectura (boolean isChip, boolean isFalloLecturaChip)
    {
        int tipoLectura = -1;

        if(isChip && !isFalloLecturaChip)
            tipoLectura = constantes.lecturaChip;
        else if(!isChip)
            tipoLectura = constantes.lecturaBanda;
        else
            tipoLectura = constantes.lecturaFallback;

        return tipoLectura;
    }

    public double calculaComision(int tipoTarjeta, double monto, int paPaisId)
    {
        SION.log(Modulo.VENTA, "Solicitud de calculo de comisión:\ntipoTarjeta=" + tipoTarjeta + "\nmonto=" + monto + "\npaisId=" + paPaisId, Level.INFO);
        double comision = 0.0;
        PorcentajeComisionDao porcentajeDao = new PorcentajeComisionDao();
        double porcentaje = porcentajeDao.obtenerPorcentaje(tipoTarjeta, paPaisId);
        if(porcentaje > 0)
            comision = Math.round(monto * porcentaje * 100.0) / 100.0;
        SION.log(Modulo.VENTA, "Comisión caculada: " + comision, Level.INFO);
        return comision;
    }

    public PeticionPagoTarjetaDto asignarValoresSolicitudPago(String respuesta, PeticionPagoTarjetaFrontBean peticionPagoFront, int tipoTarjeta, double comision, boolean prosa)
    {

        PeticionPagoTarjetaDto peticionPagoDto = new PeticionPagoTarjetaDto();

        peticionPagoDto.setNumTerminal(this.obtenerNumeroTerminal(peticionPagoFront.getNumTerminal()));
        peticionPagoDto.setMonto(Math.round(peticionPagoFront.getMonto()*100.0)/100.0);
        peticionPagoDto.setTiendaId(peticionPagoFront.getTiendaId());
        peticionPagoDto.setPaPaisId(peticionPagoFront.getPaisId());
        peticionPagoDto.setCanalPago(peticionPagoFront.getCanalPago());

        String track = "";

        track = util.obtieneValorBuscaPropiedad("Track", respuesta);
        peticionPagoDto.setChip(util.obtieneValorBuscaPropiedad("Type_of_card", respuesta).equals("CHIP_EMV"));
        peticionPagoDto.setFalloLecturaChip(util.obtieneValorBuscaPropiedad("Tipo_Lectura", respuesta).equals("FALLBACK"));

        //Campo 63 es asignado en central por una constante definida por banco para las operaciones con chip o banda magnetica
        peticionPagoDto.setC63(prosa ? "PROSA" : "N/A");

        if(peticionPagoDto.isChip() && !peticionPagoDto.isFalloLecturaChip())
        {
            track = "ñ" + util.rellenaCadenaCaracter(track, track.length(), "0", false) + "f_";
            peticionPagoDto.setTrack1(track);
            peticionPagoDto.setTrack2(track);
            peticionPagoDto.setC55(util.obtieneValorBuscaPropiedad("C55_data", respuesta));
            //peticionPagoDto.setC63(constantes.C63Chip);
            SION.log(Modulo.VENTA, "Generación de track de solicitud [CHIP]..", Level.INFO);
        }//if
        else
        {
            Validacion valida = new Validacion();
            OrdenaTracks ordena = new OrdenaTracks();
            Tracks genTracks = new Tracks();
            track = ordena.resultadoOrdenar(track);
            genTracks = valida.validarTrack(track);
            String track1 = genTracks.getTrack1();
            String track2 = genTracks.getTrack2();
            if(util.isCadenaVacia(track1))
                track1=track2;
            peticionPagoDto.setTrack1(track1);
            peticionPagoDto.setTrack2(track2);
            peticionPagoDto.setC55("N/A");
            //peticionPagoDto.setC63(constantes.C63Banda);
            SION.log(Modulo.VENTA, "Fin de generación de track de solicitud[BANDA]..", Level.INFO);
        }
        peticionPagoDto.setComision(comision);
        return peticionPagoDto;
    }//asignarValoresInfoTarjetaBean

    public String obtenerNombrePropietario (String respuestaPinpad)
    {
        String nombrePropietario = "N/A";
        try
        {
            String track = util.obtieneValorBuscaPropiedad("track", respuestaPinpad);
            if(track.matches(".*([a-zA-Z]+[\\.\\s]*)+.*"))
            {
                Validacion valida = new Validacion();
                OrdenaTracks ordena = new OrdenaTracks();
                Tracks genTracks = new Tracks();
                SION.log(Modulo.VENTA, "Obteniendo nombre del propietario: Obteniendo track=" + track, Level.INFO);
                track = ordena.resultadoOrdenar(track);
                SION.log(Modulo.VENTA, "Obteniendo nombre del propietario: Ordenando track=" + track, Level.INFO);
                genTracks = valida.validarTrack(track);
                nombrePropietario = genTracks.getNombreUsuario();
                SION.log(Modulo.VENTA, "Obteniendo nombre del propietario: nombre=" + nombrePropietario, Level.INFO);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            SION.logearExcepcion(Modulo.VENTA, e, e.getMessage());
        }
        return nombrePropietario;
    }

    public String obtenerNumeroTerminal(String cadena)
    {
        return cadena.replaceAll(".*\\(", "").replaceAll("\\D", "");
    }

    public boolean esVigente (String respuestaPinpad)
    {
        Validacion valida = new Validacion();
        Tracks genTracks = new Tracks();
        String track = util.obtieneValorBuscaPropiedad("track", respuestaPinpad);
        genTracks = valida.validarTrack(track);
        return genTracks.getEsVigente();
    }
    
    public String getDataInC55 (String c55, String tagInicial, int length)
    {
        String valor = c55.replaceAll("^.*" + tagInicial + "\\d{2}", "");
        if(valor.length() >= length)
            valor = valor.substring(0, length);
        SION.log(Modulo.VENTA, "Obtieniendo valor para campo " + tagInicial 
                + "\nc55=" + c55 + "\n" 
                + tagInicial + "=" + valor, Level.INFO);
        return valor;
    }

    
}