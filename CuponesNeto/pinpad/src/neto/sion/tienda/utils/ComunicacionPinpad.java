package neto.sion.tienda.utils;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Level;
import neto.sion.tarjeta.chip.utilerias.UtileriasCadenas;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;

public class ComunicacionPinpad {
    
    String endOfProcess="EOP";
    UtileriasCadenas util = new UtileriasCadenas();
    
    public String lecturaDatosPinpad (Process p, int timeout)
    {
        StringBuilder mensaje = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String respuesta = "";

        if((timeout/1000) < 20)
            timeout = 20;
        
        if((timeout/1000) < 1)
            timeout *= 1000;

        TemporizadorBufferedReader timer = new TemporizadorBufferedReader(timeout);

        try
        {
            String linea;
            timer.setStream(br);
            timer.start();
            while((linea =  br.readLine()) != null && !linea.contains(endOfProcess))
                mensaje.append(linea).append("\n");
            if(util.isCadenaVacia(mensaje.toString()))
                mensaje.append("ERROR: No se recibieron datos");
            else
                timer.setStream(null);
            respuesta = mensaje.toString();
        }//try
        catch(Exception ex)
        {
            SION.log(Modulo.VENTA, "Ocurrio error al leer el resultado: " + ex.getMessage(), Level.SEVERE);
            respuesta = "ERROR: " + ex.getMessage();
        }//catch
        //
        SION.log(Modulo.VENTA, respuesta, Level.INFO);
        return respuesta;
    }//lecturaInfoTarjeta 
    
    public boolean envioDatosPinpad (String[] valores, Process p)
    {
        try
        {
            if(valores.length > 0)
            {
                BufferedWriter bw = new BufferedWriter (new OutputStreamWriter(p.getOutputStream()));
                for (String valor : valores)
                {
                    bw.write(valor);
                    bw.newLine();
                    bw.flush();
                }
                bw.write(endOfProcess);
                bw.newLine();
                bw.flush();
            }
            else
            {
                SION.log(Modulo.VENTA, "No hay valores por enviar a Pinpad", Level.SEVERE);
                return false;
            }
        }//try
        catch(Exception ex)
        {
            SION.log(Modulo.VENTA, "ERROR: No se realizó el envío de la respuesta al Interop [" + ex.getMessage() + "]", Level.SEVERE);
            return false;
        }//catch
        return true;
    }//envioInfoRespuesta
}
