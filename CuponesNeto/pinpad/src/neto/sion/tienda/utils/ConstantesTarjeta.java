package neto.sion.tienda.utils;

public class ConstantesTarjeta 
{
    public int tarjetaDebito = 1;
    public int tarjetaCredito = 2;
    public int tarjetaVales = 3;
    public int tarjetaValesGob = 4;
    public int lecturaBanda = 0;
    public int lecturaChip = 1;
    public int lecturaFallback = 2;
    public int ARQCLength = 16;
    public int AIDLength = 14;

    //public String C63Chip = "& 0000400058! Q100002 9 ! Q200002 04! C400012 000000020051";
    //public String C63Banda = "& 0000400058! Q100002 9 ! Q200002 04! C400012 000000023051";

    public String codigoRespuestaAprobado = "00";
    public String codigoRespHostExitoso = "00";
    
    public String opcionReiniciarDevcon = "restart";
    public String opcionDeshabilitarDevcon = "disable";
    public String opcionHabilitarDevcon = "enable";
    public String opcionStatusDevcon = "status";
    
    public String matarProcesoWindows = "taskkill /F /IM";
    public String buscaProcesoPinpadInterop = "tasklist /FI \"IMAGENAME eq";
}