package neto.sion.tienda.bean;

public class PeticionPagoTarjetaFrontBean{

    private long tiendaId;
    private int paisId;
    private double monto;
    
    //Número de Caja E.J. 01, 02, ...
    private String numTerminal;
    private boolean sePermitenVales;
    private boolean sePermitenBancarias;
    private String canalPago;
    private String afiliacion;

    public String getNumTerminal() {
        return numTerminal;
    }

    public void setNumTerminal(String numTerminal) {
        this.numTerminal = numTerminal;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public int getPaisId() {
        return paisId;
    }

    public void setPaisId(int paisId) {
        this.paisId = paisId;
    }

    public long getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(long tiendaId) {
        this.tiendaId = tiendaId;
    }
    
    public boolean isSePermitenBancarias() {
        return sePermitenBancarias;
    }

    public void setSePermitenBancarias(boolean sePermitenTarjetas) {
        this.sePermitenBancarias = sePermitenTarjetas;
    }

    public boolean isSePermitenVales() {
        return sePermitenVales;
    }

    public void setSePermitenVales(boolean sePermitenVales) {
        this.sePermitenVales = sePermitenVales;
    }
    
    public String getCanalPago() {
        return canalPago;
    }

    public void setCanalPago(String canalPago) {
        this.canalPago = canalPago;
    }
    
    public String getAfiliacion() {
        return afiliacion;
    }

    public void setAfiliacion(String afiliacion) {
        this.afiliacion = afiliacion;
    }

    @Override
    public String toString() {
        return "PeticionPagoTarjetaFrontBean{" 
                + "tiendaId=" + tiendaId 
                + ", paisId=" + paisId 
                + ", monto=" + monto 
                + ", canalPago=" + canalPago 
                + ", numTerminal=" + numTerminal 
                + ", sePermitenVales=" + sePermitenVales 
                + ", sePermitenBancarias=" + sePermitenBancarias 
                + '}';
    }
}