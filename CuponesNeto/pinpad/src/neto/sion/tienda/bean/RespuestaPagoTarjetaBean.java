/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.bean;

/**
 *
 * @author Administrador
 */
public class RespuestaPagoTarjetaBean {
    
    private String AuthorizationCode;
    private String C63;
    private String RespHost;
    private String ResponseCode;
    
    public RespuestaPagoTarjetaBean()
    {
        this.AuthorizationCode="NA";
        this.C63="NA";
        this.RespHost="NA";
        this.ResponseCode="NA";
    }

    public String getAuthorizationCode() {
        return AuthorizationCode;
    }

    public void setAuthorizationCode(String AuthorizationCode) {
        this.AuthorizationCode = AuthorizationCode;
    }

    public String getC63() {
        return C63;
    }

    public void setC63(String C63) {
        this.C63 = C63;
    }

    public String getRespHost() {
        return RespHost;
    }

    public void setRespHost(String RespHost) {
        this.RespHost = RespHost;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String ResponseCode) {
        this.ResponseCode = ResponseCode;
    }

    @Override
    public String toString() {
        return "RespuestaClienteTarjetaBean{" 
                + "C63=" + C63
                + ", RespHost=" + RespHost
                + ", AuthorizationCode=" + AuthorizationCode
                + ", ResponseCode=" + ResponseCode
                + '}';
    }
}