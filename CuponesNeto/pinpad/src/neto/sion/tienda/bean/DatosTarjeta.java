package neto.sion.tienda.bean;


public class DatosTarjeta {
    private String nombreTarjeta;
    private String track;
    
    public DatosTarjeta() {
    }
    
    public DatosTarjeta(String nombreTarjeta, String track) {
        this.nombreTarjeta = nombreTarjeta;
        this.track = track;
    }

    public String getNombreTarjeta() {
        return nombreTarjeta;
    }

    public void setNombreTarjeta(String nombreTarjeta) {
        this.nombreTarjeta = nombreTarjeta;
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    
    
}
