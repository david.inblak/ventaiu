package neto.sion.tienda.bean;

import neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto;
//import neto.sion.venta.tarjeta.pagatodo.iso8583.dto.PeticionPagatodoDto;
import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto.PeticionPagatodoNipDto;

public class RespuestaPagoTarjetaFrontBean{

    private int codError;
    private String descError;
    private String nombreTipoTarjeta;
    private String nombreUsuario;
    private String numAutorizacion;
    private String numeroTarjeta;
    private PeticionReversoTarjetaFrontBean peticionReverso;
    private int tipoTarjeta;
    private int tipoLectura;
    private String ARQC;
    private String AID;
    
    //private PeticionPagatodoDto solicitudPagaTdo;
    private PeticionPagatodoNipDto solicitudPagaTodoNip;
    private PeticionPagoTarjetaDto peticionPagoTarjeta;
    
    

    public RespuestaPagoTarjetaFrontBean()
    {
        super();
        peticionReverso = new PeticionReversoTarjetaFrontBean();
    }

    public int getCodError() {
        return codError;
    }

    public void setCodError(int codError) {
        this.codError = codError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    public String getNombreTipoTarjeta() {
        return nombreTipoTarjeta;
    }

    public void setNombreTipoTarjeta(String nombreTipoTarjeta) {
        this.nombreTipoTarjeta = nombreTipoTarjeta;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNumAutorizacion() {
        return numAutorizacion;
    }

    public void setNumAutorizacion(String numAutorizacion) {
        this.numAutorizacion = numAutorizacion;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public PeticionReversoTarjetaFrontBean getPeticionReverso() {
        return peticionReverso;
    }

    public void setPeticionReverso(PeticionReversoTarjetaFrontBean peticionReverso) {
        this.peticionReverso = peticionReverso;
    }

    public int getTipoTarjeta() {
        return tipoTarjeta;
    }

    public void setTipoTarjeta(int tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }
    
    public int getTipoLectura() {
        return tipoLectura;
    }

    public void setTipoLectura(int tipoLectura) {
        this.tipoLectura = tipoLectura;
    }
    
    public String getAID() {
        return AID;
    }

    public void setAID(String AID) {
        this.AID = AID;
    }

    public String getARQC() {
        return ARQC;
    }

    public void setARQC(String ARQC) {
        this.ARQC = ARQC;
    }
    
     public PeticionPagoTarjetaDto getPeticionPagoTarjeta() {
        return peticionPagoTarjeta;
    }

    public void setPeticionPagoTarjeta(PeticionPagoTarjetaDto peticionPagoTarjeta) {
        this.peticionPagoTarjeta = peticionPagoTarjeta;
    }

    /*public PeticionPagatodoDto getSolicitudPagaTdo() {
        return solicitudPagaTdo;
    }

    public void setSolicitudPagaTdo(PeticionPagatodoDto solicitudPagaTdo) {
        this.solicitudPagaTdo = solicitudPagaTdo;
    }*/
    
    
    public PeticionPagatodoNipDto getSolicitudPagaTodoNip(){
        return solicitudPagaTodoNip;
    }

    public void setSolicitudPagaTodoNip(PeticionPagatodoNipDto solicitudPagaTodoNip) {
        this.solicitudPagaTodoNip = solicitudPagaTodoNip;
    }

    @Override
    public String toString() {
        return "RespuestaPagoTarjetaFrontBean{" + "codError=" + codError + ", "
                                                + "descError=" + descError + ", "
                                                + "nombreTipoTarjeta=" + nombreTipoTarjeta + ", "
                                                + "nombreUsuario=" + nombreUsuario + ", "
                                                + "numAutorizacion=" + numAutorizacion + ", "
                                                + "numeroTarjeta=" + numeroTarjeta + ", "
                                                + "peticionReverso=" + peticionReverso + ", "
                                                + "tipoTarjeta=" + tipoTarjeta + ", "
                                                + "tipoLectura=" + tipoLectura + ", "
                                                + "ARQC=" + ARQC + ", "
                                                + "AID=" + AID + ", "
                                                //+ "solicitudPagaTdo=" + solicitudPagaTdo + ", "
                                                + "solicitudPagaTodoNip=" + solicitudPagaTodoNip + ", "
                                                + "peticionPagoTarjeta=" + peticionPagoTarjeta + '}';
    }

    
}