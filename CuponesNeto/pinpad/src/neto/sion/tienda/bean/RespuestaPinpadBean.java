package neto.sion.tienda.bean;

public class RespuestaPinpadBean {

    private String RespHost;
    private String AuthorizationCode;
    private String ResponseCode;

    public RespuestaPinpadBean() {}

    public RespuestaPinpadBean(String RespHost, String AuthorizationCode, String ResponseCode) {
        this.RespHost = RespHost;
        this.AuthorizationCode = AuthorizationCode;
        this.ResponseCode = ResponseCode;
    }

    public String getAuthorizationCode() {
        return AuthorizationCode;
    }

    public void setAuthorizationCode(String AuthorizationCode) {
        this.AuthorizationCode = AuthorizationCode;
    }

    public String getRespHost() {
        return RespHost;
    }

    public void setRespHost(String RespHost) {
        this.RespHost = RespHost;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String ResponseCode) {
        this.ResponseCode = ResponseCode;
    }

    @Override
    public String toString() {
        return "RespuestaPinpadBean{" 
                + "RespHost=" + RespHost 
                + ", AuthorizationCode=" + AuthorizationCode 
                + ", ResponseCode=" + ResponseCode 
                + '}';
    }
}
