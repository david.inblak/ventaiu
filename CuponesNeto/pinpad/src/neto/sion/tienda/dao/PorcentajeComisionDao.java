package neto.sion.tienda.dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import oracle.jdbc.OracleTypes;

public class PorcentajeComisionDao extends Conexion
{
    private int paSistemaId;
    private int paModuloId;
    private int paSubModuloId;
    private int paConfiguracionId;

    public double obtenerPorcentaje(int tipoTarjeta, int paPaisId)
    {
        SION.log(Modulo.VENTA, "Solicitud de calculo de comisión"
                + "\nTipo de tarjeta=" + tipoTarjeta
                + "\nPais=" + paPaisId, Level.INFO);

        String sql = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        double porcentaje = 0;

        try
        {
            switch(tipoTarjeta)
            {
                case 1:
                    paSistemaId = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA,"comisionTarjetas.sistema").trim());
                    paModuloId = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA,"comisionTarjetas.modulo").trim());
                    paSubModuloId = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA,"comisionTarjetas.submodulo").trim());
                    paConfiguracionId = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA,"comisionTarjetas.configuracion").trim());
                    break;
                case 2:
                    paSistemaId = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA,"comisionTarjetasDebito.sistema").trim());
                    paModuloId = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA,"comisionTarjetasDebito.modulo").trim());
                    paSubModuloId = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA,"comisionTarjetasDebito.submodulo").trim());
                    paConfiguracionId = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA,"comisionTarjetasDebito.configuracion").trim());
                    break;
                default:
                    return porcentaje;
            }

            sql = SION.obtenerParametro(Modulo.VENTA,"USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS");
            SION.log(Modulo.VENTA,"SQL a ejecutar... " + sql, Level.INFO);
            cs = Conexion.prepararLlamada(sql);

            cs.setInt(1, paPaisId);
            cs.setInt(2, paSistemaId);
            cs.setInt(3, paModuloId);
            cs.setInt(4, paSubModuloId);
            cs.setInt(5, paConfiguracionId);

            cs.registerOutParameter(6, OracleTypes.NUMBER);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.NUMBER);
            cs.execute();
            porcentaje = cs.getFloat(8);

            if(porcentaje > 1)
                porcentaje/=100.0;

        }//try
        catch(Exception e)
        {
            SION.logearExcepcion(Modulo.VENTA, e, sql);
        }//catch
        finally
        {
            Conexion.cerrarResultSet(rs);
            Conexion.cerrarCallableStatement(cs);
        }//finally
        SION.log(Modulo.VENTA, "Porcentaje obtenido: " + porcentaje, Level.INFO);
        return porcentaje;
    }//calculaComision
}