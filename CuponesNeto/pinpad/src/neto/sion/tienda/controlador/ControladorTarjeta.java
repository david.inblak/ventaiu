package neto.sion.tienda.controlador;

import com.sun.mail.iap.ConnectionException;
import java.io.EOFException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto;
import neto.sion.clientews.tarjeta.dto.RespuestaPagoTarjetaDto;
import neto.sion.clientews.tarjeta.dto.RespuestaReversoTarjetaDto;
import neto.sion.clientews.tarjeta.servicefacade.ConsumirTarjetaWSImpl;
import neto.sion.tarjeta.chip.utilerias.UtileriasCadenas;
import neto.sion.tarjeta.servicios.serviceFacade.ConsumirTarjetaImp;
import neto.sion.tarjeta.utilidades.OrdenaTracks;
import neto.sion.tarjeta.utilidades.Tracks;
import neto.sion.tarjeta.utilidades.Validacion;
import neto.sion.tienda.bean.*;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.utils.ComunicacionPinpad;
import neto.sion.tienda.utils.UtileriasConv;
import neto.sion.tienda.utils.UtileriasTarjeta;
/*import neto.sion.venta.tarjeta.pagatodo.iso8583.dto.PeticionPagatodoDto;
import neto.sion.venta.tarjeta.pagatodo.iso8583.dto.RespuestaPagatodoDto;
import neto.sion.venta.tarjeta.pagatodo.iso8583.dto.WSException;
import neto.sion.venta.tarjeta.pagatodo.iso8583.facade.ConsumirTarjetaPTdo_ISO8583;
import neto.sion.venta.tarjeta.pagatodo.iso8583.facade.ConsumirTarjetaPTdo_ISO8583Imp;*/
import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto.PeticionPagatodoNipDto;
import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto.WSException;
import org.apache.axis2.AxisFault;
//import temporal.RespuestaValidaTarjetaDescuentos;
//import temporal.SolicitudValidaTarjetaDescuentos;

public class ControladorTarjeta
{
    private int tEspera;
    private int nIntentos;
    private String strBinFile;
    private boolean lecturaFallback;
    private boolean lecturaChip;
    private boolean lecturaBanda;
    private boolean prosa;
    
    public static final int CONSULTA_PAGATODO = 3;

    public ControladorTarjeta() {
        this(Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "pinpad.lectura.tiempo.espera").trim()) > 0
                ? Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "pinpad.lectura.tiempo.espera").trim())
                : 30
                , Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "pinpad.lectura.intentos").trim()) > 0
                ? Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "pinpad.lectura.intentos").trim())
                : 3
                , SION.obtenerParametro(Modulo.VENTA, "pinpad.bin.chip")
                , SION.obtenerParametro(Modulo.VENTA, "pinpad.lectura.fallback").toLowerCase().matches(".*true.*")
                , SION.obtenerParametro(Modulo.VENTA, "pinpad.lectura.chip").toLowerCase().matches(".*true.*")
                , SION.obtenerParametro(Modulo.VENTA, "pinpad.lectura.banda").toLowerCase().matches(".*true.*")
                , SION.obtenerParametro(Modulo.VENTA, "pinpad.c63.prosa").toLowerCase().matches(".*true.*")
             );
    }

    public ControladorTarjeta(
            int _tEspera
            , int _nIntentos
            , String _strBinFile
            , boolean _lecturaFallback
            , boolean _lecturaChip
            , boolean _lecturaBanda
            , boolean _prosa
            )
    {
        SION.log(Modulo.VENTA, "Valores obtenidos del archivo properties:"
                + "\nTiempo de espera: " + _tEspera
                + ", " + "\nNúmero de intentos: " + _nIntentos
                + ", " + "\nRuta de interop: " + _strBinFile
                + ", " + "\nOperación fallback permitida: " + _lecturaFallback
                + ", " + "\nCampo C63 pruebas para PROSA: " + _prosa
                , Level.INFO);
        this.tEspera = _tEspera;
        this.nIntentos = _nIntentos;
        this.strBinFile = _strBinFile;
        this.lecturaFallback = _lecturaFallback;
        this.lecturaChip = _lecturaChip;
        this.lecturaBanda = _lecturaBanda;
        this.prosa = _prosa;
    }
    
    private String[] obtenerOctetosIP(String ip){
        SION.log(Modulo.VENTA, "IP: "+ip, Level.INFO);
        String []octetos = ip.split("\\.");
        SION.log(Modulo.VENTA, "octetos: "+octetos.length, Level.INFO);
        
        if( octetos.length <= 0 ){
            octetos = new String[]{"","","",""};
        }
        return octetos;
    }
    
    public RespuestaLeerTarjetaDescuentoBean leerTarjetaDescuento(){
        ComunicacionPinpad pinpad = new ComunicacionPinpad();
        UtileriasTarjeta utilTarjeta = new UtileriasTarjeta();
        UtileriasCadenas utilCadenas = new UtileriasCadenas();
        UtileriasConv utilConv = new UtileriasConv();
        RespuestaLeerTarjetaDescuentoBean respuesta = null;
        try
        {
            //Generación de cadena para ejecución del Interop "tEspera" [Opcional], "nIntentos" [Opcional], "lecturaChipPermitida" [Opcional], "fallbackPermitido" [Opcional].
            String comando = "cmd /c " 
                    + strBinFile //nombre del ejecutable
                    + " venta " //operación a realizar por interop
                    + tEspera //número que indica el tiempo de espera en segundos
                    + " " 
                    + nIntentos //número de intento a realizar en caso de fallo en la lectura de tarjeta (chip o banda)
                    + " " 
                    + lecturaChip //booleano que indica si se activa en la operación el lector de chip en la pinpad
                    + " " 
                    + lecturaFallback;//booleano que indica si se activa la operación de fallback (lectura de banda en tarjeta con chip en caso de fallo)

            //Ejecución del Interop
            Process process = Runtime.getRuntime().exec(comando);

            SION.log(Modulo.VENTA, "Ejecutando comando " + comando, Level.INFO);

            String respuestaPinpad = pinpad.lecturaDatosPinpad(process, (tEspera * nIntentos));
            
            SION.log(Modulo.VENTA, "Respuesta Pinpad:\n" + respuestaPinpad, Level.INFO);

            if(!respuestaPinpad.contains("ERROR"))
            {
                respuesta = new RespuestaLeerTarjetaDescuentoBean();
                String track = utilCadenas.obtieneValorBuscaPropiedad("Track", respuestaPinpad);
                boolean isChip = utilCadenas.obtieneValorBuscaPropiedad("Type_of_card", respuestaPinpad).equals("CHIP_EMV");
                boolean isFalloLecturaChip = utilCadenas.obtieneValorBuscaPropiedad("Tipo_Lectura", respuestaPinpad).equals("FALLBACK");

                //Campo 63 es asignado en central por una constante definida por banco para las operaciones con chip o banda magnetica
                
                if(isChip && !isFalloLecturaChip)
                {
                    track =  "ñ" + utilCadenas.rellenaCadenaCaracter(track, track.length(), "0", false) + "f_";
                    respuesta.setTrack1(track);
                    respuesta.setTrack2(track.replace("ñ","").replace("¿", "="));
                    SION.log(Modulo.VENTA, "Generación de track de solicitud [CHIP]..", Level.INFO);
                }//if
                else
                {
                    Validacion valida = new Validacion();
                    OrdenaTracks ordena = new OrdenaTracks();
                    Tracks genTracks = new Tracks();
                    track = ordena.resultadoOrdenar(track);
                    genTracks = valida.validarTrack(track);
                    String track1 = genTracks.getTrack1();
                    String track2 = genTracks.getTrack2();
                    if(utilCadenas.isCadenaVacia(track1))
                        track1=track2;
                    respuesta.setTrack1(track);
                    respuesta.setTrack2(track2.replace("ñ","").replace("¿", "="));
                    SION.log(Modulo.VENTA, "Fin de generación de track de solicitud[BANDA]..", Level.INFO);
                }
                
                int tipoTarjeta = utilTarjeta.obtieneTipoTarjeta(respuestaPinpad);
                boolean esTarjetaDescuento = utilTarjeta.esTarjetaDescuento(respuestaPinpad);

                String nombrePropietario = "N/A";
                String numeroTarjeta = "";

                if(utilCadenas.obtieneValorBuscaPropiedad("Type_of_card", respuestaPinpad).equals("CHIP_EMV")
                   && utilCadenas.obtieneValorBuscaPropiedad("Tipo_Lectura", respuestaPinpad).equals("NORMAL"))
                {
                    String mensajePinpad = "monto=" + 0 + "\n";
                    pinpad.envioDatosPinpad(mensajePinpad.split("\\n"), process);
                    respuestaPinpad = pinpad.lecturaDatosPinpad(process, tEspera);
                    SION.log(Modulo.VENTA, "respuestaPinpad2:\n" + respuestaPinpad, Level.INFO);
                }else{
                    nombrePropietario = utilTarjeta.obtenerNombrePropietario(respuestaPinpad);
                }
                
                numeroTarjeta = utilTarjeta.obtieneNumeroTarjeta(utilCadenas.obtieneValorBuscaPropiedad("track", respuestaPinpad));
                
                respuesta.setNombrePropietario(nombrePropietario);
                respuesta.setNumeroTarjeta(numeroTarjeta);
                respuesta.setTipoTarjeta(tipoTarjeta);
                respuesta.setEsTarjetaDescuento(esTarjetaDescuento);
                respuesta.setEmisorTarjetaDescuento("Capital Social");
            }else{
                respuesta.setIdError(-1);
                respuesta.setMensajeError("No fue posible leer los datos de la tarjeta por favor intente nuevamente.  [CODIGO:5]");
                SION.logearExcepcion(Modulo.VENTA, new Exception("[CODIGO:5] " + respuestaPinpad), "");
            }
	}catch(AxisFault af)
        {
            respuesta.setIdError(-6);
            respuesta.setMensajeError("Se presentó un problema de comunicación. Favor de intentarlo nuevamente. [CODIGO:6]");
            SION.logearExcepcion(Modulo.VENTA, af, "[CODIGO:6] " + af.getMessage());
        }
        catch(Exception ex)
        {
            respuesta.setIdError(-7);
            respuesta.setMensajeError("Ocurrió un error al realizar la lectura de la tarjeta. Favor de intentarlo nuevamente. [CODIGO:7]");
            SION.logearExcepcion(Modulo.VENTA, ex, "[CODIGO:7] " + ex.getMessage());
        }
        catch (Error e)
        {
            respuesta.setIdError(-8);
            respuesta.setMensajeError("Ocurrió un error al realizar la lectura de la tarjeta. Favor de intentarlo nuevamente. [CODIGO:8]");
            SION.logearExcepcion(Modulo.VENTA, new Exception(e.getMessage()), "[CODIGO:8] " + e.getMessage());
            
        }
        return respuesta;
    }
    
    
    public RespuestaPagoTarjetaFrontBean peticionPago(PeticionPagoTarjetaFrontBean peticionPagoFront){
        
        //solicitudPagaTdo = new PeticionPagatodoDto();
        PeticionPagoTarjetaDto peticionPago = new PeticionPagoTarjetaDto();
        
        UtileriasTarjeta utilTarjeta = new UtileriasTarjeta();
        UtileriasCadenas utilCadenas = new UtileriasCadenas();
        UtileriasConv utilConv = new UtileriasConv();
        
        RespuestaPagoTarjetaDto respuestaPago = new RespuestaPagoTarjetaDto();
        ComunicacionPinpad pinpad = new ComunicacionPinpad();
        RespuestaPinpadBean respuestaPinpadBean;
        RespuestaPagoTarjetaFrontBean respuestaFrontPago = new RespuestaPagoTarjetaFrontBean();

        try
        {
            //Generación de cadena para ejecución del Interop "tEspera" [Opcional], "nIntentos" [Opcional], "lecturaChipPermitida" [Opcional], "fallbackPermitido" [Opcional].
            String comando = "cmd /c " 
                    + strBinFile //nombre del ejecutable
                    + " venta " //operación a realizar por interop
                    + tEspera //número que indica el tiempo de espera en segundos
                    + " " 
                    + nIntentos //número de intento a realizar en caso de fallo en la lectura de tarjeta (chip o banda)
                    + " " 
                    + lecturaChip //booleano que indica si se activa en la operación el lector de chip en la pinpad
                    + " " 
                    + lecturaFallback;//booleano que indica si se activa la operación de fallback (lectura de banda en tarjeta con chip en caso de fallo)

            //Ejecución del Interop
            Process process = Runtime.getRuntime().exec(comando);

            SION.log(Modulo.VENTA, "Ejecutando comando " + comando, Level.INFO);

            String respuestaPinpad = pinpad.lecturaDatosPinpad(process, (tEspera * nIntentos));
            
            SION.log(Modulo.VENTA, "Respuesta Pinpad:\n" + respuestaPinpad, Level.INFO);

            if(!respuestaPinpad.contains("ERROR"))
            {
                
                int tipoTarjeta = utilTarjeta.obtieneTipoTarjeta(respuestaPinpad);

                if(!utilTarjeta.operacionPermitida(peticionPagoFront.isSePermitenBancarias(), peticionPagoFront.isSePermitenVales(), tipoTarjeta))
                {
                    respuestaFrontPago.setCodError(-1);
                    if(tipoTarjeta < 1 )
                    {
                        respuestaFrontPago.setDescError("Ocurrió un error al realizar el pago. Favor de intentarlo nuevamente. [OBTENCION TIPO TARJETA]");
                        SION.logearExcepcion(Modulo.VENTA, new Exception(respuestaFrontPago.getDescError()+ "[OBTENCION DE TIPO DE TARJETA]"));
                    }
                    else
                        respuestaFrontPago.setDescError("ERROR: Se supero el limite de tarjetas permitidas");
                    SION.log(Modulo.VENTA, "Error durante validación de tipo y número de tarjetas permitidas:\n" + respuestaFrontPago.toString(), Level.INFO);
                    return respuestaFrontPago;
                }

                double comision = utilTarjeta.calculaComision(tipoTarjeta, peticionPagoFront.getMonto(), peticionPagoFront.getPaisId());

                if(comision < 0 )
                {
                    respuestaFrontPago.setCodError(-1);
                    respuestaFrontPago.setDescError("Ocurrió un error al realizar el pago. Favor de intentarlo nuevamente. [CALCULO COMISIÓN]");
                    SION.logearExcepcion(Modulo.VENTA, new Exception(respuestaFrontPago.getDescError() + "[CALCULO DE COMISIÓN]"));
                    return respuestaFrontPago;
                }

                String nombrePropietario = "N/A";
                String numeroTarjeta = "";

                if(utilCadenas.obtieneValorBuscaPropiedad("Type_of_card", respuestaPinpad).equals("CHIP_EMV")
                   && utilCadenas.obtieneValorBuscaPropiedad("Tipo_Lectura", respuestaPinpad).equals("NORMAL"))
                {
                    String mensajePinpad = "monto=" + String.valueOf(peticionPagoFront.getMonto() + comision) + "\n";
                    pinpad.envioDatosPinpad(mensajePinpad.split("\\n"), process);
                    respuestaPinpad = pinpad.lecturaDatosPinpad(process, tEspera);
                    SION.log(Modulo.VENTA, "respuestaPinpad2:\n" + respuestaPinpad, Level.INFO);
                }
                else
                    nombrePropietario = utilTarjeta.obtenerNombrePropietario(respuestaPinpad);

                numeroTarjeta = utilTarjeta.obtieneNumeroTarjeta(utilCadenas.obtieneValorBuscaPropiedad("track", respuestaPinpad));

                //Llenado de solicitud con información de venta y tarjeta
                peticionPago = utilTarjeta.asignarValoresSolicitudPago(respuestaPinpad, peticionPagoFront, tipoTarjeta, comision, prosa);
                

                SION.log(Modulo.VENTA, "Solicitud de pago generada:\n" + peticionPago.toString(), Level.INFO);

                
                boolean esTarjetaPagaTodo = false;
                
                boolean esTarjetaDescuento = false;
                 
                esTarjetaPagaTodo = utilTarjeta.esTarjetaPagaTodo(respuestaPinpad);
                
                if( esTarjetaPagaTodo ){
                    
                    respuestaFrontPago.setPeticionPagoTarjeta(utilTarjeta.asignarValoresSolicitudPago(respuestaPinpad, peticionPagoFront, tipoTarjeta, comision, prosa));
                    
                    PeticionPagoTarjetaDto _peticionPago = respuestaFrontPago.getPeticionPagoTarjeta();
                    
                    SION.log(Modulo.VENTA, "Es una tarjeta paga todo...", Level.INFO);
                    String COD_RESP_CORRECTA_PTODO = "00";
                    
                    Date ahora = new Date();
                    DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(Locale.getDefault());
                    decimalFormatSymbols.setDecimalSeparator('.');
                    DecimalFormat decimalFormat = new DecimalFormat("#.00", decimalFormatSymbols);
                    SimpleDateFormat dateF = new SimpleDateFormat("yyMMddHHmmss");
                    PeticionPagoTarjetaDto peticionPagoTarjetaDto = utilTarjeta.asignarValoresSolicitudPago(respuestaPinpad, peticionPagoFront, tipoTarjeta, comision, prosa);
                    
                    
                    respuestaFrontPago.setPeticionPagoTarjeta(peticionPagoTarjetaDto);
                    
                    
                    respuestaFrontPago.setSolicitudPagaTodoNip(new PeticionPagatodoNipDto());
                    
                    
                    String track2 = _peticionPago.getTrack2().replace("ñ","").replace("¿", "=");
                    
                    /*if (track2.contains("63950900")){
                        respuestaFrontPago.getSolicitudPagaTodoNip().setReferencia("6395092490004881=290550100000645");
                    }else{*/
                        respuestaFrontPago.getSolicitudPagaTodoNip().setReferencia(_peticionPago.getTrack2().replace("ñ","").replace("¿", "=") );
                    //}
                    
                    
                    respuestaFrontPago.getSolicitudPagaTodoNip().setMonto(decimalFormat.format(_peticionPago.getMonto()) );
                    respuestaFrontPago.getSolicitudPagaTodoNip().setTipoTarjetaId(tipoTarjeta);
                    respuestaFrontPago.getSolicitudPagaTodoNip().setNoTicket(dateF.format(ahora));
                    
                    
                    String []octetos = _peticionPago.getIpTerminal().split("\\.");
                    String ultimoOcteto = "";
                    if( octetos.length > 0 ){
                        ultimoOcteto = octetos[octetos.length -1];
                    }
                    
                    respuestaFrontPago.getSolicitudPagaTodoNip().setIdTerminal( ultimoOcteto );
                    respuestaFrontPago.getSolicitudPagaTodoNip().setIdSucursal( Long.toString(_peticionPago.getTiendaId()) );
                    respuestaFrontPago.setCodError(0);
                    respuestaFrontPago.setDescError("");
                    respuestaFrontPago.setNumAutorizacion("");
                    respuestaFrontPago.setTipoTarjeta(tipoTarjeta);
                    respuestaFrontPago.setNombreUsuario(nombrePropietario);
                    respuestaFrontPago.setNombreTipoTarjeta("PAGATODO");
                    respuestaFrontPago.setNumeroTarjeta(numeroTarjeta);
                    
                    SION.log(Modulo.VENTA, "respuestaFrontPago: "+respuestaFrontPago, Level.INFO);
                    
                    
                    
                    
                }else{
                    
                    
                        if(lecturaBanda 
                            && !peticionPago.isChip()
                            && !peticionPago.isFalloLecturaChip())
                            {
                                ConsumirTarjetaImp clienteBanda = new ConsumirTarjetaImp();
                                SION.log(Modulo.VENTA, "Petición pago generada [solo banda]", Level.INFO);

                                respuestaPago = utilConv.respuestaPagoDto2RespuestaPagoTarjetaDto(clienteBanda.aplicaPagoTarjeta(utilConv.peticionPagoTarjetaDto2PeticionPagoDto(peticionPago)));
                            }
                            else
                            {
                                ConsumirTarjetaWSImpl clienteChip = new ConsumirTarjetaWSImpl();
                                SION.log(Modulo.VENTA, "Petición pago generada [chip & banda]: "+peticionPago.toString(), Level.INFO);

                                respuestaPago = clienteChip.solicitudPagoTarjeta(peticionPago);
                                //respuestaPago.setNoAutorizacion(respuestaPago.getNoAutorizacion()+"X");
                            } 



                            SION.log(Modulo.VENTA, "Respuesta recibida:\n" + respuestaPago.toString(), Level.INFO);
                            //Solicitud de pago y recepción de respuesta del cliente
                            switch(respuestaPago.getCodError())
                            {
                                case 0:
                                    //if(respuestaPago.getCodRespuesta().equals("00") && respuestaPago.getNoAutorizacion().matches("\\d{1,}")) "[\\d|a-zA-Z]{1,}"
                                    if(respuestaPago.getCodRespuesta().equals("00") && respuestaPago.getNoAutorizacion().matches("[\\d|a-zA-Z]{1,}"))
                                    {//respuestaPago.setNoAutorizacion(respuestaPago.getNoAutorizacion()+"X");
                                        if(peticionPago.isChip() && !peticionPago.isFalloLecturaChip())
                                        {

                                            respuestaPinpadBean = new RespuestaPinpadBean("00", respuestaPago.getNoAutorizacion(), respuestaPago.getCodRespuesta());
                                            pinpad.envioDatosPinpad(respuestaPinpadBean.toString().split("\\n"), process);

                                            int cont = 0;

                                            while((respuestaPinpad = pinpad.lecturaDatosPinpad(process, tEspera)).contains("ERROR") && ++cont < nIntentos);

                                            respuestaFrontPago = utilConv.generaRespuestaFront(respuestaPinpad);
                                        }
                                        else
                                        {
                                            respuestaFrontPago.setCodError(0);
                                            respuestaFrontPago.setDescError("Operación finalizada");
                                        }

                                        respuestaFrontPago.setNumAutorizacion(respuestaPago.getNoAutorizacion());

                                        respuestaFrontPago.setTipoTarjeta(tipoTarjeta);

                                        respuestaFrontPago.setNombreUsuario(nombrePropietario);

                                        respuestaFrontPago.setTipoLectura(utilTarjeta.obtieneTipoLectura(peticionPago.isChip(), peticionPago.isFalloLecturaChip()));

                                        //Enmascara el número de la tarjeta
                                        if(numeroTarjeta.length() >= 6)
                                            respuestaFrontPago.setNumeroTarjeta(
                                                    utilCadenas.rellenaCadenaCaracter(
                                                        numeroTarjeta.substring(numeroTarjeta.length() - 4)
                                                        , 16
                                                        , "*"
                                                        , true)
                                                    );

                                        if(!utilCadenas.isCadenaVacia(peticionPago.getC55()) && peticionPago.getC55().length() > 16)
                                        {
                                            respuestaFrontPago.setARQC(utilTarjeta.getDataInC55(peticionPago.getC55(), "9F26", 16));
                                            respuestaFrontPago.setAID(utilTarjeta.getDataInC55(peticionPago.getC55(), "4F", 14));
                                        }
                                        else
                                        {
                                            respuestaFrontPago.setARQC("");
                                            respuestaFrontPago.setAID("");
                                        }

                                        PeticionReversoTarjetaFrontBean reverso = new PeticionReversoTarjetaFrontBean();

                                        SION.log(Modulo.VENTA, "Generación reverso respuesta front:\n" 
                                                + peticionPago.toString() + "\n"
                                                + respuestaPago.toString() + "\n"
                                                + peticionPagoFront.toString() + "\n"
                                                , Level.INFO);
                                        reverso = utilConv.generarPeticionReverso(peticionPago, respuestaPago, peticionPagoFront);
                                        SION.log(Modulo.VENTA, "Reverso respuesta front generada:\n" + reverso.toString(), Level.INFO);
                                        respuestaFrontPago.setPeticionReverso(reverso);

                                        if(respuestaFrontPago.getCodError() != 0)
                                        {
                                            this.reversoPago(respuestaFrontPago);
                                            respuestaFrontPago.setPeticionReverso(new PeticionReversoTarjetaFrontBean());
                                        }
                                    }
                                    else
                                    {
                                        respuestaFrontPago.setCodError(-1);
                                        respuestaFrontPago.setDescError(respuestaPago.getMensaje());
                                    }
                                    break;
                                case -2:
                                    respuestaFrontPago.setCodError(respuestaPago.getCodError());
                                    if(respuestaPago.getDescError().contains("|"))
                                    {
                                        String temp = respuestaPago.getDescError().replaceFirst("\\|", "");
                                        String[] tmp = temp.split("\\|");
                                        respuestaFrontPago.setDescError(tmp[0]);
                                    }
                                    else
                                        respuestaFrontPago.setDescError("Ocurrió un error al realizar el pago. Favor de intentarlo nuevamente. [CODIGO:" + respuestaPago.getCodError()+"]");
                                    SION.logearExcepcion(Modulo.VENTA, new Exception(respuestaPago.getDescError()), "[CODIGO:" + respuestaPago.getCodError()+ "]");
                                    break;

                                default:
                                    respuestaFrontPago.setCodError(respuestaPago.getCodError());
                                    respuestaFrontPago.setDescError("Ocurrió un error al realizar el pago. Favor de intentarlo nuevamente. [CODIGO:" + respuestaPago.getDescError()+"]");
                                    SION.logearExcepcion(Modulo.VENTA, new Exception(respuestaPago.getDescError()), "[CODIGO:" + respuestaPago.getCodError()+ "]");
                                    break;
                            }
                        
                    }
                
                
            }
            else
            {
                respuestaFrontPago.setCodError(-5);//Ocurrio un error al leer la tarjeta, por favor intente de nuevo
                respuestaFrontPago.setDescError("Ocurrio un error al leer la tarjeta, por favor intente nuevamente. [CODIGO:5]");
                //respuestaFrontPago.setDescError("Parece haber un problema con la configuración de la terminal pinpad, por favor pongase en contacto con soporte técnico. [CODIGO:5]");
                SION.logearExcepcion(Modulo.VENTA, new Exception("[CODIGO:5] " + respuestaPinpad), "");
            }
            //Notificación de error u operación declinada a pinpad
            if(respuestaFrontPago.getCodError() != 0 && (peticionPago.isChip() && !peticionPago.isFalloLecturaChip()))
            {
                respuestaPinpadBean = new RespuestaPinpadBean("", "", "");
                pinpad.envioDatosPinpad(respuestaPinpadBean.toString().split("\\n"), process);
            }
            process.destroy();
        }
        catch(WSException wse)
        {
            respuestaFrontPago.setCodError(-9);
            String descripcionError = manejaCausaExcepcion(wse.getCause());
            respuestaFrontPago.setDescError("[CODIGO:9] "+descripcionError);
            SION.logearExcepcion(Modulo.VENTA, wse, "[CODIGO:9] " + wse.getMessage());
        }
        catch(AxisFault af)
        {
            respuestaFrontPago.setCodError(-6);
            SION.logearExcepcion(Modulo.VENTA, af, "[CODIGO:6] " + af.getMessage());
            String descripcionError = descripcionError = manejaCausaExcepcion(af);
            respuestaFrontPago.setDescError("[CODIGO:6] "+descripcionError);
        }catch(IOException ex){
            respuestaFrontPago.setCodError(-10);
             String descripcionError = manejaCausaExcepcion(ex);
            respuestaFrontPago.setDescError(" [CODIGO:10] "+descripcionError);
            respuestaFrontPago.setDescError(" [CODIGO:10] "+descripcionError);
        }catch(Exception ex)
        {
            respuestaFrontPago.setCodError(-7);
            respuestaFrontPago.setDescError("Ocurrió un error al realizar el pago. Favor de intentarlo nuevamente. [CODIGO:7]");
            SION.logearExcepcion(Modulo.VENTA, ex, "[CODIGO:7] " + ex.getMessage());
            //ex.printStackTrace();
        }
        catch (Error e)
        {
            respuestaFrontPago.setCodError(-8);
            respuestaFrontPago.setDescError("Ocurrió un error inesperado al realizar el pago. Favor de intentarlo nuevamente. [CODIGO:8]");
            SION.logearExcepcion(Modulo.VENTA, new Exception(e.getMessage()), "[CODIGO:8] " + e.getMessage());
            //e.printStackTrace();
        }
        
        
        SION.log(Modulo.VENTA, "Respuesta de operación:\n" + respuestaPago.toString(), Level.INFO);
        SION.log(Modulo.VENTA, "Respuesta para front:\n" + respuestaFrontPago.toString(), Level.INFO);
        
        
        return respuestaFrontPago;
    }//peticionPago
    
    
    
        
    
    /*public RespuestaPagoTarjetaFrontBean peticionPago(PeticionPagoTarjetaFrontBean peticionPagoFront){
        
        //solicitudPagaTdo = new PeticionPagatodoDto();
        PeticionPagoTarjetaDto peticionPago = new PeticionPagoTarjetaDto();
        
        UtileriasTarjeta utilTarjeta = new UtileriasTarjeta();
        UtileriasCadenas utilCadenas = new UtileriasCadenas();
        UtileriasConv utilConv = new UtileriasConv();
        
        RespuestaPagoTarjetaDto respuestaPago = new RespuestaPagoTarjetaDto();
        ComunicacionPinpad pinpad = new ComunicacionPinpad();
        RespuestaPinpadBean respuestaPinpadBean;
        RespuestaPagoTarjetaFrontBean respuestaFrontPago = new RespuestaPagoTarjetaFrontBean();

        try
        {
            //Generación de cadena para ejecución del Interop "tEspera" [Opcional], "nIntentos" [Opcional], "lecturaChipPermitida" [Opcional], "fallbackPermitido" [Opcional].
            String comando = "cmd /c " 
                    + strBinFile //nombre del ejecutable
                    + " venta " //operación a realizar por interop
                    + tEspera //número que indica el tiempo de espera en segundos
                    + " " 
                    + nIntentos //número de intento a realizar en caso de fallo en la lectura de tarjeta (chip o banda)
                    + " " 
                    + lecturaChip //booleano que indica si se activa en la operación el lector de chip en la pinpad
                    + " " 
                    + lecturaFallback;//booleano que indica si se activa la operación de fallback (lectura de banda en tarjeta con chip en caso de fallo)

            //Ejecución del Interop
            Process process = Runtime.getRuntime().exec(comando);

            SION.log(Modulo.VENTA, "Ejecutando comando " + comando, Level.INFO);

            String respuestaPinpad = pinpad.lecturaDatosPinpad(process, (tEspera * nIntentos));
            
            SION.log(Modulo.VENTA, "Respuesta Pinpad:\n" + respuestaPinpad, Level.INFO);

            if(!respuestaPinpad.contains("ERROR"))
            {
                
                int tipoTarjeta = utilTarjeta.obtieneTipoTarjeta(respuestaPinpad);

                if(!utilTarjeta.operacionPermitida(peticionPagoFront.isSePermitenBancarias(), peticionPagoFront.isSePermitenVales(), tipoTarjeta))
                {
                    respuestaFrontPago.setCodError(-1);
                    if(tipoTarjeta < 1 )
                    {
                        respuestaFrontPago.setDescError("Ocurrió un error al realizar el pago. Favor de intentarlo nuevamente. [OBTENCION TIPO TARJETA]");
                        SION.logearExcepcion(Modulo.VENTA, new Exception(respuestaFrontPago.getDescError()+ "[OBTENCION DE TIPO DE TARJETA]"));
                    }
                    else
                        respuestaFrontPago.setDescError("ERROR: Se supero el limite de tarjetas permitidas");
                    SION.log(Modulo.VENTA, "Error durante validación de tipo y número de tarjetas permitidas:\n" + respuestaFrontPago.toString(), Level.INFO);
                    return respuestaFrontPago;
                }

                double comision = utilTarjeta.calculaComision(tipoTarjeta, peticionPagoFront.getMonto(), peticionPagoFront.getPaisId());

                if(comision < 0 )
                {
                    respuestaFrontPago.setCodError(-1);
                    respuestaFrontPago.setDescError("Ocurrió un error al realizar el pago. Favor de intentarlo nuevamente. [CALCULO COMISIÓN]");
                    SION.logearExcepcion(Modulo.VENTA, new Exception(respuestaFrontPago.getDescError() + "[CALCULO DE COMISIÓN]"));
                    return respuestaFrontPago;
                }

                String nombrePropietario = "N/A";
                String numeroTarjeta = "";

                if(utilCadenas.obtieneValorBuscaPropiedad("Type_of_card", respuestaPinpad).equals("CHIP_EMV")
                   && utilCadenas.obtieneValorBuscaPropiedad("Tipo_Lectura", respuestaPinpad).equals("NORMAL"))
                {
                    String mensajePinpad = "monto=" + String.valueOf(peticionPagoFront.getMonto() + comision) + "\n";
                    pinpad.envioDatosPinpad(mensajePinpad.split("\\n"), process);
                    respuestaPinpad = pinpad.lecturaDatosPinpad(process, tEspera);
                }
                else
                    nombrePropietario = utilTarjeta.obtenerNombrePropietario(respuestaPinpad);

                numeroTarjeta = utilTarjeta.obtieneNumeroTarjeta(utilCadenas.obtieneValorBuscaPropiedad("track", respuestaPinpad));

                //Llenado de solicitud con información de venta y tarjeta
                peticionPago = utilTarjeta.asignarValoresSolicitudPago(respuestaPinpad, peticionPagoFront, tipoTarjeta, comision, prosa);
                

                SION.log(Modulo.VENTA, "Solicitud de pago generada:\n" + peticionPago.toString(), Level.INFO);

                
                boolean esTarjetaPagaTodo = false;
                boolean haySaldoPagaTodo = false;
                
                boolean esTarjetaDescuento = false;
                 
                esTarjetaPagaTodo = utilTarjeta.esTarjetaPagaTodo(respuestaPinpad);
                
                if( esTarjetaPagaTodo ){
                    respuestaFrontPago.setSolicitudPagaTdo(new PeticionPagatodoDto());
                    respuestaFrontPago.setPeticionPagoTarjeta(
                            utilTarjeta.asignarValoresSolicitudPago(respuestaPinpad, peticionPagoFront, tipoTarjeta, comision, prosa));
                    
                    PeticionPagoTarjetaDto _peticionPago = respuestaFrontPago.getPeticionPagoTarjeta();
                    
                    SION.log(Modulo.VENTA, "Es una tarjeta paga todo...", Level.INFO);
                    String COD_RESP_CORRECTA_PTODO = "00";
                    
                    Date ahora = new Date();
                    DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(Locale.getDefault());
                    decimalFormatSymbols.setDecimalSeparator('.');
                    DecimalFormat decimalFormat = new DecimalFormat("#.00", decimalFormatSymbols);
                    //DecimalFormat decimalFormat = new DecimalFormat("###.##");
                    SimpleDateFormat dateF = new SimpleDateFormat("yyMMddHHmmss");
                    
                    
                    ConsumirTarjetaPTdo_ISO8583 clientePagaTodo = new ConsumirTarjetaPTdo_ISO8583Imp();
                    RespuestaPagatodoDto respPTdo=null;
                    try{
                        SION.log(Modulo.VENTA, "IP: "+_peticionPago.getIpTerminal(), Level.INFO);
                        String []octetos = _peticionPago.getIpTerminal().split("\\.");
                        SION.log(Modulo.VENTA, "octetos: "+octetos.length, Level.INFO);
                        String ultimoOcteto = "";
                        if( octetos.length > 0 ){
                            ultimoOcteto = octetos[octetos.length -1];
                        }
                        
                        respuestaFrontPago.getSolicitudPagaTdo().setReferencia(_peticionPago.getTrack2().replace("ñ","").replace("¿", "=") );
                        respuestaFrontPago.getSolicitudPagaTdo().setMonto(decimalFormat.format(_peticionPago.getMonto()) );
                        respuestaFrontPago.getSolicitudPagaTdo().setTipoTarjetaId(tipoTarjeta);
                        respuestaFrontPago.getSolicitudPagaTdo().setNoTicket(dateF.format(ahora));
                        respuestaFrontPago.getSolicitudPagaTdo().setIdTerminal( ultimoOcteto );
                        respuestaFrontPago.getSolicitudPagaTdo().setIdSucursal( Long.toString(_peticionPago.getTiendaId()) );
                        
                        SION.log(Modulo.VENTA, "Inicia consulta paga todo...", Level.INFO);
                        respPTdo = clientePagaTodo.consultaSaldoPTdo(respuestaFrontPago.getSolicitudPagaTdo());
                        SION.log(Modulo.VENTA, "Respuesta: "+respPTdo.toString(), Level.INFO);
                        SION.log(Modulo.VENTA, "Consulta paga todo realizada sin errores...", Level.INFO);
                    }catch(WSException e){
                        SION.logearExcepcion(Modulo.VENTA, e, "Error al consultar el cliente > "+e.getMessage());
                        //throw e;
                    }
                    
                    if( respPTdo == null  ){
                        respPTdo = new RespuestaPagatodoDto();
                        SION.log(Modulo.VENTA, "Consulta de saldo con errores.", Level.INFO);
                        respuestaPago.setCodError(-1);
                        respuestaPago.setCodRespuesta("01");
                        respuestaPago.setMensaje("No fue posible la comunicación con PAGA TODO, por favor intente más tarde.");
                    }else if( COD_RESP_CORRECTA_PTODO.equals(respPTdo.getrCodigoRespuesta())){
                        SION.log(Modulo.VENTA, "Consulta de saldo exitosa.", Level.INFO);
                        Double importeCientos = Double.parseDouble(respPTdo.getrSaldoDisponibleTarjeta());
                        DecimalFormat sbFormat = new DecimalFormat("#,###.00");
                        Double saldoDisponible= importeCientos/100;
                        SION.log(Modulo.VENTA, "saldo disponible: "+saldoDisponible, Level.INFO);
                        
                        haySaldoPagaTodo = saldoDisponible > 0 && _peticionPago.getMonto() <= saldoDisponible;
                        SION.log(Modulo.VENTA, "Hay saldo suficiente: "+haySaldoPagaTodo, Level.INFO);
                        if( haySaldoPagaTodo ){
                            respuestaPago.setCodError(0);
                            respuestaPago.setCodRespuesta(COD_RESP_CORRECTA_PTODO);
                            respuestaFrontPago.setCodError(0);
                            respuestaFrontPago.setDescError(respPTdo.getrDescRespuesta());                            
                            //respPTdo.getrNoSecUnicoPT()
                            respuestaPago.setMonto( decimalFormat.format(_peticionPago.getMonto()) );
                            respuestaPago.setMensaje( respPTdo.getrDescRespuesta() );
                            //respuestaPago.setNoAutorizacion( respPTdo.getrNoConfirmacion() );
                            respuestaPago.setCodRespuesta("00");
                            respuestaPago.setBanco("PAGATODO");
                            
                            //respuestaFrontPago.getSolicitudPagaTdo().setReferencia(_peticionPago.getTrack2() );
                            respuestaFrontPago.getSolicitudPagaTdo().setReferencia(_peticionPago.getTrack2().replace("ñ","").replace("¿", "=") );
                            respuestaFrontPago.getSolicitudPagaTdo().setMonto(decimalFormat.format(_peticionPago.getMonto()) );
                        }else{
                            SION.log(Modulo.VENTA, "Saldo insuficiente.\n Saldo disponible.$ "+sbFormat.format(saldoDisponible), Level.INFO);
                            respuestaPago.setCodError(-1);
                            respuestaPago.setCodRespuesta("-1");
                            respuestaPago.setMensaje("Saldo insuficiente.\n Saldo disponible: $ "+sbFormat.format(saldoDisponible));
                            respuestaFrontPago.setCodError(-1);
                            respuestaFrontPago.setDescError("Saldo insuficiente.\n Saldo disponible: $ "+sbFormat.format(saldoDisponible));
                            
                            respPTdo.setrDescRespuesta("Saldo insuficiente.\n Saldo disponible: $ "+sbFormat.format(saldoDisponible));
                            //throw new Exception("Saldo insuficiente. Saldo disponible: "+sbFormat.format(saldoDisponible), new Exception());
                        }
                        
                    }   else{
                        SION.log(Modulo.VENTA, "Respuesta no buena...", Level.INFO);
                        respuestaPago.setCodError(-1);
                        respuestaPago.setCodRespuesta("-1");
                        respuestaPago.setMensaje(respPTdo.getrDescRespuesta());
                        respuestaFrontPago.setCodError(-1);
                        respuestaFrontPago.setDescError(respPTdo.getrDescRespuesta());
                        //respPTdo = new RespuestaPagatodoDto();
                        //SION.log(Modulo.VENTA, "Consulta de saldo con errores.", Level.INFO);
                        //throw new WSException("No fue posible realizar el cobro. "+ respPTdo, 
                                //new Exception("No fue posible realizar el cobro. "+ respPTdo));
                    }
                    
                    respuestaFrontPago.setNumAutorizacion(respPTdo.getrNoConfirmacion());
                    respuestaFrontPago.setTipoTarjeta(tipoTarjeta);
                    respuestaFrontPago.setNombreUsuario(nombrePropietario);
                    respuestaFrontPago.setDescError(respPTdo.getrDescRespuesta());
                    respuestaFrontPago.setNombreTipoTarjeta("PAGATODO");
                    respuestaFrontPago.setNumeroTarjeta(numeroTarjeta);
                    
                    SION.log(Modulo.VENTA, "respuestaFrontPago: "+respuestaFrontPago, Level.INFO);
                }else{
                    
                    
                        if(lecturaBanda 
                            && !peticionPago.isChip()
                            && !peticionPago.isFalloLecturaChip())
                            {
                                ConsumirTarjetaImp clienteBanda = new ConsumirTarjetaImp();
                                SION.log(Modulo.VENTA, "Petición pago generada [solo banda]", Level.INFO);

                                respuestaPago = utilConv.respuestaPagoDto2RespuestaPagoTarjetaDto(clienteBanda.aplicaPagoTarjeta(utilConv.peticionPagoTarjetaDto2PeticionPagoDto(peticionPago)));
                            }
                            else
                            {
                                ConsumirTarjetaWSImpl clienteChip = new ConsumirTarjetaWSImpl();
                                SION.log(Modulo.VENTA, "Petición pago generada [chip & banda]: "+peticionPago.toString(), Level.INFO);

                                respuestaPago = clienteChip.solicitudPagoTarjeta(peticionPago);
                                //respuestaPago.setNoAutorizacion(respuestaPago.getNoAutorizacion()+"X");
                            } 



                            SION.log(Modulo.VENTA, "Respuesta recibida:\n" + respuestaPago.toString(), Level.INFO);
                            //Solicitud de pago y recepción de respuesta del cliente
                            switch(respuestaPago.getCodError())
                            {
                                case 0:
                                    //if(respuestaPago.getCodRespuesta().equals("00") && respuestaPago.getNoAutorizacion().matches("\\d{1,}")) "[\\d|a-zA-Z]{1,}"
                                    if(respuestaPago.getCodRespuesta().equals("00") && respuestaPago.getNoAutorizacion().matches("[\\d|a-zA-Z]{1,}"))
                                    {//respuestaPago.setNoAutorizacion(respuestaPago.getNoAutorizacion()+"X");
                                        if(peticionPago.isChip() && !peticionPago.isFalloLecturaChip())
                                        {

                                            respuestaPinpadBean = new RespuestaPinpadBean("00", respuestaPago.getNoAutorizacion(), respuestaPago.getCodRespuesta());
                                            pinpad.envioDatosPinpad(respuestaPinpadBean.toString().split("\\n"), process);

                                            int cont = 0;

                                            while((respuestaPinpad = pinpad.lecturaDatosPinpad(process, tEspera)).contains("ERROR") && ++cont < nIntentos);

                                            respuestaFrontPago = utilConv.generaRespuestaFront(respuestaPinpad);
                                        }
                                        else
                                        {
                                            respuestaFrontPago.setCodError(0);
                                            respuestaFrontPago.setDescError("Operación finalizada");
                                        }

                                        respuestaFrontPago.setNumAutorizacion(respuestaPago.getNoAutorizacion());

                                        respuestaFrontPago.setTipoTarjeta(tipoTarjeta);

                                        respuestaFrontPago.setNombreUsuario(nombrePropietario);

                                        respuestaFrontPago.setTipoLectura(utilTarjeta.obtieneTipoLectura(peticionPago.isChip(), peticionPago.isFalloLecturaChip()));

                                        //Enmascara el número de la tarjeta
                                        if(numeroTarjeta.length() >= 6)
                                            respuestaFrontPago.setNumeroTarjeta(
                                                    utilCadenas.rellenaCadenaCaracter(
                                                        numeroTarjeta.substring(numeroTarjeta.length() - 4)
                                                        , 16
                                                        , "*"
                                                        , true)
                                                    );

                                        if(!utilCadenas.isCadenaVacia(peticionPago.getC55()) && peticionPago.getC55().length() > 16)
                                        {
                                            respuestaFrontPago.setARQC(utilTarjeta.getDataInC55(peticionPago.getC55(), "9F26", 16));
                                            respuestaFrontPago.setAID(utilTarjeta.getDataInC55(peticionPago.getC55(), "4F", 14));
                                        }
                                        else
                                        {
                                            respuestaFrontPago.setARQC("");
                                            respuestaFrontPago.setAID("");
                                        }

                                        PeticionReversoTarjetaFrontBean reverso = new PeticionReversoTarjetaFrontBean();

                                        SION.log(Modulo.VENTA, "Generación reverso respuesta front:\n" 
                                                + peticionPago.toString() + "\n"
                                                + respuestaPago.toString() + "\n"
                                                + peticionPagoFront.toString() + "\n"
                                                , Level.INFO);
                                        reverso = utilConv.generarPeticionReverso(peticionPago, respuestaPago, peticionPagoFront);
                                        SION.log(Modulo.VENTA, "Reverso respuesta front generada:\n" + reverso.toString(), Level.INFO);
                                        respuestaFrontPago.setPeticionReverso(reverso);

                                        if(respuestaFrontPago.getCodError() != 0)
                                        {
                                            this.reversoPago(respuestaFrontPago);
                                            respuestaFrontPago.setPeticionReverso(new PeticionReversoTarjetaFrontBean());
                                        }
                                    }
                                    else
                                    {
                                        respuestaFrontPago.setCodError(-1);
                                        respuestaFrontPago.setDescError(respuestaPago.getMensaje());
                                    }
                                    break;
                                case -2:
                                    respuestaFrontPago.setCodError(respuestaPago.getCodError());
                                    if(respuestaPago.getDescError().contains("|"))
                                    {
                                        String temp = respuestaPago.getDescError().replaceFirst("\\|", "");
                                        String[] tmp = temp.split("\\|");
                                        respuestaFrontPago.setDescError(tmp[0]);
                                    }
                                    else
                                        respuestaFrontPago.setDescError("Ocurrió un error al realizar el pago. Favor de intentarlo nuevamente. [CODIGO:" + respuestaPago.getCodError()+"]");
                                    SION.logearExcepcion(Modulo.VENTA, new Exception(respuestaPago.getDescError()), "[CODIGO:" + respuestaPago.getCodError()+ "]");
                                    break;

                                default:
                                    respuestaFrontPago.setCodError(respuestaPago.getCodError());
                                    respuestaFrontPago.setDescError("Ocurrió un error al realizar el pago. Favor de intentarlo nuevamente. [CODIGO:" + respuestaPago.getDescError()+"]");
                                    SION.logearExcepcion(Modulo.VENTA, new Exception(respuestaPago.getDescError()), "[CODIGO:" + respuestaPago.getCodError()+ "]");
                                    break;
                            }
                        
                    }
                
                
            }
            else
            {
                respuestaFrontPago.setCodError(-5);//Ocurrio un error al leer la tarjeta, por favor intente de nuevo
                respuestaFrontPago.setDescError("Ocurrio un error al leer la tarjeta, por favor intente nuevamente. [CODIGO:5]");
                //respuestaFrontPago.setDescError("Parece haber un problema con la configuración de la terminal pinpad, por favor pongase en contacto con soporte técnico. [CODIGO:5]");
                SION.logearExcepcion(Modulo.VENTA, new Exception("[CODIGO:5] " + respuestaPinpad), "");
            }
            //Notificación de error u operación declinada a pinpad
            if(respuestaFrontPago.getCodError() != 0 && (peticionPago.isChip() && !peticionPago.isFalloLecturaChip()))
            {
                respuestaPinpadBean = new RespuestaPinpadBean("", "", "");
                pinpad.envioDatosPinpad(respuestaPinpadBean.toString().split("\\n"), process);
            }
            process.destroy();
        }
        catch(WSException wse)
        {
            respuestaFrontPago.setCodError(-9);
            String descripcionError = manejaCausaExcepcion(wse.getCause());
            respuestaFrontPago.setDescError("[CODIGO:9] "+descripcionError);
            SION.logearExcepcion(Modulo.VENTA, wse, "[CODIGO:9] " + wse.getMessage());
        }
        catch(AxisFault af)
        {
            respuestaFrontPago.setCodError(-6);
            SION.logearExcepcion(Modulo.VENTA, af, "[CODIGO:6] " + af.getMessage());
            String descripcionError = descripcionError = manejaCausaExcepcion(af);
            respuestaFrontPago.setDescError("[CODIGO:6] "+descripcionError);
        }catch(IOException ex){
            respuestaFrontPago.setCodError(-10);
             String descripcionError = manejaCausaExcepcion(ex);
            respuestaFrontPago.setDescError(" [CODIGO:10] "+descripcionError);
            respuestaFrontPago.setDescError(" [CODIGO:10] "+descripcionError);
        }catch(Exception ex)
        {
            respuestaFrontPago.setCodError(-7);
            respuestaFrontPago.setDescError("Ocurrió un error al realizar el pago. Favor de intentarlo nuevamente. [CODIGO:7]");
            SION.logearExcepcion(Modulo.VENTA, ex, "[CODIGO:7] " + ex.getMessage());
            //ex.printStackTrace();
        }
        catch (Error e)
        {
            respuestaFrontPago.setCodError(-8);
            respuestaFrontPago.setDescError("Ocurrió un error inesperado al realizar el pago. Favor de intentarlo nuevamente. [CODIGO:8]");
            SION.logearExcepcion(Modulo.VENTA, new Exception(e.getMessage()), "[CODIGO:8] " + e.getMessage());
            //e.printStackTrace();
        }
        SION.log(Modulo.VENTA, "Respuesta de operación:\n" + respuestaPago.toString(), Level.INFO);
        SION.log(Modulo.VENTA, "Respuesta para front:\n" + respuestaFrontPago.toString(), Level.INFO);
        return respuestaFrontPago;
    }//peticionPago
*/
    
    public RespuestaReversoTarjetaDto reversoPago(RespuestaPagoTarjetaFrontBean respuestaPago) throws Exception
    {
        UtileriasConv utilConv = new UtileriasConv();
        SION.log(Modulo.VENTA, "Petición reverso:\n" + respuestaPago.toString(), Level.INFO);
        RespuestaReversoTarjetaDto respuesta = new RespuestaReversoTarjetaDto();
        try
        {
            if(respuestaPago !=  null)
            {
                if(lecturaBanda 
                   && !respuestaPago.getPeticionReverso().isChip() 
                   && !respuestaPago.getPeticionReverso().isFalloLecturaChip())
                {
                    //
                    ConsumirTarjetaImp clienteBanda = new ConsumirTarjetaImp();
                    respuesta = utilConv.respuestaReversoDto2RespuestaReversoTarjetaDto(clienteBanda.reversoTarjeta(utilConv.generaPeticionReversoDto(respuestaPago), utilConv.generaRespuestaReversoDto(respuestaPago)), utilConv.generaPeticionReversoDto(respuestaPago));
                }
                else
                {
                    //
                    ConsumirTarjetaWSImpl clienteChip = new ConsumirTarjetaWSImpl();
                    respuesta = clienteChip.solicitudReversoTarjeta(utilConv.peticionReversoTarjetaBean2PeticionReversoTarjetaDto(respuestaPago.getPeticionReverso()));
                }
            }
        }
        catch(Exception e)
        {
            respuesta.setCodError(-1);
            respuesta.setDescError("ERROR: Problema al aplicar reverso " + e.getMessage());
            SION.logearExcepcion(Modulo.VENTA, e, e.getMessage());
            e.printStackTrace();
        }
        catch(Error e)
        {
            respuesta.setCodError(-2);
            respuesta.setDescError("ERROR: Problema al aplicar reverso " + e.getMessage());
            SION.logearExcepcion(Modulo.VENTA, new Exception(e.getMessage()), e.getMessage());
            e.printStackTrace();
        }
        SION.log(Modulo.VENTA, "Respuesta a solicitud de reverso: " + respuesta.toString(), Level.INFO);
        return respuesta;
    }//reversoPago
    
    private String manejaCausaExcepcion(Throwable error){
        Throwable causa = error.getCause();
        String descripcionError = "Ocurrió un error al realizar el pago. Favor de intentarlo nuevamente.";
        String tipoErrorComunicacion = " (Problema de comunicación)";
        
        if( error instanceof AxisFault ){
            return "Ocurrió un problema con la comunicación durante el envío de información, intente más tarde.";
        }else if( causa != null ){
            if( causa instanceof javax.xml.stream.XMLStreamException ){ //SocketTimeoutException = No fue posible entregar la información del cobro con tarjeta por un problema de comunicación, favor de intentarlo nuevamente más tarde.
                descripcionError = "No fue posible entregar la información del cobro con tarjeta por un problema de comunicación, favor de intentarlo nuevamente más tarde.";
            }else if( causa instanceof java.net.SocketTimeoutException ){ //SocketTimeoutException = Se agotó el tiempo de espera para recibir la respuesta del cobro con tarjeta, favor de intentarlo nuevamente.
                descripcionError = "Se agotó el tiempo de espera para recibir la respuesta del cobro con tarjeta, favor de intentarlo nuevamente."+tipoErrorComunicacion;
            }else if( causa instanceof java.net.ConnectException ){//ConnectException = Falló el intento de conexión a central para procesar el pago, favor de intentarlo nuevamente.
                descripcionError = "Falló el intento de conexión a central para procesar el pago, favor de intentarlo nuevamente."+tipoErrorComunicacion;
            }else if( causa instanceof java.net.NoRouteToHostException || 
                        causa instanceof java.net.UnknownHostException ||
                        causa instanceof java.net.SocketException ){//NoRouteToHost_UnknownHost_Socket_Exception = No fue posible resolver el servidor para procesar el pago con tarjeta, favor de intentarlo nuevamente más tarde.
                descripcionError = "No fue posible resolver el servidor para procesar el pago con tarjeta, favor de intentarlo nuevamente más tarde."+tipoErrorComunicacion;
            }else if( causa instanceof IOException ){//IOException = Falló el intento de conexión a central para procesar el pago, favor de intentarlo nuevamente.
                descripcionError = "Falló el intento de conexión a central para procesar el pago, favor de intentarlo nuevamente."+tipoErrorComunicacion;
            }else if( causa instanceof ConnectionException ){// ConnectionException = Central rechazó la conexión para realizar el cobro con tarjeta, por favor intente nuevamente.
                descripcionError = "Central rechazó la conexión para realizar el cobro con tarjeta, por favor intente nuevamente."+tipoErrorComunicacion;
            }else if(causa instanceof EOFException){//EOFException = Central terminó la comunicación, por favor intente nuevamente.
                descripcionError = "Central terminó la comunicación, por favor intente nuevamente."+tipoErrorComunicacion;
            }else if(causa.getMessage().contains("connection reset")){//reset = La conexión fue reseteada, por favor intente nuevamente
                descripcionError = "La conexión fue reseteada, por favor intente nuevamente."+tipoErrorComunicacion;
            }
        }
        return descripcionError;
    }
}//class