/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pinpadTest;

/* @author Sammy Guergachi <sguergachi at gmail.com> */
public class PagatodoReqBean {
    private String idAgente;//Central
    private String fechaHoraTransaccion;//Tienda
    private String idGrupo;//Central
    private String idSucursal;//Tienda - Prop
    private String idTerminal;//Tienda - Prop
    private String idOperador;//Tienda - Prop o Central
    private String noTicket;//Tienda
    private String sku;//Central
    private String referencia;//clave de pago, track de tarjeta, num celular
    private String idCliente;//Nulo
    private String monto;//Sin 
    private String formaLectura;//Ver refencia: 000 = Indica que no hay tarjeta presente en la operación, 902 = Tarjeta deslizada
    private String noConfirmacion;
    private String noSecUnicoPT;
    private String categoria;
    //private String[] listaProductos;

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getFechaHoraTransaccion() {
        return fechaHoraTransaccion;
    }

    public void setFechaHoraTransaccion(String fechaHoraTransaccion) {
        this.fechaHoraTransaccion = fechaHoraTransaccion;
    }

    public String getFormaLectura() {
        return formaLectura;
    }

    public void setFormaLectura(String formaLectura) {
        this.formaLectura = formaLectura;
    }

    public String getIdAgente() {
        return idAgente;
    }

    public void setIdAgente(String idAgente) {
        this.idAgente = idAgente;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getIdOperador() {
        return idOperador;
    }

    public void setIdOperador(String idOperador) {
        this.idOperador = idOperador;
    }

    public String getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(String idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getIdTerminal() {
        return idTerminal;
    }

    public void setIdTerminal(String idTerminal) {
        this.idTerminal = idTerminal;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getNoConfirmacion() {
        return noConfirmacion;
    }

    public void setNoConfirmacion(String noConfirmacion) {
        this.noConfirmacion = noConfirmacion;
    }

    public String getNoSecUnicoPT() {
        return noSecUnicoPT;
    }

    public void setNoSecUnicoPT(String noSecUnicoPT) {
        this.noSecUnicoPT = noSecUnicoPT;
    }

    public String getNoTicket() {
        return noTicket;
    }

    public void setNoTicket(String noTicket) {
        this.noTicket = noTicket;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }
    
}
