package pinpadTest;

import java.util.logging.Level;
import java.util.logging.Logger;
import neto.sion.tarjeta.chip.utilerias.UtileriasCadenas;
import neto.sion.tienda.bean.PeticionPagoTarjetaFrontBean;
import neto.sion.tienda.bean.PeticionReversoTarjetaFrontBean;
import neto.sion.tienda.bean.RespuestaPagoTarjetaFrontBean;
import neto.sion.tienda.controlador.ControladorTarjeta;

public class PinpadTest {

    public static void main (String[] args)
    {
        ControladorTarjeta controlador = new ControladorTarjeta();
        PeticionPagoTarjetaFrontBean peticion = new PeticionPagoTarjetaFrontBean();
        double monto = (Math.random() * 101);
        System.out.println("A) Monto: " + monto);
        peticion.setMonto(monto);
        peticion.setPaisId(1);
        peticion.setTiendaId(903);
        peticion.setNumTerminal("01");
        peticion.setSePermitenBancarias(true);
        peticion.setSePermitenVales(true);

        System.out.println("Solicitud Pago:" + peticion.toString());
        //RespuestaPagoTarjetaFrontBean respuesta = controlador.peticionPago(peticion);
        //System.out.println("Respuesta Pago:" + respuesta.toString());
        PeticionReversoTarjetaFrontBean reverso = new PeticionReversoTarjetaFrontBean();
        reverso.setAfiliacion("");
        reverso.setCanalPago("09");
        reverso.setChip(true);
        reverso.setComision(0.5);
        reverso.setFalloLecturaChip(false);
        reverso.setFechaLocal("111111");
        reverso.setHoraLocal("000000");
        reverso.setMonto(77.77);
        reverso.setPaPaisId(1);
        reverso.setPagoTarjetaId(111111);
        reverso.setTerminal("77");
        reverso.setTiendaId(5555);
        reverso.setTrack2("5421564321654324");
        //respuesta.setPeticionReverso(reverso);
        
        try
        {/*
            UtileriasCadenas util = new UtileriasCadenas();
            if(respuesta.getPeticionReverso()!= null)
            {
                System.out.println("Solicitud Reverso:" + respuesta.getPeticionReverso().toString());
                System.out.println("Respuesta Reverso:" + controlador.reversoPago(respuesta).toString());
            }*/
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Logger.getLogger(PinpadTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Finaliza operacion....");
    }
}