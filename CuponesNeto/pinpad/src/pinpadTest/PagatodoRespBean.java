/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pinpadTest;

/* @author Sammy Guergachi <sguergachi at gmail.com> */
public class PagatodoRespBean {
        private String rDescRespuesta;
	private String rDescMsgObligatorio;//Se debe imprimir de forma obligatoria en el ticket
	private String rIdGrupo;//Propiedad
	private String rIdSucursal;//Propiedad
	private String rIdTerminal;//Propiedad
	private String rIdOperador;//Propiedad
	private String rNoTicket;//No de ticket
	private String rReferencia;//
	private String rNoConfirmacion;//
	private String rNoSecUnicoPT;//Reverso
	private String rNoMsgObligatorio;//Se debe imprimir de forma obligatoria en el ticket
	private String rNoMsgPromocion;//
	private String rSaldoDisponibleTarjeta;
	private String rMontoAbonado;
	private String rS110;
}
