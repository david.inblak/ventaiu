/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package promocionescupone.test;

import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesAxis;
import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesError;
import neto.sion.tienda.venta.promociones.cupones.mensajeserror.MensajeXInterpretar;
import neto.sion.tienda.venta.promociones.cupones.mvp.IConfiguracionCupones;
import org.apache.axis2.AxisFault;
import org.apache.commons.httpclient.ConnectTimeoutException;

/**
 *
 * @author dramirezr
 */
public class TestInterpreteMensajes {
    public static void main(String[] args) {
         IConfiguracionCupones configuracionCupones = new ConfiguracionCuponesFake();
         InterpreteMensajesError conv = configuracionCupones.getConvertidorMensajes();
         //Object res = conv.interpretaMensaje(new MensajeXInterpretar("{\"mensaje\":\"Ocurrio un error al crear el mensaje de respues\",\"codigoError\":1}"));
         Object res = conv.interpretaMensaje(
                 new MensajeXInterpretar(
                    new AxisFault("Time out", 
                        new ConnectTimeoutException("Error de pruebas")
                 )));
         System.out.println("R: " + res);
    }
}
