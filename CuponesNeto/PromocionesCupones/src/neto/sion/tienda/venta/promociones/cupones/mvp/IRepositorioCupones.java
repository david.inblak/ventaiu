/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mvp;

import neto.sion.tienda.venta.cupones.facade.TipoCupon;

/**
 *
 * @author dramirezr
 */
public interface IRepositorioCupones {
    Object debeAbrirVentanaCupones() throws Exception;
    Object calculaDescuentosAutomaticos(Object solicitud) throws Exception;
    Object calculaDescuentosCupon(Object solicitud, Object tipoCliente) throws Exception;
    Object cancelaVentaCupones(Object solicitud) throws Exception;
    Object hayComunicacion(Object solicitud ) throws Exception;
    Object finalizaVenta(Object solicitud) throws Exception;
    Object esVentaConProveedorExterno(Object solicitud) throws Exception;
  
}
