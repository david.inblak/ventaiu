/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mvp.concreto;

import neto.sion.tienda.venta.promociones.cupones.mvp.IConfiguracionCupones;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
//import javafx.application.Platform;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.facade.TipoCupon;
import neto.sion.tienda.venta.cupones.util.ConversionBeanes;
import neto.sion.tienda.venta.cupones.util.Excepciones;
import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesAxis;
import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesError;
import neto.sion.tienda.venta.promociones.cupones.mensajeserror.MensajeXInterpretar;
import neto.sion.tienda.venta.promociones.cupones.modelo.ArticuloCupon;
import neto.sion.tienda.venta.promociones.cupones.modelo.CuponBean;
//import neto.sion.tienda.venta.promociones.cupones.modelo.IdentidadCaja;
import neto.sion.tienda.venta.promociones.cupones.modelo.addcupon.*;
import neto.sion.tienda.venta.promociones.cupones.modelo.DatosCuponesVentaDto;
import neto.sion.tienda.venta.promociones.cupones.modelo.cancela.CancelaBeanRequest;
import neto.sion.tienda.venta.promociones.cupones.modelo.preview.*;
import neto.sion.tienda.venta.promociones.cupones.mvp.IComandoCupon;
import neto.sion.tienda.venta.promociones.cupones.mvp.IPresentadorCupones;
import neto.sion.tienda.venta.promociones.cupones.mvp.IRepositorioCupones;
import neto.sion.tienda.venta.promociones.cupones.mvp.IVistaCupones;
import org.apache.axis2.AxisFault;

/**
 *
 * @author dramirezr
 */
public class PresentadorCuponesMarketec implements IPresentadorCupones {

    private static final Double CANTIDAD_ARTICULOS_UNO = 1.0;
    private static final Double UN_CENTAVO = 0.01, CERO_CENTAVOS = 0.0;
    private static final long CUPON_ACTIVO = 0, CUPON_INACTIVO = -1;
    private static final int pedido = 0, UNIDAD_UNO = 1, NORMA_EMPAQUE_UNO = 1, SIN_FAMILIA = 0;
    private static final String token = null, COD_BARRAS_PADRE_CERO = "0";
    private final static long PROCESADO_EXITOSO = 0, CODIGO_ERROR = 1;
    private final static String CUPON_ESTATUS_APLICADO = "Aplicado";
    IVistaCupones vista;
    IRepositorioCupones repo = null;
    IComandoCupon comandoCerrar;
    IComandoCupon comandoAplicar;
    IComandoCupon comandoContinuar;
    IComandoCupon comandoControlError;
    IConfiguracionCupones configuracion;
    InterpreteMensajesError convertidorMsj;
    private DatosCuponesVentaDto ide;
    TipoCupon tipoCuponesAceptados = null;
    ExecutorService threadPool = Executors.newFixedThreadPool(1);
    private static final int CODIGO_ERROR_CENTRAL = -1, CODIGO_ERROR_PROVEEDOR = 100;
    private static final long CERO = 0;

    public PresentadorCuponesMarketec(IVistaCupones vista, IConfiguracionCupones configuracion) {
        this.vista = vista;
        this.repo = null;
        this.configuracion = configuracion;
        convertidorMsj = configuracion.getConvertidorMensajes();
    }

    public void reset() {
    }

    private void setRepositorio(IRepositorioCupones repo) {
        this.repo = repo;
    }

    @Override
    public void init(Object solicitud) throws Exception {
        this.setRepositorio(configuracion.elegirRepositorio());
        tipoCuponesAceptados = null;

        DebeIntentarCupones debe = (DebeIntentarCupones) debeIntentarDescuentoCupones();
        ide = ((DatosCuponesVentaDto) solicitud).clonar();

        if (debe.getDebe()) {
            ide.setEsVentaInicializadaMktc(true);

            this.eliminarDescuentosArticulos();
            vista.mostrarCargando("Cargando artículos de la venta...");
            vista.mostrarArticulosDeVenta(ide.getArticulosCupon());
            calculaDescuentosAutomaticos(solicitud);
            vista.ocultarCargando(null);
        } else {
            ide.setEsVentaSinMarketec(true);
            ide.setMotivoSinCupones(debe.getMensaje());
            terminarCuponesXError(debe.getMensaje(), false);
        }
    }

    @Override
    public Object mostrarArticulosDeVenta(Object solicitud) throws Exception {
        return this.vista.mostrarArticulosDeVenta(solicitud);
    }

    @Override
    public DebeIntentarCupones debeIntentarDescuentoCupones() {
        DebeIntentarCupones intentaCupones = new DebeIntentarCupones(Boolean.TRUE, "OK");
        try {

            if (!(Boolean) repo.debeAbrirVentanaCupones()) {
                intentaCupones = new DebeIntentarCupones(Boolean.FALSE, "Parametro de cupones desactivado.");
            } else if (!(Boolean) repo.hayComunicacion(null)) {
                intentaCupones = new DebeIntentarCupones(Boolean.FALSE, "No hay comunicación a central por lo que se desactivan los cupones.");
            }
        } catch (Exception e) {
            intentaCupones = new DebeIntentarCupones(true, "Se presentó un problema durante la validación.");
        } finally {
            return intentaCupones;
        }
    }

    public class DebeIntentarCupones {

        Boolean debe;
        String mensaje;

        public DebeIntentarCupones(Boolean debe, String mensaje) {
            this.debe = debe;
            this.mensaje = mensaje;
        }

        public Boolean getDebe() {
            return debe;
        }

        public void setDebe(Boolean debe) {
            this.debe = debe;
        }

        public String getMensaje() {
            return mensaje;
        }

        public void setMensaje(String mensaje) {
            this.mensaje = mensaje;
        }
    }

    @Override
    public Object calculaDescuentosAutomaticos(final Object solicitud) throws Exception {
        threadPool.execute(new Runnable() {

            @Override
            public void run() {
                try {
                    vista.mostrarCargando("Calculando artículos...");
                    Object respDescuentosAutomaticos = repo.calculaDescuentosAutomaticos(generaSolicitudPreview());
                    vista.actualizarDescuentosArticulosDeVenta(generaRespuestaPreview(respDescuentosAutomaticos));
                    vista.calculaPrecioTotalConDescuentos();
                } catch (Exception ex) {
                    SION.logearExcepcion(Modulo.VENTA, ex, "calculaDescuentosAutomaticos :: threadPool >> " + Excepciones.getStackTrace(ex));
                    Object msj = convertidorMsj.interpretaMensaje(new MensajeXInterpretar(ex));
                    if (msj != null) {
                        terminarCuponesXError(msj.toString(), false);//Cualquier error lanza el comando controlar cupones
                    } else {
                        terminarCuponesXError("Se produjo un error durante la llamada a Begin cupones.", false);//Cualquier error lanza el comando controlar cupones
                    }
                } finally {
                    vista.ocultarCargando(null);
                }
            }
        });

        return null;
    }

    @Override
    public void eliminarDescuentosArticulos() throws Exception {
        for (CuponBeanResp cupon : ide.getCuponesValidados()) {
            cupon.setEstado(CUPON_INACTIVO);
        }
        vista.eliminarDescuentosArticulos(null);
    }

    @Override
    public Object calculaDescuentosXCupon(final Object solicitud) throws Exception {

        vista.mostrarCargando("");

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!validaExistenciaCupon(solicitud.toString())) {
                        String codigoCupon = solicitud.toString();
                        if (tipoCuponesAceptados == TipoCupon.Marketec) {
                            codigoCupon = coincideFormatoMARKET(solicitud.toString());
                        } else if (tipoCuponesAceptados == TipoCupon.Neto) {
                            codigoCupon = coincideFroamtoNETO(solicitud.toString());
                        }

                        AddCuponBeanReq sol = new AddCuponBeanReq(
                                pedido,
                                (int) ide.getFinTicketMktc().getPos(),
                                (int) ide.getFinTicketMktc().getSucursal(),
                                ide.getFinTicketMktc().getTicket(),
                                ide.getFinTicketMktc().getTicketCompleto(),
                                token,
                                codigoCupon,
                                String.valueOf(ide.getRegistroMktcId()),
                                ide.getRegistroPeticionNeto(),
                                obtenerArticulosNeto(ide.getArticulosCupon()));

                        Object respCuponDescuento = repo.calculaDescuentosCupon(sol, tipoCuponesAceptados);
                        Object respAdd = generaRespuestaAddCupon(respCuponDescuento);

                        vista.actualizarDescuentosArticulosDeVenta(respAdd);
                        vista.calculaPrecioTotalConDescuentos();
                    }

                } catch (Exception ex) {
                    SION.logearExcepcion(Modulo.VENTA, ex, "calculaDescuentosXCupon :: " + Excepciones.getStackTrace(ex));
                    Object msg = convertidorMsj.interpretaMensaje(new MensajeXInterpretar(ex));
                    terminarCuponesXError(msg.toString(), false);

                } finally {
                    vista.ocultarCargando(null);
                }
            }
        });

        return null;
    }

    private List<ArticuloAddCuponBean> obtenerArticulosNeto(List<ArticuloCupon> articulos) {
        List<ArticuloCupon> artsVista = (List<ArticuloCupon>) vista.obtenerArticulosVista();

        List<ArticuloAddCuponBean> artsAdd = new ArrayList<ArticuloAddCuponBean>();
        for (ArticuloCupon articuloCupon : artsVista) {
            artsAdd.add(new ArticuloAddCuponBean(articuloCupon.getId(), articuloCupon.getCantidad(), articuloCupon.getPrecio(), articuloCupon.getDescuento()));
        }
        return artsAdd;
    }
    private static Pattern ean128Patt = Pattern.compile("[?]*([0-9]{14}+[a-zA-Z]{3})$", Pattern.CASE_INSENSITIVE);
    private static Pattern ean13Patt = Pattern.compile("[?]*([0-9]{13})$", Pattern.CASE_INSENSITIVE);

    private static String coincideFormatoMARKET(String toString) {
        Matcher matcherMkt = ean13Patt.matcher(toString);
        if (matcherMkt.find()) {
            return matcherMkt.group();
        }
        return null;
    }

    private static String coincideFroamtoNETO(String toString) {
        Matcher matcherNto = ean128Patt.matcher(toString);
        if (matcherNto.find()) {
            return matcherNto.group();
        }
        return null;
    }

    public static void main(String[] args) {

        String x = "sfsF1234567890123", x1 = "+C112345678901234NTO";

        //Pattern pat = Pattern.compile("[?]*([0-9]{13})$", Pattern.CASE_INSENSITIVE);
        Matcher m = ean13Patt.matcher(x);

        System.out.println("matches: " + m.matches());
        if (m.find()) {
            System.out.println("Grupo : " + m.group());
            System.out.println("Grupo 0: " + m.group(0));
            System.out.println("Grupo 1: " + m.group(1));
            System.out.println("Grupo count: " + m.groupCount());
        }

        //Pattern patt = Pattern.compile("[?]*([0-9]{14}+[a-zA-Z]{3})$", Pattern.CASE_INSENSITIVE);
        Matcher mnto = ean128Patt.matcher(x1);

        System.out.println("matches: " + mnto.matches());
        if (mnto.find()) {
            System.out.println("Grupo : " + mnto.group());
            System.out.println("Grupo 0: " + mnto.group(0));
            System.out.println("Grupo 1: " + mnto.group(1));

            System.out.println("Grupo count: " + mnto.groupCount());
        }
    }

    @Override
    public void eliminaDescuentoXCupon(Object solicitud) throws Exception {
        vista.eliminarCuponDescuento(solicitud);
        for (Iterator it = ide.getCuponesValidados().iterator(); it.hasNext();) {
            CuponBeanResp cu = (CuponBeanResp) it.next();
            if (cu.getCodigo().equals(solicitud)) {
                cu.setEstado(CUPON_INACTIVO);
                vista.eliminarCuponDescuento(solicitud);
            }
        }
    }

    @Override
    public Object cancelaVenta(Object solicitud) {
        DatosCuponesVentaDto req = (DatosCuponesVentaDto) solicitud;

        if (req.getRegistroMktcId() > 0 || (req.getRegistroPeticionNeto() != null && !"".equals(req.getRegistroPeticionNeto()))) {
            try {
                CancelaBeanRequest cancelaReq = new CancelaBeanRequest();
                cancelaReq.setRegistroMktcId(req.getRegistroMktcId());
                cancelaReq.setRegistroPeticionNeto(req.getRegistroPeticionNeto());
                cancelaReq.setPos(req.getFinTicketMktc().getPos());
                cancelaReq.setSucursal(req.getFinTicketMktc().getSucursal());
                cancelaReq.setTicket(Long.parseLong(req.getFinTicketMktc().getTicket()));
                cancelaReq.setTicketCompleto(req.getFinTicketMktc().getTicketCompleto());
                SION.log(Modulo.VENTA, "Se envía cancenlación cupones de venta en progreso: " + req.getRegistroMktcId(), Level.INFO);
                repo.cancelaVentaCupones(cancelaReq);
            } catch (Exception e) {
                SION.logearExcepcion(Modulo.VENTA, e, "Error al cancelar la venta Marketec.");
            }
        }

        return null;
    }

    @Override
    public void continuar(Object solicitud) {
        ide.setArticulosCupon((List<ArticuloCupon>) solicitud);

        comandoAplicar.ejecutar(ide);
        if (comandoContinuar != null) {
            comandoContinuar.ejecutar();
        }
    }

    @Override
    public void calculaPrecioTotalConDescuentos() {
        vista.calculaPrecioTotalConDescuentos();
    }

    public void terminarCuponesXError(String solicitud, boolean xCodigoError) {
        //incrementaIntentoFallidoVentaCupones(xCodigoError);

        ide.getFinTicketMktc().setTicketAnterior(ide.getFinTicketMktc().getTicket());
        ide.setMotivoSinCupones(solicitud);
        ide.setEsVentaSinMarketec(true);

        vista.eliminarDescuentosArticulos(null);
        cerrarVentana();
        if (comandoControlError != null) {
            comandoControlError.ejecutar(ide, solicitud);
        }
    }

    public DatosCuponesVentaDto getIdentidad() {
        return ide;
    }
    static final SimpleDateFormat sdfNumFecha = new SimpleDateFormat("yyyyMMdd");
    static final SimpleDateFormat sdfNumHora = new SimpleDateFormat("HHmmss");
    static final SimpleDateFormat sdfNum = new SimpleDateFormat("yyyyMMddHHmmss");
    static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    private Object generaSolicitudPreview() {
        Date ahora = new Date();

        ValidaVentaDescReq req = new ValidaVentaDescReq();

        req.setCajero(ide.getFinTicketMktc().getCajeroId());
        req.setCantidadArticulos(ide.getArticulosCupon().size());
        req.setFecha(sdfNumFecha.format(ahora));
        req.setHora(sdfNumHora.format(ahora));

        req.setTicketAnterior(ide.getFinTicketMktc().getTicket());
        Double sumaCostoTotal = 0.0;
        for (ArticuloCupon art : ide.getArticulosCupon()) {
            sumaCostoTotal += art.getImporte();
        }

        req.setMontototal(sumaCostoTotal);
        long idTicketConsecutivo = generaConsecutivoMasIntento(ide);
        long idConsecutivoVenta = generaConsecutivo(ide);

        ide.getFinTicketMktc().setTicket(idTicketConsecutivo + "");
        ide.getFinTicketMktc().setTicketOriginal(idConsecutivoVenta + "");

        req.setTicket(ide.getFinTicketMktc().getTicket());
        req.setTicketOriginal(ide.getFinTicketMktc().getTicketOriginal());
        req.setTicketCompleto(ide.getFinTicketMktc().getTicketCompleto());

        List<PluBeginVentaBean> plus = new ArrayList<PluBeginVentaBean>();
        for (ArticuloCupon articuloCupon : ide.getArticulosCupon()) {
            PluBeginVentaBean pluBeginVenta = new PluBeginVentaBean();

            pluBeginVenta.setCantidad(articuloCupon.getCantidad());
            pluBeginVenta.setCodigo(String.valueOf(articuloCupon.getId()));
            pluBeginVenta.setEstructura(String.valueOf(articuloCupon.getIdFamilia()));
            pluBeginVenta.setMonto(articuloCupon.getPrecio());
            pluBeginVenta.setMontoTotal(articuloCupon.getImporte());

            plus.add(pluBeginVenta);
        }

        req.setPlu(plus);
        req.setPos(ide.getFinTicketMktc().getPos());
        req.setSucursal(ide.getFinTicketMktc().getSucursal());

        return req;
    }

    private Object generaRespuestaPreview(Object respDescuentosAutomaticos) throws Exception {
        List<ArticuloCupon> desctos = new ArrayList<ArticuloCupon>();

        try {
            ValidaVentaDescResp resp = (ValidaVentaDescResp) ConversionBeanes.convertir(respDescuentosAutomaticos, ValidaVentaDescResp.class);
            SION.log(Modulo.VENTA, " >> " + resp.toString(), Level.INFO);

            if (PROCESADO_EXITOSO == resp.getCodigoError()) {
                ide.setRegistroMktcId(resp.getRegistroMktcId());
                ide.setRegistroPeticionNeto(resp.getRegistroPeticionNeto());

                List<CuponCuponeraBeanResp> cupones = resp.getCuponesCuponera();
                if (cupones != null) {
                    for (CuponCuponeraBeanResp item : resp.getCuponesCuponera()) {
                        String cuponId = item.getCodigoCupon();
                        List<ArticuloCuponeraBeanResp> articulos = item.getArticulosCuponera();
                        if (articulos != null) {
                            for (ArticuloCuponeraBeanResp articuloCuponeraBeanResp : articulos) {
                                CuponBean cupon = new CuponBean(cuponId, true, articuloCuponeraBeanResp.getDescripcionOferta(), true, null);

                                desctos.add(new ArticuloCupon(0,
                                        articuloCuponeraBeanResp.getCantidad(),
                                        articuloCuponeraBeanResp.getMonto(),
                                        cupon,
                                        articuloCuponeraBeanResp.getCodigoBarras(),
                                        IdentificadorDescuentos.getTipoCuponAgregado(tipoCuponesAceptados),
                                        articuloCuponeraBeanResp.getDescripcionOferta()));
                            }
                        }
                    }
                }
            } else {
                if (esCodigoErrorCentral(resp.getCodigoError())) {
                    terminarCuponesXError("(Central) " + resp.getMensaje(), true);
                } else if (esCodigoErrorProveedor(resp.getCodigoError())) {
                    terminarCuponesXError("(Proveedor) " + resp.getMensaje(), true);
                }
            }
        } catch (Exception e) {
            Object msg = convertidorMsj.interpretaMensaje(new MensajeXInterpretar(e));
            terminarCuponesXError(msg.toString(), true);
        }

        return desctos;
    }

    private boolean esCodigoErrorCentral(long codigoError) {
        return CODIGO_ERROR_CENTRAL == codigoError || codigoError < CERO;
    }

    private boolean esCodigoErrorProveedor(long codigoError) {
        return CODIGO_ERROR_PROVEEDOR == codigoError || codigoError > CERO;
    }

    public long generaConsecutivoMasIntento(DatosCuponesVentaDto identidad) {
        long dato = Long.parseLong(
                identidad.getFinTicketMktc().getTicketCompleto().replace(sdfNumFecha.format(new Date()), "")
                + identidad.getIntentos());
        return dato;
    }

    public long generaConsecutivo(DatosCuponesVentaDto identidad) {
        long dato = Long.parseLong(
                identidad.getFinTicketMktc().getTicketCompleto().replace(sdfNumFecha.format(new Date()), ""));
        return dato;
    }

    public String generaConsecutivoIntentoAnterior(DatosCuponesVentaDto identidad) {
        if (identidad.getIntentos() == 1) {
            return identidad.getFinTicketMktc().getTicketCompleto().replace(sdfNumFecha.format(new Date()), "")
                    + (identidad.getIntentos() - 1) + "";
        }

        return null;
    }

    private boolean validaExistenciaCupon(String solicitud) {

        boolean yaExiste = false;

        for (Iterator it = ide.getCuponesValidados().iterator(); it.hasNext();) {
            final CuponBeanResp cup = (CuponBeanResp) it.next();
            yaExiste = cup.getCodigo().equals(solicitud);
            if (yaExiste) {
                if (cup.getEstado() == CUPON_ACTIVO) {
                    vista.mostrarMensaje("El cupón ya fue agregado previamente.");
                    return yaExiste;
                } else {//Existe inactivo se remueve anterior por si el imorte descuento es diferente
                    cup.setCodigo(cup.getCodigo());
                    it.remove();
                    //ide.getCuponesValidados().remove(cup);
                    yaExiste = false;
                }
            }
        }
        return yaExiste;
    }

    private Object generaRespuestaAddCupon(Object respCuponDescuento) throws Exception {
        List<ArticuloCupon> desctos = new ArrayList<ArticuloCupon>();

        AddCuponBeanResp resp = new AddCuponBeanResp();
        try {
            resp = (AddCuponBeanResp) ConversionBeanes.convertir(respCuponDescuento, AddCuponBeanResp.class);
        } catch (AxisFault e) {
            throw e;
        } catch (Exception e) {
            resp.setCodigoError(CODIGO_ERROR);
            Object msg = convertidorMsj.interpretaMensaje(new MensajeXInterpretar(respCuponDescuento));
            resp.setMensaje(msg.toString());
        }


        if (PROCESADO_EXITOSO == resp.getCodigoError()) {//Control de respuesta central

            String cuponId = resp.getCupon().getCodigo() + "";

            ide.setRegistroPeticionNeto(resp.getRegistroPeticionNeto());

            //Controlar el estado del cupon
            if (resp.getCupon().getEstado() == PROCESADO_EXITOSO && CUPON_ESTATUS_APLICADO.equals(resp.getCupon().getEstadoDesc())) {
                ide.getCuponesValidados().add(resp.getCupon());
                if (resp.getCupon().getDescuentoTotal() == null) {
                    List<ArticuloConDescuentoBean> articulos = resp.getCupon().getArticulos();
                    if (articulos != null) {
                        for (ArticuloConDescuentoBean articuloCuponeraBeanResp : articulos) {
                            ArticuloCupon arti = crearArticuloDescuentoAplicado(cuponId, articuloCuponeraBeanResp, resp.getCupon());
                            desctos.add(arti);
                        }
                    }

                } else {
                    DescuentoTotalBean descTotalVenta = resp.getCupon().getDescuentoTotal();
                    Double importeDescuentoTotal = Double.parseDouble("0" + descTotalVenta.getMonto());

                    CuponBean cupon = new CuponBean(descTotalVenta.getCodigo(), true, descTotalVenta.getDescripcionOferta(), false, resp.getCupon());
                    if (importeDescuentoTotal > CERO_CENTAVOS) {
                        ArticuloCupon art = crearArticuloDescuentoTotalPositivo(importeDescuentoTotal, descTotalVenta.getDescripcionOferta(), cupon);
                        desctos.add(art);
                    } else {
                        ArticuloCupon art = crearArticuloDescuentoTotalNegativo(importeDescuentoTotal, descTotalVenta.getDescripcionOferta(), cupon);
                        desctos.add(art);
                    }

                }

            } else {
                if (esStringVacio(resp.getCupon().getEstadoDesc())
                        && esStringVacio(resp.getCupon().getLinea1())
                        && esStringVacio(resp.getCupon().getLinea2())) {
                    vista.mostrarMensaje(//"El cupón no tiene un estado válido.\n"+
                            "(Proveedor)\n"
                            + "El mensaje recibo \n"
                            + "se encuentra vacío");
                } else {
                    vista.mostrarMensaje(//"El cupón no tiene un estado válido.\n"+
                            "" + resp.getCupon().getEstadoDesc() + "\n"
                            + "" + resp.getCupon().getLinea1() + "\n"
                            + "" + resp.getCupon().getLinea2());
                }

            }
        } else {
            if (esCodigoErrorCentral(resp.getCodigoError())) {
                terminarCuponesXError("(Central) " + resp.getMensaje(), true);
            } else if (esCodigoErrorProveedor(resp.getCodigoError())) {
                terminarCuponesXError("(Proveedor) " + resp.getMensaje(), true);
            }
        }
        return desctos;
    }

    private boolean esStringVacio(String str) {
        return str == null || str.isEmpty();
    }

    private ArticuloCupon crearArticuloDescuentoAplicado(String idCupon, ArticuloConDescuentoBean artDesc, CuponBeanResp cupon) {

        CuponBean _cupon = new CuponBean(idCupon, true, artDesc.getDescripcionOferta(), false, cupon);

        ArticuloCupon art = new ArticuloCupon(
                Long.parseLong(artDesc.getArticuloId() + ""),
                CANTIDAD_ARTICULOS_UNO,
                artDesc.getMontoDesc(),
                _cupon,
                artDesc.getCodigoBarras(),
                IdentificadorDescuentos.getTipoCuponAgregado(tipoCuponesAceptados),
                artDesc.getDescripcionOferta());

        return art;
    }

    private ArticuloCupon crearArticuloDescuentoTotalPositivo(double importeDescuentoTotal, String descripcionOferta, CuponBean cupon) {
        ArticuloCupon art = crearArticuloDescuentoTotal();
        art.setTipoDescuento(IdentificadorDescuentos.getTipoCuponAgregado(tipoCuponesAceptados));
        art.setCupon(cupon);
        art.setNombre(descripcionOferta);
        art.setDescuento(importeDescuentoTotal + UN_CENTAVO);

        return art;
    }

    private ArticuloCupon crearArticuloDescuentoTotalNegativo(double importeDescuentoTotal, String descripcionOferta, CuponBean cupon) {
        ArticuloCupon art = crearArticuloDescuentoTotal();
        art.setTipoDescuento(IdentificadorDescuentos.getTipoCuponAgregado(tipoCuponesAceptados));
        art.setCupon(cupon);
        art.setNombre(descripcionOferta);
        art.setDescuento(importeDescuentoTotal);
        art.setCosto(UN_CENTAVO);

        return art;
    }

    private ArticuloCupon crearArticuloDescuentoTotal() {
        return new ArticuloCupon(
                ArticuloCupon.ID_ARTICULO_DESCTO_TOTAL,
                CANTIDAD_ARTICULOS_UNO,
                "",
                UN_CENTAVO,
                UN_CENTAVO,
                0.00,
                CERO_CENTAVOS,
                CERO_CENTAVOS,
                SIN_FAMILIA,
                ArticuloCupon.ID_SIN_IEPS,
                UNIDAD_UNO,
                NORMA_EMPAQUE_UNO,
                new CuponBean(),
                String.valueOf(ArticuloCupon.ID_ARTICULO_DESCTO_TOTAL),
                COD_BARRAS_PADRE_CERO,
                0,
                CERO_CENTAVOS);
    }

    @Override
    public void setComandoAplicarDescuentos(IComandoCupon iComando) {
        comandoAplicar = iComando;
    }

    @Override
    public void setComandoCerrarCupones(IComandoCupon iComando) {
        comandoCerrar = iComando;
    }

    @Override
    public void setComandoContinuar(IComandoCupon iComando) {
        comandoContinuar = iComando;
    }

    @Override
    public void cerrarVentana() {
        cancelaVenta(ide);
        if (comandoCerrar != null) {
            comandoCerrar.ejecutar();
        }
    }

    @Override
    public void setComandoOnError(IComandoCupon iComando) {
        comandoControlError = iComando;
    }

    @Override
    public Object esCuponValido(Object solicitud) {
        if (!hayTipoCuponAceptado()) {
            calculaTipoCuponAceptado(solicitud);
        }

        return esTipoCuponAceptado(solicitud);
    }

    private boolean esTipoCuponAceptado(Object solicitud) {
        String codigoNto = coincideFroamtoNETO(solicitud.toString());
        String codigoMkt = coincideFormatoMARKET(solicitud.toString());

        if (codigoMkt == null && codigoNto == null || tipoCuponesAceptados == null) {
            vista.mostrarMensaje("Cupón inválido.");
            return false;
        }

        switch (tipoCuponesAceptados) {
            case Marketec:
                SION.log(Modulo.VENTA, "Formato coincide con Marketec > " + codigoMkt, Level.INFO);
                if (codigoMkt != null) {
                    vista.actualizaIdCupon(codigoMkt);
                    return true;
                }
                break;

            case Neto:
                SION.log(Modulo.VENTA, "Formato coincide con Neto > " + codigoNto, Level.INFO);
                if (codigoNto != null) {
                    vista.actualizaIdCupon(codigoNto);
                    return true;
                }
                break;
            default:
                vista.mostrarMensaje("Cupón inválido.");
                SION.log(Modulo.VENTA, "Id cupón intentado: " + solicitud.toString(), Level.INFO);
        }
        vista.mostrarMensaje("Cupón no acumulabre con otros tipos de cupón.");
        return false;
    }

    private void calculaTipoCuponAceptado(Object solicitud) {
        String codigoNto = coincideFroamtoNETO(solicitud.toString());
        String codigoMkt = coincideFormatoMARKET(solicitud.toString());

        if (codigoNto != null) {
            setTipoCuponAceptado(TipoCupon.Neto);
        } else if (codigoMkt != null) {
            setTipoCuponAceptado(TipoCupon.Marketec);
        } else {
            setTipoCuponAceptado(null);
            vista.mostrarMensaje("Cupón inválido.");
        }
    }

    private void setTipoCuponAceptado(TipoCupon tipo) {
        SION.log(Modulo.VENTA, "Los tipos de cupón aceptado son: " + tipo, Level.INFO);
        tipoCuponesAceptados = tipo;
    }

    private boolean hayTipoCuponAceptado() {
        return tipoCuponesAceptados != null;
    }
}
