/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mvp;

/**
 *
 * @author dramirezr
 */
public interface IVistaCupones {
    Object mostrarArticulosDeVenta(Object solcitud);
    Object actualizarDescuentosArticulosDeVenta(Object solicitud);
    void mostrarCargando(Object mensaje);
    void ocultarCargando(Object msg);
    void mostrarMensaje(Object msg);

    void eliminarDescuentosArticulos(Object object);
    
    void agregarCuponDescuento(Object solicitud);
    void eliminarCuponDescuento(Object solicitud);
    
    void calculaPrecioTotalConDescuentos();
    
    Object obtenerArticulosVista();

    public void actualizaIdCupon(String codigoNto);
}
