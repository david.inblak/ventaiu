/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mvp.concreto;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.genericos.utilidades.Utilerias;
import neto.sion.tienda.venta.cupones.facade.ClienteCupones;
import neto.sion.tienda.venta.cupones.facade.ClienteCuponesFactory;
import neto.sion.tienda.venta.cupones.facade.TipoCupon;
import neto.sion.tienda.venta.promociones.cupones.mvp.IRepositorioCupones;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author dramirezr
 */
public class RepositorioCuponesNeto implements IRepositorioCupones{
    private static final int NUMERO_INTENTOS = 1;
    private static int EXITO = 0;
    private static int ACTIVADO = 1;
    private ClienteCupones cliente = null;
        
    public RepositorioCuponesNeto() {
        cliente = ClienteCuponesFactory.getClienteCupones(TipoCupon.Neto);
    }
    
    @Override
    public Object calculaDescuentosAutomaticos(Object solicitud) throws Exception {        
        return cliente.validaDesceuntosAutomaticos(solicitud);
    }

    @Override
    public Object calculaDescuentosCupon(Object solicitud, Object tipoCliente) throws Exception {
         return cliente.calculaDescuentoCupones(solicitud);
    }
    
    @Override
    public Object cancelaVentaCupones(Object solicitud) throws Exception {
        return cliente.cancelarVenta(solicitud);
    }
    
    
    @Override
    public Object hayComunicacion(Object solicitud) throws Exception {
        return Utilerias.evaluarConexionCentral(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONESNETO.SERVICE"), NUMERO_INTENTOS);
    }

    @Override
    public Object debeAbrirVentanaCupones() throws Exception {
        Boolean fueraLineaComunicacion = false;
        Boolean fueraLineaForzado = false;
        //Fuera de línea por comunicación.
        CallableStatement cs=null;
        Connection conn = null;       
        String sql = null;
        try{
            conn = Conexion.obtenerConexion();
            
            sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS");
        
            cs = conn.prepareCall(sql);
            cs.setInt(1, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONETO.CUPONES.ACTIVOS.PAIS")));
            cs.setInt(2, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.sistema")));
            cs.setInt(3, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.modulo")));
            cs.setInt(4, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.submodulo")));
            cs.setInt(5, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.configuracion")));
            cs.registerOutParameter(6, OracleTypes.NUMBER);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.NUMBER);
            cs.execute();
            
            double resOperacionFlComunicacion = cs.getDouble(6);
            double activadoFlComunicacion = cs.getDouble(8);
            if( resOperacionFlComunicacion  == EXITO ){
                fueraLineaComunicacion = (activadoFlComunicacion == ACTIVADO);
                System.out.println( " fueraLineaComunicacion: " + fueraLineaComunicacion );
            }
            
            
            cs.setInt(1, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.PAIS")));
            cs.setInt(2, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "forzadoVentaFueraLinea.sistema")));
            cs.setInt(3, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "forzadoVentaFueraLinea.modulo")));
            cs.setInt(4, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "forzadoVentaFueraLinea.submodulo")));
            cs.setInt(5, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "forzadoVentaFueraLinea.configuracion")));
            cs.registerOutParameter(6, OracleTypes.NUMBER);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.NUMBER);
            cs.execute();
            
            double resOperacionFlForzado = cs.getDouble(6);
            double activadoFlForzado = cs.getDouble(8);
            if( resOperacionFlForzado  == EXITO ){
                fueraLineaForzado = (activadoFlForzado == ACTIVADO);
                 System.out.println( " fueraLineaForzado: " + fueraLineaForzado );
            }
            
            if( fueraLineaForzado == true || fueraLineaComunicacion == true ){
                return false;
            }
            return true;
            
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, sql);
            throw new Exception("RepositorioCuponesNeto :: Error al obtener configuración de cupones.\n" + e.getMessage());//throw e;
        } finally {
            Conexion.cerrarRecursos(conn,cs, null);
        }
    }

    @Override
    public Object finalizaVenta(Object solicitud) throws Exception {
        return cliente.finalizaVenta(solicitud);
    }

    @Override
    public Object esVentaConProveedorExterno(Object solicitud) throws Exception {
        System.out.println( "Es venta con proveedor externo: false");
        return false;
    }

    
}
