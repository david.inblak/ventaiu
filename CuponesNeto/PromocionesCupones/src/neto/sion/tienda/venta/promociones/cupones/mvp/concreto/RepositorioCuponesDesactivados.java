/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mvp.concreto;

import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.facade.ClienteCupones;
import neto.sion.tienda.venta.cupones.facade.ClienteCuponesFactory;
import neto.sion.tienda.venta.cupones.facade.TipoCupon;
import neto.sion.tienda.venta.promociones.cupones.mvp.IRepositorioCupones;

/**
 *
 * @author dramirezr
 */
public class RepositorioCuponesDesactivados implements IRepositorioCupones {

    public RepositorioCuponesDesactivados() {
    }

    @Override
    public Object calculaDescuentosAutomaticos(Object solicitud) throws Exception {
         SION.log(Modulo.VENTA, "CuponesDesactivados :: calculaDescuentosAutomaticos : No se debería llegar aqui.", Level.INFO);
        return null;
    }

    @Override
    public Object calculaDescuentosCupon(Object solicitud, Object tipoCliente) throws Exception {
        SION.log(Modulo.VENTA, "CuponesDesactivados :: calculaDescuentosCupon : No es necesario cancelar venta en central.", Level.INFO);
        return null;
    }

    @Override
    public Object cancelaVentaCupones(Object solicitud) throws Exception {
        SION.log(Modulo.VENTA, "CuponesDesactivados :: No es necesario cancelar venta en central.", Level.INFO);
        return null;
    }

    @Override
    public Object hayComunicacion(Object solicitud) throws Exception {
         SION.log(Modulo.VENTA, "CuponesDesactivados :: hayComunicacion : No debe intentar cupones.", Level.INFO);
        return false;
    }

    @Override
    public Object debeAbrirVentanaCupones() throws Exception {
        SION.log(Modulo.VENTA, "CuponesDesactivados :: debeIntentarDescuentoCupones : No debe intentar cupones.", Level.INFO);
        return false;
    }

     @Override
    public Object finalizaVenta(Object solicitud) throws Exception {
        ClienteCupones tmpCliente = ClienteCuponesFactory.getClienteCupones(TipoCupon.Neto);
        return tmpCliente.calculaDescuentoCupones(solicitud);
    }

    @Override
    public Object esVentaConProveedorExterno(Object solicitud) throws Exception {
        System.out.println( "Es venta con proveedor externo: false");
        return false;
    }
    
}
