/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mensajeserror;

/**
 *
 * @author dramirezr
 */
public class InterpreteMensajesException extends InterpreteMensajesError {
    
    public InterpreteMensajesException(InterpreteMensajesError interprete ) {
        super(interprete);
    }
    
    @Override
    public Object interpretaMensaje(MensajeXInterpretar mensaje) {
       if( mensaje == null || mensaje.data == null )
            return "Exception > Se recibió un mensaje vacío";
        
         StringBuilder strBuilder = new StringBuilder();
        
        if( mensaje.data instanceof Exception ){
            strBuilder.append("Ocurrió un error inesperado: \n")
                      .append( ((Exception)mensaje.data).getMessage() );
        }
        return strBuilder.toString();
    }
    
}
