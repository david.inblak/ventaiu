/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mvp.concreto;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesAxis;
import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesError;
import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesException;
import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesJSON;
import neto.sion.tienda.venta.promociones.cupones.mvp.IConfiguracionCupones;
import neto.sion.tienda.venta.promociones.cupones.mvp.IRepositorioCupones;
import oracle.jdbc.internal.OracleTypes;

/**
 *
 * @author dramirezr
 */
public class ConfiguracionCuponesSION implements IConfiguracionCupones{
    public static final int ACTIVADO = 1;
    
    private IRepositorioCupones repo = null;
    private InterpreteMensajesError conv = null;
    
    @Override
    public IRepositorioCupones elegirRepositorio() throws Exception {
        if( repo == null){
            CallableStatement cs=null;
            Connection conn = null;        
            String sql = null;        

            try{
                boolean cuponesMarketecActivo;
                boolean cuponesNetoActivo;

                conn = Conexion.obtenerConexion();

                sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS");

                cs = conn.prepareCall(sql);
                
                cs.setInt(1, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.PAIS")));
                cs.setInt(2, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.SISTEMA")));
                cs.setInt(3, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.MODULO")));
                cs.setInt(4, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.SUBMODULO")));
                cs.setInt(5, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.CONFIGURACIONID")));
                cs.registerOutParameter(6, OracleTypes.NUMBER);
                cs.registerOutParameter(7, OracleTypes.VARCHAR);
                cs.registerOutParameter(8, OracleTypes.NUMBER);
                cs.execute();

                cuponesMarketecActivo = cs.getInt(8) == ACTIVADO;

                
                cs.setInt(1, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONETO.CUPONES.ACTIVOS.PAIS")));
                cs.setInt(2, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONETO.CUPONES.ACTIVOS.SISTEMA")));
                cs.setInt(3, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONETO.CUPONES.ACTIVOS.MODULO")));
                cs.setInt(4, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONETO.CUPONES.ACTIVOS.SUBMODULO")));
                cs.setInt(5, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.CUPONETO.CUPONES.ACTIVOS.CONFIGURACIONID")));
                cs.registerOutParameter(6, OracleTypes.NUMBER);
                cs.registerOutParameter(7, OracleTypes.VARCHAR);
                cs.registerOutParameter(8, OracleTypes.NUMBER);
                cs.execute();

                cuponesNetoActivo = cs.getInt(8) == ACTIVADO;

                if( cuponesMarketecActivo && cuponesNetoActivo ){
                    SION.log(Modulo.VENTA, "Se usará el repositorio de cupones COMBINADO.", Level.INFO ); 
                    repo = new RepositorioCuponesNetoMarketec();
                }else if ( cuponesMarketecActivo ){
                    SION.log(Modulo.VENTA, "Se usará el repositorio de cupones MARKETEC.", Level.INFO ); 
                    repo = new RepositorioCuponesMarketec();
                }else if ( cuponesNetoActivo ){
                    SION.log(Modulo.VENTA, "Se usará el repositorio de cupones NETO.", Level.INFO ); 
                    repo = new RepositorioCuponesNeto();
                }else {
                    SION.log(Modulo.VENTA, "Se usará el repositorio de cupones DESACTIVADO.", Level.INFO ); 
                    repo = new RepositorioCuponesDesactivados();
                }
            } catch (Exception e) {
                SION.logearExcepcion(Modulo.VENTA, e, sql);
            } finally {
                Conexion.cerrarRecursos(conn,cs, null);
            }
        }
        return repo;
    }
    
    @Override
    public void resetRepositorio(){
        repo = null;
    }

    @Override
    public Object debeIntentarDescuentoCupones() throws Exception {
        elegirRepositorio();
        return repo.debeAbrirVentanaCupones();
    }

    @Override
    public Object cuponesExternosActivos() throws Exception {
        elegirRepositorio();
        return repo.esVentaConProveedorExterno(null);
    }

    @Override
    public InterpreteMensajesError getConvertidorMensajes() {
        if( conv == null)
            conv = new InterpreteMensajesAxis(new InterpreteMensajesException(new InterpreteMensajesJSON()));
        return conv;
    }
}
