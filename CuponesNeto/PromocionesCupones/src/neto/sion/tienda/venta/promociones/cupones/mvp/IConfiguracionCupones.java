/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mvp;

import neto.sion.tienda.venta.promociones.cupones.mensajeserror.InterpreteMensajesError;

/**
 *
 * @author dramirezr
 */
public interface IConfiguracionCupones {
    public InterpreteMensajesError getConvertidorMensajes();
    public IRepositorioCupones elegirRepositorio() throws Exception ;
    public Object debeIntentarDescuentoCupones() throws Exception;
    public Object cuponesExternosActivos() throws Exception ;
    public void resetRepositorio();
    
}
