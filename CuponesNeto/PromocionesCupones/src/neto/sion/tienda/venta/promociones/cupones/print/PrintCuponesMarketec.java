package neto.sion.tienda.venta.promociones.cupones.print;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.*;
import javax.print.attribute.standard.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.facade.TipoCupon;
import neto.sion.tienda.venta.promociones.cupones.modelo.finventa.LineasBean;

/**
 *
 * @author dramirezr
 */
public class PrintCuponesMarketec {
    private static JRExporter exporter = null;
    private static InputStream inJasper = null;
    private static PrintService service = null;
    
    private static String getAlineacion(String alineacion){
        if("C".toLowerCase().equals(alineacion.toLowerCase()))
            return "center";
        if("I".toLowerCase().equals(alineacion.toLowerCase()))
            return "left";
        if("D".toLowerCase().equals(alineacion.toLowerCase()))
            return "rigth";
        
        return null;
    }
    
    private static String getTamanoTexto(int tamId){
        if(tamId == 1)
            return "18px";
        if(tamId == 2)
            return "30px";
        if(tamId == 3)
            return "40px";
        if(tamId == 4)
            return "60px";
        if(tamId == 5)
            return "90px";
        return null;
    }
            

    
     private static String getInversionBack(String invertido){
        if("-1".toLowerCase().equals(invertido))
            return "#FFFFFF";
        else
            return "#000000";
    }
     
     private static String getInversion(String invertido){
        if("-1".toLowerCase().equals(invertido))
            return "#000000";
        else
            return "#FFFFFF";
    }
     
     private static String getEstilo(String invertido){
        if("B".toLowerCase().equals(invertido))
            return "bold";
        else
            return "normal";
    }
        
    public static String generaDatosCupon(List<LineasBean> lineasCupon){
        StringBuilder sbCuponImprimir = new StringBuilder();
        int maxLineas = 21, lineasActuales=0;

        sbCuponImprimir.append("<div style=\"font-family: Helvetica, Arial, sans-serif;\">");
        for (LineasBean lineaBean : lineasCupon) {
            sbCuponImprimir.append("<div style=\"");
            sbCuponImprimir.append("text-align: "       ).append( getAlineacion(lineaBean.getAlineacion()) ).append(";" );            
            sbCuponImprimir.append( "background-color: ").append( getInversion(lineaBean.getInverso())).append(";" );
            sbCuponImprimir.append("\">");
            
            sbCuponImprimir.append("<span style=\"");
            sbCuponImprimir.append( "font-size: "       ).append(getTamanoTexto(lineaBean.getSize())).append(";" );
            sbCuponImprimir.append( "color: "           ).append(getInversionBack(lineaBean.getInverso())).append(";" );
            sbCuponImprimir.append( "font-weight: "     ).append(getEstilo(lineaBean.getEstilo())).append(";" );
            sbCuponImprimir.append( "font-style: normal;" );
            sbCuponImprimir.append("\">");
            
            if( "".equals(lineaBean.getTexto().trim()) )
                sbCuponImprimir.append("&nbsp");
            else
                sbCuponImprimir.append(lineaBean.getTexto());
            
            sbCuponImprimir.append("</span>");
            sbCuponImprimir.append("</div>");
            lineasActuales += lineaBean.getSize();
            
            if( lineasActuales >= maxLineas ) break;
        }
        sbCuponImprimir.append("</div>");
        
        //System.out.println(sbCuponImprimir.toString());
                
        return sbCuponImprimir.toString();
    }
    
    private static void loadFileCupon(TipoCupon tipoCupon) throws Exception{
        //if( inJasper == null)
        {
            try
            {
                String nombreJasper = SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.TEMPLATECUPONES."+tipoCupon.getNombre());

                String so = System.getProperty("os.name");
                FileInputStream fis;
                if (so.equals("Linux")) {
                    fis = new FileInputStream(new File(File.separator + "SION" + File.separator + SION.obtenerParametro(Modulo.SION, "rutaCompiladosJasper") + File.separator + nombreJasper));
                } else {
                    SION.log(Modulo.VENTA, "D:" + File.separator + SION.obtenerParametro(Modulo.SION, "rutaCompiladosJasper") + File.separator + nombreJasper, Level.INFO);
                    fis = new FileInputStream(new File("D:" + File.separator + SION.obtenerParametro(Modulo.SION, "rutaCompiladosJasper") + File.separator + nombreJasper));
                }
                inJasper = fis;
            }
            catch (FileNotFoundException ex)
            {
                SION.log(Modulo.VENTA, getStackTrace(ex), Level.SEVERE);
                throw new Exception("Archivo plantilla de impresión no encontrado.");
            }
            catch (Exception exg)
            {
                SION.log(Modulo.VENTA, getStackTrace(exg), Level.SEVERE);
                throw new Exception("Error desconocido con archivo plantilla de impresión.");
            }
        }
        
    }
    
    
    public static void imprimirCupones(String cuponId, List<LineasBean> lineasCupon, int noCopias, 
            String nombreImagenComercio, String nombreImagenArticulo, int tipoCuponImprimir) throws Exception{
 
      
        loadFileCupon( TipoCupon.convierteIntTipoCupon(tipoCuponImprimir) );
        
        if( service == null ){
            service = obtenerServicioImpresion(SION.obtenerParametro(Modulo.SIONBASE, "nombreImpresora") );
        }
       
        if( exporter == null ){
            exporter = new JRPrintServiceExporter();
            exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
            exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
            exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE, service);
            exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET, service.getAttributes());
        }
        
        HashMap<String, Object> parametros = new HashMap();
        parametros.put("rutaImagenes", SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPON.IMAGENES.UBICACION"));
        parametros.put("cuponId", cuponId);
        parametros.put("htmlImpresion", PrintCuponesMarketec.generaDatosCupon(lineasCupon));
                
        if( nombreImagenComercio == null )
            parametros.put("nombreImagenComercio", SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPON.IMAGENCOMERCIO"));
        else
            parametros.put("nombreImagenComercio", nombreImagenComercio);
        
        if( nombreImagenArticulo == null )
            parametros.put("nombreImagenArticulo", SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPON.IMAGENARTICULO"));
        else
            parametros.put("nombreImagenArticulo", nombreImagenArticulo);
        
                
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            JasperPrint jasperPrint = JasperFillManager.fillReport(inJasper, parametros, new JREmptyDataSource(1));
            
            PrintRequestAttributeSet aset = null;
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            
            if (noCopias > 0){
                aset = new HashPrintRequestAttributeSet();
                aset.add(new Copies(noCopias));
                exporter.setParameter(JRPrintServiceExporterParameter.PRINT_REQUEST_ATTRIBUTE_SET, aset);
            }
                  
            exporter.setParameter(JRPrintServiceExporterParameter.OUTPUT_STREAM, baos); 
            exporter.exportReport();
                    
        }
        catch (JRException ex)
        {
            SION.logearExcepcion(Modulo.VENTA, ex, "finaliza método de generár documento a imprimir cupones: " + getStackTrace(ex));
            throw new Exception("JRException: Error en impresión y procesamiento de plantilla." + getStackTrace(ex));
        }
        catch (Exception exc)
        {
            SION.logearExcepcion(Modulo.VENTA, exc, "finaliza método de generár documento a imprimir cupones: "+ getStackTrace(exc));
            throw new Exception("Exception: Error desconocido en impresión y procesamiento de plantilla." );
        }catch (Error exc)
        {
            SION.logearExcepcion(Modulo.VENTA, new Exception( exc.getCause() ), "finaliza método de generár documento a imprimir cupones: "+ getStackTrace( new Exception( exc.getCause() ) ));
            throw new Exception("Error: Error desconocido en impresión y procesamiento de plantilla." );
        }
        finally
        {
            SION.log(Modulo.VENTA, "finaliza método de generár documento a imprimir cupones", Level.INFO);
            inJasper.close();
        }
    }
    
      public static String getStackTrace(Throwable aThrowable)
  {
    Writer result = new StringWriter();
    PrintWriter printWriter = new PrintWriter(result);
    aThrowable.printStackTrace(printWriter);
    return result.toString();
  }
      
    public static void main(String[] args) throws Exception {
        List<LineasBean> lcup = new ArrayList<LineasBean>();
        /*for (int i = 0; i < 5; i++) {
            //lcup.add(new LineasBean( i, "Test los 36 caracteres 20 lineas x1", 1, "C", "0", "N"));//OK 20lineas
            //lcup.add(new LineasBean( i, "18 car 10 line x2", 2, "C", "0", "N"));//ok 10lineas
            lcup.add(new LineasBean( i, "9c 5l x4", 4, "C", "0", "N")); //ok 5lineas
        }*/
        /*
        lcup.add(new LineasBean( 0, "Valido 22/01/2020 X 21/02/2020", 1, "C", "0", "N"));
        lcup.add(new LineasBean( 1, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 1, "C", "0", "N"));
        lcup.add(new LineasBean( 2, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 1, "C", "0", "N"));
        lcup.add(new LineasBean( 3, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 1, "C", "0", "N"));
        lcup.add(new LineasBean( 4, "XXXXXXXXXXXXXXXXXX", 2, "C", "0", "N"));//10
        lcup.add(new LineasBean( 5, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 1, "C", "0", "N"));
        lcup.add(new LineasBean( 6, "XXXXXXXXX", 4, "C", "0", "N"));
        lcup.add(new LineasBean( 7, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 1, "C", "0", "N"));
        lcup.add(new LineasBean( 8, "XXXXXXXXXXXXXXXXXX", 2, "C", "0", "N"));
        lcup.add(new LineasBean( 9, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 1, "C", "0", "N"));
        lcup.add(new LineasBean(10, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 1, "C", "0", "N"));//17
        lcup.add(new LineasBean(11, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 1, "C", "-1", "N"));
        lcup.add(new LineasBean(12, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 1, "C", "0", "N"));
        lcup.add(new LineasBean(13, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 1, "C", "0", "N"));
        lcup.add(new LineasBean(14, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 1, "C", "0", "N"));//21
        lcup.add(new LineasBean(15, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 1, "C", "0", "N"));
        lcup.add(new LineasBean(16, " ", 1, "C", "0", "N"));
        lcup.add(new LineasBean(17, " ", 1, "C", "0", "N"));
        lcup.add(new LineasBean(18, " ", 1, "C", "0", "N"));
        lcup.add(new LineasBean(19, "21", 1, "C", "0", "N"));
        
        
        imprimirCupones("4600060004015", lcup, 1, "LogMkt.bmp", "7629.BMP" );
        
        lcup.clear();        lcup.add(new LineasBean( 0, "Valido 22/01/2020 X 21/02/2020", 1, "C", "0", "N"));
        lcup.add(new LineasBean( 1, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 1, "C", "0", "N"));
        lcup.add(new LineasBean( 2, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 1, "C", "0", "N"));
        lcup.add(new LineasBean( 3, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 1, "C", "0", "N"));
        lcup.add(new LineasBean( 4, "AAAAAAAAAAAAAAAAAA", 2, "C", "0", "N"));//10
        lcup.add(new LineasBean( 5, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 1, "C", "0", "N"));
        lcup.add(new LineasBean( 6, "AAAAAAAAA", 4, "C", "0", "N"));
        lcup.add(new LineasBean( 7, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 1, "C", "0", "N"));
        lcup.add(new LineasBean( 8, "AAAAAAAAAAAAAAAAAA", 2, "C", "0", "N"));
        lcup.add(new LineasBean( 9, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 1, "C", "0", "N"));
        lcup.add(new LineasBean(10, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 1, "C", "0", "N"));//17
        lcup.add(new LineasBean(11, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 1, "C", "-1", "N"));
        lcup.add(new LineasBean(12, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 1, "C", "0", "N"));
        lcup.add(new LineasBean(13, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 1, "C", "0", "N"));
        lcup.add(new LineasBean(14, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 1, "C", "0", "N"));//21
        lcup.add(new LineasBean(15, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 1, "C", "0", "N"));
        lcup.add(new LineasBean(16, " ", 1, "C", "0", "N"));
        lcup.add(new LineasBean(17, " ", 1, "C", "0", "N"));
        lcup.add(new LineasBean(18, " ", 1, "C", "0", "N"));
        lcup.add(new LineasBean(19, "21", 1, "C", "0", "N"));
        
        imprimirCupones("4600060004015", lcup, 1, "LogMkt.bmp", "7629.BMP" );*/
        
        lcup.clear();
                              //orden   //Texto                                 //tamaño    //Alineacion    //Inverso   //estilo
        lcup.add(new LineasBean( 0,     "Valido 08/05/2020 a 08/07/2020",       1,          "C",            "0",        "N"));
        lcup.add(new LineasBean( 1,     "APOYO A FAMILIAS",                     3,          "C",            "0",        "B"));//3 Maximo 18 caracteres por línea
        lcup.add(new LineasBean( 2,     "MEXICANAS",                            3,          "C",            "0",        "B"));
        lcup.add(new LineasBean( 3,     "DESPENSA CON VALOR DE",                2,          "C",            "0",        "B"));
        lcup.add(new LineasBean( 4,     "$ 200.00",                             4,          "C",            "0",        "N"));
        lcup.add(new LineasBean( 5,     "EN LA COMPRA DE CUALQUIER",            1,          "C",            "-1",       "B"));
        lcup.add(new LineasBean( 6,     "PRODUCTO EN TIENDAS NETO",             1,          "C",            "-1",       "B"));//10       
        lcup.add(new LineasBean( 7,     "NO INTERCAMBIABLE POR EFECTIVO",       1,          "C",            "0",        "N"));
        lcup.add(new LineasBean( 8,     "NO ACUMULABLE CON OTRAS PROMOCIONES",  1,          "C",            "0",        "N"));
        lcup.add(new LineasBean( 9,     "NO APLICA EN TIEMPO AIRE, SERVICIOS,", 1,          "C",            "0",        "N"));
        lcup.add(new LineasBean(10,     " REFRESCOS, BEBIDAS ALCOHOLICAS",      1,          "C",            "0",        "N"));//17
        lcup.add(new LineasBean(11,     " NI CIGARROS",                         1,          "C",            "0",        "N"));
        lcup.add(new LineasBean(12,     " ",                                    1,          "C",            "0",        "N"));
        lcup.add(new LineasBean(13,     " ",                                    1,          "C",            "0",        "N"));
        lcup.add(new LineasBean(14,     " ",                                    1,          "C",            "0",        "N"));//21 Aquí se corta
        lcup.add(new LineasBean(15,     " ",                                    1,          "C",            "0",        "N"));
        lcup.add(new LineasBean(16,     " ",                                    1,          "C",            "0",        "N"));
        lcup.add(new LineasBean(17,     " ",                                    1,          "C",            "0",        "N"));
        lcup.add(new LineasBean(18,     " ",                                    1,          "C",            "0",        "N"));
        lcup.add(new LineasBean(19,     " ",                                    1,          "C",            "0",        "N"));
        lcup.add(new LineasBean(20,     "  ",                                   1,          "C",            "0",        "N"));
        
        Set<String> cupones = new HashSet<String>();
        cupones.add("20050857106203PEN");
cupones.add("20050800821812PEN");
cupones.add("20050859674184PEN");
cupones.add("20050832121900PEN");
cupones.add("20050818113804PEN");
cupones.add("20050861382125PEN");
cupones.add("20050841014361PEN");
cupones.add("20050859045433PEN");
cupones.add("20050842261287PEN");
cupones.add("20050882227407PEN");
cupones.add("20050875936644PEN");
cupones.add("20050842523874PEN");
cupones.add("20050859628622PEN");
cupones.add("20050826029530PEN");
cupones.add("20050875290488PEN");
cupones.add("20050851051767PEN");
cupones.add("20050825515213PEN");
cupones.add("20050892934674PEN");
cupones.add("20050860218414PEN");
cupones.add("20050820438594PEN");
cupones.add("20050857524760PEN");
cupones.add("20050862952210PEN");
cupones.add("20050808940824PEN");
cupones.add("20050806666423PEN");
cupones.add("20050833066191PEN");
cupones.add("20050859605064PEN");
cupones.add("20050896289323PEN");
cupones.add("20050826345531PEN");
cupones.add("20050866344016PEN");
cupones.add("20050819235913PEN");
cupones.add("20050895629590PEN");
cupones.add("20050865177875PEN");
cupones.add("20050875788032PEN");
cupones.add("20050817913363PEN");
cupones.add("20050854144808PEN");
cupones.add("20050816652957PEN");
cupones.add("20050892413787PEN");
cupones.add("20050874844461PEN");
cupones.add("20050838729452PEN");
cupones.add("20050814541974PEN");
cupones.add("20050842991402PEN");
cupones.add("20050845606251PEN");
cupones.add("20050878868214PEN");
cupones.add("20050872308856PEN");
cupones.add("20050833805697PEN");
cupones.add("20050882128238PEN");
cupones.add("20050843582364PEN");
cupones.add("20050891948544PEN");
cupones.add("20050820369485PEN");
cupones.add("20050838750354PEN");
cupones.add("20050807717138PEN");
cupones.add("20050890955631PEN");
cupones.add("20050817655557PEN");
cupones.add("20050805009657PEN");
cupones.add("20050852497101PEN");
cupones.add("20050873112411PEN");
cupones.add("20050899795565PEN");
cupones.add("20050883226453PEN");
cupones.add("20050812610923PEN");
cupones.add("20050845281665PEN");
cupones.add("20050815881062PEN");
cupones.add("20050816299033PEN");
cupones.add("20050802798886PEN");
cupones.add("20050808468458PEN");
cupones.add("20050808327946PEN");
cupones.add("20050810492896PEN");
cupones.add("20050830500144PEN");
cupones.add("20050824810557PEN");
cupones.add("20050836425632PEN");
cupones.add("20050849593947PEN");
cupones.add("20050826435823PEN");
cupones.add("20050834275240PEN");
cupones.add("20050834526081PEN");
cupones.add("20050888114116PEN");
cupones.add("20050838561332PEN");
cupones.add("20050823353233PEN");
cupones.add("20050820688658PEN");
cupones.add("20050830758051PEN");
cupones.add("20050844060815PEN");
cupones.add("20050849989137PEN");
cupones.add("20050880194948PEN");
cupones.add("20050885775567PEN");
cupones.add("20050887089365PEN");
cupones.add("20050819435026PEN");
cupones.add("20050864692281PEN");
cupones.add("20050877964456PEN");
cupones.add("20050891410254PEN");
cupones.add("20050855941985PEN");
cupones.add("20050872408036PEN");
cupones.add("20050890196920PEN");
cupones.add("20050892144294PEN");
cupones.add("20050876638262PEN");
cupones.add("20050881606205PEN");
cupones.add("20050890923850PEN");
cupones.add("20050831313373PEN");
cupones.add("20050889608601PEN");
cupones.add("20050805457227PEN");
cupones.add("20050845438030PEN");
cupones.add("20050819187188PEN");
cupones.add("20050858893794PEN");
cupones.add("20050811610088PEN");
cupones.add("20050844117785PEN");
cupones.add("20050872724037PEN");
cupones.add("20050838393335PEN");
cupones.add("20050825346430PEN");
cupones.add("20050854099382PEN");
cupones.add("20050832645632PEN");
cupones.add("20050839146027PEN");
cupones.add("20050888885552PEN");
cupones.add("20050835047067PEN");
cupones.add("20050844002642PEN");
cupones.add("20050814257618PEN");
cupones.add("20050854613372PEN");
cupones.add("20050899507227PEN");
cupones.add("20050825701142PEN");
cupones.add("20050861499362PEN");
cupones.add("20050844000605PEN");
cupones.add("20050873915233PEN");
cupones.add("20050889431751PEN");
cupones.add("20050814385993PEN");
cupones.add("20050809003982PEN");
cupones.add("20050850361660PEN");
cupones.add("20050855867792PEN");
cupones.add("20050818966565PEN");
cupones.add("20050851145672PEN");
cupones.add("20050806626856PEN");
cupones.add("20050889071481PEN");
cupones.add("20050874671053PEN");
cupones.add("20050833042497PEN");
cupones.add("20050898906242PEN");
cupones.add("20050868354043PEN");
cupones.add("20050851991672PEN");
cupones.add("20050820857206PEN");
cupones.add("20050839069246PEN");
cupones.add("20050859220448PEN");
cupones.add("20050840475597PEN");
cupones.add("20050890243718PEN");
cupones.add("20050802346491PEN");
cupones.add("20050844326416PEN");
cupones.add("20050843976283PEN");

//imprimirCupones("20050819435026PEN", lcup, 1, "neto.png", "creando.jpg", TipoCupon.Neto.ordinal() );

        for (String idCupon : cupones) {
            //System.out.println(idCupon);
            imprimirCupones(idCupon, lcup, 1, "neto.png", "creando.jpg", TipoCupon.Neto.ordinal() );
        }/**/
        
        //imprimirCupones("467974000067", lcup, 1, "LogMkt.bmp", "7629.BMP", 0);        
        /* Códigos con error
         EKT62952684562
         EKT6295268456
         EK62952684562
         E62952684562
         E629526845
         
         Códigos exitosos
         229526845
         12345678901234ekt
         65664161561145EKT
         * 
         System.out.println(PrintCuponesMarketec.generaDatosCupon(lcup));*/
        
        
        
    }
    
      private static PrintService obtenerServicioImpresion(String nombreImpresora)
    throws PrintException
  {
      
      SION.obtenerParametro(Modulo.SIONBASE, "nombreImpresora");
    try
    {
      if ((nombreImpresora == null) || (nombreImpresora.equals(""))) {
        throw new Exception("No existe el parámetro: nombreImpresoraCarta");
      }
      return buscaImpresoraPorNombre(nombreImpresora, "");
    }
    catch (Exception ex)
    {
      System.out.println(ex);
      throw new PrintException(ex);
    }
  }
      
    public static PrintService buscaImpresoraPorNombre(String nombreImpresora, String prefijoAlternativo)
    throws Exception
  {
     
      nombreImpresora = SION.obtenerParametro(Modulo.SIONBASE, "nombreImpresora");
      prefijoAlternativo = SION.obtenerParametro(Modulo.SIONBASE, "prefijoAlternativoImpresoras");
      
    AttributeSet aset = new HashAttributeSet();
    aset.add(new PrinterName(nombreImpresora, null));
    
    PrintService[] services = PrintServiceLookup.lookupPrintServices(null, aset);
    //PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
    
   SION.log(Modulo.VENTA, "Cantidad de impresoras encontradas por nombre: " + services.length, Level.INFO);
    if (services.length == 0)
    {
      if ((prefijoAlternativo == null) || (prefijoAlternativo.equals(""))) {
          SION.log(Modulo.VENTA, "No se encontró impresora con nombre " + nombreImpresora, Level.INFO);
        throw new Exception("No se encontró impresora con nombre " + nombreImpresora);
      }
      services = PrintServiceLookup.lookupPrintServices(null, null);
      for (PrintService printService : services)
      {
          SION.log(Modulo.VENTA, " +++++ IMPRESORA: " + printService.getName(), Level.INFO);
        //System.out.println(" +++++ IMPRESORA: " + printService.getName());
        if (printService.getName().startsWith(prefijoAlternativo)) {
          return printService;
        }
      }
      SION.log(Modulo.VENTA, "No se encontró impresora con nombre: " + nombreImpresora + "\n o que contenga el prefijo: " + prefijoAlternativo, Level.INFO);
      throw new Exception("No se encontró impresora con nombre: " + nombreImpresora + "\n o que contenga el prefijo: " + prefijoAlternativo);
    }
    /*PrintService[] arr$ = services;int len$ = arr$.length;int i$ = 0;
    if (i$ < len$)
    {
      PrintService printService = arr$[i$];
      */
      informacionImresora(services[0]);
      
      return services[0];
    /*}
    return null;*/
  }
    
    private static void informacionImresora(PrintService printService)
  {
    System.out.println(" ---- IMPRESORA: " + printService.getName());
        
    PrintServiceAttributeSet printServiceAttributeSet = printService.getAttributes();
    System.out.println(" ---- atributos: " + printService.getName());
       
    Attribute[] a = printServiceAttributeSet.toArray();
    for (Attribute unAtribute : a) {
      System.out.println("atributo: " + unAtribute.getName());
    }
    
    System.out.println("--- viendo valores especificos de los atributos ");
    
    System.out.println("PrinterLocation: " + printServiceAttributeSet.get(PrinterLocation.class));
    System.out.println("PrinterInfo: " + printServiceAttributeSet.get(PrinterInfo.class));
    System.out.println("PrinterState: " + printServiceAttributeSet.get(PrinterState.class));
    System.out.println("PrinterMoreInfoManufacturer: " + printServiceAttributeSet.get(PrinterMoreInfoManufacturer.class));
    System.out.println("PrinterResolution: " + printServiceAttributeSet.get(PrinterResolution.class));
    System.out.println("PrinterURI: " + printServiceAttributeSet.get(PrinterURI.class));
    System.out.println("Destination: " + printServiceAttributeSet.get(Destination.class));
    System.out.println("PrinterMakeAndModel: " + printServiceAttributeSet.get(PrinterMakeAndModel.class));
    System.out.println("PrinterIsAcceptingJobs: " + printServiceAttributeSet.get(PrinterIsAcceptingJobs.class));
  }
}
