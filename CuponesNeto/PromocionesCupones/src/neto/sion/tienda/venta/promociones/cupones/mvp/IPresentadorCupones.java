/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mvp;

/**
 *
 * @author dramirezr
 */
public interface IPresentadorCupones {
    void init(Object solicitud)  throws Exception;
    
    Object debeIntentarDescuentoCupones () ;
    Object mostrarArticulosDeVenta      (Object solicitud)  throws Exception;
    Object calculaDescuentosAutomaticos (Object solicitud) throws Exception;
    Object calculaDescuentosXCupon      (Object solicitud) throws Exception;
    Object esCuponValido              (Object solicitud);
    
    Object cancelaVenta (Object solicitud) ;
    
    void   eliminarDescuentosArticulos() throws Exception;
    void   eliminaDescuentoXCupon(Object solicitud) throws Exception;

    void continuar(Object solicitud);

    void calculaPrecioTotalConDescuentos();
    void cerrarVentana();

    void setComandoAplicarDescuentos(IComandoCupon iComando);
    void setComandoCerrarCupones(IComandoCupon iComando);
    void setComandoContinuar(IComandoCupon iComando);
    void setComandoOnError(IComandoCupon iComando);
}
