package neto.sion.tienda.venta.promociones.cupones.vista;


import java.awt.Transparency;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import neto.sion.tienda.venta.promociones.cupones.modelo.ArticuloCupon;
import neto.sion.tienda.venta.promociones.cupones.modelo.CuponBean;



/**
 *
 * @author dramirezr
 */
public class CuponUI {
    
    public static int altoCupon = 35, anchoCUpon = 200;
    
    interface ICupon {
        Object getVista();
        Object eliminaCupon();
    }
    
    public static class Cupon implements ICupon{
        CuponBean modelo;
        ArticuloCupon articulo;
        Label idCupon;

        public Cupon(CuponBean _cupon, ArticuloCupon _articulo) {
            this.modelo = _cupon;
            this.articulo = _articulo;
        }
 
        @Override
        public Node getVista() {
            int espLateral = 5;
            HBox hbPrincipal = new HBox();
            hbPrincipal.visibleProperty().bind(modelo.activo);

            hbPrincipal.setAlignment(Pos.CENTER);
            HBox.setHgrow(hbPrincipal, Priority.ALWAYS);
            hbPrincipal.setPadding(new Insets(1,espLateral,1,espLateral));
            
                        
                Pane panePrincipal = new Pane();
                
                panePrincipal.setStyle("-fx-background-color: #CCCCCC;");
                hbPrincipal.getChildren().add(panePrincipal);
                
                    Rectangle r = new Rectangle(anchoCUpon, altoCupon);
                    r.setArcHeight(5);
                    r.setArcWidth(5);
                    r.setFill(Color.web("#bcb8f5"));
                    r.setStyle("-fx-border-radius: 50px;");
                    r.strokeProperty().setValue(Color.TRANSPARENT);
                    panePrincipal.getChildren().add(r);
                            
                    HBox hb = new HBox();
                    HBox.setHgrow(hb, Priority.ALWAYS);
                    
                    hb.setAlignment(Pos.CENTER);
                    panePrincipal.getChildren().add(hb);
                        idCupon = new Label();
                        idCupon.textProperty().bind( Bindings.when(modelo.automatico).then( "AUTOMATICO" ).otherwise( modelo.idCupon ) );
                        idCupon.setFont(new Font(null, 20));
                        idCupon.setTextAlignment(TextAlignment.CENTER);
                        
                        hb.getChildren().addAll(idCupon);
                        
            return hbPrincipal;
        }
        
        @Override
        public Object eliminaCupon(){
            return true;
        }

    }
    
    public static Cupon crearCupon(CuponBean cupon, ArticuloCupon articulo){
        Cupon c = new Cupon(cupon, articulo);
        return c;
    }
}
