/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.vistamodelo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import neto.sion.impresion.ventanormal.dto.ArticuloTicketDto;
import neto.sion.impresion.ventanormal.dto.DevolucionTicketDto;
import neto.sion.impresion.ventanormal.dto.TipoPagoTicketDto;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.controles.vista.Utilerias;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.exception.SionException;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.seguridad.modelo.DatosTiendaBean;
import neto.sion.tienda.seguridad.modelo.UsuarioSesionBean;
import neto.sion.tienda.venta.DAO.DetalleArticuloDao;
import neto.sion.tienda.venta.DAO.ParametrosDao;
import neto.sion.tienda.venta.bloqueo.fachada.BloqueoVenta;
import neto.sion.tienda.venta.conciliacion.bean.PeticionActTransaccionLocalBean;
import neto.sion.tienda.venta.conciliacion.bean.PeticionConciliacionLocalBean;
import neto.sion.tienda.venta.conciliacion.bean.RespuestaActualizacionLocalBean;
import neto.sion.tienda.venta.conciliacion.bean.RespuestaConciliacionLocalBean;
import neto.sion.tienda.venta.conciliacion.controlador.ConciliacionLocalControlador;
import neto.sion.tienda.venta.fueralinea.bean.ArticuloFLBean;
import neto.sion.tienda.venta.fueralinea.bean.PeticionVentaLocalBean;
import neto.sion.tienda.venta.fueralinea.bean.RespuestaVentaLocalBean;
import neto.sion.tienda.venta.fueralinea.bean.TipoPagoBean;
import neto.sion.tienda.venta.fueralinea.controlador.VentaFueraLineaControlador;
import neto.sion.tienda.venta.impresiones.ImpresionVenta;
import neto.sion.tienda.venta.modelo.*;
//import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;
import neto.sion.venta.bloqueo.bean.BloqueoVentaBean;
import neto.sion.venta.servicios.cliente.dto.*;
import neto.sion.venta.servicios.cliente.serviceFacade.ConsumirVenta;
import neto.sion.venta.servicios.cliente.serviceFacade.ConsumirVentaImp;

/**
 *
 * @author fvega
 */
public class DevolucionesVistaModelo {

    private Utilerias utilerias;
    private ParametrosDao parametrosDevolucionesDAO;
    private PeticionVentaDto peticionVentaDto;
    private PeticionDevolucionDto peticionDevolucionDto;
    private RespuestaVentaDto respuestaVentaDto;
    private ConsumirVenta consumirVentaImp;
    private PeticionDetalleVentaDto peticionDetalleVentaDto;
    private RespuestaDetalleVentaDto respuestaDetalleVentaDto;
    private DevolucionTicketDto devolucionTicketDto;//ticket=new DevolucionTicketDto();
    private ArticuloTicketDto[] articuloTicketDtos;//articulos=new ArticuloTicketDto[4];
    private TipoPagoTicketDto[] tipoPagoTicketDtos;//tipos=new TipoPagoTicketDto[5];
    private ImpresionVenta impresionVenta;
    private BloqueoVenta bloqueoVenta;// = new BloqueoVenta();
    private RespuestaConciliacionLocalBean respuestaConciliacionLocalBean;
    private ConciliacionLocalControlador conciliacionLocalControlador;
    private PeticionConciliacionLocalBean conciliacionLocalBean;
    private PeticionActTransaccionLocalBean peticionActTransaccionLocalBean;
    private RespuestaActualizacionLocalBean respuestaActualizacionLocalBean;
    ////objetos de respuesta y peticion para store de consulta de articulo
    private PeticionDetalleArticuloBean peticionArticuloLocalBean;
    private RespuestaDetalleArticuloBean respuestaArticuloLocalBean;
    ///daos
    private DetalleArticuloDao articuloDao;
    ///
    private ObservableList<ParametrosBean> listaParametros =
            FXCollections.observableArrayList();//parametros de la operacion
    private ObservableList<ArticuloBean> listaArticulos =
            FXCollections.observableArrayList();//articulos que se encuentran en la tabla 
    private ObservableList<ArticuloBean> listaArticulosTicket =
            FXCollections.observableArrayList();//articulos que se encuentran en la tabla 
    private ObservableList<ArticuloBean> listaArticulosSeleccionados =
            FXCollections.observableArrayList();
    private ObservableList<DatosFormasPagoBean> listaDatosFormasPago =
            FXCollections.observableArrayList();
    private ObservableList<ArticuloBean> listaArticulosTemp =
            FXCollections.observableArrayList();//articulos que se encuentran en la tabla 
    private ArrayList<ArticuloDto> listaOrdenada;
    private ArrayList<TipoPagoDto> listaTiposPago = new ArrayList<TipoPagoDto>();
    //private final NumberFormat formatoDouble = new DecimalFormat("#.00");
    private boolean seImprimioTicket;
    //fuera de linea
    private PeticionVentaLocalBean peticionVentaLocalBean;
    private RespuestaVentaLocalBean respuestaVentaLocalBean;
    private VentaFueraLineaControlador ventaFueraLineaControlador;
    //private ArticuloFLBean[] articuloBeans;
    //private TipoPagoBean[] tipoPagoBeans;
    private PeticionTransaccionCentralDto peticionTransaccionCentralDto;
    private RespuestaConsultaTransaccionCentralDto respuestaConsultaTransaccionCentralDto;
    private boolean hayMarcacionFueraLinea;
    private final int ACTUALIZACION_LOCAL;
    private final int ACTUALIZACION_CENTRAL;
    private DateFormat formatoFecha;// = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private Calendar calendario;// = Calendar.getInstance();
    private String cadenaDireccion;// = "";
    private String cadenaDelegacion;// = "";
    ////
    private double contador;// = 0;
    //private int i;// = 0;
    private double suma;// = 0.0;
    ///
    private double importe = 0;
    private double cantidad = 0;
    private double devueltos = 0;
    private double precio = 0;
    private double unidad = 0;
    private boolean esEmpaquetado = false;
    ////
    private UsuarioSesionBean sesion;
    ///
    private final int VENTANA_DEVOLUCIONES = 1;

    public DevolucionesVistaModelo(Utilerias _utilerias) {

        SION.log(Modulo.VENTA, "Entrando a constructor de vista modelo en devoluciones", Level.INFO);

        this.utilerias = _utilerias;
        this.parametrosDevolucionesDAO = new ParametrosDao();
        this.articuloDao = new DetalleArticuloDao(utilerias);
        this.listaOrdenada = new ArrayList<ArticuloDto>();
        this.listaTiposPago = new ArrayList<TipoPagoDto>();
        this.peticionVentaDto = new PeticionVentaDto();
        this.peticionDevolucionDto = new PeticionDevolucionDto();
        this.respuestaVentaDto = new RespuestaVentaDto();
        this.peticionDetalleVentaDto = new PeticionDetalleVentaDto();
        this.respuestaDetalleVentaDto = new RespuestaDetalleVentaDto();
        this.devolucionTicketDto = new DevolucionTicketDto();
        this.articuloTicketDtos = null;
        this.tipoPagoTicketDtos = null;
        this.impresionVenta = new ImpresionVenta();
        this.bloqueoVenta = new BloqueoVenta();

        try {
            this.consumirVentaImp = new ConsumirVentaImp();
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error al crear instancia de consumo de venta", Level.INFO);
            SION.logearExcepcion(Modulo.VENTA, ex);
        }

        this.respuestaConciliacionLocalBean = new RespuestaConciliacionLocalBean();
        this.conciliacionLocalControlador = new ConciliacionLocalControlador();
        this.conciliacionLocalBean = new PeticionConciliacionLocalBean();
        this.peticionActTransaccionLocalBean = new PeticionActTransaccionLocalBean();
        this.respuestaActualizacionLocalBean = new RespuestaActualizacionLocalBean();

        this.seImprimioTicket = false;

        //fuera de linea
        this.peticionVentaLocalBean = new PeticionVentaLocalBean();
        this.respuestaVentaLocalBean = new RespuestaVentaLocalBean();
        this.ventaFueraLineaControlador = new VentaFueraLineaControlador();
        this.peticionTransaccionCentralDto = new PeticionTransaccionCentralDto();
        this.respuestaConsultaTransaccionCentralDto = new RespuestaConsultaTransaccionCentralDto();
        this.hayMarcacionFueraLinea = false;

        this.ACTUALIZACION_LOCAL = 0;
        this.ACTUALIZACION_CENTRAL = 1;

        this.formatoFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        this.calendario = Calendar.getInstance();

        this.cadenaDireccion = "";
        this.cadenaDelegacion = "";

        ///
        this.contador = 0;
        //this.i = 0;
        this.suma = 0.0;

        /////
        this.importe = 0;
        this.cantidad = 0;
        this.devueltos = 0;
        this.precio = 0;
        this.unidad = 0;
        this.esEmpaquetado = false;

        /////
        this.sesion = null;//VistaBarraUsuario.getSesion();
        ///
        /////objetos consutla de articulo local
        this.peticionArticuloLocalBean = null;
        this.respuestaArticuloLocalBean = null;
    }
//gets

    public ObservableList<ArticuloBean> getListaArticulosTemp() {
        return listaArticulosTemp;
    }

    public ObservableList<ArticuloBean> getListaArticulosPoliticas() {
        return listaArticulos;
    }

    public ObservableList<ArticuloBean> getListaArticulosTicket() {
        return listaArticulosTicket;
    }

    public ObservableList<ArticuloBean> getListaArticulosSeleccionados() {
        return listaArticulosSeleccionados;
    }

    public ObservableList<DatosFormasPagoBean> getListaFormasPago() {
        return listaDatosFormasPago;
    }

    public int getTamanioListaArticulosPoliticas() {
        return listaArticulos.size();
    }

    public int getTamanioListaArticulosTicket() {
        return listaArticulosTicket.size();
    }

    public int getTamanioListaArticulosSeleccionados() {
        return listaArticulosSeleccionados.size();
    }

    public double getCantidadArticulosPoliticas() {
        contador = 0;
        int i = 0;
        while (i < listaArticulos.size()) {

            if (listaArticulos.get(i).getPagranel() == 0) {
                contador += listaArticulos.get(i).getCantidad();
            } else {
                contador++;
            }

            i++;
        }


        return contador;
    }

    public double getCantidadArticulosTicket() {
        contador = 0;
        int i = 0;
        while (i < listaArticulosSeleccionados.size()) {
            contador += listaArticulosSeleccionados.get(i).getCantidad();
            i++;
        }

        return contador;
    }

    public double getImporteDevolucionPoliticas() {
        int i = 0;
        suma = 0.0;

        while (i < listaArticulos.size()) {
            suma += listaArticulos.get(i).getImporte();
            i++;
        }

        return suma;
    }

    public double getImporteDevolucionTicket() {
        int i = 0;
        suma = 0.0;

        while (i < listaArticulosSeleccionados.size()) {
            suma += listaArticulosSeleccionados.get(i).getImporte();
            i++;
        }

        return suma;
    }

    public double getArticuloImporte(int index) {
        return listaArticulos.get(index).getImporte();
    }

    public double getArticuloCantidad(int index) {
        return listaArticulos.get(index).getCantidad();
    }

    public double getArticuloUnidad(int index) {
        return listaArticulos.get(index).getPaunidad();
    }

    public double getArticuloPrecio(int index) {
        return listaArticulos.get(index).getPaprecio();
    }

    public int getErrorDevolucion() {
        return respuestaVentaDto.getPaCdgError();
    }

    public String getDescErrorDevolucion() {
        return respuestaVentaDto.getPaDescError();
    }

    public long getTransaccionDevolucion() {
        return respuestaVentaDto.getPaTransaccionId();
    }

    //consulta de parametros
    public void consultaParametrosOperacion(int pais, int sistema, int modulo, int submodulo, int configuracion) {
        listaParametros.clear();
        listaParametros.add(parametrosDevolucionesDAO.consultaParametrosOperacion(pais, sistema, modulo, submodulo, configuracion));
    }

    public int consultaErrorParametro() {
        return listaParametros.get(0).getErrorId();
    }

    public String consultaDescErrorParametro() {
        return listaParametros.get(0).getDescripcionError();
    }

    public double consultaValorConfiguracionParametro() {
        return listaParametros.get(0).getValorConfiguracion();
    }

    public void insertaPeticionConciliacion(int sistema, int modulo, int submodulo) {

        SION.log(Modulo.VENTA, "Insertando petición de conciliación en devolución", Level.INFO);

        conciliacionLocalBean.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
        conciliacionLocalBean.setPaTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
        conciliacionLocalBean.setPaUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());
        conciliacionLocalBean.setPaSistemaId(sistema);
        conciliacionLocalBean.setPaModulo(modulo);
        conciliacionLocalBean.setPaSubmoduloId(submodulo);

        SION.log(Modulo.VENTA, "Datos de conciliación enviados: " + conciliacionLocalBean.toString(), Level.INFO);

    }

    public long obtenerRespuestaConciliacion() {

        SION.log(Modulo.VENTA, "Obteniendo respuesta de conciliación de devolución", Level.INFO);

        respuestaConciliacionLocalBean = conciliacionLocalControlador.conciliacionVentaLocal(conciliacionLocalBean);


        SION.log(Modulo.VENTA, "Respuesta de conciliación recibida : " + respuestaConciliacionLocalBean.toString(), Level.INFO);
        return respuestaConciliacionLocalBean.getPaConciliacionId();

    }

    /////
    public void insertarArticuloSeleccionado(int index, int indexSeleccionados) {

        listaArticulosSeleccionados.add(
                new ArticuloBean(
                listaArticulosTicket.get(index).getCantidad(),
                listaArticulosTicket.get(index).getPacdgbarras(),
                listaArticulosTicket.get(index).getPaprecio(),
                listaArticulosTicket.get(index).getPaarticuloid(),
                listaArticulosTicket.get(index).getPanombrearticulo(),
                listaArticulosTicket.get(index).getPafamiliaid(),
                listaArticulosTicket.get(index).getPacosto(),
                listaArticulosTicket.get(index).getPaiva(),
                listaArticulosTicket.get(index).getPadescuento(),
                listaArticulosTicket.get(index).getPagranel(),
                listaArticulosTicket.get(index).getPaUnidadMedida(),
                listaArticulosTicket.get(index).getPaCdgBarrasPadre(),
                listaArticulosTicket.get(index).getPacdgerror(),
                listaArticulosTicket.get(index).getPadescerror(),
                listaArticulosTicket.get(index).getImporte(),
                listaArticulosTicket.get(index).getCompaniaid(),
                listaArticulosTicket.get(index).getDescripcioncompania(),
                listaArticulosTicket.get(index).getDevcantidad(),
                listaArticulosTicket.get(index).getDevimporte(),
                listaArticulosTicket.get(index).getDevueltos(),
                listaArticulosTicket.get(index).getPaunidad(),
                listaArticulosTicket.get(index).getPaIepsId(),
                listaArticulosTicket.get(index).getMetodoEntrada()));

        listaArticulosSeleccionados.get(indexSeleccionados).setCantidad(1);
        listaArticulosSeleccionados.get(indexSeleccionados).setDevueltos(0);
        listaArticulosSeleccionados.get(indexSeleccionados).setDevimporte(0);
        listaArticulosSeleccionados.get(indexSeleccionados).setImporte(
                listaArticulosSeleccionados.get(indexSeleccionados).getCantidad()
                * listaArticulosSeleccionados.get(indexSeleccionados).getPaprecio());

        listaArticulosTicket.get(index).setDevueltos(
                listaArticulosTicket.get(index).getDevueltos() + 1);
        listaArticulosTicket.get(index).setCantidad(
                listaArticulosTicket.get(index).getCantidad() - 1);
        listaArticulosTicket.get(index).setImporte(
                listaArticulosTicket.get(index).getCantidad()
                * listaArticulosTicket.get(index).getPaprecio());


    }

    public void modificarListaArticulos(int index, int id) {

        if (index >= 0) {

            if (listaArticulos.get(index).getImporte() > 0
                    && (listaArticulos.get(index).getCantidad()
                    > listaArticulos.get(index).getDevueltos())) {//revisa el importe y que no se haya devuelto el total del registro

                importe = 0;
                cantidad = 0;
                devueltos = 0;
                precio = 0;
                unidad = 0;
                esEmpaquetado = false;

                cantidad = listaArticulos.get(index).getCantidad();
                devueltos = listaArticulos.get(index).getDevueltos();
                importe = listaArticulos.get(index).getImporte();
                precio = listaArticulos.get(index).getPaprecio();
                unidad = listaArticulos.get(index).getPaunidad();

                if (listaArticulos.get(index).getPaunidad() > 1) {
                    esEmpaquetado = true;
                }

                if (id == 0) {//suprimir

                    importe = precio * (cantidad - devueltos);
                    importe = importe * -1;
                    cantidad = cantidad - devueltos;
                    devueltos = (int) (devueltos + (cantidad));
                    listaArticulos.get(index).setDevueltos(devueltos);
                } else if (id == 2) {//restar 1 o si es caja su contenido

                    if (devueltos < cantidad) {

                        if (esEmpaquetado) {
                            cantidad = unidad;
                            importe = unidad * precio;
                            devueltos = (int) (devueltos + unidad);
                        } else {
                            cantidad = 1;
                            devueltos += 1;
                            importe = precio;
                        }

                        importe *= -1;
                        listaArticulos.get(index).setDevueltos(devueltos);
                    }

                    if (listaArticulos.get(index).getPagranel() != 0) {

                        cantidad = listaArticulos.get(index).getCantidad();
                        importe = (precio * listaArticulos.get(index).getCantidad()) * -1;
                        listaArticulos.get(index).setDevueltos(listaArticulos.get(index).getCantidad());
                    }

                } else if (id == 1) {//sumar

                    if (esEmpaquetado) {
                        cantidad = unidad;
                        importe = precio * unidad;
                    } else {
                        cantidad = 1;
                        importe = precio;
                    }
                }

                insertarRegistroArticulo(index, id, esEmpaquetado, unidad, precio, cantidad, importe);

            }
        }

    }

    public void insertarRegistroArticulo(int index, int id, boolean esEmpaquetado, double unidad, double precio,
            double cantidad, double importe) {

        if (listaArticulos.get(listaArticulos.size() - 1).getPacdgbarras().equals(listaArticulos.get(index).getPacdgbarras())) {

            //se encuentra en el ultimo registro
            if (id == 1) {//sumar

                if (listaArticulos.get(listaArticulos.size() - 1).getImporte() > 0) {
                    if (esEmpaquetado) {
                        listaArticulos.get(listaArticulos.size() - 1).setCantidad(listaArticulos.get(listaArticulos.size() - 1).getCantidad() + unidad);
                        listaArticulos.get(listaArticulos.size() - 1).setImporte(listaArticulos.get(listaArticulos.size() - 1).getImporte() + (unidad * precio));
                    } else {
                        listaArticulos.get(listaArticulos.size() - 1).setCantidad(listaArticulos.get(listaArticulos.size() - 1).getCantidad() + 1);
                        listaArticulos.get(listaArticulos.size() - 1).setImporte(listaArticulos.get(listaArticulos.size() - 1).getImporte() + (precio));
                    }
                } else {
                    agregarArticulo(index, cantidad, importe, 0);
                }
            } else {
                agregarArticulo(index, cantidad * -1, importe, listaArticulos.get(index).getDevueltos());
            }
        } else {//insertar nueva fila
            if (id == 1) {//si suma

                agregarArticulo(index, cantidad, importe, 0);
            } else {

                agregarArticulo(index, cantidad * -1, importe, listaArticulos.get(index).getDevueltos());
            }

        }

    }

    public void agregarArticulo(int index, double cantidad, double importe, double devueltos) {
        listaArticulos.add(new ArticuloBean(
                //InicioVistaModelo.listaArticulosVenta.get(index).getCantidad(),
                cantidad,
                listaArticulos.get(index).getPacdgbarras(),
                listaArticulos.get(index).getPaprecio(),
                listaArticulos.get(index).getPaarticuloid(),
                listaArticulos.get(index).getPanombrearticulo(),
                listaArticulos.get(index).getPafamiliaid(),
                listaArticulos.get(index).getPacosto(),
                listaArticulos.get(index).getPaiva(),
                listaArticulos.get(index).getPadescuento(),
                listaArticulos.get(index).getPagranel(),
                listaArticulos.get(index).getPaUnidadMedida(),
                listaArticulos.get(index).getPaCdgBarrasPadre(),
                listaArticulos.get(index).getPacdgerror(),
                listaArticulos.get(index).getPadescerror(),
                importe,
                listaArticulos.get(index).getCompaniaid(),
                listaArticulos.get(index).getDescripcioncompania(),
                listaArticulos.get(index).getDevcantidad(),
                listaArticulos.get(index).getDevimporte(),
                devueltos,
                listaArticulos.get(index).getPaunidad(),
                listaArticulos.get(index).getPaIepsId(),
                listaArticulos.get(index).getMetodoEntrada()));
    }

    public void insertarRegistroFormaPago(DatosFormasPagoBean datosFormasPagoBean) {

        SION.log(Modulo.VENTA, "Insertando registro de forma de pago de devolución : " + datosFormasPagoBean.toString(), Level.INFO);
        listaDatosFormasPago.add(datosFormasPagoBean);

    }

    public void insertarArticulosDevolucionTicket() {

        SION.log(Modulo.VENTA, "Llenando arreglo ordenado de artículos en devolución por ticket", Level.INFO);

        int i = 0;

        while (i < listaArticulosSeleccionados.size()) {



            listaOrdenada.add(new ArticuloDto(
                    listaArticulosSeleccionados.get(i).getPaarticuloid(),
                    listaArticulosSeleccionados.get(i).getPacdgbarras(),
                    listaArticulosSeleccionados.get(i).getCantidad(),
                    listaArticulosSeleccionados.get(i).getPaprecio(),
                    listaArticulosSeleccionados.get(i).getPacosto(),
                    listaArticulosSeleccionados.get(i).getPadescuento(),
                    listaArticulosSeleccionados.get(i).getPaiva(),
                    listaArticulosSeleccionados.get(i).getPanombrearticulo(),
                    listaArticulosSeleccionados.get(i).getPagranel(),
                    listaArticulosSeleccionados.get(i).getPaIepsId(),
                    listaArticulosSeleccionados.get(i).getTipoDescuento()
                    ));
            i++;
        }

        SION.log(Modulo.VENTA, "Articulos agregados al arreglo: " + listaOrdenada.toString(), Level.INFO);
    }

    public void agregarObjetoSesion() {

        SION.log(Modulo.VENTA, "Agregando objetos a sesión.", Level.INFO);

        if (respuestaVentaDto.getPaTypCursorBlqs() != null) {

            BloqueoVentaBean nuevosDatos = (BloqueoVentaBean) VistaBarraUsuario.getSesion().getBloqueosVenta();
            nuevosDatos.setMensajeBloqueo(bloqueoVenta.bloqueDtoWSaSesion(respuestaVentaDto.getPaTypCursorBlqs()));
            nuevosDatos.setCdgError(0);
            VistaBarraUsuario.getSesion().setBloqueosVenta(nuevosDatos);

        } else if (respuestaVentaDto.getPaCdgError() == 0) {
            BloqueoVentaBean nuevosDatos = new BloqueoVentaBean();
            nuevosDatos.setCdgError(-1);
            VistaBarraUsuario.getSesion().setBloqueosVenta(nuevosDatos);
        } else {
            VistaBarraUsuario.getSesion().setBloqueosVenta(null);
        }

    }

    public void insertarArticulosDevolucion() {

        SION.log(Modulo.VENTA, "Llenando arreglo ordenado de artículos en devolución", Level.INFO);

        int idArticuloRecarga = 0;

        int h = 0;
        while (h < listaArticulos.size()) {

            int j = 0, id = 0;
            while (j < listaOrdenada.size()) {
                if (listaArticulos.get(h).getPacdgbarras().equals(listaOrdenada.get(j).getFcCdGbBarras())
                        && listaArticulos.get(h).getDevueltos() < listaArticulos.get(h).getCantidad()) {
                    //si se encuenra articulo en ultima linea
                    id = 1;
                    listaOrdenada.get(j).setFnCantidad(listaOrdenada.get(j).getFnCantidad() + (listaArticulos.get(h).getCantidad() - listaArticulos.get(h).getDevueltos()));
                    j = listaOrdenada.size();
                }
                j++;
            }
            if (id == 0) {

                if (listaArticulos.get(h).getDevueltos() < listaArticulos.get(h).getCantidad()) {

                    double cantidadArticulos = listaArticulos.get(h).getCantidad() - listaArticulos.get(h).getDevueltos();

                    if (cantidadArticulos > 0) {//valida k sea venta o devolucion no vacia
                        listaOrdenada.add(new ArticuloDto(
                                listaArticulos.get(h).getPaarticuloid(),
                                listaArticulos.get(h).getPacdgbarras(),
                                cantidadArticulos,
                                listaArticulos.get(h).getPaprecio(),
                                listaArticulos.get(h).getPacosto(),
                                listaArticulos.get(h).getPadescuento(),
                                listaArticulos.get(h).getPaiva(),
                                listaArticulos.get(h).getPanombrearticulo(),
                                listaArticulos.get(h).getPagranel(),
                                listaArticulos.get(h).getPaIepsId(),
                                listaArticulos.get(h).getTipoDescuento()
                                        ));
                    }
                }

            }
            h++;
        }


        SION.log(Modulo.VENTA, "Articulos agregados al arreglo: " + listaOrdenada.toString(), Level.INFO);

    }

    public void agregarTipoPago() {

        SION.log(Modulo.VENTA, "Agregando registros de pago de devolución al arreglo DTO", Level.INFO);

        for (Iterator<DatosFormasPagoBean> it = listaDatosFormasPago.iterator(); it.hasNext();) {
            DatosFormasPagoBean datosFormasPagoBean = it.next();
            listaTiposPago.add(new TipoPagoDto(
                    datosFormasPagoBean.getFitipopagoid(), Double.parseDouble(utilerias.getNumeroFormateado(datosFormasPagoBean.getFnmontopago())),
                    datosFormasPagoBean.getFnnumerovales(), //datosFormasPagoBean.getFcnumerotarjeta(),
                    /*
                     * datosFormasPagoBean.getFitipotarjeta(),
                     */ datosFormasPagoBean.getTarjetaidbus(),
                    Double.parseDouble(utilerias.getNumeroFormateado(datosFormasPagoBean.getComisiontarjeta()))));
        }

        SION.log(Modulo.VENTA, "Regitros de pago agregados a Dto : " + listaTiposPago.toString(), Level.INFO);


    }

    /////fuera de linea
    public boolean marcarDevolucionFueraDeLinea(double montoVenta, int sistema, int modulo, int submodulo, long conciliacion,
            boolean _esCancelacion, boolean _esDevolucionTicket, long _usuarioAutoriza) {

        if(_esDevolucionTicket){
            SION.log(Modulo.VENTA, "Entrando a método de marcado de devolución por ticket en fuera de línea", Level.INFO);
        }else{
            SION.log(Modulo.VENTA, "Entrando a método de marcado de devolución por politicas en fuera de línea", Level.INFO);
        }
        
        peticionVentaLocalBean = null;
        peticionVentaLocalBean = new PeticionVentaLocalBean();

        sesion = VistaBarraUsuario.getSesion();
        String t = sesion.getIpEstacion();
        
        t = t.concat("|CAJA");
        t = t.concat(String.valueOf(sesion.getNumeroEstacion()));
        peticionVentaLocalBean.setPaTerminal(t);
        peticionVentaLocalBean.setPaUsuarioId(sesion.getUsuarioId());
        peticionVentaLocalBean.setPaUsuarioAutorizaId(_usuarioAutoriza);
        peticionVentaLocalBean.setPaTotalVenta(Double.parseDouble(utilerias.getNumeroFormateado(montoVenta)));
        peticionVentaLocalBean.setPaPaisId(sesion.getPaisId());
        peticionVentaLocalBean.setPaTiendaId(sesion.getTiendaId());
        peticionVentaLocalBean.setPaSistemaId(sistema);
        peticionVentaLocalBean.setPaModuloId(modulo);
        peticionVentaLocalBean.setPaSubModuloId(submodulo);

        if (_esCancelacion) {
            peticionVentaLocalBean.setPaIndicador(4);

        } else {
            peticionVentaLocalBean.setPaIndicador(3);
        }
        peticionVentaLocalBean.setPaConciliacionId(conciliacion);
        
        if(_esDevolucionTicket){
            peticionVentaLocalBean.setPaMovimientoDev(respuestaDetalleVentaDto.getPaMovimientoId());
        }else{
            peticionVentaLocalBean.setPaMovimientoDev(0);
        }

        int h = 0;

        ArticuloFLBean[] articuloBeans = new ArticuloFLBean[listaOrdenada.size()];

        for (Iterator<ArticuloDto> it = listaOrdenada.iterator(); it.hasNext();) {
            ArticuloDto listaArticulosDto = it.next();

            ArticuloFLBean articuloBeanFueraLinea = new ArticuloFLBean();

            articuloBeanFueraLinea.setFiArticuloId(listaArticulosDto.getFiArticuloId());
            articuloBeanFueraLinea.setFcCdgBarras(listaArticulosDto.getFcCdGbBarras());
            articuloBeanFueraLinea.setFnCantidad(listaArticulosDto.getFnCantidad());
            articuloBeanFueraLinea.setFnPrecio(listaArticulosDto.getFnPrecio());
            articuloBeanFueraLinea.setFnCosto(listaArticulosDto.getFnCosto());
            articuloBeanFueraLinea.setFnDescuento(listaArticulosDto.getFnDescuento());
            articuloBeanFueraLinea.setFnIva(listaArticulosDto.getFnIva());
            articuloBeanFueraLinea.setFnIeps(listaArticulosDto.getFnIepsId());

            articuloBeans[h] = articuloBeanFueraLinea;

            h++;
        }

        peticionVentaLocalBean.setPaDatosArticulo(articuloBeans);

        /*SION.log(Modulo.VENTA, "Datos de los artículos en la petición de devolución fuera de línea : ", Level.INFO);
        for (h = 0; h < listaOrdenada.size(); h++) {
            SION.log(Modulo.VENTA, articuloBeans[h].toString(), Level.INFO);
        }*/

        h = 0;

        TipoPagoBean[] tipoPagoBeans = new TipoPagoBean[listaTiposPago.size()];

        for (Iterator<TipoPagoDto> it = listaTiposPago.iterator(); it.hasNext();) {
            TipoPagoDto listaPagosDto = it.next();

            TipoPagoBean tipoPagoBeanFueraLinea = new TipoPagoBean();

            tipoPagoBeanFueraLinea.setFiTipoPagoId(listaPagosDto.getFiTipoPagoId());
            tipoPagoBeanFueraLinea.setFcReferenciaPagoTarjetas(String.valueOf(listaPagosDto.getPaPagoTarjetaIdBus()));
            tipoPagoBeanFueraLinea.setFnNumeroVales(listaPagosDto.getFnNumeroVales());
            tipoPagoBeanFueraLinea.setFnImporteAdicional(listaPagosDto.getImporteAdicional());
            tipoPagoBeanFueraLinea.setFnMontoPago(listaPagosDto.getFnMontoPago());

            tipoPagoBeans[h] = tipoPagoBeanFueraLinea;

            h++;
        }

        /*SION.log(Modulo.VENTA, "Formas de pago en la petición de devolución fuera de línea : ", Level.INFO);
        for (h = 0; h < tipoPagoBeans.length; h++) {
            SION.log(Modulo.VENTA, tipoPagoBeans[h].toString(), Level.INFO);
        }*/

        peticionVentaLocalBean.setPaDatosTiposPago(tipoPagoBeans);
        
        SION.log(Modulo.VENTA, "Petición de devolución: " + peticionVentaLocalBean.toString(), Level.INFO);

        respuestaVentaLocalBean = ventaFueraLineaControlador.spServicioVentaLocaL(peticionVentaLocalBean);

        SION.log(Modulo.VENTA, "Respuesta recibida : " + respuestaVentaLocalBean.toString(), Level.INFO);


        if (respuestaVentaLocalBean.getPaCdgError() == 0) {
            return true;
        }else{
            return false;
        }
    }

    public void llenarPeticionTransaccionCentral(long conciliacion, int sistema, int modulo, int submodulo) {

        SION.log(Modulo.VENTA, "Cargando petición de actualización de transacción central", Level.INFO);

        sesion = VistaBarraUsuario.getSesion();

        peticionTransaccionCentralDto.setPaPaisId(sesion.getPaisId());
        peticionTransaccionCentralDto.setTiendaId(sesion.getTiendaId());
        peticionTransaccionCentralDto.setPaConciliacionId(conciliacion);
        peticionTransaccionCentralDto.setPaUsuarioId(sesion.getUsuarioId());
        peticionTransaccionCentralDto.setPaSistemaId(sistema);
        peticionTransaccionCentralDto.setPaModuloId(modulo);
        peticionTransaccionCentralDto.setPaSubModuloId(submodulo);

        SION.log(Modulo.VENTA, "Petición de actualización  : " + peticionTransaccionCentralDto.toString(), Level.INFO);

    }

    public long respuestaTransaccionCentral() {

        SION.log(Modulo.VENTA, "Consultando transacción asociada a la conciliación", Level.INFO);

        hayMarcacionFueraLinea = false;

        try {
            respuestaConsultaTransaccionCentralDto = consumirVentaImp.consultarTransaccionCentral(peticionTransaccionCentralDto);

            SION.log(Modulo.VENTA, "Se recibió respuesta de transacción: " + respuestaConsultaTransaccionCentralDto.toString(), Level.INFO);

        } catch (SionException ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error al intentar obtener respuesta de la transacción: "
                    + ex.getMensajeFront().replace("##", String.valueOf(ex.getCodigoAccion())), Level.SEVERE);

            hayMarcacionFueraLinea = true;
        }

        return respuestaConsultaTransaccionCentralDto.getPaCdgError();
    }

    public boolean hayMarcacionFueraDeLinea() {
        return hayMarcacionFueraLinea;
    }

    public long getTransaccionCentral() {
        return respuestaConsultaTransaccionCentralDto.getPaTransaccionId();
    }

    public long getConciliacionIdFueraLinea() {
        return respuestaVentaLocalBean.getPaConciliacionId();
    }

    //conciliacion
    public void cancelarConciliacionDevolucion(long conciliacion) {

        peticionActTransaccionLocalBean = null;
        peticionActTransaccionLocalBean = new PeticionActTransaccionLocalBean();

        peticionActTransaccionLocalBean.setPaConciliacionId(conciliacion);
        peticionActTransaccionLocalBean.setPaTransaccionVenta(getTransaccion());
        
        SION.log(Modulo.VENTA, "Peticion de cancelacion de conciliacion", Level.INFO);

        conciliacionLocalControlador.updCancelacionConciliacion(peticionActTransaccionLocalBean);
        
    }

    public void actualizarTransacciónLocal(long conciliacion, int sistema, int modulo, int submodulo,
            int tipoActualizacion) {
        
        peticionActTransaccionLocalBean = null;
        peticionActTransaccionLocalBean = new PeticionActTransaccionLocalBean();

        peticionActTransaccionLocalBean.setPaConciliacionId(conciliacion);
        if (tipoActualizacion == ACTUALIZACION_LOCAL) {
            peticionActTransaccionLocalBean.setPaTransaccionVenta(getTransaccionDevolucion());
        } else {
            peticionActTransaccionLocalBean.setPaTransaccionVenta(getTransaccionCentral());
        }
        SION.log(Modulo.VENTA, "Petición de actualización de transacción local : " + peticionActTransaccionLocalBean.toString(), Level.INFO);

        respuestaActualizacionLocalBean = conciliacionLocalControlador.actualizarTransaccionLocal(peticionActTransaccionLocalBean);

        int contador = 0;

        while (true && contador < 3) {
            if (respuestaActualizacionLocalBean.getPaCdgError() == 0) {
                break;
            } else {
                respuestaActualizacionLocalBean = conciliacionLocalControlador.actualizarTransaccionLocal(peticionActTransaccionLocalBean);
            }

            contador++;
        }

        SION.log(Modulo.VENTA, "Respuesta de actualización: " + respuestaActualizacionLocalBean.toString(), Level.INFO);


    }

    public void insertapeticionDevolucionDto(double montoDevolucion, String fecha, int tipoDevolucion,
            long transaccionDev, long movimientoiD, long conciliacion, boolean esCancelacion, long _usuarioAutoriza ) {

        sesion = VistaBarraUsuario.getSesion();

        SION.log(Modulo.VENTA, "cargando petición de devolucion", Level.INFO);

        String t = sesion.getIpEstacion();
        ///llenando peticion de devolucion
        peticionDevolucionDto.setArticulos(listaOrdenada.toArray(new ArticuloDto[0]));
        peticionDevolucionDto.setPaFechaOper(fecha);
        peticionDevolucionDto.setPaMontoTotalVta(Double.parseDouble(utilerias.getNumeroFormateado(montoDevolucion)));//
        peticionDevolucionDto.setPaPaisId(sesion.getPaisId());//
        peticionDevolucionDto.setTiendaId(sesion.getTiendaId());//
        t = t.concat("|CAJA");
        t = t.concat(String.valueOf(sesion.getNumeroEstacion()));
        peticionDevolucionDto.setPaTerminal(t);//
        if (esCancelacion) {
            peticionDevolucionDto.setPaTipoMovto(4);///4 para cancelacion
            peticionDevolucionDto.setPaTipoDevolucion(3);
        } else {
            peticionDevolucionDto.setPaTipoMovto(3);///3 para devolucion
            peticionDevolucionDto.setPaTipoDevolucion(tipoDevolucion);
        }
        peticionDevolucionDto.setPaUsuarioId(sesion.getUsuarioId());//
        peticionDevolucionDto.setPaUsuarioAutorizaId(_usuarioAutoriza);
        peticionDevolucionDto.setTiposPago(listaTiposPago.toArray(new TipoPagoDto[0]));
        peticionDevolucionDto.setPaConciliacionId(conciliacion);


        peticionDevolucionDto.setPaTransaccionDev(transaccionDev);
        peticionDevolucionDto.setPaMovimientoIdDev(movimientoiD);

        SION.log(Modulo.VENTA, "Petición de devolución : " + peticionDevolucionDto.toString(), Level.INFO);
    }

    public void registrarPeticionDetalleVenta(String transaccion) {

        peticionDetalleVentaDto.setPaNumTransaccion(transaccion);
        peticionDetalleVentaDto.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
        peticionDetalleVentaDto.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());

        SION.log(Modulo.VENTA, "petición de detalle de venta: " + peticionDetalleVentaDto.toString(), Level.INFO);

    }

    public int obtenerRespuestaDetalleVenta() {

        int bandera = 0;
        
        respuestaDetalleVentaDto = null;
        respuestaDetalleVentaDto = new RespuestaDetalleVentaDto();

        try {
            respuestaDetalleVentaDto = consumirVentaImp.obtenerDetalleVenta(peticionDetalleVentaDto);

            SION.log(Modulo.VENTA, "Respuesta del detalle de la devolución recibido exitosamente : "
                    + respuestaDetalleVentaDto.toString(), Level.INFO);

        } catch (SionException ex) {
            respuestaDetalleVentaDto = null;
            SION.log(Modulo.VENTA, "Se generó una excepción al intentar obtener la respuetsa del detalle de venta", Level.INFO);
            SION.logearExcepcion(Modulo.VENTA, ex, peticionDetalleVentaDto.toString());
            bandera = 1;
        }

        return bandera;
    }

    public RespuestaDetalleVentaDto getRespuestaDetalleVentaDto() {
        return respuestaDetalleVentaDto;
    }

    public void insertarArticuloDetalle() {

        ArticuloDto[] articuloDtos = respuestaDetalleVentaDto.getPaCurDetalleVenta();

        if (articuloDtos != null) {

            SION.log(Modulo.VENTA, "Insertando en tabla detalle de articulos " + Arrays.toString(articuloDtos), Level.INFO);

            for (int i = 0; i < articuloDtos.length; i++) {
                listaArticulosTicket.add(new ArticuloBean(
                        articuloDtos[i].getFnCantidad(),
                        articuloDtos[i].getFcCdGbBarras(),
                        articuloDtos[i].getFnPrecio(),
                        articuloDtos[i].getFiArticuloId(),//
                        articuloDtos[i].getFcNombreArticulo(),//nombre
                        0,//familia
                        articuloDtos[i].getFnCosto(),
                        articuloDtos[i].getFnIva(),//
                        (double) articuloDtos[i].getFnDescuento(),
                        articuloDtos[i].getFiAgranel(),//granel
                        0,//unidadmedida
                        0,
                        0,//error
                        "",//desc error
                        articuloDtos[i].getFnCantidad() * articuloDtos[i].getFnPrecio(),//importe
                        0,
                        "",
                        0, 0, 0, 1, articuloDtos[i].getFnIepsId(),0));

            }

            SION.log(Modulo.VENTA, "Articulos agregados a la tabla: " + articuloDtos[0].toString(), Level.INFO);
            //SION.log(Modulo.VENTA, "Artículos recibidos de BD: "+listaArticulos.get(0).getPacdgbarras(), Level.INFO);

        } else {
            SION.log(Modulo.VENTA, "No se pudo insertar la respuesta del detalle de la venta : " + articuloDtos.toString(), Level.WARNING);
        }

    }

    public void registrarDevolucion() throws SionException {

        SION.log(Modulo.VENTA, "Registrando petición de Devolución", Level.INFO);
        respuestaVentaDto = consumirVentaImp.obtenerDevolucionVenta(peticionDevolucionDto);
        SION.log(Modulo.VENTA, "Respuesta obtenida de la petición : " + respuestaVentaDto.toString(), Level.INFO);
        //
    }

    public long getTransaccion() {
        return respuestaVentaDto.getPaTransaccionId();
    }

    //impresion
    public String obtenerFecha() {

        return formatoFecha.format(new Date());
    }

    public void imprimirTicket(String transaccion, boolean esDevolucionPoliticas) {

        SION.log(Modulo.VENTA, "cargando petición de impresión", Level.INFO);

        boolean esSuperPrecio = false;

        consultaParametrosOperacion(VistaBarraUsuario.getSesion().getPaisId(),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ticket.superprecio.sistema")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ticket.superprecio.modulo")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ticket.superprecio.submodulo")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ticket.superprecio.configuracion")));

        if (consultaErrorParametro() == 0) {

            if (consultaValorConfiguracionParametro() != 1) {
                esSuperPrecio = true;
            }

        }

        sesion = VistaBarraUsuario.getSesion();

        cadenaDireccion = "";
        cadenaDelegacion = "";

        cadenaDireccion = cadenaDireccion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getCalle()));
        cadenaDireccion = cadenaDireccion.concat(" ");
        cadenaDireccion = cadenaDireccion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroExterior()));
        if (!"".equals(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior())
                && !"0".equals(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior())
                && VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior() != null) {
            cadenaDireccion = cadenaDireccion.concat("-");
            cadenaDireccion = cadenaDireccion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior()));
        }

        cadenaDelegacion = cadenaDelegacion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getMunicipio()).concat(" ").concat(
                String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getEstado())).concat(" ").concat(
                String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getCodigoPostal())));

        devolucionTicketDto.setCaja(String.valueOf(sesion.getNumeroEstacion()));
        devolucionTicketDto.setCajero(String.valueOf(sesion.getUsuarioId()));
        devolucionTicketDto.setCalleNumeroTienda(cadenaDireccion);
        devolucionTicketDto.setColoniaTienda(sesion.getDatosTienda().getColonia());
        devolucionTicketDto.setDelegacionCiudadEstadoCP(cadenaDelegacion);
        //String fechaD = obtenerFecha();
        devolucionTicketDto.setFecha(obtenerFecha().substring(0, obtenerFecha().indexOf(" ")));
        devolucionTicketDto.setHora(obtenerFecha().substring(10));
        devolucionTicketDto.setFolio(transaccion);
        devolucionTicketDto.setNombreTienda(sesion.getNombreTienda());
        devolucionTicketDto.setTiendaId(String.valueOf(sesion.getTiendaId()));
        devolucionTicketDto.setTransaccion(transaccion);
        
        if(esSuperPrecio){
            devolucionTicketDto.setEncabezado(SION.obtenerMensaje(Modulo.VENTA, "ENCABEZADO.TICKET.SUPR"));
        }else{
            devolucionTicketDto.setEncabezado(SION.obtenerMensaje(Modulo.VENTA, "ENCABEZADO.TICKET"));
        }

        devolucionTicketDto.setArticulos(impresionVenta.articulosDtoaTicket(listaOrdenada));

        if (esDevolucionPoliticas) {
            devolucionTicketDto.setTiposPago(impresionVenta.tipoPagoDtoaTicket(
                    listaTiposPago, getImporteDevolucionPoliticas(), getImporteDevolucionPoliticas(), getImporteDevolucionPoliticas()));
        } else {
            devolucionTicketDto.setTiposPago(impresionVenta.tipoPagoDtoaTicket(
                    listaTiposPago, getImporteDevolucionTicket(), getImporteDevolucionTicket(),
                    getImporteDevolucionTicket()));
        }
        devolucionTicketDto.setTotalArticulos();

        devolucionTicketDto.setIvaTotal();


        SION.log(Modulo.VENTA, "petición de impresión de la devolución : " + devolucionTicketDto.toString(), Level.INFO);

        try {
            impresionVenta.imprimirTicketDevolucion(devolucionTicketDto, true);
            SION.log(Modulo.VENTA, "Imprimiendo ticket", Level.INFO);
            seImprimioTicket = true;
        } catch (Exception e) {
            SION.log(Modulo.VENTA, "Ocurrió un problema al imprimir el ticket de devolución : " + e.getLocalizedMessage(), Level.SEVERE);
        }

    }

    public boolean seImprimioTicket() {
        return seImprimioTicket;
    }

    //limpiar
    public void limpiarListasDto() {
        listaOrdenada.clear();
        listaTiposPago.clear();
    }

    public void limpiarRegistrosPago() {
        listaDatosFormasPago.clear();
    }

    /*
     * ------------------------------------------------------------------------
     */
    ////////////consulta articulo local
    public void llenarPeticionArticuloLocal(double cantidad, String codigoBarras) {
        peticionArticuloLocalBean = null;
        peticionArticuloLocalBean = new PeticionDetalleArticuloBean();
        peticionArticuloLocalBean.setCantidad(cantidad);
        peticionArticuloLocalBean.setCodigoBarras(codigoBarras);
        peticionArticuloLocalBean.setVentana(VENTANA_DEVOLUCIONES);
    }

    public RespuestaDetalleArticuloBean consultarArticuloLocal() {
        respuestaArticuloLocalBean = null;
        if (peticionArticuloLocalBean != null) {
            respuestaArticuloLocalBean = new RespuestaDetalleArticuloBean();
            respuestaArticuloLocalBean = articuloDao.consultaArticulo(peticionArticuloLocalBean);
        }
        return respuestaArticuloLocalBean;
    }

    public int getCodigoErrorRespuestaArticuloLocal() {
        return respuestaArticuloLocalBean.getCodigoError();
    }

    public ArticuloBean getArticuloLocal() {
        return respuestaArticuloLocalBean.getArticulos();
    }
}
