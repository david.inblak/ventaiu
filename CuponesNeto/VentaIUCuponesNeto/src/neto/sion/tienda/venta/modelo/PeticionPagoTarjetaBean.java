/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

public class PeticionPagoTarjetaBean
{
  private long tiendaId;
  private int paisId;
  private double monto;
  private String numTerminal;
  private boolean sePermiteCreditoDebito;
  private boolean sePermiteVales;
  private boolean seGeneroPeticion;
  private String mensaje;
  private String canal;
  private String afiliacion;
  
  public double getMonto()
  {
    return monto;
  }
  
  public void setMonto(double monto)
  {
    this.monto = monto;
  }
  
  public String getNumTerminal()
  {
    return numTerminal;
  }
  
  public void setNumTerminal(String numTerminal)
  {
    this.numTerminal = numTerminal;
  }
  
  public int getPaisId()
  {
    return paisId;
  }
  
  public void setPaisId(int paisId)
  {
    this.paisId = paisId;
  }
  
  public long getTiendaId()
  {
    return tiendaId;
  }
  
  public void setTiendaId(long tiendaId)
  {
    this.tiendaId = tiendaId;
  }
  
  public boolean isSePermiteCreditoDebito()
  {
    return sePermiteCreditoDebito;
  }
  
  public void setSePermiteCreditoDebito(boolean sePermiteCreditoDebito)
  {
    this.sePermiteCreditoDebito = sePermiteCreditoDebito;
  }
  
  public boolean isSePermiteVales()
  {
    return sePermiteVales;
  }
  
  public void setSePermiteVales(boolean sePermiteVales)
  {
    this.sePermiteVales = sePermiteVales;
  }
  
  public boolean isSeGeneroPeticion()
  {
    return seGeneroPeticion;
  }
  
  public void setSeGeneroPeticion(boolean seGeneroPeticion)
  {
    this.seGeneroPeticion = seGeneroPeticion;
  }
  
  public String getMensaje()
  {
    return mensaje;
  }
  
  public void setMensaje(String mensaje)
  {
    this.mensaje = mensaje;
  }
  
  public String getCanal()
  {
    return canal;
  }
  
  public void setCanal(String canal)
  {
    this.canal = canal;
  }
  
  public String getAfiliacion()
  {
    return afiliacion;
  }
  
  public void setAfiliacion(String afiliacion)
  {
    this.afiliacion = afiliacion;
  }
  
  public String toString()
  {
    return "PeticionPagoTarjetaBean{tiendaId=" + tiendaId + ", paisId=" + paisId + ", monto=" + monto + ", numTerminal=" + numTerminal + ", sePermiteCreditoDebito=" + sePermiteCreditoDebito + ", sePermiteVales=" + sePermiteVales + ", seGeneroPeticion=" + seGeneroPeticion + ", mensaje=" + mensaje + ", canal=" + canal + ", afiliacion=" + afiliacion + '}';
  }
}
