/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

import neto.sion.tienda.venta.promociones.cupones.mvp.concreto.IdentificadorDescuentos;

/**
 *
 * @author dramirezr
 */
public enum TiposDescuento {
    SinDescuento("Sin Descuento"), CapitalSocial("Capital Social"), Redondeo("Redondeo"), Cupones("Cupones");
    
    String nombre;
    private TiposDescuento(String _nombre){
        nombre = _nombre;
    }
    
    @Override
    public String toString(){
        return nombre;
    }
    
    public static String getNombre(int ordinal){
        if( TiposDescuento.CapitalSocial.ordinal() == ordinal ) 
            return TiposDescuento.CapitalSocial.toString();
        else if( IdentificadorDescuentos.esDescuentoCupones(ordinal) ) 
            return TiposDescuento.Cupones.toString();
        else if( TiposDescuento.Redondeo.ordinal() == ordinal ) 
            return TiposDescuento.Redondeo.toString();
        else 
            return TiposDescuento.SinDescuento.toString();
    }
}
