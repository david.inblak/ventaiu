/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

/**
 *
 * @author fvega
 */
public class PeticionCancelacionVentaBean {

    private long usuarioCancela;
    private String fecha;
    private long usuarioAutoriza;
    private long numeroTransaccion;
    private int lineasCanceladas;
    private int ventasCanceladas;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getLineasCanceladas() {
        return lineasCanceladas;
    }

    public void setLineasCanceladas(int lineasCanceladas) {
        this.lineasCanceladas = lineasCanceladas;
    }

    public long getNumeroTransaccion() {
        return numeroTransaccion;
    }

    public void setNumeroTransaccion(long numeroTransaccion) {
        this.numeroTransaccion = numeroTransaccion;
    }

    public long getUsuarioAutoriza() {
        return usuarioAutoriza;
    }

    public void setUsuarioAutoriza(long usuarioAutoriza) {
        this.usuarioAutoriza = usuarioAutoriza;
    }

    public Long getUsuarioCancela() {
        return usuarioCancela;
    }

    public void setUsuarioCancela(long usuarioCancela) {
        this.usuarioCancela = usuarioCancela;
    }

    public int getVentasCanceladas() {
        return ventasCanceladas;
    }

    public void setVentasCanceladas(int ventasCanceladas) {
        this.ventasCanceladas = ventasCanceladas;
    }

    @Override
    public String toString() {
        return "PeticionCancelacionVentaBean{" + "usuarioCancela=" + usuarioCancela + ", fecha=" + fecha + ", usuarioAutoriza=" + usuarioAutoriza + ", numeroTransaccion=" + numeroTransaccion + ", lineasCanceladas=" + lineasCanceladas + ", ventasCanceladas=" + ventasCanceladas + '}';
    }
}
