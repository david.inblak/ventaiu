/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

/**
 *
 * @author fvega
 */
public class RespuestaValidacionTarjetaBean {

    private int codigoError;
    private String descError;

    public int getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    @Override
    public String toString() {
        return "RespuestaValidacionTarjetaBean{" + "codigoError=" + codigoError + ", descError=" + descError + '}';
    }
}
