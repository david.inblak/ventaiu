/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.vista;

import com.sion.tienda.genericos.ventanas.Ventana;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;

/**
 *
 * @author dramirezr
 */
public class VentanaPromociones implements Ventana{
    //public static SimpleBooleanProperty disponible = new SimpleBooleanProperty(false);
            
    Group root;  
    
    public VentanaPromociones(){
        root=new Group();
        root.getStyleClass().add("Root");
    }
    
    @Override
    public Parent obtenerEscena(Object... os) {
        
        root.getChildren().clear();
        root.getChildren().addAll((VBox)os[0]); 
        return root;
        
    }
    
    @Override
    public void limpiar() {
        //disponible.setValue(Boolean.FALSE);
    }

    @Override
    public String[] getUserAgentStylesheet() {
        return new String[]{"/neto/sion/tienda/venta/imagenes/inicio/Venta.css", "/neto/sion/tienda/venta/promociones/redondeo/vista/estilosPromociones.css"};
    }
    
}
