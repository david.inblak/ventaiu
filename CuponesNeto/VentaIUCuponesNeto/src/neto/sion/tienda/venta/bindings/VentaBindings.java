/* 
 * To change this template, choose Tools | Templates and open the template in 
 * the editor. 
 */
package neto.sion.tienda.venta.bindings;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;
import neto.sion.tienda.autorizador.controlador.ControladorValidacion;
import neto.sion.tienda.autorizador.controlador.RespuestaValidacion;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.caja.principal.VerificaVentana;
import neto.sion.tienda.caja.util.UtileriasLog;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.exception.SionException;
import neto.sion.tienda.genericos.util.UtileriasImpresora;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.genericos.utilidades.Utilerias;
import neto.sion.tienda.pt.iso8583.descuentos.dto.RespuestaValidaDescuentoArticulosDto;
import neto.sion.tienda.transacciones.pdstad.vista.TransaccionesPDSTADVista;
import neto.sion.tienda.venta.constantes.TiposImpresion;
import neto.sion.tienda.venta.modelo.*;
import neto.sion.tienda.venta.promociones.cupones.modelo.ArticuloCupon;
import neto.sion.tienda.venta.promociones.cupones.modelo.CuponBean;
import neto.sion.tienda.venta.promociones.cupones.modelo.DatosCuponesVentaDto;
import neto.sion.tienda.venta.promociones.cupones.mvp.IComandoCupon;
import neto.sion.tienda.venta.promociones.cupones.mvp.IConfiguracionCupones;
import neto.sion.tienda.venta.promociones.cupones.mvp.IPresentadorCupones;
import neto.sion.tienda.venta.promociones.cupones.mvp.concreto.ConfiguracionCuponesSION;
import neto.sion.tienda.venta.promociones.cupones.mvp.concreto.IdentificadorDescuentos;
import neto.sion.tienda.venta.promociones.cupones.mvp.concreto.PresentadorCuponesMarketec;
import neto.sion.tienda.venta.promociones.cupones.mvp.concreto.VistaCuponesMarketec;
import neto.sion.tienda.venta.promociones.mvp.IComando;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesPresentador;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesRepositorio;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesVista;
import neto.sion.tienda.venta.promociones.redondeo.PresentadorPromociones;
import neto.sion.tienda.venta.promociones.redondeo.RepositorioRedondeo;
import neto.sion.tienda.venta.promociones.redondeo.VistaPromociones;
import neto.sion.tienda.venta.promociones.redondeo.modelo.ModeloArticulo;
import neto.sion.tienda.venta.recargas.tad.binding.RecargasTADBinding;
import neto.sion.tienda.venta.recargas.tad.dao.FolioTiempoAireDao;
import neto.sion.tienda.venta.recargas.tad.vista.RecargasTADVista;
import neto.sion.tienda.venta.vista.CobroTarjetaVista;
import neto.sion.tienda.venta.vista.VentaVista;
import neto.sion.tienda.venta.vista.VerificadorVista;
import neto.sion.tienda.venta.vistamodelo.VentaVistaModelo;
//import javafx.beans.ventaBinding. 

/**
 *
 * @author fvega
 */
public class VentaBindings {

    public static final Locale local = new Locale("es", "MX");
    DecimalFormat decimalFormat = new DecimalFormat("#0.00");

    /*
     * public static void main(String[] args) { URL location =
     * VerificaVentana.class.getProtectionDomain().getCodeSource().getLocation();
     * System.out.println(location.getPath());
    }
     */
    private IConfiguracionCupones configuracionCupones;
    private IPromocionesRepositorio repositorioRedondeo;
    private IPromocionesVista vistaRedondeo;
    private IPromocionesPresentador presentadorRedondeo;
    private VistaCuponesMarketec vistaGralCupones;
    private IPresentadorCupones presentadorCupones;
    private neto.sion.tienda.controles.vista.Utilerias utileriasVenta;
    private VentaVista vista;
    private VentaVistaModelo vistamodelo;
    private VerificadorVista verificadorVista;
    private RecargasTADVista recargasTADVista;
    private CobroTarjetaVista cobroTarjetaVista;
    private FolioTiempoAireDao folioTiempoAireDao;
    private VerificaVentana verificaVentana;
    public SimpleBooleanProperty cuponesActivos = new SimpleBooleanProperty();

    private double montoAcarreado;
    private String cadena;
    static int idCambioBuscadorTabla;
    private double montoEfectivo;
    private double montoVales;
    private double montoVenta;
    private double montoRestante;
    private double montoMaximoVenta;
    private final int PANTALLA_CAPTURA;
    private final int PANTALLA_VENTA;
   
    private final int ACTUALIZACION_LOCAL;

    private final int TARJETA_CREDITO_DEBITO;
    private final int VALE_ELECTRONICO;
    public static boolean esVentaGral;
    public static boolean esVentaTA;
    public static boolean esVentaPagaTdo;
    public boolean esConfirmacionVentaTA;
    static boolean esVentaExitosa;
    public static boolean esVentaFueraLineaExitosa;
    static boolean esEscapeConsumido;
    static boolean hayComunicacion;
    static boolean existeParametroVentaFueraLinea;
    static boolean esMarcadoFueraLineaDirecto;
    private static boolean esMontoRecibidoValido;
    private int idPantalla;
    private int idEscape;
    private int tipoTicket;
    private final NumberFormat formateador;
    //public static Boolean hayVentaEfectivoEnProceso = false; 
    public static Boolean hayVentaFLEnProceso = false;
    //Bandera para manejar ventanas de mensaje de error 
    public static Boolean hayVentanaError = false;
    private EventHandler<KeyEvent> eventosTeclasVenta;
    private EventHandler<KeyEvent> eventosCapturaVenta;
    private EventHandler<KeyEvent> eventoEscapeVenta;
    private Image aceptarBloqueos;
    private ImageView vistaImagenAceptarBloqueos;
    private Button botonBloqueos;
    private Image aceptarTraspasos;
    private ImageView vistaImagenAceptarTraspasos;
    private Button botonTraspasos;
    private final int ACCION_VENTA = 1;
    private final int ACCION_CANCELACION = 0;
    private final int ACCION_VENTA_FL = 2;
    private final int AUTORIZACION_ELIMINACION_LINEA = 1;
    private final int AUTORIZACION_CANCELACION_VENTA = 0;
    private int intentosVerificacionVentaTA;
    ///////////////keys 
    private final KeyCombination f2 = new KeyCodeCombination(KeyCode.F2);
    private final KeyCombination f3 = new KeyCodeCombination(KeyCode.F3);
    private final KeyCombination f4 = new KeyCodeCombination(KeyCode.F4);
    private final KeyCombination f5 = new KeyCodeCombination(KeyCode.F5);
    private final KeyCombination f6 = new KeyCodeCombination(KeyCode.F6);
    private final KeyCombination f7 = new KeyCodeCombination(KeyCode.F7);
    private final KeyCombination f8 = new KeyCodeCombination(KeyCode.F8);
    private final KeyCombination f9 = new KeyCodeCombination(KeyCode.F9);
    private final KeyCombination f10 = new KeyCodeCombination(KeyCode.F10);
    private final KeyCombination enter = new KeyCodeCombination(KeyCode.ENTER);
    private final KeyCombination escape = new KeyCodeCombination(KeyCode.ESCAPE);
    private final KeyCombination supr = new KeyCodeCombination(KeyCode.DELETE);
    private final KeyCombination tab = new KeyCodeCombination(KeyCode.TAB);
    private final KeyCombination shifTab = new KeyCodeCombination(KeyCode.TAB, KeyCodeCombination.SHIFT_DOWN);
    private final KeyCombination mas = new KeyCodeCombination(KeyCode.EQUALS);
    private final KeyCombination add = new KeyCodeCombination(KeyCode.ADD);
    private final KeyCombination sustraer = new KeyCodeCombination(KeyCode.SUBTRACT);
    private final KeyCombination menos = new KeyCodeCombination(KeyCode.MINUS);
    private final KeyCodeCombination del = new KeyCodeCombination(KeyCode.DELETE);
    /////variabkles para calculo del monto 
    private double importeVales;
    private int cantidadVales;
    private double montoRecibidoVentaRapida;
    private double montoRecibido;
    private double efectivo;
    private double vales;
    private int idImporteTarjetas;
    private double importeAplicado;
    private double importePorAplicar;
    ////buscador 
    private Pattern patronBuscadorSinPunto;
    private Pattern patronBuscadorConPunto;
    private Matcher matcherBuscador;
    private ArticuloBean articuloLocal;
    private ArticuloBean articuloTempGranel;
    ///marcacion de la venta 
    private int errorVenta;
    private String[] mensajeError;
    ////impresiones 
    //private int tipoTicket; 
    private long conciliacion;
    //////////// 
    ////manejo de la acdena del articulo 
    private Pattern patronCadena;
    private Matcher matcherCadena;
    ////actualizacion de celdas 
    private String cadenaDouble;
    private int indicePunto;
    private String cadenaPuntoAlaDerecha;
    private int indexTablaArticulos;
    ///al validar un articulo escaneado 
    private Label etiquetaValidacion;
    private TextField campotextoValidacion;
    private double cantidadArticulos;
    private String cadenaValidacionArticulos;
    private double idgranel;
    private Pattern patronContienePunto;
    private Matcher matcherContienePunto;
    private int posicionSignoPor;
    private int i;
    private Pattern patronSinPunto;
    private Matcher matcherSinPunto;
    ////autorizador 
    private String autorizador;
    //bloqueos locales 
    private final int BLOQUEO_EFECTIVO = 1;
    private final int BLOQUEO_TARJETAS = 2;
    private final int BLOQUEO_VALES = 3;
    ///popup vales 
    private ChangeListener<Number> listenerPopupVales;
    private ChangeListener<Number> listenerPopupCupones;

    //metodos de entrada 
    private int metodoEntradaEscaner;// = SION.obtenerParametro(Modulo.VENTA, "metodo.entrada.escaner"); 
    private int metodoEntradaBuscador;
    private int metodoEntradaVerificador;
    private static final int TODAS_TARJETAS = 0;
    private TransaccionesPDSTADVista transaccionesPDSTADVista;
    private static final int CANTIDAD_MAXIMA_POR_ARTICULO = 999;

    public VentaBindings(VentaVista _ventaVista, neto.sion.tienda.controles.vista.Utilerias _utileriasVenta) {
        /////instancias de clases 

        try {
            this.vista = _ventaVista;
            this.utileriasVenta = _utileriasVenta;
            this.vistamodelo = new VentaVistaModelo(utileriasVenta);
            this.recargasTADVista = new RecargasTADVista(_ventaVista, vistamodelo);
            this.verificadorVista = new VerificadorVista(_ventaVista, vistamodelo, this);
            this.folioTiempoAireDao = new FolioTiempoAireDao();
            this.verificaVentana = new VerificaVentana();
            this.cobroTarjetaVista = new CobroTarjetaVista(this, vistamodelo, vista, _utileriasVenta);
            this.transaccionesPDSTADVista = new TransaccionesPDSTADVista();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            SION.log(Modulo.VENTA, "Ocurrió un error en constructor: " + exceptionAsString, Level.SEVERE);
        }


        this.montoRestante = 0;
        this.PANTALLA_CAPTURA = 0;
        this.PANTALLA_VENTA = 1;
        this.TARJETA_CREDITO_DEBITO = 1;
        this.VALE_ELECTRONICO = 0;
        this.idPantalla = PANTALLA_CAPTURA;
        this.idEscape = 0;
        this.tipoTicket = 0;
        this.ACTUALIZACION_LOCAL = 0;
        this.intentosVerificacionVentaTA = 0;

        this.formateador = new DecimalFormat("#.00");
        this.aceptarTraspasos = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/btn_Aceptar.png"));
        this.vistaImagenAceptarTraspasos = new ImageView(aceptarTraspasos);
        this.botonTraspasos = new Button("", vistaImagenAceptarTraspasos);
        this.aceptarBloqueos = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/btn_Aceptar.png"));
        this.vistaImagenAceptarBloqueos = new ImageView(aceptarBloqueos);
        this.botonBloqueos = new Button("", vistaImagenAceptarBloqueos);
        //this.botonBloqueos = new Button("Aceptar"); 

        hayComunicacion = true;
        esVentaFueraLineaExitosa = false;
        VentaBindings.existeParametroVentaFueraLinea = false;
        //this.ACTUALIZACION_CENTRAL = 1;
        VentaBindings.esMarcadoFueraLineaDirecto = false;
        VentaBindings.esMontoRecibidoValido = false;

        ///variables para calculo de monto 
        
        this.importeVales = 0;
        this.cantidadVales = 0;
        this.montoRecibidoVentaRapida = 0.0;
        this.montoRecibido = 0;
        this.efectivo = 0;
        this.vales = 0;
        this.cantidadVales = 0;
        this.montoVales = 0;
        this.idImporteTarjetas = 0;
        this.importeAplicado = 0;
        this.importePorAplicar = 0;
        ///buscador 
        this.patronBuscadorSinPunto = Pattern.compile("^[0-9].*");
        this.patronBuscadorConPunto = Pattern.compile("^[0-9]*.(.).[0-9]*.*$");

        this.matcherBuscador = null;
        this.articuloLocal = null;
        this.articuloTempGranel = null;
        ///marcacion de la venta 
        this.errorVenta = 0;
        this.mensajeError = null;
        ///tickets 
        this.tipoTicket = 0;
        this.conciliacion = 0;
        ////navegacion 
        this.montoVenta = 0;
        this.montoRecibido = 0;

        /////manejo de la cadena para articulo 
        this.patronCadena = Pattern.compile("^[0-9.*]");
        this.matcherCadena = null;
        //actualizacion de las celdas 
        this.cadenaDouble = "";
        this.indicePunto = 0;
        this.cadenaPuntoAlaDerecha = "";
        this.indexTablaArticulos = 0;
        ///al validar articulo escaneado 
        this.etiquetaValidacion = new Label();
        this.campotextoValidacion = new TextField();
        this.cantidadArticulos = 0;
        this.cadenaValidacionArticulos = "";
        this.idgranel = 0;
        this.patronContienePunto = Pattern.compile("^[0-9]*.(.).[0-9]*.*.[0-9]$");
        this.matcherContienePunto = null;
        this.posicionSignoPor = 0;
        this.i = 0;
        this.patronSinPunto = Pattern.compile("^[0-9].*.[0-9]$");
        this.matcherSinPunto = null;
        ////autorizador 
        this.autorizador = "";
        /// 
        this.listenerPopupVales = null;
        this.listenerPopupCupones = null;
        // 


        try {
            this.metodoEntradaEscaner = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "metodo.entrada.escaner").trim());
            this.metodoEntradaBuscador = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "metodo.entrada.buscador").trim());
            this.metodoEntradaVerificador = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "metodo.entrada.verificador").trim());
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, "Error al consultar metodos de entrada en archivo de propiedades, se establecen valores x default.");
            this.metodoEntradaEscaner = 1;
            this.metodoEntradaBuscador = 2;
            this.metodoEntradaVerificador = 3;
        }

        repositorioRedondeo = new RepositorioRedondeo();
        configuracionCupones = new ConfiguracionCuponesSION();

        vistaRedondeo = new VistaPromociones(repositorioRedondeo);
    }
///////////////////////////////////// 
    boolean forzadoVentaFL = false;

    public boolean trabajarVentaFueraLinea() {
        return forzadoVentaFL || existeParametroVentaFueraLinea;
    }

    public int getMetodoEntradaVerificador() {
        return metodoEntradaVerificador;
    }

    public boolean revisarStageVerificador() {
        return verificadorVista.getStatusStage();
    }

    public void cerrarStageVerificador() {
        verificadorVista.cerrarStage();
    }

    public boolean revisarStageTA() {
        return recargasTADVista.getStatusStage();
    }

    public void cerrarStageTA() {
        recargasTADVista.cerrarStage();
    }

    ///inicio 
    public void inicializarVariables(final BorderPane panel, final Label etiquetaMontoVenta, final TableView tabla,
            final TextField campoMontoRestante, final TextField campoImporteRecibido) {

        SION.log(Modulo.VENTA, "Cargando Variables de inicio Ventas", Level.INFO);
        esVentaPagaTdo = false;
        llamarVentanaTraspasos(botonTraspasos);
        llamarBloqueos(botonBloqueos);

        botonBloqueos.setMaxSize(vistaImagenAceptarBloqueos.getFitWidth(), vistaImagenAceptarBloqueos.getFitHeight());
        botonBloqueos.setMinSize(vistaImagenAceptarBloqueos.getFitWidth(), vistaImagenAceptarBloqueos.getFitHeight());
        botonBloqueos.setPrefSize(vistaImagenAceptarBloqueos.getFitWidth(), vistaImagenAceptarBloqueos.getFitHeight());

        botonTraspasos.setMaxSize(vistaImagenAceptarTraspasos.getFitWidth(), vistaImagenAceptarTraspasos.getFitHeight());
        botonTraspasos.setMinSize(vistaImagenAceptarTraspasos.getFitWidth(), vistaImagenAceptarTraspasos.getFitHeight());
        botonTraspasos.setPrefSize(vistaImagenAceptarTraspasos.getFitWidth(), vistaImagenAceptarTraspasos.getFitHeight());

        ///validar primero el bloqueo en sesion 
        //SION.log(Modulo.VENTA, "Codigo de error de bloqueos en sesion: " + VistaBarraUsuario.getSesion().getBloqueosVenta().getCdgError(), Level.INFO); 
        //if (VistaBarraUsuario.getSesion().getBloqueosVenta().getCdgError() != 0) { 
        if (neto.sion.tienda.genericos.utilidades.Utilerias.evaluarConexionCentral(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice")), 3)) {
            SION.log(Modulo.VENTA, "Se detectó comunicación con servidor, se procede a evaluar si existen bloqueos", Level.INFO);
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    evaluarBloqueosVenta();

                }
            });
        } else {
            SION.log(Modulo.VENTA, "No se detectó comunicación, se procederá a evaluar bloqueos en bd local", Level.INFO);
            validarBloqueosLocal(0);
        }
        //} else { 
        //SION.log(Modulo.VENTA, "no se encontraron datos de bloqueo en sesion, se continua con la venta", Level.INFO); 
        //} 

        agregarEventosCaptura(panel);
        cadena = "";
        montoVenta = 0;
        montoRestante = 0;
        idPantalla = PANTALLA_CAPTURA;

        if (vistamodelo.getTamanioListaArticulos() > 0) {
            for (ArticuloBean datosArticulosVentaDevBean : vistamodelo.getListaArticulos()) {
                montoVenta += datosArticulosVentaDevBean.getImporte();
            }
            etiquetaMontoVenta.textProperty().set(utileriasVenta.getNumeroFormateado(montoVenta));
        } else {
            etiquetaMontoVenta.textProperty().set("0.00");
        }

        if (Double.parseDouble(etiquetaMontoVenta.textProperty().get()) > vistamodelo.getImporteVenta()) {
            montoRestante = Double.parseDouble(etiquetaMontoVenta.textProperty().get()) - vistamodelo.getImporteVenta();
            campoMontoRestante.textProperty().set(utileriasVenta.getNumeroFormateado(montoRestante));
        } else {
            campoMontoRestante.textProperty().set("0.00");
        }

        if (montoAcarreado <= 0) {
            campoImporteRecibido.textProperty().set("");
        } else {
            campoImporteRecibido.textProperty().set(utileriasVenta.getNumeroFormateado(montoAcarreado));
        }

        SION.log(Modulo.VENTA, "Consultando parámetro de monto máximo de venta", Level.INFO);

        ///se consulta parametro de insercion de venta del dia 
        //RespuestaParametroBean parametroMontoMaximoVenta = new RespuestaParametroBean(); 
        //parametroMontoMaximoVenta = utileriasVenta.consultaParametro(UtileriasVenta.Accion.MONTO_MAXIMO_VENTA, Modulo.VENTA, Modulo.VENTA); 

        vistamodelo.llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(), 1, 2, 1, 9);
        if (vistamodelo.consultarParametro() != null) {
            if (vistamodelo.getCodigoErrorParametroOperacion() == 0) {
                SION.log(Modulo.VENTA, "Parámetro monto maximo de venta activado. ", Level.INFO);
                montoMaximoVenta = vistamodelo.getValorConfiguracionParametroOperacion();

            } else {
                SION.log(Modulo.VENTA, "Store de consulta de parametros regresa código erróneo", Level.INFO);
                montoMaximoVenta = 999999;
            }
        } else {
            SION.log(Modulo.VENTA, "Instancia de RespuestaParametroBean nula", Level.SEVERE);
            montoMaximoVenta = 999999;
        }

        if (montoMaximoVenta <= 0) {
            SION.log(Modulo.VENTA, "El monto recibido para máximo de venta es igual a cero, se asigna valor por default", Level.FINEST);
            montoMaximoVenta = 999999;
        }

        SION.log(Modulo.VENTA, "Monto máximo de venta que se usará: " + montoMaximoVenta, Level.FINEST);


        tabla.requestFocus();
    }

    ///actualizaciones 
    public void refrescarTablaArticulos() {

        vistamodelo.getListaArticulos().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable arg0) {

                if (vistamodelo.getListaArticulos().size() > 0) {

                    vista.getTablaVentas().setItems(vistamodelo.getListaArticulosTA());
                    vista.getTablaVentas().setItems(vistamodelo.getListaArticulos());

                    if (vistamodelo.getListaArticulos().size() == 1) {

                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.removerESCEventHandler();
                                VistaBarraUsuario.setLogoHabilitado(false);
                                agregarEventosEscape(vista.getPanelVentas());
                                SION.log(Modulo.VENTA, "Existe venta activa -> true", Level.INFO);
                                AdministraVentanas.setExisteVentaActiva(true);
                            }
                        });


                    }
                    if ((vistamodelo.getListaArticulos().size() > 1)) {

                        vista.posicionarScrollTabla(vistamodelo.getTamanioListaArticulos());
                    }
                }
                if (vistamodelo.getListaArticulos().size() == 0 && vistamodelo.getListaArticulosTA().size() == 0) {

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            removerEventosEscape(vista.getPanelVentas());
                            VistaBarraUsuario.setLogoHabilitado(true);
                            AdministraVentanas.agregarESCEventHandler();
                            SION.log(Modulo.VENTA, "Existe venta activa -> false", Level.INFO);
                            AdministraVentanas.setExisteVentaActiva(false);
                        }
                    });

                }
            }
        });

        vistamodelo.getListaArticulosTA().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable arg0) {


                if (vistamodelo.getListaArticulosTA().size() > 0) {

                    if (vistamodelo.getListaArticulosTA().size() == 1) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.removerESCEventHandler();
                                VistaBarraUsuario.setLogoHabilitado(false);
                                agregarEventosEscape(vista.getPanelVentas());
                                SION.log(Modulo.VENTA, "Existe venta activa -> true", Level.INFO);
                                AdministraVentanas.setExisteVentaActiva(true);
                            }
                        });

                    }


                }
                if (vistamodelo.getListaArticulosTA().size() == 0 && vistamodelo.getListaArticulos().size() == 0) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            removerEventosEscape(vista.getPanelVentas());
                            VistaBarraUsuario.setLogoHabilitado(true);
                            AdministraVentanas.agregarESCEventHandler();
                            SION.log(Modulo.VENTA, "Existe venta activa -> false", Level.INFO);
                            AdministraVentanas.setExisteVentaActiva(false);
                        }
                    });

                }
            }
        });
    }

    public void actualizarNavegadorTarjetas() {

        vistamodelo.getListaPagosTarjeta().addListener(new ListChangeListener<OperacionTarjetaBean>() {

            @Override
            public void onChanged(Change<? extends OperacionTarjetaBean> arg0) {

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        if (vistamodelo.getListaPagosTarjeta().size() == 1) {

                            try {
                                vista.agregarComponentesnavegacionTarjetas();
                            } catch (Exception e) {
                                SION.logearExcepcion(Modulo.VENTA, e, "");
                            }

                        } else if (vistamodelo.getListaPagosTarjeta().size() == 0 && vista.getTamanioComponenteNavegacionTarjetas() > 4) {

                            vista.removerComponentesNavegaciontarjetas();

                        } else if (vistamodelo.getListaPagosTarjeta().size() > 0) {
                        }
                    }
                });


            }
        });

    }

    public void refrescarMontoVenta() {
        Platform.runLater(new Runnable() {

            public void run() {
                SION.log(Modulo.VENTA, "Inciciando refrescar monto de la venta...", Level.INFO);
                vista.getTablaVentas().setItems(vistamodelo.getListaArticulosTA());
                vista.getTablaVentas().setItems(vistamodelo.getListaArticulos());

                vista.getEtiquetaMontoVenta().textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));
                SION.log(Modulo.VENTA, "Monto total de la venta: " + vista.getEtiquetaMontoVenta().textProperty().getValue(), Level.INFO);
            }
        });

    }

    public void actualizarMontoVenta(final Label etiquetaMontoVenta, final TableView tabla) {

        vistamodelo.getListaArticulos().addListener(new ListChangeListener<ArticuloBean>() {

            @Override
            public void onChanged(Change<? extends ArticuloBean> arg0) {


                vista.getTablaVentas().setItems(vistamodelo.getListaArticulosTA());
                vista.getTablaVentas().setItems(vistamodelo.getListaArticulos());

                if (idPantalla == PANTALLA_CAPTURA) {

                    if (vistamodelo.getImporteVenta() > 0) {
                        etiquetaMontoVenta.textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));
                    } else {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                etiquetaMontoVenta.textProperty().set("0.00");
                            }
                        });

                    }

                    if (vistamodelo.getTamanioListaArticulos() > 0) {
                        tabla.getSelectionModel().clearSelection();
                        tabla.getSelectionModel().selectLast();
                    } else {
                        //VentaVista.tabla.requestFocus(); 
                        vista.setFocusTablaVentas();
                    }
                }

            }
        });

    }

    public void actualizaMontoRestante(final Label etiquetaMontoVenta, final TextField campoMontoRestante,
            final TextField campoImporteRecibido) {

        etiquetaMontoVenta.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {

                if (!etiquetaMontoVenta.textProperty().get().isEmpty()) {
                    if (!campoImporteRecibido.textProperty().get().isEmpty()) {
                        montoRestante = vistamodelo.getImporteVenta() - Double.parseDouble(campoImporteRecibido.textProperty().get());
                    } else {
                        montoRestante = vistamodelo.getImporteVenta();
                    }

                    if (montoRestante > 0) {

                        if (!campoImporteRecibido.textProperty().getValue().isEmpty()) {
                            campoMontoRestante.textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta() - Double.parseDouble(campoImporteRecibido.textProperty().getValue())));

                        } else {
                            campoMontoRestante.textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));

                        }
                    } else {

                        campoMontoRestante.textProperty().set("0.00");
                    }
                } else {

                    campoMontoRestante.textProperty().set("0.00");
                }
            }
        });

    }

    public void actualizaMontoRecibido(final TextField campoEfectivo, final TextField campoVales, final TextField campoValesE,
            final TextField campoTarjeta, final TextField campoCambio, final TextField campoMontoRecibido,
            final TextField campoCantidadVales, final Label etiquetaMontoVenta, final TextField campoMontoRestante) {

        try {
            ChangeListener cl = new ChangeListener() {

                @Override
                public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                    //SION.log(Modulo.VENTA, "Actualizando montos", Level.INFO); 
                    calcularMontoRecibido();

                }
            };

            campoCantidadVales.textProperty().addListener(cl);
            campoEfectivo.textProperty().addListener(cl);
            campoVales.textProperty().addListener(cl);
            campoValesE.textProperty().addListener(cl);
            campoTarjeta.textProperty().addListener(cl);

        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "");
        }

    }

    public void calcularMontoRecibido() {

        //SION.log(Modulo.VENTA, "Calculando monto recibido de venta", Level.INFO); 

        double suma = 0.0;
        double cambio = 0.0;
        double restante = 0.0;

        double importeVales = 0.0;
        int cantidadVales = 0;
        double montoVenta = 0;
        double montoRecibido = 0;

        if ("".equals(vista.getMontoVales())) {
            importeVales = 0.0;
        } else {
            try {
                importeVales = Double.parseDouble(vista.getMontoVales());
            } catch (NumberFormatException e) {
                SION.logearExcepcion(Modulo.VENTA, e, "");
                importeVales = 0.0;
            }

        }

        try {
            montoVenta = Double.parseDouble(vista.getMontoVenta());
        } catch (NumberFormatException e) {
            montoVenta = 0;
            SION.logearExcepcion(Modulo.VENTA, e, "");
        }

        montoRecibido = getImporteRecibido();

        if (montoRecibido > montoVenta) {
            cambio = montoRecibido - montoVenta;

            if (importeVales + vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS) >= montoVenta) {
                if (!vista.getMontoEfectivo().isEmpty()) {
                    try {
                        cambio = Double.parseDouble(vista.getMontoEfectivo());
                    } catch (NumberFormatException nfe) {
                        cambio = 0;
                        SION.logearExcepcion(Modulo.VENTA, nfe, "");
                    }
                } else {
                    cambio = 0.0;
                }
            } else {
                cambio = montoRecibido - montoVenta;
            }
        } else {
            restante = montoVenta - montoRecibido;
        }

        if (montoRecibido > 0.0) {
            vista.setMontoRecibidoPantallaCaptura(utileriasVenta.getNumeroFormateado(montoRecibido));
            //vista.setMontoRecibidoPantallaCaptura(utilerias.getNumeroFormateado(montoRecibido)); 
        } else {
            vista.setMontoRecibidoPantallaCaptura("");
        }

        if (cambio > 0.0) {
            //vista.setMontoCambio(utilerias.getNumeroFormateado(cambio)); 
            vista.setMontoCambio(utileriasVenta.getNumeroFormateado(cambio));
        } else {
            vista.setMontoCambio("0.00");
        }
        if (restante > 0.0) {
            vista.setMontoRestante(utileriasVenta.getNumeroFormateado(restante));
            //vista.setMontoRestante(utilerias.getNumeroFormateado(restante)); 
        } else {
            vista.setMontoRestante("0.00");
        }

        //SION.log(Modulo.VENTA, "Monto calculado y componentes de vista actualizados", Level.INFO); 
    }

    public void actualizaImporteRecibidoPantallaRapida(final TableView tabla, final Label etiquetaMontoVenta) {
        tabla.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (enter.match(arg0) || supr.match(arg0)) {

                    try {
                        if (vistamodelo.getImporteVenta() > 0) {
                            etiquetaMontoVenta.textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));

                        } else {

                            etiquetaMontoVenta.textProperty().set("0.00");
                        }
                    } catch (NumberFormatException e) {
                        SION.logearExcepcion(Modulo.VENTA, e);
                        etiquetaMontoVenta.textProperty().set("0.00");
                    }
                }
            }
        });

    }

    /// tarjetas 
    public void actualizarEtiquetasNavegadorTarjetas(final int idTarjeta) {

        if (vistamodelo.getListaPagosTarjeta().size() > 0) {
            if (idTarjeta == TARJETA_CREDITO_DEBITO) {
                if (!"".equals(vista.getValorEtiquetaCantidadtarjetasRecibidas())) {//valida k etiqueta no este vacia 
                    if (Double.parseDouble(vista.getValorEtiquetaCantidadtarjetasRecibidas()) > 0) {
                        vista.actualizarEtiquetasNavegadorTarjetas(
                                vistamodelo.getRegistroTarjeta(TARJETA_CREDITO_DEBITO, vista.getDescripcionAutorizada(),
                                Integer.parseInt(vista.getValorEtiquetaCantidadtarjetasRecibidas()) + Integer.parseInt(vista.getValorEtiquetaCantidadValesERecibidos())).getDesctarjeta(),
                                vistamodelo.getRegistroTarjeta(TARJETA_CREDITO_DEBITO, vista.getDescripcionAutorizada(),
                                Integer.parseInt(vista.getValorEtiquetaCantidadtarjetasRecibidas()) + Integer.parseInt(vista.getValorEtiquetaCantidadValesERecibidos())).getNumerotarjeta(),
                                String.valueOf(utileriasVenta.getNumeroFormateado(vistamodelo.getRegistroTarjeta(TARJETA_CREDITO_DEBITO, vista.getDescripcionAutorizada(),
                                Integer.parseInt(vista.getValorEtiquetaCantidadtarjetasRecibidas()) + Integer.parseInt(vista.getValorEtiquetaCantidadValesERecibidos())).getMonto())),
                                vistamodelo.getRegistroTarjeta(TARJETA_CREDITO_DEBITO, vista.getDescripcionAutorizada(),
                                Integer.parseInt(vista.getValorEtiquetaCantidadtarjetasRecibidas()) + Integer.parseInt(vista.getValorEtiquetaCantidadValesERecibidos())).getAutorizacion());
                    } else {
                        vista.actualizarEtiquetasNavegadorTarjetas("", "", "", "");
                    }
                }
            } else if (idTarjeta == VALE_ELECTRONICO) {
                if (!"".equals(vista.getValorEtiquetaCantidadValesERecibidos())) {
                    if (Double.parseDouble(vista.getValorEtiquetaCantidadValesERecibidos()) > 0) {

                        vista.actualizarEtiquetasNavegadorTarjetas(
                                vistamodelo.getRegistroTarjeta(VALE_ELECTRONICO, vista.getDescripcionAutorizada(),
                                Integer.parseInt(vista.getValorEtiquetaCantidadtarjetasRecibidas()) + Integer.parseInt(vista.getValorEtiquetaCantidadValesERecibidos())).getDesctarjeta(),
                                vistamodelo.getRegistroTarjeta(VALE_ELECTRONICO, vista.getDescripcionAutorizada(),
                                Integer.parseInt(vista.getValorEtiquetaCantidadtarjetasRecibidas()) + Integer.parseInt(vista.getValorEtiquetaCantidadValesERecibidos())).getNumerotarjeta(),
                                String.valueOf(vistamodelo.getRegistroTarjeta(VALE_ELECTRONICO, vista.getDescripcionAutorizada(),
                                Integer.parseInt(vista.getValorEtiquetaCantidadtarjetasRecibidas()) + Integer.parseInt(vista.getValorEtiquetaCantidadValesERecibidos())).getMonto()),
                                vistamodelo.getRegistroTarjeta(VALE_ELECTRONICO, vista.getDescripcionAutorizada(),
                                Integer.parseInt(vista.getValorEtiquetaCantidadtarjetasRecibidas()) + Integer.parseInt(vista.getValorEtiquetaCantidadValesERecibidos())).getAutorizacion());
                    } else {
                        vista.actualizarEtiquetasNavegadorTarjetas("", "", "", "");
                    }
                }
            }
        }
    }

    ///getters 
    public String getCadenaMontoRestante() {

        double monto = 0.0;
        if (esVentaTA) {
            monto = vistamodelo.getImporteVentaTA() - getImporteRecibido();
        } else {
            monto = vistamodelo.getImporteVenta() - getImporteRecibido();
        }
        //return utilerias.getNumeroFormateado(monto); 
        return utileriasVenta.getNumeroFormateado(monto);
    }

    public double getImporteRecibidoVentaRapida() {
        montoRecibidoVentaRapida = 0.0;

        try {
            montoRecibidoVentaRapida = Double.parseDouble(vista.getMontoRecibidoPantallCaptura());
        } catch (NumberFormatException e) {
            montoRecibidoVentaRapida = 0;
        }

        if (montoRecibidoVentaRapida > 0) {
            montoRecibidoVentaRapida = Double.parseDouble(utileriasVenta.getNumeroFormateado(montoRecibidoVentaRapida));

        }

        return montoRecibidoVentaRapida;
    }

    public double getImporteRecibido() {

        montoRecibido = 0;
        efectivo = 0;
        vales = 0;

        try {
            efectivo = Double.parseDouble(vista.getMontoEfectivo());
        } catch (Exception e) {
            efectivo = 0;
        }
        try {
            vales = Double.parseDouble(vista.getMontoVales());
        } catch (Exception e) {
            vales = 0;
        }

        montoRecibido = efectivo + vales + vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS);

        return montoRecibido;
    }

    public double getMontoMaximoPermitido() {
        return Double.parseDouble(utileriasVenta.getNumeroFormateado(montoMaximoVenta));
    }

    //eventos botones  
    public void eventosBotonOtrasFormasdePago(Group grupoOtrasFormasDePago, final TextField campoImporteRecibido,
            final Label etiquetaMontoVenta) {

        grupoOtrasFormasDePago.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {


                SION.log(Modulo.VENTA, "Boton otras formas de pago presionado", Level.FINEST);

                if (idPantalla == PANTALLA_CAPTURA) {
                    if (!campoImporteRecibido.textProperty().getValue().isEmpty()) {//foucs en el campo del importe 
                        intercambiarPantalla(campoImporteRecibido, etiquetaMontoVenta);
                    } else {
                        if (vistamodelo.getImporteVenta() > 0) {                            ////cambiarCajasComponentes(PANTALLA_VENTA, vista.getPanelVentas()); 
                            if (repositorioRedondeo.esPromocionesActivas()) {
                                SION.log(Modulo.VENTA, "Promociones ACTIVAS", Level.FINEST);
                                mostrarPopWindowRedondeo(campoImporteRecibido,
                                        getTaskvalidaImporte(campoImporteRecibido, etiquetaMontoVenta),
                                        getTaskvalidaImporte(campoImporteRecibido, etiquetaMontoVenta));
                            } else {
                                cambiarCajasComponentes(PANTALLA_VENTA, vista.getPanelVentas());
                            }
                        } else {
                            if (vistamodelo.getTamanioListaArticulos() > 0) {
                                mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.importeCero"), TipoMensaje.ADVERTENCIA, vista.getTablaVentas());
                            }
                        }
                    }
                }
            }
        });
    }

    public void eventosBotonCancelar(final Group grupoCancelar, final TextField campoImporteRecibido, final TextField campoCambio,
            final TextField campoMontoRestante, final TableView tabla) {

        grupoCancelar.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {

                SION.log(Modulo.VENTA, "Botón Cancelar presionado", Level.FINEST);

                if (idPantalla == PANTALLA_CAPTURA) {

                    if (vistamodelo.getTamanioListaArticulos() > 0 || esVentaTA) {
                        SION.log(Modulo.VENTA, "Botón Cancelar presionado", Level.FINEST);
                        consultarParametrosAutorizacion(ACCION_CANCELACION, 0);
                        arg0.consume();
                    }
                } else {
                    operacionesTeclaEscape();
                }

            }
        });

    }

    public void eventosBotonTA(final Button btnVentaTA) {

        btnVentaTA.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {

                SION.log(Modulo.VENTA, "Boton de TA presionado", Level.INFO);

                if (idPantalla == PANTALLA_CAPTURA && vistamodelo.getTamanioListaArticulos() == 0 && vistamodelo.getTamanioListaArticulosTA() == 0) {
                    //if (idPantalla == PANTALLA_CAPTURA && vistamodelo.getTamanioListaArticulos() == 0) { 
                    SION.log(Modulo.VENTA, "Abriendo stage de TA", Level.INFO);
                    try {
                        recargasTADVista.inicializarStageTA();
                    } catch (Exception e) {
                        SION.logearExcepcion(Modulo.VENTA, e, getStackTrace(e));
                    } catch (Error e) {
                        SION.logearExcepcion(Modulo.VENTA, new Exception(e.getCause()), getStackTrace(e));
                    }
                } else {
                    SION.log(Modulo.VENTA, "No se cumplen las condicionas para abrir el stage de TA", Level.WARNING);
                }
            }
        });

    }

    public void eventosBotonBuscaPagoTA(final Button btnBuscaPagoTA) {

        btnBuscaPagoTA.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {

                SION.log(Modulo.VENTA, "Boton busca pago TA presionado", Level.INFO);
                SION.log(Modulo.VENTA, "Abriendo stage de TA", Level.INFO);
                try {
                    transaccionesPDSTADVista.inicializarStagePDSTAD();
                } catch (Exception e) {
                    SION.logearExcepcion(Modulo.VENTA, e, getStackTrace(e));
                } catch (Error e) {
                    SION.logearExcepcion(Modulo.VENTA, new Exception(e.getCause()), getStackTrace(e));
                }
            }
        });

    }

    public void eventosBotonPagarOtrasFormasDePago(Group grupoPago, final TextField campoImporteRecibido, final Label etiquetaMontoVenta) {

        grupoPago.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {
                SION.log(Modulo.VENTA, "Boton pagar en pantalla de formas de pago presionado", Level.FINEST);
                if (idPantalla == PANTALLA_VENTA) {
                    if (!campoImporteRecibido.textProperty().getValue().trim().equals("") && vistamodelo.getTamanioListaArticulos() > 0) {
                        if (Double.parseDouble(campoImporteRecibido.textProperty().getValue()) >= Double.parseDouble(etiquetaMontoVenta.textProperty().getValue())) {

                            //parametros eliminacion de linea 
                            SION.log(Modulo.VENTA, "Botón otras formas de pago presionado", Level.FINEST);
                            consultarParametrosAutorizacion(ACCION_VENTA, 1);

                            arg0.consume();
                        }
                    }
                } else {
                    double importeRecibio = 0;
                    try {
                        importeRecibio = Double.parseDouble(campoImporteRecibido.textProperty().getValue());
                    } catch (NumberFormatException e) {
                        importeRecibio = 0;
                    }

                    if (esVentaTA && importeRecibio > 0) {
                        if (importeRecibio >= Double.parseDouble(etiquetaMontoVenta.textProperty().getValue())) {
                            consultarParametrosAutorizacion(ACCION_VENTA, 4);
                            arg0.consume();
                        }
                    }
                }
            }
        });

    }

    public void eventosBotonPagoEfectivo(Group grupoPagoEfectivo, final TextField campoImporteRecibido, final Label etiquetaMontoVenta) {

        grupoPagoEfectivo.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {
                SION.log(Modulo.VENTA, "Botón pagar en efectivo presionado", Level.FINEST);
                if (idPantalla == PANTALLA_CAPTURA) {



                    if (!campoImporteRecibido.textProperty().getValue().trim().equals("") && (vistamodelo.getTamanioListaArticulos() > 0 || vistamodelo.getTamanioListaArticulosTA() > 0)) {

                        if (Double.parseDouble(campoImporteRecibido.textProperty().get()) >= Double.parseDouble(etiquetaMontoVenta.textProperty().get())) {
                            ////consultarParametrosAutorizacion(ACCION_VENTA, 4); 
                            if (esVentaTA) {
                                SION.log(Modulo.VENTA, "Venta de Tiempo Aire", Level.FINEST);
                                consultarParametrosAutorizacion(ACCION_VENTA, 4);
                            } else if (repositorioRedondeo.esPromocionesActivas()) {
                                SION.log(Modulo.VENTA, "Promociones ACTIVAS", Level.FINEST);
                                mostrarPopWindowRedondeo(campoImporteRecibido,
                                        getTaskvalidaImporte(campoImporteRecibido, etiquetaMontoVenta),
                                        getTaskvalidaImporte(campoImporteRecibido, etiquetaMontoVenta));
                            } else {
                                SION.log(Modulo.VENTA, "Promociones INACTIVAS", Level.FINEST);
                                consultarParametrosAutorizacion(ACCION_VENTA, 4);
                            }

                            arg0.consume();
                        }
                    }
                }
            }
        });

    }

    ////validaciones 
    public void validaCampoVales(final TextField campo, final TextField campoMontoVales, final TextField campoCantidaVales) {

        cantidadVales = 0;
        montoVales = 0;

        campo.focusedProperty().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable arg0) {
                if (campo.isFocused()) {
                    if (campoCantidaVales.textProperty().getValue().isEmpty()) {
                        cantidadVales = 0;
                    } else {
                        try {
                            if (Integer.parseInt(campoCantidaVales.textProperty().getValue()) > 0) {
                                cantidadVales = Integer.parseInt(campoCantidaVales.textProperty().getValue());
                            } else {
                                cantidadVales = 0;
                            }
                        } catch (NumberFormatException e) {
                            cantidadVales = 0;
                        }

                    }

                    if (campoMontoVales.textProperty().getValue().isEmpty()) {
                        montoVales = 0;
                    } else {
                        try {
                            if (Double.parseDouble(campoMontoVales.textProperty().getValue()) > 0) {

                                montoVales = Double.parseDouble(campoCantidaVales.textProperty().getValue());
                            } else {

                                montoVales = 0;
                            }
                        } catch (NumberFormatException e) {

                            montoVales = 0;
                        }

                    }

                    if (cantidadVales == 0 || montoVales == 0) {
                        campoMontoVales.textProperty().setValue("");
                        campoCantidaVales.textProperty().setValue("");
                    }
                }
            }
        });

    }

    public void eventosCupones(final Group grupoBotonCupones) {
        vistamodelo.getListaArticulos().addListener(new ListChangeListener<ArticuloBean>() {

            @Override
            public void onChanged(Change<? extends ArticuloBean> change) {

                try {
                    boolean debe = (Boolean) configuracionCupones.debeIntentarDescuentoCupones();
                    if (debe) {
                        boolean huboCambios = false;
                        while (change.next()) {
                            huboCambios = change.wasAdded() || change.wasRemoved();
                        }

                        if (huboCambios) {
                            double importe = 0.0;
                            for (int j = 0; j < vistamodelo.getListaArticulos().size(); j++) {
                                if (vistamodelo.getListaArticulos().get(j).getImporte() > 0
                                        && vistamodelo.getListaArticulos().get(j).getCantidad() > vistamodelo.getListaArticulos().get(j).getDevueltos()) {
                                    importe += vistamodelo.getListaArticulos().get(j).getImporte();
                                }
                            }
                            grupoBotonCupones.setDisable(importe <= 0);
                            cuponesActivos.setValue(importe > 0);
                        }
                    } else {
                        cuponesActivos.setValue(false);
                    }
                } catch (Exception e) {
                    SION.log(Modulo.VENTA, "eventosCupones : Se presentó un error al validar el estatus de cupones." + e.getMessage(), Level.INFO);
                }

            }
        });
    }

    public int validarImporteTarjetas() {

        idImporteTarjetas = 0;

        if (vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS) > Double.parseDouble(vista.getMontoVenta())) {
            idImporteTarjetas = 1;
        }

        return idImporteTarjetas;
    }

    public double validarImporteVales() {

        importeVales = 0;
        importeAplicado = vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS);
        cantidadVales = 0;

        importePorAplicar = Double.parseDouble(vista.getMontoVenta()) - importeAplicado;

        if (!vista.getCantidadVales().isEmpty()) {
            try {
                cantidadVales = Integer.parseInt(vista.getCantidadVales());
            } catch (NumberFormatException e) {
                cantidadVales = 0;
            }
        }

        montoVales = 0;

        if (!vista.getMontoVales().isEmpty()) {

            if (cantidadVales > 0) {
                montoVales = Double.parseDouble(vista.getMontoVales());

                if (montoVales > importePorAplicar) {
                    importeVales = importePorAplicar;

                } else {
                    importeVales = Double.parseDouble(vista.getMontoVales());

                }
            } else {
                importeVales = 0;
            }

        } else {
            importeVales = 0;
        }

        return importeVales;
    }

    public double validarImporteEfectivo() {

        efectivo = 0;
        importeAplicado = 0;
        importePorAplicar = 0;

        importeAplicado += vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS);

        if (!vista.getMontoVales().isEmpty()) {
            importeAplicado += Double.parseDouble(vista.getMontoVales());
        }
        if (!vista.getMontoVenta().isEmpty()) {
            importePorAplicar += Double.parseDouble(vista.getMontoVenta()) - importeAplicado;
        } else {
            importePorAplicar = 0;
        }

        if (!vista.getMontoEfectivo().isEmpty()) {
            if (Double.parseDouble(vista.getMontoEfectivo()) > importePorAplicar) {

                efectivo = importePorAplicar;
            } else {

                efectivo = Double.parseDouble(vista.getMontoEfectivo());
            }
        } else {

            efectivo = 0;
        }

        return efectivo;
    }

    ////////////////////////////////////////////////////////////////////buscador 
    public void operacionesbuscador(final StringProperty propiedadCadena,
            final Label etiquetaMontoVenta, final TextField campoBusqueda, final TextField campoImporteRecibido) {

        double cantidad = 1;
        int posicionSignoPor = 0;
        int i = 0;
        String cadenaValidacion = "";
        int idGranel = 0;

        //SION.log(Modulo.VENTA, "Entrando a método de buscador de precios", Level.FINEST); 

        if (!"".equals(cadena)) {
            SION.log(Modulo.VENTA, "Se procede a evaluar cadena ingresada por el usuario: " + cadena, Level.INFO);

            if (cadena.contains("*")) {
                SION.log(Modulo.VENTA, "Cadena ingresada por el usuario contiene caracter *", Level.INFO);

                if (cadena.contains(".")) {

                    SION.log(Modulo.VENTA, "Cadena ingresada por el usuario contiene caracter .", Level.INFO);

                    matcherBuscador = patronBuscadorConPunto.matcher(cadena);

                    if (matcherBuscador.find()) {

                        posicionSignoPor = cadena.indexOf("*");

                        while (i < posicionSignoPor) {
                            cadenaValidacion += cadena.charAt(i);
                            i++;
                        }
                        try {
                            cantidad = Double.parseDouble(cadenaValidacion);
                        } catch (NumberFormatException e) {
                            cantidad = 0;
                            SION.logearExcepcion(Modulo.VENTA, e, "");
                        }

                        if (cantidad > 0 && cantidad < CANTIDAD_MAXIMA_POR_ARTICULO) {

                            cadena = cadena.substring(posicionSignoPor + 1);
                            idGranel = 1;

                            SION.log(Modulo.VENTA, "Cantidad de artículos encontrados en cadena: " + cantidad, Level.INFO);
                            SION.log(Modulo.VENTA, "Artículo encontrado en la cadena: " + propiedadCadena.getValue(), Level.INFO);

                            consultarArticulo(cantidad, idGranel, propiedadCadena.getValue(), metodoEntradaBuscador);

                        } else {
                            mostrarAlertaRetornaFoco("La cadena ingresada: " + cadena + propiedadCadena.getValue() + " no es válida", TipoMensaje.ADVERTENCIA, vista.getTablaVentas());
                        }

                    } else {
                        mostrarAlertaRetornaFoco("La cadena ingresada: " + cadena + propiedadCadena.getValue() + " no es válida", TipoMensaje.ADVERTENCIA, vista.getTablaVentas());

                    }
                } else {

                    matcherBuscador = patronBuscadorSinPunto.matcher(cadena);
                    if (matcherBuscador.find()) {

                        SION.log(Modulo.VENTA, "El patrón de la cadena es correcto", Level.FINEST);
                        posicionSignoPor = cadena.indexOf("*");

                        cantidad = Integer.parseInt(cadena.substring(0, posicionSignoPor));

                        SION.log(Modulo.VENTA, "Cantidad encontrada en la cadena: " + cantidad, Level.INFO);
                        if (cantidad < 1) {
                            SION.log(Modulo.VENTA, "Cantidad encontrada cero, se setea a cantidad por default = 1", Level.INFO);
                            cantidad = 1;
                        } else if (cantidad > CANTIDAD_MAXIMA_POR_ARTICULO) {
                            SION.log(Modulo.VENTA, "Cantidad encontrada mayor a " + CANTIDAD_MAXIMA_POR_ARTICULO + ", se setea a cantidad por default = 1", Level.INFO);
                            cantidad = 1;
                        }
                        consultarArticulo(cantidad, idGranel, propiedadCadena.getValue(), metodoEntradaBuscador);
                    } else {
                        mostrarAlertaRetornaFoco("La cadena ingresada: " + cadena + propiedadCadena.getValue() + " no es válida", TipoMensaje.ADVERTENCIA, vista.getTablaVentas());
                    }

                }
            } else {
                AdministraVentanas.mostrarAlertaRetornaFoco("La cadena ingresada: " + cadena + propiedadCadena.getValue() + " no es válida", TipoMensaje.ADVERTENCIA, vista.getTablaVentas());

            }

        } else {

            //SION.log(Modulo.VENTA, "Se procede a invocar método DAO con valores por default", Level.INFO); 
            consultarArticulo(1, 0, propiedadCadena.getValue(), metodoEntradaBuscador);
        }

        cadena = "";

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                vista.getTablaVentas().requestFocus();

                if (vistamodelo.getImporteVenta() > 0) {
                    etiquetaMontoVenta.textProperty().setValue(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));

                } else {
                    etiquetaMontoVenta.textProperty().setValue("0.00");
                } //deshabilita campo 

                campoBusqueda.setDisable(true);
                //ajusta scroll  
                if (vistamodelo.getTamanioListaArticulos() > 1) {
                    vista.posicionarScrollTabla(vistamodelo.getTamanioListaArticulos());
                }
            }
        });

    }

    public void eventosBuscador(final BorderPane panel, final IntegerProperty propiedadEntero, final StringProperty propiedadCadena,
            final Label etiquetaMontoVenta, final TextField campoBusqueda, final TextField campoImporteRecibido) {

        campoBusqueda.focusedProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {

                if (arg1 && !arg2) {
                    if (!campoBusqueda.textProperty().getValue().isEmpty()) {
                        campoBusqueda.textProperty().setValue("");
                    }
                } else if (!arg1 && arg2) {
                    if (idPantalla == PANTALLA_VENTA) {
                        idPantalla = PANTALLA_CAPTURA;
                    }
                    agregarEventosCaptura(panel);
                }
            }
        });

        campoBusqueda.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {

                if (f8.match(arg0)) {

                    if (etiquetaMontoVenta.textProperty().getValue() != "") {
                        try {
                            if (vistamodelo.getTamanioListaArticulos() > 0
                                    && Double.parseDouble(etiquetaMontoVenta.textProperty().getValue()) > 0) {
                                //SION.log(Modulo.VENTA, ">>>****", Level.INFO); 
                                campoImporteRecibido.setDisable(false);
                                campoImporteRecibido.requestFocus();
                            }
                        } catch (NumberFormatException e) {
                            SION.logearExcepcion(Modulo.VENTA, e, "");
                        }
                    }
                    //SION.log(Modulo.VENTA, ">>>*****", Level.INFO); 
                    campoImporteRecibido.setDisable(false);
                    campoImporteRecibido.requestFocus();
                    // 
                }
            }
        });

    }

    public void llamarBloqueos(final Button boton) {

        boton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        evaluarBloqueosVenta();
                    }
                });
            }
        });

    }

    ///////////////////////////////////////////////////////marcacion de la venta 
    public void marcarVenta(final long conciliacion, final double efectivoCambio, final double montoRecibido) {

        SION.log(Modulo.VENTA, "Entrando a metodo de marcado de venta en línea", Level.INFO);

        try {//se intenta marcar la venta 
            if (esVentaTA) {
                SION.log(Modulo.VENTA, "Se procede a registrar la petición de venta TA", Level.INFO);
                vistamodelo.registrarVentaTA();
                errorVenta = vistamodelo.getErrorVentaTA();

            } else if (vistamodelo.hayTarjetaPagaTodo()) {
                SION.log(Modulo.VENTA, "Se procede a registrara la petición de venta PAGATODO", Level.INFO);
                vistamodelo.registrarVentaPagaTodo();
                errorVenta = vistamodelo.getErrorVenta();
            } else {
                SION.log(Modulo.VENTA, "Se procede a registrara la petición de venta", Level.INFO);
                try {
                    if (!((Boolean) configuracionCupones.cuponesExternosActivos())) {
                        vistamodelo.getPeticionVentaCuponesDto().setEsVentaSinMarketec(true);
                        vistamodelo.getPeticionVentaCuponesDto().setMotivoSinCupones("Cupones externos desactivados.");
                    }
                } catch (Exception ex) {
                }
                vistamodelo.registrarVenta();
                errorVenta = vistamodelo.getErrorVenta();
            }

            if (errorVenta == 0) {//venta satisfactoria 

                SION.log(Modulo.VENTA, "la peticion de venta fue aprobada, se procede a agregar objetos de sesión", Level.FINEST);
                esVentaExitosa = true;

                if (esVentaTA) {
                    //tiempoAireVista.limpiarImagenes(); 
                    vistamodelo.agregarObjetoSesionTA();
                    vistamodelo.actualizarTransacciónLocal(conciliacion, 1, 2, 1, true, ACTUALIZACION_LOCAL);
                    tipoTicket = 1;
                    vista.habilitarCampoCantidadVales(false);
                    vista.habilitarCampoMontoVales(false);
                    vista.habilitarCampoMontoValesE(false);
                    vista.habilitarCampoMontoTarjeta(false);
                    //cambiarEtiquetaPago(0); 
                    vista.cambiarEstadoCampoImporteRecibido(true);
                    //VentaVista.campoImporteRecibido.setDisable(true); 
                    vista.setMontoRecibidoPantallaCaptura("");
                    //VentaVista.tabla.requestFocus(); 
                } else {
                    vistamodelo.agregarObjetoSesion();
                    vistamodelo.actualizarTransacciónLocal(conciliacion, 1, 2, 1, false, ACTUALIZACION_LOCAL);
                    tipoTicket = 2;
                }

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        impresionTicket(tipoTicket, efectivoCambio, montoRecibido, false, conciliacion, false);
                        //impresionTicket(tipoTicket, efectivoCambio, conciliacion, false, conciliacion, false); 

                        if (vistamodelo.seCompletoTicketVenta() || vistamodelo.seCompletoTicketVentaTA()) {
                            evaluarBloqueosVenta();
                        }
                    }
                });

            } else {//error de venta 

                if (esVentaTA) {

                    if (errorVenta == 100 || errorVenta == 180 || errorVenta == 114) {
                        SION.log(Modulo.VENTA, "Se recibió código error " + errorVenta + ", se procede actualizar conciliación a estatus 4", Level.INFO);
                        vistamodelo.reversarConciliacionVentaTA(conciliacion);
                    } else {
                        SION.log(Modulo.VENTA, "Se recibió código diferente a 100,114 o 180 se procede actualizar conciliación a estatus 5, "
                                + "código de error" + errorVenta, Level.INFO);
                        vistamodelo.cancelarConciliacionVenta(conciliacion);
                    }

                    //tiempoAireVista.limpiarImagenes(); 
                    vista.cambiarEstadoCampoImporteRecibido(true);
                    vista.setMontoRecibidoPantallaCaptura("");

                    SION.log(Modulo.VENTA, "La venta TA no se llevó a cabo, código de error: " + vistamodelo.getErrorVentaTA() + ", "
                            + vistamodelo.getDescErrorVentaTA(), Level.INFO);

                } else {

                    SION.log(Modulo.VENTA, "La venta no se llevó a cabo, código de error: " + vistamodelo.getErrorVenta() + ", "
                            + vistamodelo.getDescErrorVenta(), Level.INFO);

                    vistamodelo.cancelarConciliacionVenta(conciliacion);

                    mensajeError = vistamodelo.getDescErrorVenta().substring(1).split("\\|");

                    String data = null;
                    for (String string : mensajeError) {
                        if (!"".equals(string.trim())) {
                            data = string;
                            break;
                        }
                    }
                    //System.out.println(">"+data); 
                    mensajeError[0] = data;

                    if (idPantalla == PANTALLA_CAPTURA) {
                        mostrarAlertaRetornaFoco(vistamodelo.getDescErrorVenta(), TipoMensaje.ERROR, vista.getTablaVentas());

                    } else {
                        mostrarAlertaRetornaFoco(mensajeError[0], TipoMensaje.ERROR, vista.getCampoMontoEfectivo());

                    }
                }

                vistamodelo.borrarRegistrosValesEfectivo();

                RecargasTADBinding.numero = 0;
            }
        }//truena marcado de venta                             
        catch (SionException e) {
            SION.logearExcepcion(Modulo.VENTA, e, "");

            if (esVentaTA) {
                SION.log(Modulo.VENTA, "Ocurrió un error al intentar registrar la petición de venta de Tiempo Aire. ", Level.SEVERE);
            } else {
                SION.log(Modulo.VENTA, "Ocurrió un error al intentar registrar la petición de venta. "
                        + e.getMensajeFront().replace("##", String.valueOf(e.getCodigoAccion())), Level.SEVERE);
            }

            hayComunicacion = false;

            RecargasTADBinding.numero = 0;

        }
    }

    public void impresionTicket(int tipoTicket, double efectivoCambio, double montoRecibido, boolean esVentaFueraLinea,
            long conciliacion, boolean esConfirmacionRecarga) {

        try {
            SION.log(Modulo.VENTA, "Se procede a entrar a mètodo de impresión de ticket", Level.INFO);
            vistamodelo.imprimir(tipoTicket, efectivoCambio, montoRecibido, esVentaFueraLinea, conciliacion, esConfirmacionRecarga);
        } catch (Exception e) {

            SION.log(Modulo.VENTA, "Ocurrió un error al intentar imprimir el ticket de transacción " + vistamodelo.getTransaccion()
                    + " : " + getStackTrace(e), Level.SEVERE);

            limpiarVenta();
        }

        if ((esVentaExitosa || esVentaFueraLineaExitosa)
                && (!vistamodelo.seCompletoTicketVenta() && !vistamodelo.seCompletoTicketVentaTA())) {

            int parametroVentana = 0;

            try {
                parametroVentana = Integer.parseInt(SION.obtenerParametro(Modulo.SION, "SEGURIDAD.VENTANADESBLOQUEO.EGRESOVALORES"));
            } catch (NumberFormatException e) {
                parametroVentana = 1;
            }

            if (esVentaTA) {
                if (verificaVentana.consultaEstatusVentana(parametroVentana)) {

                    SION.log(Modulo.VENTA, "estatus de ventana de bloqueo = true, se muestra alerta de error de impresión y "
                            + "se continua con la venta", Level.FINEST);

                    if (esConfirmacionVentaTA) {
                        mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresora").replace(
                                "%1", String.valueOf(vistamodelo.getTransaccionConfirmacionTA())), TipoMensaje.INFO, vista.getTablaVentas());

                    } else {
                        mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresora").replace(
                                "%1", String.valueOf(vistamodelo.getTransaccionTA())), TipoMensaje.INFO, vista.getTablaVentas());

                    }

                } else {
                    SION.log(Modulo.VENTA, "estatus de ventana de bloquoe = false, mostrando alerta de error de impresora en impresión de ticket de venta TA", Level.FINEST);

                    if (esConfirmacionVentaTA) {
                        mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresora").replace(
                                "%1", String.valueOf(vistamodelo.getTransaccionConfirmacionTA())), TipoMensaje.INFO, vista.getTablaVentas());

                    } else {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlerta(SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresora").replace(
                                        "%1", String.valueOf(vistamodelo.getTransaccionTA())), TipoMensaje.INFO, botonBloqueos);
                            }
                        });
                    }
                }

            } else {
                if (verificaVentana.consultaEstatusVentana(parametroVentana)) {
                    SION.log(Modulo.VENTA, "estatus de ventana de bloqueo = true, mostrando alerta de "
                            + "error de impresora y se continúa con la venta", Level.FINEST);
                    if (esVentaFueraLinea) {
                        mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresora").replace(
                                "%1", String.valueOf(vistamodelo.getTransaccion()).concat("O")), TipoMensaje.INFO, vista.getTablaVentas());

                    } else {
                        mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresora").replace(
                                "%1", String.valueOf(vistamodelo.getTransaccion()).concat("I")), TipoMensaje.INFO, vista.getTablaVentas());

                    }
                } else {
                    SION.log(Modulo.VENTA, "estatus de ventana de bloqueo = false, mostrando alerta de "
                            + "error de impresora en impresión de ticket de venta", Level.FINEST);

                    if (esVentaFueraLinea) {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlerta(SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresora").replace(
                                        "%1", String.valueOf(vistamodelo.getTransaccion()).concat("O")), TipoMensaje.INFO, botonBloqueos);
                            }
                        });

                    } else {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlerta(SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresora").replace(
                                        "%1", String.valueOf(vistamodelo.getTransaccion()).concat("I")), TipoMensaje.INFO, botonBloqueos);
                            }
                        });

                    }
                }

            }
        }
    }

    public void impresionTicketFueraLinea(double efectivoCambio, double montoRecibido) {

        tipoTicket = 0;

        try {
            if (esVentaTA) {
                tipoTicket = TiposImpresion.TICKET_TIEMPO_AIRE;
            } else {
                tipoTicket = TiposImpresion.TICKET_NORMAL;
            }
            if (vistamodelo.getListaPagosTarjeta().size() > 0) {
                tipoTicket = TiposImpresion.TICKET_PAGOS_ELETRONICOS;
            }

            SION.log(Modulo.VENTA, "Se procede a imprimir ticket de venta fuera de línea", Level.INFO);

            vistamodelo.imprimir(tipoTicket, efectivoCambio, montoRecibido, true, vistamodelo.getConciliacionIdFueraLinea(), false);

        } catch (Exception e) {

            SION.log(Modulo.VENTA, "Ocurrió un error al intentar imprimir el ticket de venta fuera de línea conciliación: "
                    + vistamodelo.getConciliacionIdFueraLinea() + " : " + getStackTrace(e), Level.SEVERE);

            limpiarVenta();
        }

        SION.log(Modulo.VENTA, "Ticket de venta completado? " + vistamodelo.seCompletoTicketVenta(), Level.FINEST);
        if (!vistamodelo.seCompletoTicketVenta()) {

            SION.log(Modulo.VENTA, "Mostrando alerta de fallo en impresión en el ticket de venta fuera de línea", Level.FINEST);
            mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.noimpresora").replace(
                    "%1", String.valueOf(vistamodelo.getConciliacionIdFueraLinea()).concat("O")), TipoMensaje.INFO, vista.getTablaVentas());

        }

    }

    public void aplicarVentaFueraDeLinea(double _importeVenta, int _sistema, int _modulo, int _submodulo,
            long _conciliacion, final String _usuarioAutoriza, String _estatusLecturaTarjeta) {

        if (!hayVentaFLEnProceso) {
            SION.log(Modulo.VENTA, "VENTA FUERA DE LINEA", Level.INFO);
            hayVentaFLEnProceso = true;
        } else {
            SION.log(Modulo.VENTA, "Venta en proceso (Se intentó realizar una venta FL con otra venta sin terminar)", Level.INFO);
            return;
        }

        int intentos = 0;

        vistamodelo.llenarPeticionVentaFueraLinea(_importeVenta, _sistema, _modulo, _submodulo, _conciliacion, _usuarioAutoriza, _estatusLecturaTarjeta);
        vistamodelo.llenarListaArticulosVentaFueraLinea();
        vistamodelo.llenarListaTiposPagoVentaFueraLinea();

        do {

            SION.log(Modulo.VENTA, "intento de marcado de venta fuera de linea #" + intentos, Level.INFO);
            esVentaFueraLineaExitosa = vistamodelo.marcarVentaFueraDeLinea();
            intentos++;

        } while (intentos < 5 && esVentaFueraLineaExitosa == false);

        if (esVentaFueraLineaExitosa) {
            SION.log(Modulo.VENTA, "la venta se realizó en " + intentos + " intentos", Level.INFO);
        } else {
            SION.log(Modulo.VENTA, "El número de intentos de marcado de venta se terminó, "
                    + "la venta fuera de línea no se pudo completar", Level.SEVERE);
            if (esMarcadoFueraLineaDirecto) {
                SION.log(Modulo.VENTA, "No se generó conciliación, se continúa con el proceso", Level.INFO);
            } else {
                SION.log(Modulo.VENTA, "Se procede a cancelar conciliación generada", Level.WARNING);
                vistamodelo.cancelarConciliacionVenta(_conciliacion);
            }

            if (idPantalla == PANTALLA_CAPTURA) {
                mostrarAlertaRetornaFoco(SION.obtenerMensaje(
                        Modulo.VENTA, "ventas.errorBDLocal"), TipoMensaje.ERROR, vista.getTablaVentas());

            } else {
                mostrarAlertaRetornaFoco(SION.obtenerMensaje(
                        Modulo.VENTA, "ventas.errorBDLocal"), TipoMensaje.ERROR, vista.getCampoMontoEfectivo());

            }
        }
        hayVentaFLEnProceso = false;

    }

    public void aplicarVentaFueraDeLineaPagaTodo(double _importeVenta, int _sistema, int _modulo, int _submodulo,
            long _conciliacion, final String _usuarioAutoriza, String _estatusLecturaTarjeta) {

        int intentos = 0;
        vistamodelo.llenarPeticionVentaFueraLinea(_importeVenta, _sistema, _modulo, _submodulo, _conciliacion, _usuarioAutoriza, _estatusLecturaTarjeta);
        vistamodelo.llenarListaArticulosVentaFueraLinea();
        vistamodelo.llenarListaTiposPagoVentaFueraLinea();
        do {
            SION.log(Modulo.VENTA, "intento de marcado de venta fuera de linea #" + intentos, Level.INFO);
            esVentaFueraLineaExitosa = vistamodelo.marcarVentaFueraDeLinea();
            intentos++;
        } while (intentos < 5 && esVentaFueraLineaExitosa == false);

        if (esVentaFueraLineaExitosa) {
            vistamodelo.actualizaConciliacionPendiente();
            SION.log(Modulo.VENTA, "la venta se realizó en " + intentos + " intentos", Level.INFO);
        } else {
            SION.log(Modulo.VENTA, "El número de intentos de marcado de venta se terminó, "
                    + "la venta fuera de línea PAGA TODO no se pudo completar", Level.SEVERE);
            mostrarAlertaRetornaFoco(SION.obtenerMensaje(
                    Modulo.VENTA, "ventas.errorBDLocal"), TipoMensaje.ERROR, vista.getCampoMontoEfectivo());
        }
    }

    public long generarConciliacion(boolean _esRecargaTA) {


        SION.log(Modulo.VENTA, "Generando conciliacion", Level.INFO);

        conciliacion = 0;

        int sistema_conciliacion = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacion.venta.sistema"));
        int modulo_conciliacion = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacion.venta.modulo"));
        int submodulo_conciliacion = 0;


        if (_esRecargaTA) {
            submodulo_conciliacion = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacion.recarga.submodulo"));
        } else {
            //venta de articulos 
            submodulo_conciliacion = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacion.venta.submodulo"));

            if (vistamodelo.getPeticionVentaCuponesDto() != null
                    && vistamodelo.getPeticionVentaCuponesDto().getFinTicketMktc() != null
                    && vistamodelo.getPeticionVentaCuponesDto().getFinTicketMktc().getTicketCompleto() != null
                    && !"".equals(vistamodelo.getPeticionVentaCuponesDto().getFinTicketMktc().getTicketCompleto())) {
                return Long.parseLong(
                        vistamodelo.getPeticionVentaCuponesDto().getFinTicketMktc().getTicketCompleto());
            }
        }

        vistamodelo.llenarPeticionConciliacion(sistema_conciliacion, modulo_conciliacion, submodulo_conciliacion);

        int intentos_Conciliacion = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "conciliacion.intentos"));
        int intento = 0;

        do {

            SION.log(Modulo.VENTA, "Intento número " + intento, Level.INFO);
            conciliacion = vistamodelo.obtenerRespuestaConciliacion();

            SION.log(Modulo.VENTA, "Número de conciliación obtenido: " + conciliacion, Level.INFO);
            if (conciliacion == 0 && intento < 5) {
                SION.log(Modulo.VENTA, "Se procede a realizar un intento más", Level.INFO);
            } else {
                if (intento == 5) {
                    SION.log(Modulo.VENTA, "Intentos de generación de numero de conciliación agotados, "
                            + "se prodece a cancelar al venta", Level.SEVERE);
                }
            }

            intento++;

        } while (conciliacion == 0 && intento < intentos_Conciliacion);

        return conciliacion;



    }

    public boolean esMontoRecibidoValido() {////validacion para evitar que se escanee un articulo sobre el campo con el moto de pago 

        esMontoRecibidoValido = false;

        if (idPantalla == PANTALLA_VENTA) {
            if (getImporteRecibido() <= getMontoMaximoPermitido()) {
                esMontoRecibidoValido = true;
            }
        } else {
            if (getImporteRecibidoVentaRapida() <= getMontoMaximoPermitido()) {
                esMontoRecibidoValido = true;
            }
        }

        return esMontoRecibidoValido;
    }

    public void validarVentaVales(final int idOperacion, final String _usuarioAutoriza) {
        double montoVales = 0;
        double montoVenta = 0;

        try {
            montoVales = Double.parseDouble(vista.getCampoMontoVales().textProperty().getValue());
        } catch (NumberFormatException e) {
            montoVales = 0;
        }

        montoVenta = vistamodelo.getImporteVenta();

        //se valida q el monto ingresado en vales no sea mayor al 10% sobre el persio d la venta, si es asi se  
        //manda popup de confirmacion al cajero 
        if (montoVales > (montoVenta * 1.1)) {

            listenerPopupVales = new ChangeListener<Number>() {

                @Override
                public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
                    if (arg2.intValue() == 1) {
                        SION.log(Modulo.VENTA, "Boton aceptar presionado", Level.INFO);
                        iniciarVenta(idOperacion, _usuarioAutoriza);
                        utileriasVenta.getEstatusPopup().removeListener(listenerPopupVales);
                    } else if (arg2.intValue() == 2) {
                        SION.log(Modulo.VENTA, "Boton cancelar presionado", Level.INFO);
                        vista.getCampoCantidadVales().textProperty().setValue("");
                        vista.getCampoMontoVales().textProperty().setValue("");
                        utileriasVenta.getEstatusPopup().removeListener(listenerPopupVales);
                        vista.removerImagenLoading();

                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                vista.getCampoCantidadVales().requestFocus();
                            }
                        });
                    }
                }
            };
            utileriasVenta.getEstatusPopup().addListener(listenerPopupVales);

            String cadenaMontoVales = String.valueOf(montoVales);

            if (cadenaMontoVales.contains(".")) {
                utileriasVenta.mostrarPopupPregunta(SION.obtenerMensaje(Modulo.VENTA, "venta.vales.error.monto").
                        replace("%1", cadenaMontoVales.substring(0, cadenaMontoVales.indexOf("."))), TipoMensaje.PREGUNTA);
            } else {
                utileriasVenta.mostrarPopupPregunta(SION.obtenerMensaje(Modulo.VENTA, "venta.vales.error.monto").
                        replace("%1", cadenaMontoVales), TipoMensaje.PREGUNTA);
            }
        } else {
            iniciarVenta(idOperacion, _usuarioAutoriza);
        }
    }

    private void limpiadoParcialPorErrorPagaTodo() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                AdministraVentanas.mostrarAlertaRetornaFoco(vistamodelo.getDescErrorVenta(),
                        TipoMensaje.ERROR, vista.getTablaVentas());

                vista.setMontoRecibidoPantallaCaptura("");
                vista.setMontoCambio("");
                //vista.setMontoVenta("0.00"); 
                vista.setMontoRestante("0.00");

                vista.setMontoEfectivo("");
                vista.setMontoVales("");
                vista.setCantidadVales("");
                vista.setMontoValesE("");
                vista.setMontoTarjetas("");

                vista.setValorEtiquetaCantidadTarjetasRecibidas("0");
                vista.setValorEtiquetaCantidadValesERecibidos("0");
                vista.setValorMontoCantidadTarjetasRecibidas("0.00");
                vista.setValorMontoCantidadValesERecibidos("0.00");

                cambiarCajasComponentes(PANTALLA_CAPTURA, vista.getPanelVentas());

                vista.getTablaVentas().setItems(vistamodelo.getListaArticulos());

                vista.setFocusTablaVentas();

                vista.setDescripcionTarjetaBancaria("");
                vista.setDescripcionNoTarjeta(String.valueOf(""));
                vista.setDescripcionImporteTarjeta("");
                vista.setDescripcionAutorizada("");
            }
        });
    }

    private void validarComunicacionCentral() {
        long timedOut = 0;
        int puerto = 0;

        timedOut = vistamodelo.obtenerTimedOut();
        if (timedOut == 0) {
            try {
                timedOut = Long.valueOf(SION.obtenerParametro(Modulo.VENTA, "ventanormal.axis2.timeout"));
                SION.log(Modulo.VENTA, "Timed out asigando desde archivo de propiedades", Level.INFO);
            } catch (Exception e) {
                timedOut = 10000;
                SION.log(Modulo.VENTA, "Timed out asignado por default al no encontrarse parametro ni archivo de propiedades", Level.INFO);
            }
        }

        SION.log(Modulo.VENTA, "Timed out con que se consultara comunicacion: " + timedOut + " ms", Level.INFO);

        try {
            puerto = Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.timedout.puerto"));
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, "Error al obtener propiedad venta.timedout.puerto");
            puerto = 0;
        }

        try {
            if (puerto > 0) {
                String host = "";
                try {
                    host = SION.obtenerParametro(Modulo.VENTA, "venta.timedout.host");
                } catch (Exception e) {
                    host = "";
                    SION.logearExcepcion(Modulo.VENTA, e, "Ocurrio un error al obtener propiedad venta.timedout.host");
                }
                if (host == null || host.isEmpty()) {
                    SION.log(Modulo.VENTA, "Se procede a verificar si hay conexión intentando con dirección wsdl:"
                            + String.valueOf(SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice.redondeo")), Level.INFO);
                    hayComunicacion = Utilerias.evaluarConexionCentral(SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice.redondeo"), 3);
                } else {
                    SION.log(Modulo.VENTA, "Se procede a verificar si hay conexión intentando con host: " + host + ":" + puerto, Level.INFO);
                    hayComunicacion = Utilerias.evaluarConexionCentral(host, puerto, 3, (int) timedOut, Modulo.VENTA);
                }
            } else {
                hayComunicacion = Utilerias.evaluarConexionCentral(SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice"), 3);
            }
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, "excepcion al intentar consultar propiedad venta.timedout.host");
        }


        SION.log(Modulo.VENTA, "Hay comunicación? " + hayComunicacion, Level.INFO);
    }

    private void abrirCajon() {
        vistamodelo.llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cajon.sistema")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cajon.modulo")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cajon.submodulo")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cajon.configuracion")));

        if ((vistamodelo.consultarParametro() != null)
                && (vistamodelo.getCodigoErrorParametroOperacion() == 0)) {
            if (vistamodelo.getValorConfiguracionParametroOperacion() == 1.0D) {

                SION.log(Modulo.VENTA, "Parametro activo. Abriendo cajon", Level.INFO);

                try {
                    final UtileriasImpresora ui = new UtileriasImpresora();

                    ui.abrirCaja();
                    SION.log(Modulo.VENTA, "Cajon abierto", Level.INFO);
                } catch (Exception ex) {
                    SION.logearExcepcion(Modulo.CAJA, ex, UtileriasLog.getStackTrace(ex));
                } catch (Error e) {
                    SION.log(Modulo.VENTA, "Error: " + e.getLocalizedMessage(), Level.SEVERE);
                }

            } else {
                SION.log(Modulo.VENTA, "Parametro de cajon desactivado", Level.INFO);
            }

        }
    }
    private boolean tareaVentaEnEjecusion = false;

    public void iniciarVenta(final int idOperacion, final String _usuarioAutoriza) {

        if (tareaVentaEnEjecusion) {
            SION.log(Modulo.VENTA, "Hay una tarea de venta ejecutandose en este momento, se ignorará esta solicitud de procesar venta.", Level.WARNING);
            return;
        }

        tareaVentaEnEjecusion = true;
        montoVenta = Double.parseDouble(vista.getMontoVenta());

        final Task tarea = new Task() {

            @Override
            protected Object call() throws Exception {

                int tarjetas = 0;
                double vales = 0.0;
                double efectivo = 0.0;
                double comisionVales = 0.0;
                double efectivoCambio = 0.0;

                int tipomov = 1;
                double montoRecibido = 0.0;

                long conciliacion = 0;
                long codigoErrorFueraLinea = 0;
                int folioTa = 0;

                vistamodelo.validaTarjetaPagaTodo();
                vistamodelo.borrarRegistrosValesEfectivo();
                vistamodelo.limpiarListasDto();
                esVentaFueraLineaExitosa = false;
                esVentaExitosa = false;
                existeParametroVentaFueraLinea = false;
                forzadoVentaFL = false;
                hayComunicacion = true;
                esMarcadoFueraLineaDirecto = false;
                esConfirmacionVentaTA = false;

                if (idOperacion == 1 || idOperacion == 3) {

                    SION.log(Modulo.VENTA, "El método fue invocado desde la pantalla de otras formas de pago", Level.INFO);

                    tarjetas = validarImporteTarjetas();
                    SION.log(Modulo.VENTA, "Monto con tarjetas detectado: " + tarjetas, Level.INFO);

                    if (tarjetas == 0) {
                        vales = validarImporteVales();

                        if (vales > 0 && vales < Double.parseDouble(vista.getMontoVales())) {
                            comisionVales = Double.parseDouble(vista.getMontoVales()) - vales;

                            vistamodelo.insertarRegistroFormaPago(new DatosFormasPagoBean(3, Double.parseDouble(utileriasVenta.getNumeroFormateado(vales)), Integer.parseInt(vista.getCantidadVales()), "", 0, 0,
                                    Double.parseDouble(utileriasVenta.getNumeroFormateado(comisionVales))));
                        } else {
                            if (vales > 0) {
                                vistamodelo.insertarRegistroFormaPago(new DatosFormasPagoBean(3,
                                        Double.parseDouble(utileriasVenta.getNumeroFormateado(vales)), Integer.parseInt(vista.getCantidadVales()), "", 0, 0, 0));

                            }

                            efectivo = validarImporteEfectivo();

                            if (efectivo > 0 && efectivo == Double.parseDouble(vista.getMontoEfectivo())) {
                                montoRecibido = efectivo;

                                vistamodelo.insertarRegistroFormaPago(new DatosFormasPagoBean(1,
                                        Double.parseDouble(utileriasVenta.getNumeroFormateado(efectivo)), 0, "", 0, 0, montoRecibido));

                            } else if (efectivo > 0 && efectivo < Double.parseDouble(vista.getMontoEfectivo())) {

                                efectivoCambio = Double.parseDouble(vista.getMontoEfectivo()) - efectivo;
                                montoRecibido = efectivoCambio + efectivo;

                                vistamodelo.insertarRegistroFormaPago(new DatosFormasPagoBean(1,
                                        Double.parseDouble(utileriasVenta.getNumeroFormateado(efectivo)), 0, "", 0, 0, montoRecibido));

                            }

                        }
                    }
                }

                if (idOperacion == 4) {

                    SION.log(Modulo.VENTA, "El método fue invocado desde la pantalla de captura de artículos", Level.INFO);
                    ///cantidad en pantalla 
                    montoRecibido = Double.parseDouble(vista.getMontoRecibidoPantallCaptura());

                    double temp = Double.parseDouble(utileriasVenta.getNumeroFormateado(montoRecibido
                            - (Double.parseDouble(vista.getMontoVenta()) + vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS))));

                    if (temp < 0) {
                        if (temp * -1 >= vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS)) {
                            efectivo = montoRecibido + temp;
                        } else {
                            efectivo = Double.parseDouble(vista.getMontoVenta()) - vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS);
                        }
                    } else {
                        if (temp >= vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS)) {
                            efectivo = Double.parseDouble(vista.getMontoVenta()) - vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS);
                        } else {
                            efectivo = montoRecibido - temp;
                        }
                    }

                    if ((montoRecibido + vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS)) > Double.parseDouble(vista.getMontoVenta())) {
                        efectivoCambio = (montoRecibido + vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS)) - Double.parseDouble(vista.getMontoVenta());
                    }

                    vistamodelo.insertarRegistroFormaPago(new DatosFormasPagoBean(1, Double.parseDouble(utileriasVenta.getNumeroFormateado(efectivo)), 0, "", 0, 0, montoRecibido));
                }
                ///se agregan los tipos de pago y se obtiene cadena de estatus de tarjeta en el caso q hubiera 
                String estatusLectura = vistamodelo.agregarTiposPago();

                /// se valida la ip 
                if (!vistamodelo.getIp().isEmpty() && vistamodelo.getIp() != null) {
                    //se llena lista ordenada de articulos 
                    if (esVentaTA) {
                        vistamodelo.llenarListaArticulosVentaTA();
                    } else {
                        vistamodelo.llenarListaArticulosVenta(idOperacion);
                    }

                    if (vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS) > montoVenta) {
                        SION.log(Modulo.VENTA, "El importe recibído en tarjetas es mayor que el importe total de la venta", Level.FINEST);
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.montos.tarjeta.negativos"),
                                        TipoMensaje.ADVERTENCIA, vista.getCampoMontoEfectivo());
                                vistamodelo.borrarRegistrosValesEfectivo();
                            }
                        });
                    } else //se valida monto recibido >= venta 
                    if ((Double.parseDouble(utileriasVenta.getNumeroFormateado(efectivoCambio + efectivo + vales + vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS) + comisionVales))
                            >= Double.parseDouble(utileriasVenta.getNumeroFormateado(montoVenta)))) {

                        //se consultan parametros para ver si esta activada la venta Fl desde BD 
                        consultarParametrosAutorizacion(ACCION_VENTA_FL, 0);
                        if (trabajarVentaFueraLinea() && vistamodelo.hayTarjetaPagaTodo()) {
                            existeParametroVentaFueraLinea = false;
                            forzadoVentaFL = false;
                            SION.log(Modulo.VENTA, "Parametro fuera de línea activo pero pago con tarjeta pagatodo encontrada, "
                                    + "procede todo el flujo en línea.", Level.INFO);
                        }

                        if (!trabajarVentaFueraLinea() || esVentaTA) {
                            abrirCajon();

                            //////consulta parametro de timed out 
                            validarComunicacionCentral();

                            if (hayComunicacion) {

                                SION.log(Modulo.VENTA, "Se detectó que existe comunicación, se procede a enviar petición de conciliación", Level.FINEST);
                                ///se genera numero de conciliacion en BD local 
                                conciliacion = generarConciliacion(esVentaTA);
                                //se valida folio de conciliacion recibido 
                                if (conciliacion != 0) {

                                    if (esVentaTA) {
                                        tipomov = 2;
                                        ///se genera folio de recarga 
                                        folioTa = folioTiempoAireDao.consultaFolioTA();
                                        SION.log(Modulo.VENTA, "Folio de venta Ta generado: " + folioTa, Level.INFO);
                                        //si es un folio valido se llena la peticion de venta de TA y se marca la venta 
                                        if (folioTiempoAireDao.seGeneroFolioTA()) {
                                            SION.log(Modulo.VENTA, "Código de venta TA generado correctamente se procede a llenar petición de venta TA", Level.FINEST);
                                            vistamodelo.llenarpeticionVentaTADto(montoRecibido, montoVenta, tipomov,
                                                    conciliacion, recargasTADVista.getCompania(), folioTa);
                                            SION.log(Modulo.VENTA, "Se procede a invocar método de marcado de venta TA en línea",
                                                    Level.INFO);

                                            marcarVenta(conciliacion, efectivoCambio, montoRecibido);///se intenta marca venta 
                                        } else {
                                            SION.log(Modulo.VENTA, "Folio de venta TA generado incorrectamente, se suspende venta TA", Level.SEVERE);
                                        }

                                    } else {///se llena peticion de venta gral y se marca la venta 
                                        vistamodelo.llenarPeticionVentaDto(montoRecibido, montoVenta, tipomov, conciliacion);

                                        SION.log(Modulo.VENTA, "Se procede a invocar método de marcado de venta en línea", Level.INFO);

                                        marcarVenta(conciliacion, efectivoCambio, montoRecibido);///se intenta marca venta 
                                    }

                                    ////timed out en la venta 
                                    //entra a este caso si en marcarVenta() ocurre un error de comunicacion 
                                    if (vistamodelo.hayTarjetaPagaTodo()) {
                                        if (vistamodelo.getErrorVenta() != 0) {
                                            SION.log(Modulo.VENTA, "Mostrando mensaje de central.", Level.INFO);
                                            vistamodelo.removerPagoPagaTodo();
                                            
                                            refrescarMontoVenta();
                                                
                                            limpiadoParcialPorErrorPagaTodo();

                                            if (vistamodelo.getForzarFueraLineaPAGATODO()) {
                                                SION.log(Modulo.VENTA, "Se procede a marcar la venta fuera de línea al no hallar comunicación", Level.INFO);
                                                aplicarVentaFueraDeLineaPagaTodo(montoVenta, 1, 2, 1, conciliacion, _usuarioAutoriza, estatusLectura);

                                                SION.log(Modulo.VENTA, "Comprobando estatus de la respuesta a la petición de venta fuera de línea, "
                                                        + " Venta fuera de línea exitosa? " + esVentaFueraLineaExitosa, Level.FINEST);
                                                ///si tuvo exito la venta FL se imprime ticket 
                                                SION.log(Modulo.VENTA, "Venta fuera de línea PAGA TODO exitosa?" + esVentaFueraLineaExitosa, Level.FINEST);
                                                if (esVentaFueraLineaExitosa) {
                                                    SION.log(Modulo.VENTA, "NO se imprime el ticket", Level.FINEST);
                                                } else {
                                                    if (vistamodelo.getDescErrorFueraLinea().contains("|")) {
                                                        final String mensaje = vistamodelo.getDescErrorFueraLinea(); //msj[0]; 
                                                        mostrarAlertaRetornaFoco(mensaje, TipoMensaje.ERROR, vista.getTablaVentas());
                                                    }
                                                }
                                            }

                                        }
                                    } else if (vistamodelo.getPeticionVentaCuponesDto().isEsVentaInicializadaMktc()
                                            && !vistamodelo.getPeticionVentaCuponesDto().isEsVentaSinMarketec()
                                            && !vistamodelo.hayPromociones(TiposDescuento.Redondeo)
                                            && !hayComunicacion) {
                                        //Eliminar los descuentos. 
                                        AdministraVentanas.mostrarAlerta(
                                                "Ocurrio un problema con la comunicacion, por lo que se perderán los descuentos y promociones", TipoMensaje.ERROR);

                                        vistamodelo.eliminarArticulosRedondeo();
                                        vistamodelo.eliminarDescuentosDeCupones(delegadoPresentadorCuponesEliminarCupones());
                                        vistamodelo.getPeticionVentaCuponesDto().setEsVentaSinMarketec(true);

                                    } else if (!hayComunicacion) {
                                        ///si es una venta gral se intenta marcar fuera de linea 
                                        if (!esVentaTA) {
                                            SION.log(Modulo.VENTA, "Se procede a marcar la venta fuera de línea al no hallar comunicación", Level.INFO);
                                            aplicarVentaFueraDeLinea(montoVenta, 1, 2, 1, conciliacion, _usuarioAutoriza, estatusLectura);

                                            SION.log(Modulo.VENTA, "Comprobando estatus de la respuesta a la petición de venta fuera de línea, "
                                                    + " Venta fuera de línea exitosa? " + esVentaFueraLineaExitosa, Level.FINEST);
                                            ///si tuvo exito la venta FL se imprime ticket 
                                            if (esVentaFueraLineaExitosa) {
                                                impresionTicketFueraLinea(efectivoCambio, montoRecibido);
                                            } else {
                                                if (!esVentaTA && !esVentaExitosa && !hayComunicacion) {
                                                    //si mensaje de error contiene | error de base de datos y corta cadena  
                                                    //si no contiene es q no paso una validacion en el cliente y se muestra msj completo 
                                                    if (vistamodelo.getDescErrorFueraLinea().contains("|")) {
                                                        String[] msj = vistamodelo.getDescErrorFueraLinea().substring(1).split("\\|");
                                                        final String mensaje = msj[0];
                                                        if (idPantalla == PANTALLA_CAPTURA) {
                                                            mostrarAlertaRetornaFoco(mensaje, TipoMensaje.ERROR, vista.getTablaVentas());
                                                        } else {
                                                            mostrarAlertaRetornaFoco(mensaje, TipoMensaje.ERROR, vista.getCampoMontoEfectivo());
                                                        }
                                                    } else {
                                                        if (idPantalla == PANTALLA_CAPTURA) {
                                                            mostrarAlertaRetornaFoco(vistamodelo.getDescErrorFueraLinea(), TipoMensaje.ERROR, vista.getTablaVentas());
                                                        } else {
                                                            mostrarAlertaRetornaFoco(vistamodelo.getDescErrorFueraLinea(), TipoMensaje.ERROR, vista.getCampoMontoEfectivo());
                                                        }
                                                    }
                                                }
                                            }
                                        } else {////venta de TA 
                                            //revisa si se genero folio d conciliacion 
                                            if (folioTiempoAireDao.seGeneroFolioTA()) {

                                                intentosVerificacionVentaTA = 0;
                                                //se consulta parametro para ver cuantas veces se intentara confirmar la recarga en BD central 
                                                SION.log(Modulo.VENTA, "Se procede a consultar parámetros de número de intentos de verificación de venta TA", Level.INFO);

                                                vistamodelo.llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(),
                                                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "intentosTA.sistema")),
                                                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "intentosTA.modulo")),
                                                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "intentosTA.submodulo")),
                                                        Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "intentosTA.configuracion")));
                                                if (vistamodelo.consultarParametro() != null)
                                                {
                                                    ///si codigo de parametro = 0 se consultara en central para confirmar la recarga 
                                                    if (vistamodelo.getCodigoErrorParametroOperacion() == 0)//if (parametroIntentosTA.getCodigoError() == 0) { 
                                                    {
                                                        int contadorIntentos = 1;
                                                        boolean seMarcoVentaTa = false;
                                                        long tiempo = 0;
                                                        /// 
                                                        //intentosVerificacionVentaTA = (int) parametroIntentosTA.getValorConfiguracion(); 
                                                        intentosVerificacionVentaTA = ((int) vistamodelo.getValorConfiguracionParametroOperacion());
                                                        SION.log(Modulo.VENTA, "Número de intentos recibidos de BD: " + intentosVerificacionVentaTA, Level.FINEST);

                                                        SION.log(Modulo.VENTA, "Se procede a consultar parametros de tiempo de espera para verificación de ventaTA", Level.INFO);
                                                        vistamodelo.llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "tiempoEntreIntentosTA.sistema")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "tiempoEntreIntentosTA.modulo")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "tiempoEntreIntentosTA.submodulo")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "tiempoEntreIntentosTA.configuracion")));

                                                        ///se consulta el tiempo que se tomara entra los intentos de confirmacion d recarga 
                                                        //RespuestaParametroBean parametroTiempoIntentosTA = new RespuestaParametroBean(); 
                                                        ///si codigo de parametro igual a cero se toma el tiempo q die la BD 
                                                        ///si no se establece en 5000 
                                                        if (vistamodelo.consultarParametro() != null) {//if (parametroTiempoIntentosTA != null) { 
                                                            if (vistamodelo.getCodigoErrorParametroOperacion() == 0) {//if (parametroTiempoIntentosTA.getCodigoError() == 0) { 
                                                                tiempo = (long) vistamodelo.getValorConfiguracionParametroOperacion();//(long)parametroTiempoIntentosTA.getValorConfiguracion(); 
                                                                SION.log(Modulo.VENTA, "Valor obtenido en seg. " + tiempo, Level.INFO);
                                                                tiempo = tiempo * 1000;
                                                            } else {
                                                                SION.log(Modulo.VENTA, "Se recibe parámetro " + vistamodelo.getCodigoErrorParametroOperacion() + ", se asignan 5 segundos", Level.INFO);
                                                                tiempo = 5000;
                                                            }

                                                            SION.log(Modulo.VENTA, "Intento: " + intentosVerificacionVentaTA, Level.FINEST);
                                                            ///comienzan los intentos de confirmacion d recarga 
                                                            do {
                                                                SION.log(Modulo.VENTA, "Intento " + contadorIntentos + "/" + intentosVerificacionVentaTA, Level.INFO);
                                                                //se llena peticion 
                                                                vistamodelo.llenarPeticionConfirmacion(conciliacion, 1, 2, 1);
                                                                //se obtiene codigo d respuesta 
                                                                codigoErrorFueraLinea = vistamodelo.respuestaTransaccionCentral();

                                                                SION.log(Modulo.VENTA, "Código de error recibido: " + codigoErrorFueraLinea, Level.INFO);
                                                                ///entra aqui si se recibio respuesta exitosa en respuestaTransaccionCentral() 
                                                                if (!vistamodelo.hayMarcacionFueraDeLinea()) {
                                                                    //revisa el codigo d error en la respuesta 
                                                                    if (codigoErrorFueraLinea == 0) {
                                                                        SION.log(Modulo.VENTA, "La venta se confirmó en: " + contadorIntentos + " intentos", Level.INFO);
                                                                        seMarcoVentaTa = true;
                                                                        esConfirmacionVentaTA = true;
                                                                    }
                                                                }

                                                                contadorIntentos++;
                                                                SION.log(Modulo.VENTA, "Se procede a aplicar delay", Level.FINEST);
                                                                Thread.sleep(tiempo);
                                                                SION.log(Modulo.VENTA, "Delay completado", Level.FINEST);

                                                            } while (contadorIntentos <= intentosVerificacionVentaTA && !seMarcoVentaTa);

                                                            //si se confirmo la recarga 
                                                            if (seMarcoVentaTa) {

                                                                //tiempoAireVista.limpiarImagenes(); 
                                                                vistamodelo.agregarObjetoSesionTA();

                                                                esVentaExitosa = true;
                                                                //se actualiza la transaccion 
                                                                vistamodelo.actualizarTransacciónLocal(conciliacion, 1, 2, 1, true, ACTUALIZACION_LOCAL);
                                                                //si la venta fue TA habilitar d enuevo campos d las otras formas de opago 
                                                                vista.habilitarCampoCantidadVales(false);
                                                                vista.habilitarCampoMontoVales(false);
                                                                vista.habilitarCampoMontoValesE(false);
                                                                vista.habilitarCampoMontoTarjeta(false);
                                                                //cambiarEtiquetaPago(0); 
                                                                vista.cambiarEstadoCampoImporteRecibido(true);
                                                                //VentaVista.campoImporteRecibido.setDisable(true); 
                                                                vista.setMontoRecibidoPantallaCaptura("");
                                                                //VentaVista.tabla.requestFocus(); 
                                                                ///se manda imprimir ticket 
                                                                final double cambio = efectivoCambio;
                                                                final double recibido = montoRecibido;
                                                                final long noConciliacion = conciliacion;
                                                                Platform.runLater(new Runnable() {

                                                                    @Override
                                                                    public void run() {

                                                                        impresionTicket(1, cambio, recibido, false, noConciliacion, true);
                                                                        if (vistamodelo.seCompletoTicketVenta() || vistamodelo.seCompletoTicketVentaTA()) {
                                                                            evaluarBloqueosVenta();
                                                                        }
                                                                    }
                                                                });

                                                            } else {
                                                                SION.log(Modulo.VENTA, "Se terminaron los intentos de verificación de venta TA, no se pudo comprobar que la venta se llevó a cabo", Level.FINEST);
                                                                vistamodelo.cancelarConciliacionVenta(conciliacion);
                                                                transaccionesPDSTADVista.inicializarStagePDSTAD();
                                                                
                                                            }
                                                        } else {
                                                            SION.log(Modulo.VENTA, "Instancia de RespuestaParametroBean nula, no se verificara la marcación de la venta TA", Level.SEVERE);
                                                        }
                                                    } else {///no se verifica la recarga 
                                                        SION.log(Modulo.VENTA, "Store de consulta de parametros regresa código erróneo, no se verificara la marcación de la venta TA", Level.WARNING);
                                                    }
                                                } else {
                                                    SION.log(Modulo.VENTA, "Instancia de RespuestaParametroBean nula, no se verificara la marcación de la venta TA", Level.SEVERE);
                                                }
                                            }
                                        }
                                    } else {
                                        //si fue un intento de recarga que regresa codigo error de servicio se muestra 
                                        if (esVentaTA && !esVentaExitosa) {
                                            if (vistamodelo.getDescErrorVentaTA() != null) {
                                                mostrarAlertaRetornaFoco(vistamodelo.getDescErrorVentaTA(), TipoMensaje.ERROR, vista.getTablaVentas());
                                            } else {
                                                transaccionesPDSTADVista.inicializarStagePDSTAD();
                                            }
                                        }
                                    }
                                } else {
                                    SION.log(Modulo.VENTA, "No se generó un número de conciliación válido, la venta fue cancelada", Level.SEVERE);
                                    if (idPantalla == PANTALLA_CAPTURA) {
                                        mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.errorBDLocal"), TipoMensaje.ERROR, vista.getTablaVentas());
                                    } else {
                                        mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.errorBDLocal"), TipoMensaje.ERROR, vista.getCampoMontoEfectivo());
                                    }
                                }
                            } else if (vistamodelo.hayTarjetaPagaTodo()) {
                                vistamodelo.removerPagoPagaTodo();
                                limpiadoParcialPorErrorPagaTodo();
                                SION.log(Modulo.VENTA, "Se detectó que no hay comunicación,"
                                        + " Con tarjetas PAGA TODO no es posible la venta fuera de línea.", Level.INFO);
                                mostrarAlertaRetornaFoco("Se detectó que no hay comunicación, Con tarjetas PAGA TODO no es posible la venta fuera de línea.",
                                        TipoMensaje.ERROR, vista.getCampoMontoEfectivo());
                            } else {//se marca fuera de linea por no detectarse comunicacion 

                                SION.log(Modulo.VENTA, "Se detectó que no existe comunicación, se procede a marcar la venta fuera de línea.", Level.INFO);

                                if (!esVentaTA) {
                                    esMarcadoFueraLineaDirecto = true;
                                    aplicarVentaFueraDeLinea(montoVenta, 1, 2, 4, 0, _usuarioAutoriza, estatusLectura);
                                } else if (vistamodelo.hayTarjetaPagaTodo()) {
                                    SION.log(Modulo.VENTA, "Se perdio la comunicación, Con tarjetas pagatodo no es posible la venta fuera de línea.", Level.INFO);
                                    mostrarAlertaRetornaFoco(
                                            SION.obtenerMensaje(Modulo.VENTA, "ventas.pagatodo.fueradelinea.perdida"), TipoMensaje.ERROR, vista.getCampoMontoTarjeta());
                                } else {
                                    transaccionesPDSTADVista.inicializarStagePDSTAD();
                                }
                                if (esVentaFueraLineaExitosa) {
                                    impresionTicketFueraLinea(efectivoCambio, montoRecibido);
                                }
                            }
                        } else if (vistamodelo.hayTarjetaPagaTodo()) {
                            SION.log(Modulo.VENTA, "Se detectó que se encuentra activado el parámetro de venta en fuera de línea,"
                                    + " Con tarjetas PAGA TODO no es posible la venta fuera de línea.", Level.INFO);
                            mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.pagatodo.fueradelinea.directo"),
                                    TipoMensaje.ERROR, vista.getCampoMontoTarjeta());
                        } else {//se marca fuera de linea por parametro activado 
                            SION.log(Modulo.VENTA, "Se detectó que se encuentra activado el parámetro de venta en fuera de línea,"
                                    + "se procede a marcar la venta", Level.INFO);

                            if (!esVentaTA) {
                                esMarcadoFueraLineaDirecto = true;
                                aplicarVentaFueraDeLinea(montoVenta, 1, 2, 4, 0, _usuarioAutoriza, estatusLectura);
                            } else {
                                transaccionesPDSTADVista.inicializarStagePDSTAD();
                            }
                            
                            if (esVentaFueraLineaExitosa) {
                                impresionTicketFueraLinea(efectivoCambio, montoRecibido);
                                validarBloqueosLocal(0);
                            }
                        }
                    } else {
                        SION.log(Modulo.VENTA, "El monto de la venta es menor al importe recibido, no se marca la venta", Level.FINEST);
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.montos"),
                                        TipoMensaje.ADVERTENCIA, vista.getCampoMontoEfectivo());
                                vistamodelo.borrarRegistrosValesEfectivo();
                            }
                        });
                    }
                } else {
                    SION.log(Modulo.VENTA, "No se obtuvo una IP válida para el equipo, la venta no se llevó a cabo", Level.INFO);
                    mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.ip"), TipoMensaje.ERROR, vista.getCampoMontoEfectivo());
                }
                return "ok";
            }
        };

        final Service servicio = new Service() {

            @Override
            protected Task createTask() {
                return tarea;
            }
        };

        servicio.stateProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                //TODO: Reset los cupones 
                if (arg2.equals(Worker.State.SUCCEEDED)) {
                    vistamodelo.resetHayTarjetaPagaTodo();
                    vistamodelo.getCtrlTarjetasDescuento().resetearTarjetasDescuento();

                    SION.log(Modulo.VENTA, "Tarea de marcado de venta concluyó sin errores. Venta exitosa? " + esVentaExitosa
                            + ", Venta Fuera de línea exitosa?" + esVentaFueraLineaExitosa, Level.INFO);

                    if (esVentaTA) {
                        limpiarComponentesTA();
                    }

                    vista.removerImagenLoading();

                    if (esVentaExitosa) {

                        guardarLineasCanceladas(0, false);

                        vista.getTablaVentas().setItems(vistamodelo.getListaArticulos());

                        SION.log(Modulo.VENTA, "Existe parámetro de creación de índices de artículos? " + VentaVista.existeParametroIndices, Level.INFO);
                        if (VentaVista.existeParametroIndices) {
                            vista.resetIndices();
                        }

                        guardarVentaDia();
                        if (!esVentaTA) {
                            guardarMetodosEntradaxArticulo();
                        }
                        limpiarVenta();

                    } else {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                vista.setFocusCampoImporteEfectivo();
                            }
                        });
                    }
                    esVentaExitosa = false;

                    if (esVentaFueraLineaExitosa) {

                        guardarLineasCanceladas(0, false);

                        vista.getTablaVentas().setItems(vistamodelo.getListaArticulos());
                        SION.log(Modulo.VENTA, "Existe parámetro de creación de índices de artículos? " + VentaVista.existeParametroIndices, Level.INFO);

                        if (VentaVista.existeParametroIndices) {
                            vista.resetIndices();
                        }
                        guardarVentaDia();
                        guardarMetodosEntradaxArticulo();
                        try {
                            vistamodelo.verificarCambioFL();
                        } catch (Exception e) {
                            SION.logearExcepcion(Modulo.VENTA, e, "Ocurrió un error al intentar registrar el método de entrada de los artículos: " + e.getMessage().toString());
                        }
                        limpiarVenta();
                    }
                    esVentaTA = false;
                    esVentaGral = true;

                    //hayVentaEfectivoEnProceso = false;//Terminó bien la venta 
                    hayVentaFLEnProceso = false;
                    tareaVentaEnEjecusion = false;
                    SION.log(Modulo.VENTA, "Desactivar venta en proceso (Terminó bien la venta)...", Level.INFO);
                } else if (arg2.equals(Worker.State.FAILED)) {
                    vistamodelo.resetHayTarjetaPagaTodo();
                    vistamodelo.getCtrlTarjetasDescuento().resetearTarjetasDescuento();
                    SION.log(Modulo.VENTA, "Tarea de marcado de venta terminó con una excepción: "
                            + getStackTrace(servicio.getException()) + ", Venta exitosa? " + esVentaExitosa
                            + ", Venta Fuera de línea exitosa?" + esVentaFueraLineaExitosa, Level.SEVERE);

                    if (esVentaTA) {
                        limpiarComponentesTA();
                    }
                    esVentaTA = false;
                    esVentaGral = true;

                    vista.removerImagenLoading();
                    vista.getTablaVentas().setItems(vistamodelo.getListaArticulos());
                    limpiarVenta();
                    esVentaExitosa = false;

                    //hayVentaEfectivoEnProceso = false;//Terminó mal la venta 
                    hayVentaFLEnProceso = false;
                    SION.log(Modulo.VENTA, "Desactivar venta en proceso (Terminó mal la venta)...", Level.INFO);
                    tareaVentaEnEjecusion = false;
                    //servicio.getException().printStackTrace(); 
                } else if (arg2.equals(Worker.State.CANCELLED)) {
                    tareaVentaEnEjecusion = false;
                    //hayVentaEfectivoEnProceso = false;//Terminó debido a que el proceso fue cancelado ( Worker.cancel() ) 
                    hayVentaFLEnProceso = false;
                    SION.log(Modulo.VENTA, "Desactivar venta en proceso (Terminó mal la venta) debido a que el proceso fue cancelado ( Worker.cancel() )", Level.INFO);
                }
            }
        });

        SION.log(Modulo.VENTA, "Entrando a método de llenado de petición de venta", Level.INFO);

        SION.log(Modulo.VENTA, "INICIA proceso de registro de venta...", Level.INFO);
        servicio.start();

    }

    public void validarBloqueosLocal(long _conciliacion) {
        SION.log(Modulo.VENTA, "Validación de bloqueos locales deshabilitada...", Level.INFO);
    }

    private void guardarLineasCanceladas(int cantidadVentas, boolean esCancelacion) {
        if (autorizador.equals("")) {
            autorizador = "0";
        }
        SION.log(Modulo.VENTA, "Verificando si existen líneas de artículos cancelados", Level.INFO);
        ///se llena lista de articulos que sufrieron alguna cancelacion 
        //vistamodelo.llenarListaArticulosCancelados(); 
        ///se llena peticion de articulos caneclados para insertar en local 
        if (esCancelacion) {
            vistamodelo.llenarPeticionArticulosCancelados(
                    VistaBarraUsuario.getSesion().getUsuarioId(), Long.parseLong(autorizador),
                    0,//transaccion 
                    vistamodelo.obtenerFecha().substring(0, 10),
                    0, cantidadVentas);
            vistamodelo.insertarArticulosCancelados();
        } else {
            if (vistamodelo.getCantidadLineasCanceladas() > 0) {
                vistamodelo.llenarPeticionArticulosCancelados(
                        VistaBarraUsuario.getSesion().getUsuarioId(), Long.parseLong(autorizador),
                        0,//transaccion 
                        vistamodelo.obtenerFecha().substring(0, 10),
                        vistamodelo.getCantidadLineasCanceladas(), cantidadVentas);
                vistamodelo.insertarArticulosCancelados();
            } else {
                SION.log(Modulo.VENTA, "No existen registros de líneas canceladas, se continùa con la operación", Level.INFO);
            }
        }
    }

    private void guardarMetodosEntradaxArticulo() {
        ///se consulta parametro de insercion de venta del dia 
        SION.log(Modulo.VENTA, "Se procede a consultar parámetro de operación de inserción de métodos de entrada de artículos", Level.INFO);
        //RespuestaParametroBean parametroRegistroMetodoEntrada = new RespuestaParametroBean(); 
        //parametroRegistroMetodoEntrada = utileriasVenta.consultaParametro(UtileriasVenta.Accion.REGISTRO_METODO_ENTRADA, Modulo.VENTA, Modulo.VENTA); 
        vistamodelo.llenarPeticionParametro(
                VistaBarraUsuario.getSesion().getPaisId(),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.metodos.entrada.sistema")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.metodos.entrada.modulo")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.metodos.entrada.submodulo")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.metodos.entrada.configuracion")));

        if (vistamodelo.consultarParametro() != null) { //if (parametroRegistroMetodoEntrada != null) { 
            if (vistamodelo.getCodigoErrorParametroOperacion() == 0) { //if (parametroRegistroMetodoEntrada.getCodigoError() == 0) { 
                if (vistamodelo.getValorConfiguracionParametroOperacion() == 1.0D) { //if (parametroRegistroMetodoEntrada.getValorConfiguracion() == 1) { 
                    SION.log(Modulo.VENTA, "Parámetro de inserción de métodos de entrada de artículos activado. ", Level.INFO);
                    vistamodelo.llenarPeticionMetodosEntrada();
                    vistamodelo.insertarMetodosEntrada();
                } else {
                    SION.log(Modulo.VENTA, "Parámetro de de métodos de entrada de artículos desactivado", Level.WARNING);
                }
            } else {
                SION.log(Modulo.VENTA, "Store de consulta de parametros regresa código erróneo", Level.INFO);
            }
        } else {
            SION.log(Modulo.VENTA, "Instancia de RespuestaParametroBean nula", Level.SEVERE);
        }
    }

    private void guardarVentaDia() {
        ///se consulta parametro de insercion de venta del dia 
        SION.log(Modulo.VENTA, "Se procede a consultar parámetro de operación de inserción de la venta del día", Level.INFO);
        vistamodelo.llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaDia.sistema")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaDia.modulo")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaDia.submodulo")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaDia.configuracion")));
        //RespuestaParametroBean parametroVentaDia = new RespuestaParametroBean(); 
        //parametroVentaDia = utileriasVenta.consultaParametro(UtileriasVenta.Accion.VENTA_DIA, Modulo.REPORTES_VENTA, Modulo.VENTA); 
        if (vistamodelo.consultarParametro() != null) { //if (parametroVentaDia != null) { 
            if (vistamodelo.getCodigoErrorParametroOperacion() == 0) { //if (parametroVentaDia.getCodigoError() == 0) { 
                if (vistamodelo.getValorConfiguracionParametroOperacion() == 1.0D) { //if (parametroVentaDia.getValorConfiguracion() == 1) { 
                    SION.log(Modulo.VENTA, "Parámetro de inserción de venta del día activado. ", Level.INFO);
                    vistamodelo.llenarPeticionVentaDelDia();
                    vistamodelo.insertarVentaDelDia();
                } else {
                    SION.log(Modulo.VENTA, "Parámetro de inserción de venta del día desactivado", Level.WARNING);
                }
            } else {
                SION.log(Modulo.VENTA, "Store de consulta de parametros regresa código erróneo", Level.INFO);
            }
        } else {
            SION.log(Modulo.VENTA, "Instancia de RespuestaParametroBean nula", Level.SEVERE);
        }
    }

    public void cancelarVenta() {
        configuracionCupones.resetRepositorio();

        SION.log(Modulo.VENTA, "Entrando a método de cancelación de venta", Level.INFO);
        boolean existenTarjetas = false;
        vistamodelo.getCtrlTarjetasDescuento().resetearTarjetasDescuento();
        if (idPantalla == PANTALLA_CAPTURA) {

            guardarLineasCanceladas(1, true);

            if (vistamodelo.getListaPagosTarjeta().size() > 0) {

                existenTarjetas = true;
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        SION.log(Modulo.VENTA, "Se procede a lanzar reversos después de revisar que se tienen registros de "
                                + vistamodelo.getListaPagosTarjeta().size() + " tarjetas", Level.INFO);
                        vistamodelo.reversarPagoTarjeta();//***?//Verificar por que no se hacen los reversos 

                        SION.log(Modulo.VENTA, "Reverso aplicado", Level.INFO);
                        vista.setMontoRecibidoPantallaCaptura("");
                        vista.setMontoCambio("0.00");
                        vista.setMontoRestante("0.00");
                        vista.setMontoVenta("0.00");
                        SION.log(Modulo.VENTA, "Limpiando Navegador de tarjetas", Level.INFO);

                        vista.setValorEtiquetaCantidadTarjetasRecibidas("0");
                        vista.setValorEtiquetaCantidadValesERecibidos("0");
                        vista.setValorMontoCantidadTarjetasRecibidas("0.00");
                        vista.setValorMontoCantidadValesERecibidos("0.00");

                        vista.setDescripcionTarjetaBancaria("");
                        vista.setDescripcionNoTarjeta("");
                        vista.setDescripcionImporteTarjeta("");
                        vista.setDescripcionAutorizada("");
                    }
                });
            }

            try {
                if (vistamodelo.getTamanioListaArticulos() > 0 || esVentaTA) {
                    if (esVentaTA) {
                        vista.getTablaVentas().setItems(vistamodelo.getListaArticulos());

                        vistamodelo.getListaArticulosTA().clear();
                        vista.cambiarComponentesTAaVenta(0);
                        recargasTADVista.limpiarCamposTexto();
                        RecargasTADBinding.numero = 0;
                        RecargasTADBinding.numeroc = 0;
                        recargasTADVista.removerEfectosImagenes();
                    }

                    if (!existenTarjetas) {
                        vistamodelo.limpiarListasBean();
                        vistamodelo.limpiarListasDto();
                    }

                    montoAcarreado = 0;

                    vista.setMontoRecibidoPantallaCaptura("");
                    vista.setMontoCambio("0.00");
                    vista.setMontoRestante("0.00");
                    vista.setMontoVenta("0.00");

                    mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.cancelada"), TipoMensaje.INFO, vista.getTablaVentas());
                }

                esVentaTA = false;
                esVentaGral = true;
                vista.setFocusTablaVentas();

            } catch (Exception e) {
                e.printStackTrace();
            } catch (Error e) {
                e.printStackTrace();
            }

        } else if (idPantalla == PANTALLA_VENTA) {

            if (!vista.getMontoEfectivo().isEmpty()) {
                montoAcarreado = Double.parseDouble(vista.getMontoEfectivo());

                if (esVentaTA) {


                    vista.setMontoRestante("0.00");
                    vista.setMontoRecibidoPantallaCaptura("");
                    vista.setMontoCambio("0.00");

                    vistamodelo.limpiarListasDto();
                    vistamodelo.limpiarListasBean();
                    montoAcarreado = 0;

                    vista.habilitarCampoCantidadVales(false);
                    vista.habilitarCampoMontoVales(false);
                    vista.habilitarCampoMontoValesE(false);
                    vista.habilitarCampoMontoTarjeta(false);

                    vista.setMontoVenta("0.00");


                }

                try {
                    if (Double.parseDouble(vista.getMontoCambio()) <= 0) {
                        vista.setMontoCambio("0.00");
                        vista.setMontoRestante("0.00");
                    }
                } catch (NumberFormatException e) {
                    vista.setMontoCambio("0.00");
                    vista.setMontoRestante("0.00");

                }
            } else {
                montoAcarreado = 0;

            }

            vista.setMontoVales("");
            vista.setMontoValesE("");
            vista.setMontoTarjetas("");
            vista.setCantidadVales("");

            cambiarCajasComponentes(PANTALLA_CAPTURA, vista.getPanelVentas());

            idEscape = 0;

            esVentaTA = false;
            esVentaGral = true;

        }

        if (vistamodelo.hayCupones()) {
            presentadorCupones.cancelaVenta(vistamodelo.getPeticionVentaCuponesDto());
        }

        vistamodelo.getListaPagosTarjeta().clear();

        vistamodelo.limpiarListasBean();
        vistamodelo.limpiarListasDto();

        vista.setFocusTablaVentas();
        esVentaPagaTdo = false;

        vistamodelo.setPeticionVentaCuponesDto(new DatosCuponesVentaDto());
    }

    ///////////////////////////////////////////////////////////eventos del panel 
    public void agregarEventosEscape(BorderPane panel) {
        if (eventoEscapeVenta != null) {
            SION.log(Modulo.VENTA, "Agregando eventos de la tecla escape al panel de venta", Level.INFO);
            panel.addEventFilter(KeyEvent.KEY_PRESSED, eventoEscapeVenta);

        }
    }

    public void removerEventosEscape(BorderPane panel) {
        if (eventoEscapeVenta != null) {
            SION.log(Modulo.VENTA, "Removiendo eventos de la tecla escape al panel de venta", Level.INFO);
            panel.removeEventFilter(KeyEvent.KEY_PRESSED, eventoEscapeVenta);

        }
    }

    public void agregarEventosCaptura(BorderPane panel) {

        if (eventosTeclasVenta != null && eventosCapturaVenta != null) {
            SION.log(Modulo.VENTA, "Agregando eventos de las teclas f,f4,f8,f9,f10,enter y de captura de dígitos al panel de venta", Level.SEVERE);
            panel.addEventFilter(KeyEvent.KEY_PRESSED, eventosTeclasVenta);
            panel.addEventFilter(KeyEvent.KEY_PRESSED, eventosCapturaVenta);

        }
    }

    public void removerEventosCaptura(BorderPane panel) {

        if (eventosTeclasVenta != null && eventosCapturaVenta != null) {
            SION.log(Modulo.VENTA, "Removiendo eventos de las teclas f,f4,f8,f9,f10,enter y de captura de dígitos al panel de venta", Level.SEVERE);
            panel.removeEventFilter(KeyEvent.KEY_PRESSED, eventosTeclasVenta);
            panel.removeEventFilter(KeyEvent.KEY_PRESSED, eventosCapturaVenta);

        }
    }

    public void cancelarVentaCaptura(final TableView tabla, final TextField campoImporteRecibido,
            final TextField campoCambio, final Label etiquetamontoVenta, final TextField campoMontoRestante) {

        if (campoImporteRecibido.isFocused()) {//focus en campo del importe 

            campoImporteRecibido.textProperty().set("");
            campoCambio.textProperty().set("0.00");
            campoMontoRestante.textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta() - getImporteRecibido()));
            campoImporteRecibido.setDisable(true);
            tabla.requestFocus();
        } else if (tabla.isFocused()) {
            consultarParametrosAutorizacion(ACCION_CANCELACION, 0);
        } else {
            tabla.requestFocus();
        }
    }

    private void mostrarAlertaRetornaFoco(final String mensaje, final TipoMensaje tipo, final Control control) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                AdministraVentanas.mostrarAlertaRetornaFoco(mensaje, tipo, control);
            }
        });
    }
    IComandoCupon redondeo;
    private Long ultimaTimeStamp = 0L;
    private final StringBuffer codigoBarras = new StringBuffer();

    public void eventosPanelVenta(final BorderPane panel, final TableView tabla, final Node nodo, final TextField campoImporteRecibido,
            final TextField campoCambio, final Label etiquetamontoVenta, final TextField campoBuscador,
            final TextField campoMontoRestante) {

        //repositorioCupones = new MarketecRepositorioCupones();//new FakeRepositorioCupones(); 
        vistaGralCupones = new VistaCuponesMarketec();
        presentadorCupones = new PresentadorCuponesMarketec(vistaGralCupones, configuracionCupones);
        vistaGralCupones.setPresenter(presentadorCupones);

        //tarejetas de descuento 
        campoImporteRecibido.focusedProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> obs, Boolean old, Boolean nuevo) {
                if (nuevo && vistamodelo.getCtrlTarjetasDescuento().isNecesitaValidarArticulos()) {//Gano foco 
                    //SION.log(Modulo.VENTA, "Foco activo...", Level.INFO); 
                    Platform.runLater(new Runnable() {

                        public void run() {
                            vista.cargarLoading();
                        }
                    });
                    Service servicio = new Service() {

                        public Task createTask() {
                            return new Task() {

                                public Object call() {
                                    try {
                                        vistamodelo.actualizaArticulosXValidar(vistamodelo.getListaArticulos());
                                        RespuestaValidaDescuentoArticulosDto respValidacion =
                                                vistamodelo.getCtrlTarjetasDescuento().validarDescuentoArticulos(null);

                                        if (respValidacion != null) {
                                            vistamodelo.actualizarListaArticulos(vistamodelo.getCtrlTarjetasDescuento().getArticulosValidados());
                                            if (respValidacion.getErrorId() == 0) {
                                                SION.log(Modulo.VENTA, "Codigo de error 0", Level.INFO);
                                            } else {
                                                SION.log(Modulo.VENTA, "Respuesta de error: "
                                                        + respValidacion.getErrorId() + ", " + respValidacion.getErrorMensaje(), Level.INFO);
                                                mostrarAlertaRetornaFoco(respValidacion.getErrorMensaje(), TipoMensaje.ERROR,
                                                        campoImporteRecibido);
                                            }
                                            vistamodelo.getCtrlTarjetasDescuento().setNecesitaValidarArticulos(false);
                                            refrescarMontoVenta();
                                        } else {
                                            SION.log(Modulo.VENTA, "Respuesta nula.", Level.INFO);
                                            mostrarAlertaRetornaFoco("Ocurrio un problema al consultar los descuentos para los artículos.",
                                                    TipoMensaje.ERROR, campoImporteRecibido);
                                        }
                                    } catch (Exception e) {
                                        SION.logearExcepcion(Modulo.VENTA, e, mensajeError);
                                    }
                                    return "OK";
                                }
                            };
                        }
                    };
                    servicio.stateProperty().addListener(new ChangeListener() {

                        public void changed(ObservableValue obv, Object oldV, Object newV) {
                            if (newV == Worker.State.SUCCEEDED || newV == Worker.State.FAILED) {
                                Platform.runLater(new Runnable() {

                                    public void run() {
                                        //SION.log(Modulo.VENTA, ">>>******", Level.INFO); 
                                        vista.removerImagenLoading();
                                        campoImporteRecibido.requestFocus();
                                    }
                                });
                            }
                        }
                    });

                    servicio.start();

                }

            }
        });

        eventoEscapeVenta = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent ke) {

                if (escape.match(ke)) {
                    if (idPantalla == PANTALLA_CAPTURA) {
                        SION.log(Modulo.VENTA, "Escape presionado, se procede a cancelar venta", Level.INFO);
                        cancelarVentaCaptura(tabla, campoImporteRecibido, campoCambio, etiquetamontoVenta, campoMontoRestante);
                        esEscapeConsumido = false;
                    }
                }

            }
        };



        eventosTeclasVenta = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent ke) {

                if (enter.match(ke) || f10.match(ke)) {

                    ke.consume();

                    if (f10.match(ke)) {
                        SION.log(Modulo.VENTA, "f10 presionado", Level.INFO);
                    }


                    if (idPantalla == PANTALLA_CAPTURA) {
                        if (campoImporteRecibido.isFocused()) {//focus en el campo del importe 

                            //vista.requestFocusLoading(); 
                            SION.log(Modulo.VENTA, "Botón enter presinado sobre campo de importe en efectivo en pantalla de captura", Level.INFO);
                            SION.log(Modulo.VENTA, "campoImporteRecibido: " + campoImporteRecibido.textProperty().get(), Level.INFO);
                            SION.log(Modulo.VENTA, "etiquetamontoVenta: " + etiquetamontoVenta.textProperty().get(), Level.INFO);
                            if (!campoImporteRecibido.textProperty().get().isEmpty()) {//si no esta vacio 

                                try {
                                    montoRecibido = Double.parseDouble(campoImporteRecibido.textProperty().get());
                                } catch (Exception e) {
                                    montoRecibido = 0;
                                }

                                SION.log(Modulo.VENTA, "montoRecibido: " + montoRecibido, Level.INFO);
                                if (montoRecibido >= Double.parseDouble(etiquetamontoVenta.textProperty().get())) {  //importe recibido NO vacio             
                                    ////consultarParametrosAutorizacion(ACCION_VENTA, 4);  // (Enter final) Validar mostrar promociones    
                                    SION.log(Modulo.VENTA, "montoRecibido >= etiquetamontoVenta", Level.INFO);

                                    if (esVentaTA) {
                                        SION.log(Modulo.VENTA, "Promociones no válidas para ventas de Tiempo Aire...", Level.INFO);
                                        consultarParametrosAutorizacion(ACCION_VENTA, 4);
                                    } else if (repositorioRedondeo.esPromocionesActivas()) {
                                        SION.log(Modulo.VENTA, "Promociones activas...", Level.INFO);
                                        mostrarPopWindowRedondeo(campoImporteRecibido,
                                                getTaskvalidaImporte(campoImporteRecibido, etiquetamontoVenta),
                                                getTaskvalidaImporte(campoImporteRecibido, etiquetamontoVenta));
                                    } else {
                                        SION.log(Modulo.VENTA, "Promociones inactivas...", Level.INFO);
                                        consultarParametrosAutorizacion(ACCION_VENTA, 4);
                                    }

                                } else {
                                    SION.log(Modulo.VENTA, "montoRecibido < etiquetamontoVenta", Level.INFO);
                                    if (!esVentaTA) {
                                        ////intercambiarPantalla(campoImporteRecibido, etiquetamontoVenta);// (Enter1) Validar mostrar promociones 
                                        //Importe recibo vacio 
                                        if (repositorioRedondeo.esPromocionesActivas()) {
                                            SION.log(Modulo.VENTA, "Promociones activas...", Level.INFO);
                                            mostrarPopWindowRedondeo(campoImporteRecibido,
                                                    getTaskvalidaImporte(campoImporteRecibido, etiquetamontoVenta),
                                                    getTaskvalidaImporte(campoImporteRecibido, etiquetamontoVenta));
                                        } else {
                                            SION.log(Modulo.VENTA, "Promociones inactivas...", Level.INFO);
                                            intercambiarPantalla(campoImporteRecibido, etiquetamontoVenta);
                                        }

                                    }
                                }
                            } else {
                                SION.log(Modulo.VENTA, "montoRecibido vacio", Level.INFO);
                                if (!esVentaTA) {
                                    //// intercambiarPantalla(campoImporteRecibido, etiquetamontoVenta);// (Enter2) Validar mostrar promociones OK505 
                                    if (repositorioRedondeo.esPromocionesActivas()) {
                                        SION.log(Modulo.VENTA, "Promociones activas...", Level.INFO);
                                        mostrarPopWindowRedondeo(campoImporteRecibido,
                                                getTaskvalidaImporte(campoImporteRecibido, etiquetamontoVenta),
                                                getTaskvalidaImporte(campoImporteRecibido, etiquetamontoVenta));
                                    } else {
                                        SION.log(Modulo.VENTA, "Promociones inactivas...", Level.INFO);
                                        intercambiarPantalla(campoImporteRecibido, etiquetamontoVenta);
                                    }


                                }
                            }
                        } else if (!esVentaTA) {
                            hayVentanaError = false;
                            //SION.log(Modulo.VENTA, ">" + vista.campoBuscador.isFocused()+">" + vista.getTablaVentas().isFocused(), Level.INFO); 
                            validarArticuloEscaneado(tabla, nodo);
                        }

                        if (esVentaTA) {
                            //SION.log(Modulo.VENTA, ">>>*******", Level.INFO); 
                            campoImporteRecibido.setDisable(false);
                            campoImporteRecibido.requestFocus();
                        }
                    }
                } else if (f9.match(ke)) {
                    SION.log(Modulo.VENTA, "f9 Presionado", Level.INFO);
                    Platform.runLater(new Runnable() {

                        public void run() {
                            vista.cargarLoading();
                        }
                    });
                    final Service servicio = new Service() {

                        @Override
                        public Task createTask() {
                            return new Task() {

                                public Object call() {
                                    try {
                                        vistamodelo.actualizaArticulosXValidar(vistamodelo.getListaArticulos());
                                        RespuestaValidaDescuentoArticulosDto respValidacion =
                                                vistamodelo.getCtrlTarjetasDescuento().validarDescuentoArticulos(null);

                                        if (respValidacion != null) {
                                            vistamodelo.actualizarListaArticulos(vistamodelo.getCtrlTarjetasDescuento().getArticulosValidados());
                                            if (respValidacion.getErrorId() == 0) {
                                                SION.log(Modulo.VENTA, "Codigo de error 0", Level.INFO);
                                            } else {
                                                SION.log(Modulo.VENTA, "Respuesta de error: "
                                                        + respValidacion.getErrorId() + ", " + respValidacion.getErrorMensaje(), Level.INFO);
                                                mostrarAlertaRetornaFoco(respValidacion.getErrorMensaje(), TipoMensaje.ERROR,
                                                        campoImporteRecibido);
                                            }
                                            vistamodelo.getCtrlTarjetasDescuento().setNecesitaValidarArticulos(false);
                                            refrescarMontoVenta();
                                        } else {
                                            SION.log(Modulo.VENTA, "Respuesta nula.", Level.INFO);
                                            mostrarAlertaRetornaFoco("Ocurrio un problema al consultar los desceuntos para los artículos.",
                                                    TipoMensaje.ERROR, campoImporteRecibido);
                                        }
                                    } catch (Exception e) {
                                        SION.logearExcepcion(Modulo.VENTA, e, mensajeError);
                                    }

                                    return "OK";
                                }
                            };
                        }
                    ;
                    }; 
                    servicio.stateProperty().addListener(new ChangeListener() {

                        public void changed(ObservableValue obs, Object oldV, Object newV) {
                            if (newV == Worker.State.SUCCEEDED || newV == Worker.State.FAILED) {
                                Platform.runLater(new Runnable() {

                                    public void run() {
                                        vista.removerImagenLoading();
                                    }
                                });

                                if (idPantalla == PANTALLA_CAPTURA && !esVentaTA) {
                                    if (campoImporteRecibido.isFocused()) {//foucs en el campo del importe 
                                        ////intercambiarPantalla(campoImporteRecibido, etiquetamontoVenta);// (f9) Validar mostrar promociones 
                                        mostrarPopWindowRedondeo(campoImporteRecibido,
                                                getTaskvalidaImporte(campoImporteRecibido, etiquetamontoVenta),
                                                getTaskvalidaImporte(campoImporteRecibido, etiquetamontoVenta));
                                    } else {
                                        if (vistamodelo.getImporteVenta() > 0) {
                                            cambiarCajasComponentes(PANTALLA_VENTA, vista.getPanelVentas());
                                        } else {
                                            mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.importeCero"), TipoMensaje.ADVERTENCIA, tabla);
                                        }
                                    }
                                }
                            }
                        }
                    });

                    servicio.start();

                } else if (f3.match(ke)) {
                    SION.log(Modulo.VENTA, "F3 detectado...", Level.INFO);
                    if (vistamodelo.getTamanioListaArticulos() < 1 && idPantalla == PANTALLA_CAPTURA && vistamodelo.getTamanioListaArticulosTA() == 0) {
                        SION.log(Modulo.VENTA, "Se intenta abrir stage de venta de telefonicas...", Level.INFO);
                        recargasTADVista.inicializarStageTA();
                    } else {
                        SION.log(Modulo.VENTA, "no inicia ta - " + vistamodelo.getTamanioListaArticulosTA() + " - " + vistamodelo.getTamanioListaArticulos() + " - " + PANTALLA_CAPTURA, Level.INFO);
                    }
                } else if (f4.match(ke)) {
                    //if (ventaVistamodelo.getTamanioListaArticulos()<1 && idPantalla==0) { 
                    if (!esVentaTA) {
                        try {
                            verificadorVista.incializarStage();
                        } catch (Exception e) {
                            StringWriter sw = new StringWriter();
                            e.printStackTrace(new PrintWriter(sw));
                            String exceptionAsString = sw.toString();
                            SION.logearExcepcion(Modulo.VENTA, e, "Excepcion : " + exceptionAsString);
                        } catch (Error e) {
                            StringWriter sw = new StringWriter();
                            e.printStackTrace(new PrintWriter(sw));
                            String exceptionAsString = sw.toString();
                            SION.log(Modulo.VENTA, "error: " + exceptionAsString, Level.SEVERE);
                        }
                    }
                    //} 
                } else if (f8.match(ke)) {
                    if (idPantalla == PANTALLA_CAPTURA) {
                        if (campoBuscador.isFocused()) {//si el focus esta en el buscador 
                            if (!"".equals(etiquetamontoVenta.textProperty().getValue())) {
                                try {
                                    if (vistamodelo.getTamanioListaArticulos() > 0
                                            && Double.parseDouble(etiquetamontoVenta.textProperty().getValue()) > 0) {

                                        campoImporteRecibido.setDisable(false);
                                        campoImporteRecibido.requestFocus();
                                    }
                                } catch (NumberFormatException e) {
                                    SION.logearExcepcion(Modulo.VENTA, e, "");
                                }
                            }
                        } else {
                            if (!vista.getCampoMontoEfectivo().isFocused()
                                    && !vista.getCampoCantidadVales().isFocused()
                                    && !vista.getCampoMontoVales().isFocused()
                                    && !vista.getCampoMontoValesE().isFocused()
                                    && !vista.getCampoMontoTarjeta().isFocused()) {
                                if (!"".equals(etiquetamontoVenta.textProperty().getValue())) {
                                    try {
                                        if (vistamodelo.getTamanioListaArticulos() > 0
                                                && Double.parseDouble(etiquetamontoVenta.textProperty().getValue()) > 0) {

                                            campoImporteRecibido.setDisable(false);
                                            campoImporteRecibido.requestFocus();
                                        }
                                    } catch (NumberFormatException e) {
                                        SION.logearExcepcion(Modulo.VENTA, e, "");
                                    }
                                }
                                if (esVentaTA) {
                                    //SION.log(Modulo.VENTA, ">>>*", Level.INFO); 
                                    campoImporteRecibido.setDisable(false);
                                    campoImporteRecibido.requestFocus();
                                }
                            }
                        }
                    }
                } else if (f7.match(ke)) {
                    SION.log(Modulo.VENTA, "f7 Presionado", Level.INFO);
                    operacionesTarjetaDescuento();
                } else if (f6.match(ke)) {
                    SION.log(Modulo.VENTA, "f6 Presionado", Level.INFO);
                    if (cuponesActivos.getValue()) {
                        operacionesCuponesDescuento();
                    }
                } else if (f5.match(ke)) {
                    //try { 
                    //AdministraVentanas.mostrarEscena("venta.reimpresiones"); 
                    //AdministraVentanas.mostrarEscena("caja.depositosanclada.principal"); 
                    //CobroTarjetaVista ctv = new CobroTarjetaVista(VentaBindings.this, vistamodelo, vista,utileriasVenta);  
                    //ctv.getStageCobroTarjeta(null); 
                    //cobroTarjetaVista.getStageCobroTarjeta(null); 
                    //} catch (Exception e) { 
                    //} 
                } else if (f2.match(ke)) {
                    if (idPantalla == PANTALLA_CAPTURA && !esVentaTA) {

                        if (campoImporteRecibido.isFocused()) {//focus en el campo del importe                                                         
                            campoImporteRecibido.setDisable(true);
                            campoBuscador.setDisable(false);
                            campoBuscador.requestFocus();
                            if (vistamodelo.hayPromociones(TiposDescuento.Redondeo)) {
                                mostrarAlertaRetornaFoco(
                                        "Al agregar un articulo a la venta se eliminará el artículo de la promoción previamente seleccionado.",
                                        TipoMensaje.ADVERTENCIA, campoBuscador);
                            }
                        } else if (campoBuscador.isFocused()) {//focus en el buscador 
                            tabla.requestFocus();
                        } else {
                            campoBuscador.setDisable(false);
                            campoBuscador.requestFocus();
                            if (vistamodelo.hayPromociones(TiposDescuento.Redondeo)) {
                                mostrarAlertaRetornaFoco(
                                        "Al agregar un articulo a la venta se eliminará el artículo de la promoción previamente seleccionado.",
                                        TipoMensaje.ADVERTENCIA, campoBuscador);
                            }
                        }
                    }
                }


            }
        };

        eventosCapturaVenta = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent ke) {

                if (!campoBuscador.isFocused() && !campoImporteRecibido.isFocused()) {

                    matcherCadena = patronCadena.matcher(ke.getText());
                    if (matcherCadena.find()) {
                        cadena += ke.getText();

                    }
                }
            }
        };


        tabla.focusedProperty().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable arg0) {
                if (tabla.isFocused()) {
                    campoBuscador.setDisable(true);
                }
            }
        });
    }

    private Task getTaskvalidaImporte(final TextField campoImporteRecibido, final Label etiquetamontoVenta) {
        return new Task() {

            @Override
            protected Object call() throws Exception {
                try {
                    if (Double.parseDouble("0" + campoImporteRecibido.textProperty().get()) >= vistamodelo.getImporteVenta()) {
                        consultarParametrosAutorizacion(ACCION_VENTA, 4);
                        SION.log(Modulo.VENTA, "TaskvalidaImporte:::consultarParametrosAutorizacion(ACCION_VENTA, 4);", Level.INFO);
                    } else {
                        intercambiarPantalla(campoImporteRecibido, etiquetamontoVenta);
                        SION.log(Modulo.VENTA, "intercambiarPantalla(campoImporteRecibido, etiquetamontoVenta);", Level.INFO);
                    }
                } catch (Exception e) {
                    SION.logearExcepcion(Modulo.VENTA, e, "intercambiarPantalla(campoImporteRecibido, etiquetamontoVenta);" + getStackTrace(e));
                }

                return "OK";
            }
        };
    }

    public void mostrarPopWindowRedondeo(
            final TextField campoImporteRecibido,
            final Task tareaRechazado, final Task tareaAceptado) {
        SION.log(Modulo.VENTA, "mostrarPopWindowRedondeo...", Level.INFO);

        vistaRedondeo.resetVista();//vistaRedondeo = new VistaPromociones(repositorioRedondeo);        
        presentadorRedondeo = new PresentadorPromociones(vistaRedondeo, repositorioRedondeo);

        vistaRedondeo.setPresentador(presentadorRedondeo);

        if (repositorioRedondeo.esPromocionesActivas()) {
            SION.log(Modulo.VENTA, "Promociones activas (Redondeo)...", Level.INFO);

            SION.log(Modulo.VENTA, "Valida si no hay promociones y si es una venta candidata...", Level.INFO);

            if (!vistamodelo.hayPromociones(TiposDescuento.Redondeo)) {
                SION.log(Modulo.VENTA, "No hay promociones asignadas a la venta", Level.INFO);
                if (presentadorRedondeo.esVentaCandidata(vistamodelo.getImporteVenta())) {
                    SION.log(Modulo.VENTA, "Venta aplica para promocion de redondeo", Level.INFO);
                    AdministraVentanas.mostrarPopup("promociones", (Node) presentadorRedondeo.getVistaPromociones());
                    presentadorRedondeo.iniciarVista();

                    vistaRedondeo.setImporteTotal(vistamodelo.getImporteVenta());
                    vistaRedondeo.setImporteRecibido(Double.parseDouble("0" + campoImporteRecibido.textProperty().get()));

                    presentadorRedondeo.setComandoCerrarPromocion(new IComando() {

                        public void executar(Object... x) {
                            SION.log(Modulo.VENTA, "Cerrando ventana de promociones redondeo.", Level.INFO);
                            AdministraVentanas.cerrarPopup();
                        }
                    });

                    presentadorRedondeo.setComandoRechazarPromocion(
                            new IComando() {

                                @Override
                                public void executar(Object... x) {
                                    AdministraVentanas.cerrarPopup();
                                    if (tareaRechazado != null) {
                                        tareaRechazado.run();
                                    }
                                }
                            });
                    presentadorRedondeo.setComandoAceptarPromocion(
                            new IComando() {

                                @Override
                                public void executar(Object... parametros) {
                                    //Agregar articulos de las promociones seleccionadas 
                                    if (parametros[0] != null) {
                                        Set<ModeloArticulo> arts = (Set<ModeloArticulo>) parametros[0];
                                        if (arts.iterator().hasNext()) {
                                            insertaArticuloPromocion(arts.iterator().next());
                                            if (vistamodelo.getPeticionVentaCuponesDto() != null
                                                    && vistamodelo.getPeticionVentaCuponesDto().getFinTicketMktc() != null) {
                                                vistamodelo.getPeticionVentaCuponesDto().getFinTicketMktc().setTicketAnterior(
                                                        vistamodelo.getPeticionVentaCuponesDto().getFinTicketMktc().getTicket());
                                            }

                                        }
                                    }

                                    //Recalcular importe recibido con dato de promociones 
                                    if (parametros[1] != null) {
                                        final Double importeRecibido = (Double) parametros[1];
                                        campoImporteRecibido.textProperty().set(String.format(local, "%.2f", importeRecibido));
                                        //campoImporteRecibido.textProperty().set(decimalFormat.format(importeRecibido)); 
                                    }
                                    AdministraVentanas.cerrarPopup();
                                    //Continuar con la operación normal 
                                    if (tareaAceptado != null) {
                                        tareaAceptado.run();
                                    }

                                }
                            });


                } else {
                    SION.log(Modulo.VENTA, "Venta no aplica para promocion de redondeo", Level.INFO);
                    tareaRechazado.run();
                }
            } else {
                SION.log(Modulo.VENTA, "Ya hay promociones asignadas a la venta.", Level.INFO);
                tareaRechazado.run();
            }
        } else {
            SION.log(Modulo.VENTA, "Promociones inactivas (Redondeo)...", Level.INFO);
            tareaRechazado.run();
        }
    }

    public void mostrarPopWindowCupones(final IComandoCupon continuar) {

        SION.log(Modulo.VENTA, "mostrarPopWindowCupones", Level.INFO);
        //repositorioCupones = new MarketecRepositorioCupones();//new FakeRepositorioCupones(); 
        vistaGralCupones = new VistaCuponesMarketec();
        presentadorCupones = new PresentadorCuponesMarketec(vistaGralCupones, configuracionCupones);
        vistaGralCupones.setPresenter(presentadorCupones);

        PresentadorCuponesMarketec.DebeIntentarCupones debe = (PresentadorCuponesMarketec.DebeIntentarCupones) presentadorCupones.debeIntentarDescuentoCupones();
        if (!debe.getDebe()) {
            vistamodelo.getPeticionVentaCuponesDto().setEsVentaSinMarketec(true);
            vistamodelo.getPeticionVentaCuponesDto().setEsVentaInicializadaMktc(true);
            vistamodelo.getPeticionVentaCuponesDto().setMotivoSinCupones(debe.getMensaje());
            //continuar.ejecutar();
            return;
        }

        //TODO: Al incremenar un articulo cancelar los cupones. 
        SION.log(Modulo.VENTA, "Intentará cupones", Level.INFO);
        final IComandoCupon comandoIniciarCupones = new IComandoCupon() {

            @Override
            public void ejecutar(Object... parametro) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        if (vistamodelo.getPeticionVentaCuponesDto() == null) {
                            vistamodelo.setPeticionVentaCuponesDto(new DatosCuponesVentaDto());
                        }

                        List<ArticuloCupon> articulos = new ArrayList<ArticuloCupon>();
                        try {
                            for (ArticuloBean articuloBean : vistamodelo.getListaArticulos()) {

                                if (articuloBean.getTipoDescuento() != TiposDescuento.Redondeo.ordinal()
                                        && !IdentificadorDescuentos.esDescuentoCupones(articuloBean.getTipoDescuento())
                                        && (articuloBean.getCantidad() - articuloBean.getDevueltos()) > 0) {
                                    ArticuloCupon artAgregar = new ArticuloCupon(
                                            articuloBean.getPaarticuloid(), articuloBean.getCantidad() - articuloBean.getDevueltos(),
                                            articuloBean.getPanombrearticulo(), articuloBean.getPaprecio(),
                                            articuloBean.getPacosto(), articuloBean.getPadescuento(), articuloBean.getPaiva(), articuloBean.getPagranel(),
                                            articuloBean.getPafamiliaid(), articuloBean.getPaIepsId(), articuloBean.getPaunidad(), articuloBean.getPaUnidadMedida(),
                                            new CuponBean(), articuloBean.getPacdgbarras(), articuloBean.getPaCdgBarrasPadre() + "", articuloBean.getTipoDescuento(),
                                            articuloBean.getDevueltos());

                                    int indc = articulos.indexOf(artAgregar);
                                    if (indc < 0) {
                                        articulos.add(artAgregar);
                                    } else {
                                        articulos.get(indc).setCantidad(articulos.get(indc).getCantidad() + artAgregar.getCantidad());
                                    }

                                }
                            }

                            //Iniciar cupones 
                            if (vistamodelo.getPeticionVentaCuponesDto() == null) {
                                vistamodelo.setPeticionVentaCuponesDto(new DatosCuponesVentaDto());
                            }

                            //Generar el idConciliacion y enviarlo. 

                            conciliacion = generarConciliacion(false);
                            vistamodelo.getPeticionVentaCuponesDto().getFinTicketMktc().setTicketCompleto(conciliacion + "");


                            vistamodelo.getPeticionVentaCuponesDto().getFinTicketMktc().setCajeroId((int) VistaBarraUsuario.getSesion().getUsuarioId());
                            vistamodelo.getPeticionVentaCuponesDto().getFinTicketMktc().setSucursal((int) VistaBarraUsuario.getSesion().getTiendaId());
                            vistamodelo.getPeticionVentaCuponesDto().getFinTicketMktc().setPos((int) VistaBarraUsuario.getSesion().getNumeroEstacion());
                            //vistamodelo.getPeticionVentaCuponesDto().getFinTicketMktc().setTicketCompleto(conciliacion+"");                         
                            vistamodelo.getPeticionVentaCuponesDto().setArticulosCupon(articulos);
                            vistamodelo.getPeticionVentaCuponesDto().incrementeIntentos();

                            AdministraVentanas.mostrarPopup("cupones", (Node) vistaGralCupones.getVista());
                            presentadorCupones.init(vistamodelo.getPeticionVentaCuponesDto());
                        } catch (Exception ex) {
                            SION.logearExcepcion(Modulo.VENTA, ex, "Pop confirma borrar vales");
                            SION.log(Modulo.VENTA, "No se intentarán los cupones : " + getStackTrace(ex), Level.INFO);
                            continuar.ejecutar();
                        }
                    }
                });

            }
        };




        try {
            //hay descuentos asignados? 
            boolean hayDescuentos = false;
            for (ArticuloBean articuloCupon : vistamodelo.getListaArticulos()) {
                hayDescuentos = articuloCupon.getTipoDescuento() != ArticuloCupon.ID_SIN_DESCUENTO;
                if (hayDescuentos) {
                    break;
                }
            }

            if (hayDescuentos) {
                listenerPopupCupones = new ChangeListener<Number>() {

                    final int ACEPTADO = 1, CANCELADO = 2, AGREGADO = 0;

                    @Override
                    public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number newVal) {
                        if (newVal.intValue() != AGREGADO) {
                            utileriasVenta.getEstatusPopup().removeListener(listenerPopupCupones);
                        }

                        if (newVal.intValue() == ACEPTADO) {
                            SION.log(Modulo.VENTA, "Acepta eliminar descuentos", Level.INFO);
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    vistamodelo.eliminarArticulosRedondeo();
                                    vistamodelo.eliminarDescuentosDeCupones(delegadoPresentadorCuponesEliminarCupones());
                                    presentadorCupones.cancelaVenta(vistamodelo.getPeticionVentaCuponesDto());
                                    try {
                                        Thread.currentThread().wait(200L);
                                    } catch (InterruptedException ex) {
                                    }
                                    //Mostrar ventana
                                    comandoIniciarCupones.ejecutar(0);
                                }
                            });

                        } else if (newVal.intValue() == CANCELADO) {
                            SION.log(Modulo.VENTA, "Rechaza eliminar descuentos", Level.INFO);
                        }
                    }
                };
                //Notificar que se perderan los cupones capturados previamente 
                utileriasVenta.getEstatusPopup().addListener(listenerPopupCupones);
                utileriasVenta.mostrarPopupPregunta("Se perderan los descuentos que hay actualmente.\n\n ¿Desea continuar?", TipoMensaje.PREGUNTA);
            } else {
                //Mostrar ventana 
                comandoIniciarCupones.ejecutar(0);
            }



            //Aplicar los descuentos que vienen de la ventana 
            presentadorCupones.setComandoAplicarDescuentos(new IComandoCupon() {

                @Override
                public void ejecutar(final Object... parametro) {

                    DatosCuponesVentaDto ideAplicado = (DatosCuponesVentaDto) parametro[0];
                    List<ArticuloCupon> articulos = ideAplicado.getArticulosCupon();

                    if (articulos != null) {
                        for (ArticuloCupon articuloCupon : articulos) {
                            if (articuloCupon.getId() == ArticuloCupon.ID_ARTICULO_DESCTO_TOTAL) {
                                ArticuloBean arti = new ArticuloBean(
                                        //double acantidad, String apacdgbarras, double apaprecio, long apaarticuloid, String apanombrearticulo, 
                                        articuloCupon.getCantidad(), articuloCupon.getCodigoBarras(), articuloCupon.getPrecio(), articuloCupon.getId(), articuloCupon.getNombre(),
                                        //int apafamiliaid, double apacosto, double apaiva, double apadescuento, double apagranel, int apaunidadmedida, 
                                        articuloCupon.getIdFamilia(), articuloCupon.getCosto(), articuloCupon.getIva(), articuloCupon.getDescuento(), articuloCupon.getAgranel(), articuloCupon.getNormaEmpaque(),
                                        //long apaCdgBarrasPadre, int apacdgerror, String apadescerror, double aimporte, int acompaniaid,  
                                        Long.parseLong(articuloCupon.getCodigoBarrasPadre()), 0, "", articuloCupon.getImporte(), 0,
                                        //String adescripcioncompania, double adevcantidad, double adevimporte, double adevueltos, int apaunidad, int aiepsid, 
                                        "", 0, 0, 0, articuloCupon.getUnidad(), articuloCupon.getIdIEps(),
                                        //int aMetodoEntrada 
                                        ArticuloCupon.ID_METODO_ENTRADA_DESC_INSERTADO);
                                arti.setTipoDescuento(articuloCupon.getTipoDescuento());
                                vistamodelo.getListaArticulos().add(vistamodelo.getListaArticulos().size(), arti);
                            } else {
                                for (ArticuloBean articuloBean : vistamodelo.getListaArticulos()) {
                                    if (articuloBean.getPacdgbarras().equals(articuloCupon.getCodigoBarras())
                                            && IdentificadorDescuentos.esDescuentoCupones(articuloCupon.getTipoDescuento())
                                            && (articuloBean.getCantidad() - articuloBean.getDevueltos()) > 0) {
                                        articuloBean.setTipoDescuento(articuloCupon.getTipoDescuento());
                                        articuloBean.setPadescuento(articuloCupon.getDescuento());
                                        articuloBean.setImporte(
                                                articuloBean.getCantidad() * (articuloBean.getPaprecio() - articuloBean.getPadescuento()));
                                    }
                                }
                            }


                        }
                    }

                    vista.getEtiquetaMontoVenta().textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));

                    vistamodelo.setPeticionVentaCuponesDto(ideAplicado);

                    SION.log(Modulo.VENTA, "Cerrando ventana de cupones.", Level.INFO);
                    AdministraVentanas.cerrarPopup();

                }
            });

            presentadorCupones.setComandoCerrarCupones(new IComandoCupon() {

                @Override
                public void ejecutar(Object... parametro) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            SION.log(Modulo.VENTA, "Cerrando ventana de cupones.", Level.INFO);
                            vistamodelo.eliminarArticulosRedondeo();
                            //Eliminar articulos con descuento por cupones                 
                            vistamodelo.eliminarDescuentosDeCupones(delegadoPresentadorCuponesEliminarCupones());

                            vista.getEtiquetaMontoVenta().textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));

                            AdministraVentanas.cerrarPopup();
                        }
                    });
                }
            });

            presentadorCupones.setComandoContinuar(continuar);

            presentadorCupones.setComandoOnError(
                    new IComandoCupon() {

                        @Override
                        public void ejecutar(final Object... parametros) {
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    vistamodelo.setPeticionVentaCuponesDto((DatosCuponesVentaDto) parametros[0]);

                                    //Eliminar articulos con descuento por redondeo                                     
                                    vistamodelo.eliminarArticulosRedondeo();
                                    //Eliminar articulos con descuento por cupones                 
                                    vistamodelo.eliminarDescuentosDeCupones(delegadoPresentadorCuponesEliminarCupones());

                                    vista.getEtiquetaMontoVenta().textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));

                                    AdministraVentanas.mostrarAlerta(
                                            "Se produjo un error de comunicación con el sistema de cupones, "
                                            + "por lo que SE PERDIERON TODOS LOS DESCUENTOS.", TipoMensaje.ERROR);
                                }
                            });

                        }
                    });
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "setComandoOnError");
        }


    }

    public Runnable delegadoPresentadorCuponesEliminarCupones() {
        return new Runnable() {

            @Override
            public void run() {
                if (vistamodelo.hayCupones()) {
                    presentadorCupones.cancelaVenta(vistamodelo.getPeticionVentaCuponesDto());
                }
            }
        };
    }

    ///////////////////////////////////////////////////////eventos de navegacion 
    public void eventosTab(final TableView tabla, final TextField campoImporteRecibido, final TextField campoMontoRestante,
            final TextField campoMontoTarjeta, final TextField campoMontoEfectivo) {

        campoMontoTarjeta.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (tab.match(arg0) || shifTab.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            campoMontoEfectivo.requestFocus();
                        }
                    });
                }
            }
        });

        campoMontoRestante.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (tab.match(arg0) || shifTab.match(arg0)) {
                    if (vistamodelo.getTamanioListaArticulos() > 0 || vistamodelo.getTamanioListaArticulosTA() > 0) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                //SION.log(Modulo.VENTA, ">>>**", Level.INFO); 
                                campoImporteRecibido.setDisable(false);
                                campoImporteRecibido.requestFocus();
                            }
                        });
                    } else {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                tabla.requestFocus();
                            }
                        });
                    }
                }
            }
        });

        tabla.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {

                if (tab.match(arg0) || shifTab.match(arg0)) {
                    if (vistamodelo.getTamanioListaArticulos() > 0 || vistamodelo.getTamanioListaArticulosTA() > 0) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                //SION.log(Modulo.VENTA, ">>>***", Level.INFO); 
                                campoImporteRecibido.setDisable(false);
                                campoImporteRecibido.requestFocus();
                            }
                        });
                    } else {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                tabla.requestFocus();
                            }
                        });
                    }
                }

            }
        });

    }

    public void operacionesTarjetaDescuento(Group grBotonTarDesc) {
        grBotonTarDesc.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {
                operacionesTarjetaDescuento();
            }
        });
    }

    public void operacionesCuponesDescuento(Group grBotonTarDesc) {
        grBotonTarDesc.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {
                operacionesCuponesDescuento();
            }
        });
    }
    //TODO: Validar si se va a trabajar con cupones cuando hay forzadoFL

    public void operacionesCuponesDescuento() {
        consultarParametrosAutorizacion(ACCION_VENTA_FL, 0);
        if (existeParametroVentaFueraLinea) {
            AdministraVentanas.mostrarAlerta("Sin cupones", "Por el momento no hay servicio para cupones. (Fuera de línea activado).", TipoMensaje.ADVERTENCIA);
            SION.log(Modulo.VENTA, "Omite cupones por fuera de línea activado.", Level.INFO);
        } else {
            mostrarPopWindowCupones(redondeo);
        }
    }

    public void operacionesTarjetaDescuento() {
        SION.log(Modulo.VENTA, "Iniciando Proceso de lectura de tarjeta", Level.INFO);
        Service servicio = new Service() {

            public Task createTask() {
                return new Task() {

                    public Object call() {
                        try {
                            SION.log(Modulo.VENTA, "Ya hay tarjeta de descuento leida, y se forzará a validación de descuentos...", Level.INFO);
                            vistamodelo.getCtrlTarjetasDescuento().setNecesitaValidarArticulos(true);
                            vistamodelo.actualizaArticulosXValidar(vistamodelo.getListaArticulos());
                            RespuestaValidaDescuentoArticulosDto respValidacion =
                                    vistamodelo.getCtrlTarjetasDescuento().validarDescuentoArticulos(null);

                            if (respValidacion != null) {
                                vistamodelo.actualizarListaArticulos(vistamodelo.getCtrlTarjetasDescuento().getArticulosValidados());
                                if (respValidacion.getErrorId() != 0) {
                                    SION.log(Modulo.VENTA, "Respuesta de error: "
                                            + respValidacion.getErrorId() + ", " + respValidacion.getErrorMensaje(), Level.WARNING);
                                    mostrarAlertaRetornaFoco(respValidacion.getErrorMensaje(), TipoMensaje.ERROR,
                                            vista.getTablaVentas());
                                }
                            } else {
                                SION.log(Modulo.VENTA, "Respuesta nula.", Level.WARNING);
                                mostrarAlertaRetornaFoco("Ocurrio un problema al consultar los desceuntos para los artículos.",
                                        TipoMensaje.ERROR, vista.getTablaVentas());
                            }
                            refrescarMontoVenta();

                        } catch (Exception e) {
                            SION.logearExcepcion(Modulo.VENTA, e, "");
                        }

                        return "OK";
                    }
                };
            }
        };
        servicio.stateProperty().addListener(new ChangeListener() {

            public void changed(ObservableValue obv, Object oldV, Object newV) {
                if (newV == Worker.State.SUCCEEDED || newV == Worker.State.FAILED) {
                    Platform.runLater(new Runnable() {

                        public void run() {
                            vista.removerImagenLoading();
                        }
                    });
                }
            }
        });

        if (!vistamodelo.hayTarjetaDescuento()) {
            SION.log(Modulo.VENTA, "No hay tarjeta de descuento leida...", Level.INFO);
            cobroTarjetaVista.getStageLeerTarjeta();
        } else {
            Platform.runLater(new Runnable() {

                public void run() {
                    vista.cargarLoading();
                }
            });
            servicio.start();
        }
        /// 
    }

    //////////////////////////////////////////////////eventos de los componentes 
    public void operacionesTarjeta(final TextField campoTarjetas, final int idTarjetaVales) {

        final EventHandler<KeyEvent> ke = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                event.consume();
                if (!vista.getMontoEfectivo().isEmpty()) {

                    try {
                        montoEfectivo = Double.parseDouble(vista.getMontoEfectivo());
                    } catch (NumberFormatException e) {
                        SION.logearExcepcion(Modulo.VENTA, e, "");
                        montoEfectivo = 0;
                    }
                } else {
                    montoEfectivo = 0;
                }
                if (!vista.getMontoVales().isEmpty()) {

                    try {
                        montoVales = Double.parseDouble(vista.getMontoVales());
                    } catch (NumberFormatException e) {
                        SION.logearExcepcion(Modulo.VENTA, e, mensajeError);
                        montoVales = 0;
                    }
                } else {
                    montoVales = 0;
                }

                montoVenta = Double.parseDouble(vista.getMontoVenta());

                if (enter.match(event)) {
                    if (!campoTarjetas.textProperty().getValue().isEmpty() && Double.parseDouble(campoTarjetas.textProperty().getValue()) > 0) {

                        double montoPermitidoParaAplicar = (montoVenta - (montoEfectivo + montoVales + vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS)));

                        //String formatoresta = utilerias.getNumeroFormateado(montoResta); 
                        String formatoresta = utileriasVenta.getNumeroFormateado(montoPermitidoParaAplicar);
                        montoPermitidoParaAplicar = Double.valueOf(formatoresta);

                        double montoPorAplicar = Double.valueOf(campoTarjetas.textProperty().getValue());
                        //String formatoCampoTexto = utilerias.getNumeroFormateado(montoCampoTexto); 
                        String formatoCampoTexto = utileriasVenta.getNumeroFormateado(montoPorAplicar);
                        montoPorAplicar = Double.valueOf(formatoCampoTexto);


                        if (montoPorAplicar <= montoPermitidoParaAplicar) {
                            vista.cargarLoading();
                            PeticionPagoTarjetaBean peticionPagoTarjeta = vistamodelo.generaPeticionPagoTarjeta(montoPorAplicar);
                            //se manda llamar vista de pago con tarjeta y se le envia peticion 
                            SION.log(Modulo.VENTA, "Iniciando Proceso de cobro con tarjeta", Level.INFO);
                            cobroTarjetaVista.getStageCobroTarjeta(peticionPagoTarjeta);
                        } else {
                            SION.log(Modulo.VENTA, "El monto por aplicar es mayor al permitido para realizar la venta, mostrando advertencia", Level.WARNING);
                            mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.excedeTarjeta"), TipoMensaje.ADVERTENCIA, campoTarjetas);
                        }
                    }
                }

            }
        };
        campoTarjetas.addEventHandler(KeyEvent.KEY_PRESSED, ke);
    }

    public void eventosCampoImporteRecibido(final TextField campoImporteRecibido, final TextField campoCambio,
            final Label etiquetaMontoVenta, final TableView tabla, final TextField campoBusqueda,
            final TextField campoMontoRestante) {

        campoImporteRecibido.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (tab.match(arg0) || shifTab.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            tabla.requestFocus();
                        }
                    });

                }
            }
        });

        campoImporteRecibido.focusedProperty().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable arg0) {
                if (campoImporteRecibido.isFocused()) {
                    campoBusqueda.setDisable(true);
                    if (idPantalla == PANTALLA_VENTA) {
                        idPantalla = PANTALLA_CAPTURA;
                        agregarEventosCaptura(null);
                    }
                }
            }
        });

        //campoImporteRecibido.setOnKeyReleased(new EventHandler<KeyEvent>() { 
        campoImporteRecibido.textProperty().addListener(new ChangeListener<String>() {

            double cambio = 0;
            double restante = 0;

            @Override
            public void changed(ObservableValue<? extends String> arg0, String oldValue, String newValue) {
                //@Override 
                //public void handle(KeyEvent arg0) { 

                if (etiquetaMontoVenta.textProperty().get().isEmpty()) {
                    campoCambio.textProperty().set("0.00");
                    campoMontoRestante.textProperty().set("0.00");
                } else {
                    try {
                        cambio = Double.parseDouble(campoImporteRecibido.textProperty().get()) - Double.parseDouble(etiquetaMontoVenta.textProperty().get());
                        if (cambio > 0) {
                            campoCambio.textProperty().set(utileriasVenta.getNumeroFormateado(cambio));
                            campoMontoRestante.textProperty().set("0.00");
                        } else {
                            restante = Double.parseDouble(etiquetaMontoVenta.textProperty().get()) - Double.parseDouble(campoImporteRecibido.textProperty().get());
                            if (restante > 0) {
                                campoMontoRestante.textProperty().set(utileriasVenta.getNumeroFormateado(restante));
                            } else {
                                campoMontoRestante.textProperty().set("0.00");
                            }
                            campoCambio.textProperty().set("0.00");
                        }
                    } catch (NumberFormatException ex) {
                        cambio = 0;
                        restante = 0;
                        campoMontoRestante.textProperty().set("0" + etiquetaMontoVenta.textProperty().get());
                        campoCambio.textProperty().set("0.00");
                        //SION.logearExcepcion(Modulo.VENTA, ex, ""); 
                    }
                }
            }
        });
    }

    public void operacionesTeclaEscape() {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {

                SION.log(Modulo.VENTA, "regresando a pantalla de captura de articulos", Level.FINE);

                vista.setMontoEfectivo("");
                vista.setMontoVales("");
                vista.setMontoValesE("");
                vista.setMontoTarjetas("");
                vista.setCantidadVales("");

                if (vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS) > 0) {
                    montoRestante = vistamodelo.getImporteVenta() - vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS);
                    montoAcarreado = vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS);
                } else {
                    montoRestante = vistamodelo.getImporteVenta();
                    montoAcarreado = 0;
                }


                if (montoAcarreado > 0) {
                    try {
                        vista.setMontoRecibidoPantallaCaptura(utileriasVenta.getNumeroFormateado(montoAcarreado));
                        //vista.setMontoRecibidoPantallaCaptura(utilerias.getNumeroFormateado(montoAcarreado)); 
                    } catch (NumberFormatException e) {

                        vista.setMontoRecibidoPantallaCaptura("");
                    }
                } else {
                    vista.setMontoRecibidoPantallaCaptura("");
                }

                if (montoRestante > 0) {
                    try {
                        vista.setMontoRestante(utileriasVenta.getNumeroFormateado(montoRestante));
                        //vista.setMontoRestante(utilerias.getNumeroFormateado(montoRestante)); 
                    } catch (NumberFormatException e) {
                        SION.logearExcepcion(Modulo.VENTA, e);
                        vista.setMontoRestante("0.00");
                    }
                } else {
                    vista.setMontoRestante("0.00");
                }

                double cambio = 0.0;

                try {

                    if (getImporteRecibido() > vistamodelo.getImporteVenta()) {
                        if (vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS) >= vistamodelo.getImporteVenta()) {

                            cambio = vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS) - vistamodelo.getImporteVenta();
                            //vista.setMontoCambio(utilerias.getNumeroFormateado(cambio)); 
                            vista.setMontoCambio(utileriasVenta.getNumeroFormateado(cambio));
                        } else {
                            vista.setMontoCambio("0.00");
                        }

                    } else {
                        vista.setMontoCambio("0.00");
                    }

                } catch (NumberFormatException e) {
                    vista.setMontoCambio("0.00");
                }

                esEscapeConsumido = true;

                cambiarCajasComponentes(PANTALLA_CAPTURA, vista.getPanelVentas());

                idEscape = 0;

                esVentaTA = false;
                esVentaGral = true;

                vista.setFocusTablaVentas();

            }
        });

    }

    public void eventosCamposOtrasFormasDePago(final TextField campoEfectivo, final TextField campoVales, final TextField campoValesE,
            final TextField campoTarjetas, final TextField campoCantidadVales) {

        EventHandler<KeyEvent> eventoEfectivoVales = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {

                if (!hayVentanaError) {
                    if (f10.match(arg0) || enter.match(arg0)) {

                        arg0.consume();

                        if (f10.match(arg0)) {
                            SION.log(Modulo.VENTA, "F10 presionado en pantalla de otras formas de pago", Level.INFO);
                        } else if (enter.match(arg0)) {
                            SION.log(Modulo.VENTA, "Enter presionado en pantalla de otras formas de pago", Level.INFO);
                        }

                        if (getImporteRecibido() >= vistamodelo.getImporteVenta()) {
                            consultarParametrosAutorizacion(ACCION_VENTA, 1);

                        } else {
                            if (idPantalla == PANTALLA_CAPTURA) {
                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(
                                                Modulo.VENTA, "ventas.montos"), TipoMensaje.ADVERTENCIA, vista.getTablaVentas());
                                        hayVentanaError = true;
                                    }
                                });

                            } else {
                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.montos"),
                                                TipoMensaje.ADVERTENCIA, campoEfectivo);
                                        hayVentanaError = true;
                                    }
                                });

                            }
                        }
                    } else if (escape.match(arg0)) {
                        SION.log(Modulo.VENTA, "escape presionado", Level.INFO);
                        operacionesTeclaEscape();
                    }

                } else {
                    hayVentanaError = false;
                }



            }
        };
        campoEfectivo.addEventFilter(KeyEvent.KEY_PRESSED, eventoEfectivoVales);
        campoVales.addEventFilter(KeyEvent.KEY_PRESSED, eventoEfectivoVales);
        campoCantidadVales.addEventFilter(KeyEvent.KEY_PRESSED, eventoEfectivoVales);

        EventHandler<KeyEvent> eventoTarjetas = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (f10.match(arg0)) {
                    SION.log(Modulo.VENTA, "F10 presionado sobre campo de monto de tarjetas", Level.FINEST);
                    consultarParametrosAutorizacion(ACCION_VENTA, 3);
                    arg0.consume();
                } else if (escape.match(arg0)) {
                    SION.log(Modulo.VENTA, "escape presionado", Level.INFO);
                    operacionesTeclaEscape();
                }
            }
        };
        campoValesE.addEventHandler(KeyEvent.KEY_PRESSED, eventoTarjetas);
        campoTarjetas.addEventHandler(KeyEvent.KEY_PRESSED, eventoTarjetas);

    }

    public void formatearCampoTarjetas(TextField campo) {
        ValidadorNumerico validadorNumerico = new ValidadorNumerico();
        validadorNumerico.validadorCampoTarjetas(campo);
    }

    public void formatearCampoVales(TextField campo, int digitosPermitidos) {
        ValidadorNumerico validadorNumerico = new ValidadorNumerico();
        validadorNumerico.validadorCampoVales(campo, digitosPermitidos);
    }

    public void formatearCampoNip(PasswordField campo, int digitosPermitidos) {
        ValidadorNumerico validadorNumerico = new ValidadorNumerico();
        validadorNumerico.validadorCampoNip(campo, digitosPermitidos);
    }

    public void intercambiarPantalla(TextField campoImporteRecibido, Label etiquetaMontoVenta) {
        double importe = 0;

        if (!campoImporteRecibido.textProperty().get().isEmpty()) {
            try {
                importe = Double.parseDouble(campoImporteRecibido.textProperty().get());
            } catch (NumberFormatException e) {
                importe = 0;
            }
        }

        if (importe < Double.parseDouble(etiquetaMontoVenta.textProperty().get())) {

            montoAcarreado = importe;

            cambiarCajasComponentes(PANTALLA_VENTA, vista.getPanelVentas());

            campoImporteRecibido.setDisable(true);
            System.out.println("montoAcarreado : " + montoAcarreado);

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    vista.setFocusCampoImporteEfectivo();
                }
            });

        }
    }

    public void cambiarCajasComponentes(int id, final BorderPane panel) {

        if (id == PANTALLA_VENTA) {

            VistaBarraUsuario.setLogoHabilitado(false);
            AdministraVentanas.removerESCEventHandler();

            agregarEventosEscape(panel);
            removerEventosCaptura(panel);

            if (montoAcarreado > 0) {
                vista.setMontoEfectivo(String.valueOf(utileriasVenta.getNumeroFormateado(montoAcarreado)));
            }

            idPantalla = PANTALLA_VENTA;
            idEscape = 1;

            vista.cambiarPantalla(PANTALLA_VENTA);
        } else {

            agregarEventosCaptura(panel);

            idPantalla = PANTALLA_CAPTURA;

            vista.cambiarPantalla(PANTALLA_CAPTURA);
        }
    }

    /////////////////////////////////////////////////operaciones de  de la tabla 
    public void llenarTabla(TableView tabla) {
        tabla.setItems(vistamodelo.getListaArticulos());
    }

    public <E> void asignaPropiedadEnColumna(TableColumn columna, E e, final String idColumna) {
        columna.setCellValueFactory(new PropertyValueFactory<ArticuloBean, E>(idColumna));
    }

    public void posicionarScrollEnTabla(final TableView tabla) {

        EventHandler<KeyEvent> eh = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (enter.match(arg0)) {

                    if (vistamodelo.getListaArticulos().size() > 1) {

                        tabla.getSelectionModel().clearSelection();
                        tabla.getSelectionModel().selectLast();
                        tabla.scrollTo(vistamodelo.getTamanioListaArticulos());
                        tabla.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
                    }

                }
            }
        };
        tabla.addEventHandler(KeyEvent.KEY_PRESSED, eh);

    }

    public <E> void setCellFactory(final TableView tabla, final TableColumn columna, final E e, final Pos posicion,
            final boolean seValidaFormato) {

        columna.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn param) {
                return new TableCell<ArticuloBean, E>() {

                    @Override
                    public void updateItem(E item, boolean empty) {
                        super.updateItem(item, empty);

                        if (!this.isEmpty()) {
                            ////reavisa si E es Intger 
                            if (e.toString().contains(Integer.class.getCanonicalName())) {
                            } else if (e.toString().contains(Double.class.getCanonicalName())) {
                                if (seValidaFormato) {

                                    cadenaDouble = "";
                                    indicePunto = 0;
                                    cadenaPuntoAlaDerecha = "";

                                    cadenaDouble = String.valueOf(item);
                                    indicePunto = cadenaDouble.indexOf(".");
                                    cadenaPuntoAlaDerecha = cadenaDouble.substring(indicePunto + 1);

                                    if (Double.parseDouble(cadenaPuntoAlaDerecha) > 0) {
                                        this.textProperty().setValue(cadenaDouble);//this.textProperty().setValue(cadenaDouble); 
                                    } else {
                                        this.textProperty().setValue(cadenaDouble.substring(0, indicePunto));
                                    }

                                } else {
                                    this.textProperty().setValue(utileriasVenta.getNumeroFormateado(Double.parseDouble(item.toString())));
                                }
                            } else {///es String 
                                this.textProperty().setValue(String.valueOf(item));
                                //this.setWrapText(true); 
                                //this.setTextOverrun(OverrunStyle.ELLIPSIS); 
                            }

                            if (vistamodelo.getListaArticulos().size() > 0) {
                                if (vistamodelo.getListaArticulos().get(this.getIndex()).getImporte() <= 0) {
                                    //this.setTextFill(Color.RED); 
                                    this.setStyle("-fx-text-fill: red; -fx-font: 20pt Verdana;");
                                } else {
                                    this.setStyle("-fx-text-fill: black; -fx-font: 20pt Verdana;");
                                }
                            } else {
                                setStyle("-fx-text-fill: black;-fx-font: 20pt Verdana;");
                            }

                            this.alignmentProperty().set(posicion);
                            //this.setFont(Font.getDefault()); 
                            //this.setStyle("-fx-font: 20pt Verdana;"); 


                        }
                    }
                };
            }
        });
    }

    public void validarArticuloEscaneado(final TableView tabla, Node nodo) {

        cantidadArticulos = 1;
        cadenaValidacionArticulos = "";
        idgranel = 0;

        //SION.log(Modulo.VENTA, "Entrando a método de validación de cadena ingresada por usuario", Level.FINEST); 

        SION.log(Modulo.VENTA, "Cadena ingresada por usuario: " + cadena, Level.INFO);

        if (cadena != null && cadena.trim().length() != 0 && !esVentaTA) {

            if (cadena.contains("*")) {
                SION.log(Modulo.VENTA, "se detectó que la cadena contiene caracter *", Level.FINEST);
                if (cadena.contains(".")) {
                    SION.log(Modulo.VENTA, "se detectó que cadena contiene .", Level.FINEST);
                    matcherContienePunto = patronContienePunto.matcher(cadena);
                    if (matcherContienePunto.find()) {
                        SION.log(Modulo.VENTA, "La cadena contiene patrón de venta de articulo a granel", Level.FINEST);
                        posicionSignoPor = cadena.indexOf("*");
                        i = 0;
                        while (i < posicionSignoPor) {
                            cadenaValidacionArticulos += cadena.charAt(i);
                            i++;
                        }
                        try {
                            cantidadArticulos = Double.parseDouble(cadenaValidacionArticulos);
                        } catch (NumberFormatException e) {
                            cantidadArticulos = 0;
                        }
                        cadena = cadena.substring(posicionSignoPor + 1);

                        idgranel = 1;

                        SION.log(Modulo.VENTA, "Cantidad de artículos encontrados en cadena: " + cantidadArticulos, Level.INFO);
                        SION.log(Modulo.VENTA, "Artículo encontrado en la cadena: " + cadena, Level.INFO);
                    }
                } else {

                    matcherSinPunto = patronSinPunto.matcher(cadena);
                    if (matcherSinPunto.find()) {

                        SION.log(Modulo.VENTA, "Cadena contiene patrón de venta de más de un artículo", Level.FINEST);
                        posicionSignoPor = cadena.indexOf("*");

                        i = 0;
                        while (i < posicionSignoPor) {
                            cadenaValidacionArticulos += cadena.charAt(i);
                            i++;
                        }
                        try {
                            cantidadArticulos = Double.parseDouble(cadenaValidacionArticulos);
                        } catch (NumberFormatException e) {
                            cantidadArticulos = 0;
                        }
                        cadena = cadena.substring(posicionSignoPor + 1);

                        SION.log(Modulo.VENTA, "Cantidad encontrada en la cadena: " + cantidadArticulos, Level.INFO);
                        SION.log(Modulo.VENTA, "Artículo encontrado en la cadena: " + cadena, Level.INFO);

                    } else {
                        mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.codigoNoValido"), TipoMensaje.ERROR, tabla);
                    }
                }
            }

            if (cantidadArticulos > 0 && cantidadArticulos < 1000) {

                consultarArticulo(cantidadArticulos, idgranel, cadena, metodoEntradaEscaner);

                etiquetaValidacion = new Label();
                campotextoValidacion = new TextField();

                if (nodo instanceof Label) {
                    etiquetaValidacion = (Label) nodo;
                    etiquetaValidacion.textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));
                } else if (nodo instanceof TextField) {
                    campotextoValidacion = (TextField) nodo;
                    campotextoValidacion.textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));
                }

            } else if (cantidadArticulos > CANTIDAD_MAXIMA_POR_ARTICULO) {
                cantidadArticulos = 1;
                mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.cantidadArticulos"), TipoMensaje.INFO, tabla);
            } else {
                mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.cantidadArticulos"), TipoMensaje.INFO, tabla);

            }
            cadena = "";

            if (vistamodelo.getTamanioListaArticulos() == 1) {

                VistaBarraUsuario.setLogoHabilitado(false);
                AdministraVentanas.removerESCEventHandler();

                SION.log(Modulo.VENTA, "Existe venta activa -> true", Level.INFO);
                AdministraVentanas.setExisteVentaActiva(true);

                agregarEventosEscape(vista.getPanelVentas());
            }

        } else {//pasar a campo de importe 

            if (!vista.getMontoVenta().isEmpty()) {
                try {
                    if (vistamodelo.getTamanioListaArticulos() > 0
                            && Double.parseDouble(vista.getMontoVenta()) > 0) {

                        vista.cambiarEstadoCampoImporteRecibido(false);
                        if (!articuloRecienEscaneado) {
                            vista.setFocusCampoImporteRecibido();
                        } else {
                            articuloRecienEscaneado = false;
                        }
                    }
                } catch (NumberFormatException e) {
                    SION.logearExcepcion(Modulo.VENTA, e, "");
                }
            }

        }

        if (esVentaTA) {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    vista.setFocusCampoImporteRecibido();
                }
            });
        }
    }

    private String truncarCantidad(double _cantidad) {

        String cantidadTruncada = "";
        if (_cantidad > 0) {
            cantidadTruncada = String.valueOf(_cantidad);
            if (cantidadTruncada.contains(".")) {
                int posicionPunto = cantidadTruncada.indexOf(".");
                if ((cantidadTruncada.length() - posicionPunto) > 3) {
                    cantidadTruncada = cantidadTruncada.substring(0, posicionPunto + 3);
                }
            }
        }

        return cantidadTruncada;
    }

    public void insertaArticuloPromocion(ModeloArticulo articuloPromocion) {
        int METODO_ENTRADA_PROMOCION = 4;
        int ID_FAMILIA_SERVICIOS = 4;

        ArticuloBean conversionArticulo = new ArticuloBean();


        conversionArticulo.setCantidad(articuloPromocion.getCantidad());
        conversionArticulo.setCompaniaid(0);
        conversionArticulo.setDescripcioncompania("N/A");
        conversionArticulo.setDevcantidad(0);
        conversionArticulo.setDevueltos(0);
        conversionArticulo.setMetodoEntrada(METODO_ENTRADA_PROMOCION);
        conversionArticulo.setPaCdgBarrasPadre(Long.parseLong(articuloPromocion.getCodBarraspadre()));
        conversionArticulo.setPaIepsId(articuloPromocion.getIdIEPS());
        conversionArticulo.setPaUnidadMedida(articuloPromocion.getUnidad());
        conversionArticulo.setPaarticuloid(articuloPromocion.getId());
        conversionArticulo.setPacdgbarras(articuloPromocion.getCodBarras());
        conversionArticulo.setPacdgerror(0);
        conversionArticulo.setPacosto(articuloPromocion.getCosto());
        conversionArticulo.setPadescerror("OK");
        conversionArticulo.setPadescuento(articuloPromocion.getDescuento());
        conversionArticulo.setPafamiliaid(articuloPromocion.getIdFamilia());
        conversionArticulo.setPagranel(articuloPromocion.getAgranel());
        conversionArticulo.setPaiva(articuloPromocion.getIva());
        conversionArticulo.setPanombrearticulo(articuloPromocion.getNombre());
        conversionArticulo.setPaprecio(articuloPromocion.getPrecio());
        conversionArticulo.setPaunidad(articuloPromocion.getUnidad());
        conversionArticulo.setProveedorTAid(0);
        conversionArticulo.setTipoDescuento(TiposDescuento.Redondeo.ordinal());
        conversionArticulo.setImporte(articuloPromocion.getImporte());

        int _idgranel = (int) conversionArticulo.getPagranel();



        if (articuloPromocion != null) {///valida que respuesta de store no sea nula 
            articuloRecienEscaneado = true;
            articuloLocal = conversionArticulo;//vistamodelo.getArticuloLocal(); 
            articuloLocal.setMetodoEntrada(METODO_ENTRADA_PROMOCION);

            //valida los campos del articulo 
            if (!"".equals(articuloLocal.getPacdgbarras())
                    && articuloLocal.getPaprecio() != 0
                    && !"".equals(articuloLocal.getPadescerror())
                    && articuloLocal.getPacdgerror() == 0
                    && articuloLocal.getPacdgbarras() != null) {

                SION.log(Modulo.VENTA, "insertaArticuloPromocion :: Artículo cumple con las condiciones para ser insertado en lista", Level.INFO);
                ///se verifican que no sobrepase el monto maximo de venta 
                if ((vistamodelo.getImporteVenta() + articuloLocal.getImporte()) <= montoMaximoVenta) {
                    //valida que no sea un servicio 
                    if (articuloLocal.getPafamiliaid() == ID_FAMILIA_SERVICIOS) {
                        mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.escaneoServicio"), TipoMensaje.ADVERTENCIA, vista.getTablaVentas());
                    } else {
                        ///valida la cantidad de articulos 
                        if (articuloLocal.getCantidad() < CANTIDAD_MAXIMA_POR_ARTICULO) {

                            if (_idgranel == 1) {//articulos a granel 
                                if (articuloLocal.getPagranel() != 0) {
                                    vistamodelo.getListaArticulos().add(articuloLocal);

                                } else {
                                    mostrarAlertaRetornaFoco(SION.obtenerMensaje(
                                            Modulo.VENTA, "ventas.granel").replace("%1", articuloLocal.getPanombrearticulo()), TipoMensaje.ADVERTENCIA, vista.getTablaVentas());
                                }
                            } else {//no granel 
                                SION.log(Modulo.VENTA, "Artículo no agranel agregado > " + articuloLocal, Level.INFO);
                                vistamodelo.getListaArticulos().add(articuloLocal);

                                for (ArticuloBean s : vistamodelo.getListaArticulos()) {
                                    SION.log(Modulo.VENTA, "> " + s, Level.INFO);
                                }


                            }
                        }
                    }


                } else {
                    mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.montoVenta"), TipoMensaje.ADVERTENCIA, vista.getTablaVentas());
                }

            } else {
                ///si hay un mensaje de error se muestra, sino se obtiene de archivo property          
                if (!articuloLocal.getPadescerror().isEmpty()) {
                    mostrarAlertaRetornaFoco(articuloLocal.getPadescerror(), TipoMensaje.ADVERTENCIA, vista.getTablaVentas());
                }

                SION.log(Modulo.VENTA, "insertaArticuloPromocion :: El artículo no cumple con los datos minimos para ser agregado a la venta.", Level.WARNING);
            }

        } else {
            SION.log(Modulo.VENTA, "insertaArticuloPromocion :: El artículo enviado es nulo.", Level.WARNING);
        }
    }
    double __cantidad;
    double __idgranel;
    String __cadena;
    int __metodoEntrada;
    ChangeListener<Number> escuchaConfirmaEliminaCupones = new ChangeListener<Number>() {

        public int ACEPTAR = 1, AGREGADO = 0;

        @Override
        public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
            //if( arg2.intValue() != AGREGADO ) 
            //System.out.println("<escuchaConfirmaEliminaCupones>");

            if (arg2.intValue() == ACEPTAR) {
                //System.out.println("<EliminaListener>");

                utileriasVenta.getEstatusPopup().removeListener(escuchaConfirmaEliminaCupones);
                //Eliminar articulos con descuento por redondeo                                     
                vistamodelo.eliminarArticulosRedondeo();
                //Eliminar articulos con descuento por cupones                 
                vistamodelo.eliminarDescuentosDeCupones(delegadoPresentadorCuponesEliminarCupones());

                consultarArticuloX(__cantidad, __idgranel, __cadena, __metodoEntrada);
            } else {//CANCELAR 
                //System.out.println("<EliminaListener>");
                utileriasVenta.getEstatusPopup().removeListener(escuchaConfirmaEliminaCupones);
            }
        }
    };
    boolean articuloRecienEscaneado = false;

    public void consultarArticulo(double _cantidad, double _idgranel, final String _cadena, int _metodoEntrada) {
        System.out.println("<consultarArticulo>");
        //Eliminar los cupones i hay 
        if (vistamodelo.hayCupones()) {
            __cantidad = _cantidad;
            __idgranel = _idgranel;
            __cadena = _cadena;
            __metodoEntrada = _metodoEntrada;
            //System.out.println("<HayPromociones>");
            utileriasVenta.mostrarPopupPregunta("Para poder agregar más articulos es necesario eliminar los cupones.\n\n"
                    + "¿Desea eliminar los descuentos y promociones?", TipoMensaje.PREGUNTA);
            //System.out.println("<AgregaListener>");
            utileriasVenta.getEstatusPopup().addListener(escuchaConfirmaEliminaCupones);
            return;
        }
        consultarArticuloX(_cantidad, _idgranel, _cadena, _metodoEntrada);
    }

    public void consultarArticuloX(double _cantidad, double _idgranel, final String _cadena, int _metodoEntrada) {
        double importe = 0;
        double cantidad = Double.parseDouble(truncarCantidad(_cantidad));

        vistamodelo.llenarPeticionArticuloLocal(cantidad, _cadena.trim());

        if (vistamodelo.consultarArticuloLocalConDescuento() != null) {///valida que respuesta de store no sea nula 
            articuloRecienEscaneado = true;
            articuloLocal = vistamodelo.getArticuloLocal();
            articuloLocal.setMetodoEntrada(_metodoEntrada);

            //valida los campos del articulo 
            if (!"".equals(articuloLocal.getPacdgbarras()) && articuloLocal.getPaprecio() != 0
                    && !"".equals(articuloLocal.getPadescerror())
                    && articuloLocal.getPacdgerror() == 0 && articuloLocal.getPacdgbarras() != null) {

                vistamodelo.getCtrlTarjetasDescuento().setNecesitaValidarArticulos(true);
                SION.log(Modulo.VENTA, "Artículo cumple con las condiciones para ser insertado en lista", Level.INFO);
                ///se verifican que no sobrepase el monto maximo de venta 
                if ((vistamodelo.getImporteVenta() + articuloLocal.getImporte()) <= montoMaximoVenta) {
                    //valida que no sea un servicio 
                    if (articuloLocal.getPafamiliaid() == 4) {
                        mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.escaneoServicio"), TipoMensaje.ADVERTENCIA, vista.getTablaVentas());

                    } else {
                        ///valida la cantidad de articulos 
                        if (((articuloLocal.getCantidad()) < CANTIDAD_MAXIMA_POR_ARTICULO)) {

                            if (_idgranel == 1) {//articulos a granel 

                                if (articuloLocal.getPagranel() != 0) {
                                    int sizeListaArticulo = vistamodelo.getTamanioListaArticulos();
                                    if (vistamodelo.getTamanioListaArticulos() > 0) {

                                        if (articuloLocal.getPacdgbarras().equals(vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).getPacdgbarras())
                                                && vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).getDevueltos() == 0) {
                                            vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).setCantidad(
                                                    Double.parseDouble(
                                                    utileriasVenta.getNumeroFormateado(
                                                    articuloLocal.getCantidad() + vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).getCantidad())));
                                            vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).setImporte(
                                                    (vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).getPaprecio() - vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).getPadescuento())
                                                    * vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).getCantidad());
                                        } else {
                                            boolean seEncontroRegistro = false;
                                            //se busca el registro en tabla para englobar mismos articulos 

                                            for (int i = 0; i < vistamodelo.getListaArticulos().size(); i++) {
                                                //si se encuentra registro se valida que no sea un registro devuelto 
                                                if (vistamodelo.getListaArticulos().get(i).getPacdgbarras().equals(articuloLocal.getPacdgbarras())
                                                        && vistamodelo.getListaArticulos().get(i).getImporte() > 0 && vistamodelo.getListaArticulos().get(i).getDevueltos() == 0) {

                                                    //se guarda el registro  
                                                    articuloTempGranel = vistamodelo.getListaArticulos().get(i);
                                                    //se borra de la lista 
                                                    vistamodelo.getListaArticulos().remove(i);
                                                    seEncontroRegistro = true;
                                                }
                                            }

                                            if (seEncontroRegistro) {
                                                articuloLocal.setCantidad(Double.parseDouble(utileriasVenta.getNumeroFormateado(articuloLocal.getCantidad() + articuloTempGranel.getCantidad())));
                                                articuloLocal.setImporte(utileriasVenta.RedondearImporte(articuloLocal.getCantidad(), articuloLocal.getPaprecio(), articuloLocal.getPadescuento()));
                                                articuloTempGranel = null;
                                            }

                                            vistamodelo.getListaArticulos().add(articuloLocal);
                                            vistamodelo.eliminarArticulosRedondeo();
                                        }

                                    } else {

                                        vistamodelo.getListaArticulos().add(articuloLocal);
                                        vistamodelo.eliminarArticulosRedondeo();

                                    }
                                } else {
                                    mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.granel").replace("%1", articuloLocal.getPanombrearticulo()),
                                            TipoMensaje.ADVERTENCIA, vista.getTablaVentas());
                                }
                            } else {//no granel 
                                //si tamaño de lista mayor a cero revisa 
                                int sizeListaArticulo = vistamodelo.getTamanioListaArticulos();
                                if (sizeListaArticulo > 0) {
                                    if (articuloLocal.getPacdgbarras().equals(vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).getPacdgbarras())
                                            && vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).getDevueltos() == 0
                                            && (vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).getCantidad() + articuloLocal.getCantidad()) < 1000) {

                                        //valida que el ultimo artiulo ingresado sea igual que se pretende ingresar y que no se hayan devueltso y que el row no sea mayor a mil 
                                        vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).setCantidad(articuloLocal.getCantidad() + vistamodelo.getListaArticulos().get(vistamodelo.getTamanioListaArticulos() - 1).getCantidad());
                                        vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).setImporte(
                                                Double.parseDouble(utileriasVenta.getNumeroFormateado(
                                                (vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).getPaprecio()
                                                - vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).getPadescuento())
                                                * vistamodelo.getListaArticulos().get(sizeListaArticulo - 1).getCantidad())));

                                    } else {
                                        vistamodelo.getListaArticulos().add(articuloLocal);
                                        vistamodelo.eliminarArticulosRedondeo();

                                    }
                                } else {//si tamaño es cero solo inserta 
                                    vistamodelo.getListaArticulos().add(articuloLocal);
                                    vistamodelo.eliminarArticulosRedondeo();

                                }
                            }
                        }
                    }


                } else {
                    mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.montoVenta"), TipoMensaje.ADVERTENCIA, vista.getTablaVentas());
                }

            } else {
                ///si hay un mensaje de error se muestra, sino se obtiene de archivo property          
                if (!articuloLocal.getPadescerror().isEmpty()) {
                    mostrarAlertaRetornaFoco(articuloLocal.getPadescerror(), TipoMensaje.ADVERTENCIA, vista.getTablaVentas());
                } else {
                    mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.noArticulo").replace("%1", _cadena), TipoMensaje.ERROR, vista.getTablaVentas());
                }

                SION.log(Modulo.VENTA, "No se encontró el artículo " + _cadena + "en BD", Level.FINEST);
            }

        } else {
            mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.errorBDLocal"), TipoMensaje.ERROR, vista.getTablaVentas());
        }

    }

    public void actualizarRegistroArticulo(TableView tabla, Label etiqueta, int id) {

        indexTablaArticulos = tabla.getSelectionModel().getSelectedIndex();

        if (indexTablaArticulos == -1) {
            indexTablaArticulos = vistamodelo.getListaArticulos().size() - 1;
        }

        vistamodelo.modificarListaArticulos(indexTablaArticulos, id);

        if (vistamodelo.getImporteVenta() > 0) {
            etiqueta.textProperty().setValue(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));
        } else {
            etiqueta.textProperty().setValue("0.00");
        }
    }

    ////////////////////////////////////////////////////////////////////bloqueos 
    public void llamarVentanaTraspasos(final Button boton) {

        boton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                AdministraVentanas.cerrarAlerta();
                AdministraVentanas.mostrarEscena(SION.obtenerParametro(Modulo.VENTA, "ventanasTraspasos"));

            }
        });

    }
    private ChangeListener escuchaElimina = new ChangeListener<Number>() {

        final int CAMBIO = 0, ACEPTADO = 1, CANCELADO = 2;

        public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number newVal) {
            if (newVal.intValue() != CAMBIO) {
                utileriasVenta.getEstatusPopup().removeListener(escuchaElimina);
            }

            if (newVal.intValue() == ACEPTADO) {
                SION.log(Modulo.VENTA, "Acepta eliminar descuentos", Level.INFO);
                Platform.runLater(new Runnable() {

                    public void run() {   //Eliminar articulos con descuento por redondeo y cupones        
                        vistamodelo.eliminarArticulosRedondeo();
                        vistamodelo.eliminarDescuentosDeCupones(delegadoPresentadorCuponesEliminarCupones());
                        //Resetear los cupones agregados 
                        ////vistamodelo.getPeticionVentaCuponesDto().getCuponesValidados().clear(); 
                        ////vistamodelo.getPeticionVentaCuponesDto().getArticulosCupon().clear(); 
                        vista.getEtiquetaMontoVenta().textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));
                    }
                });
            } else if (newVal.intValue() == CANCELADO) {
                SION.log(Modulo.VENTA, "Rechaza eliminar descuentos", Level.INFO);
            }
        }
    };

    public void eventosAgregarQuitarArticuloEnTabla(final TableView tabla, final TableColumn columna, final Label etiquetaMontoVenta) {
        //TODO: Quitar lso descuentos  de cupones al agregar o quitar un articulo. 

        tabla.setOnKeyReleased(new EventHandler<KeyEvent>() {

            int index = 0;

            @Override
            public void handle(KeyEvent event) {
                if ((supr.match(event) || mas.match(event) || add.match(event) || sustraer.match(event) || menos.match(event)) && !esVentaTA) {

                    index = tabla.getSelectionModel().getSelectedIndex();
                    if (index < 0) {
                        index = 0;
                    }

                    SION.log(Modulo.VENTA, "verificando artículo en tabla de ventas > Index de tabla seleccionado: " + index, Level.INFO);
                    if (vistamodelo.getArticuloCodigoBarras(index) != null) {
                        SION.log(Modulo.VENTA, "Artículo a modificar: " + vistamodelo.getArticuloCodigoBarras(index) + " "
                                + vistamodelo.getArticuloNombreArticulo(index), Level.INFO);
                    }
                }

                BigDecimal totalTarjetas = new BigDecimal(String.valueOf(vistamodelo.getSumaImporteTarjetas(TODAS_TARJETAS)));
                totalTarjetas.setScale(2, RoundingMode.FLOOR);
                int indexTablaArticulos = tabla.getSelectionModel().getSelectedIndex();

                BigDecimal totalVenta = new BigDecimal(String.valueOf(vistamodelo.getImporteVenta()));

                if (!esVentaTA) {

                    if (sustraer.match(event) || menos.match(event)) {
                        if (vistamodelo.getListaArticulos().get(indexTablaArticulos).getPaarticuloid() == ArticuloCupon.ID_ARTICULO_DESCTO_TOTAL
                                || vistamodelo.getListaArticulos().get(indexTablaArticulos).getDevueltos() >= vistamodelo.getListaArticulos().get(indexTablaArticulos).getCantidad()) {
                            return;//No es posible modificar un articulo de descuento o una eliminacion de articulo  
                        } else if (vistamodelo.hayPromociones()) {//Cuando hay descuentos y se intenta modificar 
                            utileriasVenta.getEstatusPopup().addListener(escuchaElimina);
                            utileriasVenta.mostrarPopupPregunta("Es necesario eliminar los descuentos y promociones aplicadas antes de realizar esta operación. "
                                    + "\n\n¿Desea eliminar los descuentos y promociones?", TipoMensaje.PREGUNTA);
                            return;
                        }

                        BigDecimal importeRestar = new BigDecimal(String.valueOf(vistamodelo.getListaArticulos().get(indexTablaArticulos).getPaprecio()));
                        importeRestar.setScale(2, RoundingMode.FLOOR);
                        BigDecimal importeDiferencia = totalVenta.subtract(totalTarjetas).subtract(importeRestar);
                        importeDiferencia.setScale(2, RoundingMode.FLOOR);

                        SION.log(Modulo.VENTA,
                                "Tecla - presionada "
                                + "\n importeRestar: " + importeRestar
                                + "\n importeDiferencia: " + importeDiferencia, Level.INFO);

                        if (importeDiferencia.doubleValue() >= 0) {
                            actualizarRegistroArticulo(tabla, etiquetaMontoVenta, 2);
                        } else {
                            SION.log(Modulo.VENTA, "No es posible hacer la sustracción", Level.INFO);
                            mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.montos.tarjeta.mayor_q_venta"), TipoMensaje.ADVERTENCIA, tabla);
                        }

                    } else if (supr.match(event)) {
                        if (vistamodelo.getListaArticulos().get(indexTablaArticulos).getPaarticuloid() == ArticuloCupon.ID_ARTICULO_DESCTO_TOTAL) {
                            if (supr.match(event)) {//Puedes elimnar todos los descuentos que hay actualmente 
                                utileriasVenta.getEstatusPopup().addListener(escuchaElimina);
                                utileriasVenta.mostrarPopupPregunta("¿Desea eliminar los descuentos y promociones?", TipoMensaje.PREGUNTA);
                            }
                            return;//No es posible modificar un articulo de descuento 
                        }

                        BigDecimal importeRestar = new BigDecimal(String.valueOf(vistamodelo.getListaArticulos().get(indexTablaArticulos).getPaprecio()));
                        BigDecimal importeDiferencia = totalVenta.subtract(totalTarjetas).subtract(importeRestar);

                        SION.log(Modulo.VENTA, "Tecla Supr presionada ", Level.INFO);
                        SION.log(Modulo.VENTA, "importeDiferencia: " + importeDiferencia, Level.INFO);
                        if (importeDiferencia.doubleValue() >= 0) {
                            actualizarRegistroArticulo(tabla, etiquetaMontoVenta, 0);
                        } else {
                            SION.log(Modulo.VENTA, "No es posible hacer la eliminación", Level.INFO);
                            mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.montos.tarjeta.mayor_q_venta"), TipoMensaje.ADVERTENCIA, tabla);
                        }
                    } else if (mas.match(event) || add.match(event)) {
                        SION.log(Modulo.VENTA, "Tecla + presionada", Level.INFO);

                        if (index >= 0) {//valida indice  
                            if ((vistamodelo.getImporteVenta() + vistamodelo.getArticuloPrecio(index)) <= montoMaximoVenta
                                    && //(ventaVistamodelo.getArticuloCantidad(index)+ 
                                    (vistamodelo.getArticuloUnidad(index) + vistamodelo.getArticuloCantidad(index)) < CANTIDAD_MAXIMA_POR_ARTICULO) {

                                actualizarRegistroArticulo(tabla, etiquetaMontoVenta, 1);
                            } else {
                                mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "ventas.excedeCantidadArticulos"), TipoMensaje.INFO, tabla);
                            }
                        }
                    }

                    if (vistamodelo.getImporteVenta() <= 0) {
                        etiquetaMontoVenta.textProperty().set("0.00");
                    } else {
                        etiquetaMontoVenta.textProperty().set(utileriasVenta.getNumeroFormateado(vistamodelo.getImporteVenta()));
                    }

                }
            }
        });

    }
    /////////////////////////////////////////limpieza de variables y componentes 

    public void limpiarVenta() {

        SION.log(Modulo.VENTA, "Inicializando variables.", Level.INFO);

        if (esVentaExitosa || esVentaFueraLineaExitosa) {
            RecargasTADBinding.numero = 0;
            montoAcarreado = 0;

            vistamodelo.limpiarListasDto();
            vistamodelo.limpiarListasBean();
            vista.setMontoRecibidoPantallaCaptura("");
            vista.setMontoCambio("");
            vista.setMontoVenta("0.00");
            vista.setMontoRestante("0.00");
            vista.setMontoEfectivo("");
            vista.setMontoVales("");
            vista.setCantidadVales("");
            vista.setMontoValesE("");
            vista.setMontoTarjetas("");

            vista.setValorEtiquetaCantidadTarjetasRecibidas("0");
            vista.setValorEtiquetaCantidadValesERecibidos("0");
            vista.setValorMontoCantidadTarjetasRecibidas("0.00");
            vista.setValorMontoCantidadValesERecibidos("0.00");

            cambiarCajasComponentes(PANTALLA_CAPTURA, vista.getPanelVentas());

            vista.getTablaVentas().setItems(vistamodelo.getListaArticulos());

            vista.setFocusTablaVentas();

            vista.setDescripcionTarjetaBancaria("");
            vista.setDescripcionNoTarjeta(String.valueOf(""));
            vista.setDescripcionImporteTarjeta("");
            vista.setDescripcionAutorizada("");

            vistamodelo.setPeticionVentaCuponesDto(new DatosCuponesVentaDto());
            configuracionCupones.resetRepositorio();
        }
    }

    public void limpiarVariables(TableView tabla) {
        //hayVentaEfectivoEnProceso = false; 
        hayVentaFLEnProceso = false;
        SION.log(Modulo.VENTA, "Limpiando Variables de Venta", Level.INFO);

        tabla.setItems(vistamodelo.getListaArticulos());

        montoAcarreado = 0;
        montoEfectivo = 0;
        montoVales = 0;
        montoVenta = 0;

        /*
         * longitudAnterior = 0; ultimoValorValido = 0;
         */

        vistamodelo.limpiarListasDto();
        vistamodelo.limpiarListasBean();

        esVentaTA = false;
        esVentaGral = true;
        esVentaPagaTdo = false;

        cadena = "";

        recargasTADVista.limpiarCamposTexto();

        vista.cambiarComponentesTAaVenta(0);

        VistaBarraUsuario.removerObjetoSesion("bloqueoSesion");

        esVentaExitosa = false;
    }

    public void RevisarCajasAlSalir(final BorderPane panel) {

        if (idPantalla == PANTALLA_VENTA) {

            cambiarCajasComponentes(PANTALLA_CAPTURA, panel);
        }
    }

    public void limpiarComponentesTA() {
        vista.cambiarComponentesTAaVenta(0);
        vista.setMontoVenta("0.00");
        vista.setMontoRecibidoPantallaCaptura("");
        vista.setMontoCambio("0.00");
        vista.setMontoRestante("0.00");
        vista.cambiarEstadoCampoImporteRecibido(true);
        vista.getTablaVentas().setItems(vistamodelo.getListaArticulos());
        recargasTADVista.limpiarCamposTexto();


        vistamodelo.getListaArticulosTA().clear();
    }

    public void evaluarBloqueosVenta() {

        vistamodelo.inicializarVariablesBloqueo();

        try {
            vistamodelo.evaluarBloqueoVenta(botonTraspasos, idPantalla);
            if (vistamodelo.existeAvisoBloqueo()) {
                if (idPantalla == PANTALLA_CAPTURA) {
                    mostrarAlertaRetornaFoco(vistamodelo.getMensajeAvisoBloqueo(), TipoMensaje.INFO, vista.getTablaVentas());

                } else {
                    mostrarAlertaRetornaFoco(vistamodelo.getMensajeAvisoBloqueo(), TipoMensaje.INFO, vista.getCampoMontoEfectivo());
                }
            } else {
                if (vistamodelo.existeBloqueo()) {

                    int parametroVentana = 0;

                    try {
                        parametroVentana = Integer.parseInt(SION.obtenerParametro(Modulo.SION, "SEGURIDAD.VENTANADESBLOQUEO.EGRESOVALORES"));
                    } catch (NumberFormatException e) {
                        parametroVentana = 1;
                    }

                    SION.log(Modulo.VENTA, "Se procede a consultar estatus de ventana", Level.FINE);
                    if (verificaVentana.consultaEstatusVentana(parametroVentana)) {
                        SION.log(Modulo.VENTA, "Estatus true, la venta continua ", Level.FINE);

                    } else {
                        SION.log(Modulo.VENTA, "Estatus false, se procede a llamar ventana de traspasos", Level.FINE);
                        Platform.runLater(new Runnable() {
                           
                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlerta(vistamodelo.getMensajeBloqueo(), TipoMensaje.INFO, botonTraspasos);
                            }
                        });

                    }
                } else if (vistamodelo.existe2oAvisoBloqueo()) {
                    if (idPantalla == PANTALLA_CAPTURA) {
                        mostrarAlertaRetornaFoco(vistamodelo.get2oMensajeAvisoBloqueo(), TipoMensaje.INFO, vista.getTablaVentas());
                    } else {
                        mostrarAlertaRetornaFoco(vistamodelo.get2oMensajeAvisoBloqueo(), TipoMensaje.INFO, vista.getCampoMontoEfectivo());
                    }
                } else {
                    if (idPantalla == PANTALLA_CAPTURA) {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                AdministraVentanas.cerrarAlertaFocus(vista.getTablaVentas());
                            }
                        });
                    } else {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                AdministraVentanas.cerrarAlertaFocus(vista.getCampoMontoEfectivo());
                            }
                        });
                    }
                }
            }
        } catch (Exception e) {
            SION.log(Modulo.VENTA, "Ocurrió un error al evaluar los bloqueos del usuario. Error: " + e.toString(), Level.SEVERE);
        }
    }

    //autorizador 
    public void consultarParametrosAutorizacion(final int accion, final int idVenta) {
        SION.log(Modulo.VENTA, "Entrando a método de consulta de parámetros de autorización", Level.INFO);

        if (accion == ACCION_VENTA) {

            vista.cargarLoading();

            SION.log(Modulo.VENTA, "Se procede a consultar parámetros de eliminación de filas en tabla de artículos", Level.INFO);
            vistamodelo.llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacionLinea.sistema")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacionLinea.modulo")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacionLinea.submodulo")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacionLinea.configuracion")));

            if (vistamodelo.consultarParametro() != null) {
                if (vistamodelo.getCodigoErrorParametroOperacion() == 0) {
                    if ((vistamodelo.getCantidadArticulosDevueltos() > 0.0D) && ((int) vistamodelo.getValorConfiguracionParametroOperacion() == 1)) {
                        validarLlamadaAutorizadorVenta(true, idVenta);
                    } else {
                        validarLlamadaAutorizadorVenta(false, idVenta);
                    }
                } else {
                    validarLlamadaAutorizadorVenta(false, idVenta);
                }
            } else {
                validarLlamadaAutorizadorVenta(false, idVenta);
            }

        } else if (accion == ACCION_CANCELACION) {

            SION.log(Modulo.VENTA, "Se procede a consultar parámetros de cancelación de venta", Level.INFO);
            vistamodelo.llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacionVenta.sistema")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacionVenta.modulo")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacionVenta.submodulo")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacionVenta.configuracion")));

            if (vistamodelo.consultarParametro() != null) {
                if ((vistamodelo.getCodigoErrorParametroOperacion() == 0) && ((int) vistamodelo.getValorConfiguracionParametroOperacion() == 1)) {
                    ejecutarAutorizador(idVenta, AUTORIZACION_CANCELACION_VENTA);
                } else {
                    SION.log(Modulo.VENTA, "Código de error recibido diferente de cero, se cancela la venta sin llamar al autorizador", Level.WARNING);
                    cancelarVenta();
                }
            } else {
                SION.log(Modulo.VENTA, "Instancia de respuesta nula, se cancela la venta sin llamar autorizador", Level.SEVERE);
                cancelarVenta();
            }

        } else if (accion == ACCION_VENTA_FL) {
            SION.log(Modulo.VENTA, "Se procede a consultar parámetros de venta fuera de línea", Level.INFO);
            vistamodelo.llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.sistema")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.modulo")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.submodulo")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.configuracion")));

            if (vistamodelo.consultarParametro() != null) {
                if ((vistamodelo.getCodigoErrorParametroOperacion() == 0) && ((int) vistamodelo.getValorConfiguracionParametroOperacion() == 1)) {
                    existeParametroVentaFueraLinea = true;
                } else {
                    existeParametroVentaFueraLinea = false;
                }
            } else {
                existeParametroVentaFueraLinea = false;
            }
            SION.log(Modulo.VENTA, "bandera Venta fuera de línea: " + existeParametroVentaFueraLinea, Level.INFO);



            vistamodelo.llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "forzadoVentaFueraLinea.sistema")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "forzadoVentaFueraLinea.modulo")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "forzadoVentaFueraLinea.submodulo")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "forzadoVentaFueraLinea.configuracion")));

            if (vistamodelo.consultarParametro() != null) {
                if ((vistamodelo.getCodigoErrorParametroOperacion() == 0) && ((int) vistamodelo.getValorConfiguracionParametroOperacion() == 1)) {
                    forzadoVentaFL = true;
                } else {
                    forzadoVentaFL = false;
                }
            } else {
                forzadoVentaFL = false;
            }
            SION.log(Modulo.VENTA, "Parámetro de venta fuera de línea forzado: " + forzadoVentaFL, Level.INFO);
        }

    }

    private void validarLlamadaAutorizadorVenta(boolean seLLamaAutorizador, int idVenta) {
        if (esMontoRecibidoValido()) {
            if (seLLamaAutorizador) {
                ejecutarAutorizador(idVenta, AUTORIZACION_ELIMINACION_LINEA);
            } else {
                validarVentaVales(idVenta, "");
            }
        } else {
            //hayVentaEfectivoEnProceso = false; 
            vista.removerImagenLoading();
            if (idPantalla == PANTALLA_CAPTURA) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "venta.MontoMaximoRecibido"),
                                TipoMensaje.ADVERTENCIA, vista.getTablaVentas());
                        hayVentanaError = true;
                    }
                });

            } else {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.VENTA, "venta.MontoMaximoRecibido"),
                                TipoMensaje.ADVERTENCIA, vista.getCampoMontoEfectivo());
                        hayVentanaError = true;
                    }
                });

            }
        }
    }

    public void ejecutarAutorizador(final int idVenta, final int idOperacion) {

        SION.log(Modulo.VENTA, "Ejecutando autorizador", Level.FINEST);

        final ControladorValidacion controladorValidacion = new ControladorValidacion();

        controladorValidacion.autorizarEmpleadoAccion(String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()),
                Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "ventas.sistema"))),
                Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "ventas.modulo"))),
                Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "ventas.submodulo"))),
                Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "ventas.opcion"))));

        controladorValidacion.getValidacionFinalizada().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observado, Boolean valorViejo, Boolean valorNuevo) {
                if (valorNuevo == true) {

                    RespuestaValidacion respuesta = controladorValidacion.getRespuestaValidacion();
                    if (respuesta == RespuestaValidacion.NO_AUTORIZADO) {

                        regresarFoco(0);
                    }
                    if (respuesta == RespuestaValidacion.AUTENTICADO) {

                        String[] msjAutorizador = null;
                        msjAutorizador = String.valueOf(controladorValidacion.getUsuariosAutorizados().get(0)).substring(0).split("-");
                        autorizador = msjAutorizador[0];

                        SION.log(Modulo.VENTA, "respuesta de autorizador: autenticado", Level.INFO);

                        if (idOperacion == AUTORIZACION_ELIMINACION_LINEA) {
                            SION.log(Modulo.VENTA, "Operación de autorizador: eliminación de linea", Level.INFO);
                            validarVentaVales(idVenta, autorizador);

                        } else if (idOperacion == AUTORIZACION_CANCELACION_VENTA) {
                            SION.log(Modulo.VENTA, "Operación de autorizador: cancelar venta", Level.INFO);
                            cancelarVenta();
                        }

                        regresarFoco(1);
                    }
                    if (respuesta == RespuestaValidacion.ERROR_PARAMETRIZACION_SIN_AUTORIZADORES) {

                        regresarFoco(0);
                    }
                    if (respuesta == RespuestaValidacion.ERROR_CRITICO) {

                        regresarFoco(0);
                    }
                    if (respuesta == RespuestaValidacion.VALIDACION_ABORTADA) {
                        regresarFoco(0);
                    }
                }
            }
        });


    }

    public void regresarFoco(int id) {

        if (id == 0) {
            vista.removerImagenLoading();
        }

        if (idPantalla == PANTALLA_CAPTURA) {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    vista.setFocusTablaVentas();
                }
            });
        } else {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    vista.setFocusCampoImporteEfectivo();
                }
            });
        }
    }

    ///verificaciones 
    public boolean estaStageTarjetasMostrandose() {
        return cobroTarjetaVista.estaStageMostrandose();
    }

    public void cerrarStageTarjetas() {
        cobroTarjetaVista.getStageTarjetas().close();
    }

    public int getCantidadRegistrosTabla() {
        int cantidad = 0;

        cantidad = vistamodelo.getListaArticulos().size();

        return cantidad;
    }

    public TextField getCampoTarjeta() {
        return vista.getCampoMontoTarjeta();
    }

    public void removerLoading() {
        vista.removerImagenLoading();
    }

    public static String getStackTrace(Throwable aThrowable) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }

    public void muetraVentanaNip() {
        cobroTarjetaVista.muestraVentanaNip();
    }
}