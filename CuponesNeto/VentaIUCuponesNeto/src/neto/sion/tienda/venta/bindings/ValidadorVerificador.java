/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.bindings;


import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyCombination.Modifier;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author 702382
 */
public class ValidadorVerificador {

    private int longitudAnterior;
    private String ultimoCadenaValida;
    
    private String numerosValidos;
    
    private boolean esCaracterValido;
    
    private KeyCombination enter;
    
    
    public ValidadorVerificador() {
        
        this.longitudAnterior = 0;
        this.ultimoCadenaValida = "";
        this.numerosValidos = "0123456789";
        this.esCaracterValido = false;
        this.enter = new KeyCodeCombination(KeyCode.ENTER);
    }
    
    
    public void formatearCampoTexto(final TextField campo) {
        
        EventHandler<KeyEvent> eventoValidador = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                
                if(!enter.match(arg0)){
                
                    esCaracterValido=false;
                    
                    if(campo.getText().length()<=18){

                        if(!numerosValidos.contains(arg0.getText())){
                            //arg0.consume();
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    campo.setText("");
                                }
                            });

                            if(ultimoCadenaValida.length()>0){
                                
                                campo.setText(ultimoCadenaValida);
                            }else{
                                
                                ultimoCadenaValida="";
                            }

                        }else{
                            esCaracterValido=true;
                            ultimoCadenaValida=campo.getText();
                            
                        }


                       
                    
                    }else{
                        
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                campo.setText(ultimoCadenaValida);
                                campo.end();
                                campo.deselect();
                            }
                        });
                        
                    }
                
                }
                
            }
        };
        
        campo.addEventFilter(KeyEvent.KEY_PRESSED, eventoValidador);
    
    }
    
    
    
}