/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vistamodelo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import neto.sion.impresion.ventanormal.dto.ServicioTicketDto;
import neto.sion.impresion.ventanormal.dto.VentaServiciosTicketDto;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.binding.AntadBinding;
import neto.sion.tienda.pago.servicios.dao.PagoServiciosDao;
import neto.sion.tienda.pago.servicios.modelo.*;
import neto.sion.tienda.venta.cliente.servicios.antad.dto.*;
import neto.sion.tienda.venta.cliente.servicios.antad.serviceFacade.ConsumirAntadPagosServicioImp;
import neto.sion.tienda.venta.conciliacion.bean.PeticionActTransaccionLocalBean;
import neto.sion.tienda.venta.conciliacion.bean.RespuestaActualizacionLocalBean;
import neto.sion.tienda.venta.conciliacion.controlador.ConciliacionLocalControlador;
import neto.sion.tienda.venta.fueralinea.bean.PagoServicioBean;
import neto.sion.tienda.venta.fueralinea.bean.PeticionPagoServicioLocalBean;
import neto.sion.tienda.venta.fueralinea.bean.RespuestaPagoServicioLocalBean;
import neto.sion.tienda.venta.fueralinea.bean.TipoPagoBean;
import neto.sion.tienda.venta.fueralinea.controlador.VentaFueraLineaControlador;
import neto.sion.tienda.venta.impresiones.ImpresionVenta;
import neto.sion.tienda.venta.utilerias.bean.RespuestaParametroBean;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;

/**
 *
 * @author fvegap
 */
public class AntadVistaModelo {

    private PagoServiciosVistaModelo vm;
    private UtileriasVenta utilerias;
    private final int AGRUPACION_ANTAD = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.IDAGRUPACION.ANTAD").trim());
    private final int AGRUPACION_PAGATODO = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.IDAGRUPACION.PAGATODO").trim());
    private final int CATEGORIA_ANTAD_CONSULTASKU = 2;
    private PeticionConsultaDto peticionConsultaAntad;
    private RespuestaConsultaDto respuestaConsultaAntad;
    private PeticionAutorizacionDto peticionAutorizacionAntad;
    private RespuestaAutorizacionDto respuestaAutorizacionAntad;
    private ConsumirAntadPagosServicioImp clienteAntad;
    private AntadBinding antadBinding;
    private VentaFueraLineaControlador pagoFueraLinea;
    private PeticionPagoServicioLocalBean peticionPagoLocal;
    private RespuestaPagoServicioLocalBean respuestaPagoLocal;
    private PeticionActualizacionPagoFueraLineaBean peticionActualizacionPagoFL;
    private RespuestaActualizacionPagoFueraLineaBean respuestaActualizacionPagoFL;
    boolean existeProblemaComunicacion = false;
    boolean seImprimioTicketExitoso = false;
    private final int PAGO_EXITOSO_CENTRAL = 3;
    private final int PAGO_POR_CONFIRMAR_CENTRAL = 9;
    
    private RespuestaConsultaPagoAntadDto respuestaConsultaBDCentral;

    public AntadVistaModelo(PagoServiciosVistaModelo _vm, UtileriasVenta _utilerias, AntadBinding _antadBinding) {

        this.vm = _vm;
        this.utilerias = _utilerias;
        this.antadBinding = _antadBinding;

        //antad
        this.clienteAntad = new ConsumirAntadPagosServicioImp();

        ///fuera de linea
        this.pagoFueraLinea = new VentaFueraLineaControlador();
        this.peticionPagoLocal = new PeticionPagoServicioLocalBean();
        this.respuestaPagoLocal = new RespuestaPagoServicioLocalBean();
        this.peticionActualizacionPagoFL = new PeticionActualizacionPagoFueraLineaBean();
        this.respuestaActualizacionPagoFL = new RespuestaActualizacionPagoFueraLineaBean();

    }

    //getters
    public PeticionAutorizacionDto getPeticionAutorizacionAntad() {
        return peticionAutorizacionAntad;
    }

    public PeticionConsultaDto getPeticionConsultaAntad() {
        return peticionConsultaAntad;
    }

    public RespuestaAutorizacionDto getRespuestaAutorizacionAntad() {
        return respuestaAutorizacionAntad;
    }

    public RespuestaConsultaDto getRespuestaConsultaAntad() {
        return respuestaConsultaAntad;
    }

    public PeticionPagoServicioLocalBean getPeticionPagoLocal() {
        return peticionPagoLocal;
    }

    public RespuestaPagoServicioLocalBean getRespuestaPagoLocal() {
        return respuestaPagoLocal;
    }

    public RespuestaConsultaPagoAntadDto getRespuestaConsultaBDCentral() {
        return respuestaConsultaBDCentral;
    }
    
    

    //consulta
    public void consultaPagoAntad(boolean _esConsultaSKU, long _emisorId, String _referencia, double _monto) {

        peticionConsultaAntad = null;
        peticionConsultaAntad = new PeticionConsultaDto();

        boolean seGeneroConciliacion = false;

        //se genera numero de conciliacion
        vm.generarConciliacion(
                Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SISTEMA")),//1
                Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.MODULO")),//2
                Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SUBMODULO.ANTAD")));//11

        if (vm.getRespuestaConciliacionLocalBean() != null) {
            SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de conciliacion: " + vm.getRespuestaConciliacionLocalBean().toString(), Level.INFO);
            if (vm.getRespuestaConciliacionLocalBean().getPaCdgError() == 0
                    && vm.getRespuestaConciliacionLocalBean().getPaConciliacionId() > 0) {
                seGeneroConciliacion = true;
            }
        }

        if (seGeneroConciliacion) {

            boolean seRecibioConsultaExitosa = false;

            peticionConsultaAntad.setPaConciliacionId(vm.getRespuestaConciliacionLocalBean().getPaConciliacionId());
            peticionConsultaAntad.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
            peticionConsultaAntad.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
            peticionConsultaAntad.setConsultaSKU(generaConsultaSKUAntad(buscaSKUxId((int) _emisorId)));


            respuestaConsultaAntad = null;
            respuestaConsultaAntad = new RespuestaConsultaDto();

            try {
                respuestaConsultaAntad = clienteAntad.consultarPago(peticionConsultaAntad);
                SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de ConsultaSKU: " + respuestaConsultaAntad.toString(), Level.INFO);
                seRecibioConsultaExitosa = true;
            } catch (Exception ex) {
                SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex, "Ocurrió un error al intentar realizar la consulta del pago: " + ex.getLocalizedMessage());
            }

            //valida que se haya recibido respuesta
            if (seRecibioConsultaExitosa) {
                //v<lida codigo de error
                if (respuestaConsultaAntad.getCodigoError() == 0) {
                    agregaReferenciaAntad(_emisorId, _referencia, _monto);
                }

            } else {

                //cancelar conciliacion
                cancelarConciliacionVenta(true);

                respuestaConsultaAntad = null;
                respuestaConsultaAntad = new RespuestaConsultaDto();
                respuestaConsultaAntad.setRespuestaConsultaSKUAntad(null);
                respuestaConsultaAntad.setCodigoError(-2);
                respuestaConsultaAntad.setDescError("Ocurrió un error al validar el pago. Por favor, intente más tarde.");
            }

        } else {
            SION.log(Modulo.PAGO_SERVICIOS, "EL proceso de consulta de pago se detendra debido a que no se logro generar un numero de conciliacion", Level.INFO);

            respuestaConsultaAntad = null;
            respuestaConsultaAntad = new RespuestaConsultaDto();
            respuestaConsultaAntad.setRespuestaConsultaSKUAntad(null);
            respuestaConsultaAntad.setCodigoError(-1);
            respuestaConsultaAntad.setDescError("En estos momentos no es posible realizar el pago, por favor contacte a Soporte Técnico");
        }

    }

    public void agregaReferenciaAntad(long _emisorId, String _referencia, double _monto) {

        SION.log(Modulo.PAGO_SERVICIOS, "Agregando a lista de referencias validadas", Level.INFO);
        vm.getListaReferenciasValidadas().add(new ReferenciaValidaBean(_referencia, "", (int) _emisorId, respuestaConsultaAntad));
        SION.log(Modulo.PAGO_SERVICIOS, "Lista de referencias actualizada: " + vm.getListaReferenciasValidadas(), Level.INFO);


        SION.log(Modulo.PAGO_SERVICIOS, "Agregando registro a lista de pagos acumulados", Level.INFO);


        double comision = 0;
        double monto = 0;

        if (respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getComision() != null) {
            if (!respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getComision().isEmpty()) {
                comision = Double.parseDouble(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getComision());
            }
        }

        if (respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getMonto() != null) {
            if (!respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getMonto().isEmpty()) {
                monto = Double.parseDouble(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getMonto());
                //si el monto recibido es diferente de cero, sera el enviado en la autorizacion sino el capturado por el cajero
                if (monto <= 0) {
                    SION.log(Modulo.PAGO_SERVICIOS, "Monto recibido en consulta menor o igual a cero, se usara el capturado por cajero: " + _monto, Level.INFO);
                    monto = _monto;
                } else {
                    SION.log(Modulo.PAGO_SERVICIOS, "Monto recibido en consulta mayor a cero, se continua operacion con la nueva cantidad", Level.INFO);
                }
            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "Monto recibido en consulta es nulo, se usara el capturado por cajero: " + _monto, Level.INFO);
                monto = _monto;
            }
        }

        vm.getListaPagosAcumulados().add(new ServicioBean(
                true, //check en tabla
                String.valueOf(_emisorId), //ruta del logo
                (int) _emisorId, //emisor
                buscaNombreEmisorXId((int) _emisorId), //nombre del emisor
                1, //tipopago
                comision,//comision
                monto, //importe
                _referencia, //referencia
                0.0, //impuesto
                ""));  //deposito
        SION.log(Modulo.PAGO_SERVICIOS, "Lista de pagos actualizada: " + vm.getListaPagosAcumulados().toString(), Level.INFO);

    }

    private ConsultaSKUDto generaConsultaSKUAntad(String _sku) {
        ConsultaSKUDto consultaSKU = new ConsultaSKUDto();

        consultaSKU.setCaja(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "antad.caja.neto"));
        consultaSKU.setCajero(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "antad.cajero.neto"));
        consultaSKU.setComercio(Integer.valueOf(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "antad.comercio.neto")));
        consultaSKU.setHorario(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "antad.horario.neto"));
        consultaSKU.setSKU(_sku);

        
       consultaSKU.setSucursal(String.valueOf(VistaBarraUsuario.getSesion().getTiendaId()));


        consultaSKU.setmCertificado(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "antad.certificado.neto"));

        return consultaSKU;
    }

    public void cargarReferenciaAntad(final int _index, final int _tipoPago, final double _importe, final String _referencia) {

        vm.getListaPagosAcumulados().add(
                new ServicioBean(
                true, //true si aparece con check en tabla 
                String.valueOf(vm.getListaEmpresas().get(_index).getEmpresaId()), //ruta del logo
                vm.getListaEmpresas().get(_index).getEmpresaId(), //emisorId
                vm.getListaEmpresas().get(_index).getDescrpcion(), //nombe del emisor
                1, //id del tipode pago
                Double.parseDouble(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getComision()), //comision
                Double.parseDouble(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getMonto()), //importe
                _referencia, //referencia
                0, //impuesto
                "" //deposito
                ));

    }

    //autorizacion
    public void autorizaPagoAntad(double _montoRecibido, String _referencia) {

        SION.log(Modulo.PAGO_SERVICIOS, "Paso 1/4: Generar peticion de Autorizacion", Level.INFO);

        peticionAutorizacionAntad = null;
        peticionAutorizacionAntad = new PeticionAutorizacionDto();

        String t = VistaBarraUsuario.getSesion().getIpEstacion();
        t = t.concat("|CAJA");
        t = t.concat(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));

        peticionAutorizacionAntad.setAutorizacion(generaAutorizacion(_referencia));
        peticionAutorizacionAntad.setFecha(obtenerFecha());
        peticionAutorizacionAntad.setFueraDeLinea(0);
        //peticionAutorizacionAntad.setMontoTotal(Double.parseDouble(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getMonto()));
        peticionAutorizacionAntad.setMontoTotal(Double.parseDouble(utilerias.getNumeroFormateado(vm.getListaPagosAcumulados().get(0).getImporte()
                + vm.getListaPagosAcumulados().get(0).getComision())));
        peticionAutorizacionAntad.setNumTransaccion(0);
        peticionAutorizacionAntad.setPaConciliacionId(vm.getRespuestaConciliacionLocalBean().getPaConciliacionId());
        peticionAutorizacionAntad.setPaPaisId(peticionConsultaAntad.getPaPaisId());
        peticionAutorizacionAntad.setTerminal(t);
        peticionAutorizacionAntad.setTiendaId(peticionConsultaAntad.getTiendaId());
        peticionAutorizacionAntad.setTipoMovto(7);
        peticionAutorizacionAntad.setUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());
        peticionAutorizacionAntad.setArregloTiposPago(generaArregloTiposPago(_montoRecibido));

        SION.log(Modulo.PAGO_SERVICIOS, "Paso 1/4 completo.", Level.INFO);

        //grabar pago fl antes de enviar a central, sirve par validar duplicados antes de enviar a antad
        boolean seGraboRegistroFL = false;

        int intentosPagoFL = 3;
        int intento = 1;

        SION.log(Modulo.PAGO_SERVICIOS, "Paso 2/4: Grabar registro localmente", Level.INFO);
        do {
            SION.log(Modulo.PAGO_SERVICIOS, "Intento No. " + intento + " de pago fuera de linea", Level.INFO);
            grabarPagoFueraLinea();

        } while (intento <= intentosPagoFL && respuestaPagoLocal == null);

        if (respuestaPagoLocal != null) {

            if (respuestaPagoLocal.getCodigoError() == 0) {

                seGraboRegistroFL = true;
                SION.log(Modulo.PAGO_SERVICIOS, "Paso 2/4 completo.", Level.INFO);
            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "Paso 2/4 incompleto, termina proceso de aplicación de pago.", Level.WARNING);
            }

        }

        
        if (seGraboRegistroFL) {

            respuestaAutorizacionAntad = null;
            respuestaAutorizacionAntad = new RespuestaAutorizacionDto();

            existeProblemaComunicacion = false;

            try {
                SION.log(Modulo.PAGO_SERVICIOS, "Paso 3/4: Envío de pago a Central.", Level.INFO);
                respuestaAutorizacionAntad = clienteAntad.aplicarPago(peticionAutorizacionAntad);
                SION.log(Modulo.PAGO_SERVICIOS, "Paso 3/4 completo, respuesta de central recibida, validando codigo de respuesta.", Level.INFO);
                //throw new Exception("error de prueba");
            } catch (Exception ex) {
                existeProblemaComunicacion = true;
                SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex, "Paso 3/4 incompleto, termina proceso de aplicacion del pago: " + ex.getLocalizedMessage());
            }
            
            //existeProblemaComunicacion = true;

            if (!existeProblemaComunicacion) {
                //valida si la autorizacion fue exitosa
                SION.log(Modulo.PAGO_SERVICIOS, "Paso 4/4: Validacion de codigo de respuesta, actualizacion de pago bd local e impresion de ticket.", Level.INFO);


                /*
                 * respuestaAutorizacionAntad.setCodigoError(0);
                 * respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().setFolioTransaccion("111111");
                 * respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().setFolioComercio("150");
                 * respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().setNumAuth("012345");
                 * respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().setRespCode("00");
                 * respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().setDatosAdicionales("!!!!");
                 */
                
                
                if (respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getRespCode()!=null &&
                    respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getRespCode().equals("PA")    ){
                    
                     SION.log(Modulo.PAGO_SERVICIOS, "El pago que se intenta ya esta pagado en antad", Level.INFO);
                    cancelarConciliacionVenta(true);

                    //se cancela operacion debido a que no se pudo guardar venta en bd local

                    //valida si existe respuesta de local lo cual indica un error generado en bd, caso contrario ocurrio una exceocion al conectar
                    respuestaAutorizacionAntad.setDescError("No se pudo realizar el pago,: " + respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeTicket());

                    respuestaAutorizacionAntad.setCodigoError(-7);
                    respuestaAutorizacionAntad.setMovimientoId(0);
                    respuestaAutorizacionAntad.setPaArrayBloqueos(null);
                    respuestaAutorizacionAntad.setRespuestaAutorizacionAntad(null);
                    respuestaAutorizacionAntad.setTransaccionId(0);
                    
                    
                }else{
                    
                    if (respuestaAutorizacionAntad.getCodigoError() == 0) {
                        //respuesta exitosa 
                        SION.log(Modulo.PAGO_SERVICIOS, "Codigo de respuesta exitoso, actualizando folio central", Level.INFO);

                        //actualizar el registro de pago fl creado al principio
                        actualizarEstatusLocal(PAGO_EXITOSO_CENTRAL, true, false);

                        //impresion del ticket
                        double montoCambio = _montoRecibido - Double.parseDouble(utilerias.getNumeroFormateado(peticionAutorizacionAntad.getMontoTotal()));

                        imprimirTicketAntad(_montoRecibido, montoCambio, false, true, false);

                        //actualizar sesion
                        vm.agregarObjetoSesion(arrayBloqueosAntad2Venta(), respuestaAutorizacionAntad.getCodigoError());

                        //guardar venta del dia
                        guardarVentaDia();

                        //evalua bloqueos
                        antadBinding.evaluarBloqueosVenta();

                    } else if (respuestaAutorizacionAntad.getCodigoError() == -3) {
                        //en este caso el pago se afecto correctamente en antad, sin embargo ocurrio un problema al marcarlo en bd central por lo que se grabara fuera de linea

                        completarVentaExitosaPagoPendiente(_montoRecibido, true);

                    } else {
                    
                        //error en la respuesta

                        SION.log(Modulo.PAGO_SERVICIOS, "Se recibe codigo de error, se limpian listas y se muestra msj de error", Level.INFO);

                        //actualizar mensaje error
                        PagoServiciosDao dao = new PagoServiciosDao();
                        peticionActualizacionPagoFL = new PeticionActualizacionPagoFueraLineaBean();
                        peticionActualizacionPagoFL.setConciliacionId(peticionAutorizacionAntad.getPaConciliacionId());
                        peticionActualizacionPagoFL.setEstatusId(5);
                        if (respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeCajero() != null) {
                            peticionActualizacionPagoFL.setRespuestaAntad(respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeCajero());
                        } else {
                            peticionActualizacionPagoFL.setRespuestaAntad("Ocurrió un error al aplicar el pago en WS Antad, no se obtuvo mensaje de respuesta.");
                        }
                        
                        peticionActualizacionPagoFL.setTransaccionId(0);
                        respuestaActualizacionPagoFL = new RespuestaActualizacionPagoFueraLineaBean();
                        respuestaActualizacionPagoFL = dao.actualizarPagoFL(peticionActualizacionPagoFL, 5);

                        cancelarConciliacionVenta(false);
                        vm.limpiaParcial();

                    }
                    
                }
            } else {

                //si existe problema de comunicacion se actualizara a estatus 9 para ser tomada por demonio y ser conciliada posteriormente
                SION.log(Modulo.PAGO_SERVICIOS, "Paso 4 incompleto, se procede a consultar pago en Central ya que no se obtuvo respuesta.", Level.INFO);
                
                int numeroIntentos = 3;
                int intentoConsulta = 1;
                boolean seRealizoConsulta = false;
                boolean seEncontroPago = false;
                boolean elPagoFueExitoso = false;
                
                do{
                    
                    SION.log(Modulo.PAGO_SERVICIOS, "Intento de consulta "+intento+"/"+numeroIntentos, Level.INFO);
                    SION.log(Modulo.PAGO_SERVICIOS, "Peticicion de consulta de pago a BD Central: "+peticionAutorizacionAntad.toString(), Level.INFO);
                    try {
                        respuestaConsultaBDCentral = clienteAntad.consultarPagoAntad(peticionAutorizacionAntad);
                        seRealizoConsulta = true;
                        SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de consulta de pago: "+respuestaConsultaBDCentral.toString(), Level.INFO);
                    } catch (Exception ex) {
                        seRealizoConsulta = false;
                        SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex, "Ocurrio un error a realizar consulta de pago: "+ex.getLocalizedMessage());
                    }
                    
                }while(intento<=numeroIntentos && !seRealizoConsulta);
                
                if(seRealizoConsulta){
                    if(respuestaConsultaBDCentral.getCodigoError()==1){
                        //pago encontrado en central
                        seEncontroPago = true;
                        if(respuestaConsultaBDCentral.getFolioAntad()>0 && respuestaConsultaBDCentral.getTransaccionId()>0){
                            elPagoFueExitoso = true;
                        }
                    }
                }else{
                    completarVentaExitosaPagoPendiente(_montoRecibido, false);
                }
                
                if(seEncontroPago){
                    
                    if(elPagoFueExitoso){
                        
                        //consulta exitosa con pago autorizado
                        SION.log(Modulo.PAGO_SERVICIOS, "Consulta de pago exitosa, se encuentra pago aplicado en BD Central", Level.INFO);

                        //actualizar el registro de pago fl creado al principio
                        actualizarEstatusLocal(PAGO_EXITOSO_CENTRAL, true, true);

                        //impresion del ticket
                        double montoCambio = _montoRecibido - Double.parseDouble(utilerias.getNumeroFormateado(peticionAutorizacionAntad.getMontoTotal()));

                        imprimirTicketAntad(_montoRecibido, montoCambio, false, true, true);

                        //actualizar sesion - no se actualizan objetos de sesion ya q consultade pago no trae bloqueos
                        //vm.agregarObjetoSesion(arrayBloqueosAntad2Venta(), respuestaConsultaBDCentral.getCodigoError());

                        //guardar venta del dia
                        guardarVentaDia();

                        //evalua bloqueos
                        //antadBinding.evaluarBloqueosVenta();
                        
                    }else{
                        
                        //consulta exitosa con pago rechazado
                        SION.log(Modulo.PAGO_SERVICIOS, "Consulta de pago exitosa, se encuentra pago rechazado en la consulta a BD Central", Level.INFO);
                        
                        //actualizar mensaje error
                        PagoServiciosDao dao = new PagoServiciosDao();
                        peticionActualizacionPagoFL = new PeticionActualizacionPagoFueraLineaBean();
                        peticionActualizacionPagoFL.setConciliacionId(peticionAutorizacionAntad.getPaConciliacionId());
                        peticionActualizacionPagoFL.setEstatusId(5);
                        peticionActualizacionPagoFL.setRespuestaAntad(respuestaConsultaBDCentral.getMensaejAntad());
                        
                        //llenar respuesta autorizacion q se valida en binding
                        respuestaAutorizacionAntad.setCodigoError(-6);
                        respuestaAutorizacionAntad.setMovimientoId(0);
                        respuestaAutorizacionAntad.setPaArrayBloqueos(null);
                        respuestaAutorizacionAntad.setRespuestaAutorizacionAntad(null);
                        respuestaAutorizacionAntad.setTransaccionId(0);
                        
                        peticionActualizacionPagoFL.setTransaccionId(0);
                        respuestaActualizacionPagoFL = new RespuestaActualizacionPagoFueraLineaBean();
                        respuestaActualizacionPagoFL = dao.actualizarPagoFL(peticionActualizacionPagoFL, 5);

                        cancelarConciliacionVenta(false);
                        vm.limpiaParcial();
                        
                    }
              
                }else{
                    
                    //consulta exitosa no se encontro el pago
                    SION.log(Modulo.PAGO_SERVICIOS, "Consulta de pago exitosa, el pago no se encontro", Level.INFO);
                        

                    //actualizar mensaje error
                    PagoServiciosDao dao = new PagoServiciosDao();
                    peticionActualizacionPagoFL = new PeticionActualizacionPagoFueraLineaBean();
                    peticionActualizacionPagoFL.setConciliacionId(peticionAutorizacionAntad.getPaConciliacionId());
                    peticionActualizacionPagoFL.setEstatusId(5);
                    peticionActualizacionPagoFL.setRespuestaAntad(respuestaConsultaBDCentral.getMensaejAntad());
                        
                    respuestaAutorizacionAntad.setCodigoError(-5);
                    respuestaAutorizacionAntad.setMovimientoId(0);
                    respuestaAutorizacionAntad.setPaArrayBloqueos(null);
                    respuestaAutorizacionAntad.setRespuestaAutorizacionAntad(null);
                    respuestaAutorizacionAntad.setTransaccionId(0);
                        
                    peticionActualizacionPagoFL.setTransaccionId(0);
                    respuestaActualizacionPagoFL = new RespuestaActualizacionPagoFueraLineaBean();
                    respuestaActualizacionPagoFL = dao.actualizarPagoFL(peticionActualizacionPagoFL, 5);

                    cancelarConciliacionVenta(false);
                    vm.limpiaParcial();
                    
                }

                

            }


        } else {

            cancelarConciliacionVenta(true);

            //se cancela operacion debido a que no se pudo guardar venta en bd local
            respuestaAutorizacionAntad = new RespuestaAutorizacionDto();

            //valida si existe respuesta de local lo cual indica un error generado en bd, caso contrario ocurrio una exceocion al conectar
            if (respuestaPagoLocal != null) {
                if (respuestaPagoLocal.getDescError() != null) {
                    respuestaAutorizacionAntad.setDescError(respuestaPagoLocal.getDescError());
                } else {
                    respuestaAutorizacionAntad.setDescError("Ocurrió un error al intentar aplicar el pago. Por favor, contacte a soporte técnico.");
                }
            }


            respuestaAutorizacionAntad.setCodigoError(-2);
            respuestaAutorizacionAntad.setMovimientoId(0);
            respuestaAutorizacionAntad.setPaArrayBloqueos(null);
            respuestaAutorizacionAntad.setRespuestaAutorizacionAntad(null);
            respuestaAutorizacionAntad.setTransaccionId(0);
        }


    }

    /*
     * public boolean seCompletoAutorizacion(){ return seCompletoAutorizacion; }
     */
    public void completarVentaExitosaPagoPendiente(double _montoRecibido, final boolean seRecibioRespuesta) {

        int intentosActualizacionFL = 3;
        int intento = 1;

        do {
            SION.log(Modulo.PAGO_SERVICIOS, "Intento No. " + intento + " de actualizacion del pago fuera de linea", Level.INFO);
            //intento de actualizacion del pago en nuevo estatus
            //en caso de no lograr actualizar no se hace nada en front
            actualizarEstatusLocal(PAGO_POR_CONFIRMAR_CENTRAL, seRecibioRespuesta, false);

        } while (intento <= intentosActualizacionFL && respuestaActualizacionPagoFL == null);

        if (respuestaPagoLocal != null) {

            if (respuestaPagoLocal.getCodigoError() == 0) {
                //pago fl exitoso

                imprimirTicketAntad(_montoRecibido, _montoRecibido - peticionAutorizacionAntad.getMontoTotal(), true, seRecibioRespuesta, false);

                guardarVentaDia();

                antadBinding.evaluarBloqueosVenta();

            }

        }

    }

    public void actualizarEstatusLocal(int _estatus, boolean _seRecibioRespuestaAntad, boolean _seConsultoPago) {

        peticionActualizacionPagoFL = null;
        peticionActualizacionPagoFL = new PeticionActualizacionPagoFueraLineaBean();
        respuestaActualizacionPagoFL = null;
        respuestaActualizacionPagoFL = new RespuestaActualizacionPagoFueraLineaBean();
        PagoServiciosDao dao = new PagoServiciosDao();

        peticionActualizacionPagoFL.setConciliacionId(peticionConsultaAntad.getPaConciliacionId());
        if (_estatus == PAGO_EXITOSO_CENTRAL) {
            if(_seConsultoPago){
                SION.log(Modulo.PAGO_SERVICIOS, "Actualizando transaccion: " + respuestaConsultaBDCentral.getTransaccionId(), Level.INFO);
                peticionActualizacionPagoFL.setTransaccionId(respuestaConsultaBDCentral.getTransaccionId());
            }else{
                SION.log(Modulo.PAGO_SERVICIOS, "Actualizando transaccion: " + respuestaAutorizacionAntad.getTransaccionId(), Level.INFO);
                peticionActualizacionPagoFL.setTransaccionId(respuestaAutorizacionAntad.getTransaccionId());
            }
        } else {
            //peticionActualizacionPagoFL.setTransaccionId(0);
        }
        peticionActualizacionPagoFL.setEstatusId(_estatus);

        String respuestaAntad = "";

        if (_seRecibioRespuestaAntad) {

            //entra este caso cuando se recibe una respuesta del ws antad y sera considerado por el demonio
            //en este caso solo falta ser aplicado en bd central pero se tiene certeza que el pago fue aplicado en antad

            boolean existeMensajeCajero = false;
            String mensajeRespuestaAntad = "";

            if(_seConsultoPago){

                mensajeRespuestaAntad = respuestaConsultaBDCentral.getMensaejAntad();
                
                //agrega numero de autorizacion antad
                respuestaAntad = String.valueOf(respuestaConsultaBDCentral.getFolioAntad()).concat("||").concat(mensajeRespuestaAntad);
            
            }else{
                
                if (respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeCajero() != null) {
                    if (!respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeCajero().isEmpty()) {
                        mensajeRespuestaAntad = respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeCajero();
                        existeMensajeCajero = true;
                    }
                }

                if (respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeTicket() != null) {
                    if (!respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeTicket().isEmpty()) {

                        if (existeMensajeCajero) {
                            mensajeRespuestaAntad = mensajeRespuestaAntad.concat("|");
                        }

                        mensajeRespuestaAntad = mensajeRespuestaAntad.concat(respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeTicket());
                    }
                }
                
                //agrega numero de autorizacion antad
                respuestaAntad = String.valueOf(respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getNumAuth()).concat("||").concat(mensajeRespuestaAntad);
                
            }
            
        } else {

            //entra cuando ocurre una excepcion donde no se pudo applicar el pago, se marca como fuera de linea para que la tome el demonio
            //puede ocurrir que sea tomada y marcada por demonio sin haber aplicado en antad, en este caso entra en la conciliacion

            //se concatena 0 como folio antad y msj generico 

            respuestaAntad = "0||".concat("Pago de Servicio Antad registrado Fuera de linea. ");

        }



        //agrega campo operacion
        respuestaAntad = String.valueOf(peticionAutorizacionAntad.getAutorizacion().getOperacion()).concat("||").concat(respuestaAntad);

        //agrega campo SKU
        //respuestaAntad = String.valueOf(peticionAutorizacionAntad.getAutorizacion().getSKU()).concat("||").concat(respuestaAntad);
        //Se cambia 11/08/2015 por campo folio transaccion XCD para q se guarde en lugar de sku
        if(_seConsultoPago){
            respuestaAntad = String.valueOf(respuestaConsultaBDCentral.getFolioAntad()).concat("||").concat(respuestaAntad);
        }else{
            respuestaAntad = String.valueOf(respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getFolioTransaccion()).concat("||").concat(respuestaAntad);
        }
        //agrega campo modo ingreso
        respuestaAntad = String.valueOf(peticionAutorizacionAntad.getAutorizacion().getModoIngreso()).concat("||").concat(respuestaAntad);

        peticionActualizacionPagoFL.setRespuestaAntad(respuestaAntad);

        SION.log(Modulo.PAGO_SERVICIOS, "Peticion de actualizacion de pago: " + peticionActualizacionPagoFL.toString(), Level.INFO);

        respuestaActualizacionPagoFL = dao.actualizarPagoFL(peticionActualizacionPagoFL, _estatus);

        SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de actualizacion de pago: " + respuestaActualizacionPagoFL.toString(), Level.INFO);
    }

    public AutorizacionDto generaAutorizacion(String _referencia) {

        AutorizacionDto autorizacion = new AutorizacionDto();

        long comercio = 0;
        //double comision = 0;
        double monto = 0;

        autorizacion.setCaja(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getCaja());
        autorizacion.setCajero(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getCajero());
        autorizacion.setCodigoBarras(getCodigoBarrasFromListaEmpresas(Long.parseLong("65".concat(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getEmisor())),
                vm.getRespuestaEmpresasBean()));

        if (respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getComercio() != null) {
            if (!respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getComercio().isEmpty()) {
                comercio = Long.parseLong(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getComercio());
            }
        }
        autorizacion.setComercio(comercio);

        if (respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getComision() != null) {
            if (!respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getComision().isEmpty()) {
                //comision = Double.parseDouble(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getComision());
                autorizacion.setComision(Double.valueOf(utilerias.getNumeroFormateado(Double.parseDouble(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getComision()))));
            } else {
                autorizacion.setComision(0.00);
            }
        } else {
            autorizacion.setComision(0.00);
        }


        autorizacion.setEmisor("65".concat(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getEmisor()));
        autorizacion.setFolioComercio(Long.parseLong(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "antad.foliocomercio.neto").trim()));
        autorizacion.setHorario(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getHorario());
        autorizacion.setModoIngreso("01");

        if (respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getMonto() != null) {
            if (!respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getMonto().isEmpty()) {
                monto = Double.parseDouble(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getMonto());
            }
        }
        if (monto > 0) {
            autorizacion.setMonto(monto);
        } else {
            autorizacion.setMonto(Double.parseDouble(utilerias.getNumeroFormateado(vm.getListaPagosAcumulados().get(0).getImporte())));
        }

        autorizacion.setOperacion(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getOperacion());
        autorizacion.setReferencia(_referencia);
        autorizacion.setReferencia2("");
        autorizacion.setReferencia3("");
        autorizacion.setReintento(0);
        autorizacion.setSKU(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getSKU());
        autorizacion.setSucursal(respuestaConsultaAntad.getRespuestaConsultaSKUAntad().getConsultaResult().getSucursal());



        autorizacion.setTicket(String.valueOf(peticionConsultaAntad.getPaConciliacionId()));
        autorizacion.setmCertificado(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "antad.certificado.neto").trim());

        return autorizacion;
    }

    public void grabarPagoFueraLinea() {

        SION.log(Modulo.PAGO_SERVICIOS, "Comienza proceso de pago Fuera de Linea", Level.INFO);

        peticionPagoLocal = null;
        peticionPagoLocal = new PeticionPagoServicioLocalBean();

        //inicializar

        String t = VistaBarraUsuario.getSesion().getIpEstacion();
        t = t.concat("|CAJA");
        t = t.concat(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));

        peticionPagoLocal.setArrayPagos(tipoPagoDto2Bean(peticionAutorizacionAntad.getArregloTiposPago()));
        peticionPagoLocal.setArrayServicios(autorizacion2PagoServicioFL());
        peticionPagoLocal.setConciliacionId(peticionAutorizacionAntad.getPaConciliacionId());
        peticionPagoLocal.setIndicador(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "pago.local.indicador")));//7
        peticionPagoLocal.setModuloId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.MODULO")));//2
        peticionPagoLocal.setPais(peticionAutorizacionAntad.getPaPaisId());
        peticionPagoLocal.setSistemaId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SISTEMA")));//1
        peticionPagoLocal.setSubModuloId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SUBMODULO.ANTAD")));//11
        peticionPagoLocal.setTerminal(t);
        peticionPagoLocal.setTiendaId(peticionAutorizacionAntad.getTiendaId());
        peticionPagoLocal.setTotalVenta(peticionAutorizacionAntad.getMontoTotal());
        peticionPagoLocal.setUsuarioId(peticionAutorizacionAntad.getUsuarioId());

        respuestaPagoLocal = null;
        respuestaPagoLocal = new RespuestaPagoServicioLocalBean();

        SION.log(Modulo.PAGO_SERVICIOS, "Peticion de registro de PS ANtad fuera de linea: " + peticionPagoLocal.toString(), Level.INFO);

        respuestaPagoLocal = pagoFueraLinea.spPagoServicioLocal(peticionPagoLocal);

        SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de registro de PS Antad: " + respuestaPagoLocal.toString(), Level.INFO);

    }

    public TipoPagoDto[] generaArregloTiposPago(double _montoRecibido) {
        TipoPagoDto[] arreglo = new TipoPagoDto[1];

        TipoPagoDto tipoPago = new TipoPagoDto();

        tipoPago.setMontoPago(peticionAutorizacionAntad.getMontoTotal());
        tipoPago.setImporteAdicional(_montoRecibido);
        tipoPago.setNumeroVales(0);
        tipoPago.setPagoTarjetaIdBus(0);
        tipoPago.setTipoPagoId(1);

        arreglo[0] = tipoPago;

        return arreglo;
    }

    private PagoServicioBean[] autorizacion2PagoServicioFL() {
        ArrayList<PagoServicioBean> pagos = new ArrayList<PagoServicioBean>();

        PagoServicioBean pago = new PagoServicioBean();

        pago.setAdicionales(peticionAutorizacionAntad.getAutorizacion().getSKU());
        pago.setArticuloId(Long.parseLong(peticionAutorizacionAntad.getAutorizacion().getEmisor()));
        pago.setCodigoBarras(peticionAutorizacionAntad.getAutorizacion().getSKU());
        pago.setUid(String.valueOf(peticionAutorizacionAntad.getUId()));
        pago.setComision(peticionAutorizacionAntad.getAutorizacion().getComision());
        pago.setImpuesto(0);
        pago.setMonto(peticionAutorizacionAntad.getAutorizacion().getMonto());
        pago.setParametros("");
        pago.setReferencia(peticionAutorizacionAntad.getAutorizacion().getReferencia());

        pagos.add(pago);

        return pagos.toArray(new PagoServicioBean[0]);
    }

    private TipoPagoBean[] tipoPagoDto2Bean(TipoPagoDto[] _tiposPago) {
        ArrayList<TipoPagoBean> tiposPago = new ArrayList<TipoPagoBean>();

        for (TipoPagoDto dto : _tiposPago) {
            TipoPagoBean bean = new TipoPagoBean();

            bean.setFcReferenciaPagoTarjetas(String.valueOf(dto.getPagoTarjetaIdBus()));
            bean.setFiTipoPagoId(dto.getTipoPagoId());
            bean.setFnImporteAdicional(dto.getImporteAdicional());
            bean.setFnMontoPago(dto.getMontoPago());
            bean.setFnNumeroVales(dto.getNumeroVales());

            tiposPago.add(bean);
        }

        return tiposPago.toArray(new TipoPagoBean[0]);
    }

    public neto.sion.pago.servicios.cliente.dto.BloqueoDto[] bloqueosAntad2Switch(BloqueoDto[] _bloqueosAntad) {

        ArrayList<neto.sion.pago.servicios.cliente.dto.BloqueoDto> listaBloqueos = new ArrayList<neto.sion.pago.servicios.cliente.dto.BloqueoDto>();

        for (BloqueoDto bloqueoAntad : _bloqueosAntad) {
            neto.sion.pago.servicios.cliente.dto.BloqueoDto bloqueo = new neto.sion.pago.servicios.cliente.dto.BloqueoDto();

            bloqueo.setFiAvisosFalt(bloqueoAntad.getFiAvisosFalt());
            bloqueo.setFiEstatusBloqueoId(bloqueoAntad.getFiEstatusBloqueoId());
            bloqueo.setFiNumAvisos(bloqueoAntad.getFiNumAvisos());
            bloqueo.setFiTipoPagoId(bloqueoAntad.getFiTipoPagoId());
            bloqueo.setTiendaId(bloqueoAntad.getTiendaId());

            listaBloqueos.add(bloqueo);
        }

        return listaBloqueos.toArray(new neto.sion.pago.servicios.cliente.dto.BloqueoDto[0]);
    }

    public void actualizarTransacciónLocal(int sistema, int modulo, int submodulo) {

        SION.log(Modulo.PAGO_SERVICIOS, "Actualizando conciliacion en BD local", Level.INFO);

        PeticionActTransaccionLocalBean peticionActTransaccionLocalBean = new PeticionActTransaccionLocalBean();
        RespuestaActualizacionLocalBean respuestaActualizacionLocalBean = new RespuestaActualizacionLocalBean();


        peticionActTransaccionLocalBean.setPaConciliacionId(peticionAutorizacionAntad.getPaConciliacionId());
        peticionActTransaccionLocalBean.setPaTransaccionVenta(respuestaAutorizacionAntad.getTransaccionId());

        SION.log(Modulo.PAGO_SERVICIOS, "Petición de actualización de transacción local : " + peticionActTransaccionLocalBean.toString(), Level.INFO);

        ConciliacionLocalControlador conciliacionLocalControlador = new ConciliacionLocalControlador();

        respuestaActualizacionLocalBean = conciliacionLocalControlador.actualizarTransaccionLocal(peticionActTransaccionLocalBean);

        int contador = 0;

        while (true && contador < 3) {
            if (respuestaActualizacionLocalBean.getPaCdgError() == 0) {
                break;
            } else {
                respuestaActualizacionLocalBean = conciliacionLocalControlador.actualizarTransaccionLocal(peticionActTransaccionLocalBean);
            }

            contador++;
        }

        SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de la petición de actualización de la transacción local : " + respuestaActualizacionLocalBean.toString(), Level.INFO);

    }

    public void cancelarConciliacionVenta(boolean _soloSeTieneConciliacion) {

        SION.log(Modulo.PAGO_SERVICIOS, "Cancelando conciliacion " + peticionConsultaAntad.getPaConciliacionId() + " de venta en BD local", Level.INFO);

        PeticionActTransaccionLocalBean peticionActTransaccionLocalBean = new PeticionActTransaccionLocalBean();

        peticionActTransaccionLocalBean.setPaConciliacionId(peticionConsultaAntad.getPaConciliacionId());

        if (_soloSeTieneConciliacion) {
            peticionActTransaccionLocalBean.setPaTransaccionVenta(0);
        } else {
            peticionActTransaccionLocalBean.setPaTransaccionVenta(respuestaAutorizacionAntad.getTransaccionId());
        }

        ConciliacionLocalControlador conciliacionLocalControlador = new ConciliacionLocalControlador();
        conciliacionLocalControlador.updCancelacionConciliacion(peticionActTransaccionLocalBean);


    }

    //validaciones
    private String getCodigoBarrasFromListaEmpresas(long _emisor, RespuestaEmpresasBean _respuestaEmpresasBean) {
        String codigoBarras = "";

        for (EmpresaBean empresaBean : _respuestaEmpresasBean.getEmpresaBeans()) {
            if (empresaBean != null) {
                if (_emisor == empresaBean.getEmpresaId()) {
                    codigoBarras = empresaBean.getCodigoBarras();
                    break;
                }
            }
        }

        return codigoBarras;
    }

    public boolean esEmisorAntad(long _emisor, RespuestaEmpresasBean _respuestaEmpresasBean) {
        boolean esAntad = false;

        for (EmpresaBean empresa : _respuestaEmpresasBean.getEmpresaBeans()) {
            if (empresa != null) {
                if (_emisor == empresa.getEmpresaId()) {
                    if (empresa.getAgrupacionId() == AGRUPACION_ANTAD) {
                        esAntad = true;
                        break;
                    }
                }
            }
        }

        return esAntad;
    }

    public boolean esEmisorAntad() {
        boolean existePago = false;

        for (ServicioBean servicio : vm.getListaPagosAcumulados()) {
            if (esEmisorAntad(servicio.getEmisorId(), vm.getRespuestaEmpresasBean())) {
                existePago = true;
                break;
            }
        }

        return existePago;
    }
    
    
    
    public boolean esEmisorPagatodo(long _emisor, RespuestaEmpresasBean _respuestaEmpresasBean) {
        boolean esPagatodo = false;

        for (EmpresaBean empresa : _respuestaEmpresasBean.getEmpresaBeans()) {
            if (empresa != null) {
                if (_emisor == empresa.getEmpresaId()) {
                    if (empresa.getAgrupacionId() == AGRUPACION_PAGATODO) {
                        esPagatodo = true;
                        break;
                    }
                }
            }
        }

        return esPagatodo;
    }
    
    
    
     public boolean existePagoPagatodo() {
        boolean existePago = false;

        for (ServicioBean servicio : vm.getListaPagosAcumulados()) {
            if (esEmisorPagatodo(servicio.getEmisorId(), vm.getRespuestaEmpresasBean())) {
                existePago = true;
                break;
            }
        }

        return existePago;
    }
    

    public boolean esEmisorAntad(long _emisor) {
        return esEmisorAntad(_emisor, vm.getRespuestaEmpresasBean());
    }

    //impresion ticket
    public void imprimirTicketAntad(double _montoRecibido, double _montoCambio, boolean _esFueraLinea, boolean _seRecibioRespuestaAntad, boolean _seConsultoPago) {

        ImpresionVenta impresionControlador = new ImpresionVenta();
        VentaServiciosTicketDto ticketAntad = generarTicketAntad(_montoRecibido, _montoCambio, impresionControlador, _esFueraLinea, _seRecibioRespuestaAntad, _seConsultoPago);

        seImprimioTicketExitoso = false;

        if (ticketAntad != null) {
            
            SION.log(Modulo.PAGO_SERVICIOS, "Ticket a imprimir: " + ticketAntad.toString(), Level.INFO);
            
            //se valida que solo se imprima ticket si hubo algun pago exitoso
            
            if (ticketAntad.getServicios().length > 0) {

                try {
                    ticketAntad.setIvaTotal();
                    impresionControlador.imprimirTicketServicios(ticketAntad);
                    seImprimioTicketExitoso = true;
                    SION.log(Modulo.PAGO_SERVICIOS, "El ticket se imprimio correctamente", Level.INFO);
                } catch (Exception ex) {
                    SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex);
                    SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió una excepcion al imprimir ticket de pago", Level.SEVERE);

                } catch (Error err) {
                    SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un error al imprimir el ticket de pago", Level.SEVERE);

                }
            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "No se encontraron servicios exitosos, no se imprime ticket", Level.WARNING);
                seImprimioTicketExitoso = true;
            }
        } else {
            
            //Error al construir ticket
            SION.log(Modulo.PAGO_SERVICIOS, "Instancia de ticket nula, se continua con el proceso y se muestra msj de error en la impresion", Level.WARNING);
            
        }

    }

    public boolean seImprimioTicket() {
        return seImprimioTicketExitoso;
    }

    private VentaServiciosTicketDto generarTicketAntad(double _montoRecibido, double _montoCambio, ImpresionVenta _impresionControlador, boolean _esFueraLinea, boolean _seRecibioRespuestaAntad, boolean _seConsultoPago) {

        VentaServiciosTicketDto ticket = new VentaServiciosTicketDto();

        SION.log(Modulo.PAGO_SERVICIOS, "Construyendo ticket de pago de servicios Antad", Level.INFO);

        String cadenaDireccion = "";
        String cadenaDelegacion = "";

        boolean esSuperPrecio = false;

        RespuestaParametroBean parametro = new RespuestaParametroBean();
        parametro = utilerias.consultaParametro(UtileriasVenta.Accion.ENCABEZADO_TICKET, Modulo.VENTA, Modulo.PAGO_SERVICIOS);
        if (parametro != null) {
            if (parametro.getCodigoError() == 0 && parametro.getValorConfiguracion() != 1) {
                esSuperPrecio = true;
            }
        }

        try {

            cadenaDireccion = cadenaDireccion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getCalle()));
            cadenaDireccion = cadenaDireccion.concat(" ");
            cadenaDireccion = cadenaDireccion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroExterior()));
            if (!"".equals(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior())
                    && !"0".equals(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior())
                    && VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior() != null) {
                cadenaDireccion = cadenaDireccion.concat("-");
                cadenaDireccion = cadenaDireccion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior()));
            }

            cadenaDelegacion = cadenaDelegacion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getMunicipio()).concat(" ").concat(
                    String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getEstado())).concat(" ").concat(
                    String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getCodigoPostal())));


            ticket.setCaja(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));
            ticket.setCajero(String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()));
            ticket.setCalleNumeroTienda(cadenaDireccion);
            ticket.setColoniaTienda(VistaBarraUsuario.getSesion().getDatosTienda().getColonia());
            ticket.setDelegacionCiudadEstadoCP(cadenaDelegacion);
            if (esSuperPrecio) {
                ticket.setEncabezado(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ENCABEZADO.TICKET.SUPR"));
            } else {
                ticket.setEncabezado(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ENCABEZADO.TICKET"));
            }
            ticket.setFecha(obtenerFecha().substring(0, obtenerFecha().indexOf(" ")));
            ticket.setHora(obtenerFecha().substring(10));
            ticket.setNombreTienda(VistaBarraUsuario.getSesion().getNombreTienda());
            if (_esFueraLinea) {
                ticket.setFolio(String.valueOf(respuestaPagoLocal.getConciliacionId()).concat("O"));
                ticket.setTransaccion(String.valueOf(respuestaPagoLocal.getConciliacionId()).concat("O"));

            } else {
                if(_seConsultoPago){
                    ticket.setFolio(String.valueOf(respuestaConsultaBDCentral.getTransaccionId()).concat("I"));
                    ticket.setTransaccion(String.valueOf(respuestaConsultaBDCentral.getTransaccionId()).concat("I"));
                }else{
                    ticket.setFolio(String.valueOf(respuestaAutorizacionAntad.getTransaccionId()).concat("I"));
                    ticket.setTransaccion(String.valueOf(respuestaAutorizacionAntad.getTransaccionId()).concat("I"));
                }
            }
            ticket.setServicios(arrayAutorizacionAntad2ServicioTicketDto(_seRecibioRespuestaAntad, _seConsultoPago));

            ticket.setTiendaId(String.valueOf(VistaBarraUsuario.getSesion().getTiendaId()));
            ticket.setTotalComision(utilerias.getNumeroFormateado(peticionAutorizacionAntad.getAutorizacion().getComision()));
            ticket.setTiposPago(_impresionControlador.tipoPagoDtoaTicket(arrayTiposPagoServicioAntad2Venta(), peticionAutorizacionAntad.getMontoTotal(), _montoRecibido, _montoCambio));
            ticket.setTotalServicios();
            ticket.setTotalLetras(String.valueOf(_impresionControlador.getTotalLetras()));

        } catch (Exception e) {
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió una excepción al construir el ticket", Level.SEVERE);
            ticket = null;
        } catch (Error e){
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió un error al construir el ticket", Level.SEVERE);
            ticket = null;
        }

        return ticket;
    }

    //venta dia
    public void guardarVentaDia() {

        PeticionVentaDiaBean peticionVentaDiaBean = new PeticionVentaDiaBean();
        RespuestaVentaDiaBean respuestaVentaDiaBean = new RespuestaVentaDiaBean();

        VentaDiaBean[] pagosDia = cargarPagosDia();

        if (pagosDia != null) {

            peticionVentaDiaBean.setVentaDiaBeans(pagosDia);

            PagoServiciosDao dao = new PagoServiciosDao();
            respuestaVentaDiaBean = dao.guardarVentaDia(peticionVentaDiaBean);

            if (respuestaVentaDiaBean != null) {
                if (respuestaVentaDiaBean.getCodigoError() == 0) {
                    SION.log(Modulo.PAGO_SERVICIOS, "La venta se guardo exitosamente", Level.INFO);
                } else {
                    SION.log(Modulo.PAGO_SERVICIOS, respuestaVentaDiaBean.getDescripcionError(), Level.SEVERE);
                }
            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de inserción de venta del día nula", Level.SEVERE);
            }
        } else {
            SION.log(Modulo.PAGO_SERVICIOS, "No hay pagos que registrar como venta del día en esta operación", Level.WARNING);
        }
    }

    private VentaDiaBean[] cargarPagosDia() {

        ArrayList<VentaDiaBean> listaVentas = new ArrayList<VentaDiaBean>();

        int iepsId = 0;

        try {
            iepsId = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "servicio.iepsId").trim());
        } catch (NumberFormatException e) {
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e, "Ocurrio un error al consultar iepsId desde archivo de propiedades, se asiga 69 default");
            iepsId = 69;
        }

        VentaDiaBean pagoDia = new VentaDiaBean();
        pagoDia.setArticuloId(Long.parseLong(peticionAutorizacionAntad.getAutorizacion().getEmisor()));
        pagoDia.setCantidad(1);
        pagoDia.setCodigoBarras(peticionAutorizacionAntad.getAutorizacion().getCodigoBarras());
        pagoDia.setCosto(peticionAutorizacionAntad.getMontoTotal());
        pagoDia.setDescuento(0);
        pagoDia.setIva(0);
        pagoDia.setPrecio(0);
        pagoDia.setIepsId(iepsId);

        listaVentas.add(pagoDia);

        return listaVentas.toArray(new VentaDiaBean[0]);
    }

    //utilerias
    private neto.sion.pago.servicios.cliente.dto.BloqueoDto[] arrayBloqueosAntad2Venta() {
        ArrayList<neto.sion.pago.servicios.cliente.dto.BloqueoDto> listaBloqueos = new ArrayList<neto.sion.pago.servicios.cliente.dto.BloqueoDto>();

        for (BloqueoDto bloqueoAntad : respuestaAutorizacionAntad.getPaArrayBloqueos()) {
            neto.sion.pago.servicios.cliente.dto.BloqueoDto bloqueoVenta = new neto.sion.pago.servicios.cliente.dto.BloqueoDto();
            bloqueoVenta.setFiAvisosFalt(bloqueoAntad.getFiAvisosFalt());
            bloqueoVenta.setFiEstatusBloqueoId(bloqueoAntad.getFiEstatusBloqueoId());
            bloqueoVenta.setFiNumAvisos(bloqueoAntad.getFiNumAvisos());
            bloqueoVenta.setFiTipoPagoId(bloqueoAntad.getFiTipoPagoId());
            listaBloqueos.add(bloqueoVenta);
        }

        return listaBloqueos.toArray(new neto.sion.pago.servicios.cliente.dto.BloqueoDto[0]);
    }

    private ArrayList<neto.sion.venta.servicios.cliente.dto.TipoPagoDto> arrayTiposPagoServicioAntad2Venta() {
        ArrayList<neto.sion.venta.servicios.cliente.dto.TipoPagoDto> listaTiposPago = new ArrayList<neto.sion.venta.servicios.cliente.dto.TipoPagoDto>();

        for (TipoPagoDto tipoPago : peticionAutorizacionAntad.getArregloTiposPago()) {
            neto.sion.venta.servicios.cliente.dto.TipoPagoDto tipoPagoDto = new neto.sion.venta.servicios.cliente.dto.TipoPagoDto();
            tipoPagoDto.setImporteAdicional(tipoPago.getImporteAdicional());
            tipoPagoDto.setFnMontoPago(tipoPago.getMontoPago());
            tipoPagoDto.setFnNumeroVales(tipoPago.getNumeroVales());
            tipoPagoDto.setPaPagoTarjetaIdBus(tipoPago.getPagoTarjetaIdBus());
            tipoPagoDto.setFiTipoPagoId(tipoPago.getTipoPagoId());
            listaTiposPago.add(tipoPagoDto);
        }

        return listaTiposPago;
    }

    public ServicioTicketDto[] arrayAutorizacionAntad2ServicioTicketDto(boolean _seRecibioRespuesta, boolean _seConsultoPago) {
        ArrayList<ServicioTicketDto> listaServicios = new ArrayList<ServicioTicketDto>();

        ServicioTicketDto servicioTicket = new ServicioTicketDto();

        servicioTicket.setComision(String.valueOf(peticionAutorizacionAntad.getAutorizacion().getComision()));
        servicioTicket.setConceptoImpuesto("0");
        //servicioTicket.setEmisora(String.valueOf(peticionAutorizacionAntad.getAutorizacion().getEmisor()));
        servicioTicket.setEmisora(buscaNombreEmisorXId(Integer.parseInt(peticionAutorizacionAntad.getAutorizacion().getEmisor())));
        servicioTicket.setEsConceptoImpuesto(false);
        servicioTicket.setImporte(utilerias.getNumeroFormateado(peticionAutorizacionAntad.getAutorizacion().getMonto()));
        servicioTicket.setIva(0);

        if (_seRecibioRespuesta) {
            if(_seConsultoPago){
                servicioTicket.setLeyenda("TRANSACCION APROBADA | Transaccion Aprobada."
                        +"Servicio operado por Plataforma Electronica XCD. "
                        +"El pago quedara aplicado en 24 hrs. Si se supende su servicio, envie desde su celular reactiva espacio y su numero de cliente al 30200."
                        +"Para aclaraciones marque 01(81)-8988-0200. Favor de guardar este comprobante de pago para posibles aclaraciones.");
                //servicioTicket.setLeyenda(respuestaConsultaBDCentral.getMensaejAntad());
                servicioTicket.setNoAutorizacion(String.valueOf(respuestaConsultaBDCentral.getFolioAntad()));
                servicioTicket.setFolioXCD("0");
            }else{
                servicioTicket.setLeyenda("TRANSACCION APROBADA | Transaccion Aprobada."
                        +"Servicio operado por Plataforma Electronica XCD. "
                        +"El pago quedara aplicado en 24 hrs. Si se supende su servicio, envie desde su celular reactiva espacio y su numero de cliente al 30200."
                        +"Para aclaraciones marque 01(81)-8988-0200. Favor de guardar este comprobante de pago para posibles aclaraciones.");
                //servicioTicket.setLeyenda(respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getMensajeTicket());
                servicioTicket.setNoAutorizacion(respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getNumAuth());
                servicioTicket.setFolioXCD(respuestaAutorizacionAntad.getRespuestaAutorizacionAntad().getANTADWSResponse().getFolioTransaccion());
            }
        } else {
            //cuando se recibe excepcion y se marca fl
            servicioTicket.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.ANTAD.MENSAJE.TICKETFL"));
            servicioTicket.setNoAutorizacion("0");
            servicioTicket.setFolioXCD("");
        }

        servicioTicket.setEsConceptoImpuesto(false);
        servicioTicket.setConceptoImpuesto("");

        servicioTicket.setReferencia(peticionAutorizacionAntad.getAutorizacion().getReferencia());

        listaServicios.add(servicioTicket);

        return listaServicios.toArray(new ServicioTicketDto[0]);
    }

    public String buscaNombreEmisorXId(int _emisor) {
        String nombre = "";

        for (EmpresaBean bean : vm.getListaEmpresas()) {
            if (bean != null) {
                if (bean.getEmpresaId() == _emisor) {
                    nombre = bean.getDescrpcion();
                    break;
                }
            }
        }

        return nombre;
    }

    public String buscaSKUxId(int _emisor) {
        String sku = "";

        for (EmpresaBean bean : vm.getListaEmpresas()) {
            if (bean != null) {
                if (bean.getEmpresaId() == _emisor) {
                    sku = bean.getCodigoBarras();
                    break;
                }
            }
        }

        return sku;
    }

    public String obtenerFecha() {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar Cal = Calendar.getInstance();
        return dateFormat.format(new Date());
    }

    public void limpiarAntad() {
        SION.log(Modulo.PAGO_SERVICIOS, "Limpiando variables Antad", Level.INFO);
        peticionConsultaAntad = null;
        respuestaConsultaAntad = null;
        peticionAutorizacionAntad = null;
        respuestaAutorizacionAntad = null;
    }

    public void limpiar() {
        vm.limpiar();
    }
}
