/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.binding;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import neto.sion.pago.servicios.cliente.dto.PagoServicioVentaDto;
import neto.sion.tienda.autorizador.controlador.ControladorValidacion;
import neto.sion.tienda.autorizador.controlador.RespuestaValidacion;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.caja.principal.VerificaVentana;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.dao.PagoServiciosDao;
import neto.sion.tienda.pago.servicios.modelo.*;
import neto.sion.tienda.pago.servicios.vista.ConfirmacionPagoVista;
import neto.sion.tienda.pago.servicios.vista.PagoServiciosVista;
import neto.sion.tienda.pago.servicios.vista.ValidadorReferenciaAntadVista;
import neto.sion.tienda.pago.servicios.vista.ValidadorReferenciaPagatodoVista;
import neto.sion.tienda.pago.servicios.vista.ValidadorReferenciaSwitchVista;
import neto.sion.tienda.pago.servicios.vistamodelo.PagoServiciosVistaModelo;
import neto.sion.tienda.venta.utilerias.bean.RespuestaParametroBean;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta.Accion;
import neto.sion.venta.pagatodo.ps.dto.ConsultaComisionDtoResp;

/**
 *
 * @author fvega
 */
public class PagoServiciosBinding {

    private UtileriasVenta utilerias;
    private PagoServiciosVista vista;
    //private PagosParcialesVista vistaPagosParciales;
    private PagoServiciosVistaModelo vistaModelo;
    private ArrayList<String> listaNombresServiciosFrecuentes;
    //popup de aviso de pagos desmarcados
    private HBox cajaPopupBotones;
    private Image imagenAceptarPagosDesmarcados;
    private Image imagenAceptarPagosDesmarcadosH;
    private ImageView vistaImagenAceptarPagosDesmarcados;
    private Button botonAceptarPagosDesmarcados;
    private Image imagenCancelarPagosDesmarcados;
    private Image imagenCancelarPagosDesmarcadosH;
    private ImageView vistaImagenCancelarPagosDesmarcados;
    private Button botonCancelarpagosDesmarcados;
    private boolean esFueraLinea;
    //
    private boolean esReferenciaValida = false;
    private final int EFECTIVO = 1;
    private final int TARJETA = 2;
    private final int PAGOSERVTIPOPIN = 2;
    private int tipoPago = 1;
    //bloqueos
    private VerificaVentana verificaVentana;
    private Image aceptarTraspasos;
    private ImageView vistaImagenAceptarTraspasos;
    private Button botonTraspasos;
    private Image aceptarBloqueos;
    private ImageView vistaImagenAceptarBloqueos;
    private Button botonBloqueos;
    //
    //bloqueos locales
    private final int BLOQUEO_EFECTIVO = 1;
    private final int BLOQUEO_TARJETAS = 2;
    private final int BLOQUEO_VALES = 3;
    //validacion de montos de referencia
    private boolean sobrepasaMontoValido = false;
    //private boolean esImporteConDecimales = false;
    private String RUTA_FRECUENTES;
    private String RUTA_ICONOS_TABLA;
    private String RUTA_REFERENCIAS;
    ////emisores para los que se maneja caso especial
    private final int EMISOR_ORIFLAME = 118;
    private final int EMISOR_MAZ = 196;
    private final int EMISOR_SKY = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.SKY").trim());
    private final int EMISOR_GOBIERNODF = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.GOBIERNODF").trim());
    private final int EMISOR_TOTALPLAY = 216;
    private final int EMISOR_IUSACELL = 26;
    private final int EMISOR_IUSACELL_SECSA = 27;
    private final int EMISOR_DINEROEXP = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.DINEROEXP").trim());
    
   
    //Comision de pagatodo para el emisor
    private double dComision;
    
    
    //
    ///popup vales
    //private ChangeListener<Number> listenerPopupValidacionMonto;
    ///antad
    private AntadBinding antadBinding;
    private PagatodoBinding pagatodoBinding;
    private ValidadorReferenciaSwitchVista validadorSwitch;
    private ValidadorReferenciaAntadVista validadorAntad;
    private ValidadorReferenciaPagatodoVista validadorPagatodo;

    public PagoServiciosBinding(UtileriasVenta _utileriasVenta, PagoServiciosVista _vista) {
        SION.log(Modulo.PAGO_SERVICIOS, "Version ESL SE AGREGA FLUJO PAGATODO", Level.SEVERE);
        this.utilerias = _utileriasVenta;
        this.vista = _vista;
        this.vistaModelo = new PagoServiciosVistaModelo(utilerias, this);
        this.dComision = 0;
        
        //this.vistaPagosParciales = new PagosParcialesVista(vista, utilerias, vistaModelo);
        this.listaNombresServiciosFrecuentes = new ArrayList<String>();
        //popup de aviso de pagos desmarcados
        this.cajaPopupBotones = utilerias.getHBox(10, 300, 100);
        this.imagenAceptarPagosDesmarcados = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/btn_Aceptar.png");
        this.imagenAceptarPagosDesmarcadosH = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/btn_Aceptar_2.png");
        this.vistaImagenAceptarPagosDesmarcados = new ImageView(imagenAceptarPagosDesmarcados);
        this.botonAceptarPagosDesmarcados = new Button("");
        this.imagenCancelarPagosDesmarcados = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/btn_Cancelar.png");
        this.imagenCancelarPagosDesmarcadosH = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/btn_Cancelar_2.png");
        this.vistaImagenCancelarPagosDesmarcados = new ImageView(imagenCancelarPagosDesmarcados);
        this.botonCancelarpagosDesmarcados = new Button("");
        this.esFueraLinea = false;

        //bloqueos
        this.verificaVentana = new VerificaVentana();
        this.aceptarTraspasos = new Image(getClass().getResourceAsStream("/neto/sion/tienda/pago/servicios/vista/btn_Aceptar.png"));
        this.vistaImagenAceptarTraspasos = new ImageView(aceptarTraspasos);
        this.botonTraspasos = new Button("", vistaImagenAceptarTraspasos);
        this.aceptarBloqueos = new Image(getClass().getResourceAsStream("/neto/sion/tienda/pago/servicios/vista/btn_Aceptar.png"));
        this.vistaImagenAceptarBloqueos = new ImageView(aceptarBloqueos);
        this.botonBloqueos = new Button("", vistaImagenAceptarBloqueos);
        //
        this.RUTA_FRECUENTES = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "RUTA.IMAGENES.FRECUENTES");
        this.RUTA_ICONOS_TABLA = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "RUTA.IMAGENES.TABLA");
        this.RUTA_REFERENCIAS = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "RUTA.IMAGENES.REFERENCIAS");

        //
        //this.listenerPopupValidacionMonto = null;

        //antad
        this.antadBinding = new AntadBinding(vista, this, vistaModelo, utilerias, verificaVentana);
        this.validadorAntad = new ValidadorReferenciaAntadVista(antadBinding, utilerias, this);
        
        //Pagatodo
        this.pagatodoBinding = new PagatodoBinding(vista, this, vistaModelo, utilerias, verificaVentana);
        this.validadorPagatodo = new ValidadorReferenciaPagatodoVista(pagatodoBinding, utilerias, this);
        
        //validador swtich
        this.validadorSwitch = new ValidadorReferenciaSwitchVista(utilerias, this);
        
        

        try {
            cargarListaEmpresas();
        } catch (Exception e) {
            SION.log(Modulo.PAGO_SERVICIOS, "Error al cargar lista de empresas", Level.SEVERE);
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
        }

        crearPopupMensajes();
        creaListenerBotonTraspasos();
        creaListenerBotonBloqueos();
        evaluarBloqueosVenta();
    }

    public ArrayList<String> getListaNombresServiciosfrecuentes() {
        return listaNombresServiciosFrecuentes;
    }

    private void crearPopupMensajes() {

        cajaPopupBotones.getChildren().addAll(botonAceptarPagosDesmarcados, botonCancelarpagosDesmarcados);

        botonAceptarPagosDesmarcados.setMaxSize(145, 50);
        botonAceptarPagosDesmarcados.setMinSize(145, 50);
        botonAceptarPagosDesmarcados.setPrefSize(145, 50);
        botonCancelarpagosDesmarcados.setMaxSize(145, 50);
        botonCancelarpagosDesmarcados.setMinSize(145, 50);
        botonCancelarpagosDesmarcados.setPrefSize(145, 50);
        botonAceptarPagosDesmarcados.setGraphic(vistaImagenAceptarPagosDesmarcados);
        botonCancelarpagosDesmarcados.setGraphic(vistaImagenCancelarPagosDesmarcados);
        vistaImagenAceptarPagosDesmarcados.setFitWidth(145);
        vistaImagenAceptarPagosDesmarcados.setFitHeight(50);
        vistaImagenCancelarPagosDesmarcados.setFitWidth(145);
        vistaImagenCancelarPagosDesmarcados.setFitHeight(50);
        botonCancelarpagosDesmarcados.setStyle("-fx-alignment: center;");

        setImagenHover(vistaImagenAceptarPagosDesmarcados, imagenAceptarPagosDesmarcados, imagenAceptarPagosDesmarcadosH);
        setImagenHover(vistaImagenCancelarPagosDesmarcados, imagenCancelarPagosDesmarcados, imagenCancelarPagosDesmarcadosH);

        botonAceptarPagosDesmarcados.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                //aplicarPago();
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.cerrarAlerta();
                    }
                });

                if (esFueraLinea) {

                    vistaModelo.llenarPeticionPagoFueraLinea(true, vista.getMontoVentaProperty().get());
                    vistaModelo.pagarFueraDeLinea();

                    validarPagoLocal(true);

                } else {
                    aplicarpago(tipoPago);
                }
            }
        });

        botonCancelarpagosDesmarcados.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.cerrarAlerta();
                        vistaModelo.limpiaParcial();
                    }
                });

            }
        });

    }

    private void setImagenHover(final ImageView _vistaImagen, final Image _imagen, final Image _imagenHover) {
        _vistaImagen.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    _vistaImagen.setCursor(Cursor.HAND);
                    _vistaImagen.setImage(_imagenHover);
                } else if (arg1 && !arg2) {
                    _vistaImagen.setCursor(Cursor.NONE);
                    _vistaImagen.setImage(_imagen);
                }
            }
        });
    }

    public void cargarListaEmpresas() {
        vistaModelo.cargarListaEmpresas();
    }

    public boolean esEmisorActivo(long _emisor) {
        return vistaModelo.esEmisorActivo(_emisor);
    }

    public void cargarListaPagosAcumulados(TableView _tabla) {
        _tabla.setItems(vistaModelo.getListaPagosAcumulados());
    }

    public <E> void asignaPropiedadBean(TableColumn columna, E e, final String id) {
        columna.setCellValueFactory(new PropertyValueFactory<ServicioBean, E>(id));
    }

    public <E> void setCellFactory(final TableView tabla, final TableColumn columna, final E e, final Pos posicion,
            final boolean seValidaFormato) {

        columna.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn param) {
                return new TableCell<ServicioBean, E>() {

                    @Override
                    public void updateItem(E item, boolean empty) {
                        super.updateItem(item, empty);

                        if (!this.isEmpty()) {
                            ////reavisa si E es Intger
                            if (e.toString().contains(Integer.class.getCanonicalName())) {
                            } else if (e.toString().contains(Double.class.getCanonicalName())) {
                                if (seValidaFormato) {

                                    String cadenaDouble = "";
                                    int indicePunto = 0;
                                    String cadenaPuntoAlaDerecha = "";

                                    cadenaDouble = String.valueOf(item);
                                    indicePunto = cadenaDouble.indexOf(".");
                                    cadenaPuntoAlaDerecha = cadenaDouble.substring(indicePunto + 1);

                                    if (Double.parseDouble(cadenaPuntoAlaDerecha) > 0) {
                                        this.setText(cadenaDouble);
                                    } else {
                                        this.setText(cadenaDouble.substring(0, indicePunto));
                                    }

                                } else {
                                    this.setText(utilerias.getNumeroFormateado(Double.parseDouble(item.toString())));
                                }
                            } else {///es String
                                this.setText(String.valueOf(item));
                            }

                            if (vistaModelo.getListaPagosAcumulados().get(this.getIndex()).getImporte() <= 0) {
                                this.setTextFill(Color.RED);
                            }

                            this.alignmentProperty().set(posicion);
                            //this.setStyle("-fx-font: 20pt Verdana;");
                        }
                    }
                };
            }
        });
    }

    public void setCallbackImagen(final TableColumn _columna) {
        _columna.setCellValueFactory(new Callback<CellDataFeatures<ServicioBean, ImageView>, ObservableValue<ImageView>>() {

            @Override
            public ObservableValue<ImageView> call(CellDataFeatures<ServicioBean, ImageView> arg0) {
                final ServicioBean usuario = arg0.getValue();
                Image imagen = null;
                ImageView vistaImagen = null;
                boolean seCargoImagen = false;
                try {

                    imagen = vista.getImagen(RUTA_ICONOS_TABLA.concat(usuario.getRutaLogo().trim()).concat(".png"));
                    if (imagen.getHeight() > 0 && imagen.getWidth() > 0) {
                        seCargoImagen = true;
                    }
                } catch (Exception e) {
                    SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
                }

                if (seCargoImagen) {
                    vistaImagen = new ImageView(imagen);
                } else {
                    imagen = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/logoNeto.png");
                    vistaImagen = new ImageView(imagen);
                }

                
                
                vistaImagen.setFitWidth(imagen.getWidth());
                vistaImagen.setFitHeight(imagen.getHeight());
                
                /*vistaImagen.setFitWidth(utilerias.getAnchoMonitor() * .05);
                vistaImagen.setFitHeight(utilerias.getAlturaMonitor() * .035);*/

                return new SimpleObjectProperty<ImageView>(vistaImagen);
            }
        });
    }

    public void setCallbackCheckBox(TableColumn _columna, final Label _etiquetaImportePago) {
        _columna.setCellValueFactory(new Callback<CellDataFeatures<ServicioBean, CheckBox>, ObservableValue<CheckBox>>() {

            @Override
            public ObservableValue<CheckBox> call(
                    CellDataFeatures<ServicioBean, CheckBox> arg0) {
                final ServicioBean usuario = arg0.getValue();
                final CheckBox checkBox = new CheckBox();

                if (usuario.getCheckId()) {
                    checkBox.selectedProperty().setValue(Boolean.TRUE);
                }

                checkBox.alignmentProperty().set(Pos.CENTER);
                checkBox.setVisible(false);
                checkBox.setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent arg0) {
                        //imprimirListaPantalla("lista antes");
                        if (checkBox.isSelected()) {
                            int contador = 0;
                            int index = 0;
                            boolean seEncontroRegistro = false;

                            for (ServicioBean bean : vistaModelo.getListaPagosAcumulados()) {
                                if (bean.getEmisorId() == usuario.getEmisorId()
                                        && bean.getReferencia().equals(usuario.getReferencia())) {
                                    seEncontroRegistro = true;
                                    index = contador;
                                }
                                contador++;
                            }

                            if (seEncontroRegistro) {
                                vistaModelo.getListaPagosAcumulados().get(index).setCheckId(true);
                                actualizarMontos();
                            }
                        } else {
                            if (vistaModelo.getListaPagosAcumulados().size() > 0) {
                                boolean seEncontroRegistro = false;
                                int contador = 0;
                                int index = 0;

                                for (ServicioBean bean : vistaModelo.getListaPagosAcumulados()) {
                                    if (bean.getEmisorId() == usuario.getEmisorId()
                                            && bean.getReferencia().equals(usuario.getReferencia())) {
                                        seEncontroRegistro = true;
                                        index = contador;
                                    }
                                    contador++;
                                }
                                if (seEncontroRegistro) {
                                    vistaModelo.getListaPagosAcumulados().get(index).setCheckId(false);
                                    actualizarMontos();
                                }
                            }
                        }

                        double importe = vistaModelo.getImporteTotalPago();
                        if (importe > 0) {
                            _etiquetaImportePago.setText(utilerias.getNumeroFormateado(importe));
                        } else {
                            _etiquetaImportePago.setText("0.00");
                        }
                        //imprimirListaPantalla("lista despues");

                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                vista.getTablaServicios().requestFocus();
                            }
                        });

                    }
                });



                return new SimpleObjectProperty<CheckBox>(checkBox);
            }
        });
    }

    private boolean esDecimal(double cantidad) {

        String cadena = "";
        cadena = String.valueOf(cantidad);
        boolean esDecimal = false;

        if (cadena.contains(".")) {

            try {

                int decimales = Integer.parseInt(cadena.substring(cadena.indexOf(".") + 1, cadena.length()));

                if (decimales > 0) {
                    esDecimal = true;
                }

            } catch (Exception e) {
                SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e, "error al validar el numero");
            }

        }

        return esDecimal;

    }

    public boolean esNumeroReferenciaValida(int _index, String _referencia, boolean esTelevia) {

        boolean esValida = true;
        sobrepasaMontoValido = false;
        //esImporteConDecimales = false;

        for (ServicioBean bean : vistaModelo.getListaPagosAcumulados()) {

            ////valida duplicados y reglas de negocio
            switch (bean.getEmisorId()) {
                case EMISOR_ORIFLAME:
                    //no permitir que se capturen dos referencias oriflame aunque sean distintos pedidos
                    if (bean.getEmisorId() == vistaModelo.getListaEmpresas().get(_index).getEmpresaId()) {
                        esValida = false;
                    }
                    break;
                default:

                    if (bean.getEmisorId() == vistaModelo.getListaEmpresas().get(_index).getEmpresaId()
                            && (bean.getReferencia().trim().equals(_referencia)
                            || bean.getReferencia().trim().contains(_referencia.trim()))) {

                        esValida = false;
                    }
                    break;
            }
        }

        ////valida montos
        if (esTelevia) {
            if (vista.getMontoTelevia() > 30000) {
                if (SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "emisores.validarmonto").contains(String.valueOf(vistaModelo.getListaEmpresas().get(_index).getEmpresaId()).concat(","))) {
                    sobrepasaMontoValido = true;
                    esValida = false;
                }
            }
        } else if (vistaModelo.getListaEmpresas().get(_index).getEmpresaId() == EMISOR_ORIFLAME) {
            ///si es oriflame no se valida 
        }else if (vistaModelo.getListaEmpresas().get(_index).getEmpresaId() == EMISOR_DINEROEXP){
            return esValida;
        }else {
            if (Double.parseDouble(vista.getCampoPago().getText()) > 30000) {
                if (SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "emisores.validarmonto").contains(String.valueOf(vistaModelo.getListaEmpresas().get(_index).getEmpresaId()).concat(","))) {
                    sobrepasaMontoValido = true;
                    esValida = false;
                }
            }
        }


        return esValida;
    }

    public boolean sobrepasaMontoValidoEmisor() {
        return sobrepasaMontoValido;
    }

    //public boolean esMontoConDecimales() {
    //    return esImporteConDecimales;
    //}
    private void agregaPagoAcumuladoDefault(final int _index, final int _tipoPago, final double _importe, final String _referencia) {

        SION.log(Modulo.PAGO_SERVICIOS, "Agregando pago a lista de acumulados", Level.INFO);

        String adicionales = vistaModelo.getRespuestaValidaReferencia().getAdicionales();
        String nuevaReferencia = _referencia;

        if (adicionales.contains("<Referencia>") && adicionales.contains("</Referencia>")) {

            nuevaReferencia = vistaModelo.getRespuestaValidaReferencia().getAdicionales().substring(
                    vistaModelo.getRespuestaValidaReferencia().getAdicionales().indexOf("<Referencia>") + 12,
                    vistaModelo.getRespuestaValidaReferencia().getAdicionales().indexOf("</Referencia>"));

        }

        /*
         * PeticionConsultaReferenciaBean peticionConsulta = new
         * PeticionConsultaReferenciaBean(); SION.log(Modulo.PAGO_SERVICIOS,
         * "Agregando emisor a lista d epagos: "+(long)
         * vistaModelo.getListaEmpresas().get(_index).getEmpresaId(),
         * Level.INFO); peticionConsulta.setPaEmisorId((long)
         * vistaModelo.getListaEmpresas().get(_index).getEmpresaId());
         * peticionConsulta.setPaFechaConc(vistaModelo.obtenerFecha());
         * peticionConsulta.setPaMontoPago(_importe);
         * peticionConsulta.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
         * peticionConsulta.setPaReferencia(nuevaReferencia.trim());
         * peticionConsulta.setPaTiendaId((int) VistaBarraUsuario.getSesion().getTiendaId());
         */

        //if (!existePagoFueraLinea(peticionConsulta)) {

        /*
         * vistaModelo.getListaPagosAcumulados().add( new ServicioBean( true,
         * String.valueOf(vistaModelo.getListaEmpresas().get(_index).getEmpresaId()),
         * vistaModelo.getListaEmpresas().get(_index).getEmpresaId(),
         * vistaModelo.getListaEmpresas().get(_index).getDescrpcion(),
         * _tipoPago,
         * vistaModelo.getRespuestaValidaReferencia().getComisionCliente() +
         * vistaModelo.getRespuestaValidaReferencia().getIva(), _importe,
         * nuevaReferencia, vistaModelo.getRespuestaValidaReferencia().getIva(), ""));
         */

        vistaModelo.getListaPagosAcumulados().add(
                new ServicioBean(
                true, String.valueOf(vistaModelo.getPeticionValidaReferencia().getEmisorId()),
                vistaModelo.getPeticionValidaReferencia().getEmisorId(),
                vistaModelo.getListaEmpresas().get(_index).getDescrpcion(), _tipoPago,
                vistaModelo.getRespuestaValidaReferencia().getComisionCliente()
                + vistaModelo.getRespuestaValidaReferencia().getIva(), _importe, nuevaReferencia,
                vistaModelo.getRespuestaValidaReferencia().getIva(), ""));

        //} else {
        //AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.fueralinea.encontrado"), TipoMensaje.ADVERTENCIA, vista.getCampoReferencia());
        //}
    }

    public void agregaPagoOriflame(int _tipoPago, PagoOriflameBean pago, double comision) {

        SION.log(Modulo.PAGO_SERVICIOS, "Agregando pago Oriflame a lista de acumulados", Level.INFO);

        if (vistaModelo.getOriflame() != null) {
            if (vistaModelo.getOriflame().getListaPagosSeleccionados() != null
                    && vistaModelo.getOriflame().getListaPagosSeleccionados().size() > 0) {

                PeticionConsultaOriflameBean peticionConsulta = new PeticionConsultaOriflameBean();
                //peticionConsulta.setPaEmisorId((long) EMISOR_ORIFLAME);
                //peticionConsulta.setFecha(vistaModelo.obtenerFecha());
                //peticionConsulta.setPaMontoPago(pago.getMonto());
                peticionConsulta.setPaisId(VistaBarraUsuario.getSesion().getPaisId());
                peticionConsulta.setReferencia(vistaModelo.getPeticionValidaReferencia().getReferencia());
                peticionConsulta.setTiendaId((int) VistaBarraUsuario.getSesion().getTiendaId());

                //if (!existePagoFueraLinea(peticionConsulta)) {
                //if (!existePedidoOriflameFueraLinea(peticionConsulta, pago)) {

                vistaModelo.getListaPagosAcumulados().add(
                        new ServicioBean(
                        true, String.valueOf(EMISOR_ORIFLAME), EMISOR_ORIFLAME,
                        buscaNombreEmisorXId(EMISOR_ORIFLAME), _tipoPago, comision,
                        pago.getMonto(), vistaModelo.getPeticionValidaReferencia().getReferencia(),
                        vistaModelo.getRespuestaValidaReferencia().getIva(), pago.getSubReferencia()));

                //} else {
                //  AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.fueralinea.encontrado"), TipoMensaje.ADVERTENCIA, vista.getCampoReferencia());
                //}

            }
        }

    }

    public void agregaPagoTotalplay(int _tipoPago, double comision) {

        SION.log(Modulo.PAGO_SERVICIOS, "Agregando pago acumulado Totalplay", Level.INFO);

        if (vistaModelo.getPeticionValidaReferencia() != null) {

            PeticionConsultaReferenciaBean peticionConsulta = new PeticionConsultaReferenciaBean();
            peticionConsulta.setPaEmisorId((long) EMISOR_TOTALPLAY);
            peticionConsulta.setPaFechaConc(vistaModelo.obtenerFecha());
            peticionConsulta.setPaMontoPago(vistaModelo.getPeticionValidaReferencia().getImporte());
            peticionConsulta.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
            peticionConsulta.setPaReferencia(vistaModelo.getPeticionValidaReferencia().getReferencia());
            peticionConsulta.setPaTiendaId((int) VistaBarraUsuario.getSesion().getTiendaId());

            //if (!existePagoFueraLinea(peticionConsulta)) {
            vistaModelo.getListaPagosAcumulados().add(
                    new ServicioBean(
                    true, String.valueOf(EMISOR_TOTALPLAY), EMISOR_TOTALPLAY,
                    buscaNombreEmisorXId(EMISOR_TOTALPLAY), _tipoPago, comision,
                    vistaModelo.getPeticionValidaReferencia().getImporte(),
                    vistaModelo.getPeticionValidaReferencia().getReferencia(),
                    vistaModelo.getRespuestaValidaReferencia().getIva(),
                    vistaModelo.getPeticionValidaReferencia().getReferencia()));
            //} else {
            //  AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.fueralinea.encontrado"), TipoMensaje.ADVERTENCIA, vista.getCampoReferencia());
            //}
        }

    }

    public void agregaPagoIusacell(int _tipoPago, double comision) {

        SION.log(Modulo.PAGO_SERVICIOS, "Agregando pago acumulado Iusacell", Level.INFO);

        if (vistaModelo.getPeticionValidaReferencia() != null) {

            PeticionConsultaReferenciaBean peticionConsulta = new PeticionConsultaReferenciaBean();
            peticionConsulta.setPaEmisorId((long) EMISOR_IUSACELL);
            peticionConsulta.setPaFechaConc(vistaModelo.obtenerFecha());
            peticionConsulta.setPaMontoPago(vistaModelo.getPeticionValidaReferencia().getImporte());
            peticionConsulta.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
            peticionConsulta.setPaReferencia(vistaModelo.getPeticionValidaReferencia().getReferencia());
            peticionConsulta.setPaTiendaId((int) VistaBarraUsuario.getSesion().getTiendaId());

            //if (!existePagoFueraLinea(peticionConsulta)) {
            vistaModelo.getListaPagosAcumulados().add(
                    new ServicioBean(
                    true, String.valueOf(EMISOR_IUSACELL), EMISOR_IUSACELL,
                    buscaNombreEmisorXId(EMISOR_IUSACELL), _tipoPago, comision,
                    vistaModelo.getPeticionValidaReferencia().getImporte(),
                    vistaModelo.getPeticionValidaReferencia().getReferencia(),
                    vistaModelo.getRespuestaValidaReferencia().getIva(),
                    vistaModelo.getPeticionValidaReferencia().getReferencia()));
            //} else {
            //  AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.fueralinea.encontrado"), TipoMensaje.ADVERTENCIA, vista.getCampoReferencia());
            //}
        }

    }

    public void agregaPagoIusacell_Secsa(int _tipoPago, double comision) {

        SION.log(Modulo.PAGO_SERVICIOS, "Agregando pago acumulado Iusacell Secsa", Level.INFO);

        if (vistaModelo.getPeticionValidaReferencia() != null) {

            PeticionConsultaReferenciaBean peticionConsulta = new PeticionConsultaReferenciaBean();
            peticionConsulta.setPaEmisorId((long) EMISOR_IUSACELL_SECSA);
            peticionConsulta.setPaFechaConc(vistaModelo.obtenerFecha());
            peticionConsulta.setPaMontoPago(vistaModelo.getPeticionValidaReferencia().getImporte());
            peticionConsulta.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
            peticionConsulta.setPaReferencia(vistaModelo.getPeticionValidaReferencia().getReferencia());
            peticionConsulta.setPaTiendaId((int) VistaBarraUsuario.getSesion().getTiendaId());

            //if (!existePagoFueraLinea(peticionConsulta)) {
            vistaModelo.getListaPagosAcumulados().add(
                    new ServicioBean(
                    true, String.valueOf(EMISOR_IUSACELL_SECSA), EMISOR_IUSACELL_SECSA,
                    buscaNombreEmisorXId(EMISOR_IUSACELL_SECSA), _tipoPago, comision,
                    vistaModelo.getPeticionValidaReferencia().getImporte(),
                    vistaModelo.getPeticionValidaReferencia().getReferencia(),
                    vistaModelo.getRespuestaValidaReferencia().getIva(),
                    vistaModelo.getPeticionValidaReferencia().getReferencia()));
            //} else {
            //  AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.fueralinea.encontrado"), TipoMensaje.ADVERTENCIA, vista.getCampoReferencia());
            //}
        }

    }

    public void agregaPagoMAZ(int _tipoPago, double comision) {

        SION.log(Modulo.PAGO_SERVICIOS, "Agregando pago MAZ a lista de acumulados", Level.INFO);

        if (vistaModelo.getPeticionValidaReferencia() != null) {

            PeticionConsultaReferenciaBean peticionConsulta = new PeticionConsultaReferenciaBean();
            peticionConsulta.setPaEmisorId((long) EMISOR_MAZ);
            peticionConsulta.setPaFechaConc(vistaModelo.obtenerFecha());
            peticionConsulta.setPaMontoPago(vistaModelo.getPeticionValidaReferencia().getImporte());
            peticionConsulta.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
            peticionConsulta.setPaReferencia(vistaModelo.getPeticionValidaReferencia().getReferencia());
            peticionConsulta.setPaTiendaId((int) VistaBarraUsuario.getSesion().getTiendaId());

            //if (!existePagoFueraLinea(peticionConsulta)) {
            vistaModelo.getListaPagosAcumulados().add(
                    new ServicioBean(
                    true, String.valueOf(EMISOR_MAZ), EMISOR_MAZ,
                    buscaNombreEmisorXId(EMISOR_MAZ), _tipoPago, comision,
                    vistaModelo.getPeticionValidaReferencia().getImporte(),
                    vistaModelo.getPeticionValidaReferencia().getReferencia(),
                    vistaModelo.getRespuestaValidaReferencia().getIva(),
                    vistaModelo.getPeticionValidaReferencia().getReferencia()));
            //} else {
            //  AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.fueralinea.encontrado"), TipoMensaje.ADVERTENCIA, vista.getCampoReferencia());
            //}
        }/*
         * else{ vistaModelo.getListaPagosAcumulados().add( new ServicioBean(
         * true, String.valueOf(EMISOR_MAZ), EMISOR_MAZ,
         * buscaNombreEmisorXId(EMISOR_MAZ), _tipoPago, comision, 200,
         * "1234567890", 0, "1234567890")); }
         */

    }

    private void validarRespuestaReferencia(final int _index, final int _tipoPago, final double _importe, final String _referencia) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {

                if (esReferenciaValida) {
                    ///switch para casos especiales por emisor
                    switch (vistaModelo.getListaEmpresas().get(_index).getEmpresaId()) {
                        case EMISOR_ORIFLAME:
                            //se agrega desde front desde vista d oriflame
                            break;
                        case EMISOR_MAZ:
                            //se pide confirmacion para agregar pago a la tabla desde vista de maz
                            break;
                        case EMISOR_IUSACELL:

                            break;
                        case EMISOR_IUSACELL_SECSA:

                            break;
                        case EMISOR_TOTALPLAY:

                            break;
                        default:
                            agregaPagoAcumuladoDefault(_index, _tipoPago, _importe, _referencia);
                            break;
                    }

                    vista.getCajaOverlay().setVisible(true);

                } else {
                    if (vistaModelo.getRespuestaValidaReferencia() != null) {
                        if (vistaModelo.getMensajeReferencia() != null) {
                            if (!vistaModelo.getMensajeReferencia().isEmpty()) {

                                if (vistaModelo.getMensajeReferencia().contains("|")) {
                                    final String[] msj = vistaModelo.getMensajeReferencia().substring(1).split("\\|");
                                    Platform.runLater(new Runnable() {

                                        @Override
                                        public void run() {
                                            AdministraVentanas.mostrarAlertaRetornaFoco(msj[0],
                                                    TipoMensaje.ERROR, vista.getCampoReferencia());
                                        }
                                    });

                                } else {
                                    Platform.runLater(new Runnable() {

                                        @Override
                                        public void run() {
                                            AdministraVentanas.mostrarAlertaRetornaFoco(vistaModelo.getMensajeReferencia(), TipoMensaje.ADVERTENCIA, vista.getCampoReferencia());
                                        }
                                    });

                                }
                            }
                        } else {
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicios.error.referencia"), TipoMensaje.ERROR, cajaPopupBotones);
                                }
                            });

                        }
                    } else {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicios.error.referencia"), TipoMensaje.ERROR, cajaPopupBotones);
                            }
                        });

                    }
                }
            }
        });
    }

    public void agregarReferencia(final int _index, final int _tipoPago, final double _importe, final String _referencia) {

        esReferenciaValida = false;

        vista.cargarLoading();

        final Task tarea = new Task() {

            @Override
            protected Object call() throws Exception {
                esReferenciaValida = vistaModelo.esReferenciaValidaSwitch(_referencia, _importe, vistaModelo.getListaEmpresas().get(_index).getEmpresaId(), _tipoPago);
                
                
                
                return "ok";
            }
        };
        final Service servicio = new Service() {

            @Override
            protected Task createTask() {
                return tarea;
            }
        };
        servicio.stateProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                if (arg2.equals(Worker.State.SUCCEEDED)) {
                    vista.removerImagenLoading();
                    SION.log(Modulo.PAGO_SERVICIOS, "Tarea de carga de pago termina correctamente, es referencia valida: " + esReferenciaValida, Level.INFO);
                    
                    
                    if (vistaModelo.getListaEmpresas().get(_index).getEmpresaId() == EMISOR_DINEROEXP){
                        
                        double importeDEX =0.00;
                        if (vistaModelo.getRespuestaValidaReferencia()!=null &&
                            vistaModelo.getRespuestaValidaReferencia().getAdicionales()!=null &&
                            vistaModelo.getRespuestaValidaReferencia().getAdicionales().contains("<Saldo>")){
                            importeDEX =  Double.valueOf(vistaModelo.getRespuestaValidaReferencia().getAdicionales().substring(
                            vistaModelo.getRespuestaValidaReferencia().getAdicionales().indexOf("<Saldo>")+7,
                            vistaModelo.getRespuestaValidaReferencia().getAdicionales().indexOf("</Saldo>")));
                        }
                        
                        
                        validarRespuestaReferencia(_index, _tipoPago, importeDEX, _referencia);
                    }else{
                        validarRespuestaReferencia(_index, _tipoPago, _importe, _referencia);
                    }
                    
                    
                } else if (arg2.equals(Worker.State.FAILED)) {

                    StringWriter sw = new StringWriter();
                    servicio.getException().printStackTrace(new PrintWriter(sw));
                    String exceptionAsString = sw.toString();
                    SION.log(Modulo.PAGO_SERVICIOS, "Exception al ejecutar dispositivo: " + exceptionAsString, Level.INFO);


                    vista.removerImagenLoading();
                    SION.log(Modulo.PAGO_SERVICIOS, "Tarea de carga de pago termina con la siquiente excepcion: " + servicio.getException() + ", es referencia valida: " + esReferenciaValida, Level.SEVERE);
                    validarRespuestaReferencia(_index, _tipoPago, _importe, _referencia);
                }
            }
        });
        servicio.start();

    }

    public double getImportePago() {
        return vistaModelo.getImporteTotalPago();
    }

    public void actualizarMontos() {

        double importe = vistaModelo.getImporteTotalPago();
        vista.setMontoVentaProperty(Double.parseDouble(utilerias.getNumeroFormateado(importe)));

        if (vista.getMontoVentaProperty().get() == 0) {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    vista.getCampoMontoCambio().setText("0.00");
                    vista.getCampoMontoRestante().setText("0.00");
                    vista.getCampoMontoRecibido().setText("0.00");
                }
            });
        } else {

            if (vista.getBindingMontoRestante() > 0) {
                vista.getCampoMontoRestante().setText(utilerias.getNumeroFormateado(vista.getBindingMontoRestante()));
            } else {
                vista.getCampoMontoRestante().setText("0.00");
            }

            if (vista.getBindingMontoCambio() > 0) {
                vista.getCampoMontoCambio().setText(utilerias.getNumeroFormateado(vista.getBindingMontoCambio()));
            } else {
                vista.getCampoMontoCambio().setText("0.00");
            }
        }
    }

    public void creaListenersListas(final Label etiquetaPago, final TableView _tabla) {

        vistaModelo.getListaPagosAcumulados().addListener(new ListChangeListener<ServicioBean>() {

            @Override
            public void onChanged(Change<? extends ServicioBean> arg0) {

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        double importe = vistaModelo.getImporteTotalPago();
                        if (importe > 0) {
                            actualizarMontos();
                        } else {
                            etiquetaPago.setText("0.00");
                        }

                        _tabla.requestFocus();
                        if (vistaModelo.getListaPagosAcumulados().size() > 0) {
                            _tabla.getSelectionModel().selectLast();
                        }
                    }
                });
            }
        });

    }

    public void eliminarPago(int _index, String _referencia) {
        int contador = 0;
        int indexId = 0;
        boolean seEncontroregistro = false;

        //con esta linea se evita error de seleccion de tabla
        if (_index >= vistaModelo.getListaPagosAcumulados().size()) {
            _index = vistaModelo.getListaPagosAcumulados().size() - 1;
        }

        if (vistaModelo.getListaPagosAcumulados().get(_index).getCheckId()
                && _referencia.equals(vistaModelo.getListaPagosAcumulados().get(_index).getReferencia())) {
            seEncontroregistro = true;
            indexId = contador;
        }
        contador++;


        if (seEncontroregistro) {
            vistaModelo.getListaPagosAcumulados().remove(_index);
        }
    }

    public int getCantidadPagosTotal() {
        return vistaModelo.getListaPagosAcumulados().size();
    }

    public int getCantidadPagosSeleccionados() {
        return vistaModelo.getListaPagosSeleccionados().size();
    }

    private void aplicarPagoServicio(int _tipoPago) {

        SION.log(Modulo.PAGO_SERVICIOS, "Entrando a metodo de envio de peticion de pago de servicios", Level.INFO);

        //SE QUITA FUERA DE LINEA PARA PS SWITCH
        /*boolean existeParametroFueraLinea = false;
        esFueraLinea = false;
        RespuestaParametroBean respuestaParametro = utilerias.consultaParametro(Accion.VENTA_FL,
                Modulo.VENTA, Modulo.PAGO_SERVICIOS);
        if (antadBinding.existePagoAntad()) {
            //antad solo en linea
            esFueraLinea = false;
        } else {
            if (respuestaParametro != null) {
                if (respuestaParametro.getCodigoError() == 0) {
                    if (respuestaParametro.getValorConfiguracion() == 1) {
                        existeParametroFueraLinea = true;
                        //para casillas desactivadas
                        esFueraLinea = true;
                    }
                }
            }
        }*/

        tipoPago = _tipoPago;
        vistaModelo.getListaPagosSeleccionados().clear();
        boolean existenDesmarcados = false;

        if (vistaModelo.getListaPagosAcumulados().size() > 0) {
            for (ServicioBean bean : vistaModelo.getListaPagosAcumulados()) {
                if (bean.getCheckId()) {
                    vistaModelo.getListaPagosSeleccionados().add(bean);
                } else {
                    existenDesmarcados = true;
                }
            }
            if (vistaModelo.getListaPagosSeleccionados().size() > 0) {

                //valida si el pago a realizar se enviara a switch o antad
                if (antadBinding.existePagoAntad()) {
                    aplicarPagoAntad();
                }else if(pagatodoBinding.existePagoPagatodo()) {
                    
                    SION.log(Modulo.PAGO_SERVICIOS,"Intenta realizar el pago por medio de pagatodo", Level.INFO);
                    aplicarPagoPagatodo();
                }else {
                    //CAMBIO PARA QUE TODOS LOS PAGOS SEAN EN LINEA OMH
                    aplicarPagosSwitch(existenDesmarcados, false, _tipoPago);
                }

            }

        } else {

            SION.log(Modulo.PAGO_SERVICIOS, "No se encontraron pagos a enviar", Level.INFO);

        }

    }

    public void aplicarPagosSwitch(boolean existenDesmarcados, boolean existeParametroFueraLinea, int _tipoPago) {

        SION.log(Modulo.PAGO_SERVICIOS, "cantidad de pagos a enviar: " + vistaModelo.getListaPagosSeleccionados().size(), Level.INFO);
        for (ServicioBean bean : vistaModelo.getListaPagosSeleccionados()) {
            vistaModelo.agregarPeticionPago(bean);
        }

        vistaModelo.agregarTipoPago(Double.parseDouble(utilerias.getNumeroFormateado(vista.getMontoVentaProperty().get())), Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(vista.getCampoMontoRecibido().getText()))), _tipoPago);

        ////msj de alerta de pagos no seleccionados
        if (existenDesmarcados) {
            if (esFueraLinea) {
                SION.log(Modulo.PAGO_SERVICIOS, "Se detectaron casillas sin marcar, mostrando "
                        + "ventana de advertencia de pago fuera de linea", Level.INFO);
            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "Se detectaron casillas sin marcar, mostrando "
                        + "ventana de advertencia de pago en linea", Level.INFO);
            }

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    AdministraVentanas.mostrarAlerta("Existen casillas con servicios sin marcar ¿deseas continuar?",
                            TipoMensaje.ADVERTENCIA, cajaPopupBotones);
                }
            });

        } else {
            SION.log(Modulo.PAGO_SERVICIOS, "No se detectaron casillas vacías, aplicando pago.", Level.INFO);

            if (existeParametroFueraLinea) {
                SION.log(Modulo.PAGO_SERVICIOS, "Comienza proceso de pago de servicio fuera de linea por"
                        + " parametro activado", Level.INFO);

                vistaModelo.llenarPeticionPagoFueraLinea(true, vista.getMontoVentaProperty().get());
                vistaModelo.pagarFueraDeLinea();

                validarPagoLocal(true);
                //validarMontoPagoServicio(EFECTIVO, true, true);

            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "Comienza proceso de pago de servicio en linea", Level.INFO);
                aplicarpago(_tipoPago);
            }
        }

    }

    public void aplicarPagoAntad() {

        antadBinding.aplicarAutorizacion(Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(vista.getCampoMontoRecibido().getText().trim()))), // monto recibido por el pago
                vista.getCampoReferencia().getText().trim());  //referencia ingresada

    }
    
    
     public void aplicarPagoPagatodo() {

         pagatodoBinding.aplicarAutorizacion(Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(vista.getCampoMontoRecibido().getText().trim()))),
                                             this.dComision,
                                             vista.getCampoReferencia().getText().trim());


    }

    public void validarMontoPagoServicio(final int _tipoPago) {
        double montoPorConfirmar = 0;
        
        SION.log(Modulo.PAGO_SERVICIOS, "Valida monto para pago de servicio", Level.INFO);

        //consultar en bd el importe por el q se debe confirmar
        RespuestaParametroBean respuestaParametro = utilerias.consultaParametro(Accion.MONTO_A_CONFIRMAR_PS, Modulo.PAGO_SERVICIOS, Modulo.PAGO_SERVICIOS);

        if (respuestaParametro != null) {
            if (respuestaParametro.getCodigoError() == 0) {
                montoPorConfirmar = respuestaParametro.getValorConfiguracion();
                SION.log(Modulo.PAGO_SERVICIOS, "Monto a confirmar por operador obtenido desde parametro: $" + montoPorConfirmar, Level.INFO);
            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "Monto a confirmar establecido por default : $1500", Level.INFO);
                montoPorConfirmar = 1500;
            }
        } else {
            SION.log(Modulo.PAGO_SERVICIOS, "Monto a confirmar establecido por default : $1500", Level.INFO);
            montoPorConfirmar = 1500;
        }

        boolean existePagoSobreMonto = false;
        ArrayList<ServicioBean> referenciaPorConfirmar = new ArrayList<ServicioBean>();
        int cantidadPagosSobreMonto = 0;

        //validar importe de pago
        for (ServicioBean servicio : vistaModelo.getListaPagosAcumulados()) {
            if (servicio.getImporte() >= montoPorConfirmar && servicio.getCheckId()) {
                existePagoSobreMonto = true;
                //referenciaPorConfirmar.add(servicio);
                cantidadPagosSobreMonto++;
            }
        }

        if (existePagoSobreMonto) {

            SION.log(Modulo.PAGO_SERVICIOS, "Pagos por arriba del monto de validación detectados, pidiendo confirmación de usuario. Cantidad de pagos por encima de monto por validar: " + cantidadPagosSobreMonto, Level.INFO);

            for (ServicioBean servicio : vistaModelo.getListaPagosAcumulados()) {
                if (servicio.getCheckId()) {
                    referenciaPorConfirmar.add(servicio);
                }
            }

            ConfirmacionPagoVista confirmacion = new ConfirmacionPagoVista(this, utilerias);
            confirmacion.abreStageConfirmacion(referenciaPorConfirmar.toArray(new ServicioBean[0]), _tipoPago);
            //confirmacion.abreStageConfirmacion(vistaModelo.getListaPagosSeleccionados().toArray(new ServicioBean[0]), _tipoPago);

//            listenerPopupValidacionMonto = new ChangeListener<Number>() {
//
//                @Override
//                public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
//                    if (arg2.intValue() == 1) {
//                        SION.log(Modulo.VENTA, "Boton aceptar presionado", Level.INFO);
//                        aplicarPagoServicio(_tipoPago);
//                        utilerias.getEstatusPopup().removeListener(listenerPopupValidacionMonto);
//                    } else if (arg2.intValue() == 2) {
//                        SION.log(Modulo.VENTA, "Boton cancelar presionado", Level.INFO);
//                        vista.getCampoMontoRecibido().setText("");
//                        utilerias.getEstatusPopup().removeListener(listenerPopupValidacionMonto);
//                        vista.removerImagenLoading();
//                        Platform.runLater(new Runnable() {
//
//                            @Override
//                            public void run() {
//                                vista.getCampoMontoRecibido().requestFocus();
//                            }
//                        });
//                    }
//                }
//            };
//            utilerias.getEstatusPopup().addListener(listenerPopupValidacionMonto);
//
//            String cadenaMontoPorConfirmar = String.valueOf(montoPorConfirmar);
//            
//            String mensajeConfirmacion = (SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.pregunta.monto").trim()
//                    .replace("%1", String.valueOf(cantidadPagosSobreMonto))
//                    .replace("%2", referenciaPorConfirmar.toString()));
//
//            utilerias.mostrarPopupPregunta(mensajeConfirmacion.
//                    replace("%1", cadenaMontoPorConfirmar.substring(0, cadenaMontoPorConfirmar.indexOf("."))), TipoMensaje.PREGUNTA);
        } else {
            SION.log(Modulo.PAGO_SERVICIOS, "No existen pagos con monto superior a limite maximo para validacion, se continua con el proceso.", Level.INFO);
            aplicarPagoServicio(_tipoPago);

        }
    }

    public void confirmarPago(int _tipoPago) {
        aplicarPagoServicio(_tipoPago);
    }

    public void cancelarPago() {
        vista.getCampoMontoRecibido().setText("");
        vista.removerImagenLoading();
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                vista.getCampoMontoRecibido().requestFocus();
            }
        });
    }

    private String getReferenciasConfirmacion(ArrayList<String> _referencias) {
        String cadena = "";

        if (_referencias.size() > 0) {

            for (String referencia : _referencias) {
                cadena = cadena.concat("\n " + referencia + ", ");
            }
        }

        return cadena;
    }

    private void aplicarpago(final int _tipoPago) {
        vista.cargarLoading();
        final Task tarea = new Task() {

            @Override
            protected Object call() throws Exception {

                vistaModelo.pagarServicios(vista.getMontoVentaProperty().get(), _tipoPago);

                return "ok";
            }
        };
        final Service servicio = new Service() {

            @Override
            protected Task createTask() {
                return tarea;
            }
        };
        servicio.stateProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                if (arg2.equals(Worker.State.SUCCEEDED)) {
                    SION.log(Modulo.PAGO_SERVICIOS, "Tarea de pago termina corerctamente.", Level.INFO);
                    vista.removerImagenLoading();
                    revisarEstadoRespuestaPago();

                } else if (arg2.equals(Worker.State.FAILED)) {
                    SION.log(Modulo.PAGO_SERVICIOS, "Tarea de pago termina con la siguiente excepcion: " + servicio.getException().getLocalizedMessage(), Level.SEVERE);
                    vista.removerImagenLoading();
                    revisarEstadoRespuestaPago();

                }
            }
        });
        servicio.start();
    }

    private void revisarEstadoRespuestaPago() {

        boolean seLogroPago = false;
        //BloqueoDto[] arregloBloqueos;
        int codigoError = 0;

        if (vistaModelo.getRespuestaPagoServicios() != null) {
            SION.log(Modulo.PAGO_SERVICIOS, "Evaluando respuesta de switch", Level.INFO);
            switch (vistaModelo.getRespuestaPagoServicios().getPaCdgError()) {
                case 0:

                    //se recibe respuesta exitosa de BD, se valida si los pagos fueron exitosos
                    vistaModelo.actualizarTransacciónLocal(
                            Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SISTEMA")),
                            Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.MODULO")),
                            Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SUBMODULO")));

                    SION.log(Modulo.PAGO_SERVICIOS, "respuesta validada, se continua con el proceso de impresion e insercion de "
                            + "datos de sesion y venta del dia", Level.INFO);
                    try{
                    impresionTicket(Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(vista.getCampoMontoCambio().getText()))),
                            Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(vista.getCampoMontoRecibido().getText()))),
                            vistaModelo.getRespuestaPagoServicios().getPaTransaccionId(), false);
                    } catch(Exception ex){
                        StringWriter sw = new StringWriter();
                            ex.printStackTrace(new PrintWriter(sw));
                            String exceptionAsString = sw.toString();
                            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un problema al imprimir el ticket "+exceptionAsString, Level.SEVERE);
                    }
                    vistaModelo.agregarObjetoSesion(vistaModelo.getRespuestaPagoServicios().getPaArrayBloqueos(),
                            vistaModelo.getRespuestaPagoServicios().getPaCdgError());
                    vistaModelo.guardarVentaDia(false);
                    evaluarBloqueosVenta();

                    vista.limpiar();
                    vistaModelo.limpiar();

                    break;
                case -1000:
                    //se recibe respuesta exitosa de BD como time out, se valida si los pagos fueron exitosos
                    vistaModelo.actualizarTransacciónLocal(
                            Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SISTEMA")),
                            Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.MODULO")),
                            Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SUBMODULO")));

                    SION.log(Modulo.PAGO_SERVICIOS, "respuesta validada, se continua con el proceso de impresion e insercion de "
                            + "datos de sesion y venta del dia, TIME OUT EN SERVICIO", Level.SEVERE);
                    try{
                    impresionTicket(Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(vista.getCampoMontoCambio().getText()))),
                            Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(vista.getCampoMontoRecibido().getText()))),
                            vistaModelo.getRespuestaPagoServicios().getPaTransaccionId(), false);
                    } catch(Exception ex){
                        StringWriter sw = new StringWriter();
                            ex.printStackTrace(new PrintWriter(sw));
                            String exceptionAsString = sw.toString();
                            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un problema al imprimir el ticket "+exceptionAsString, Level.SEVERE);
                    }
                    vistaModelo.agregarObjetoSesion(vistaModelo.getRespuestaPagoServicios().getPaArrayBloqueos(),
                            vistaModelo.getRespuestaPagoServicios().getPaCdgError());
                    vistaModelo.guardarVentaDia(false);
                    evaluarBloqueosVenta();

                    AdministraVentanas.mostrarAlertaRetornaFoco(vistaModelo.getRespuestaPagoServicios().getPaDescError(),
                            TipoMensaje.DEFAULT, vista.getCampoReferencia());

                    vista.limpiar();
                    vistaModelo.limpiar();
                    break;
                    
                case -1:
                    SION.log(Modulo.PAGO_SERVICIOS, "Se recibe codigo de error, se limpian listas y se muestra msj de error -1", Level.INFO);
                    Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.error.timeout"),
                                            TipoMensaje.ERROR, vista.getCampoReferencia());

                                }
                            });
                    vista.limpiar();
                    vistaModelo.limpiar();
                    vista.getCajaOverlay().setVisible(false);
                    break;
                default:
                    SION.log(Modulo.PAGO_SERVICIOS, "Se recibe codigo de error, se limpian listas y se muestra msj de error", Level.INFO);

                    if (vistaModelo.getRespuestaPagoServicios().getPaDescError() != null) {
                        if (vistaModelo.getRespuestaPagoServicios().getPaDescError().contains("|")) {
                            final String[] msj = vistaModelo.getRespuestaPagoServicios().getPaDescError().split("\\|");
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    AdministraVentanas.mostrarAlertaRetornaFoco(msj[0],
                                            TipoMensaje.ERROR, botonAceptarPagosDesmarcados);
                                }
                            });

                        } else {
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.error.todos"),
                                            TipoMensaje.ERROR, botonAceptarPagosDesmarcados);
                                }
                            });

                        }
                    }

                    vistaModelo.cancelarConciliacionVenta(false);
                    vistaModelo.limpiaParcial();
                    break;
            }
        } else {
            /*SION.log(Modulo.PAGO_SERVICIOS, "No se recibe respuesta de WS, se procede a grabar pago en BD local", Level.INFO);
            ////flujo fuera de linea
            vistaModelo.llenarPeticionPagoFueraLinea(false, vista.getMontoVentaProperty().get());
            vistaModelo.pagarFueraDeLinea();
            validarPagoLocal(false);
            //validarMontoPagoServicio(EFECTIVO, true, false);
            * /*/
            SION.log(Modulo.PAGO_SERVICIOS, "Error al realizar el pago........", Level.INFO);

                    if (vistaModelo.getRespuestaPagoServicios().getPaDescError() != null) {
                        if (vistaModelo.getRespuestaPagoServicios().getPaDescError().contains("|")) {
                            final String[] msj = vistaModelo.getRespuestaPagoServicios().getPaDescError().split("\\|");
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    AdministraVentanas.mostrarAlertaRetornaFoco(msj[0],
                                            TipoMensaje.ERROR, botonAceptarPagosDesmarcados);
                                }
                            });

                        } else {
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.error.todos"),
                                            TipoMensaje.ERROR, botonAceptarPagosDesmarcados);
                                }
                            });

                        }
                    }

                    //vistaModelo.cancelarConciliacionVenta(false);
                    //vistaModelo.limpiaParcial();
                    vista.limpiar();
                    vistaModelo.limpiar();
                    vista.getCajaOverlay().setVisible(false);
       
        }

    }

    private void validarPagoLocal(boolean existeParametroFL) {
        ///validar respuesta
        if (vistaModelo.esPagoLocalExitoso()) {

            impresionTicket(Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(vista.getCampoMontoCambio().getText()))),
                    Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(vista.getCampoMontoRecibido().getText()))),
                    vistaModelo.getRespuestaPagoLocal().getConciliacionId(), true);

            vistaModelo.guardarVentaDia(true);

            if (existeParametroFL) {
                validarBloqueosLocal(vistaModelo.getPeticionpagoLocal().getConciliacionId());
            }

            vista.limpiar();
            vistaModelo.limpiar();

        } else {
            if (vistaModelo.getRespuestaPagoLocal().getDescError() != null) {
                final String[] msj = vistaModelo.getRespuestaPagoLocal().getDescError().substring(1).split("\\|");

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(msj[0],
                                TipoMensaje.ERROR, vista.getTablaServicios());
                    }
                });

            } else {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(
                                Modulo.PAGO_SERVICIOS, "servicio.error.pagolocal"), TipoMensaje.ERROR, vista.getTablaServicios());
                    }
                });

            }

            if (vistaModelo.getPeticionpagoLocal().getConciliacionId() > 0) {
                vistaModelo.cancelarConciliacionVenta(true);
            }

            vistaModelo.limpiaParcial();
        }
    }

    public void validarBloqueosLocal(long _conciliacion) {

        ///valida bloqueos
        if (vistaModelo.consultaBloqueosLocal(_conciliacion) != null) {
            if (vistaModelo.existeBloqueoLocal()) {

                int parametroVentana = 0;
                boolean estaVentanaAbierta = false;

                try {
                    parametroVentana = Integer.parseInt(SION.obtenerParametro(Modulo.SION, "SEGURIDAD.VENTANADESBLOQUEO.EGRESOVALORES"));
                } catch (NumberFormatException e) {
                    parametroVentana = 1;
                }

                SION.log(Modulo.PAGO_SERVICIOS, "Se procede a consultar estatus de ventana", Level.INFO);
                if (verificaVentana.consultaEstatusVentana(parametroVentana)) {
                    SION.log(Modulo.PAGO_SERVICIOS, "Estatus true, la venta continua ", Level.INFO);

                } else {
                    SION.log(Modulo.PAGO_SERVICIOS, "Estatus false, se procede a llamar ventana de traspasos", Level.INFO);

                    String cadenaBloqueo = "";
                    cadenaBloqueo = SION.obtenerMensaje(Modulo.VENTA, "venta.bloqueo.local");

                    switch (vistaModelo.getTipoBloqueolocal()) {
                        case BLOQUEO_EFECTIVO:
                            cadenaBloqueo = cadenaBloqueo.replace("%1", "Efectivo");
                            break;
                        case BLOQUEO_TARJETAS:
                            cadenaBloqueo = cadenaBloqueo.replace("%1", "Tarjetas");
                            break;
                        case BLOQUEO_VALES:
                            cadenaBloqueo = cadenaBloqueo.replace("%1", "Vales");
                            break;
                    }

                    final String cadena = cadenaBloqueo;
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            AdministraVentanas.mostrarAlerta(cadena, TipoMensaje.INFO, botonTraspasos);
                        }
                    });

                }

            } else if (vistaModelo.existeAvisoBloqueoLocal()) {

                String cadenaBloqueo = "";
                cadenaBloqueo = SION.obtenerMensaje(Modulo.VENTA, "venta.avisobloqueo.local");

                switch (vistaModelo.getTipoBloqueolocal()) {
                    case BLOQUEO_EFECTIVO:
                        cadenaBloqueo = cadenaBloqueo.replace("%1", "Efectivo");
                        break;
                    case BLOQUEO_TARJETAS:
                        cadenaBloqueo = cadenaBloqueo.replace("%1", "Tarjetas");
                        break;
                    case BLOQUEO_VALES:
                        cadenaBloqueo = cadenaBloqueo.replace("%1", "Vales");
                        break;
                }

                final String cadenaMsj = cadenaBloqueo;
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(cadenaMsj.replace("%2", String.valueOf(vistaModelo.getAvisosRestantesLocal())),
                                TipoMensaje.INFO, vista.getTablaServicios());
                    }
                });



            }
        }

    }

    private boolean existenPagosCancelados(PagoServicioVentaDto[] _arregloPagos) {
        boolean existenCancelados = false;
        for (PagoServicioVentaDto pago : _arregloPagos) {
            if (pago != null) {
                if (pago.getMovimientoId() == 0) {
                    existenCancelados = true;
                }
            } else {
                existenCancelados = true;
            }
        }
        return existenCancelados;
    }

    private void cargarPagosSeleccionados() {
        vistaModelo.getListaPagosSeleccionados().clear();
        for (ServicioBean bean : vistaModelo.getListaPagosAcumulados()) {
            if (bean.getCheckId()) {
                vistaModelo.getListaPagosSeleccionados().add(bean);
            }
        }

        SION.log(Modulo.PAGO_SERVICIOS, "pagos seleccionados: " + vistaModelo.getListaPagosSeleccionados().toString(), Level.INFO);

    }

    public ObservableList<EmpresaBean> getListaEmpresasCabecera() {
        return vistaModelo.getListaEmpresasCabecera();
    }
    
    
    public ObservableList<EmpresaBean> getListaEmpresas() {
        return vistaModelo.getListaEmpresas();
    }

    public void cargarListaServiciosFrecuentes() {

        listaNombresServiciosFrecuentes.clear();
        ServicioFrecuenteBean[] beans = null;

        try {
            beans = vistaModelo.consultarServiciosFrecuentes(
                    VistaBarraUsuario.getSesion().getPaisId(), VistaBarraUsuario.getSesion().getTiendaId()).getServicioFrecuenteBeans();
        } catch (Exception e) {
            e.printStackTrace();
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un error al cargar servicios frecuentes", Level.SEVERE);
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);

        }

        if (beans != null) {
            for (ServicioFrecuenteBean bean : beans) {
                if (bean != null) {
                    listaNombresServiciosFrecuentes.add(String.valueOf(bean.getModuloId()));
                }
            }
        }
        SION.log(Modulo.PAGO_SERVICIOS, "Lista de servicios frecuentes: " + listaNombresServiciosFrecuentes.toString(), Level.INFO);
    }

    public EmpresaBean getIndiceServicioFrecuente(String _id) {
        EmpresaBean bean = new EmpresaBean();
        int indice = -1;
        int contador = 0;
        for (EmpresaBean _bean : vistaModelo.getListaEmpresas()) {
            if (_bean != null) {
                if (_id.equals(String.valueOf(_bean.getEmpresaId()))) {
                    indice = contador;
                    bean = _bean;
                }
            }
            contador++;
        }
        return bean;
    }

    public int getIndiceServicio(String _id) {

        int indice = -1;
        int contador = 0;
        for (EmpresaBean bean : vistaModelo.getListaEmpresas()) {
            if (bean != null) {
                if (_id.equals(String.valueOf(bean.getDescrpcion().trim()))) {
                    indice = contador;

                }
            }
            contador++;
        }
        return indice;
    }

    public String buscaIdXNombre(String _nombre) {

        String id = "";
        for (EmpresaBean bean : vistaModelo.getListaEmpresas()) {

            if (bean != null) {
                if (bean.getDescrpcion().trim().equals(_nombre.trim())) {

                    id = String.valueOf(bean.getEmpresaId());
                    break;
                }
            }
        }
        return id;
    }
    
    
    public int buscaTipoServicioXNombre(String _nombre) {
        
        int tipo = 1;
        for (EmpresaBean bean : vistaModelo.getListaEmpresas()) {

            if (bean != null) {
                if (bean.getDescrpcion().trim().equals(_nombre.trim())) {

                    tipo = bean.getTipoServicio();
                    break;
                }
            }
        }
        return tipo;
        
    }
    
    public int buscaTipoServicioXEmisorId(int emisorId) {
        
        int tipo = 1;
        for (EmpresaBean bean : vistaModelo.getListaEmpresas()) {

            if (bean != null) {
                if (bean.getEmpresaId() == emisorId) {

                    tipo = bean.getTipoServicio();
                    break;
                }
            }
        }
        return tipo;
        
    }
    
    
    public String buscaIdAgrupacionXNombre(String _nombre) {

        String IdAgrupacion = "";
        for (EmpresaBean bean : vistaModelo.getListaEmpresas()) {
            if (bean != null) {
                if (bean.getDescrpcion().trim().equals(_nombre.trim())) {

                    IdAgrupacion = String.valueOf(bean.getAgrupacionId());
                    break;
                }
            }
        }
        return IdAgrupacion;
    }
    
    
    

    public String buscaNombreEmisorXId(int _emisor) {
        String nombre = "";

        for (EmpresaBean bean : vistaModelo.getListaEmpresas()) {
            if (bean != null) {
                if (bean.getEmpresaId() == _emisor) {
                    nombre = bean.getDescrpcion();
                    break;
                }
            }
        }

        return nombre;
    }

    public void crearListaAutocompletar(final AutoCompleteList<String> list) {

        // filtering of list
        list.inputText().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> source, String oldValue, String newValue) {
                list.getAvailableItems().clear();
                for (String item : vistaModelo.getListaDescEmpresas()) {
                    if (item.toLowerCase().contains(newValue) || item.contains(newValue)) {
                        list.getAvailableItems().add(item);
                    }
                }
                list.getAvailableItemsSelectionModel().selectFirst();
            }
        });

        list.getChosenItems().addListener(new ListChangeListener<String>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends String> change) {
                //System.out.println("Your destiny consists of: ");
                for (String chosen : list.getChosenItems()) {
                    //System.out.println(" => " + chosen);
                    //list.getAvailableItemsSelectionModel().select(chosen);
                    list.setText(chosen);
                }

                if (buscaIdXNombre(vista.getCampoEmisor().getText()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.AVON"))) {
                    vista.cargarComponentesAvon();
                    vista.setRadioVendedorAvon();
                } else if (buscaIdXNombre(vista.getCampoEmisor().getText()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.JAFRA"))) {
                    vista.cargarComponentesJafra();
                } else if (buscaIdXNombre(vista.getCampoEmisor().getText()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.TELEVIA"))) {
                    vista.cargarComponentesTelevia();
                } else if (buscaIdXNombre(vista.getCampoEmisor().getText()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.ORIFLAME"))) {
                    vista.cargarComponentesOriflame();
                } else if (buscaIdXNombre(vista.getCampoEmisor().getText()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.DINEROEXP"))) {
                    vista.cargarComponentesDineroExpress();
                } else if (buscaTipoServicioXNombre(vista.getCampoEmisor().getText()) == PAGOSERVTIPOPIN ){
                    vista.cargarComponentesPines();
                } else {
                        vista.cargarComponentesDefault();
                    }

                vista.getCampoReferencia().setText("");
                vista.getCampoReferencia().requestFocus();

            }
        });

    }
    
    
    public void crearListaAutocompletarOverlay(final AutoCompleteList<String> list) {

        // filtering of list
        list.inputText().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> source, String oldValue, String newValue) {
                list.getAvailableItems().clear();
                for (String item : vistaModelo.getListaDescEmpresas()) {
                    if (item.toLowerCase().contains(newValue) || item.contains(newValue)) {
                        list.getAvailableItems().add(item);
                    }
                }
                list.getAvailableItemsSelectionModel().selectFirst();
            }
        });

        list.getChosenItems().addListener(new ListChangeListener<String>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends String> change) {
                //System.out.println("Your destiny consists of: ");
                for (String chosen : list.getChosenItems()) {
                    //System.out.println(" => " + chosen);
                    //list.getAvailableItemsSelectionModel().select(chosen);
                    list.setText(chosen);
                }
                
                vista.setEmisoraBuscador(vista.getCampoOverlay().getText());
                
                if (buscaIdXNombre(vista.getCampoEmisor().getText()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.AVON"))) {
                    vista.cargarComponentesAvon();
                    vista.setRadioVendedorAvon();
                } else if (buscaIdXNombre(vista.getCampoEmisor().getText()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.JAFRA"))) {
                    vista.cargarComponentesJafra();
                } else if (buscaIdXNombre(vista.getCampoEmisor().getText()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.TELEVIA"))) {
                    vista.cargarComponentesTelevia();
                } else if (buscaIdXNombre(vista.getCampoEmisor().getText()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.ORIFLAME"))) {
                    vista.cargarComponentesOriflame();
                } else if (buscaIdXNombre(vista.getCampoEmisor().getText()).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.DINEROEXP"))) {
                    vista.cargarComponentesDineroExpress();
                } else if (buscaTipoServicioXNombre(vista.getCampoEmisor().getText()) == PAGOSERVTIPOPIN ){
                    vista.cargarComponentesPines();
                } else {
                        vista.cargarComponentesDefault();
                    }

                
                
                
                
                //vista.setEmisoraBuscadorOverlay("");
                vista.ocultaOverlayEmisores();

            }
        });

    }

    public void creaListenerEventoESC() {
        vistaModelo.getListaPagosAcumulados().addListener(new ListChangeListener<ServicioBean>() {

            @Override
            public void onChanged(Change<? extends ServicioBean> arg0) {
                if (vistaModelo.getListaPagosAcumulados().size() == 1) {
                    SION.log(Modulo.PAGO_SERVICIOS, "Removiendo ESC ventana", Level.INFO);
                    AdministraVentanas.removerESCEventHandler();
                    SION.log(Modulo.PAGO_SERVICIOS, "Agregando ESC pago de servicios", Level.INFO);
                    vista.agregarEventoESC();
                } else if (vistaModelo.getListaPagosAcumulados().size() == 0) {
                    SION.log(Modulo.PAGO_SERVICIOS, "Removiendo ESC pago de servicios", Level.INFO);
                    vista.removerEventoESC();
                    SION.log(Modulo.PAGO_SERVICIOS, "Agregando ESC ventana", Level.INFO);
                    AdministraVentanas.agregarESCEventHandler();
                }
            }
        });
    }

    //bloqueos
    public void creaListenerBotonTraspasos() {

        botonTraspasos.setMaxSize(vistaImagenAceptarTraspasos.getFitWidth(), vistaImagenAceptarTraspasos.getFitHeight());
        botonTraspasos.setMinSize(vistaImagenAceptarTraspasos.getFitWidth(), vistaImagenAceptarTraspasos.getFitHeight());
        botonTraspasos.setPrefSize(vistaImagenAceptarTraspasos.getFitWidth(), vistaImagenAceptarTraspasos.getFitHeight());

        botonTraspasos.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {

                AdministraVentanas.cerrarAlerta();
                AdministraVentanas.mostrarEscena(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "ventanasTraspasos"));

            }
        });

    }

    public void creaListenerBotonBloqueos() {

        botonBloqueos.setMaxSize(vistaImagenAceptarBloqueos.getFitWidth(), vistaImagenAceptarBloqueos.getFitHeight());
        botonBloqueos.setMinSize(vistaImagenAceptarBloqueos.getFitWidth(), vistaImagenAceptarBloqueos.getFitHeight());
        botonBloqueos.setPrefSize(vistaImagenAceptarBloqueos.getFitWidth(), vistaImagenAceptarBloqueos.getFitHeight());

        botonBloqueos.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        evaluarBloqueosVenta();
                    }
                });
            }
        });

    }

    public void evaluarBloqueosVenta() {

        vistaModelo.inicializarVariablesBloqueo();

        try {
            vistaModelo.evaluarBloqueoVenta(botonTraspasos);
            if (vistaModelo.existeAvisoBloqueo()) {

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(vistaModelo.getMensajeAvisoBloqueo(),
                                TipoMensaje.INFO, vista.getTablaServicios());
                    }
                });


            } else {
                if (vistaModelo.existeBloqueo()) {

                    int parametroVentana = 0;
                    boolean estaVentanaAbierta = false;

                    try {
                        parametroVentana = Integer.parseInt(SION.obtenerParametro(Modulo.SION, "SEGURIDAD.VENTANADESBLOQUEO.EGRESOVALORES"));
                    } catch (NumberFormatException e) {
                        parametroVentana = 1;
                    }

                    SION.log(Modulo.PAGO_SERVICIOS, "Se procede a consultar estatus de ventana", Level.FINE);
                    if (verificaVentana.consultaEstatusVentana(parametroVentana)) {
                        SION.log(Modulo.PAGO_SERVICIOS, "Estatus true, la venta continua ", Level.FINE);

                    } else {
                        SION.log(Modulo.PAGO_SERVICIOS, "Estatus false, se procede a llamar ventana de traspasos", Level.FINE);
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlerta(vistaModelo.getMensajeBloqueo(), TipoMensaje.INFO, botonTraspasos);
                            }
                        });

                    }

                } else if (vistaModelo.existe2oAvisoBloqueo()) {

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            AdministraVentanas.mostrarAlertaRetornaFoco(vistaModelo.get2oMensajeAvisoBloqueo(),
                                    TipoMensaje.INFO, vista.getTablaServicios());
                        }
                    });


                } else {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            AdministraVentanas.cerrarAlertaFocus(vista.getTablaServicios());
                        }
                    });


                }
            }
        } catch (Exception e) {
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió un error al evaluar los bloqueos del usuario. Error: " + e.toString(), Level.SEVERE);
        }
    }

    //fin bloqueo
    ///impresiones
    public void impresionTicket(double cambio, double montoRecibido, final long transaccion, boolean _esFueraLinea) {
        vista.getCajaOverlay().setVisible(false);
        vistaModelo.armarTicket(montoRecibido, cambio, _esFueraLinea);

        if (!vistaModelo.seImprimioTicket()) {

            int parametroVentana = 0;
            boolean estaVentanaAbierta = false;

            try {
                parametroVentana = Integer.parseInt(SION.obtenerParametro(Modulo.SION, "SEGURIDAD.VENTANADESBLOQUEO.EGRESOVALORES"));
            } catch (NumberFormatException e) {
                parametroVentana = 1;
            }

            if (verificaVentana.consultaEstatusVentana(parametroVentana)) {
                SION.log(Modulo.PAGO_SERVICIOS, "estatus de ventana de bloqueo = true, mostrando alerta de "
                        + "error de impresora y se continúa con la venta", Level.INFO);

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ventas.noimpresora").replace(
                                "%1", String.valueOf(transaccion).concat("I")), TipoMensaje.INFO, vista.getTablaServicios());
                    }
                });


            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "estatus de ventana de bloquoe = false, mostrando alerta de "
                        + "error de impresora en impresión de ticket de venta", Level.INFO);

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        AdministraVentanas.mostrarAlerta(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ventas.noimpresora").replace(
                                "%1", String.valueOf(transaccion).concat("I")), TipoMensaje.INFO, botonBloqueos);
                    }
                });

            }

        }
    }
    ///fin impresiones

    ///autorizador
    public void ejecutarAutorizador() {

        SION.log(Modulo.PAGO_SERVICIOS, "Entrando a método de autorizador", Level.INFO);

        final ControladorValidacion controladorValidacion = new ControladorValidacion();

        controladorValidacion.autorizarEmpleadoAccion(String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()),
                Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "ventas.sistema"))),
                Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "ventas.modulo"))),
                Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "ventas.submodulo"))),
                Integer.parseInt(String.valueOf(SION.obtenerParametro(Modulo.VENTA, "ventas.opcion"))));

        controladorValidacion.getValidacionFinalizada().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observado, Boolean valorViejo, Boolean valorNuevo) {
                if (valorNuevo == true) {

                    RespuestaValidacion respuesta = controladorValidacion.getRespuestaValidacion();
                    if (respuesta == RespuestaValidacion.NO_AUTORIZADO) {

                        regresarFoco();
                    }
                    if (respuesta == RespuestaValidacion.AUTENTICADO) {

                        SION.log(Modulo.PAGO_SERVICIOS, "respuesta de autorizador: autenticado", Level.FINEST);

                        //si el pago es antad se cancela el numero de conciliacion para no ser tomado en cuenta por el demonio
                        if (existePagoAntad()) {
                            antadBinding.cancelarConciliacionAntad();
                        }

                        limpiar();
                        vista.limpiar();
                        vistaModelo.limpiar();
                        regresarFoco();
                        vista.getCajaOverlay().setVisible(false);
                    }
                    if (respuesta == RespuestaValidacion.ERROR_PARAMETRIZACION_SIN_AUTORIZADORES) {

                        regresarFoco();
                    }
                    if (respuesta == RespuestaValidacion.ERROR_CRITICO) {

                        regresarFoco();
                    }
                    if (respuesta == RespuestaValidacion.VALIDACION_ABORTADA) {
                        regresarFoco();
                    }

                }
            }
        });
    }

    public void regresarFoco() {
        vista.getTablaServicios().requestFocus();
    }

    ///
    public void limpiar() {
        vistaModelo.limpiar();
    }

    public void limpiarVista() {
        vista.limpiar();
    }

    public PagoServiciosVistaModelo getVistaModelo() {
        return vistaModelo;
    }

    public boolean existePagoFueraLinea(PeticionConsultaReferenciaBean _peticion) {
        boolean existePago = false;

        RespuestaConsultaReferenciaBean respuestaConsulta = vistaModelo.consultaReferencia(_peticion);

        if (respuestaConsulta != null) {
            if (respuestaConsulta.getPaCdgError() != 0) {
                existePago = true;
            }
        }

        return existePago;
    }

    public boolean existePedidoOriflameFueraLinea(PeticionConsultaOriflameBean _peticion, PagoOriflameBean _pago) {
        boolean existePedido = false;

        RespuestaConsultaOriflameBean respuesta = vistaModelo.consultaPedidoOriflame(_peticion);

        if (respuesta != null) {
            for (String pedido : respuesta.getPedidos()) {
                if (pedido != null) {
                    String numeroPedido = pedido.substring(0, pedido.indexOf("|")).trim();
                    //System.out.println("comparando: "+_pago.getSubReferencia()+" - "+numeroPedido);
                    if (numeroPedido.trim().equals(_pago.getSubReferencia().trim())) {
                        existePedido = true;
                    }
                }
            }
        }

        return existePedido;
    }

    ///antad
    public boolean existePagoAntad() {
        return antadBinding.existePagoAntad();
    }
    
    ///Pagatodo
    public boolean existePagoPagatodo() {
        
        
        return antadBinding.existePagoPagatodo();
    }

    public boolean esEmisorAntad(long _emisor) {
        return antadBinding.esEmisorAntad(_emisor);
    }
    
    public boolean esEmisorPagatodo(long _emisor) {
        return pagatodoBinding.esEmisoPagatodo(_emisor);
    }
    
    
    public ConsultaComisionDtoResp consultaComisionXidEmisor(long emisorId){
        
         return pagatodoBinding.consultaComisionXidEmisor(emisorId);
    }
    
    
    
    
    
    public String comisionXIdEmisor(int idEmisor) {

        String comision = "";
        for (EmpresaBean bean : vistaModelo.getListaEmpresas()) {

            if (bean != null) {
                if(bean.getEmpresaId() == idEmisor){
                    comision = bean.getComision();
                    
                    break;
                }
            }
        }
        return comision;
    }
    
    public void abreStageSwitch(int _emisorIndex, String _refencia, boolean _esTelevia){
        
        double _monto = Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(vista.getCampoPago().getText().trim())));
        
        validadorSwitch.abreStageConfirmacionReferenciaSwitch(_emisorIndex,_monto, _refencia, _esTelevia);
    }
    
    

    public void abreStageAntad() {

        validadorAntad.abreStageConfirmacionReferenciaAntad(Long.parseLong(buscaIdXNombre(vista.getAutocompletarLista().getTextField().getText().trim())),
                Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(vista.getCampoPago().getText().trim()))),
                vista.getCampoReferencia().getText().trim());

    }
    
    
    public void abreStagePagatodo(){
        
       
        
        String strIdEmisor = buscaIdXNombre(vista.getAutocompletarLista().getTextField().getText().trim());
        //dComisiond = Double.parseDouble(comisionXIdEmisor(Integer.valueOf(strIdEmisor)));
   
        double dMonto =0;
        
        int tipoServicio = buscaTipoServicioXEmisorId(Integer.valueOf(strIdEmisor));
        
         if ( tipoServicio == PAGOSERVTIPOPIN){
             dMonto = vista.getMontoPines();
         }else{
             dMonto = Double.parseDouble(utilerias.getNumeroFormateado(Double.parseDouble(vista.getCampoPago().getText().trim())));
         }
        
        validadorPagatodo.abreStageConfirmacionReferenciaPagatodo(Long.parseLong(strIdEmisor),
                                                                  dMonto,
                                                                  vista.getCampoReferencia().getText().trim(),
                                                                  tipoServicio);
        
    }
    
    public void setComision(double _dComision){
        this.dComision = _dComision;
    }
    
    
    
    
     public neto.sion.tienda.pago.servicios.modelo.RespuestaParametroBean getImporteLimValida() {
      
       PagoServiciosDao dao = new PagoServiciosDao();
       
       PeticionParametroBean request = new PeticionParametroBean();
       
        

       request.setSistemaId(Integer.valueOf(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "IMPORTE.LIMITE.PAGOSERVICIOS.SISTEMA")));
       request.setModuloId(Integer.valueOf(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "IMPORTE.LIMITE.PAGOSERVICIOS.MODULO")));
       request.setSubmoduloId(Integer.valueOf(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "IMPORTE.LIMITE.PAGOSERVICIOS.SUBMODULO")));
       request.setConfiguracionId(Integer.valueOf(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "IMPORTE.LIMITE.PAGOSERVICIOS.CONFIGURACION")));
       
      
      return dao.consultaParametroOperacion(request);
     }
     
     
     
     public void limpiarCamposReferencia(){
         
         vista.getCampoReferencia().clear();
         vista.getCampoPago().clear();
         vista.getCampoReferencia().requestFocus();
         
    }
     
     
     public void validarReferenciasDuplicadas(int _emisorIndex, final String _referencia, boolean _esTelevia){
         
         vista.validarReferenciasDuplicadas(_emisorIndex, _referencia, _esTelevia);
         
     }
     
    
}
