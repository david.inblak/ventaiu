/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author esantiago
 */
public class PeticionPagaTodoBean {
    
    private String cajaId;
    private String cajero;
    private String codigoBarras;
    private String comercio;
    private String comision;
    private String emisor;
    private String folioComercio;
    private String horario;
    private String modoIngreso;
    private String monto;
    private String operacion;
    private String referencia;
    private String referencia2;
    private String referencia3;
    private String reintento;
    private String sku;
    private String sucursal;
    private String ticket;
    private String certificado;
    private String conciliacion;
    private String paisId;

    /**
     * @return the cajaId
     */
    public String getCajaId() {
        return cajaId;
    }

    /**
     * @param cajaId the cajaId to set
     */
    public void setCajaId(String cajaId) {
        this.cajaId = cajaId;
    }

    /**
     * @return the cajero
     */
    public String getCajero() {
        return cajero;
    }

    /**
     * @param cajero the cajero to set
     */
    public void setCajero(String cajero) {
        this.cajero = cajero;
    }

    /**
     * @return the codigoBarras
     */
    public String getCodigoBarras() {
        return codigoBarras;
    }

    /**
     * @param codigoBarras the codigoBarras to set
     */
    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    /**
     * @return the comercio
     */
    public String getComercio() {
        return comercio;
    }

    /**
     * @param comercio the comercio to set
     */
    public void setComercio(String comercio) {
        this.comercio = comercio;
    }

    /**
     * @return the comision
     */
    public String getComision() {
        return comision;
    }

    /**
     * @param comision the comision to set
     */
    public void setComision(String comision) {
        this.comision = comision;
    }

    /**
     * @return the emisor
     */
    public String getEmisor() {
        return emisor;
    }

    /**
     * @param emisor the emisor to set
     */
    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    /**
     * @return the folioComercio
     */
    public String getFolioComercio() {
        return folioComercio;
    }

    /**
     * @param folioComercio the folioComercio to set
     */
    public void setFolioComercio(String folioComercio) {
        this.folioComercio = folioComercio;
    }

    /**
     * @return the horario
     */
    public String getHorario() {
        return horario;
    }

    /**
     * @param horario the horario to set
     */
    public void setHorario(String horario) {
        this.horario = horario;
    }

    /**
     * @return the modoIngreso
     */
    public String getModoIngreso() {
        return modoIngreso;
    }

    /**
     * @param modoIngreso the modoIngreso to set
     */
    public void setModoIngreso(String modoIngreso) {
        this.modoIngreso = modoIngreso;
    }

    /**
     * @return the monto
     */
    public String getMonto() {
        return monto;
    }

    /**
     * @param monto the monto to set
     */
    public void setMonto(String monto) {
        this.monto = monto;
    }

    /**
     * @return the operacion
     */
    public String getOperacion() {
        return operacion;
    }

    /**
     * @param operacion the operacion to set
     */
    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    /**
     * @return the referencia
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * @param referencia the referencia to set
     */
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    /**
     * @return the referencia2
     */
    public String getReferencia2() {
        return referencia2;
    }

    /**
     * @param referencia2 the referencia2 to set
     */
    public void setReferencia2(String referencia2) {
        this.referencia2 = referencia2;
    }

    /**
     * @return the referencia3
     */
    public String getReferencia3() {
        return referencia3;
    }

    /**
     * @param referencia3 the referencia3 to set
     */
    public void setReferencia3(String referencia3) {
        this.referencia3 = referencia3;
    }

    /**
     * @return the reintento
     */
    public String getReintento() {
        return reintento;
    }

    /**
     * @param reintento the reintento to set
     */
    public void setReintento(String reintento) {
        this.reintento = reintento;
    }

    /**
     * @return the sku
     */
    public String getSku() {
        return sku;
    }

    /**
     * @param sku the sku to set
     */
    public void setSku(String sku) {
        this.sku = sku;
    }

    /**
     * @return the sucursal
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * @param sucursal the sucursal to set
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * @return the ticket
     */
    public String getTicket() {
        return ticket;
    }

    /**
     * @param ticket the ticket to set
     */
    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    /**
     * @return the certificado
     */
    public String getCertificado() {
        return certificado;
    }

    /**
     * @param certificado the certificado to set
     */
    public void setCertificado(String certificado) {
        this.certificado = certificado;
    }

    
    
    

    /**
     * @return the conciliacion
     */
    public String getConciliacion() {
        return conciliacion;
    }

    /**
     * @param conciliacion the conciliacion to set
     */
    public void setConciliacion(String conciliacion) {
        this.conciliacion = conciliacion;
    }
    
    
    @Override
    public String toString(){
        return "PeticionPagaTodoBean {cajaId : " + cajaId + 
                                   ", cajero : "+ cajero +
                                   ", codigoBarras : "+ codigoBarras + 
                                   ", comercio : " + comercio +
                                   ", comision :  " + comision +
                                   ", emisor : " + emisor + 
                                   ", folioComercio : " + folioComercio +
                                   ", horario : " + horario +
                                   ", modoIngreso : " + modoIngreso + 
                                   ", monto : " + monto +
                                   ", operacion : " + operacion + 
                                   ", referencia : " + referencia +
                                   ", referencia2 : " + referencia2 +
                                   ", referencia3 : " + referencia3 +
                                   ", reintento : " + reintento +
                                   ", sku : " + sku +
                                   ", sucursal : " + sucursal +
                                   ", ticket : " + ticket +
                                   ", certificado : " + certificado +
                                   ", conciliacion : " + conciliacion +
                                   "}";
    }

    /**
     * @return the paisId
     */
    public String getPaisId() {
        return paisId;
    }

    /**
     * @param paisId the paisId to set
     */
    public void setPaisId(String paisId) {
        this.paisId = paisId;
    }

    
    
}
