/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

import java.util.Arrays;

/**
 *
 * @author fvega
 */
public class PeticionVentaDiaBean {

    private VentaDiaBean[] ventaDiaBeans;

    public VentaDiaBean[] getVentaDiaBeans() {
        return ventaDiaBeans;
    }

    public void setVentaDiaBeans(VentaDiaBean[] ventaDiaBeans) {
        this.ventaDiaBeans = ventaDiaBeans;
    }

    @Override
    public String toString() {
        return "PeticionVentaDiaBean{" + "ventaDiaBeans=" + Arrays.toString(ventaDiaBeans) + '}';
    }
}
