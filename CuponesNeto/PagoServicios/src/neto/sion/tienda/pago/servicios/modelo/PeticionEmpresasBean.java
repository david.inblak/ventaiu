/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author fvega
 */
public class PeticionEmpresasBean {

    private long tiendaId;
    private int paisId;

    public int getPaisId() {
        return paisId;
    }

    public void setPaisId(int paisId) {
        this.paisId = paisId;
    }

    public long getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(long tiendaId) {
        this.tiendaId = tiendaId;
    }

    @Override
    public String toString() {
        return "PeticionEmpresasBean{" + "tiendaId=" + tiendaId + ", paisId=" + paisId + '}';
    }
}
