/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vista;

import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.binding.PagoServiciosBinding;
import neto.sion.tienda.pago.servicios.modelo.ServicioBean;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;

/**
 *
 * @author fvega
 */
public class ConfirmacionPagoVista {
    
    //
    private PagoServiciosBinding bindingPS;
    private UtileriasVenta utilerias;
    private ObservableList<ServicioBean> listaPagos = FXCollections.observableArrayList();
    private int tipoPago;

    //contenedores
    public Stage stage;
    private Group grupo;
    private Scene escena;
    private BorderPane panel;
    private VBox cajaPrincipal;
    
    //cajaPrincipal
    private VBox cajaCabecero;
    private VBox cajaTabla;
    private HBox cajaBotones;
    
    //cajaCabecero
    private VBox cajaRellenoCabecero;
    private Label etiquetaPreguntaConfirmación;
    
    //cajaTabla
    private HBox cajaCabeceroTabla;
    private TableView tablaPagos;
    private TableColumn columnaEmisor;
    private TableColumn columnaReferencia;
    private TableColumn columnaImporte;
    
    //cajaCabeceroTabla
    private VBox cajaColumnaEmisor;
    private VBox cajaColumnaReferencia;
    private VBox cajaColumnaImporte;
    
    //cajaColumnaEmisor
    private Label etiquetaCabeceroEmisor;
    
    //cajaColumnaReferencia
    private Label etiquetaCabeceroReferencia;
    
    //cajaColumnaImporte
    private Label etiquetaCabeceroImporte;
        
    //cajaBotones
    private VBox cajaBotonAceptar;
    private VBox cajaBotonCancelar;
    
    //cajaBotonAceptar
    private Image imagenAceptar;
    private Image imagenAceptarHover;
    private ImageView vistaImagenAceptar;
    private Button botonAceptar;
    
    // cajaBotonCancelar
    private Image imagenCancelar;
    private Image imagenCancelarHover;
    private ImageView vistaImagenCancelar;
    private Button botonCancelar;    

    public ConfirmacionPagoVista(PagoServiciosBinding _bindingPS, UtileriasVenta _utilerias) {
        
        //
        this.utilerias = _utilerias;
        this.bindingPS = _bindingPS;
        
        //contenedores
        this.grupo = new Group();
        this.panel = new BorderPane();
        this.cajaPrincipal = utilerias.getVBox(0, 800, 400);
        
        //cajaPrincipal
        this.cajaCabecero = utilerias.getVBox(0, 800, 120);
        this.cajaTabla = utilerias.getVBox(0, 750, 220);
        this.cajaBotones = utilerias.getHBox(0, 800, 60);
        
        //cajaCabecero
        this.cajaRellenoCabecero = utilerias.getVBox(0, 800, 30);
        this.etiquetaPreguntaConfirmación = utilerias.getLabel("Se aplicarán los siguientes pagos. Por favor revise los montos", 750, 80);
    
        //cajaTabla
        this.cajaCabeceroTabla = utilerias.getHBox(0, 750, 30);
        this.tablaPagos = new TableView();
        this.columnaEmisor = utilerias.getColumna("Emisor", 300);
        this.columnaReferencia = utilerias.getColumna("Referencia", 350);
        this.columnaImporte = utilerias.getColumna("importe", 85);
        
        //cajaCabeceroTabla
        this.cajaColumnaEmisor = utilerias.getVBox(0, 300, 30);
        this.cajaColumnaReferencia = utilerias.getVBox(0, 350, 30);
        this.cajaColumnaImporte = utilerias.getVBox(0, 100, 30);
        
        //cajaColumnaEmisor
        this.etiquetaCabeceroEmisor = utilerias.getLabel("Emisor", 300, 25);
        
        //cajaColumnaReferencia
        this.etiquetaCabeceroReferencia = utilerias.getLabel("Referencia", 350, 25);
        
        //cajaColumnaImporte
        this.etiquetaCabeceroImporte = utilerias.getLabel("Importe", 100, 25);
        
        //cajaBotones
        this.cajaBotonAceptar = utilerias.getVBox(0, 400, 60);
        this.cajaBotonCancelar = utilerias.getVBox(0, 400, 60);
    
        //cajaBotonAceptar
        //this.imagenAceptar = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/boton_aceptar.png");
        this.imagenAceptar = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/imgs/ico_valida_pago.png");
        this.imagenAceptarHover = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/boton_aceptar_hover.png");
        this.vistaImagenAceptar = new ImageView(imagenAceptar);
        //this.botonAceptar = new Button("", vistaImagenAceptar);
        this.botonAceptar = new Button();
    
        // cajaBotonCancelar
        //this.imagenCancelar = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/boton_cancelar.png");
        this.imagenCancelar = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/imgs/ico_cancel.png");
        //this.imagenCancelarHover = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/boton_cancelar_hover.png");
        this.imagenCancelarHover = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/imgs/ico_cancel.png");
        this.vistaImagenCancelar = new ImageView(imagenCancelar);
        //this.botonCancelar = new Button("", vistaImagenCancelar);
        this.botonCancelar = new Button();
        
    }
    
    private void agregarEstilos(){
        cajaPrincipal.getStyleClass().add("CajaPrincipalOriflame");
        
        etiquetaPreguntaConfirmación.setWrapText(true);
        etiquetaPreguntaConfirmación.setStyle("-fx-alignment: center; -fx-font: 23pt Verdana; -fx-text-fill: #084B8A;");
        cajaCabecero.setStyle("-fx-alignment: center;");
        
        tablaPagos.getStyleClass().add("TablaOriflame");
        cajaColumnaEmisor.setStyle("-fx-alignment: center;");
        cajaColumnaReferencia.setStyle("-fx-alignment: center;");
        cajaColumnaImporte.setStyle("-fx-alignment: center;");
        etiquetaCabeceroEmisor.setStyle("-fx-alignment: center; -fx-font: 18pt Verdana; -fx-text-fill: white;");
        etiquetaCabeceroReferencia.setStyle("-fx-alignment: center; -fx-font: 18pt Verdana; -fx-text-fill: white;");
        etiquetaCabeceroImporte.setStyle("-fx-alignment: center-left; -fx-font: 18pt Verdana; -fx-text-fill: white;");
        cajaTabla.getStyleClass().add("CajaTabla");
        
        cajaBotonAceptar.setStyle("-fx-alignment: center;");
        cajaBotonCancelar.setStyle("-fx-alignment: center;");
    }
    
    private void ajustaComponentes(){
        
        //cajaCabecero
        cajaCabecero.getChildren().addAll(cajaRellenoCabecero,etiquetaPreguntaConfirmación);
        
        //cajaTabla
        tablaPagos.setMaxSize(750, 190);
        tablaPagos.getColumns().addAll(columnaEmisor,columnaReferencia,columnaImporte);
        cajaColumnaEmisor.getChildren().addAll(etiquetaCabeceroEmisor);
        cajaColumnaReferencia.getChildren().addAll(etiquetaCabeceroReferencia);
        cajaColumnaImporte.getChildren().addAll(etiquetaCabeceroImporte);
        cajaCabeceroTabla.getChildren().addAll(cajaColumnaEmisor,cajaColumnaReferencia,cajaColumnaImporte);
        cajaTabla.getChildren().addAll(cajaCabeceroTabla, tablaPagos);
        
        //cajaBotones
        /*vistaImagenCancelar.setFitWidth(180);
        vistaImagenCancelar.setFitHeight(40);*/
        
        vistaImagenCancelar.setFitWidth(25);
        vistaImagenCancelar.setFitHeight(25);
        
        botonCancelar.getStyleClass().add("buttonCancelar");
        botonCancelar.setText("Cancelar");
        botonCancelar.setGraphic(vistaImagenCancelar);
        botonCancelar.setContentDisplay(ContentDisplay.RIGHT);
        botonCancelar.setWrapText(true);
        
        botonCancelar.setMaxSize(180, 40);
        botonCancelar.setMinSize(180, 40);
        botonCancelar.setPrefSize(180, 40);
        
        /*vistaImagenAceptar.setFitWidth(180);
        vistaImagenAceptar.setFitHeight(40);*/
        
        vistaImagenAceptar.setFitWidth(25);
        vistaImagenAceptar.setFitHeight(25);
        
        botonAceptar.getStyleClass().add("buttonAceptar");
        botonAceptar.setText("Aceptar");
        botonAceptar.setGraphic(vistaImagenAceptar);
        botonAceptar.setContentDisplay(ContentDisplay.RIGHT);
        botonAceptar.setWrapText(true);
        
        botonAceptar.setMaxSize(180, 40);
        botonAceptar.setMinSize(180, 40);
        botonAceptar.setPrefSize(180, 40);
        
        
        cajaBotonAceptar.getChildren().add(botonAceptar);
        cajaBotonCancelar.getChildren().addAll(botonCancelar);
        cajaBotones.getChildren().addAll(cajaBotonAceptar,cajaBotonCancelar);
        
        cajaPrincipal.getChildren().add(cajaCabecero);
        cajaPrincipal.getChildren().add(cajaTabla);
        cajaPrincipal.getChildren().add(cajaBotones);
        
        panel.setTop(cajaPrincipal);
        panel.setMinHeight(400);
        panel.setPrefHeight(400);
        panel.setMaxHeight(400);
        panel.setMinWidth(800);
        panel.setPrefWidth(800);
        panel.setMaxWidth(800);
        
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                grupo.getChildren().addAll(panel);
                botonCancelar.requestFocus();
            }
        });
    }
    
    private void crearHandlers(){
        
        botonCancelar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                SION.log(Modulo.PAGO_SERVICIOS, "Botón cancelar presionado", Level.INFO);
                bindingPS.cancelarPago();
                stage.close();
                arg0.consume();
            }
        });
        
        botonAceptar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                SION.log(Modulo.PAGO_SERVICIOS, "Botón aceptar presionado", Level.INFO);
                bindingPS.confirmarPago(tipoPago);
                stage.close();
                arg0.consume();
            }
        });
        
    }
    
    
    public void creaListeners () {
        
        //quita cabecero de la tabla
        tablaPagos.widthProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
                Pane header = (Pane) tablaPagos.lookup("TableHeaderRow");
                if (header.isVisible()) {
                    header.setMaxHeight(0);
                    header.setMinHeight(0);
                    header.setPrefHeight(0);
                    header.setVisible(false);
                }
            }
        });
        
        botonAceptar.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    vistaImagenAceptar.setImage(imagenAceptarHover);
                } else if (arg1 && !arg2) {
                    vistaImagenAceptar.setImage(imagenAceptar);
                }
            }
        });
        
        botonCancelar.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    vistaImagenCancelar.setImage(imagenCancelarHover);
                } else if (arg1 && !arg2) {
                    vistaImagenCancelar.setImage(imagenCancelar);
                }
            }
        });
        
    }
    
    private void setCursor(final Node _nodo) {
        _nodo.hoverProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    _nodo.setCursor(Cursor.HAND);
                } else if (arg1 && !arg2) {
                    _nodo.setCursor(Cursor.NONE);
                }
            }
        });
    }
    
    private void cargaTabla(ServicioBean[] _listaPagos){
        
        listaPagos.clear();
        ServicioBean[] arregloServicios = new ServicioBean[_listaPagos.length];
        
        int contador = 0;
        
        for(ServicioBean servicio : _listaPagos){
            arregloServicios[contador] = new ServicioBean();
            arregloServicios[contador].setImporte(Double.parseDouble(utilerias.getNumeroFormateado(
                    servicio.getImporte()+servicio.getComision())));
            arregloServicios[contador].setNombreEmisor(servicio.getNombreEmisor());
            arregloServicios[contador].setReferencia(servicio.getReferencia());
            listaPagos.add(arregloServicios[contador]);
            contador++;
        }
        
        bindingPS.asignaPropiedadBean(columnaEmisor, String.class, "nombreEmisor");
        bindingPS.asignaPropiedadBean(columnaImporte, Double.class, "importe");
        bindingPS.asignaPropiedadBean(columnaReferencia, String.class, "referencia");
        tablaPagos.setItems(listaPagos);
    }
    
    public Stage getStageConfirmacion() {
        
        return stage;
    }
    
    public void abreStageConfirmacion(final ServicioBean[] _listaPagos, final int _tipoPago) {

        SION.log(Modulo.PAGO_SERVICIOS, "Abriendo Popup de confirmacion de importe de pago de servicios", Level.INFO);

        if (escena == null) {

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    escena = new Scene(grupo, 800, 400, Color.TRANSPARENT);
                    stage = new Stage(StageStyle.UNDECORATED);
                    stage.initModality(Modality.APPLICATION_MODAL);

                    agregarEstilos();
                    ajustaComponentes();
                    crearHandlers();
                    creaListeners();
                    
                    escena.getStylesheets().addAll(PagoServiciosVista.class.getResource("/neto/sion/tienda/pago/servicios/vista/EstilosPagoServicios.css").toExternalForm());

                    stage.setTitle("Confirmacion de pagos de servicio");
                    stage.setScene(escena);
                    stage.centerOnScreen();
                    stage.show();
                }
            });

        } else {

            stage.setTitle("Confirmacion de pagos de servicio");
            stage.setScene(escena);
            stage.centerOnScreen();
            stage.show();

        }
        
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                tipoPago = _tipoPago;
                cargaTabla(_listaPagos);
                botonCancelar.requestFocus();
            }
        });
    }

}
