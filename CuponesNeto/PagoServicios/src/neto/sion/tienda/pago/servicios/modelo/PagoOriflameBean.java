/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author fvega
 */
public class PagoOriflameBean {

    private SimpleBooleanProperty checkId;
    private SimpleStringProperty subReferencia;
    private SimpleDoubleProperty monto;
    private SimpleStringProperty fecha;
    private SimpleBooleanProperty esDuplicado;

    public PagoOriflameBean(boolean _checkId, String subReferencia, double monto, String fecha, boolean esDuplicado) {
        this.checkId = new SimpleBooleanProperty(_checkId);
        this.subReferencia = new SimpleStringProperty(subReferencia);
        this.monto = new SimpleDoubleProperty(monto);
        this.fecha = new SimpleStringProperty(fecha);
        this.esDuplicado = new SimpleBooleanProperty(esDuplicado);
    }

    public boolean getEsDuplicado() {
        return esDuplicado.get();
    }

    public void setEsDuplicado(SimpleBooleanProperty esDuplicado) {
        this.esDuplicado = esDuplicado;
    }
        
    public boolean getCheckId() {
        return this.checkId.get();
    }

    public void setCheckId(boolean checkId) {
        this.checkId.set(checkId);
    }
    
    public SimpleBooleanProperty checkIdProperty(){
        return checkId;
    }
    
    public String getFecha() {
        return fecha.get();
    }

    public void setFecha(String fecha) {
        this.fecha.set(fecha);
    }
    
    public SimpleStringProperty fechaProperty(){
        return fecha;
    }

    public double getMonto() {
        return monto.get();
    }

    public void setMonto(double monto) {
        this.monto.set(monto);
    }
    
    public SimpleDoubleProperty montoProperty(){
        return monto;
    }

    public String getSubReferencia() {
        return subReferencia.get();
    }

    public void setSubReferencia(String subReferencia) {
        this.subReferencia.set(subReferencia);
    }
    
    public SimpleStringProperty subReferenciaProperty(){
        return subReferencia;
    }

    @Override
    public String toString() {
        return "PagoOriflameBean{" + "checkId=" + checkId + ", subReferencia=" + 
                subReferencia.get() + ", monto=" + monto.get() 
                + ", fecha=" + fecha.get() + ", esDuplicado=" + esDuplicado.get() +'}';
    }
}
