/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author fvega
 */
public class ServicioFrecuenteBean {

    private long moduloId;
    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public long getModuloId() {
        return moduloId;
    }

    public void setModuloId(long moduloId) {
        this.moduloId = moduloId;
    }

    @Override
    public String toString() {
        return "ServicioFrecuenteBean{" + "moduloId=" + moduloId + ", descripcion=" + descripcion + '}';
    }
}
