/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author fvegap
 */
public class PeticionActualizacionPagoFueraLineaBean {

    private long conciliacionId;
    private long transaccionId;
    private int estatusId;
    private String respuestaAntad;

    public long getConciliacionId() {
        return conciliacionId;
    }

    public void setConciliacionId(long conciliacionId) {
        this.conciliacionId = conciliacionId;
    }

    public int getEstatusId() {
        return estatusId;
    }

    public void setEstatusId(int estatusId) {
        this.estatusId = estatusId;
    }

    public long getTransaccionId() {
        return transaccionId;
    }

    public void setTransaccionId(long transaccionId) {
        this.transaccionId = transaccionId;
    }

    public String getRespuestaAntad() {
        return respuestaAntad;
    }

    public void setRespuestaAntad(String respuestaAntad) {
        this.respuestaAntad = respuestaAntad;
    }

    @Override
    public String toString() {
        return "PeticionActualizacionPagoFueraLineaBean{" + "conciliacionId=" + conciliacionId + ", transaccionId=" + transaccionId + ", estatusId=" + estatusId + ", respuestaAntad=" + respuestaAntad + '}';
    }
}
