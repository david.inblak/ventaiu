/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author fvega
 */
public class ServicioBean {

    private boolean checkId;
    private SimpleStringProperty rutaLogo;
    private SimpleIntegerProperty emisorId;
    private SimpleStringProperty nombreEmisor;
    private SimpleIntegerProperty tipoPagoId;
    private SimpleDoubleProperty comision;
    private SimpleDoubleProperty importe;
    private SimpleStringProperty referencia;
    private SimpleDoubleProperty impuesto;
    private String deposito;
       
    public ServicioBean(boolean _checkId, String _rutalogo, int _emisorId,
            String _nombreEmisor, int _tipoPagoId, double _comision, 
            double _importe, String _referencia, double _impuesto, String _deposito) {
        this.checkId = _checkId;
        this.rutaLogo = new SimpleStringProperty(_rutalogo);
        this.emisorId = new SimpleIntegerProperty(_emisorId);
        this.nombreEmisor = new SimpleStringProperty(_nombreEmisor);
        this.tipoPagoId = new SimpleIntegerProperty(_tipoPagoId);
        this.comision = new SimpleDoubleProperty(_comision);
        this.importe = new SimpleDoubleProperty(_importe);
        this.referencia = new SimpleStringProperty(_referencia);
        this.impuesto = new SimpleDoubleProperty(_impuesto);
        this.deposito = _deposito;
    }

    public ServicioBean() {
        this.checkId = false;
        this.rutaLogo = new SimpleStringProperty("");
        this.emisorId = new SimpleIntegerProperty(0);
        this.nombreEmisor = new SimpleStringProperty("");
        this.tipoPagoId = new SimpleIntegerProperty(0);
        this.comision = new SimpleDoubleProperty(0);
        this.importe = new SimpleDoubleProperty(0);
        this.referencia = new SimpleStringProperty("");
        this.impuesto = new SimpleDoubleProperty(0);
        this.deposito = "";
    }
    
    public boolean getCheckId() {
        return this.checkId;
    }

    public void setCheckId(boolean checkId) {
        this.checkId = checkId;
    }

    public int getEmisorId() {
        return this.emisorId.get();
    }

    public void setEmisorId(int emisorId) {
        this.emisorId.set(emisorId);
    }

    public int emisorIdProperty() {
        return this.emisorId.get();
    }

    public double getImporte() {
        return this.importe.get();
    }

    public void setImporte(double importe) {
        this.importe.set(importe);
    }

    public SimpleDoubleProperty importeProperty() {
        return this.importe;
    }

    public String getNombreEmisor() {
        return this.nombreEmisor.get();
    }

    public void setNombreEmisor(String nombreEmisor) {
        this.nombreEmisor.set(nombreEmisor);
    }

    public SimpleStringProperty nombreEmisorProperty() {
        return this.nombreEmisor;
    }

    public String getReferencia() {
        return this.referencia.get();
    }

    public void setReferencia(String referencia) {
        this.referencia.set(referencia);
    }

    public SimpleStringProperty referenciaProperty() {
        return this.referencia;
    }

    public int getTipoPagoId() {
        return this.tipoPagoId.get();
    }

    public void setTipoPagoId(int tipoPagoId) {
        this.tipoPagoId.set(tipoPagoId);
    }

    public SimpleIntegerProperty tipoPagoIdproperty() {
        return this.tipoPagoId;
    }

    public double getComision() {
        return this.comision.get();
    }

    public void setComision(double comision) {
        this.comision.set(comision);
    }

    public double getImpuesto() {
        return this.impuesto.get();
    }

    public void setImpuesto(double impuesto) {
        this.impuesto.set(impuesto);
    }
    
    public SimpleDoubleProperty comisionProperty() {
        return this.comision;
    }

    public String getRutaLogo() {
        return this.rutaLogo.get();
    }

    public void setRutaLogo(String rutaLogo) {
        this.rutaLogo.set(rutaLogo);
    }

    public SimpleStringProperty rutaLogoProperty() {
        return this.rutaLogo;
    }

    public String getDeposito() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito = deposito;
    }

    @Override
    public String toString() {
        return "ServicioBean{" + "checkId=" + checkId + ", rutaLogo=" + rutaLogo.get() 
                + ", emisorId=" + emisorId.get() + ", nombreEmisor=" + nombreEmisor.get() 
                + ", tipoPagoId=" + tipoPagoId.get() + ", comision=" + comision.get() 
                + ", importe=" + importe.get() + ", referencia=" + referencia.get() + ", deposito=" + deposito
                + ", impuesto=" + impuesto.get() + '}';
    }

    
}
