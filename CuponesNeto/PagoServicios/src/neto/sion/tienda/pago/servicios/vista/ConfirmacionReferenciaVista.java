/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vista;

import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import neto.sion.pago.servicios.cliente.dto.RespuestaReferenciaDto;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.vistamodelo.PagoServiciosVistaModelo;
import neto.sion.tienda.pago.servicios.vistamodelo.PagoServiciosVistaModelo.ConfirmacionPago;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;

/**
 *
 * @author fvegap
 */
public class ConfirmacionReferenciaVista {
    
    private UtileriasVenta utilerias;
    private PagoServiciosVistaModelo pagosVistaModelo;

    //contenedores
    public Stage stage;
    private Group grupo;
    private Scene escena;
    private BorderPane panel;
    private VBox cajaPrincipal;
    
    //cajaPrincipal
    private VBox cajaBarraSuperior;
    private VBox cajaLogo;
    private VBox cajaDatos;
    
    //cajaBarraSuperior
    private Image imagenCerrar;
    private ImageView vistaImagenCerrar;
    
    //cajaLogo
    private Image imagenLogo;
    private ImageView vistaImagenLogo;
    
    //cajaDatos
    private VBox cajaEtiquetas;
    private HBox cajaBotones;
    
    //cajaEtiquetas
    private VBox cajaEtiquetaMensaje;
    private HBox cajaInfoPago;
    
    //cajaEtiquetaMensaje
    private Label etiquetaMensaje;
    
    //cajaInfoPago
    private Label etiquetaCliente;
    private Label etiquetaNombreCliente;
    private Label etiquetaSaldo;
    private Label etiquetaClienteSaldo;
    
    //cajaBotones
    private Image imagenBotonCorrecto;
    private Image imagenBotonCorrectoHover;
    private ImageView vistaImagenCorrecto;
    private Button botonCorrecto;
    private Image imagenBotonIncorrecto;
    private Image imagenBotonIncorrectoHover;
    private ImageView vistaImagenIncorrecto;
    private Button botonIncorrecto;
    
    //
    private int tipoPago;
    private double comision;
    
    //
    private SimpleBooleanProperty seAcepto;
    
    private ConfirmacionPago emisor_X_Confirmar;

    public ConfirmacionReferenciaVista(UtileriasVenta _utilerias, int _tipoPago, PagoServiciosVistaModelo _pagosVistaModelo, ConfirmacionPago _emisor_x_confirmar) {
        
        this.utilerias = _utilerias;
        this.pagosVistaModelo = _pagosVistaModelo;
        this.emisor_X_Confirmar = _emisor_x_confirmar;
        
        this.tipoPago = _tipoPago;
        this.comision = 0;
        
        //contenedores
        this.grupo = new Group();
        this.panel = new BorderPane();
        this.cajaPrincipal = utilerias.getVBox(0, 1100, 415);
        
        //cajaPrincipal
        this.cajaBarraSuperior = utilerias.getVBox(0, 1050, 30);
        this.cajaLogo = utilerias.getVBox(0, 1050, 120);
        this.cajaDatos = utilerias.getVBox(0, 1050, 250);
        
        //cajaBarraSuperior
        this.imagenCerrar = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/cerrar.png");
        this.vistaImagenCerrar = new ImageView(imagenCerrar);
        
        //cajaLogo
        switch(emisor_X_Confirmar){
            case TOTALPLAY:
                this.imagenLogo = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/total_play_logo.png");
                break;
            case IUSACELL:
                this.imagenLogo = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/logo_iusa.jpg");
                break;
            case IUSACELL_SECSA:
                this.imagenLogo = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/logo_iusa.jpg");
                break;
        }
        
        
        this.vistaImagenLogo = new ImageView(imagenLogo);
        
        //cajaDatos
        this.cajaEtiquetas = utilerias.getVBox(0, 1070, 180);
        this.cajaBotones = utilerias.getHBox(15, 1070, 60);
        
        //cajaEtiquetas
        this.cajaEtiquetaMensaje = utilerias.getVBox(5, 50, 80);
        this.cajaInfoPago = utilerias.getHBox(5, 1070, 80);
        
        //cajaEtiquetaMensaje
        this.etiquetaMensaje = new Label("Favor de validar la información del pago");
    
        //cajaInfoPago
        this.etiquetaCliente = new Label("Cliente: ");
        this.etiquetaNombreCliente = utilerias.getLabel("", 550, 50);
        this.etiquetaSaldo = new Label("Saldo: ");
        this.etiquetaClienteSaldo = utilerias.getLabel("", 150, 50);
    
        //cajaBotones
        this.imagenBotonCorrecto = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/MAZcorrecto.png");
        this.imagenBotonCorrectoHover = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/MAZcorrecto.png");
        this.vistaImagenCorrecto = new ImageView(imagenBotonCorrecto);
        this.botonCorrecto = new Button("", vistaImagenCorrecto);
        this.imagenBotonIncorrecto = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/MAZincorrecto.png");
        this.imagenBotonIncorrectoHover = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/MAZincorrecto.png");
        this.vistaImagenIncorrecto = new ImageView(imagenBotonIncorrecto);
        this.botonIncorrecto = new Button("", vistaImagenIncorrecto);
        
        //
        this.seAcepto = new SimpleBooleanProperty(false);
        
    }

    private void agregarEstilos(){
        
        cajaBarraSuperior.getStyleClass().add("CajaBarraSuperior");
        cajaLogo.getStyleClass().add("CajaLogoMAZ");
        cajaDatos.setStyle("-fx-alignment: center; -fx-background-color: transparent;");
        
        etiquetaMensaje.setStyle("-fx-alignment: center-right bottom-left; -fx-font: 23pt Verdana; -fx-text-fill: #2F4F4F;");
        etiquetaCliente.setStyle("-fx-alignment: center-right bottom-left; -fx-font: 23pt Verdana; -fx-text-fill: #2F4F4F;");
        etiquetaSaldo.setStyle("-fx-alignment: center-right bottom-left; -fx-font: 23pt Verdana; -fx-text-fill: #2F4F4F;");
     
        etiquetaNombreCliente.getStyleClass().add("TextFieldMAZ");
        etiquetaClienteSaldo.getStyleClass().add("TextFieldMAZ");
        
        cajaEtiquetaMensaje.setStyle("-fx-alignment: center;");
        cajaInfoPago.setStyle("-fx-alignment: center;");
        cajaEtiquetas.setStyle("-fx-alignment: center;");
        
        
        
        
        cajaBotones.setStyle("-fx-alignment: center;");
        
        cajaPrincipal.setStyle("-fx-alignment: center; -fx-background-color: transparent;");
        panel.getStyleClass().add("CajaConfirmacion");
        //grupo.setStyle("-fx-background-color: transparent;");
    }
    
    private void ajustaComponentes(){
        
        //cajaBarraSuperior
        vistaImagenCerrar.setFitWidth(20);
        vistaImagenCerrar.setFitHeight(20);

        //cajaLogo
        vistaImagenLogo.setFitHeight(110);
        vistaImagenLogo.setFitWidth(340);
        
        //cajaDatos
        etiquetaMensaje.setMaxSize(500, 120);
        etiquetaMensaje.setMinSize(500, 120);
        etiquetaMensaje.setPrefSize(500, 120);
       
        etiquetaClienteSaldo.setAlignment(Pos.CENTER);
        etiquetaSaldo.setMaxSize(120, 120);
        etiquetaSaldo.setMinSize(120, 120);
        etiquetaSaldo.setMaxSize(120, 120);
        etiquetaMensaje.setWrapText(true);
        etiquetaMensaje.setTextAlignment(TextAlignment.CENTER);
        cajaEtiquetaMensaje.getChildren().addAll(etiquetaMensaje);
        cajaEtiquetas.getChildren().addAll(cajaEtiquetaMensaje, cajaInfoPago);
        
        //etiquetaNombre.setText("LOS SUPERSONICOS");
        etiquetaNombreCliente.setAlignment(Pos.CENTER);
        etiquetaCliente.setMaxSize(120, 120);
        etiquetaCliente.setMinSize(120, 120);
        etiquetaCliente.setPrefSize(120, 120);
        //cajaInfoPago.getChildren().addAll(etiquetaCliente, etiquetaNombreCliente, etiquetaSaldo, etiquetaClienteSaldo);
        
        
        
        
        vistaImagenCorrecto.setFitWidth(200);
        vistaImagenCorrecto.setFitHeight(40);
        vistaImagenIncorrecto.setFitWidth(200);
        vistaImagenIncorrecto.setFitHeight(40);
        botonCorrecto.setMaxSize(200, 40);
        botonCorrecto.setMinSize(200, 40);
        botonCorrecto.setPrefSize(200, 40);
        botonIncorrecto.setMaxSize(200, 40);
        botonIncorrecto.setMinSize(200, 40);
        botonIncorrecto.setPrefSize(200, 40);
        cajaBotones.getChildren().addAll(botonCorrecto,botonIncorrecto);
        
        cajaBarraSuperior.getChildren().addAll(vistaImagenCerrar);

        cajaLogo.getChildren().addAll(vistaImagenLogo);

        cajaDatos.getChildren().addAll(cajaEtiquetas, cajaBotones);

        cajaPrincipal.getChildren().addAll(cajaBarraSuperior, cajaLogo, cajaDatos);
        
        panel.setTop(cajaPrincipal);
        panel.setMinHeight(415);
        panel.setPrefHeight(415);
        panel.setMaxHeight(415);
        panel.setMinWidth(1100);
        panel.setPrefWidth(1100);
        panel.setMaxWidth(1100);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                grupo.getChildren().addAll(panel);
            }
        });
    }
    
    private void setHover(final ImageView _vistaImagen, final Image _imagen, final Image _imagenHover) {

        _vistaImagen.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    _vistaImagen.setImage(_imagenHover);
                } else if (arg1 && !arg2) {
                    _vistaImagen.setImage(_imagen);
                }
            }
        });
    }
    
    private void crearHandlers(){
        
        botonCorrecto.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                SION.log(Modulo.PAGO_SERVICIOS, "Boton correcto presionado", Level.INFO);
                
                switch (emisor_X_Confirmar){
                    case TOTALPLAY:
                        pagosVistaModelo.agregaReferenciaTotalplay(tipoPago, comision);
                        break;
                    case IUSACELL:
                        pagosVistaModelo.agregaReferenciaIusacell(tipoPago, comision);
                        break;
                    case IUSACELL_SECSA:
                        pagosVistaModelo.agregaReferenciaIusacell_Secsa(tipoPago, comision);
                        break;
                }
                
                
                stage.close();
            }
        });
        
        botonIncorrecto.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                SION.log(Modulo.PAGO_SERVICIOS, "Boton incorrecto presionado", Level.INFO);
                pagosVistaModelo.limpiaParcial();
                stage.close();
            }
        });
        
        vistaImagenCerrar.setOnMouseClicked(new EventHandler<MouseEvent>(){

            @Override
            public void handle(MouseEvent arg0) {
                SION.log(Modulo.PAGO_SERVICIOS, "Boton cerrar presionado", Level.INFO);
                pagosVistaModelo.limpiaParcial();
                pagosVistaModelo.quitaOverlaySeleccionado();
                stage.close();
            }
        });

    }
    
    private void setCursor(final Node _nodo) {
        _nodo.hoverProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    _nodo.setCursor(Cursor.HAND);
                } else if (arg1 && !arg2) {
                    _nodo.setCursor(Cursor.NONE);
                }
            }
        });
    }

    public boolean getSeAcepto() {
        return seAcepto.get();
    }
    
    public Stage getStageMAZ() {
        return stage;
    }
    
    private String obtenerTitularCuenta(RespuestaReferenciaDto _respuestaReferencia){
        String titular = "";
        
        //Ejemplo de cadena de adicionales
        //|switchCon:2~retencion:2~cuentaConcentra:01720199506230~cobraComision:1~sucursalid:1~canalid:2018~cekt:5.0~importe:513.0~sufijo:L~idemisor:216~cliente:ILVER MANUEL LOPEZ ESPINOZA~referencia:09000001004616685~ivaemisor:0.8
        
        try{
            titular = _respuestaReferencia.getAdicionales().substring(_respuestaReferencia.getAdicionales().indexOf("<Cliente>")+9, _respuestaReferencia.getAdicionales().indexOf("</Cliente>"));
        
        }catch(Exception e){
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un error al obtener titular de campo adicionales: "+e.getLocalizedMessage(), Level.WARNING);
            titular="";
        }
        
        return titular;
    }
    
    private String obtenerSaldo(RespuestaReferenciaDto _respuestaReferencia){
        String saldo = "";
        
        //Ejemplo de cadena de adicionales
        //|switchCon:2~retencion:2~cuentaConcentra:01720199506230~cobraComision:1~sucursalid:1~canalid:2018~cekt:5.0~importe:513.0~sufijo:L~idemisor:216~cliente:ILVER MANUEL LOPEZ ESPINOZA~referencia:09000001004616685~ivaemisor:0.8
        
        try{
            saldo = _respuestaReferencia.getAdicionales().substring(_respuestaReferencia.getAdicionales().indexOf("<Saldo>")+7, _respuestaReferencia.getAdicionales().indexOf("</Saldo>"));
        
        }catch(Exception e){
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un error al obtener saldo de campo adicionales: "+e.getLocalizedMessage(), Level.WARNING);
            saldo="";
        }
        
        return saldo;
    }
    
    public void abreStageConfirmacion(final RespuestaReferenciaDto _respuestaReferencia, double _comision) {

        SION.log(Modulo.PAGO_SERVICIOS, "Abriendo pantalla de confirmación de pago de servicio", Level.INFO);

        if (escena == null) {

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    escena = new Scene(grupo, 1100, 415, Color.TRANSPARENT);
                    stage = new Stage(StageStyle.TRANSPARENT);
                    stage.initModality(Modality.APPLICATION_MODAL);

                    agregarEstilos();
                    ajustaComponentes();
                    crearHandlers();
                    
                    setCursor(vistaImagenCerrar);
                    setCursor(botonCorrecto);
                    setCursor(botonIncorrecto);

                    escena.getStylesheets().addAll(PagoServiciosVista.class.getResource("/neto/sion/tienda/pago/servicios/vista/EstilosPagoServicios.css").toExternalForm());
                    
                    if(cajaInfoPago.getChildren().size()==2){
                        cajaInfoPago.getChildren().removeAll(etiquetaCliente,etiquetaNombreCliente);
                    }else if(cajaInfoPago.getChildren().size()==4){
                        cajaInfoPago.getChildren().removeAll(etiquetaCliente,etiquetaNombreCliente, etiquetaSaldo, etiquetaClienteSaldo);
                    }
                    
                    etiquetaNombreCliente.setText(obtenerTitularCuenta(_respuestaReferencia));
                    if(emisor_X_Confirmar.equals(ConfirmacionPago.TOTALPLAY)
                            || emisor_X_Confirmar.equals(ConfirmacionPago.IUSACELL_SECSA)){
                        cajaInfoPago.getChildren().addAll(etiquetaCliente, etiquetaNombreCliente, etiquetaSaldo, etiquetaClienteSaldo);
                        etiquetaClienteSaldo.setText(obtenerSaldo(_respuestaReferencia));
                    }else {
                        cajaInfoPago.getChildren().addAll(etiquetaCliente, etiquetaNombreCliente);
                    }

                    stage.setTitle("Pagos Oriflame");
                    stage.setScene(escena);
                    stage.centerOnScreen();
                    stage.show();
                }
            });

        } else {

            stage.setTitle("Pagos Oriflame");
            stage.setScene(escena);
            stage.centerOnScreen();
            stage.show();

        }
        
        comision = _comision;
        
        
    }
    
     
    
}
