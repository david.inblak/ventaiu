/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.binding;

import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.modelo.PagoOriflameBean;
import neto.sion.tienda.pago.servicios.modelo.ServicioBean;
import neto.sion.tienda.pago.servicios.vista.OriflameVista;
import neto.sion.tienda.pago.servicios.vistamodelo.OriflameVistaModelo;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;

/**
 *
 * @author fvega
 */
public class OriflameBinding {

    private OriflameVista vista;
    private UtileriasVenta utilerias;
    private OriflameVistaModelo vistaModelo;
    private boolean sePermiteMultiSeleccion;
    private SimpleBooleanProperty existeMultiseleccion;

    public OriflameBinding(OriflameVista _vista, UtileriasVenta _utilerias) {

        this.utilerias = _utilerias;
        this.vista = _vista;
        this.vistaModelo = new OriflameVistaModelo();

        this.sePermiteMultiSeleccion = false;
        this.existeMultiseleccion = new SimpleBooleanProperty(false);

    }

    public <E> void asignaPropiedadBean(TableColumn columna, E e, final String id) {
        columna.setCellValueFactory(new PropertyValueFactory<PagoOriflameBean, E>(id));
    }

    public void enlazarTablapagos() {
        vista.getTablaPagos().setItems(vistaModelo.getListaTotalPagos());
    }

    public void evaluaCabecero(String _cadena) {
        String cadenaCabecero = _cadena.substring(_cadena.indexOf("<Cabecero>") + 10, _cadena.indexOf("</Cabecero>"));
        String[] arrayCabecero = cadenaCabecero.split("\\|");

        if (arrayCabecero[0] != null) {
            vista.setCabeceroTabla2(arrayCabecero[0]);
        }
        if (arrayCabecero[1] != null) {
            vista.setCabeceroTabla3(arrayCabecero[1]);
        }
        if (arrayCabecero[2] != null) {
            vista.setCabeceroTabla4(arrayCabecero[2]);
        }
    }

    public void evaluaDatosGenerales(String _cadena) {
        String cadenaDatosGenerales = _cadena.substring(_cadena.indexOf("<General>") + 9, _cadena.indexOf("</General>"));
        String[] arrayGenerales = cadenaDatosGenerales.split("\\|");

        if (arrayGenerales[4] != null) {
            if (arrayGenerales[4].trim().equals("1")) {
                sePermiteMultiSeleccion = true;
            }
        }

        SION.log(Modulo.PAGO_SERVICIOS, "Multiseleccion permitida para esta referencia: " + sePermiteMultiSeleccion, Level.INFO);
    }

    public void evaluarPagos(String _cadena, ServicioBean[] _servicios) {
        String cadenaPagos = _cadena.substring(_cadena.indexOf("<Pagos>") + 7, _cadena.indexOf("</Pagos>"));
        String[] arrayPagos = cadenaPagos.split(",");

        for (String pago : arrayPagos) {
            String[] pagoInd = pago.split("\\|");
            boolean existePago = false;
            
            ///se valida referencias y subreferencias duplicadas
            for (ServicioBean servicio : _servicios) {
                
                if (servicio.getEmisorId() == 118 && servicio.getDeposito().trim().equals(pagoInd[0].trim())) {
                    existePago = true;
                    break;
                }
            }
            if (existePago) {
                
                if (pagoInd[0] != null && pagoInd[1] != null && pagoInd[2] != null) {
                    PagoOriflameBean bean = new PagoOriflameBean(false, pagoInd[0], Double.parseDouble(pagoInd[1].trim()), 
                            pagoInd[2], true);
                    vistaModelo.getListaTotalPagos().add(bean);
                }
            } else {
                
                if (pagoInd[0] != null && pagoInd[1] != null && pagoInd[2] != null) {
                    PagoOriflameBean bean = new PagoOriflameBean(false, pagoInd[0], Double.parseDouble(pagoInd[1].trim()), 
                            pagoInd[2], false);
                    vistaModelo.getListaTotalPagos().add(bean);
                    
                }
            }
            
        }

    }

    public void revisarLista() {
        int contador = 0;

        for (PagoOriflameBean bean : vistaModelo.getListaTotalPagos()) {
            if (bean.getCheckId()) {
                contador++;
            }
        }

        if (contador > 1) {
            existeMultiseleccion.set(true);
        } else {
            existeMultiseleccion.set(false);
        }
    }

    public void setCallbackCheckBox(TableColumn _columna) {
        _columna.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PagoOriflameBean, CheckBox>, ObservableValue<CheckBox>>() {

            @Override
            public ObservableValue<CheckBox> call(
                    final TableColumn.CellDataFeatures<PagoOriflameBean, CheckBox> arg0) {
                final PagoOriflameBean pago = arg0.getValue();
                final CheckBox checkBox = new CheckBox();
                
                if(pago.getEsDuplicado()){
                    checkBox.setDisable(true);
                    checkBox.getStyleClass().add("CheckBoxOriflameDes");
                }else{
                    checkBox.getStyleClass().add("CheckBoxOriflame");
                }
                
                if (pago.getCheckId()) {
                    checkBox.selectedProperty().setValue(Boolean.TRUE);
                }

                checkBox.alignmentProperty().set(Pos.CENTER);
                

                pago.checkIdProperty().addListener(new ChangeListener<Boolean>() {

                    @Override
                    public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {

                        if (arg2 && !arg1) {
                            checkBox.selectedProperty().setValue(Boolean.TRUE);
                        } else if (arg1 && !arg2) {
                            checkBox.selectedProperty().setValue(Boolean.FALSE);
                        }

                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {

                                actualizarMontoTotal();
                                vista.getTablaPagos().requestFocus();
                            }
                        });
                    }
                });

                checkBox.setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent arg0) {

                        for (PagoOriflameBean bean : vistaModelo.getListaTotalPagos()) {
                            if (bean.getSubReferencia().equals(pago.getSubReferencia())
                                    && bean.getFecha().equals(pago.getFecha())
                                    && bean.getMonto() == pago.getMonto()) {
                                if (checkBox.isSelected()) {
                                    bean.setCheckId(true);
                                } else {
                                    bean.setCheckId(false);
                                }
                            } else {
                                if (!sePermiteMultiSeleccion) {
                                    bean.setCheckId(false);
                                }
                            }
                        }
                    }
                });

                return new SimpleObjectProperty<CheckBox>(checkBox);
            }
        });
    }

    private void actualizarMontoTotal() {

        double monto = 0;

        for (PagoOriflameBean pago : vistaModelo.getListaTotalPagos()) {
            if (pago.getCheckId()) {
                monto += pago.getMonto();
            }
        }

        if (monto > 0) {
            vista.getTextoMontoTotal().setText(utilerias.getNumeroFormateado(monto));
        } else {
            vista.getTextoMontoTotal().setText("0.00");
        }

    }

    public void cargarPagosSeleccionados() {

        for (PagoOriflameBean pago : vistaModelo.getListaTotalPagos()) {
            if (pago.getCheckId()) {
                vistaModelo.getListaPagosSeleccionados().add(pago);
            }
        }

    }

    public ObservableList<PagoOriflameBean> getListaPagosSeleccionados() {
        return vistaModelo.getListaPagosSeleccionados();
    }

    public void limpiar() {
        vistaModelo.getListaPagosSeleccionados().clear();
        vistaModelo.getListaTotalPagos().clear();
    }
}
