/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vista;

import java.util.logging.Level;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;

/**
 *
 * @author fvega
 */
public class ValidadorCampoReferencias {

    private String caracteresValidos;
    private boolean esCaracterValido;
    private KeyCombination enter;
    private int maxCaracteres;

    public ValidadorCampoReferencias() {

        this.caracteresValidos = "0123456789qwertyuiopasdfghjklzxcvbnmñQWERTYUIOPASDFGHJKLÑZXCVBNM";
        this.enter = new KeyCodeCombination(KeyCode.ENTER);
    }

    public void setMaXCaracteres(int _valor) {
        SION.log(Modulo.PAGO_SERVICIOS, "Estableciendo maximo valor de caracteres permitidos para referencia: "+(_valor+1), Level.INFO);
        maxCaracteres = _valor;
    }

    public void formatearCampoTexto(final TextField campo) {

        EventHandler<KeyEvent> eventoValidador = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {

                if (!enter.match(arg0)) {

                    //esCaracterValido = false;
                    if (campo.getText().length() <= maxCaracteres) {

                        if (!caracteresValidos.contains(arg0.getCharacter())) {
                            arg0.consume();
                            
                        }
                    }else{
                        arg0.consume();
                    }
                }

            }
        };

        campo.addEventFilter(KeyEvent.KEY_TYPED, eventoValidador);

    }
}