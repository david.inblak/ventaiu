/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vistamodelo;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import neto.sion.tienda.pago.servicios.modelo.PagoOriflameBean;

/**
 *
 * @author fvega
 */
public class OriflameVistaModelo {
    
    private ObservableList<PagoOriflameBean> listaPagosTotal =
            FXCollections.observableArrayList();
    
    private ObservableList<PagoOriflameBean> listaPagosSeleccionados =
            FXCollections.observableArrayList();

    public OriflameVistaModelo() {
    }
    
    public ObservableList<PagoOriflameBean> getListaTotalPagos(){
        return listaPagosTotal;
    }
    
    public ObservableList<PagoOriflameBean> getListaPagosSeleccionados(){
        return listaPagosSeleccionados;
    }
    
}
