package neto.sion.clientews.tarjeta.servicefacade;

import java.rmi.RemoteException;

import neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto;
import neto.sion.clientews.tarjeta.dto.PeticionReversoTarjetaDto;
import neto.sion.clientews.tarjeta.dto.RespuestaPagoTarjetaDto;
import neto.sion.clientews.tarjeta.dto.RespuestaReversoTarjetaDto;

public interface ConsumirTarjetaWS {
	public RespuestaPagoTarjetaDto solicitudPagoTarjeta(PeticionPagoTarjetaDto _peticion) throws RemoteException;
	public RespuestaReversoTarjetaDto solicitudReversoTarjeta(PeticionReversoTarjetaDto _peticion) throws RemoteException;
}
