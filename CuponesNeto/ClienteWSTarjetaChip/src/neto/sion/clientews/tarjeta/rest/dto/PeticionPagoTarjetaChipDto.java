/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.clientews.tarjeta.rest.dto;

/**
 *
 * @author dramirezr
 */
public class PeticionPagoTarjetaChipDto {
    private int paPaisId;
  private long paConciliacionId;
  
  private long uId;
  private String ipTerminal;
  private long tiendaId;
  
    private String	c55;
    private String	c63;
    private boolean	chip;
    private double	comision;
    private boolean	falloLecturaChip;
    private double	monto;
    private String numTerminal;
    private String	track1;
    private String	track2;
    private String canalPago;

    public String getC55() {
        return c55;
    }

    public void setC55(String c55) {
        this.c55 = c55;
    }

    public String getC63() {
        return c63;
    }

    public void setC63(String c63) {
        this.c63 = c63;
    }

    public String getCanalPago() {
        return canalPago;
    }

    public void setCanalPago(String canalPago) {
        this.canalPago = canalPago;
    }

    public boolean isChip() {
        return chip;
    }

    public void setChip(boolean chip) {
        this.chip = chip;
    }

    public double getComision() {
        return comision;
    }

    public void setComision(double comision) {
        this.comision = comision;
    }

    public boolean isFalloLecturaChip() {
        return falloLecturaChip;
    }

    public void setFalloLecturaChip(boolean falloLecturaChip) {
        this.falloLecturaChip = falloLecturaChip;
    }

    public String getIpTerminal() {
        return ipTerminal;
    }

    public void setIpTerminal(String ipTerminal) {
        this.ipTerminal = ipTerminal;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getNumTerminal() {
        return numTerminal;
    }

    public void setNumTerminal(String numTerminal) {
        this.numTerminal = numTerminal;
    }

    public long getPaConciliacionId() {
        return paConciliacionId;
    }

    public void setPaConciliacionId(long paConciliacionId) {
        this.paConciliacionId = paConciliacionId;
    }

    public int getPaPaisId() {
        return paPaisId;
    }

    public void setPaPaisId(int paPaisId) {
        this.paPaisId = paPaisId;
    }

    public long getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(long tiendaId) {
        this.tiendaId = tiendaId;
    }

    public String getTrack1() {
        return track1;
    }

    public void setTrack1(String track1) {
        this.track1 = track1;
    }

    public String getTrack2() {
        return track2;
    }

    public void setTrack2(String track2) {
        this.track2 = track2;
    }

    public long getuId() {
        return uId;
    }

    public void setuId(long uId) {
        this.uId = uId;
    }

    @Override
    public String toString() {
        return "PeticionPagoTarjetaChipDto{" + "paPaisId=" + paPaisId + ", paConciliacionId=" + paConciliacionId + ", uId=" + uId + ", ipTerminal=" + ipTerminal + ", tiendaId=" + tiendaId + ", c55=" + c55 + ", c63=" + c63 + ", chip=" + chip + ", comision=" + comision + ", falloLecturaChip=" + falloLecturaChip + ", monto=" + monto + ", numTerminal=" + numTerminal + ", track1=" + track1 + ", track2=" + track2 + ", canalPago=" + canalPago + '}';
    }
    
    
    
}
