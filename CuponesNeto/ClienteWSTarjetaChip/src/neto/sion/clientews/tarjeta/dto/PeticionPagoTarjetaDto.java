package neto.sion.clientews.tarjeta.dto;

import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;



@SuppressWarnings("serial")
public class PeticionPagoTarjetaDto extends PeticionBaseVentaDto
{
	private String	c55;
	private String	c63;
	private boolean	chip;
	private double	comision;
	private boolean	falloLecturaChip;
	private double	monto;
	private String numTerminal; 
	private String	track1;
	private String	track2;
	private String canalPago;

	public String getC55() {
		return c55;
	}

	public void setC55(String c55) {
		this.c55 = c55;
	}

	public String getC63() {
		return c63;
	}

	public void setC63(String c63) {
		this.c63 = c63;
	}

	public boolean isChip() {
		return chip;
	}

	public void setChip(boolean chip) {
		this.chip = chip;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public boolean isFalloLecturaChip() {
		return falloLecturaChip;
	}

	public void setFalloLecturaChip(boolean falloLecturaChip) {
		this.falloLecturaChip = falloLecturaChip;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getNumTerminal() {
		return numTerminal;
	}

	public void setNumTerminal(String numTerminal) {
		this.numTerminal = numTerminal;
	}

	public String getTrack1() {
		return track1;
	}

	public void setTrack1(String track1) {
		this.track1 = track1;
	}

	public String getTrack2() {
		return track2;
	}

	public void setTrack2(String track2) {
		this.track2 = track2;
	}

	public String getCanalPago() {
		return canalPago;
	}

	public void setCanalPago(String canalPago) {
		this.canalPago = canalPago;
	}

	@Override
	public String toString() {
		return "PeticionPagoBean ["
				+ super.toString()
				+ ", c55=" + c55
				+ ", c63=" + c63
				+ ", comision=" + comision
				+ ", chip=" + chip
				+ ", falloLecturaChip=" + falloLecturaChip
				+ ", monto=" + monto
				+ ", numTerminal=" + numTerminal
				+ ", track1=" + track1
				+ ", track2=" + track2
				+ ", canalPago=" + canalPago
				+ "]";
	}
}
