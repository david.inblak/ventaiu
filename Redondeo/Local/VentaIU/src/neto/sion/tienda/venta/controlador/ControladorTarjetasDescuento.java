package neto.sion.tienda.venta.controlador;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.bean.DatosTarjeta;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pt.iso8583.descuentos.dto.DArticuloDto;
import neto.sion.tienda.pt.iso8583.descuentos.dto.RespuestaValidaDescuentoArticulosDto;
import neto.sion.tienda.pt.iso8583.descuentos.dto.SolicitudValidaTarjetaDescuentosDto;
import neto.sion.tienda.pt.iso8583.descuentos.facade.ClienteTarjetaDesc_CapitalSocialPTISO8583;
import neto.sion.tienda.venta.modelo.ArticuloBean;
import neto.sion.tienda.venta.modelo.TiposDescuento;

public class ControladorTarjetasDescuento {
    /*public static Set<AbstractMap.SimpleEntry<String, TarjetaDescuentos>> tarjetas = 
            new TreeSet<AbstractMap.SimpleEntry<String, TarjetaDescuentos>>();*/
    private Set<ArticuloBean> articulosValidados = new HashSet<ArticuloBean>();
    private Set<ArticuloBean> articulosXValidar = new HashSet<ArticuloBean>();
    private static TarjetaDescuentos tarjetaDescuentos;
    private boolean necesitaValidarArticulos;
    private double ultimoImporteAceptado;
        
    public boolean creartarjetaDescuento(DatosTarjeta _datosTarjeta){
        /*
        for (Iterator<AbstractMap.SimpleEntry<String, TarjetaDescuentos>> it = tarjetas.iterator(); it.hasNext();) {
            AbstractMap.SimpleEntry<String, TarjetaDescuentos> elemento = it.next();
            if( elemento.getKey().equals(_bin) ){
                return elemento.getValue();
            }
        }*/
        
        //validar si es una tarjeta paga todo
        
        //tarjetas.add(new AbstractMap.SimpleEntry<String, TarjetaDescuentos>(_bin, null));
        necesitaValidarArticulos = true;
        if( tarjetaDescuentos == null ){
            tarjetaDescuentos = new TarjetaDescuentos(_datosTarjeta);
            if( _datosTarjeta.getNombreTarjeta().equals("Capital Social") ){
                ClienteTarjetaDesc_CapitalSocialPTISO8583 validador = new ClienteTarjetaDesc_CapitalSocialPTISO8583();
                tarjetaDescuentos.setValidadorArticulos(validador);
            }
        }else{
            return false;
        }
        
        return true;
    }

    public double getUltimoImporteAceptado() {
        return ultimoImporteAceptado;
    }

    public void setUltimoImporteAceptado(double ultimoImporteAceptado) {
        this.ultimoImporteAceptado = ultimoImporteAceptado;
    }
    
    
    
    public boolean isNecesitaValidarArticulos() {
        return necesitaValidarArticulos;
    }

    public void setNecesitaValidarArticulos(boolean necesitaValidarArticulos) {
        if( hayTarjetasDesceunto())
            this.necesitaValidarArticulos = necesitaValidarArticulos;
    }

    public static TarjetaDescuentos getTarjetaDescuentos() {
        return tarjetaDescuentos;
    }

    public static void setTarjetaDescuentos(TarjetaDescuentos tarjetaDescuentos) {
        ControladorTarjetasDescuento.tarjetaDescuentos = tarjetaDescuentos;
    }

    public Set<ArticuloBean> getArticulosValidados() {
        return articulosValidados;
    }

    public void setArticulosValidados(Set<ArticuloBean> articulosValidados) {
        this.articulosValidados = articulosValidados;
    }

    public Set<ArticuloBean> getArticulosXValidar() {
        return articulosXValidar;
    }

    public void setArticulosXValidar(Set<ArticuloBean> articulosXValidar) {
        this.articulosXValidar = articulosXValidar;
    }
    
    
    
    public void resetearTarjetasDescuento(){
        tarjetaDescuentos = null;
        articulosValidados.clear();
        articulosXValidar.clear();
        necesitaValidarArticulos = false;
    }
    
    public static TarjetaDescuentos obtenertarjetaDescuentos(){
        return tarjetaDescuentos;
    }
            
    public void actualizaArticulosXValidar(List<ArticuloBean> todosArticulosValidar){
        for (ArticuloBean articuloBean : todosArticulosValidar) {
            ArticuloBean abean = articuloBean.clonar();
            if( abean.getImporte() < 0 ){
                abean.setCantidad(articuloBean.getCantidad()*-1);
            }
            articulosXValidar.add(abean);      
        }
    }
    
    public ArticuloBean buscarArticuloDescuento(ArticuloBean articulo){
        ArticuloBean artEncontrado = null;
        artEncontrado = articulo;
        for (Iterator<ArticuloBean> it = articulosValidados.iterator(); it.hasNext();) {
            ArticuloBean articuloBean = it.next();
            if( articulo.equals(articuloBean) ){
                artEncontrado = articuloBean;
                break;
            }
        }
       
        return artEncontrado;
    }
    
    public ArticuloBean buscarArticuloXCBDescuento(String codigoBarras){
        ArticuloBean artEncontrado = null;
        for (Iterator<ArticuloBean> it = articulosValidados.iterator(); it.hasNext();) {
            ArticuloBean articuloBean = it.next();
            if( codigoBarras.equals( articuloBean.getPacdgbarras() ) ){
                artEncontrado = articuloBean;
                break;
            }
        }
        return artEncontrado;
    }
    
    public RespuestaValidaDescuentoArticulosDto validarDescuentoArticulos(DatosTarjeta tarjeta) {
        return validarDescuentoArticulos(tarjeta, false);
    }
    
    public RespuestaValidaDescuentoArticulosDto validarDescuentoArticulos(DatosTarjeta tarjeta, boolean forzado) {
        RespuestaValidaDescuentoArticulosDto respuesta = null;
        if( necesitaValidarArticulos || forzado ){
            if(tarjeta==null) tarjeta = tarjetaDescuentos.getDatosTarjeta();

            SION.log(Modulo.VENTA, "Preparando solicitud de validacion de artículos.", Level.WARNING);
            SolicitudValidaTarjetaDescuentosDto sol = new SolicitudValidaTarjetaDescuentosDto();

            sol.setSucursal( String.valueOf(VistaBarraUsuario.getSesion().getTiendaId()) );
            sol.setPais(VistaBarraUsuario.getSesion().getPaisId());
            sol.setOperador( String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()) );
            sol.setTrack(tarjeta.getTrack());
            sol.setTerminal( obtenerOctetosIP(VistaBarraUsuario.getSesion().getIpEstacion())[3] );
            
            double totalEnv =0;
            Set<DArticuloDto> artsXValidar = new HashSet<DArticuloDto>();
            for (Iterator<ArticuloBean> it = articulosXValidar.iterator(); it.hasNext();) {
                ArticuloBean articuloBean = it.next();
                DArticuloDto articuloDto = fnArticuloBean_to_Dto(articuloBean);
                artsXValidar.add(articuloDto);
                
                totalEnv += articuloBean.getImporte();
            }

            sol.setArticulosXValidar(artsXValidar);

        
            try {
                //Lanzar operacion de cliente
                respuesta = tarjetaDescuentos.validarArticulos(sol);
                if( respuesta != null ){
                    
                    articulosXValidar.clear();
                    articulosValidados.clear();
                    necesitaValidarArticulos = false;
                    if( respuesta.getErrorId() == 0 ){
                        //necesitaValidarArticulos = false;
                        setUltimoImporteAceptado( totalEnv );
                        for (DArticuloDto dArticuloDto : respuesta.getArticulosValidados()) {
                            articulosValidados.add( fnArticiloDto_to_Bean(dArticuloDto) );
                        }
                    }else{
                        setUltimoImporteAceptado(0.0d);
                    }
                    
                    if( respuesta.getErrorId() != 1 )
                        respuesta.setErrorId(respuesta.getErrorId());
                    else
                        respuesta.setErrorId(0);
                    
                    respuesta.setErrorMensaje(respuesta.getErrorMensaje());
                }else{
                    respuesta.setErrorId(-1);
                    respuesta.setErrorMensaje("Ocurrio un problema al validar los descuentos por favor intente más tarde.");
                    SION.log(Modulo.VENTA, "No se obtubo respuesta de la validación de descuentos.", Level.WARNING);
                }
            } catch (Exception ex) {
                respuesta = new RespuestaValidaDescuentoArticulosDto();
                respuesta.setErrorId(-1);
                respuesta.setErrorMensaje("Se produjo un error al intentar consultar los descuentos de los artículos.");
                SION.logearExcepcion(Modulo.VENTA, ex, getStackTrace(ex));
            }
        }else{
            respuesta = new RespuestaValidaDescuentoArticulosDto();
            SION.log(Modulo.VENTA, "No es necesario consultar descuentos de artículos.", Level.WARNING);
        }
        return respuesta;
    }
    
    public DArticuloDto fnArticuloBean_to_Dto(ArticuloBean art) {
        DArticuloDto abean = new DArticuloDto();
        abean.setArticuloId     (art.getPaarticuloid());
        abean.setCodigobarras   (art.getPacdgbarras());
        abean.setCantidad       (art.getCantidad());
        abean.setCosto          (art.getPacosto());
        abean.setDescuento      (art.getPadescuento());
        abean.setIepsId         (art.getPaIepsId());
        abean.setIva            (art.getPaiva());
        abean.setPrecio         (art.getPaprecio());
        return abean;
    }
    
    public ArticuloBean fnArticiloDto_to_Bean(DArticuloDto articuloDto) {
        ArticuloBean adto = new ArticuloBean();
        adto.setPaarticuloid(articuloDto.getArticuloId());
        adto.setPacdgbarras(articuloDto.getCodigobarras());
        adto.setCantidad(articuloDto.getCantidad());
        adto.setPacosto(articuloDto.getCosto());
        adto.setPadescuento(articuloDto.getDescuento());
        adto.setPaIepsId((int)articuloDto.getIepsId());
        adto.setPaiva(articuloDto.getIva());
        adto.setPaprecio(articuloDto.getPrecio());
        adto.setTipoDescuento(TiposDescuento.CapitalSocial.ordinal());
        
        return adto;
    }
    
    private String[] obtenerOctetosIP(String ip){
        SION.log(Modulo.VENTA, "IP: "+ip, Level.INFO);
        String []octetos = ip.split("\\.");
        SION.log(Modulo.VENTA, "octetos: "+octetos.length, Level.INFO);
        
        if( octetos.length <= 0 ){
            octetos = new String[]{"","","",""};
        }
        return octetos;
    }
    
    
    
    
    public static String getStackTrace(Throwable aThrowable) {
            Writer result = new StringWriter();
            PrintWriter printWriter = new PrintWriter(result);
            aThrowable.printStackTrace(printWriter);
            return result.toString();
    }

    public boolean hayTarjetasDesceunto() {
        return tarjetaDescuentos!=null;
    }
    
    
}
