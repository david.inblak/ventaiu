/*
 * To change this template, choose Tools | Templates and open the template aplicarDevolucion
 * in
 * the editor.
 */
package neto.sion.tienda.venta.vista;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import com.sion.tienda.genericos.ventanas.Ventana;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Screen;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.controles.vista.Utilerias;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.modelo.TipoBuscador;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.genericos.vista.ControlAutocompletarBusqueda;
import neto.sion.tienda.genericos.vista.skinAutobusqueda;
import neto.sion.tienda.venta.bindings.DevolucionesBindings;
import neto.sion.tienda.venta.bindings.ValidadorTicket;
//import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;

/**
 *
 * @author fvega
 */
public class DevolucionVista implements Ventana {

    //buscador
    private Button b1;
    private Button b2;
    private StringProperty stringProperty;
    public static ControlAutocompletarBusqueda cab;
    private ChangeListener changelistener;
    //instancias
    private Utilerias utilerias;
    private ValidadorTicket validadorTicket;
    private DevolucionesBindings binding;
    //cajas
    private BorderPane panelPrincipal;
    private Group root;
    private VBox cajaPrincipal;
    private VBox cajaDev;
    private HBox cajaSuperior;
    private VBox cajaInferiorPoliticas;
    private VBox cajaInferiorTicket;
    private VBox cajaSuperior_1;
    private HBox cajaSuperior_2;
    private HBox cajaSuperior_3;
    private HBox cajaInferiorPoliticas1;
    private VBox cajaInferiorPoliticas1_1;
    private HBox cajaInferiorPoliticas1_2;
    private HBox cajaInferiorPoliticas1_3;
    private VBox cajaInferiorPoliticas2;
    private VBox cajaInferiorPoliticas3;
    private HBox cajaInferiorPoliticas4;
    private VBox cajaInferiorPoliticas4_1;
    private VBox cajaInferiorPoliticas4_2;
    private VBox cajaInferiorPoliticas4_3;
    private HBox cajaInferiorPoliticas5;
    private HBox cajaInferiorTicket1;
    private HBox cajaInferiorTicket1_1;
    private HBox cajaInferiorTicket1_2;
    private HBox cajaInferiorTicket2;
    private VBox cajaInferiorTicket2_1;
    private HBox cajaInferiorTicket2_1_1;
    private VBox cajaInferiorTicket2_1_2;
    private VBox cajaInferiorTicket2_1_3;
    private VBox cajaInferiorTicket2_2;
    private HBox cajaInferiorTicket2_2_1;
    private VBox cajaInferiorTicket2_2_2;
    private VBox cajaInferiorTicket2_2_3;
    private VBox cajaInferiorTicket2_2_4;
    private HBox cajaInferiorTicket2_2_4_1;
    private VBox cajaInferiorTicket2_2_4_1_1;
    private VBox cajaInferiorTicket2_2_4_1_2;
    private VBox cajaInferiorTicket2_2_4_1_3;
    private HBox cajaInferiorTicket2_2_4_2;
    private VBox cajaInferiorTicket2_2_4_2_1;
    private VBox cajaInferiorTicket2_2_4_2_2;
    private VBox cajaInferiorTicket2_2_4_2_3;
    private HBox cajaInferiorTicket3;
    //componentes parte superior
    private Label etiquetaSeleccionaTipo;
    private RadioButton radioBdevolucionPolitica;
    private Image imagenDevPoliticaSelecccionada;
    private ImageView vistaimagenDevPoliticaSeleccionada;
    private Label etiquetaDevPoliticaSeleccionada;
    private TextField campoTicket;
    private Image imagenDevTicketSeleccionado;
    private ImageView vistaimagenDevTicketSeleccionado;
    private Label etiquetaDevTicketSeleccionado;
    //componentes politicas
    private Label etiquetaProductosSeleccionados;
    private Image imagenProductosSeleccionados;
    private ImageView vistaImagenProductosSeleccionados;
    private Image imagenLupa;
    private ImageView vistaImagenLupa;
    private Image imagenCodigoBarras;
    private ImageView vistaImagenCodigoBarras;
    private Image imagenSeleccionada;
    private ImageView vistaImagenSeleccionada;
    private TextField campoProducto;
    private TableView tablaPoliticas;
    private TableColumn columnaProductoPoliticas;
    private TableColumn columnaCantidadPoliticas;
    private TableColumn columnaExtra2;
    private TableColumn columnaPrecioPoliticas;
    private TableColumn columnaImportePoliticas;
    //TableColumn columnaCantidadDevPoliticas;
    //TableColumn columnaImporteDevPoliticas;
    private TableColumn columnaExtra;
    private Label etiquetaDetalleDevolucion;
    private Label etiquetaProductosDevolucion;
    private Image imagenProductosDevolucion;
    private ImageView vistaImagenProductosDevolucion;
    private TextField campoProsuctosDevolucion;
    private Label etiquetaImporteDevolver;
    private Image imagenImporteDevolver;
    private ImageView vistaImagenImporteDevolver;
    private TextField campoImporteDevolver;
    //componentes tickets
    public Group grupoCasillaCancelacion;
    private Circle circuloCasillaCancelacion;
    private Image imagenCasillaCancelacion;
    private ImageView vistaImagenCasillaCancelacion;
    private Label etiquetaCancelacionTicket;
    private Label etiquetaProductosPorTicket;
    private Label etiquetaProductosSeleccionadosTicket;
    private Label etiquetaDetalleDevTicket;
    private Label etiquetaProductosDevTicket;
    private Label etiquetaImporteDevTicket;
    private TableView tablaTicket;
    private TableView tablaSeleccionadosTicket;
    private TableColumn columnaProductoTicket;
    private TableColumn columnaCantidadTicket;
    private TableColumn columnaPrecioTicket;
    private TableColumn columnaImporteTicket;
    private TableColumn columnaExtraTicket;
    private TableColumn columnaProductoTicketSeleccionado;
    private TableColumn columnaCantidadTicketSeleccionado;
    private TableColumn columnaPrecioTicketSeleccionado;
    private TableColumn columnaImporteTicketSeleccionado;
    private TableColumn columnaExtraTicketSeleccionado;
    private TextField campoProductosDevTicket;
    private TextField campoImporteDevTicket;
    private Image imagenAceptarPoliticas;
    private Image imagenAceptarPoliticasHover;
    private ImageView vistaImagenAceptarPoliticas;
    private Button botonAceptar;
    private Image imagenCancelarPoliticas;
    private Image imagenCancelarPoliticasHover;
    private ImageView vistaImagenCancelarPoliticas;
    private Button botonCancelar;
    //constantes
    private double ancho;
    private double alto;
    private boolean estaSucia;
    private ChangeListener listener;
    private EventHandler<KeyEvent> eventosPanel;
    private final int PANTALLA_POLITICAS = 0;
    private final int PANTALLA_TICKET = 1;
    private int idPantalla;
    private String cadena;
    private boolean esAutorizadorConsumido;
    private boolean esDevolucionTicket;
    /////loading
    ImageView imagenLoading;
    Screen s;
    /////keys
    private final KeyCombination F2 = new KeyCodeCombination(KeyCode.F2);
    private final KeyCombination F8 = new KeyCodeCombination(KeyCode.F8);
    private final KeyCombination F10 = new KeyCodeCombination(KeyCode.F10);
    private final KeyCombination ESCAPE = new KeyCodeCombination(KeyCode.ESCAPE);
    private final KeyCombination DERECHA = new KeyCodeCombination(KeyCode.RIGHT);
    private final KeyCombination IZQUIERDA = new KeyCodeCombination(KeyCode.LEFT);
    private final KeyCombination ABAJO = new KeyCodeCombination(KeyCode.DOWN);
    private final KeyCombination ARRIBA = new KeyCodeCombination(KeyCode.UP);
    private final KeyCombination TAB = new KeyCodeCombination(KeyCode.TAB);
    private final KeyCombination SHIFTTAB = new KeyCodeCombination(KeyCode.TAB, KeyCodeCombination.SHIFT_ANY);
    private final KeyCombination ENTER = new KeyCodeCombination(KeyCode.ENTER);
    private final KeyCombination SUPR = new KeyCodeCombination(KeyCode.DELETE);
    private final KeyCombination MAS = new KeyCodeCombination(KeyCode.EQUALS);
    private final KeyCombination ADD = new KeyCodeCombination(KeyCode.ADD);
    private final KeyCombination SUBTRACT = new KeyCodeCombination(KeyCode.SUBTRACT);
    private final KeyCombination MENOS = new KeyCodeCombination(KeyCode.MINUS);

    public DevolucionVista() {


        SION.log(Modulo.VENTA, "Entrando a constructor de Vista de la devolución", Level.INFO);

        this.utilerias = new Utilerias();
        this.validadorTicket = new ValidadorTicket();
        this.binding = new DevolucionesBindings(this,utilerias);
        this.alto = utilerias.getAlturaMonitor();
        this.ancho = utilerias.getAnchoMonitor();
        //componentes caja superior
        this.etiquetaSeleccionaTipo = utilerias.getLabel("Seleccione el tipo de devolución", ancho * .3, alto * .05);
        this.radioBdevolucionPolitica = new RadioButton("Devolución por política");
        this.imagenDevPoliticaSelecccionada = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Dev_seleccionado.png");
        //this.imagenDevPoliticaSelecccionada = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Dev_seleccionado.png"));
        this.vistaimagenDevPoliticaSeleccionada = new ImageView(imagenDevPoliticaSelecccionada);
        this.etiquetaDevPoliticaSeleccionada = new Label("", vistaimagenDevPoliticaSeleccionada);
        //DevolucionVista.campoTicket = utilerias.getTextField("Devolución por ticket", ancho*.2, alto*.05);
        this.campoTicket = validadorTicket.getTextFieldFormatoTicket("I", "O", ancho * .2, alto * .05, "Devolución por ticket");
        this.imagenDevTicketSeleccionado = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Dev_seleccionado.png");
        //this.imagenDevTicketSeleccionado = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Dev_seleccionado.png"));
        this.vistaimagenDevTicketSeleccionado = new ImageView(imagenDevTicketSeleccionado);
        this.etiquetaDevTicketSeleccionado = new Label("", vistaimagenDevTicketSeleccionado);
        //componentes caja inferior politicas
        this.etiquetaProductosSeleccionados = new Label("Productos seleccionados para devolución");
        this.imagenProductosSeleccionados = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Dev_prod.png");
        //this.imagenProductosSeleccionados = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Dev_prod.png"));
        this.vistaImagenProductosSeleccionados = new ImageView(imagenProductosSeleccionados);
        this.imagenLupa = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Dev_lupa.png");
        //this.imagenLupa = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Dev_lupa.png"));
        this.vistaImagenLupa = new ImageView(imagenLupa);
        /*
         * this.imagenCodigoBarras = new
         * Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Dev_busca.png"));
         * this.vistaImagenCodigoBarras; this.imagenAutorizada;
         * this.vistaImagenAutorizada;
         */
        this.campoProducto = utilerias.getTextField("Buscar producto...", ancho * .2, alto * .045);

        this.tablaPoliticas = new TableView();
        //this.columnaCantidadDevPoliticas = utilerias.getColumna("Cantidad a Devolver", ancho*.15);
        this.columnaCantidadPoliticas = utilerias.getColumna("Cantidad", ancho * .15);
        this.columnaExtra2 = utilerias.getColumna("", ancho * .05);
        //this.columnaImporteDevPoliticas = utilerias.getColumna("Importe a Devolver", ancho*.15);
        this.columnaImportePoliticas = utilerias.getColumna("Importe", ancho * .15);
        this.columnaPrecioPoliticas = utilerias.getColumna("Precio", ancho * .15);
        this.columnaProductoPoliticas = utilerias.getColumna("Producto", ancho * .35);
        this.columnaExtra = utilerias.getColumna("", ancho * .03);

        this.etiquetaDetalleDevolucion = new Label("Detalle de devolución");

        this.etiquetaProductosDevolucion = new Label("Productos en devolución");
        this.imagenProductosDevolucion = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Dev_prod2.png");
        //this.imagenProductosDevolucion = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Dev_prod2.png"));
        this.vistaImagenProductosDevolucion = new ImageView(imagenProductosDevolucion);
        this.campoProsuctosDevolucion = utilerias.getTextField("0", ancho * .15, alto * .05);

        this.etiquetaImporteDevolver = new Label("Importe a devolver");
        this.imagenImporteDevolver = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Dev_importe.png");
        //this.imagenImporteDevolver = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Dev_importe.png"));
        this.vistaImagenImporteDevolver = new ImageView(imagenImporteDevolver);
        this.campoImporteDevolver = utilerias.getTextField("0.00", ancho * .15, alto * .05);

        this.imagenAceptarPoliticas = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_Aceptar.png");
        this.imagenAceptarPoliticasHover = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_Aceptar_2.png");
        //this.imagenAceptarPoliticas = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/btn_Aceptar.png"));
        //this.imagenAceptarPoliticasHover = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/btn_Aceptar_2.png"));
        this.vistaImagenAceptarPoliticas = new ImageView(imagenAceptarPoliticas);
        this.botonAceptar = new Button();

        this.imagenCancelarPoliticas = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_Cancelar.png");
        this.imagenCancelarPoliticasHover = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_Cancelar_2.png");
        //this.imagenCancelarPoliticas = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/btn_Cancelar.png"));
        //this.imagenCancelarPoliticasHover = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/btn_Cancelar_2.png"));
        this.vistaImagenCancelarPoliticas = new ImageView(imagenCancelarPoliticas);
        this.botonCancelar = new Button();

        //componentes caja inferior ticket

        this.grupoCasillaCancelacion = new Group();
        this.circuloCasillaCancelacion = new Circle(10, 10, 10, Color.BLACK);
        this.imagenCasillaCancelacion = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Reimpresion_casillaT.png");
        this.vistaImagenCasillaCancelacion = new ImageView(imagenCasillaCancelacion);
        this.etiquetaCancelacionTicket = new Label("Cancelación de ticket");

        this.etiquetaProductosPorTicket = new Label("Productos para devolución por ticket");
        this.etiquetaProductosSeleccionadosTicket = new Label("Productos seleccionados a devolución");
        this.etiquetaDetalleDevTicket = new Label("Detalle de devolución");
        this.etiquetaProductosDevTicket = new Label("Productos en devolución");
        this.etiquetaImporteDevTicket = new Label("Importe a devolver");

        this.tablaTicket = new TableView();
        this.tablaSeleccionadosTicket = new TableView();

        this.columnaCantidadTicket = utilerias.getColumna("Cantidad", ancho * .06);
        this.columnaImporteTicket = utilerias.getColumna("Importe", ancho * .06);
        this.columnaPrecioTicket = utilerias.getColumna("Precio", ancho * .06);
        this.columnaProductoTicket = utilerias.getColumna("Producto", ancho * .25);
        this.columnaExtraTicket = utilerias.getColumna("Producto", ancho * .02);

        this.columnaCantidadTicketSeleccionado = utilerias.getColumna("Cantidad", ancho * .06);
        this.columnaImporteTicketSeleccionado = utilerias.getColumna("Importe", ancho * .06);
        this.columnaPrecioTicketSeleccionado = utilerias.getColumna("Precio", ancho * .06);
        this.columnaProductoTicketSeleccionado = utilerias.getColumna("Producto", ancho * .25);
        this.columnaExtraTicketSeleccionado = utilerias.getColumna("Producto", ancho * .02);

        this.campoProductosDevTicket = utilerias.getTextField("0", ancho * .1, alto * .05);
        this.campoImporteDevTicket = utilerias.getTextField("0.00", ancho * .1, alto * .05);

        //cajas
        panelPrincipal = new BorderPane();
        this.root = new Group();
        this.cajaPrincipal = utilerias.getVBox(5, (ancho), (alto * .78));
        cajaDev = utilerias.getVBox(5, (ancho * .97), (alto * .78));
        this.cajaSuperior = utilerias.getHBox(5, ancho * .97, alto * .1);

        this.cajaSuperior_1 = utilerias.getVBox(5, ancho * .36, alto * .1);
        cajaSuperior_2 = utilerias.getHBox(5, ancho * .25, alto * .1);
        this.cajaSuperior_3 = utilerias.getHBox(5, ancho * .36, alto * .1);

        this.cajaInferiorPoliticas = utilerias.getVBox(0, ancho * .97, alto * .65);
        this.cajaInferiorTicket = utilerias.getVBox(0, ancho * .97, alto * .65);

        this.cajaInferiorPoliticas1 = utilerias.getHBox(5, ancho * .9, alto * .05);
        this.cajaInferiorPoliticas1_1 = utilerias.getVBox(5, ancho * .28, alto * .05);
        this.cajaInferiorPoliticas1_2 = utilerias.getHBox(5, ancho * .17, alto * .05);
        this.cajaInferiorPoliticas1_3 = utilerias.getHBox(5, ancho * .28, alto * .05);
        this.cajaInferiorPoliticas2 = utilerias.getVBox(0, ancho * .97, alto * .38);
        this.cajaInferiorPoliticas3 = utilerias.getVBox(0, ancho * .9, alto * .05);
        this.cajaInferiorPoliticas4 = utilerias.getHBox(0, ancho * .9, alto * .12);
        this.cajaInferiorPoliticas4_1 = utilerias.getVBox(5, ancho * .32, alto * .12);
        this.cajaInferiorPoliticas4_2 = utilerias.getVBox(15, ancho * .26, alto * .12);
        this.cajaInferiorPoliticas4_3 = utilerias.getVBox(5, ancho * .32, alto * .12);
        this.cajaInferiorPoliticas5 = utilerias.getHBox(30, ancho * .97, alto * .05);

        this.cajaInferiorTicket1 = utilerias.getHBox(10, ancho * .9, alto * .05);
        this.cajaInferiorTicket1_1 = utilerias.getHBox(10, ancho * .45, alto * .05);
        this.cajaInferiorTicket1_2 = utilerias.getHBox(5, ancho * .45, alto * .05);
        this.cajaInferiorTicket2 = utilerias.getHBox(5, ancho * .9, alto * .55);
        this.cajaInferiorTicket2_1 = utilerias.getVBox(0, ancho * .45, alto * .55);
        this.cajaInferiorTicket2_1_1 = utilerias.getHBox(0, ancho * .45, alto * .05);
        this.cajaInferiorTicket2_1_2 = utilerias.getVBox(0, ancho * .45, alto * .45);
        this.cajaInferiorTicket2_1_3 = utilerias.getVBox(0, ancho * .45, alto * .05);
        this.cajaInferiorTicket2_2 = utilerias.getVBox(0, ancho * .45, alto * .55);
        this.cajaInferiorTicket2_2_1 = utilerias.getHBox(25, ancho * .45, alto * .05);
        this.cajaInferiorTicket2_2_2 = utilerias.getVBox(0, ancho * .45, alto * .3);
        this.cajaInferiorTicket2_2_3 = utilerias.getVBox(0, ancho * .45, alto * .05);
        this.cajaInferiorTicket2_2_4 = utilerias.getVBox(3, ancho * .45, alto * .15);
        this.cajaInferiorTicket2_2_4_1 = utilerias.getHBox(3, ancho * .45, alto * .06);
        this.cajaInferiorTicket2_2_4_1_1 = utilerias.getVBox(1, ancho * .06, alto * .06);
        this.cajaInferiorTicket2_2_4_1_2 = utilerias.getVBox(1, ancho * .21, alto * .06);
        this.cajaInferiorTicket2_2_4_1_3 = utilerias.getVBox(1, ancho * .15, alto * .06);
        this.cajaInferiorTicket2_2_4_2 = utilerias.getHBox(0, ancho * .45, alto * .06);
        this.cajaInferiorTicket2_2_4_2_1 = utilerias.getVBox(1, ancho * .06, alto * .06);
        this.cajaInferiorTicket2_2_4_2_2 = utilerias.getVBox(1, ancho * .21, alto * .06);
        this.cajaInferiorTicket2_2_4_2_3 = utilerias.getVBox(1, ancho * .15, alto * .06);
        this.cajaInferiorTicket3 = utilerias.getHBox(30, ancho * .9, alto * .05);

        this.cadena = "";
        this.esDevolucionTicket = false;
        this.esAutorizadorConsumido = false;
        this.idPantalla = PANTALLA_POLITICAS;
        this.eventosPanel = null;

        establecerEstilos();
        establecerValoresComponentesVisuales();
        aplicarEventos();
        navegacionTeclas();

        this.listener = new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {

                if (arg2) {
                    if (idPantalla == 0) {
                        tablaPoliticas.requestFocus();
                        radioBdevolucionPolitica.selectedProperty().set(true);
                    } else {
                        tablaTicket.requestFocus();
                    }

                }
            }
        };
        root.focusedProperty().addListener(listener);

        this.b1 = new Button();
        this.b2 = new Button();
        this.stringProperty = new SimpleStringProperty();
        this.changelistener = new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {

                binding.operacionesbuscador(panelPrincipal, stringProperty, campoProducto, campoProsuctosDevolucion, campoImporteDevolver);

            }
        };

        this.estaSucia = false;
        this.s = (Screen) Screen.getPrimary();
        this.imagenLoading = new ImageView(utilerias.getImagen("/com/sion/tienda/genericos/images/popup/loading.gif"));


    }

    private void establecerEstilos() {

        SION.log(Modulo.VENTA, "Estableciendo los estilos de componentes de Devoluciones", Level.FINEST);

        //caja superior
        cajaSuperior.getStyleClass().add("CajaFondo3");

        cajaSuperior_1.getStyleClass().add("CajaCenterRight");
        cajaSuperior_2.getStyleClass().add("CajaCenter");
        cajaSuperior_3.getStyleClass().add("CajaCenterLeft");

        campoTicket.getStyleClass().add("TextField");

        //politicas
        radioBdevolucionPolitica.getStyleClass().add("RadioButton");
        campoProducto.getStyleClass().add("TextField");


        etiquetaDetalleDevolucion.setStyle("-fx-alignment: center; -fx-font: 15pt Verdana; -fx-text-fill: #2E64FE");

        etiquetaProductosSeleccionados.setStyle("-fx-alignment: center-left; -fx-font: 19pt Verdana; -fx-text-fill: #2E64FE");

        etiquetaSeleccionaTipo.setStyle("-fx-alignment: center-left; -fx-font: 19pt Verdana; -fx-text-fill: #2E64FE");

        etiquetaImporteDevolver.setFont(Font.font("", FontWeight.BOLD, 15));
        etiquetaProductosDevolucion.setFont(Font.font("", FontWeight.BOLD, 15));
        etiquetaProductosDevolucion.setStyle("-fx-alignment: center-left;");
        etiquetaImporteDevolver.setStyle("-fx-alignment: center-left;");

        campoProsuctosDevolucion.getStyleClass().add("TextField");
        campoImporteDevolver.getStyleClass().add("TextField");


        //ticket
        etiquetaCancelacionTicket.setStyle("-fx-alignment: center-left; -fx-font: 19pt Verdana; -fx-text-fill: #2E64FE");

        etiquetaProductosPorTicket.setStyle("-fx-alignment: center-left; -fx-font: 19pt Verdana; -fx-text-fill: #2E64FE");
        etiquetaProductosSeleccionadosTicket.setStyle("-fx-alignment: center-left; -fx-font: 19pt Verdana; -fx-text-fill: #2E64FE");
        etiquetaDetalleDevTicket.setStyle("-fx-alignment: center-left; -fx-font: 19pt Verdana; -fx-text-fill: #2E64FE");

        etiquetaProductosDevTicket.setFont(Font.font("", FontWeight.BOLD, 15));
        etiquetaImporteDevTicket.setFont(Font.font("", FontWeight.BOLD, 15));
        etiquetaProductosDevTicket.setStyle("-fx-alignment: center-left;");
        etiquetaImporteDevTicket.setStyle("-fx-alignment: center-left;");

        campoProductosDevTicket.getStyleClass().add("TextField");
        campoImporteDevTicket.getStyleClass().add("TextField");

        tablaTicket.setMaxSize(ancho * .45, alto * .45);
        tablaTicket.setMinSize(ancho * .45, alto * .45);
        tablaTicket.setPrefSize(ancho * .45, alto * .45);

        tablaSeleccionadosTicket.setMaxSize(ancho * .45, alto * .3);
        tablaSeleccionadosTicket.setMinSize(ancho * .45, alto * .3);
        tablaSeleccionadosTicket.setPrefSize(ancho * .45, alto * .3);

        //cajas

        cajaInferiorTicket2_2_4_1_1.getStyleClass().add("CajaCenter");
        cajaInferiorTicket2_2_4_1_2.getStyleClass().add("CajaCenterLeft");
        cajaInferiorTicket2_2_4_1_3.getStyleClass().add("CajaCenter");

        cajaInferiorTicket2_2_4_2_1.getStyleClass().add("CajaCenter");
        cajaInferiorTicket2_2_4_2_2.getStyleClass().add("CajaCenterLeft");
        cajaInferiorTicket2_2_4_2_3.getStyleClass().add("CajaCenter");

        cajaInferiorTicket1_1.getStyleClass().add("CajaCenterLeft");
        cajaInferiorTicket1_2.getStyleClass().add("CajaCenterLeft");

        cajaInferiorTicket2_1_1.getStyleClass().add("CajaFondo1");
        cajaInferiorTicket2_1_3.getStyleClass().add("CajaFondo5");

        cajaInferiorTicket2_2_1.getStyleClass().add("CajaFondo1");
        cajaInferiorTicket2_2_3.getStyleClass().add("CajaFondo4");
        cajaInferiorTicket2_2_4.getStyleClass().add("CajaFondo5");

        cajaInferiorTicket3.getStyleClass().add("CajaCenterRight");

        cajaInferiorPoliticas.getStyleClass().add("CajaCenter");
        cajaInferiorTicket.getStyleClass().add("CajaCenter");

        cajaInferiorPoliticas4_1.getStyleClass().add("CajaCenterRight");
        cajaInferiorPoliticas4_2.getStyleClass().add("CajaCenter");
        cajaInferiorPoliticas4_3.getStyleClass().add("CajaCenterLeft");

        cajaInferiorPoliticas1_1.getStyleClass().add("CajaCenterRight");
        cajaInferiorPoliticas1_2.getStyleClass().add("CajaCenterLeft");
        cajaInferiorPoliticas1_3.getStyleClass().add("CajaCenterLeft");

        cajaInferiorPoliticas1.getStyleClass().add("CajaFondo1");
        cajaInferiorPoliticas2.getStyleClass().add("CajaCenter");
        cajaInferiorPoliticas3.getStyleClass().add("CajaFondo4");
        cajaInferiorPoliticas4.getStyleClass().add("CajaFondo5");
        cajaInferiorPoliticas5.getStyleClass().add("CajaCenter");

        cajaPrincipal.getStyleClass().add("CajaTopLeft");
        //cajaDev.getStyleClass().add("CapturaFront");
        cajaDev.getStyleClass().add("CajaConBorde");

        panelPrincipal.getStyleClass().add("CajaCenter");



    }

    private void establecerValoresComponentesVisuales() {

        //politicas
        tablaPoliticas.setMaxSize(ancho * .9, alto * .36);
        tablaPoliticas.setMinSize(ancho * .9, alto * .36);
        tablaPoliticas.setPrefSize(ancho * .9, alto * .36);
        tablaPoliticas.getColumns().addAll(columnaCantidadPoliticas, columnaExtra2, columnaProductoPoliticas,
                columnaPrecioPoliticas, columnaImportePoliticas, columnaExtra);

        vistaimagenDevTicketSeleccionado.setFitHeight(alto * .03);
        vistaimagenDevTicketSeleccionado.setFitWidth(ancho * .03);

        vistaimagenDevPoliticaSeleccionada.setFitHeight(alto * .03);
        vistaimagenDevPoliticaSeleccionada.setFitWidth(ancho * .03);

        etiquetaDevPoliticaSeleccionada.setMaxSize(ancho * .04, alto * .03);
        etiquetaDevPoliticaSeleccionada.setMinSize(ancho * .04, alto * .03);
        etiquetaDevPoliticaSeleccionada.setPrefSize(ancho * .04, alto * .03);

        etiquetaDevTicketSeleccionado.setMaxSize(ancho * .04, alto * .03);
        etiquetaDevTicketSeleccionado.setMinSize(ancho * .04, alto * .03);
        etiquetaDevTicketSeleccionado.setPrefSize(ancho * .04, alto * .03);

        vistaImagenLupa.setFitHeight(alto * .04);
        vistaImagenLupa.setFitWidth(ancho * .03);

        vistaImagenProductosSeleccionados.setFitHeight(alto * .045);
        vistaImagenProductosSeleccionados.setFitWidth(alto * .055);

        vistaImagenAceptarPoliticas.setFitHeight(alto * .045);
        vistaImagenAceptarPoliticas.setFitWidth(ancho * .1);
        vistaImagenCancelarPoliticas.setFitHeight(alto * .045);
        vistaImagenCancelarPoliticas.setFitWidth(ancho * .1);

        botonAceptar.setMaxSize(ancho * .1, alto * .045);
        botonAceptar.setMinSize(ancho * .1, alto * .045);
        botonAceptar.setPrefSize(ancho * .1, alto * .045);

        botonCancelar.setMaxSize(ancho * .1, alto * .045);
        botonCancelar.setMinSize(ancho * .1, alto * .045);
        botonCancelar.setPrefSize(ancho * .1, alto * .045);

        botonAceptar.setGraphic(vistaImagenAceptarPoliticas);
        botonCancelar.setGraphic(vistaImagenCancelarPoliticas);

        //cajas
        cajaSuperior_1.getChildren().add(etiquetaSeleccionaTipo);
        cajaSuperior_2.getChildren().addAll(radioBdevolucionPolitica, etiquetaDevPoliticaSeleccionada);
        cajaSuperior_3.getChildren().add(campoTicket);

        cajaSuperior.getChildren().addAll(cajaSuperior_1, cajaSuperior_2, cajaSuperior_3);

        //politicas
        cajaInferiorPoliticas1_1.getChildren().addAll(etiquetaProductosSeleccionados);
        cajaInferiorPoliticas1_2.getChildren().addAll(vistaImagenProductosSeleccionados);
        cajaInferiorPoliticas1_3.getChildren().addAll(vistaImagenLupa, campoProducto);

        cajaInferiorPoliticas1.getChildren().addAll(cajaInferiorPoliticas1_1, cajaInferiorPoliticas1_2, cajaInferiorPoliticas1_3);
        cajaInferiorPoliticas2.getChildren().addAll(tablaPoliticas);
        cajaInferiorPoliticas3.getChildren().add(etiquetaDetalleDevolucion);
        cajaInferiorPoliticas5.getChildren().addAll(botonAceptar, botonCancelar);

        cajaInferiorPoliticas4_1.getChildren().addAll(vistaImagenProductosDevolucion, vistaImagenImporteDevolver);
        cajaInferiorPoliticas4_2.getChildren().addAll(etiquetaProductosDevolucion, etiquetaImporteDevolver);
        cajaInferiorPoliticas4_3.getChildren().addAll(campoProsuctosDevolucion, campoImporteDevolver);

        cajaInferiorPoliticas4.getChildren().addAll(cajaInferiorPoliticas4_1, cajaInferiorPoliticas4_2, cajaInferiorPoliticas4_3);

        cajaInferiorPoliticas.getChildren().addAll(cajaInferiorPoliticas1, cajaInferiorPoliticas2, cajaInferiorPoliticas3,
                cajaInferiorPoliticas4, cajaInferiorPoliticas5);

        //ticket

        circuloCasillaCancelacion.setLayoutX(15);
        circuloCasillaCancelacion.setLayoutY(12);

        grupoCasillaCancelacion.getChildren().add(vistaImagenCasillaCancelacion);

        tablaTicket.getColumns().addAll(columnaCantidadTicket, columnaProductoTicket, columnaPrecioTicket, columnaImporteTicket);

        tablaSeleccionadosTicket.getColumns().addAll(columnaCantidadTicketSeleccionado, columnaProductoTicketSeleccionado,
                columnaPrecioTicketSeleccionado, columnaImporteTicketSeleccionado);

        cajaInferiorTicket1_1.getChildren().addAll(grupoCasillaCancelacion, etiquetaCancelacionTicket);

        cajaInferiorTicket2_2_4_1_2.getChildren().add(etiquetaProductosDevTicket);
        cajaInferiorTicket2_2_4_1_3.getChildren().add(campoProductosDevTicket);

        cajaInferiorTicket2_2_4_2_2.getChildren().add(etiquetaImporteDevTicket);
        cajaInferiorTicket2_2_4_2_3.getChildren().add(campoImporteDevTicket);

        cajaInferiorTicket2_1_1.getChildren().add(etiquetaProductosPorTicket);
        cajaInferiorTicket2_1_2.getChildren().add(tablaTicket);

        cajaInferiorTicket2_2_1.getChildren().add(etiquetaProductosSeleccionadosTicket);
        cajaInferiorTicket2_2_2.getChildren().add(tablaSeleccionadosTicket);
        cajaInferiorTicket2_2_3.getChildren().add(etiquetaDetalleDevTicket);
        cajaInferiorTicket2_2_4.getChildren().addAll(cajaInferiorTicket2_2_4_1, cajaInferiorTicket2_2_4_2);

        cajaInferiorTicket2_2_4_1.getChildren().addAll(cajaInferiorTicket2_2_4_1_1, cajaInferiorTicket2_2_4_1_2,
                cajaInferiorTicket2_2_4_1_3);
        cajaInferiorTicket2_2_4_2.getChildren().addAll(cajaInferiorTicket2_2_4_2_1, cajaInferiorTicket2_2_4_2_2,
                cajaInferiorTicket2_2_4_2_3);

        cajaInferiorTicket2_1.getChildren().addAll(cajaInferiorTicket2_1_1, cajaInferiorTicket2_1_2, cajaInferiorTicket2_1_3);
        cajaInferiorTicket2_2.getChildren().addAll(cajaInferiorTicket2_2_1, cajaInferiorTicket2_2_2, cajaInferiorTicket2_2_3,
                cajaInferiorTicket2_2_4);

        /*SION.log(Modulo.VENTA, "Consultando parámetro activación de casilla de cancelación", Level.INFO);
        binding.consultarParametroOperacion(
                VistaBarraUsuario.getSesion().getPaisId(), 
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacion.sistema")), 
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacion.modulo")), 
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacion.submodulo")), 
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacion.configuracion")));
        if(binding.getCodigoErrorParametro()==0){
            SION.log(Modulo.VENTA, "Parámetro activado, se agrega casilla", Level.INFO);
            cajaInferiorTicket1.getChildren().addAll(cajaInferiorTicket1_1);
        }else{
            SION.log(Modulo.VENTA, "Parámetro desactivado: "+binding.getDescErrorParametro(), Level.INFO);
        }*/
                
        //cajaInferiorTicket1.getChildren().addAll(cajaInferiorTicket1_2);
        cajaInferiorTicket2.getChildren().addAll(cajaInferiorTicket2_1, cajaInferiorTicket2_2);
        //cajaInferiorTicket3.getChildren().addAll());

        cajaInferiorTicket.getChildren().addAll(cajaInferiorTicket1, cajaInferiorTicket2, cajaInferiorTicket3);

        cajaDev.getChildren().addAll(cajaSuperior, cajaInferiorPoliticas);
        cajaPrincipal.getChildren().add(cajaDev);

        panelPrincipal.setTop(cajaPrincipal);

        root.getChildren().add(panelPrincipal);

        deshabilitarDragTabla(tablaTicket);
        deshabilitarDragTabla(tablaPoliticas);
        deshabilitarDragTabla(tablaSeleccionadosTicket);

        eventoSeleccionCasillas(grupoCasillaCancelacion, circuloCasillaCancelacion);
        eventosActualizacionCasillas();
    }

    public void limpiarComponentes() {

        SION.log(Modulo.VENTA, "Limpiando componentes de la vista devoluciones", Level.FINEST);

        campoImporteDevTicket.setText("0.00");
        campoImporteDevolver.setText("0.00");
        campoProducto.setText("");
        campoProductosDevTicket.setText("0");
        campoProsuctosDevolucion.setText("0");
        campoTicket.setText("");
    }

    public void aplicarEventos() {

        SION.log(Modulo.VENTA, "Invocando métodos de capa de binding de devoluciones", Level.FINEST);


        binding.insertaDatosDevolucionPoliticas(tablaPoliticas);

        binding.insertaDatosDevolucionTicket(tablaTicket);

        binding.insertaDatosDevolucionSeleccionados(tablaSeleccionadosTicket);


        //politicas

        binding.asignaPropiedadBean(columnaCantidadPoliticas, String.class, "cantidad");
        binding.asignaPropiedadBean(columnaImportePoliticas, Double.class, "importe");
        binding.asignaPropiedadBean(columnaPrecioPoliticas, Double.class, "paprecio");
        binding.asignaPropiedadBean(columnaProductoPoliticas, Double.class, "panombrearticulo");

        binding.eventosTablaPoliticas(tablaPoliticas, campoProsuctosDevolucion, campoImporteDevolver);

        binding.actualizarComponentesPoliticas(tablaPoliticas, campoProsuctosDevolucion, campoImporteDevolver);

        binding.revisarValoresNegativos(tablaPoliticas, columnaImportePoliticas);
        binding.llenarColumnaConFormato(columnaCantidadPoliticas, Pos.CENTER_RIGHT);
        binding.actualizarColumnas(columnaPrecioPoliticas, Double.class, Pos.CENTER_RIGHT, true);
        binding.actualizarColumnas(columnaProductoPoliticas, String.class, Pos.CENTER_LEFT, false);


        //tickets
        binding.asignaPropiedadBean(columnaCantidadTicketSeleccionado, String.class, "cantidad");
        binding.asignaPropiedadBean(columnaImporteTicketSeleccionado, Double.class, "importe");
        binding.asignaPropiedadBean(columnaPrecioTicketSeleccionado, Double.class, "paprecio");
        binding.asignaPropiedadBean(columnaProductoTicketSeleccionado, Double.class, "panombrearticulo");

        binding.llenarColumnaConFormato(columnaCantidadTicketSeleccionado, Pos.BASELINE_RIGHT);
        binding.actualizarColumnas(columnaImporteTicketSeleccionado, Double.class, Pos.CENTER_RIGHT, true);
        binding.actualizarColumnas(columnaPrecioTicketSeleccionado, Double.class, Pos.CENTER_RIGHT, true);
        binding.actualizarColumnas(columnaProductoTicketSeleccionado, String.class, Pos.CENTER_LEFT, false);

        binding.asignaPropiedadBean(columnaCantidadTicket, String.class, "cantidad");
        binding.asignaPropiedadBean(columnaImporteTicket, Double.class, "importe");
        binding.asignaPropiedadBean(columnaPrecioTicket, Double.class, "paprecio");
        binding.asignaPropiedadBean(columnaProductoTicket, Double.class, "panombrearticulo");

        binding.llenarColumnaConFormato(columnaCantidadTicket, Pos.BASELINE_RIGHT);
        binding.actualizarColumnas(columnaImporteTicket, Double.class, Pos.CENTER_RIGHT, true);
        binding.actualizarColumnas(columnaPrecioTicket, Double.class, Pos.CENTER_RIGHT, true);
        binding.actualizarColumnas(columnaProductoTicket, String.class, Pos.CENTER_LEFT, false);

        binding.actualizarComponentesTicket(tablaSeleccionadosTicket, campoProductosDevTicket, campoImporteDevTicket, tablaTicket);

        aplicarEfectoHover(vistaImagenAceptarPoliticas, imagenAceptarPoliticas, imagenAceptarPoliticasHover);
        aplicarEfectoHover(vistaImagenCancelarPoliticas, imagenCancelarPoliticas, imagenCancelarPoliticasHover);

    }

    public void cargarLoading() {

        SION.log(Modulo.VENTA, "Cargando loading al panel", Level.FINEST);

        imagenLoading.setFitHeight(100);
        imagenLoading.setFitWidth(100);
        imagenLoading.setCache(true);
        imagenLoading.setStyle("-fx-background-color:transparent;");
        imagenLoading.setLayoutX((s.getBounds().getMaxX() / 2) - imagenLoading.getFitHeight());
        imagenLoading.setLayoutY((s.getBounds().getMaxY() / 2) - imagenLoading.getFitHeight());

        panelPrincipal.getChildren().add(panelPrincipal.getChildren().size(), imagenLoading);
        panelPrincipal.setDisable(true);

    }

    public void removerLoading() {

        SION.log(Modulo.VENTA, "Removiendo loading del panel", Level.FINEST);

        panelPrincipal.getChildren().remove(panelPrincipal.getChildren().size() - 1);
        panelPrincipal.setDisable(false);

    }

    public void setFocusCampoTicket() {
        campoTicket.requestFocus();
    }

    public void setFocusTablaArticulosDevolucionPorTicket() {
        tablaTicket.requestFocus();
    }

    public void setFocusTablaArticulosDevolucionPorPoliticas() {
        tablaPoliticas.requestFocus();
    }

    public void ajustarScrollTablaPoliticas(int posicion) {
        tablaPoliticas.getSelectionModel().clearSelection();
        tablaPoliticas.getSelectionModel().selectLast();
        tablaPoliticas.scrollTo(posicion);
        //tablaPoliticas.scrollTo(devolucionesVistaModelo.getTamanioListaArticulosPoliticas());
        tablaPoliticas.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    public void setCantidadArticulosDevolucionPorTicket(String cantidad) {
        campoProductosDevTicket.setText(cantidad);
    }

    public void setImporteArticulosDevolucionPorTicket(String importe) {
        campoImporteDevTicket.setText(importe);
    }

    public void setCantidadArticulosDevolucionPorPoliticas(String cantidad) {
        campoProsuctosDevolucion.setText(cantidad);
    }

    public void setImporteArticulosDevolucionPorPoliticas(String importe) {
        campoImporteDevolver.setText(importe);
    }

    public void setAutorizadorConsumido(boolean estado) {
        esAutorizadorConsumido = estado;
    }

    public boolean esDevolucionPorTicket() {
        return esDevolucionTicket;
    }

    public int getPantalla() {
        return idPantalla;
    }

    public TableView getTablaDevolucionPorPoliticas() {
        return tablaPoliticas;
    }

    public TableView getTablaDevolucionPorTicket() {
        return tablaTicket;
    }

    public TextField getCampoTicket() {
        return campoTicket;
    }

    public void setCadenaCampoTicket(String cadena) {
        campoTicket.setText(cadena);
    }

    public String getCadenaArticulo() {
        return cadena;
    }

    public void setCadenaArticulo(String _cadena) {
        cadena = _cadena;
    }

    public void agregarEventos(BorderPane panel) {
        panel.addEventHandler(KeyEvent.KEY_RELEASED, eventosPanel);

    }

    public void removerEventos(BorderPane panel) {
        panel.removeEventHandler(KeyEvent.KEY_RELEASED, eventosPanel);

    }

    public void reestablecerpantalla() {
        idPantalla = PANTALLA_POLITICAS;
    }

    private void navegacionTeclas() {

        //////////////eventos del panel

        eventosPanel = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {

                if (ESCAPE.match(arg0)) {

                    if (!esAutorizadorConsumido) {
                        binding.limpiarDevolucion();
                        campoTicket.setText("");
                    } else {
                        esAutorizadorConsumido = false;
                    }
                } else if (F2.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            campoProducto.requestFocus();
                        }
                    });

                } else if (F8.match(arg0)) {
                    if (idPantalla == PANTALLA_POLITICAS) {
                        idPantalla = PANTALLA_TICKET;
                        radioBdevolucionPolitica.selectedProperty().set(false);

                        binding.limpiarDevolucion();
                        cambiarCajas(PANTALLA_TICKET);
                        campoTicket.requestFocus();
                    } else {
                        idPantalla = PANTALLA_POLITICAS;
                        radioBdevolucionPolitica.selectedProperty().set(true);

                        binding.limpiarDevolucion();
                        cambiarCajas(PANTALLA_POLITICAS);
                        tablaPoliticas.requestFocus();
                    }
                } else if (F10.match(arg0)) {

                    if (idPantalla == PANTALLA_POLITICAS) {

                        if (binding.getTamanioListaArticulosPoliticas() > 0) {
                            binding.ejecutarAutorizador();
                        }
                    } else {

                        if (binding.getImporteArticulosDevolucionPorTicket() > 0) {
                            esDevolucionTicket = true;
                            binding.ejecutarAutorizador();
                        }
                    }

                }

            }
        };

        //////////////eventos del boron radio

        EventHandler<KeyEvent> eventoRadioBoton = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (DERECHA.match(arg0) || TAB.match(arg0) || SHIFTTAB.match(arg0)) {//////////////////////////////////revisar
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            radioBdevolucionPolitica.selectedProperty().set(false);
                            campoProsuctosDevolucion.setText("");
                            campoImporteDevolver.setText("");
                            campoTicket.requestFocus();
                        }
                    });

                } else if (ABAJO.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            campoProducto.requestFocus();
                        }
                    });
                } else if (ARRIBA.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            radioBdevolucionPolitica.requestFocus();
                        }
                    });
                }
            }
        };

        radioBdevolucionPolitica.addEventFilter(KeyEvent.KEY_PRESSED, eventoRadioBoton);

        radioBdevolucionPolitica.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                if (idPantalla == PANTALLA_TICKET) {

                    idPantalla = PANTALLA_POLITICAS;
                    radioBdevolucionPolitica.requestFocus();
                    radioBdevolucionPolitica.selectedProperty().set(true);
                    binding.limpiarDevolucion();
                    campoTicket.setText("");
                    cambiarCajas(PANTALLA_POLITICAS);
                    tablaPoliticas.requestFocus();
                }
            }
        });

        ////////////eventos de la tabla politicas

        tablaPoliticas.setOnKeyTyped(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent ke) {

                Pattern p = Pattern.compile("^[0-9.*]");
                Matcher m = p.matcher(ke.getCharacter());

                if (m.find()) {
                    cadena += ke.getCharacter();
                }
            }
        });

        EventHandler<KeyEvent> eventoTablaPoliticas = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (SUPR.match(arg0)) {
                    binding.insertarRegistroArticulo(tablaPoliticas.getSelectionModel().getSelectedIndex(), 0);
                    binding.setearMontosDevolucionPoliticas();
                } else if (MAS.match(arg0) || ADD.match(arg0)) {
                    binding.sumarArticuloTablaDevolucionPorPoliticas(tablaPoliticas.getSelectionModel().getSelectedIndex());
                } else if (SUBTRACT.match(arg0) || MENOS.match(arg0)) {
                    binding.insertarRegistroArticulo(tablaPoliticas.getSelectionModel().getSelectedIndex(), 2);
                    binding.setearMontosDevolucionPoliticas();
                } else if (ENTER.match(arg0)) {
                    if (!"".equals(cadena.trim())) {
                        binding.capturarArticulo();
                    }
                } else if (ARRIBA.match(arg0)) {

                    if (tablaPoliticas.getSelectionModel().getSelectedIndex() == 0
                            || tablaPoliticas.getSelectionModel().getSelectedIndex() == -1) {
                        campoProducto.requestFocus();
                    }
                } else if (ABAJO.match(arg0)) {

                    if (tablaPoliticas.getSelectionModel().getSelectedIndex() == binding.getTamanioListaArticulosPoliticas() - 1) {
                        botonAceptar.requestFocus();
                    }
                } else if (TAB.match(arg0) || SHIFTTAB.match(arg0)) {

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            botonAceptar.requestFocus();
                        }
                    });
                }
            }
        };

        tablaPoliticas.addEventFilter(KeyEvent.KEY_PRESSED, eventoTablaPoliticas);

        ////////////eventos del campo de ticket

        EventHandler<KeyEvent> eventoCampoTicket = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (ENTER.match(arg0)) {//consulta de ticket

                    if (campoTicket.getText().contains("I") || campoTicket.getText().contains("O")) {
                        binding.iniciarProcesoDevolucion(campoTicket.getText());
                    } else {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco("El formato de la transacción es inválido. Por favor verifique.", TipoMensaje.ADVERTENCIA, campoTicket);
                            }
                        });
                        
                    }

                } else if (IZQUIERDA.match(arg0)) {
                    if (idPantalla == PANTALLA_TICKET) {
                        idPantalla = PANTALLA_POLITICAS;
                        radioBdevolucionPolitica.selectedProperty().set(true);
                        binding.limpiarDevolucion();
                        campoTicket.setText("");

                        campoProsuctosDevolucion.setText("");
                        campoImporteDevolver.setText("");

                        cambiarCajas(PANTALLA_POLITICAS);
                        tablaPoliticas.requestFocus();
                    }
                } else if (TAB.match(arg0) || SHIFTTAB.match(arg0) || ABAJO.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            tablaTicket.requestFocus();
                        }
                    });
                }
            }
        };

        campoTicket.addEventFilter(KeyEvent.KEY_PRESSED, eventoCampoTicket);

        campoTicket.focusedProperty().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable arg0) {
                if (campoTicket.isFocused()) {

                    if (idPantalla == PANTALLA_POLITICAS) {
                        idPantalla = PANTALLA_TICKET;
                        if (radioBdevolucionPolitica.selectedProperty().get() == true) {
                            radioBdevolucionPolitica.selectedProperty().set(false);
                        }
                        binding.limpiarDevolucion();
                        campoTicket.setText("");
                        cambiarCajas(PANTALLA_TICKET);
                    }
                }
            }
        });

        ///////////eventos del campo buscador

        EventHandler<KeyEvent> eventoCampoBuscador = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (ABAJO.match(arg0) || TAB.match(arg0) || SHIFTTAB.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            tablaPoliticas.requestFocus();

                        }
                    });
                } else if (ARRIBA.match(arg0)) {

                    if (idPantalla == PANTALLA_POLITICAS) {
                        radioBdevolucionPolitica.requestFocus();
                    } else {
                        campoTicket.requestFocus();
                    }
                }
            }
        };

        campoProducto.addEventFilter(KeyEvent.KEY_PRESSED, eventoCampoBuscador);

        ///////////eventos boton aceptar

        EventHandler<KeyEvent> eventoBotonAceptar = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (ARRIBA.match(arg0)) {
                    if (idPantalla == PANTALLA_POLITICAS) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {

                                tablaPoliticas.requestFocus();
                            }
                        });
                    } else {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {

                                tablaSeleccionadosTicket.requestFocus();
                            }
                        });

                    }
                } else if (IZQUIERDA.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            botonCancelar.requestFocus();
                        }
                    });
                } else if (TAB.match(arg0) || DERECHA.match(arg0) || SHIFTTAB.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            botonCancelar.requestFocus();
                        }
                    });
                }
            }
        };

        botonAceptar.addEventFilter(KeyEvent.KEY_PRESSED, eventoBotonAceptar);

        botonAceptar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                if (idPantalla == PANTALLA_POLITICAS) {
                    if (binding.getTamanioListaArticulosPoliticas() > 0) {
                        binding.ejecutarAutorizador();
                    }
                } else {
                    if (binding.getTamanioListaArticulosSeleccionadosTicket() > 0) {
                        esDevolucionTicket = true;
                        binding.ejecutarAutorizador();
                    } else {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco("Por favor agrégue artículos a la lista.",
                                TipoMensaje.ADVERTENCIA, tablaTicket);
                            }
                        });
                        
                    }
                }
            }
        });

        /////////////////eventos boton cancelar

        EventHandler<KeyEvent> eventoBotonCancelar = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (ARRIBA.match(arg0)) {
                    if (idPantalla == PANTALLA_POLITICAS) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {

                                tablaPoliticas.requestFocus();
                            }
                        });
                    } else {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {

                                tablaSeleccionadosTicket.requestFocus();
                            }
                        });
                    }
                } else if (DERECHA.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            botonAceptar.requestFocus();
                        }
                    });
                } else if (TAB.match(arg0) || SHIFTTAB.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            if (idPantalla == PANTALLA_POLITICAS) {
                                radioBdevolucionPolitica.requestFocus();
                            } else {
                                campoTicket.requestFocus();
                            }
                        }
                    });

                }
            }
        };

        botonCancelar.addEventFilter(KeyEvent.KEY_PRESSED, eventoBotonCancelar);

        botonCancelar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {

                campoProducto.setText("");
                campoTicket.setText("");

                campoProsuctosDevolucion.setText("");
                campoImporteDevolver.setText("");
                campoProductosDevTicket.setText("");
                campoImporteDevTicket.setText("");

                binding.limpiarDevolucion();

            }
        });

        ////////////////eventos de tabla de articulos en devolucions x ticket

        tablaTicket.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {
                if (arg0.getClickCount() == 2) {
                    binding.seleccionarArticulo(tablaTicket.getSelectionModel().getSelectedIndex());
                }
            }
        });

        EventHandler<KeyEvent> eventoTablaTicket = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (ENTER.match(arg0)) {

                    binding.seleccionarArticulo(tablaTicket.getSelectionModel().getSelectedIndex());

                } else if (ARRIBA.match(arg0)) {
                    if (tablaTicket.getSelectionModel().getSelectedIndex() <= 0) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                campoTicket.requestFocus();
                            }
                        });
                    }
                } else if (DERECHA.match(arg0) || TAB.match(arg0) || SHIFTTAB.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            tablaSeleccionadosTicket.requestFocus();
                        }
                    });

                } else if (ABAJO.match(arg0)) {
                    if (tablaTicket.getSelectionModel().getSelectedIndex() == binding.getTamanioListaArticulosTicket() - 1) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                botonAceptar.requestFocus();
                            }
                        });
                    }
                }
            }
        };

        tablaTicket.addEventFilter(KeyEvent.KEY_PRESSED, eventoTablaTicket);

        /////////////eventos tabla de articulos seleccionados en devolucions x ticket

        EventHandler<KeyEvent> eventoTablaSeleccionadosTicket = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (IZQUIERDA.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            tablaTicket.requestFocus();
                        }
                    });
                } else if (ARRIBA.match(arg0)) {

                    if (tablaSeleccionadosTicket.getSelectionModel().getSelectedIndex() <= 0) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                campoTicket.requestFocus();
                            }
                        });
                    }
                } else if (ABAJO.match(arg0)) {

                    if (tablaSeleccionadosTicket.getSelectionModel().getSelectedIndex()
                            == binding.getTamanioListaArticulosSeleccionadosTicket() - 1) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {

                                botonAceptar.requestFocus();
                            }
                        });
                    }
                } else if (TAB.match(arg0) || SHIFTTAB.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            botonAceptar.requestFocus();
                        }
                    });
                }
            }
        };

        tablaSeleccionadosTicket.addEventFilter(KeyEvent.KEY_PRESSED, eventoTablaSeleccionadosTicket);

        tablaSeleccionadosTicket.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {

                int index = 0;
                index = tablaSeleccionadosTicket.getSelectionModel().getSelectedIndex();

                index = binding.validarIndexTablaArticulosSeleccionadosDevolucionPorTicket(index);

                if (SUPR.match(arg0)) {
                    binding.suprimirLinea(index);
                } else if (MENOS.match(arg0) || SUBTRACT.match(arg0)) {
                    binding.restarArticulo(index);
                }

            }
        });


    }

    public void cambiarCajas(final int idCambio) {

        if (idCambio == PANTALLA_POLITICAS) {

            SION.log(Modulo.VENTA, "Cambiando a pantalla de devolución por políticas Neto", Level.INFO);

            if (grupoCasillaCancelacion.getChildren().size() > 1) {
                grupoCasillaCancelacion.getChildren().remove(1);
            }

            campoProducto.setDisable(false);

            //remover de ticket
            cajaDev.getChildren().remove(1);

            if (cajaInferiorTicket2_2_1.getChildren().size() > 1) {
                cajaInferiorTicket2_2_1.getChildren().remove(1);
            }
            if (cajaInferiorTicket2_2_4_1_1.getChildren().size() > 1) {
                cajaInferiorTicket2_2_4_1_1.getChildren().remove(0);
            }
            if (cajaInferiorTicket2_2_4_2_1.getChildren().size() > 0) {
                cajaInferiorTicket2_2_4_2_1.getChildren().remove(0);
            }
            if (cajaInferiorTicket3.getChildren().size() > 0) {
                cajaInferiorTicket3.getChildren().remove(0);
                cajaInferiorTicket3.getChildren().remove(0);
            }
            if (cajaInferiorTicket1_2.getChildren().size() > 0) {
                cajaInferiorTicket1_2.getChildren().remove(0);
                cajaInferiorTicket1_2.getChildren().remove(0);
            }

            //agregar a politicas
            cajaDev.getChildren().add(cajaInferiorPoliticas);

            if (cajaSuperior_3.getChildren().size() > 1) {
                cajaSuperior_3.getChildren().remove(1);
            }
            if (cajaInferiorPoliticas1_2.getChildren().size() < 1) {
                cajaInferiorPoliticas1_2.getChildren().add(vistaImagenProductosSeleccionados);
            }
            if (cajaInferiorPoliticas4_1.getChildren().size() < 1) {
                cajaInferiorPoliticas4_1.getChildren().add(vistaImagenProductosDevolucion);
            }
            if (cajaInferiorPoliticas4_1.getChildren().size() < 2) {
                cajaInferiorPoliticas4_1.getChildren().add(vistaImagenImporteDevolver);
            }
            if (cajaSuperior_2.getChildren().size() < 2) {
                cajaSuperior_2.getChildren().add(etiquetaDevPoliticaSeleccionada);
            }
            if (cajaInferiorPoliticas5.getChildren().size() < 2) {
                cajaInferiorPoliticas5.getChildren().add(botonAceptar);
                cajaInferiorPoliticas5.getChildren().add(botonCancelar);
            }
            if (cajaInferiorPoliticas1_3.getChildren().size() < 2) {
                cajaInferiorPoliticas1_3.getChildren().add(vistaImagenLupa);
                cajaInferiorPoliticas1_3.getChildren().add(campoProducto);
            }

            esDevolucionTicket = false;

        } else if (idCambio == PANTALLA_TICKET) {

            SION.log(Modulo.VENTA, "Cambiando a pantalla de devolución por transacción", Level.INFO);

            campoProducto.setDisable(true);

            //remover de politicas
            if (cajaInferiorPoliticas1_2.getChildren().size() > 0) {
                cajaInferiorPoliticas1_2.getChildren().remove(0);
            }
            if (cajaInferiorPoliticas4_1.getChildren().size() > 1) {
                cajaInferiorPoliticas4_1.getChildren().remove(0);
            }
            if (cajaInferiorPoliticas4_1.getChildren().size() > 0) {
                cajaInferiorPoliticas4_1.getChildren().remove(0);
            }

            if (cajaInferiorPoliticas5.getChildren().size() > 0) {
                cajaInferiorPoliticas5.getChildren().remove(0);
                cajaInferiorPoliticas5.getChildren().remove(0);
            }
            if (cajaInferiorPoliticas1_3.getChildren().size() > 0) {
                cajaInferiorPoliticas1_3.getChildren().remove(0);
                cajaInferiorPoliticas1_3.getChildren().remove(0);
            }
            cajaDev.getChildren().remove(1);

            //agregar a ticket
            cajaDev.getChildren().add(cajaInferiorTicket);

            if (cajaInferiorTicket2_2_1.getChildren().size() < 2) {
                cajaInferiorTicket2_2_1.getChildren().add(vistaImagenProductosSeleccionados);
            }
            if (cajaInferiorTicket2_2_4_1_1.getChildren().size() < 1) {
                cajaInferiorTicket2_2_4_1_1.getChildren().add(vistaImagenProductosDevolucion);
            }
            if (cajaInferiorTicket2_2_4_2_1.getChildren().size() < 1) {
                cajaInferiorTicket2_2_4_2_1.getChildren().add(vistaImagenImporteDevolver);
            }

            if (cajaSuperior_2.getChildren().size() > 1) {
                cajaSuperior_2.getChildren().remove(1);
            }
            if (cajaSuperior_3.getChildren().size() < 2) {
                cajaSuperior_3.getChildren().add(etiquetaDevTicketSeleccionado);
            }

            if (cajaInferiorTicket3.getChildren().size() < 2) {
                cajaInferiorTicket3.getChildren().add(botonAceptar);
                cajaInferiorTicket3.getChildren().add(botonCancelar);
            }
            if (cajaInferiorTicket1_2.getChildren().size() < 1) {
                cajaInferiorTicket1_2.getChildren().add(vistaImagenLupa);
                cajaInferiorTicket1_2.getChildren().add(campoProducto);
            }

            esDevolucionTicket = true;

        }

    }

    public void regresarFoco() {
        if (idPantalla == PANTALLA_POLITICAS) {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    tablaPoliticas.requestFocus();
                }
            });
        } else {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    tablaTicket.requestFocus();
                }
            });
        }
    }

    public void revisarCajasAlSalir() {
        if (idPantalla == PANTALLA_TICKET) {
            cambiarCajas(PANTALLA_POLITICAS);
        }
    }

    public void eventoSeleccionCasillas(final Group grupo, final Circle circulo) {

        grupo.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {

                if (grupo.getChildren().size() == 1) {
                    grupo.getChildren().add(circulo);
                } else if (grupo.getChildren().size() == 2) {
                    grupo.getChildren().remove(1);
                }

            }
        });

    }

    public void eventosActualizacionCasillas() {

        grupoCasillaCancelacion.getChildren().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable arg0) {
                if (grupoCasillaCancelacion.getChildren().size() == 1) {
                    binding.setCasillaCancelacionNoSeleccionada();
                } else if (grupoCasillaCancelacion.getChildren().size() > 1) {
                    binding.setCasillaCancelacionSeleccionada();
                }
            }
        });

    }

    private void deshabilitarDragTabla(final TableView tabla) {
        tabla.addEventFilter(javafx.scene.input.MouseEvent.MOUSE_DRAGGED, new EventHandler<javafx.scene.input.MouseEvent>() {

            @Override
            public void handle(javafx.scene.input.MouseEvent arg0) {

                arg0.consume();

            }
        });
    }

    private void aplicarEfectoHover(final ImageView vistaImagen, final Image imagen, final Image imagenHover) {

        vistaImagen.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {

                if (!arg1 && arg2) {
                    vistaImagen.setImage(imagenHover);
                } else if (arg1 && !arg2) {
                    vistaImagen.setImage(imagen);
                }
            }
        });
    }
    
    @Override
    public Parent obtenerEscena(Object... obj) {

        SION.log(Modulo.VENTA, "Entrando a método obtener escena en devoluciones", Level.INFO);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                VistaBarraUsuario.setTituloVentana("Devoluciones");

                try {
                    SION.log(Modulo.VENTA, "Poblando atajos en devoluciones Param: "
                            + SION.obtenerParametro(Modulo.VENTA, "atajoDev.1") + ", "
                            + SION.obtenerParametro(Modulo.VENTA, "atajoDev.2") + ", "
                            + SION.obtenerParametro(Modulo.VENTA, "atajoDev.3"), Level.FINEST);
                    VistaBarraUsuario.poblarAtajos(Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "atajoDev.1")),
                            Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "atajoDev.2")),
                            Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "atajoDev.3")));
                } catch (Exception e) {

                    SION.log(Modulo.VENTA, "Ocurrió un error al invocar método de atajos Param:", Level.INFO);
                }
            }
        });

        root.focusedProperty().removeListener(listener);
        root.focusedProperty().addListener(listener);

        campoImporteDevTicket.setEditable(false);
        campoImporteDevolver.setEditable(false);
        campoProsuctosDevolucion.setEditable(false);
        campoProductosDevTicket.setEditable(false);

        binding.InicializarDevoluciones();

        if (cab == null) {
            cab = new ControlAutocompletarBusqueda(campoProducto, b1, campoProducto.textProperty(),
                    stringProperty, skinAutobusqueda.GRANDE, false, TipoBuscador.ARTICULOS, 0);
        }
        
        /**SION.log(Modulo.VENTA, "Consultando parámetro activación de casilla de cancelación", Level.INFO);
        binding.consultarParametroOperacion(
                VistaBarraUsuario.getSesion().getPaisId(), 
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacion.sistema")), 
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacion.modulo")), 
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacion.submodulo")), 
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "cancelacion.configuracion")));
        if(binding.getCodigoErrorParametro()==0){
            SION.log(Modulo.VENTA, "Parámetro activado, mostrando casilla de cancelación", Level.INFO);
            cajaInferiorTicket1.getChildren().clear();
            cajaInferiorTicket1.getChildren().addAll(cajaInferiorTicket1_1, cajaInferiorTicket1_2);
        }else{
            SION.log(Modulo.VENTA, "Parámetro desactivado: "+binding.getDescErrorParametro(), Level.INFO);
            cajaInferiorTicket1.getChildren().clear();
            cajaInferiorTicket1.getChildren().addAll(cajaInferiorTicket1_2);
        }*/

        agregarEventos(panelPrincipal);

        cab.monitorCambioSeleccion.removeListener(changelistener);
        cab.monitorCambioSeleccion.addListener(changelistener);

        estaSucia = true;

        return root;
    }
    
    public void removerCasillaSeleccionada(){
        if (grupoCasillaCancelacion.getChildren().size() > 1) {
            grupoCasillaCancelacion.getChildren().remove(1);
        }
    }

    @Override
    public void limpiar() {

        SION.log(Modulo.VENTA, "Entrando a método limpiar en devoluciones", Level.INFO);

        binding.limpiarDevolucion();

        if (estaSucia) {
            revisarCajasAlSalir();
            limpiarComponentes();
            reestablecerpantalla();
        }

        removerCasillaSeleccionada();

        SION.log(Modulo.VENTA, "Saliendo de método limpiar", Level.FINEST);

    }

    @Override
    public String[] getUserAgentStylesheet() {
        return new String[]{"/neto/sion/tienda/venta/imagenes/inicio/EstilosDevoluciones.css",
                    "/neto/sion/tienda/venta/imagenes/inicio/estilo2.css"};
    }
}