/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author 702382
 */
public class ParametrosBean {
    
    SimpleIntegerProperty errorId;
    SimpleStringProperty descripcionError;
    SimpleDoubleProperty valorConfiguracion;

    public ParametrosBean(int errorId, String descripcionError, int valorConfiguracion) {
        this.errorId = new SimpleIntegerProperty(errorId);
        this.descripcionError = new SimpleStringProperty(descripcionError);
        this.valorConfiguracion = new SimpleDoubleProperty(valorConfiguracion);
    }

    public String getDescripcionError() {
        return descripcionError.get();
    }

    public void setDescripcionError(String adescripcionError) {
        descripcionError.set(adescripcionError);
    }
    
    public SimpleStringProperty descripcionErrorProperty(){
        return descripcionError;
    }

    public Integer getErrorId() {
        return errorId.get();
    }

    public void setErrorId(Integer aerrorId) {
        errorId.set(aerrorId);
    }

    public SimpleIntegerProperty errorIdProperty(){
        return errorId;
    }
    
    public double getValorConfiguracion() {
        return valorConfiguracion.get();
    }

    public void setValorConfiguracion(double avalorConfiguracion) {
        valorConfiguracion.set(avalorConfiguracion);
    }
    
    public SimpleDoubleProperty valorConfiguracionProperty(){
        return valorConfiguracion;
    }

    @Override
    public String toString() {
        return "ParametrosBean{" + "errorId=" + errorId + ", descripcionError=" + descripcionError + ", valorConfiguracion=" + valorConfiguracion + '}';
    }
    
    
    
}
