/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.modelo;

/**
 *
 * @author fvega
 */
public class PeticionArticulosVendidosBean {
    
    private ArticuloVendidoBean[] articuloVendidoBeans;

    public ArticuloVendidoBean[] getArticuloVendidoBeans() {
        return articuloVendidoBeans;
    }

    public void setArticuloVendidoBeans(ArticuloVendidoBean[] articuloVendidoBeans) {
        this.articuloVendidoBeans = articuloVendidoBeans;
    }

    @Override
    public String toString() {
        String cadena = "PeticionArticulosVendidosBean{ articuloVendidoBeans= ";
        
        for(ArticuloVendidoBean bean : articuloVendidoBeans){
            cadena+=(bean.toString());
        }
        
        cadena+=("}");
        return cadena;
    }
    
}
