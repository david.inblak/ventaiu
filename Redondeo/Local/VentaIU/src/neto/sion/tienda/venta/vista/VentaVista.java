/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package neto.sion.tienda.venta.vista;

import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import com.sion.tienda.genericos.ventanas.Ventana;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.util.Callback;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.controles.vista.Utilerias;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.modelo.TipoBuscador;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.genericos.vista.ControlAutocompletarBusqueda;
import neto.sion.tienda.genericos.vista.skinAutobusqueda;
import neto.sion.tienda.venta.bindings.VentaBindings;
import neto.sion.tienda.venta.modelo.ArticuloBean;
import neto.sion.tienda.venta.promociones.redondeo.RepositorioRedondeo;
/**
 *
 * @author fvega
 */
public class VentaVista
 implements Ventana {
    
    private final String version = "1.5";

    //instancias de clases
    private VentaBindings binding;
    private Utilerias utilerias;
    private ChangeListener listener;
    
    ////
    private int PANTALLA_CAPTURA;
    private int PANTALLA_VENTA;
    
    //cajas principales
    private Group root;
    private BorderPane panelPrincipal;
    
    //panelPrincipal
    private HBox cajaCentral;
    
    //cajaCentral
    private VBox cajaIzquierdaVentas;
    private VBox cajaDerechaVentas;
    public VBox cajaIzquierdaPagos;
    //public VBox contDerecha;
    
    //cajaIzquierdaVentas
    private HBox cajaCabeceroTabla;
    private TableView tabla = new TableView();
    public TextField campoBuscador;
    private TableColumn columnaRelleno;
    private TableColumn importeCol;
    private TableColumn cantidadCol;
    private TableColumn productoCol;
    private TableColumn descuentoCol;
    private TableColumn precioCol;
    private Button b1;
    private Button b2;
    private StringProperty stringProperty;
    public static ControlAutocompletarBusqueda cab;
    
    //cajaCabeceroTabla
    private HBox cajaCabeceroTabla_1;
    private HBox cajaCabeceroTabla_2;
    private HBox cajaCabeceroTabla_3;
    private HBox cajaCabeceroTabla_4;
    private HBox cajaCabeceroTabla_5;
    private Label etiquetaCabeceroTabla_1;
    private Label etiquetaCabeceroTabla_2;
    private Label etiquetaCabeceroTabla_3;
    private Label etiquetaCabeceroTabla_4;
    private Label etiquetaCabeceroTabla_5;
    
    //cajaIzquierdaPagos
    public VBox cajaSuperiorPagos;
    public HBox cajaInferiorPagos;
    
    //cajaSuperiorPagos
    public HBox cajaCabeceroFormasPago;
    public HBox cajaDetalleFormasPago;
    
    //cajaDetalleFormasPago
    public VBox cajaCentralIzquierdaPagos;
    public VBox cajaCentralDerechaPagos;
    
    
    //cajaDetalleFormasPago
    //efectivo
    private Image imagenMontoEfectivo;
    private ImageView vistaImagenMontoEfectivo;
    //vales
    private Image imagenMontoVales;
    private ImageView vistaImagenMontoVales;
    //valesE
    private Image imagenMontoValesE;
    private ImageView vistaImagenMontoValesE;
    //tarjetas
    private Image imagenMontoTarjeta;
    private ImageView vistaImagenMontoTarjeta;
    
    //textfields formas de pago
    public TextField campoMontoEfectivo;
    public TextField campoCantidadVales;
    public TextField campoMontoVales;
    public TextField campoMontoValesE;
    public TextField campoMontoTarjeta;
    
    private VBox cajaTarjetas;
    private HBox cajaTarjetasInterior1;
    private HBox cajaTarjetasInterior2;
    private VBox cajaTarjetasInterior2_1;
    private VBox cajaTarjetasInterior2_2;
    private VBox cajaTarjetasInterior2_3;
    private VBox cajaTarjetasInterior2_4;
    private VBox cajaTarjetasInterior2_5;
    private VBox cajaTarjetasInterior2_5_1;
    private VBox cajaTarjetasInterior2_5_2;
    private VBox cajaTarjetasInterior2_6;
    private VBox cajaTarjetasInterior2_6_1;
    private VBox cajaTarjetasInterior2_6_2;
    private HBox hBoxVales;
    //componentes caja derecha
    private Image imagenVentaTA;
    private Image imagenVentaTAHover;
    private ImageView vistaImagenVentaTA;
    private Label etiquetaVentaTA;
    private Group grupoVentaTA;
    //public Label etiquetaVentaTA;
    private Label etiquetaMontoCapturado;
    private Label etiquetaMontoVenta;
    private Label etiquetaMontoDescuento;
    private Label etiquetaMontoRestanteTexto;
    //Label etiquetaMontoRestanteImagen;
    private TextField campoMontoRestante;
    private Label etiquetaImporteRecibido;
    private TextField campoImporteRecibido;
    private Label etiquetaCambio;
    private TextField campoCambio;
    private Image imagenPagoEfectivo;
    private Image imagenPagoEfectivoHover;
    private ImageView vistaImagenPagoEfectivo;
    private Label etiquetaPagoEfectivo;
    private Group grupoPagoEfectivo;
    private Image imagenOtrasFormasPago;
    private Image imagenOtrasFormasPagoHover;
    private ImageView vistaImagenOtrasFormasPago;
    private Label etiquetaOtrasFormasPago;
    private Group grupoOtrasFormasPago;
    private Image imagenCancelar;
    private Image imagenCancelarHover;
    private ImageView vistaImagenCancelar;
    private Label etiquetaCancelarCompra;
    private Group grupoCancelar;
    private Group grupoImagenesMontoVenta;
    //Group grupoImagenesMontoTarjeta;
    private Image imagenPago;
    private Image imagenTarjetaDescto;
    private Image imagenPagoHover;
    private Image imagenTarjetaDesctoHover;
    private ImageView vistaImagenPago;
    private ImageView vistaImagenTarjetaDescto;
    private Label etiquetaPago;
    private Label etiquetaTarjetaDescto;
    private Group grupoPago;
    private Group grupoTarjetaDescto;
    
    
    
    
    //tarjetas
    private Label etiquetaInfoTarjetas;
    private Label etiquetaTarjetasRecibidas;
    private Label etiquetaValesERecibidos;
    private Label etiquetaCantidadTarjetasRecibidas;
    private Label etiquetaCantidadValesERecibidos;
    private Label etiquetaMontoCantidadTarjetasRecibidas;
    private Label etiquetaMontoCantidadValesERecibidos;

    private Label etiquetaImagenAutorizada;
    private Label etiquetaTarjetaBancariaDesc;
    private Label etiquetaNoTarjetaDesc;
    private Label etiquetaImporteTarjetaDesc;
    private Label etiquetaAutorizadaDesc;
    private Label etiquetaTarjetaBancaria;
    private Label etiquetaNoTarjeta;
    private Label etiquetaImporteTarjeta;
    private Image imagenAutorizada;
    private ImageView vistaImagenAutorizada;
    private Image imagenBotonTarjetaSig;
    private ImageView vistaImagenBotonTarjetaSig;
    private Image imagenBotonTarjetaSig2;
    private ImageView vistaImagenBotonValeESig;
    private Image imagenBotonTarjetaAnt;
    private ImageView vistaImagenBotonTarjetaAnt;
    private Label etiquetaRegresar;
    //constantes
    //public ListView<String> view;
    private double ancho;
    private double alto;
    private boolean estaSucia;
    private ChangeListener changelistener;
    //loading
    private ImageView imagenLoading;
    private Screen screen;// = (Screen) Screen.getPrimary();
    //////keys
    private final KeyCombination mas = new KeyCodeCombination(KeyCode.ADD);
    private final KeyCombination arriba = new KeyCodeCombination(KeyCode.UP);
    private final KeyCombination abajo = new KeyCodeCombination(KeyCode.DOWN);
    private final KeyCombination izquierda = new KeyCodeCombination(KeyCode.LEFT);
    private final KeyCombination derecha = new KeyCodeCombination(KeyCode.RIGHT);
    private final KeyCombination tab = new KeyCodeCombination(KeyCode.TAB);
    private final KeyCombination shifTab = new KeyCodeCombination(KeyCode.TAB, KeyCodeCombination.SHIFT_ANY);
    private final KeyCombination f2 = new KeyCodeCombination(KeyCode.F2);
    private final KeyCombination f12 = new KeyCodeCombination(KeyCode.F12);
    private final KeyCombination escape = new KeyCodeCombination(KeyCode.ESCAPE);
    public static boolean existeParametroIndices;
    private EventHandler<KeyEvent> eventoF12Venta;
    //envio de alarma
    private EventHandler<KeyEvent> eventoAlarma1;
    private EventHandler<KeyEvent> eventoAlarma2;
    private EventHandler<KeyEvent> eventoAlarma3;
    private final KeyCombination alarma1 = new KeyCodeCombination(KeyCode.ENTER, KeyCodeCombination.CONTROL_DOWN);
    private final KeyCombination alarma2 = new KeyCodeCombination(KeyCode.ENTER, KeyCodeCombination.SHIFT_DOWN);
    private final KeyCombination alarma3 = new KeyCodeCombination(KeyCode.ENTER, KeyCodeCombination.META_DOWN);

    public VentaVista() 
	{
		SION.log(Modulo.VENTA, "VentaIU.jar Version: 1.1.0 - cifrado iso + soporte para redondeo y tipo de descuento.", Level.INFO);
                SION.log(Modulo.VENTA, "Versión de JavaFX: " + System.getProperty("javafx.runtime.version") , Level.INFO);
                
        ///SION.log(Modulo.VENTA, "VentaIU.jar Version:"+version+" - Validación de versión en componente, actualización de drivers para pinpad.", Level.INFO);
	    
        try{
        
        //instancias
        this.utilerias = new Utilerias();
        this.binding = new VentaBindings(this, utilerias);
        
        //constantes
        this.PANTALLA_CAPTURA = 0;
        this.PANTALLA_VENTA = 1;
        this.ancho = (utilerias.getAnchoMonitor());
        this.alto = (utilerias.getAlturaMonitor());
        this.root = new Group();
        
        this.cajaCentral = utilerias.getHBox(5, (ancho), (alto * .7));
        
        //cajaCentral
        this.cajaIzquierdaVentas = utilerias.getVBox(10, (ancho * 0.7), (alto * .7));
        this.cajaDerechaVentas = utilerias.getVBox(0, (ancho * 0.3), (alto * .7));
        this.cajaIzquierdaPagos = utilerias.getVBox(5, ancho * .7, alto * .7);
        //this.contDerecha = new VBox(0);
        
        //cajaIzquierdaVentas
        this.campoBuscador = utilerias.getTextField("", (ancho * .68), (alto * .05));
        this.cajaCabeceroTabla = utilerias.getHBox(0, ancho * .68, alto * .025);
        this.cajaCabeceroTabla_1 = utilerias.getHBox(0, ancho * .08, alto * .025);
        this.cajaCabeceroTabla_2 = utilerias.getHBox(0, ancho * .38, alto * .025);
        this.cajaCabeceroTabla_3 = utilerias.getHBox(0, ancho * .075, alto * .025);
        this.cajaCabeceroTabla_4 = utilerias.getHBox(0, ancho * .075, alto * .025);
        this.cajaCabeceroTabla_5 = utilerias.getHBox(0, ancho * .075, alto * .025);
        this.etiquetaCabeceroTabla_1 = utilerias.getLabel("", ancho * .08, alto * .025);
        this.etiquetaCabeceroTabla_2 = utilerias.getLabel("", ancho * .38, alto * .025);
        this.etiquetaCabeceroTabla_3 = utilerias.getLabel("", ancho * .075, alto * .025);
        this.etiquetaCabeceroTabla_4 = utilerias.getLabel("", ancho * .075, alto * .025);
        this.etiquetaCabeceroTabla_5 = utilerias.getLabel("", ancho * .075, alto * .025);
        this.tabla = new TableView();
        
        //cajaIzquierdaPagos
        this.cajaSuperiorPagos = utilerias.getVBox(0, ancho * .65, alto * .45);
        this.cajaInferiorPagos = utilerias.getHBox(0, ancho * .65, alto * .25);
        
        //cajaSuperiorPagos
        this.cajaCabeceroFormasPago = utilerias.getHBox(0, ancho*.65, alto*.05);
        this.cajaDetalleFormasPago = utilerias.getHBox(0, ancho*.65, alto*.4);
        
        //cajaDetalleFormasPago
        this.cajaCentralIzquierdaPagos = utilerias.getVBox(25, ancho * .27, alto * .45);
        this.cajaCentralDerechaPagos = utilerias.getVBox(25, ancho * .27, alto * .45);
        
        //cajaDerechaVentas
        
        
        
        //cajas
        this.cajaTarjetas = utilerias.getVBox(2, ancho * .65, alto * .15);
        this.cajaTarjetasInterior1 = utilerias.getHBox(10, ancho * .65, alto * .01);
        this.hBoxVales = utilerias.getHBox(2, ancho * .27, alto / 16);
        //componentes caja derecha

        this.imagenVentaTA = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_TA.jpg");
        this.imagenVentaTAHover = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_TAhover.jpg");
        this.vistaImagenVentaTA = new ImageView(imagenVentaTA);
        this.etiquetaVentaTA = new Label("", vistaImagenVentaTA);

        //this.textoOtrasFormasPago = new Text("Otras Formas de Pago");
        this.imagenOtrasFormasPago = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_otrasformaspago.jpg");
        this.imagenOtrasFormasPagoHover = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_otrasformaspagar_hover.jpg");
        this.vistaImagenOtrasFormasPago = new ImageView(imagenOtrasFormasPago);
        this.etiquetaOtrasFormasPago = new Label("", vistaImagenOtrasFormasPago);

        //this.textoPagoEfectivo = new Text("Pago en Efectivo");
        this.imagenPagoEfectivo = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Venta_pagoefectivo.jpg"));
        this.imagenPagoEfectivoHover = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Venta_pagoefectivo_hover.jpg"));
        this.vistaImagenPagoEfectivo = new ImageView(imagenPagoEfectivo);
        this.etiquetaPagoEfectivo = new Label("", vistaImagenPagoEfectivo);

        //this.textoPago = new Text("Pagar");
        this.imagenPago = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_pagar.jpg");
        this.imagenPagoHover = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_pagar_hover.jpg");
        this.imagenTarjetaDescto = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/TarjetaDescuento.jpg");
        this.imagenTarjetaDesctoHover = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/TarjetaDescuentoHover.jpg");
        
        this.vistaImagenPago = new ImageView(imagenPago);
        this.vistaImagenTarjetaDescto = new ImageView(imagenTarjetaDescto);
        this.etiquetaPago = new Label("", vistaImagenPago);
        this.etiquetaTarjetaDescto = new Label("", vistaImagenTarjetaDescto);

        //this.textoCancelar = new Text("Cancelar Compra");
        this.imagenCancelar = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_cancelar.jpg");
        this.imagenCancelarHover = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_cancelar_hover.jpg");
        this.vistaImagenCancelar = new ImageView(imagenCancelar);
        this.etiquetaCancelarCompra = new Label("", vistaImagenCancelar);
        this.grupoCancelar = new Group();

        this.etiquetaMontoCapturado = utilerias.getLabelConImageView("/neto/sion/tienda/venta/imagenes/inicio/Venta_montoventa.jpg", alto * .15, ancho * .25, Pos.CENTER_LEFT);
        this.etiquetaMontoRestanteTexto = utilerias.getLabel("Importe Restante", ancho * .25, alto * .02);
        this.etiquetaImporteRecibido = utilerias.getLabel("Importe Recibido", ancho * .25, alto * .02);
        this.etiquetaCambio = utilerias.getLabel("Cambio", ancho * .23, alto * .02);

        /////
        this.grupoImagenesMontoVenta = new Group();
        
        this.columnaRelleno = utilerias.getColumna("", ancho * .05);
        this.cantidadCol = utilerias.getColumna("Cantidad", ancho * .045);
        this.productoCol = utilerias.getColumna("Producto", ancho * .40);
        this.precioCol = utilerias.getColumna("Precio", ancho * .05);
        this.descuentoCol = utilerias.getColumna("Descuento", ancho * .05);
        this.importeCol = utilerias.getColumna("Importe", ancho * .07);
        this.b1 = new Button();
        this.b2 = new Button();
        this.stringProperty = new SimpleStringProperty();

        this.imagenMontoEfectivo = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_montoefectivo.jpg");
        this.vistaImagenMontoEfectivo = new ImageView(imagenMontoEfectivo);

        this.imagenMontoVales = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_montovales.jpg");
        this.vistaImagenMontoVales = new ImageView(imagenMontoVales);

        this.imagenMontoValesE = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_montoValesE.jpg");
        this.vistaImagenMontoValesE = new ImageView(imagenMontoValesE);

        this.imagenMontoTarjeta = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_montotarjeta.jpg");
        this.vistaImagenMontoTarjeta = new ImageView(imagenMontoTarjeta);

        this.etiquetaInfoTarjetas = new Label("");
        this.etiquetaTarjetasRecibidas = new Label("Tarjetas Crédito/Débito: ");
        this.etiquetaValesERecibidos = new Label("Vales Electrónicos: ");
        this.imagenBotonTarjetaSig = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/ico_Agregar_1.png");
        this.vistaImagenBotonTarjetaSig = new ImageView(imagenBotonTarjetaSig);
        this.imagenBotonTarjetaSig2 = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/ico_Agregar.png");
        this.vistaImagenBotonValeESig = new ImageView(imagenBotonTarjetaSig2);
        this.imagenBotonTarjetaAnt = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/Venta_anterior.png");
        this.vistaImagenBotonTarjetaAnt = new ImageView(imagenBotonTarjetaAnt);
        this.estaSucia = false;


        this.cajaTarjetasInterior2_5_1 = utilerias.getVBox(5, ancho * .14, alto * .13);
        this.cajaTarjetasInterior2_5_2 = utilerias.getVBox(5, ancho * .14, alto * .1);
        this.cajaTarjetasInterior2_6_1 = utilerias.getVBox(5, ancho * .18, alto * .13);
        this.cajaTarjetasInterior2_6_2 = utilerias.getVBox(5, ancho * .18, alto * .1);

        VentaVista.existeParametroIndices = false;


        /////loading 
        this.screen = (Screen) Screen.getPrimary();
        this.imagenLoading = new ImageView(utilerias.getImagen("/com/sion/tienda/genericos/images/popup/loading.gif"));


        this.listener = new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {

                if (arg2) {
                    tabla.requestFocus();
                }
            }
        };

        inicializarComponentesEstaticos();

        this.changelistener = new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                
                binding.operacionesbuscador(stringProperty, etiquetaMontoVenta, campoBuscador, campoImporteRecibido);
            }
        };


        establecerEstilos();
        ajustarTamanioComponentes();
        llamarMetodosBinding();
        creaEventoF12Venta();
        crearEventosAlarma();
        
        
        }catch(Exception e){
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            SION.log(Modulo.VENTA, "excepcion: " + exceptionAsString, Level.SEVERE);
        }catch(Error e){
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            SION.log(Modulo.VENTA, "error: " + exceptionAsString, Level.SEVERE);
        }
    }

    private void inicializarComponentesEstaticos() {

        SION.log(Modulo.VENTA, "Inicializando componentes estáticos de la vista", Level.INFO);


        //caja derecha
        campoImporteRecibido = utilerias.getTextField("0.00", ancho * .25, alto * .05);
        campoCambio = utilerias.getTextField("0.00", ancho * .25, alto * .05);
        
        //caja derecha
        grupoVentaTA = new Group();
        grupoPagoEfectivo = new Group();
        grupoOtrasFormasPago = new Group();
        grupoPago = new Group();
        grupoTarjetaDescto = new Group();

        etiquetaMontoVenta = utilerias.getLabelConEstilos("EtiquetaMontoVenta", ancho * .24, alto * .1, Pos.CENTER_RIGHT, 0, alto * .07);
        campoMontoRestante = utilerias.getTextField("", ancho * .25, alto * .05);

        //componentes formas de pago
        campoMontoEfectivo = utilerias.getTextField("0", (ancho * .25), (alto / 16));
        campoCantidadVales = utilerias.getTextField("0", (ancho * .05), (alto / 16));
        campoMontoVales = utilerias.getTextField("0", (ancho * .2), (alto / 16));
        campoMontoValesE = utilerias.getTextField("0", (ancho * .25), (alto / 16));
        campoMontoTarjeta = utilerias.getTextField("0", (ancho * .25), (alto / 16));

        campoMontoEfectivo.setId("efectivo");
        campoCantidadVales.setId("cvales");
        campoMontoVales.setId("vales");
        campoMontoValesE.setId("valesE");
        campoMontoTarjeta.setId("tarjetas");
        campoImporteRecibido.setId("importerecibido");
        

        //componentes tarjetas
        cajaTarjetasInterior2 = utilerias.getHBox(5, ancho * .65, alto * .14);

        etiquetaCantidadTarjetasRecibidas = new Label("0");
        etiquetaCantidadValesERecibidos = new Label("0");
        etiquetaMontoCantidadTarjetasRecibidas = new Label("0.00");
        etiquetaMontoCantidadValesERecibidos = new Label("0.00");
        etiquetaRegresar = new Label("Regresar");

        //etiquetaTarjetaBancaria = new Label("Tarjeta Bancaria");
        etiquetaTarjetaBancaria = new Label("");
        etiquetaNoTarjeta = new Label("No. Tarjeta");
        etiquetaImporteTarjeta = new Label("Importe");

        imagenAutorizada = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Venta_TarjetaAutorizada.png"));
        vistaImagenAutorizada = new ImageView(imagenAutorizada);
        etiquetaImagenAutorizada = new Label("", vistaImagenAutorizada);

        etiquetaTarjetaBancariaDesc = new Label("");
        etiquetaNoTarjetaDesc = new Label("");
        etiquetaImporteTarjetaDesc = new Label("");
        etiquetaAutorizadaDesc = new Label("");

        //cajas
        cajaTarjetasInterior2_1 = utilerias.getVBox(15, ancho * .22, alto * .23);
        cajaTarjetasInterior2_2 = utilerias.getVBox(15, ancho * .035, alto * .23);
        cajaTarjetasInterior2_3 = utilerias.getVBox(15, ancho * .05, alto * .23);
        cajaTarjetasInterior2_4 = utilerias.getVBox(15, ancho * .035, alto * .23);

        cajaTarjetasInterior2_5 = utilerias.getVBox(10, ancho * .14, alto * .23);
        cajaTarjetasInterior2_6 = utilerias.getVBox(10, ancho * .18, alto * .23);

        panelPrincipal = new BorderPane();

        root = new Group();

        root.focusedProperty().addListener(listener);
    }

    private void establecerEstilos() {

        SION.log(Modulo.VENTA, "Agregando estilos", Level.INFO);

        panelPrincipal.getStyleClass().add("CajaCenter");
        
        //panelPrincipal
        cajaCentral.getStyleClass().add("CajaCentral");
        
        //cajaCentral
        cajaIzquierdaVentas.getStyleClass().add("TopCenter");
        cajaDerechaVentas.getStyleClass().add("TopCenter");
        cajaIzquierdaPagos.getStyleClass().add("TopCenter");
        //contDerecha.getStyleClass().add("TopCenter");
        
        //cajaIzquierdaVentas
        //tabla.getStyleClass().add("TablaVenta");
        etiquetaCabeceroTabla_1.setStyle("-fx-alignment: center; -fx-font: 18pt Verdana; -fx-text-fill: white; -fx-background-color: #32a3ea;;");
        etiquetaCabeceroTabla_2.setStyle("-fx-alignment: center; -fx-font: 18pt Verdana; -fx-text-fill: white; -fx-background-color: #32a3ea;;");
        etiquetaCabeceroTabla_3.setStyle("-fx-alignment: center; -fx-font: 18pt Verdana; -fx-text-fill: white; -fx-background-color: #32a3ea;;");
        etiquetaCabeceroTabla_4.setStyle("-fx-alignment: center; -fx-font: 18pt Verdana; -fx-text-fill: white; -fx-background-color: #32a3ea;;");
        etiquetaCabeceroTabla_5.setStyle("-fx-alignment: center; -fx-font: 18pt Verdana; -fx-text-fill: white; -fx-background-color: #32a3ea;");
        cajaCabeceroTabla_1.setStyle("-fx-background-color: transparent; -fx-alignment: center;");
        cajaCabeceroTabla_2.setStyle("-fx-background-color: transparent; -fx-alignment: center;");
        cajaCabeceroTabla_3.setStyle("-fx-background-color: transparent; -fx-alignment: center;");
        cajaCabeceroTabla_4.setStyle("-fx-background-color: transparent; -fx-alignment: center;");
        cajaCabeceroTabla_5.setStyle("-fx-background-color: transparent; -fx-alignment: center;");
        
        //cajaIzquierdaPagos
        cajaSuperiorPagos.getStyleClass().add("Center");
        cajaInferiorPagos.getStyleClass().add("Center");
        cajaCabeceroFormasPago.getStyleClass().add("CajaCabeceroformasPago");
        cajaDetalleFormasPago.getStyleClass().add("CajaDetalleformasPago");
        cajaCentralIzquierdaPagos.getStyleClass().add("CajaDetalleformasPagoIzq");
        cajaCentralDerechaPagos.getStyleClass().add("CajaDetalleformasPagoDer");
        
        if (utilerias.getAnchoMonitor() >= 1440) {

            //caja izquierda

            if (campoBuscador.getStyleClass().size() > 0) {

                for (int i = 0; i < campoBuscador.getStyleClass().size(); i++) {
                    campoBuscador.getStyleClass().remove(0);
                }
            }

            campoBuscador.getStyleClass().add("TextField");

            //caja derecha
            campoImporteRecibido.getStyleClass().add("TextField");
            campoCambio.getStyleClass().add("TextField");

            //formas de pago
            campoMontoEfectivo.getStyleClass().add("TextFieldFormasPago");
            campoCantidadVales.getStyleClass().add("TextFieldFormasPago");
            campoMontoVales.getStyleClass().add("TextFieldFormasPago");
            campoMontoValesE.getStyleClass().add("TextFieldFormasPago");
            campoMontoTarjeta.getStyleClass().add("TextFieldFormasPago");

            //tarjetas
            etiquetaTarjetasRecibidas.setAlignment(Pos.CENTER_RIGHT);
            etiquetaValesERecibidos.setAlignment(Pos.CENTER_RIGHT);

            etiquetaInfoTarjetas.getStyleClass().add("EtiquetaTarjetas");
            etiquetaTarjetasRecibidas.getStyleClass().add("EtiquetaTarjetas");
            etiquetaValesERecibidos.getStyleClass().add("EtiquetaTarjetas");

            etiquetaCantidadTarjetasRecibidas.getStyleClass().add("EtiquetaCantidadTarjetas");

            etiquetaCantidadValesERecibidos.getStyleClass().add("EtiquetaCantidadValesE");

            etiquetaMontoCantidadTarjetasRecibidas.getStyleClass().add("EtiquetaMontoTarjetas");
            etiquetaMontoCantidadValesERecibidos.getStyleClass().add("EtiquetaMontoTarjetas");


        } else {

            //caja izquierda
            campoBuscador.getStyleClass().add("TextField23");

            //caja derecha
            campoImporteRecibido.getStyleClass().add("TextField23");
            campoCambio.getStyleClass().add("TextField23");

            //formas de pago
            campoMontoEfectivo.getStyleClass().add("TextField23");
            campoCantidadVales.getStyleClass().add("TextField23");
            campoMontoVales.getStyleClass().add("TextField23");
            campoMontoValesE.getStyleClass().add("TextField23");
            campoMontoTarjeta.getStyleClass().add("TextField23");

            //tarjetas
            etiquetaTarjetasRecibidas.setAlignment(Pos.CENTER_RIGHT);
            etiquetaValesERecibidos.setAlignment(Pos.CENTER_RIGHT);

            etiquetaInfoTarjetas.getStyleClass().add("EtiquetaTarjetas2");
            etiquetaTarjetasRecibidas.getStyleClass().add("EtiquetaTarjetas2");
            etiquetaValesERecibidos.getStyleClass().add("EtiquetaTarjetas2");

            etiquetaCantidadTarjetasRecibidas.getStyleClass().add("EtiquetaCantidadTarjetas2");

            etiquetaCantidadValesERecibidos.getStyleClass().add("EtiquetaCantidadValesE2");

            etiquetaMontoCantidadTarjetasRecibidas.getStyleClass().add("EtiquetaMontoTarjetas2");
            etiquetaMontoCantidadValesERecibidos.getStyleClass().add("EtiquetaMontoTarjetas2");
        }



        etiquetaRegresar.getStyleClass().add("EtiquetaRegresarTarjetas");

        etiquetaTarjetaBancaria.getStyleClass().add("EtiquetaVenta2");
        etiquetaNoTarjeta.getStyleClass().add("EtiquetaVenta2");
        etiquetaImporteTarjeta.getStyleClass().add("EtiquetaVenta2");

        etiquetaTarjetaBancariaDesc.getStyleClass().add("EtiquetaVenta3");
        etiquetaNoTarjetaDesc.getStyleClass().add("EtiquetaVenta3");
        etiquetaImporteTarjetaDesc.getStyleClass().add("EtiquetaVenta3");
        etiquetaAutorizadaDesc.getStyleClass().add("EtiquetaVenta3");

        //cajas
        cajaTarjetas.getStyleClass().add("CajaTarjetas");

        cajaTarjetasInterior2_1.getStyleClass().add("CajaInteriorTarjetasDerecha");
        cajaTarjetasInterior2_2.getStyleClass().add("CajaInteriorTarjetasCentro");
        cajaTarjetasInterior2_3.getStyleClass().add("CajaInteriorTarjetasCentro");
        cajaTarjetasInterior2_4.getStyleClass().add("CajaInteriorTarjetasCentro");
        cajaTarjetasInterior2_5.getStyleClass().add("CajaInteriorTarjetasDerecha");
        cajaTarjetasInterior2_5_1.getStyleClass().add("CajaInteriorTarjetasDerecha");
        cajaTarjetasInterior2_5_2.getStyleClass().add("CajaInteriorTarjetasTopRight");
        cajaTarjetasInterior2_6.getStyleClass().add("CajaInteriorTarjetasIzquierda");
        cajaTarjetasInterior2_6_1.getStyleClass().add("CajaInteriorTarjetasIzquierda");
        cajaTarjetasInterior2_6_2.getStyleClass().add("CajaInteriorTarjetasTopLeft");

        cajaTarjetasInterior1.getStyleClass().add("CajaTarjetasINterior");
        cajaTarjetasInterior2.getStyleClass().add("CajaTarjetasINterior");

        hBoxVales.getStyleClass().add("Center");
    }

    private void limpiarComponentes() {

        SION.log(Modulo.VENTA, "Limpiando componentes de la vista", Level.FINEST);

        binding.RevisarCajasAlSalir(panelPrincipal);

        ancho = 0;
        alto = 0;
        etiquetaMontoVenta.textProperty().setValue("0.00");
        campoBuscador.textProperty().setValue("");
        stringProperty.setValue("");
        etiquetaInfoTarjetas.textProperty().setValue("");

        //estaticos
        campoImporteRecibido.textProperty().setValue("");
        campoCambio.textProperty().setValue("");
        campoMontoEfectivo.textProperty().setValue("");
        campoCantidadVales.textProperty().setValue("");
        campoMontoVales.textProperty().setValue("");
        campoMontoValesE.textProperty().setValue("");
        campoMontoTarjeta.textProperty().setValue("");
        etiquetaCantidadTarjetasRecibidas.textProperty().setValue("0");
        etiquetaCantidadValesERecibidos.textProperty().setValue("0");
        etiquetaMontoCantidadTarjetasRecibidas.textProperty().setValue("0.00");
        etiquetaMontoCantidadValesERecibidos.textProperty().setValue("0.00");
        etiquetaTarjetaBancariaDesc.textProperty().setValue("");
        etiquetaNoTarjetaDesc.textProperty().setValue("");
        etiquetaImporteTarjetaDesc.textProperty().setValue("");
        etiquetaAutorizadaDesc.textProperty().setValue("");

        if (binding.revisarStageTA()) {
            binding.cerrarStageTA();
        }

        if (binding.estaStageTarjetasMostrandose()) {
            binding.cerrarStageTarjetas();
        }

        if (binding.revisarStageVerificador()) {
            binding.cerrarStageVerificador();
        }

    }

    private void ajustarTamanioComponentes() {

        SION.log(Modulo.VENTA, "Ajustando componentes pantalla principal", Level.INFO);
        
        //layout principal
        //cargar el root
        root.getChildren().add(panelPrincipal);
        
        //cargar el panel principal
        panelPrincipal.setTop(cajaCentral);
        
        //VBox contDerecha = new VBox(5);
        //contDerecha.getStyleClass().add("TopCenter");
        
        //contDerecha.getChildren().addAll(grupoTarjetaDescto, cajaDerechaVentas);
        //carga cajaCentral
        cajaCentral.getChildren().addAll(cajaIzquierdaVentas, cajaDerechaVentas);
        //cajaCentral.getChildren().addAll(cajaIzquierdaVentas, contDerecha);
        
        //cajaCentral
        cajaIzquierdaVentas.getChildren().addAll(campoBuscador, cajaCabeceroTabla,tabla);
        cajaIzquierdaVentas.setAlignment(Pos.CENTER);
        cajaDerechaVentas.getChildren().addAll( grupoTarjetaDescto,
                grupoVentaTA, grupoImagenesMontoVenta, etiquetaMontoRestanteTexto, campoMontoRestante, 
                etiquetaImporteRecibido, campoImporteRecibido, etiquetaCambio, campoCambio, 
                grupoPagoEfectivo, grupoOtrasFormasPago, grupoCancelar);
        cajaDerechaVentas.setAlignment(Pos.CENTER_LEFT);
        cajaDerechaVentas.setSpacing(5);
        cajaIzquierdaPagos.getChildren().addAll(cajaSuperiorPagos, cajaInferiorPagos);
        //cajaIzquierdaPagos.setEffect(dropShadow);
        cajaIzquierdaPagos.setAlignment(Pos.CENTER);
        
        //cajaIzquierdaVentas
        campoBuscador.setPromptText("Buscar");
        campoBuscador.setDisable(true);
        tabla.getColumns().clear();
        tabla.getColumns().addAll(cantidadCol, columnaRelleno, productoCol, descuentoCol,precioCol, importeCol);
        tabla.setPrefSize(ancho * .68, alto * .62);
        tabla.setMaxSize(ancho * .68, alto * .62);
        tabla.setMinSize(ancho * .68, alto * .62);
        tabla.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        tabla.setEditable(false);
        
        //cajaIzquierdaPagos
        cajaSuperiorPagos.getChildren().addAll(cajaCabeceroFormasPago,cajaDetalleFormasPago);
        cajaDetalleFormasPago.getChildren().addAll(cajaCentralIzquierdaPagos, cajaCentralDerechaPagos);
        cajaInferiorPagos.getChildren().addAll(cajaTarjetas);
        
        //cajaSuperiorPagos
        cajaCentralIzquierdaPagos.getChildren().addAll(vistaImagenMontoEfectivo, vistaImagenMontoVales, vistaImagenMontoValesE, vistaImagenMontoTarjeta);
        cajaCentralDerechaPagos.getChildren().addAll(campoMontoEfectivo, hBoxVales, campoMontoValesE, campoMontoTarjeta);
        
        //cajaCentralDerechaPagos
        hBoxVales.getChildren().addAll(campoCantidadVales, campoMontoVales);

        //cajaCabeceroTabla
        etiquetaCabeceroTabla_1.textProperty().setValue("Cantidad");
        etiquetaCabeceroTabla_2.textProperty().setValue("Producto");
        etiquetaCabeceroTabla_5.textProperty().setValue("Descuento");
        etiquetaCabeceroTabla_3.textProperty().setValue("Precio");
        etiquetaCabeceroTabla_4.textProperty().setValue("Importe");
        cajaCabeceroTabla_1.getChildren().addAll(etiquetaCabeceroTabla_1);
        cajaCabeceroTabla_2.getChildren().addAll(etiquetaCabeceroTabla_2);
        cajaCabeceroTabla_3.getChildren().addAll(etiquetaCabeceroTabla_3);
        cajaCabeceroTabla_4.getChildren().addAll(etiquetaCabeceroTabla_4);
        cajaCabeceroTabla_5.getChildren().addAll(etiquetaCabeceroTabla_5);
        cajaCabeceroTabla.getChildren().addAll(cajaCabeceroTabla_1, cajaCabeceroTabla_2, cajaCabeceroTabla_5, cajaCabeceroTabla_3, cajaCabeceroTabla_4);
        
        //tabla
        columnaRelleno.resizableProperty().setValue(false);
        precioCol.resizableProperty().setValue(false);
        importeCol.resizableProperty().setValue(false);
        cantidadCol.resizableProperty().setValue(false);
        descuentoCol.resizableProperty().setValue(false);
        productoCol.resizableProperty().setValue(false);
        columnaRelleno.sortableProperty().set(false);
        precioCol.sortableProperty().set(false);
        importeCol.sortableProperty().setValue(false);
        cantidadCol.sortableProperty().setValue(false);
        productoCol.sortableProperty().setValue(false);
        descuentoCol.sortableProperty().setValue(false);
        
        //caja izquierda
        vistaImagenCancelar.setFitHeight(alto * .055);
        vistaImagenCancelar.setFitWidth(ancho * .25);

        vistaImagenVentaTA.setFitHeight(alto * .055);
        vistaImagenVentaTA.setFitWidth(ancho * .25);

        vistaImagenOtrasFormasPago.setFitHeight(alto * .055);
        vistaImagenOtrasFormasPago.setFitWidth(ancho * .25);

        vistaImagenPagoEfectivo.setFitHeight(alto * .1);
        vistaImagenPagoEfectivo.setFitWidth(ancho * .25);
        
        vistaImagenTarjetaDescto.setFitHeight(alto * .055);
        vistaImagenTarjetaDescto.setFitWidth(ancho * .25);

        vistaImagenPago.setFitHeight(alto * .11);
        vistaImagenPago.setFitWidth(ancho * .25);

        etiquetaPago.setMaxSize(ancho * .25, alto * .11);
        etiquetaPago.setMinSize(ancho * .25, alto * .11);
        etiquetaPago.setPrefSize(ancho * .25, alto * .11);
        
        etiquetaTarjetaDescto.setMaxSize(ancho * .25, alto * .055);
        etiquetaTarjetaDescto.setMinSize(ancho * .25, alto * .055);
        etiquetaTarjetaDescto.setPrefSize(ancho * .25, alto * .055);
        
        etiquetaCancelarCompra.setMaxSize(ancho * .25, alto * .055);
        etiquetaCancelarCompra.setMinSize(ancho * .25, alto * .055);
        etiquetaCancelarCompra.setPrefSize(ancho * .25, alto * .055);

        etiquetaOtrasFormasPago.setMaxSize(ancho * .25, alto * .055);
        etiquetaOtrasFormasPago.setMinSize(ancho * .25, alto * .055);
        etiquetaOtrasFormasPago.setPrefSize(ancho * .25, alto * .055);

        etiquetaPagoEfectivo.setMaxSize(ancho * .25, alto * .1);
        etiquetaPagoEfectivo.setMinSize(ancho * .25, alto * .1);
        etiquetaPagoEfectivo.setPrefSize(ancho * .25, alto * .1);

        grupoTarjetaDescto.setBlendMode(BlendMode.MULTIPLY);
        grupoPago.setBlendMode(BlendMode.MULTIPLY);
        grupoVentaTA.setBlendMode(BlendMode.MULTIPLY);
        grupoCancelar.setBlendMode(BlendMode.MULTIPLY);
        grupoOtrasFormasPago.setBlendMode(BlendMode.MULTIPLY);
        grupoPagoEfectivo.setBlendMode(BlendMode.MULTIPLY);

        grupoVentaTA.getChildren().addAll(etiquetaVentaTA);
        grupoTarjetaDescto.getChildren().addAll(etiquetaTarjetaDescto);
        grupoPago.getChildren().addAll(etiquetaPago);
        grupoCancelar.getChildren().addAll(etiquetaCancelarCompra);
        grupoOtrasFormasPago.getChildren().addAll(etiquetaOtrasFormasPago);
        grupoPagoEfectivo.getChildren().addAll(etiquetaPagoEfectivo);


        campoImporteRecibido.setDisable(true);
        campoCambio.setDisable(true);
        if (utilerias.getAnchoMonitor() >= 1440) {
            campoMontoRestante.getStyleClass().add("CampoMontoRestante");
        } else {
            campoMontoRestante.getStyleClass().add("CampoMontoRestante23");
        }

        campoMontoRestante.setEditable(false);

        etiquetaMontoRestanteTexto.setStyle("-fx-font: 18pt Verdana;");
        etiquetaImporteRecibido.setStyle("-fx-font: 18pt Verdana;");
        etiquetaCambio.setStyle("-fx-font: 18pt Verdana;");

        //componentes formas de pago
        vistaImagenMontoEfectivo.setFitHeight(alto / 16);
        vistaImagenMontoEfectivo.setFitWidth(ancho * .25);

        vistaImagenMontoVales.setFitHeight(alto / 16);
        vistaImagenMontoVales.setFitWidth(ancho * .25);

        vistaImagenMontoValesE.setFitHeight(alto / 16);
        vistaImagenMontoValesE.setFitWidth(ancho * .25);

        vistaImagenMontoTarjeta.setFitHeight(alto / 16);
        vistaImagenMontoTarjeta.setFitWidth(ancho * .25);

        //componentes tarjetas


        vistaImagenBotonTarjetaSig.setFitHeight(ancho * .03);
        vistaImagenBotonTarjetaSig.setFitWidth(ancho * .03);

        vistaImagenBotonValeESig.setFitHeight(ancho * .03);
        vistaImagenBotonValeESig.setFitWidth(ancho * .03);

        vistaImagenBotonTarjetaAnt.setFitHeight(ancho * .03);
        vistaImagenBotonTarjetaAnt.setFitWidth(ancho * .03);

        vistaImagenAutorizada.setFitHeight(ancho * .025);
        vistaImagenAutorizada.setFitWidth(ancho * .03);

        etiquetaImagenAutorizada.setGraphic(vistaImagenAutorizada);
        etiquetaImagenAutorizada.setPrefSize(ancho * .03, ancho * .03);
        etiquetaImagenAutorizada.setMaxSize(ancho * .03, ancho * .03);
        etiquetaImagenAutorizada.setMinSize(ancho * .03, ancho * .03);

        ////fin componentes tarjetas

        ////componentes lado derecho
        grupoImagenesMontoVenta.getChildren().addAll(etiquetaMontoCapturado, etiquetaMontoVenta);

        ////cajas

        cajaTarjetasInterior2_1.getChildren().addAll(etiquetaTarjetasRecibidas, etiquetaValesERecibidos);
        cajaTarjetasInterior2_2.getChildren().addAll(etiquetaCantidadTarjetasRecibidas, etiquetaCantidadValesERecibidos);
        cajaTarjetasInterior2_3.getChildren().addAll(etiquetaMontoCantidadTarjetasRecibidas, etiquetaMontoCantidadValesERecibidos);
        cajaTarjetasInterior2_4.getChildren().addAll(vistaImagenBotonTarjetaSig, vistaImagenBotonValeESig);

        cajaTarjetasInterior2_5_1.getChildren().addAll(etiquetaTarjetaBancaria, etiquetaNoTarjeta, etiquetaImporteTarjeta);
        cajaTarjetasInterior2_5_2.getChildren().addAll(etiquetaImagenAutorizada);

        cajaTarjetasInterior2_6_1.getChildren().addAll(etiquetaTarjetaBancariaDesc, etiquetaNoTarjetaDesc, etiquetaImporteTarjetaDesc);
        cajaTarjetasInterior2_6_2.getChildren().addAll(etiquetaAutorizadaDesc);

        cajaTarjetasInterior2_5.getChildren().addAll(cajaTarjetasInterior2_5_1, cajaTarjetasInterior2_5_2);
        cajaTarjetasInterior2_6.getChildren().addAll(cajaTarjetasInterior2_6_1, cajaTarjetasInterior2_6_2);

        cajaTarjetasInterior1.getChildren().add(etiquetaInfoTarjetas);
        cajaTarjetasInterior2.getChildren().addAll(cajaTarjetasInterior2_1, cajaTarjetasInterior2_2, cajaTarjetasInterior2_3, cajaTarjetasInterior2_4);

        cajaTarjetas.getChildren().addAll(cajaTarjetasInterior1, cajaTarjetasInterior2);

        
        quitaCabeceraTabla(tabla);

        aplicarEfectoBoton(vistaImagenTarjetaDescto, imagenTarjetaDescto, imagenTarjetaDesctoHover, grupoTarjetaDescto);
        aplicarEfectoBoton(vistaImagenPago, imagenPago, imagenPagoHover, grupoPago);
        aplicarEfectoBoton(vistaImagenVentaTA, imagenVentaTA, imagenVentaTAHover, grupoVentaTA);
        aplicarEfectoBoton(vistaImagenCancelar, imagenCancelar, imagenCancelarHover, grupoCancelar);
        aplicarEfectoBoton(vistaImagenOtrasFormasPago, imagenOtrasFormasPago, imagenOtrasFormasPagoHover, grupoOtrasFormasPago);
        aplicarEfectoBoton(vistaImagenPagoEfectivo, imagenPagoEfectivo, imagenPagoEfectivoHover, grupoPagoEfectivo);

        deshabilitarBuscador(campoBuscador, tabla);

        deshabailitarDragTabla();

        movimientosTeclas(campoMontoEfectivo, campoCantidadVales, campoMontoVales, campoMontoValesE, campoMontoTarjeta);

        actualizarImporteTeclaMas(campoImporteRecibido);
        actualizarImporteTeclaMas(campoMontoEfectivo);
        actualizarImporteTeclaMas(campoMontoValesE);
        actualizarImporteTeclaMas(campoMontoTarjeta);

        actualizarMontosCamposTarjetas();
    }

    public void quitaCabeceraTabla(final TableView tabla) {
        tabla.widthProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
                Pane header = (Pane) tabla.lookup("TableHeaderRow");
                if (header.isVisible()) {
                    header.setMaxHeight(0);
                    header.setMinHeight(0);
                    header.setPrefHeight(0);
                    header.setVisible(false);
                }
            }
        });
    }
    
    private void llamarMetodosBinding() {

        SION.log(Modulo.VENTA, "Llamando Métodos de Capa Binding", Level.FINEST);

        binding.asignaPropiedadEnColumna(cantidadCol, Integer.class, "cantidad");
        binding.asignaPropiedadEnColumna(productoCol, String.class, "panombrearticulo");
        binding.asignaPropiedadEnColumna(precioCol, Double.class, "paprecio");
        binding.asignaPropiedadEnColumna(importeCol, Double.class, "importe");
        binding.asignaPropiedadEnColumna(descuentoCol, Double.class, "padescuento");
        binding.llenarTabla(tabla);

        binding.eventosTab(tabla, campoImporteRecibido, campoMontoRestante, campoMontoTarjeta, campoMontoEfectivo);

        binding.actualizarMontoVenta(etiquetaMontoVenta, tabla);

        binding.actualizaMontoRestante(etiquetaMontoVenta, campoMontoRestante, campoImporteRecibido);

        binding.eventosPanelVenta(panelPrincipal, tabla, etiquetaMontoVenta, campoImporteRecibido, campoCambio, etiquetaMontoVenta, campoBuscador, campoMontoRestante);

        binding.setCellFactory(tabla, cantidadCol, Double.class, Pos.CENTER_RIGHT, true);
        binding.setCellFactory(tabla, productoCol, String.class, Pos.CENTER_LEFT, false);
        //binding.setCellFactory(tabla, descuentoCol, Double.class, Pos.CENTER_RIGHT, false);
        binding.setCellFactory(tabla, precioCol, Double.class, Pos.CENTER_RIGHT, false);
        binding.setCellFactory(tabla, importeCol, Double.class, Pos.CENTER_RIGHT, false);
        
        descuentoCol.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn param) {
                return new TableCell<ArticuloBean, Double>() {

                    @Override
                    public void updateItem(Double item, boolean empty) {
                        super.updateItem(item, empty);

                        if (!this.isEmpty()) {
                            ////reavisa si E es Intger
                            /*DecimalFormatSymbols dfs = new DecimalFormatSymbols(new Locale("es", "MX"));
                            DecimalFormat dfmt = new DecimalFormat("#0.##", dfs);*/
                            //this.textProperty().setValue( dfmt.format(item) );
                            this.textProperty().setValue( String.format("%.2f",item) );
                            if( item.doubleValue() > 0 )
                                this.setStyle("-fx-text-fill: blue; -fx-font: 20pt Verdana;");
                            else
                                this.setStyle("-fx-text-fill: white; -fx-font: 20pt Verdana;");
                            this.alignmentProperty().set(Pos.CENTER_RIGHT);
                        }
                    }
                };
            }
        });

        /*
         * ventaBinding.actualizarFormatoDeCeldas(cantidadCol,
         * Pos.BASELINE_RIGHT);
         *
         * ventaBinding.alinearColumnas(precioCol, Double.class,
         * Pos.CENTER_RIGHT, true);
         *
         * ventaBinding.alinearColumnas(productoCol, String.class,
         * Pos.CENTER_LEFT, false);
         */
        
        
        binding.eventosCampoImporteRecibido(campoImporteRecibido, campoCambio, etiquetaMontoVenta, tabla, campoBuscador, campoMontoRestante);

        binding.eventosAgregarQuitarArticuloEnTabla(tabla, importeCol, etiquetaMontoVenta);

        binding.refrescarTablaArticulos();

        //ventaBinding.revisarValoresNegativos(tabla, importeCol);

        binding.posicionarScrollEnTabla(tabla);
        binding.eventosBotonPagarOtrasFormasDePago(grupoPago, campoImporteRecibido, etiquetaMontoVenta);
        binding.eventosBotonTA(grupoVentaTA);
        binding.eventosBotonPagoEfectivo(grupoPagoEfectivo, campoImporteRecibido, etiquetaMontoVenta);
        binding.eventosBotonOtrasFormasdePago(grupoOtrasFormasPago, campoImporteRecibido, etiquetaMontoVenta);
        binding.eventosBotonCancelar(grupoCancelar, campoImporteRecibido, campoCambio, campoMontoRestante, tabla);
        /////fin ventaBinding venta
        eventoBotonNavegacionTarjetas(vistaImagenBotonValeESig, etiquetaTarjetaBancariaDesc, etiquetaNoTarjetaDesc, etiquetaImporteTarjetaDesc, etiquetaAutorizadaDesc, 0);
        eventoBotonNavegacionTarjetas(vistaImagenBotonTarjetaSig, etiquetaTarjetaBancariaDesc, etiquetaNoTarjetaDesc, etiquetaImporteTarjetaDesc, etiquetaAutorizadaDesc, 1);
        binding.actualizarNavegadorTarjetas();

        binding.formatearCampoTarjetas(campoImporteRecibido);
        binding.formatearCampoTarjetas(campoMontoEfectivo);
        binding.formatearCampoTarjetas(campoMontoValesE);
        binding.formatearCampoTarjetas(campoMontoTarjeta);
        binding.formatearCampoVales(campoMontoVales, 5);
        binding.formatearCampoVales(campoCantidadVales, 2);

        binding.validaCampoVales(campoMontoEfectivo, campoMontoVales, campoCantidadVales);
        binding.validaCampoVales(campoMontoValesE, campoMontoVales, campoCantidadVales);
        binding.validaCampoVales(campoMontoTarjeta, campoMontoVales, campoCantidadVales);


        binding.actualizaMontoRecibido(campoMontoEfectivo, campoMontoVales, campoMontoValesE, campoMontoTarjeta, campoCambio, campoImporteRecibido, campoCantidadVales, etiquetaMontoVenta, campoMontoRestante);


        binding.eventosCamposOtrasFormasDePago(campoMontoEfectivo, campoMontoVales, campoMontoValesE, campoMontoTarjeta, campoCantidadVales);

        setCursor(vistaImagenBotonTarjetaSig);
        setCursor(vistaImagenBotonValeESig);

        binding.operacionesTarjeta(campoMontoTarjeta, 1);
        binding.operacionesTarjeta(campoMontoValesE, 2);
        
        binding.operacionesTarjetaDescuento(grupoTarjetaDescto);

    }
    
    private void setCursor(final Node _nodo) {

        _nodo.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {

                if (arg2 && !arg1) {
                    _nodo.setCursor(Cursor.HAND);

                } else if (arg1 && !arg2) {
                    _nodo.setCursor(Cursor.NONE);

                }


            }
        });

    }

    /////////tarjetas
    public void setValorNumerotarjeta(String numero) {
        etiquetaNoTarjeta.textProperty().setValue(numero);
    }

    public String getValorEtiquetaCantidadtarjetasRecibidas() {
        return etiquetaCantidadTarjetasRecibidas.textProperty().getValue().trim();
    }

    public void setValorEtiquetaCantidadTarjetasRecibidas(String cantidad) {
        etiquetaCantidadTarjetasRecibidas.textProperty().setValue(cantidad);
    }

    public String getValorEtiquetaCantidadValesERecibidos() {
        return etiquetaCantidadValesERecibidos.textProperty().getValue().trim();
    }

    public void setValorEtiquetaCantidadValesERecibidos(String cantidad) {
        etiquetaCantidadValesERecibidos.textProperty().setValue(cantidad);
    }

    public String getValorMontoCantidadTarjetasRecibidas() {
        return etiquetaMontoCantidadTarjetasRecibidas.textProperty().getValue().trim();
    }

    public void setValorMontoCantidadTarjetasRecibidas(String cantidad) {
        etiquetaMontoCantidadTarjetasRecibidas.textProperty().setValue(cantidad);
    }

    public String getValorMontoCantidadValesERecibidos() {
        return etiquetaMontoCantidadValesERecibidos.textProperty().getValue().trim();
    }

    public void setValorMontoCantidadValesERecibidos(String cantidad) {
        etiquetaMontoCantidadValesERecibidos.textProperty().setValue(cantidad);
    }

    public void setDescripcionTarjetaBancaria(String descripcion) {
        etiquetaTarjetaBancariaDesc.textProperty().setValue(descripcion);
    }

    public void setDescripcionNoTarjeta(String descripcion) {
        etiquetaNoTarjetaDesc.textProperty().setValue(descripcion);
    }

    public void setDescripcionImporteTarjeta(String descripcion) {
        etiquetaImporteTarjetaDesc.textProperty().setValue(descripcion);
    }

    public String getDescripcionAutorizada() {
        return etiquetaAutorizadaDesc.textProperty().getValue().trim();
    }

    public void setDescripcionAutorizada(String descripcion) {
        etiquetaAutorizadaDesc.textProperty().setValue(descripcion);
    }

    public void actualizarEtiquetasNavegadorTarjetas(final String tarjetaBancariaDesc, final String noTarjetaDesc, final String importeTarjetaDesc, final String autorizadaDesc) {

        etiquetaTarjetaBancariaDesc.textProperty().setValue(tarjetaBancariaDesc);
        etiquetaNoTarjetaDesc.textProperty().setValue(noTarjetaDesc);
        etiquetaImporteTarjetaDesc.textProperty().setValue(importeTarjetaDesc);
        etiquetaAutorizadaDesc.textProperty().setValue(autorizadaDesc);

    }

    public void eventoBotonNavegacionTarjetas(ImageView imagenBoton, final Label etiquetaTarjetaBancariaDesc, final Label etiquetaNoTarjetaDesc, final Label etiquetaImporteTarjetaDesc, final Label etiquetaAutorizadaDesc, final int idTarjeta) {
        imagenBoton.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                binding.actualizarEtiquetasNavegadorTarjetas(idTarjeta);
            }
        });
    }

    public void cargarLoading() {

        panelPrincipal.setDisable(true);

        SION.log(Modulo.VENTA, "Agregando imagen de loading al panel", Level.INFO);

        imagenLoading.setFitHeight(100);
        imagenLoading.setFitWidth(100);
        imagenLoading.setCache(true);
        imagenLoading.setStyle("-fx-background-color:transparent;");
        imagenLoading.setLayoutX((screen.getBounds().getMaxX() / 2) - imagenLoading.getFitHeight());
        imagenLoading.setLayoutY((screen.getBounds().getMaxY() / 2) - imagenLoading.getFitHeight());
        try {
            panelPrincipal.getChildren().add(panelPrincipal.getChildren().size(), imagenLoading);
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, "Excepcion al cargar loading");
        }
    }
    
    public void requestFocusLoading(){
        imagenLoading.requestFocus();
    }
    
    public void removerImagenLoading() {

        SION.log(Modulo.VENTA, "Removiendo loading", Level.INFO);
        try {
            panelPrincipal.getChildren().remove(imagenLoading);
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, "Error al remover loading");
        }
        panelPrincipal.setDisable(false);
    }

    public void setFocusTablaVentas() {
        tabla.requestFocus();
    }

    public void setFocusCampoImporteRecibido() {//SION.log(Modulo.VENTA, "*<<<", Level.INFO);
        campoImporteRecibido.requestFocus();
    }

    public void setFocusCampoImporteEfectivo() {
        campoMontoEfectivo.requestFocus();
    }

    public void setFocusCampoTarjeta() {
        campoMontoTarjeta.requestFocus();
    }

    public void cambiarEstadoCampoImporteRecibido(boolean estado) {
        campoImporteRecibido.setDisable(estado);
    }

    public TableView getTablaVentas() {
        return tabla;
    }

    public Label getEtiquetaMontoVenta() {
        return etiquetaMontoVenta;
    }

    public BorderPane getPanelVentas() {
        return panelPrincipal;
    }

    public String getMontoVenta() {
        return etiquetaMontoVenta.textProperty().get().trim();
    }

    public void setMontoVenta(String cantidad) {
        etiquetaMontoVenta.textProperty().set(cantidad);
    }

    public String getMontoRecibidoPantallCaptura() {
        return campoImporteRecibido.textProperty().get().trim();
    }

    public void setMontoRecibidoPantallaCaptura(String cantidad) {
        campoImporteRecibido.textProperty().set(cantidad);
    }

    public String getMontoCambio() {
        return campoCambio.textProperty().get().trim();
    }

    public void setMontoCambio(String cantidad) {
        campoCambio.textProperty().set(cantidad);
    }

    public String getMontoRestante() {
        return campoMontoRestante.textProperty().get().trim();
    }

    public void setMontoRestante(String cantidad) {
        campoMontoRestante.textProperty().set(cantidad);
    }

    public String getCantidadVales() {
        return campoCantidadVales.textProperty().get().trim();
    }

    public void setCantidadVales(String cantidad) {
        campoCantidadVales.textProperty().set(cantidad);
    }

    public String getMontoVales() {
        return campoMontoVales.textProperty().get();
    }

    public void setMontoVales(String cantidad) {
        campoMontoVales.textProperty().set(cantidad);
    }

    public String getMontoEfectivo() {
        return campoMontoEfectivo.textProperty().get().trim();
    }

    public void setMontoEfectivo(String cadena) {
        campoMontoEfectivo.textProperty().set(cadena);
    }

    public String getMontoValesE() {
        return campoMontoValesE.textProperty().get().trim();
    }

    public void setMontoValesE(String cadena) {
        campoMontoValesE.textProperty().set(cadena);
    }

    public String getMontoTarjeta() {
        return campoMontoTarjeta.textProperty().get().trim();
    }

    public void setMontoTarjetas(String cadena) {
        campoMontoTarjeta.textProperty().set(cadena);
    }

    public TextField getCampoMontoEfectivo() {
        return campoMontoEfectivo;
    }

    public TextField getCampoCantidadVales() {
        return campoCantidadVales;
    }

    public TextField getCampoMontoVales() {
        return campoMontoVales;
    }

    public TextField getCampoMontoValesE() {
        return campoMontoValesE;
    }

    public TextField getCampoMontoTarjeta() {
        return campoMontoTarjeta;
    }

    public void habilitarCampoMontoEfectivo(boolean estado) {
        campoMontoEfectivo.setDisable(estado);
    }

    public void habilitarCampoCantidadVales(boolean estado) {
        campoCantidadVales.setDisable(estado);
    }

    public void habilitarCampoMontoVales(boolean estado) {
        campoMontoVales.setDisable(estado);
    }

    public void habilitarCampoMontoValesE(boolean estado) {
        campoMontoValesE.setDisable(estado);
    }

    public void habilitarCampoMontoTarjeta(boolean estado) {
        campoMontoTarjeta.setDisable(estado);
    }

    public void posicionarScrollTabla(int index) {
        tabla.getSelectionModel().clearSelection();
        tabla.getSelectionModel().selectLast();
        tabla.scrollTo(index);
        tabla.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    public int getTamanioComponenteNavegacionTarjetas() {
        return cajaTarjetasInterior2.getChildren().size();
    }

    public void agregarComponentesnavegacionTarjetas() {

        cajaTarjetasInterior2.getChildren().add(cajaTarjetasInterior2_5);
        cajaTarjetasInterior2.getChildren().add(cajaTarjetasInterior2_6);

    }

    public void removerComponentesNavegaciontarjetas() {
        cajaTarjetasInterior2.getChildren().remove(4);
        cajaTarjetasInterior2.getChildren().remove(4);
    }

    public void cambiarComponentesTAaVenta(final int idCambio) {

        if (idCambio == 1) {
            //grupoTarjetaDescto.setVisible(true);
            if (cajaDerechaVentas.getChildren().size() == 11) {//11
                cajaDerechaVentas.getChildren().remove(0);//venta de TA
                cajaDerechaVentas.getChildren().remove(0);//tarjeta
                
                cajaDerechaVentas.getChildren().remove(8);
                cajaDerechaVentas.getChildren().remove(8);
                cajaDerechaVentas.getChildren().add(8, grupoPago);
            }
        } else {
            //grupoTarjetaDescto.setVisible(false);
            if (cajaDerechaVentas.getChildren().size() == 10) {//10
                cajaDerechaVentas.getChildren().remove(8);
                cajaDerechaVentas.getChildren().add(8, grupoPagoEfectivo);
                cajaDerechaVentas.getChildren().add(9, grupoOtrasFormasPago);
                cajaDerechaVentas.getChildren().add(0, grupoTarjetaDescto);
            }
        }
    }

    public void cambiarPantalla(int pantalla) {

        cajaCentral.getChildren().clear();

        if (pantalla == PANTALLA_VENTA) {
            //grupoTarjetaDescto.setVisible(false);
            cajaCentral.getChildren().addAll(cajaIzquierdaPagos, cajaDerechaVentas);
            //cajaCentral.getChildren().addAll(cajaIzquierdaPagos, contDerecha);

            if (cajaDerechaVentas.getChildren().size() > 8) {

                cajaDerechaVentas.getChildren().remove(0);
                cajaDerechaVentas.getChildren().remove(0);
                cajaDerechaVentas.getChildren().remove(7);
                cajaDerechaVentas.getChildren().remove(7);

                cajaDerechaVentas.getChildren().add(7, grupoPago);

                cajaDerechaVentas.setSpacing(12);
            }

            campoMontoEfectivo.requestFocus();

        } else if (pantalla == PANTALLA_CAPTURA) {
            //grupoTarjetaDescto.setVisible(true);
            cajaCentral.getChildren().addAll(cajaIzquierdaVentas, cajaDerechaVentas);
            //cajaCentral.getChildren().addAll(cajaIzquierdaPagos, contDerecha);

            if (cajaDerechaVentas.getChildren().size() < 11) {
                cajaDerechaVentas.getChildren().remove(7);

                cajaDerechaVentas.getChildren().add(0, grupoVentaTA);
                cajaDerechaVentas.getChildren().add(8, grupoPagoEfectivo);
                cajaDerechaVentas.getChildren().add(9, grupoOtrasFormasPago);
                cajaDerechaVentas.getChildren().add(0, grupoTarjetaDescto);

                cajaDerechaVentas.setSpacing(5);
            }

        }

    }

    private void actualizarImporteTeclaMas(final TextField campotexto) {

        EventHandler<KeyEvent> eventoActualizarImporteTeclaMas = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {

                if (mas.match(arg0)) {
                    SION.log(Modulo.VENTA, "Tecla + presionada, seteando monto restante en campo " + campotexto.getId(), Level.INFO);
                    campotexto.textProperty().set(binding.getCadenaMontoRestante());
                }
            }
        };

        campotexto.addEventFilter(KeyEvent.KEY_PRESSED, eventoActualizarImporteTeclaMas);

    }

    private void actualizarMontosCamposTarjetas() {

        campoMontoValesE.focusedProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (!arg2 && arg1) {

                    if (!campoMontoValesE.textProperty().getValue().isEmpty() && !binding.estaStageTarjetasMostrandose()) {
                        SION.log(Modulo.VENTA, "Limpiando campo de vales electronicos", Level.FINEST);
                        campoMontoValesE.textProperty().setValue("");
                    }
                }
            }
        });

        campoMontoTarjeta.focusedProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (!arg2 && arg1) {

                    if (!campoMontoTarjeta.textProperty().getValue().isEmpty() && !binding.estaStageTarjetasMostrandose()) {
                        SION.log(Modulo.VENTA, "Limpiando campo de monto de tarjetas", Level.FINEST);
                        campoMontoTarjeta.textProperty().setValue("");
                    }
                }
            }
        });

    }

    private void aplicarEfectoBoton(final ImageView vistaImagen, final Image imagen, final Image imagenHover, final Group grupo) {

        grupo.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {

                if (!arg1 && arg2) {
                    vistaImagen.setImage(imagenHover);
                } else if (arg1 && !arg2) {
                    vistaImagen.setImage(imagen);
                }
            }
        });
    }

    private void deshabilitarBuscador(final TextField campoBuscador, final TableView tabla) {

        campoBuscador.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (tab.match(arg0) || f2.match(arg0) || shifTab.match(arg0)) {
                    tabla.requestFocus();
                    campoBuscador.setDisable(true);
                }
            }
        });
    }

    public void eventoEscBotonestarjetas(final Button boton) {

        boton.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (escape.match(arg0)) {
                    binding.operacionesTeclaEscape();
                }
            }
        });

    }

    public void movimientosTeclas(final TextField campoMontoEfectivo, final TextField campoCantidadVales, final TextField campoMontoVales, final TextField campoMontoValesE, final TextField campoMontoTarjeta) {

        EventHandler<KeyEvent> eventoCampoMontoEfectivo = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (abajo.match(arg0) || tab.match(arg0) || shifTab.match(arg0)) {

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            campoCantidadVales.requestFocus();
                        }
                    });

                }
            }
        };

        campoMontoEfectivo.addEventFilter(KeyEvent.KEY_PRESSED, eventoCampoMontoEfectivo);

        EventHandler<KeyEvent> eventoCampoCantidadVales = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (arriba.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            campoMontoEfectivo.requestFocus();
                        }
                    });
                }
                if (derecha.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            campoMontoVales.requestFocus();
                        }
                    });
                }
                if (abajo.match(arg0) || tab.match(arg0) || shifTab.match(arg0)) {

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            campoMontoValesE.requestFocus();
                        }
                    });
                }
            }
        };

        campoCantidadVales.addEventFilter(KeyEvent.KEY_PRESSED, eventoCampoCantidadVales);

        EventHandler<KeyEvent> eventoCampoMontoVales = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (arriba.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            campoMontoEfectivo.requestFocus();
                        }
                    });
                }
                if (izquierda.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            campoCantidadVales.requestFocus();
                        }
                    });
                }
                if (abajo.match(arg0) || tab.match(arg0) || shifTab.match(arg0)) {

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            campoMontoValesE.requestFocus();
                        }
                    });
                }
            }
        };

        campoMontoVales.addEventFilter(KeyEvent.KEY_PRESSED, eventoCampoMontoVales);

        EventHandler<KeyEvent> eventoCampoMontoValesE = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (arriba.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            campoCantidadVales.requestFocus();
                        }
                    });
                }
                if (abajo.match(arg0) || tab.match(arg0) || shifTab.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            campoMontoTarjeta.requestFocus();
                        }
                    });


                }
            }
        };

        campoMontoValesE.addEventFilter(KeyEvent.KEY_PRESSED, eventoCampoMontoValesE);

        EventHandler<KeyEvent> eventoCampoMontoTarjeta = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (arriba.match(arg0)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            campoMontoValesE.requestFocus();
                        }
                    });
                } 
            }
        };

        campoMontoTarjeta.addEventFilter(KeyEvent.KEY_PRESSED, eventoCampoMontoTarjeta);



    }

    private void deshabailitarDragTabla() {
        tabla.addEventFilter(javafx.scene.input.MouseEvent.MOUSE_DRAGGED, new EventHandler<javafx.scene.input.MouseEvent>() {

            @Override
            public void handle(javafx.scene.input.MouseEvent arg0) {
                arg0.consume();
            }
        });
    }

    private void creaEventoF12Venta() {

        eventoF12Venta = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (f12.match(arg0)) {
                    SION.log(Modulo.VENTA, "F12 presionado.", Level.INFO);

                    String permisoPagoServicios = SION.obtenerParametro(Modulo.SION, "SEGURIDAD.PAGOSERVICIOS");

                    if (permisoPagoServicios != null) {
                        SION.log(Modulo.VENTA, "Propiedad de pago servicios encontrado en archivo", Level.INFO);
                        if (VistaBarraUsuario.getSesion().comparaPermiso(permisoPagoServicios)) {
                            SION.log(Modulo.VENTA, "Permiso para mostrar pago de servicios validado", Level.INFO);
                            double montoVenta = 0;
                            try {
                                montoVenta = Double.parseDouble(etiquetaMontoVenta.textProperty().getValue());
                            } catch (NumberFormatException e) {
                                SION.logearExcepcion(Modulo.VENTA, e);
                            }
                            if (montoVenta == 0 && binding.getCantidadRegistrosTabla() == 0) {
                                SION.log(Modulo.VENTA, "Enviando a pantalla de pago de servicios", Level.INFO);
                                AdministraVentanas.mostrarEscena(SION.obtenerParametro(Modulo.VENTA, "ventanaServicios"));
                            } else {
                                SION.log(Modulo.VENTA, "Existe un monto pendiente por pagar, se permanece en pantalla de ventas", Level.WARNING);
                            }
                        }
                    }
                }
            }
        };

    }

    private void crearEventosAlarma() {

        SION.log(Modulo.VENTA, "Creando eventos alarma seguridad", Level.INFO);

        eventoAlarma1 = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (alarma1.match(arg0)) {
                    //System.out.println("alarma con control");
                    SION.log(Modulo.VENTA, "Alarma de seguridad activada, comando presionado: ctrl + enter", Level.INFO);
                    utilerias.enviarAlarma();
                }
            }
        };

        eventoAlarma2 = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (alarma2.match(arg0)) {
                    //System.out.println("alarma con shift");
                    SION.log(Modulo.VENTA, "Alarma de seguridad activada, comando presionado: shift + enter", Level.INFO);
                    utilerias.enviarAlarma();
                }
            }
        };

        eventoAlarma3 = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (alarma3.match(arg0)) {
                    //System.out.println("alarma con windows");
                    SION.log(Modulo.VENTA, "Alarma de seguridad activada, comando presionado: windows + enter", Level.INFO);
                    utilerias.enviarAlarma();
                }
            }
        };
    }

    private void agregarEventoAlarma() {
        SION.log(Modulo.VENTA, "Agregando eventos Alarma ", Level.INFO);
        panelPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, eventoAlarma1);
        panelPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, eventoAlarma2);
        panelPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, eventoAlarma3);
    }

    private void agregaEventoF12Venta() {
        SION.log(Modulo.VENTA, "Agregando evento F12 pago de servicios", Level.INFO);
        panelPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, eventoF12Venta);
    }

    private void removerEventoF12Venta() {
        SION.log(Modulo.VENTA, "Removiendo evento F12 pago de servicios", Level.INFO);
        panelPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, eventoF12Venta);
    }

    @Override
    public Parent obtenerEscena(Object... obj) {
        SION.log(Modulo.VENTA, "Inicializando Pantalla de Ventas", Level.INFO);


        root.focusedProperty().removeListener(listener);
        root.focusedProperty().addListener(listener);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                //validar jars en bd
                //validarComponentes();
                RepositorioRedondeo.leerImagenesRedondeo();
            }
        });
        
        
        try{

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                VistaBarraUsuario.setTituloVentana("Punto de Venta");

                SION.log(Modulo.VENTA, "Llamando a Método Poblar Atajos", Level.FINEST);

                try {
                    VistaBarraUsuario.poblarAtajos(Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "atajo.1")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "atajo.2")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "atajo.3")));
                } catch (Exception e) {
                    SION.log(Modulo.VENTA, "Ocurrió un error al invocar método de atajos " + e.getMessage(), Level.SEVERE);
                }
            }
        });

        if (cab == null) {
            SION.log(Modulo.VENTA, "Instancia de buscador nula, se procede a crearla", Level.INFO);
            Platform.runLater(new Runnable() {
                @Override
            public void run() {
                    cab = new ControlAutocompletarBusqueda(campoBuscador, b1, campoBuscador.textProperty(), stringProperty, skinAutobusqueda.EXTRAGRANDEFMED, false, TipoBuscador.ARTICULOS, 0);
                    cab.monitorCambioSeleccion.removeListener(changelistener);
                    cab.monitorCambioSeleccion.addListener(changelistener);
                }
                
            });
            
            

            //crearIndiceConstructor();

        }

        //final ConstructorIndiceBusqueda cib = new ConstructorIndiceBusqueda();
        //try {////quitar
          //  ConstructorIndiceBusqueda.contruirIndiceArticulos();
            //final CajaDeTextoBusqueda campoBuscar = new CajaDeTextoBusqueda();
        //} catch (Exception ex) {
          //  ex.printStackTrace();
        //}

        AdministraVentanas.removerF12EventHandler();
        agregaEventoF12Venta();
        agregarEventoAlarma();

        binding.inicializarVariables(panelPrincipal, etiquetaMontoVenta, tabla, campoMontoRestante, campoImporteRecibido);
        estaSucia = true;
        
        }catch(Exception e){
            
            //respuesta de error generica
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            SION.log(Modulo.VENTA, "Ocurrió un error al obtener escena: " + exceptionAsString, Level.SEVERE);
            
        }
        
        return root;
    }
    /*
    private void validarComponentes(){
        
        boolean existenComponentesDesactualizados = false;
        
        SION.log(Modulo.VENTA, "Validando version de los componentes utilizados por el modulo de venta", Level.INFO);
        
        if(!utilerias.esVersionUtileriasValida()){
            existenComponentesDesactualizados = true;
        }
        
        if (!utilerias.esVersionValida(version, UtileriasVenta.Componente.VENTA_PRINCIPAL, Modulo.VENTA, Modulo.VENTA)){
            existenComponentesDesactualizados = true;
        }
        
        if(existenComponentesDesactualizados){
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    AdministraVentanas.mostrarAlertaRetornaFoco("El sistema no se encuentra actualizado. Para evitar posibles errores contacte a Soporte Técnico.", TipoMensaje.ADVERTENCIA, tabla);
                }
            });
            
        }
        
    }*/

    public void resetIndices() {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {

                SION.log(Modulo.VENTA, "Reseteando Indices de buscador", Level.INFO);
                cab.resetIndices();
                existeParametroIndices = false;

            }
        });
    }

    @Override
    public void limpiar() 
	{
        SION.log(Modulo.VENTA, "Limpiando escena - VENTA", Level.INFO);

        Platform.runLater(new Runnable() 
		{
            @Override
            public void run() {
                binding.limpiarVariables(tabla);
            }
        });

        if (estaSucia) {
            limpiarComponentes();
        }

        removerEventoF12Venta();

    }

    @Override
    public String[] getUserAgentStylesheet() 
	{
        return new String[]{"/neto/sion/tienda/venta/imagenes/inicio/Venta.css", "/neto/sion/tienda/venta/imagenes/inicio/estilo2.css"};
    }
}
