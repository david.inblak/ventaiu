/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package neto.sion.tienda.venta.vista;

import java.net.URL;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyCombination.Modifier;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import neto.sion.tienda.controles.vista.Utilerias;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.modelo.TipoBuscador;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.genericos.vista.ControlAutocompletarBusqueda;
import neto.sion.tienda.genericos.vista.skinAutobusqueda;
import neto.sion.tienda.venta.bindings.VentaBindings;
import neto.sion.tienda.venta.bindings.VerificadorBinding;
import neto.sion.tienda.venta.modelo.RespuestaParametroBean;
import neto.sion.tienda.venta.vistamodelo.VentaVistaModelo;

/**
 *
 * @author fvega
 */
public class VerificadorVista {

    //instancias de clase
    private VentaVista ventaVista;
    private VerificadorBinding verificadorBinding;
    private VentaVistaModelo vistaModelo;
    private VentaBindings ventaBindings;
    private Utilerias utilerias;
    //banderas
    private boolean estaStageActivo;
    //contenedores
    public Stage stage;
    private Group grupo;
    private Scene escena;
    private BorderPane panel;
    private VBox cajaPrincipal;
    private VBox cajaCabecero;
    private VBox cajaIconoVerificador;
    private VBox cajaComponentesBuscador;
    private HBox caja3_1;
    private VBox cajaEtiquetaCodBarras;
    private VBox caja3_1_2;
    private HBox caja3_2;
    private VBox caja3_2_1;
    private VBox caja3_2_2;
    private VBox cajaEtiquetaResultado;
    private HBox cajaBuscador;
    private VBox caja5_1;
    private VBox caja5_2;
    private HBox cajaBotonesInferiores;
    private VBox cajaBotonVerificar;
    private VBox cajaBotonAgregarArt;
    private VBox cajaRelleno;
    //componentes
    private Image imagenBuscador;
    private ImageView vistaImagenBuscador;
    private Label etiquetaVerificadorPrecio;
    private Label etiquetaBuscadorActivado;
    private final int CAMPO_BUSCADOR;
    private final int CAMPO_ESCANER;
    private SimpleIntegerProperty indicadorBuscador;
    private PasswordField campoCodigo;
    private TextField campoCodigoBuscador;
    private Image imagenBuscar;
    private Image imagenBuscarHover;
    private ImageView etiquetaBuscar;
    private Label etiquetaResultadoBusqueda;
    private Label etiquetaDescripcionArticulo;
    private TextField campoPrecio;
    private Image imagenVerificacion;
    private Image imagenVerificacionHover;
    private ImageView etiquetaVerificacion;
    private Image imagenAgregar;
    private Image imagenAgregarHover;
    private ImageView etiquetaAgregar;
    //buscador
    private Button b1;
    private Button b2;
    private StringProperty stringProperty;
    public ControlAutocompletarBusqueda cab;
    private ChangeListener changelistener;
    private KeyCombination f2;
    private boolean existeParametroVerificador;

    public VerificadorVista(VentaVista ventaVista, VentaVistaModelo vistaModelo, VentaBindings ventaBindings) {

        SION.log(Modulo.VENTA, "Entrando a constructor de vista de verificador de precios ", Level.INFO);

        ////instancias de clases
        this.ventaVista = ventaVista;
        this.vistaModelo = vistaModelo;
        this.ventaBindings = ventaBindings;
        this.utilerias = new Utilerias();
        this.verificadorBinding = new VerificadorBinding(ventaVista, vistaModelo, ventaBindings, utilerias, this);
        this.estaStageActivo = false;
        this.grupo = new Group();
        this.panel = new BorderPane();
        
        this.cajaPrincipal = utilerias.getVBox(0, 600, 550);
        
        //cajaPrincipal
        this.cajaCabecero = utilerias.getVBox(0, 600, 70);
        this.cajaIconoVerificador = utilerias.getVBox(0, 550, 130);
        this.cajaComponentesBuscador = utilerias.getVBox(1, 530, 80);
        this.cajaEtiquetaResultado = utilerias.getVBox(1, 550, 30);
        this.cajaBuscador = utilerias.getHBox(5, 550, 110);
        this.cajaBotonesInferiores = utilerias.getHBox(5, 550, 40);
        this.cajaRelleno = utilerias.getVBox(0, 600, 110);
        
        //cajaIconoVerificador
        
        this.caja3_1 = utilerias.getHBox(0, 550, 40);
        this.cajaEtiquetaCodBarras = utilerias.getVBox(5, 275, 40);
        this.caja3_1_2 = utilerias.getVBox(5, 275, 40);
        this.caja3_2 = utilerias.getHBox(5, 550, 40);
        this.caja3_2_1 = utilerias.getVBox(1, 350, 40);
        this.caja3_2_2 = utilerias.getVBox(1, 200, 40);
        
        
        this.caja5_1 = utilerias.getVBox(1, 275, 110);
        this.caja5_2 = utilerias.getVBox(1, 275, 110);
        
        this.cajaBotonVerificar = utilerias.getVBox(1, 275, 35);
        this.cajaBotonAgregarArt = utilerias.getVBox(1, 275, 35);
        

        this.imagenBuscador = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/ico_CodigoBarras.png");
        //this.imagenBuscador = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/iconoBuscador_2.png"));
        this.vistaImagenBuscador = new ImageView(imagenBuscador);
        this.etiquetaVerificadorPrecio = new Label("Código de Barras:");
        this.etiquetaBuscadorActivado = new Label("Buscador activado");
        this.CAMPO_BUSCADOR = 0;
        this.CAMPO_ESCANER = 1;
        this.indicadorBuscador = new SimpleIntegerProperty(CAMPO_ESCANER);
        this.campoCodigo = new PasswordField();
        this.campoCodigoBuscador = utilerias.getTextField("", 330, 30);
        this.imagenBuscar = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_Buscar.jpg");
        //this.imagenBuscar = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Ver_buscar.png"));
        this.imagenBuscarHover = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_Buscar_on.jpg");
        //this.imagenBuscarHover = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Ver_buscarHover.png"));
        this.etiquetaBuscar = new ImageView(imagenBuscar);
        this.etiquetaResultadoBusqueda = new Label("Resultado de Búsqueda de precio");
        this.etiquetaDescripcionArticulo = new Label("SIN INFORMACIÓN");
        this.campoPrecio = new TextField();
        this.imagenVerificacion = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_VerificarArticulo.jpg");
        this.imagenVerificacionHover = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_VerificarArticulo_on.jpg");
        //this.imagenVerificacion = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Ver_verificar.png"));
        //this.imagenVerificacionHover = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Ver_verificarHover.png"));
        this.etiquetaVerificacion = new ImageView(imagenVerificacion);
        this.imagenAgregar = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_AgregarCarrito.jpg");
        this.imagenAgregarHover = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_AgregarCarrito_on.jpg");
        //this.imagenAgregar = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Ver_agregar.png"));
        //this.imagenAgregarHover = new Image(getClass().getResourceAsStream("/neto/sion/tienda/venta/imagenes/inicio/Ver_agregarHover.png"));
        this.etiquetaAgregar = new ImageView(imagenAgregar);


        this.b1 = new Button();
        this.b2 = new Button();
        this.stringProperty = new SimpleStringProperty();
        this.existeParametroVerificador = false;


        this.changelistener = new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {

                verificadorBinding.operacionesbuscador(panel, stringProperty, campoCodigoBuscador, campoPrecio, etiquetaDescripcionArticulo, stage);
            }
        };

        this.f2 = new KeyCodeCombination(KeyCode.F2);

        verificadorBinding.getSeIntrodujoArticuloPorBuscador().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (existeParametroVerificador) {
                    if (arg2 && !arg1) {

                        try {
                            cajaBotonAgregarArt.getChildren().remove(etiquetaAgregar);
                        } catch (Exception e) {
                        }

                    } else if (arg1 && !arg2) {

                        try {
                            cajaBotonAgregarArt.getChildren().addAll(etiquetaAgregar);
                        } catch (Exception e) {
                        }
                    }
                }
            }
        });

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                escena = new Scene(grupo, 600, 550, Color.WHITE);
                stage = new Stage(StageStyle.UNDECORATED);
                stage.initModality(Modality.APPLICATION_MODAL);

                establecerEstilos();
                establecerDatosComponentes();
                llamarBindingVerificador();
            }
        });


    }

    private void establecerEstilos() {

        SION.log(Modulo.VENTA, "Agregando estilos a Verificador", Level.INFO);

        //panel.setStyle("-fx-background-color: white; -fx-border-radius: 10px;");

        etiquetaBuscadorActivado.setStyle("-fx-alignment: center-left; -fx-font: 15pt Verdana; -fx-text-fill: red;");

        etiquetaVerificadorPrecio.setStyle("-fx-alignment: center; -fx-font: 22pt Verdana; -fx-text-fill: #32a3ea; -fx-text-weight: bold;");
        etiquetaVerificadorPrecio.setAlignment(Pos.CENTER_LEFT);

        campoCodigo.setStyle("-fx-font: 15pt Verdana; -fx-border-color: F6802B; -fx-border-style: solid; -fx-border-width: 2; -fx-background-color: white; -fx-border-radius: 10px; -fx-background-radius: 11px;");
        campoCodigoBuscador.setStyle("-fx-font: 15pt Verdana; -fx-border-color: F6802B; -fx-border-style: solid; -fx-border-width: 2; -fx-background-color: white; -fx-border-radius: 10px; -fx-background-radius: 11px;");
        campoPrecio.setStyle("-fx-font: 35pt Verdana; -fx-border-color: F6802B; -fx-border-style: solid; -fx-border-width: 2; -fx-background-color: white; -fx-border-radius: 10px; -fx-background-radius: 11px;");

        cajaPrincipal.setStyle("-fx-alignment:center; -fx-border-width: 2; -fx-border-color: blue;");
        cajaCabecero.getStyleClass().add("CajaCabeceroVerificador");
        cajaIconoVerificador.getStyleClass().add("CajaIconoVerificador");
        
        
        cajaComponentesBuscador.setStyle("-fx-alignment: center;-fx-border-width: 0 0 1 0;-fx-border-color: lightgray;");
        caja3_1.setStyle("-fx-alignment: center;");
        cajaEtiquetaCodBarras.setStyle("-fx-alignment: center;");
        caja3_1_2.setStyle("-fx-alignment: center;");
        caja3_2.setStyle("-fx-alignment: top-center;");
        caja3_2_1.setStyle("-fx-alignment: center;");
        caja3_2_2.setStyle("-fx-alignment: center;");
        cajaEtiquetaResultado.setStyle("-fx-text-fill: #32a3ea; -fx-alignment:center; -fx-background-color: white;");
        cajaBuscador.setStyle("-fx-alignment: center;");
        caja5_1.setStyle("-fx-alignment: center;");
        /*
         * caja5_1_1.setStyle("-fx-alignment: center-left;");
         * caja5_1_2.setStyle("-fx-alignment: center-left;");
         * caja5_1_3.setStyle("-fx-alignment: center-left;");
         */
        caja5_2.setStyle("-fx-alignment:center;");
        //caja5.setStyle("-fx-alignment: center;");
        cajaBotonesInferiores.setStyle("-fx-alignment: center;");
        cajaBotonVerificar.setStyle("-fx-alignment: center;");
        cajaBotonAgregarArt.setStyle("-fx-alignment: center;");

        //panel.setStyle("-fx-background-repeat: no-repeat; -fx-background-radius: 16px; -fx-background-color: white ; -fx-border-color: transparent, transparent, orange ; -fx-border-width: 7; -fx-border-radius: 16; -fx-border-insets:-7, -3, 0");
        //panel.setStyle(" -fx-background-color: white ; -fx-border-color: transparent, transparent, orange ; -fx-border-width: 3; -fx-border-radius: 1; fx-border-insets:-5");
        //panel.setStyle("-fx-alignment: center;");
    }

    private void establecerDatosComponentes() {

        SION.log(Modulo.VENTA, "Ajustando componentes de Verificador", Level.INFO);

        vistaImagenBuscador.setFitHeight(60);
        vistaImagenBuscador.setFitWidth(60);

        etiquetaVerificadorPrecio.setMaxSize(250, 30);
        etiquetaVerificadorPrecio.setMinSize(250, 30);
        etiquetaVerificadorPrecio.setPrefSize(250, 30);
        //etiquetaVerificadorPrecio.setAlignment(Pos.CENTER);

        campoCodigo.setMaxSize(330, 30);
        campoCodigo.setMinSize(330, 30);
        campoCodigo.setPrefSize(330, 30);
        campoCodigoBuscador.setMaxSize(330, 30);
        campoCodigoBuscador.setMinSize(330, 30);
        campoCodigoBuscador.setPrefSize(330, 30);

        etiquetaBuscar.setFitWidth(130);
        etiquetaBuscar.setFitHeight(30);

        etiquetaResultadoBusqueda.setMaxSize(530, 30);
        etiquetaResultadoBusqueda.setMinSize(530, 30);
        etiquetaResultadoBusqueda.setPrefSize(530, 30);
        etiquetaResultadoBusqueda.setAlignment(Pos.CENTER);

        campoPrecio.setMaxSize(230, 100);
        campoPrecio.setMinSize(230, 100);
        campoPrecio.setPrefSize(230, 100);

        etiquetaVerificacion.setFitHeight(35);
        etiquetaVerificacion.setFitWidth(230);

        etiquetaAgregar.setFitHeight(35);
        etiquetaAgregar.setFitWidth(230);

        etiquetaDescripcionArticulo.setMaxSize(230, 100);
        etiquetaDescripcionArticulo.setMinSize(230, 100);
        etiquetaDescripcionArticulo.setPrefSize(230, 100);
        etiquetaDescripcionArticulo.setFont(Font.font("", FontWeight.SEMI_BOLD, 25));
        etiquetaDescripcionArticulo.setWrapText(true);

        //cajaPrincipal

        //cajaCabecero.getChildren().addAll();
        
        cajaIconoVerificador.getChildren().addAll(vistaImagenBuscador);
        cajaComponentesBuscador.getChildren().addAll(caja3_1, caja3_2);
        cajaEtiquetaCodBarras.getChildren().add(etiquetaVerificadorPrecio);

        caja3_1.getChildren().addAll(cajaEtiquetaCodBarras, caja3_1_2);
        caja3_2.getChildren().addAll(campoCodigo, etiquetaBuscar);
        
        cajaEtiquetaResultado.getChildren().addAll(etiquetaResultadoBusqueda);
        
        
        cajaBuscador.getChildren().addAll(caja5_1, caja5_2);
        caja5_1.getChildren().addAll(etiquetaDescripcionArticulo);
        caja5_2.getChildren().addAll(campoPrecio);
        
        cajaBotonesInferiores.getChildren().addAll(cajaBotonVerificar, cajaBotonAgregarArt);
        cajaBotonVerificar.getChildren().addAll(etiquetaVerificacion);
        cajaBotonAgregarArt.getChildren().addAll(etiquetaAgregar);

        cajaPrincipal.getChildren().addAll(cajaCabecero, cajaIconoVerificador, cajaComponentesBuscador, cajaEtiquetaResultado, cajaBuscador, cajaBotonesInferiores,cajaRelleno);

        panel.setTop(cajaPrincipal);
        //panel.setCenter(caja2);
        //panel.setTop(caja);
        //panel.setBottom(caja3);
        panel.setMinHeight(550);
        panel.setMaxHeight(550);
        panel.setMinWidth(600);
        panel.setMaxWidth(600);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                grupo.getChildren().addAll(panel);
            }
        });

    }

    private void llamarBindingVerificador() {

        SION.log(Modulo.VENTA, "Lamando metodos de capa de binding de Verificador", Level.FINEST);
        //try{
        verificadorBinding.eventoBotonBuscar(etiquetaBuscar, campoCodigo, campoPrecio, etiquetaDescripcionArticulo);

        verificadorBinding.eventoCampoCodigo(campoCodigo, campoPrecio, etiquetaDescripcionArticulo, stage);
        //verificadorBinding.eventoCampoBuscador(campoCodigoBuscador, campoPrecio, etiquetaDescripcionArticulo, stage);

        verificadorBinding.eventoBotonAgregar(etiquetaAgregar, campoPrecio, etiquetaDescripcionArticulo, stage);

        //verificadorBinding.eventoEtiquetaCerrar(etiquetaCerrar, stage);

        verificadorBinding.validarCampo(campoCodigo);

        verificadorBinding.eventoESC(panel, stage);


        verificadorBinding.efectoHoverEtiquetas(etiquetaBuscar, imagenBuscar, imagenBuscarHover);

        verificadorBinding.efectoHoverEtiquetas(etiquetaVerificacion, imagenVerificacion, imagenVerificacionHover);

        verificadorBinding.efectoHoverEtiquetas(etiquetaAgregar, imagenAgregar, imagenAgregarHover);

        eventoCambioDeCampos();
        
        
        SION.log(Modulo.VENTA, "termina binfing verificador", Level.INFO);
    }

    private void eventoCambioDeCampos() {

        EventHandler<KeyEvent> eventof2 = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (f2.match(arg0)) {

                    if (indicadorBuscador.get() == CAMPO_ESCANER) {

                        indicadorBuscador.set(CAMPO_BUSCADOR);
                        caja3_1_2.getChildren().add(etiquetaBuscadorActivado);
                        caja3_2.getChildren().remove(0);
                        caja3_2.getChildren().remove(0);
                        caja3_2.getChildren().addAll(campoCodigoBuscador, etiquetaBuscar);

                        etiquetaDescripcionArticulo.setText("NO HAY INFORMACIÓN");
                        limpiar();
                        verificadorBinding.setSeIntrodujoArticuloPorBuscador(true);

                    } else {

                        indicadorBuscador.set(CAMPO_ESCANER);
                        caja3_1_2.getChildren().remove(0);
                        caja3_2.getChildren().remove(0);
                        caja3_2.getChildren().remove(0);
                        caja3_2.getChildren().addAll(campoCodigo, etiquetaBuscar);

                        etiquetaDescripcionArticulo.setText("NO HAY INFORMACIÓN");
                        limpiar();
                        verificadorBinding.setSeIntrodujoArticuloPorBuscador(false);

                    }
                }
            }
        };

        panel.addEventFilter(KeyEvent.KEY_PRESSED, eventof2);

    }

    public void limpiar() {

        SION.log(Modulo.VENTA, "Limpiando compoenentes visuales de verificador de precios", Level.FINEST);
        campoCodigoBuscador.setText("");
        campoPrecio.setText("");

    }

    public Boolean getStatusStage() {
        return stage.isShowing();
    }

    public void cerrarStage() {
        verificadorBinding.setSeIntrodujoArticuloPorBuscador(false);
        stage.close();
    }

    public Stage incializarStage() {

        SION.log(Modulo.VENTA, "Abriendo ventana de verificador de precios", Level.INFO);

        try {
            /*vistaModelo.llenarPeticionParametro(Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.buscador.validador.sistema")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.buscador.validador.sistema")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.buscador.validador.modulo")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.buscador.validador.submodulo")),
                    Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.buscador.validador.configuracion")));

            RespuestaParametroBean respuestaValidador = vistaModelo.consultarParametro();*/
            
            vistaModelo.llenarPeticionParametro(Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.buscador.validador.sistema")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.buscador.validador.sistema")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.buscador.validador.modulo")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.buscador.validador.submodulo")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.buscador.validador.configuracion")));

	      	RespuestaParametroBean respuestaValidador = vistaModelo.consultarParametro();

            if (respuestaValidador.getCodigoError() == 0) {
                if (respuestaValidador.getValorConfiguracion() == 1) {
                    existeParametroVerificador = true;
                }
            }
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, "Ocrrió una excepción al obtener parametro de restricción de buscador en validador de precios");
        }
        
        

        verificadorBinding.setExisteParametroValidador(existeParametroVerificador);

        if (cab == null) {
            cab = new ControlAutocompletarBusqueda(campoCodigoBuscador, b1, campoCodigoBuscador.textProperty(), stringProperty, skinAutobusqueda.EXTRAGRANDEFMED, false, TipoBuscador.ARTICULOS, 0);
        }

        cab.monitorCambioSeleccion.removeListener(changelistener);
        cab.monitorCambioSeleccion.addListener(changelistener);

        campoCodigoBuscador.setText("");
        campoPrecio.setText("");
        campoPrecio.setEditable(false);
        etiquetaDescripcionArticulo.setText("NO HAY INFORMACIÓN");

        escena.getStylesheets().addAll(VerificadorVista.class.getResource("/neto/sion/tienda/venta/imagenes/inicio/Venta.css").toExternalForm());
        stage.setTitle("Verificador de Precios");
        stage.setScene(escena);
        stage.centerOnScreen();
        stage.show();

        return stage;

    }
}