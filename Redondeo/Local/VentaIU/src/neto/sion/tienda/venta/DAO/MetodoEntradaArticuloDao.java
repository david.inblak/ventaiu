/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.modelo.MetodoEntradaArticuloBean;
import neto.sion.tienda.venta.modelo.PeticionMetodosEntradaArticuloBean;
import neto.sion.tienda.venta.modelo.RespuestaMetodosEntradaArticuloBean;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.apache.commons.dbcp.DelegatingConnection;

/**
 *
 * @author fvegap
 */
public class MetodoEntradaArticuloDao {

    private OracleConnection oracleConnection;
    private CallableStatement callableStatement;
    private String sql;
    private CallableStatement cs;
    private Connection connection;
    private ARRAY arregloArticulos;
    private Object[] objetoArregloArticulos;
    private StructDescriptor estructura;
    private ArrayDescriptor estructuraArreglo;

    public MetodoEntradaArticuloDao() {
        this.sql = null;
        this.cs = null;
    }

    public RespuestaMetodosEntradaArticuloBean guardarArticulosXMetodoEntrada(PeticionMetodosEntradaArticuloBean _peticion) {
        RespuestaMetodosEntradaArticuloBean respuesta = new RespuestaMetodosEntradaArticuloBean();

        SION.log(Modulo.VENTA, "Petición de inserción de artículos por mpétodo de entrada: " + _peticion.toString(), Level.INFO);

        int contador = 0;

        try {
            connection = Conexion.obtenerConexion();
            oracleConnection = (OracleConnection) ((DelegatingConnection) connection).getInnermostDelegate();
            sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAVENTAS.SPINSERTAMETODOENTRADA");
            SION.log(Modulo.VENTA, "SQL a ejecutar... " + sql, Level.FINE);
            callableStatement = connection.prepareCall(sql);

            objetoArregloArticulos = new Object[_peticion.getMetodosEntrada().length];
            estructura = StructDescriptor.createDescriptor(
                    SION.obtenerParametro(Modulo.VENTA, "estructura.metodoEntrada"), oracleConnection);
            estructuraArreglo = ArrayDescriptor.createDescriptor(
                    SION.obtenerParametro(Modulo.VENTA, "estructura.arreglo.metodoEntrada"), oracleConnection);

            for (MetodoEntradaArticuloBean articulo : _peticion.getMetodosEntrada()) {
                Object[] objeto = new Object[]{articulo.getPaisId(), articulo.getTiendaId(), articulo.getFecha(),
                    articulo.getUsuarioId(), articulo.getArticuloId(), articulo.getCodigoBarras(),
                    articulo.getMetodoEntradaId(), articulo.getCantidad()};
                objetoArregloArticulos[contador] = new STRUCT(estructura, oracleConnection, objeto);
                contador++;
            }

            arregloArticulos = new ARRAY(estructuraArreglo, oracleConnection, objetoArregloArticulos);

            callableStatement.setArray(1, arregloArticulos);
            callableStatement.registerOutParameter(2, OracleTypes.NUMBER);
            callableStatement.registerOutParameter(3, OracleTypes.VARCHAR);
            callableStatement.execute();

            respuesta.setCodigoError(callableStatement.getInt(2));
            respuesta.setDescError(callableStatement.getString(3));

            SION.log(Modulo.VENTA, "Respuesta a la petición: " + respuesta.toString(), Level.INFO);

        } catch (Exception e) {
            e.printStackTrace();
            SION.log(Modulo.VENTA, "Se generó un error al insertar métodos de entrada", Level.SEVERE);
            SION.logearExcepcion(Modulo.VENTA, e, sql);
            respuesta = null;
        } finally {
            Conexion.cerrarConexion(connection);
            Conexion.cerrarCallableStatement(callableStatement);
        }


        return respuesta;
    }
}
