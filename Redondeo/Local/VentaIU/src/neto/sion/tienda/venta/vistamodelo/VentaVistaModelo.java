/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package neto.sion.tienda.venta.vistamodelo;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import neto.sion.base.bean.BloqueoBean;
import neto.sion.clientews.tarjeta.dto.RespuestaReversoTarjetaDto;
import neto.sion.impresion.ventanormal.dto.ArticuloTicketDto;
import neto.sion.impresion.ventanormal.dto.VentaNormalTicketDto;
import neto.sion.impresion.ventanormal.dto.VentaTATicket;
import neto.sion.impresion.ventanormal.dto.VentaTarjetaVoucher;
import neto.sion.tarjeta.servicios.dto.RespuestaReversoDto;
import neto.sion.tarjeta.servicios.serviceFacade.ConsumirTarjeta;
import neto.sion.tarjeta.servicios.serviceFacade.ConsumirTarjetaImp;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.bean.*;
import neto.sion.tienda.caja.bean.ObjetoBloqueo;
import neto.sion.tienda.caja.controlador.BloqueosOperadorLocal;
import neto.sion.tienda.controlador.ControladorTarjeta;
import neto.sion.tienda.controles.vista.Utilerias;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.exception.SionException;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.seguridad.modelo.UsuarioSesionBean;
import neto.sion.tienda.venta.DAO.*;
import neto.sion.tienda.venta.bean.PeticionActualizaConciliacionPendienteBean;
import neto.sion.tienda.venta.bean.RespuestaActualizaConciliacionPendienteBean;
import neto.sion.tienda.venta.bloqueo.bean.MensajeBloqueoBean;
import neto.sion.tienda.venta.bloqueo.fachada.BloqueoVenta;
import neto.sion.tienda.venta.conciliacion.bean.PeticionActTransaccionLocalBean;
import neto.sion.tienda.venta.conciliacion.bean.PeticionConciliacionLocalBean;
import neto.sion.tienda.venta.conciliacion.bean.RespuestaActualizacionLocalBean;
import neto.sion.tienda.venta.conciliacion.bean.RespuestaConciliacionLocalBean;
import neto.sion.tienda.venta.conciliacion.controlador.ConciliacionLocalControlador;
import neto.sion.tienda.venta.constantes.TiposImpresion;
import neto.sion.tienda.venta.controlador.ControladorTarjetasDescuento;
import neto.sion.tienda.venta.fueralinea.bean.ArticuloFLBean;
import neto.sion.tienda.venta.fueralinea.bean.PeticionVentaLocalBean;
import neto.sion.tienda.venta.fueralinea.bean.RespuestaVentaLocalBean;
import neto.sion.tienda.venta.fueralinea.bean.TipoPagoBean;
import neto.sion.tienda.venta.fueralinea.controlador.VentaFueraLineaControlador;
import neto.sion.tienda.venta.impresiones.ImpresionVenta;
import neto.sion.tienda.venta.modelo.*;
import neto.sion.tienda.venta.modelo.RespuestaPagoTarjetaBean;
import neto.sion.tienda.venta.modelo.RespuestaParametroBean;
import neto.sion.tienda.venta.modelo.RespuestaValidacionTarjetaBean;
import neto.sion.tienda.venta.modelo.TarjetasBean;
import neto.sion.tienda.venta.recargas.tad.bean.CompaniaBean;
import neto.sion.tienda.venta.recargas.tad.binding.RecargasTADBinding;
import neto.sion.venta.bloqueo.bean.BloqueoVentaBean;
import neto.sion.venta.recargas.internet.recargale.cliente.dto.ArticuloRIDto;
import neto.sion.venta.recargas.internet.recargale.cliente.dto.PeticionVentaRecInternetDto;
import neto.sion.venta.recargas.internet.recargale.cliente.dto.RespuestaVentaRecInternetDto;
import neto.sion.venta.recargas.internet.recargale.cliente.dto.TipoPagoRIDto;
import neto.sion.venta.recargas.internet.recargale.cliente.serviceFacade.ClienteWSRecargaInternet;
import neto.sion.venta.servicios.cliente.dto.*;
import neto.sion.venta.servicios.cliente.serviceFacade.ConsumirVenta;
import neto.sion.venta.servicios.cliente.serviceFacade.ConsumirVentaImp;
import neto.sion.venta.tarjeta.pt.iso8583.nip.facade.ConsumirTarjetaPT_ISO8583NIP;
import neto.sion.venta.tarjeta.pt.iso8583.nip.facade.ConsumirTarjetaPT_ISO8583NIPImp;

/**
 *
 * @author fvega
 */
public class VentaVistaModelo {

    private Utilerias utilerias;
    private ParametrosDao parametrosDevolucionesDAO;
    private ConsumirVenta consumirVentaImp;
    private ClienteWSRecargaInternet clienteVentaRecInternet;
    private ConsumirVenta consumirVentaPagaTodoImp;
    private ImpresionVenta impresionVenta;
    private PeticionVentaDto peticionVentaDto;
    private PeticionVentaTADto peticionVentaTADto;
    private CompaniaBean compania;
    private PeticionDetalleVentaDto peticionDetalleVentaDto;
    private RespuestaDetalleVentaDto respuestaDetalleVentaDto;
    private RespuestaVentaDto respuestaVentaDto;

    
    private RespuestaVentaDto respuestaVentaTADto;
    private RespuestaConciliacionLocalBean respuestaConciliacionLocalBean;
    private ConciliacionLocalControlador conciliacionLocalControlador;
    private PeticionConciliacionLocalBean peticionConciliacion;
    private PeticionActTransaccionLocalBean peticionActTransaccionLocalBean;
    private RespuestaActualizacionLocalBean respuestaActualizacionLocalBean;
    //implementacion chip
    private ObservableList<OperacionTarjetaBean> listaPagosTarjeta = FXCollections.observableArrayList();
    private final int TARJETA_CREDITO = 1;
    private final int VALE_ELECTRONICO = 2;
    //fuera linea
    private PeticionVentaLocalBean peticionVentaLocalBean;
    private TipoPagoBean tipoPagoBean;
    //private VentaFueraLineaDAO ventaFueraLineaDAO;
    private RespuestaVentaLocalBean respuestaVentaLocalBean;
    private ArticuloFLBean[] articuloBeans;
    private TipoPagoBean[] tipoPagoBeans;
    //private ArticuloFLBean articuloBeanFueraLinea;
    //private TipoPagoBean tipoPagoBeanFueraLinea;
    private VentaFueraLineaControlador ventaFueraLineaControlador;
    private ConsumirTarjeta consumirTarjeta;
    private BloqueoVenta bloqueoVenta;
    private DetalleArticuloDao daoArticulos;
    
    ////objetos de respuesta y peticion para store de consulta de articulo
    private PeticionDetalleArticuloBean peticionArticuloLocalBean;
    private RespuestaDetalleArticuloBean respuestaArticuloLocalBean;
    
    ///objetos de peticion y respuesrta de insercion de articulso de venta para reporte
    private VentaDiaDao vendidosDao;
    private ArticuloVendidoBean[] articuloVendidoBeans;
    private PeticionArticulosVendidosBean peticionArticulosVendidosBean;
    private RespuestaArticulosVendidosBean respuestaArticulosVendidosBean;
    ///objetos de confirmacion de recarga
    private PeticionTransaccionCentralDto peticionTransaccionCentralDto;
    //private RespuestaConsultaTransaccionCentralDto respuestaConsultaTransaccionCentralDto;
    private RespuestaConsultaTransaccionTADto respuestaConsultaTransaccionTADto;
    ///objetos de peticion y respuesta de parametros de operacion
    private ParametroDao parametroDao;
    private PeticionParametroBean peticionParametroBean;
    private RespuestaParametroBean respuestaParametroBean;
    ///objetos de peticion y respuesta de cancelacion de la venta
    private LineasCanceladasDao cancelacionVentaDao;
    private PeticionCancelacionVentaBean peticionCancelacionVentaBean;
    private RespuestaCancelacionVentaBean respuestaCancelacionVentaBean;
    ///
    private ValidacionTarjetaDao validacionTarjetaDao;
    private PeticionValidacionTarjetaBean peticionValidacionTarjeta;
    private RespuestaValidacionTarjetaBean respuestaValidacionTarjeta;
    ///
    private ArrayList<ArticuloDto> listaOrdenada = new ArrayList<ArticuloDto>();
    private ArrayList<TipoPagoDto> listaTiposPago = new ArrayList<TipoPagoDto>();
    private final NumberFormat formateador = new DecimalFormat("#.00");
    private String terminal;
    private int pais;
    private long tiendaId;
    private long usuario;
    private long caja;
    private String ip;
    private String fecha;
    private ObservableList<ParametrosBean> listaParametros = FXCollections.observableArrayList();//parametros de la operacion
    //private ObservableList<TarjetasBean> listaTarjetas =
    //FXCollections.observableArrayList();//tarjetas aplicadas en la venta
    private ObservableList<ArticuloBean> listaArticulos = FXCollections.observableArrayList();//articulos que se encuentran en la tabla *todos
    private ObservableList<ArticuloBean> listaArticulosTA = FXCollections.observableArrayList();//solo debera contener 1 en venta TA
    private ObservableList<ArticuloBean> listaArticulosCancelados = FXCollections.observableArrayList();//articulos que se encuentran en la tabla *todos
    private ObservableList<neto.sion.tienda.venta.modelo.DatosTiendaBean> listaDatosTienda = FXCollections.observableArrayList();//datos de la tienda   *quitar
    private ObservableList<DatosFormasPagoBean> listaDatosFormasPago = FXCollections.observableArrayList();
    private ObservableList<String> listaDatosFormasPagoTarjeta = FXCollections.observableArrayList();//arreglo provisional * quitar
    private ObservableList<DatosTiempoAireBean> listaDatosTiempoAire = FXCollections.observableArrayList();//contiene los montos de recargas i sus compañias
    private ObservableList<String> listaUltimasTransaccionesRealizadas = FXCollections.observableArrayList();
    
    
    private boolean fueImpresionExitosa;
    private boolean fueVoucherExitoso;
    private boolean fueImpresionTaExitoso;
    private boolean existeAvisoBloqueo;
    private String mensajeAvisoBloqueo;
    private boolean existeBloqueo;
    private String mensajeBloqueo;
    private boolean existe2oAvisoBloqueo;
    private String mensaje2oAvisoBloqueo;
    private boolean hayMarcacionFueraLinea;
    private final int ACTUALIZACION_LOCAL;
    private final int ACTUALIZACION_CENTRAL;
    ////variables sesion
    private BloqueoVentaBean datosSesion;// = VistaBarraUsuario.getSesion().getBloqueosVenta();
    private MensajeBloqueoBean mensajeBloqueoAviso;// = bloqueoVenta.buscarBloqueo(datosSesion.getMensajeBloqueo(), VistaBarraUsuario.getSesion().getNombreUsuario());
    private boolean existenDatosSesion;
    private PeticionBloqueoDto peticionBloqueoDto;// = new PeticionBloqueoDto();
    private BloqueoBean[] datosBloqueo;// = datosSesion.getMensajeBloqueo();
    ////consulta de articulos
    //private ArticuloFLBean datosArticulosBean;// = storeDatosArticulosVenta.consultaArticulo(cadena.trim(), cantidad, 0);
    private UsuarioSesionBean sesion;// = VistaBarraUsuario.getSesion();
    ///id de ventana
    private final int VENTANA_VENTA;
    //bloqueos fuera de linea
    private BloqueosOperadorLocal bloqueoLocal;
    private List<ObjetoBloqueo> arregloBloqueosLocal;
    private boolean existeBloqueoLocal = false;
    private boolean existeAvisoLocal = false;
    private int tipoBloqueo = 0;
    private int avisosRestantesBloqueoLocal = 0;
    //timed out de la venta
    private long timedOut;
    ///componente chip
    private ControladorTarjeta controladorPinpad;
    private int numeroVentasFL = 0;
    //inserción de artículos por método de entrada
    private PeticionMetodosEntradaArticuloBean peticionMetodoEntrada;
    private RespuestaMetodosEntradaArticuloBean respuestaMetodoEntrada;
    private MetodoEntradaArticuloDao metodosEntradaArticulosDao;
    
    private ControladorTarjetasDescuento ctrlTarjetasDescuento;
    
    
    public boolean esRecargaInternet;

    public ControladorTarjetasDescuento getCtrlTarjetasDescuento() {
        return ctrlTarjetasDescuento;
    }
    
    
    
    public boolean getForzarFueraLineaPAGATODO() {
        return respuestaVentaDto.getPaCdgError() == -100;
    }


    public VentaVistaModelo(Utilerias _utilerias) {
        ctrlTarjetasDescuento = new ControladorTarjetasDescuento();
        this.utilerias = _utilerias;
        this.parametrosDevolucionesDAO = new ParametrosDao();

        this.peticionVentaDto = new PeticionVentaDto();
        this.peticionVentaTADto = new PeticionVentaTADto();
        this.compania = new CompaniaBean();
        this.peticionDetalleVentaDto = new PeticionDetalleVentaDto();
        this.respuestaDetalleVentaDto = new RespuestaDetalleVentaDto();
        this.respuestaVentaDto = new RespuestaVentaDto();
        this.respuestaVentaTADto = new RespuestaVentaDto();
        this.bloqueoVenta = new BloqueoVenta();
        this.impresionVenta = new ImpresionVenta();
        this.daoArticulos = new DetalleArticuloDao(utilerias);

        this.peticionConciliacion = new PeticionConciliacionLocalBean();
        this.respuestaConciliacionLocalBean = new RespuestaConciliacionLocalBean();
        this.conciliacionLocalControlador = new ConciliacionLocalControlador();
        this.peticionActTransaccionLocalBean = new PeticionActTransaccionLocalBean();
        this.respuestaActualizacionLocalBean = new RespuestaActualizacionLocalBean();

        //fuera linea
        this.peticionVentaLocalBean = new PeticionVentaLocalBean();
        this.tipoPagoBean = new TipoPagoBean();
        //this.ventaFueraLineaDAO = new VentaFueraLineaDAO();
        this.respuestaVentaLocalBean = new RespuestaVentaLocalBean();

        //this.tipoPagoBeanFueraLinea = new TipoPagoBean();
        this.articuloBeans = null;
        this.tipoPagoBeans = null;
        this.ventaFueraLineaControlador = new VentaFueraLineaControlador();

        /////objetos consutla de articulo local
        this.peticionArticuloLocalBean = null;
        this.respuestaArticuloLocalBean = null;
        
        ///objetos de peticion y respuesrta de insercion de articulso de venta para reporte
        this.vendidosDao = new VentaDiaDao();
        this.articuloVendidoBeans = null;
        this.peticionArticulosVendidosBean = null;
        this.respuestaArticulosVendidosBean = null;
        ///objetos de confirmacion de recarga
        this.peticionTransaccionCentralDto = new PeticionTransaccionCentralDto();
        this.respuestaConsultaTransaccionTADto = new RespuestaConsultaTransaccionTADto();
        //this.respuestaConsultaTransaccionCentralDto = new RespuestaConsultaTransaccionCentralDto();
        ///objetos de peticion y respuesta de parametros de operacion
        this.parametroDao = new ParametroDao();
        this.peticionParametroBean = null;
        this.respuestaParametroBean = null;
        ///objetos de peticion y respuesta de cancelacion de la venta
        this.cancelacionVentaDao = new LineasCanceladasDao();
        this.peticionCancelacionVentaBean = null;
        this.respuestaCancelacionVentaBean = null;
        ////
        this.validacionTarjetaDao = new ValidacionTarjetaDao();
        this.peticionValidacionTarjeta = new PeticionValidacionTarjetaBean();
        this.respuestaValidacionTarjeta = new RespuestaValidacionTarjetaBean();
        ///
        this.fueVoucherExitoso = false;
        this.fueImpresionTaExitoso = false;
        this.fueImpresionExitosa = false;

        this.existeAvisoBloqueo = false;
        this.mensajeAvisoBloqueo = "";
        this.existeBloqueo = false;
        this.mensajeBloqueo = "";
        this.existe2oAvisoBloqueo = false;
        this.mensaje2oAvisoBloqueo = "";

        this.hayMarcacionFueraLinea = false;

        this.timedOut = obtenerTimedOut();

        try {
            this.consumirVentaImp = new ConsumirVentaImp(timedOut);
            this.clienteVentaRecInternet = new ClienteWSRecargaInternet();
            //this.consumirVentaPagaTodoImp = new ConsumirVentaImp(timedOut);
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error al crear instancia de consumo de venta", Level.INFO);
            SION.logearExcepcion(Modulo.VENTA, ex);
        }

        try {
            consumirTarjeta = new ConsumirTarjetaImp();
        } catch (Exception ex) {
            SION.log(Modulo.VENTA, "Ocurriò un error al crear instancia de consumo de venta con tarjeta", Level.INFO);
            SION.logearExcepcion(Modulo.VENTA, ex);
        }

        this.ACTUALIZACION_LOCAL = 0;
        this.ACTUALIZACION_CENTRAL = 1;

        //establecerDatosSesion();
        //actualizarListaTransacciones();
        this.datosSesion = null;
        this.mensajeBloqueoAviso = null;
        this.existenDatosSesion = false;
        this.peticionBloqueoDto = new PeticionBloqueoDto();
        this.datosBloqueo = null;//datosSesion.getMensajeBloqueo();
        ////
        //this.datosArticulosBean = null;//storeDatosArticulosVenta.consultaArticulo(cadena.trim(), cantidad, 0);
        this.sesion = null;//VistaBarraUsuario.getSesion();
        ////escaneo de articulos
        this.VENTANA_VENTA = 0;

        ///bloqueos fuera de linea
        this.bloqueoLocal = new BloqueosOperadorLocal(Modulo.VENTA);
        this.arregloBloqueosLocal = null;

        //componente chip
        this.controladorPinpad = null;

        //metodos de entrada
        this.metodosEntradaArticulosDao = new MetodoEntradaArticuloDao();

        this.ctrlTarjetasDescuento = new ControladorTarjetasDescuento();
        
        this.esRecargaInternet =false;
    }

    public long obtenerTimedOut() {
        long tiempo = 0;
        
        llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.timedout.sistema")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.timedout.modulo")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.timedout.submodulo")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.timedout.configuracion")));
        
        //respuestaParametro = utilerias.consultaParametro(UtileriasVenta.Accion.TIME_OUT_VENTA, Modulo.VENTA, Modulo.VENTA);

        /*llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.timedout.sistema")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.timedout.modulo")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.timedout.submodulo")),
                Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.timedout.configuracion")));*/

    if (consultarParametro() != null) {
             if (getCodigoErrorParametroOperacion() == 0)
			 {
                SION.log(Modulo.VENTA, "parametro de timed out encontrado", Level.INFO);
                if (getValorConfiguracionParametroOperacion() > 0) {
                

                    tiempo = (long) getValorConfiguracionParametroOperacion();
                

                }
            } else {
                SION.log(Modulo.VENTA, "Parametro de timed out desactivado, se asigna desde archivo de propiedades", Level.INFO);
            }
        }

        return tiempo;
    }

    //////////////////////////////////////////////////////////getters listas
    public ObservableList<OperacionTarjetaBean> getListaPagosTarjeta() {
        return listaPagosTarjeta;
    }

    public ObservableList<ArticuloBean> getListaArticulosTA() {
        return listaArticulosTA;
    }
    

    /*
     * public ObservableList<TarjetasBean> getListaTarjetas() { return
     * listaTarjetas; }
     */
    public ObservableList<ArticuloBean> getListaArticulos() {
        return listaArticulos;
    }

    public ObservableList<neto.sion.tienda.venta.modelo.DatosTiendaBean> getListaDatosTienda() {
        return listaDatosTienda;
    }

    public ObservableList<DatosFormasPagoBean> getListaFormasPago() {
        return listaDatosFormasPago;
    }

    public double getCantidadArticulos() {
        int contador = 0;
        int i = 0;

        while (i < listaArticulos.size()) {
            contador += listaArticulos.get(i).getCantidad();
            i++;
        }

        return contador;
    }

    public double getCantidadArticulosDevueltos() {
        double contador = 0;
        int i = 0;

        while (i < listaArticulos.size()) {
            if (listaArticulos.get(i).getDevueltos() > 0) {
                contador += listaArticulos.get(i).getDevueltos();
            }
            i++;
        }

        return contador;
    }

    public TarjetasBean getRegistroTarjeta(int idTarjeta, String autorizacion, int cantidadTarjetas) {
        TarjetasBean registroTarjetas = new TarjetasBean("", "", "", "", "", "", "", "", "", "", 0.0, 0.0, "", "", "", "","", "", 0, "", 0, "", "");
        boolean seEncontro = false;
        int index = 0;
        int i = 0;
        int tarjetasContadas = 0;
        int posicionesLibresTarjetas = 0;
        int posicionesLibresVales = 0;


        for (Iterator<OperacionTarjetaBean> it = listaPagosTarjeta.iterator(); it.hasNext();) {
            OperacionTarjetaBean operacionTarjetaBean = (OperacionTarjetaBean)it.next();
            if (idTarjeta == 1) {
                if (operacionTarjetaBean.getRespuestaPago().getTipoTarjeta() == 1 || operacionTarjetaBean.getRespuestaPago().getTipoTarjeta() == 2) {
                    tarjetasContadas++;
                    if (String.valueOf(operacionTarjetaBean.getRespuestaPago().getNumeroAutorizacion()).equals(autorizacion)) {
                        seEncontro = true;
                        index = i;

                    }
                }
            } else if (idTarjeta == 0 && operacionTarjetaBean.getRespuestaPago().getTipoTarjeta() == 3) {
                tarjetasContadas++;
                if (String.valueOf(operacionTarjetaBean.getRespuestaPago().getNumeroAutorizacion()).equals(autorizacion)) {
                    seEncontro = true;
                    index = i;

                }
            }
            i++;
            //tarjetasContadas++;
        }

        seEncontro = false;

        if (index <= cantidadTarjetas - 2) {
            for (i = index + 1; i < cantidadTarjetas; i++) {
                if (idTarjeta == 1) {
                    if (listaPagosTarjeta.get(i).getRespuestaPago().getTipoTarjeta() == 1 || listaPagosTarjeta.get(i).getRespuestaPago().getTipoTarjeta() == 2) {
                        index = i;
                        seEncontro = true;
                        break;
                    }
                } else {
                    if (listaPagosTarjeta.get(i).getRespuestaPago().getTipoTarjeta() == 3) {
                        index = i;
                        seEncontro = true;
                        break;
                    }
                }
            }
        }

        if (!seEncontro) {
            for (i = 0; i <= index; i++) {
                if (idTarjeta == 1) {
                    if (listaPagosTarjeta.get(i).getRespuestaPago().getTipoTarjeta() == 1 || listaPagosTarjeta.get(i).getRespuestaPago().getTipoTarjeta() == 2) {
                        index = i;
                        break;
                    }
                } else {
                    if (listaPagosTarjeta.get(i).getRespuestaPago().getTipoTarjeta() == 3) {
                        index = i;
                        break;
                    }
                }
            }
        }


        registroTarjetas.setAutorizacion(String.valueOf(listaPagosTarjeta.get(index).getRespuestaPago().getNumeroAutorizacion()));
        registroTarjetas.setDesctarjeta(listaPagosTarjeta.get(index).getRespuestaPago().getDescTarjeta());
        registroTarjetas.setNumerotarjeta(listaPagosTarjeta.get(index).getRespuestaPago().getNumerotarjeta());
        registroTarjetas.setMonto(listaPagosTarjeta.get(index).getPeticionPago().getMonto());

        return registroTarjetas;
    }

    public String getIp() {
        return VistaBarraUsuario.getSesion().getIpEstacion();
    }

    public long getTransaccionTA() {
        return respuestaVentaTADto.getPaTransaccionId();
    }

    public long getTransaccion() {
        return respuestaVentaDto.getPaTransaccionId();
    }

    public int getErrorVentaTA() {
        return respuestaVentaTADto.getPaCdgError();
    }

    public int getErrorVenta() {
        return respuestaVentaDto.getPaCdgError();
    }

    public String getDescErrorVenta() {
        return respuestaVentaDto.getPaDescError();
    }

    public String getDescErrorVentaTA() {
        //respuestaVentaTADto = new RespuestaVentaDto();
        //respuestaVentaTADto.setPaDescError("Error al actualizar crédito");
        return respuestaVentaTADto.getPaDescError();
    }

    public int getTamanioListaArticulos() {
        return listaArticulos.size();
    }

    public int getTamanioListaArticulosTA() {
        return listaArticulosTA.size();
    }

    /*
     * public int getTamanioListaTarjetas() { return listaTarjetas.size(); }
     */
    public String getArticuloCodigoBarras(int index) {
        return listaArticulos.get(index).getPacdgbarras();
    }

    public String getArticuloNombreArticulo(int index) {
        return listaArticulos.get(index).getPanombrearticulo();
    }

    public double getArticuloImporte(int index) {
        return listaArticulos.get(index).getImporte();
    }

    public double getArticuloCantidad(int index) {
        return listaArticulos.get(index).getCantidad();
    }

    public double getArticuloUnidad(int index) {
        return listaArticulos.get(index).getPaunidad();
    }

    public double getArticuloPrecio(int index) {

        return listaArticulos.get(index).getPaprecio();

    }
    
    public boolean esArticuloPromocion(int index, TiposDescuento tpoDescto) {
        return listaArticulos.get(index).getTipoDescuento() == tpoDescto.ordinal();
    }

    public void registrarVentaTA() throws SionException {
        
        SION.log(Modulo.VENTA, "registrarVentaTA Compañia: " + peticionVentaTADto.getCompaniaTelefonicaId(),Level.INFO);
        
        
        if (esRecargaInternet && peticionVentaTADto.getCompaniaTelefonicaId()==8){
       
            
            String nombreCompania = "";
            int companiaRecargale = 0;
            
            SION.log(Modulo.VENTA, "Enviando petición de Venta de Recarga de internet Recargale", Level.INFO);
            

            respuestaVentaTADto = null;
            respuestaVentaTADto = new RespuestaVentaDto();

            
            PeticionVentaRecInternetDto peticionRI = new PeticionVentaRecInternetDto();
            
            
            ArticuloRIDto[] arrArticulos = new ArticuloRIDto[peticionVentaTADto.getArticulos().length];
            ArticuloRIDto artDto;
            
            SION.log(Modulo.VENTA,"size " + peticionVentaTADto.getArticulos().length, Level.INFO);
            
            
            for(int i = 0 ; i < peticionVentaTADto.getArticulos().length;i ++){
                
                artDto = new ArticuloRIDto();
		artDto.setFcCdGbBarras(peticionVentaTADto.getArticulos()[i].getFcCdGbBarras());
		artDto.setFcNombreArticulo(peticionVentaTADto.getArticulos()[i].getFcNombreArticulo());
		artDto.setFiAgranel(peticionVentaTADto.getArticulos()[i].getFiAgranel());
		artDto.setFiArticuloId(peticionVentaTADto.getArticulos()[i].getFiArticuloId());
		artDto.setFnCantidad(peticionVentaTADto.getArticulos()[i].getFnCantidad());
		artDto.setFnCosto(peticionVentaTADto.getArticulos()[i].getFnCosto());
		artDto.setFnDescuento(peticionVentaTADto.getArticulos()[i].getFnDescuento());
		artDto.setFnIva(peticionVentaTADto.getArticulos()[i].getFnIva());
		artDto.setFnPrecio(peticionVentaTADto.getArticulos()[i].getFnPrecio());
                
                nombreCompania = peticionVentaTADto.getArticulos()[i].getFcNombreArticulo();
                  
                arrArticulos[i] = artDto;
                
            }
            
            
            peticionRI.setArticulos(arrArticulos);
            peticionRI.setCompaniaTelefonicaId(peticionVentaTADto.getCompaniaTelefonicaId());
            peticionRI.setFueraLinea(peticionVentaTADto.getFueraLinea());
            peticionRI.setNumFolio(peticionVentaTADto.getNumFolio());
            peticionRI.setPaConciliacionId(peticionVentaTADto.getPaConciliacionId());
            peticionRI.setPaFechaOper(peticionVentaTADto.getPaFechaOper());
            peticionRI.setPaMontoTotalVta(peticionVentaTADto.getPaMontoTotalVta());
            peticionRI.setPaPaisId(peticionVentaTADto.getPaPaisId());
            peticionRI.setPaTerminal(peticionVentaTADto.getPaTerminal());
            peticionRI.setPaTipoMovto(peticionVentaTADto.getPaTipoMovto());
            peticionRI.setPaTransaccionDev(peticionVentaTADto.getPaTransaccionDev());
            peticionRI.setPaUsuarioId(peticionVentaTADto.getPaUsuarioId());
            peticionRI.setTelefono(peticionVentaTADto.getTelefono());
            peticionRI.setTiendaId(peticionVentaTADto.getTiendaId());
            
            
            if (peticionVentaTADto.getTiposPago()!=null){
                SION.log(Modulo.VENTA, " getTiposPago OK", Level.INFO);
                TipoPagoRIDto[] tiposPagoRI = new TipoPagoRIDto[peticionVentaTADto.getTiposPago().length];
                TipoPagoRIDto tipoPagoRI;
                for(int i=0; i<peticionVentaTADto.getTiposPago().length; i++){
                    tipoPagoRI = new TipoPagoRIDto();

                    tipoPagoRI.setFiTipoPagoId(peticionVentaTADto.getTiposPago()[i].getFiTipoPagoId());
                    tipoPagoRI.setFnMontoPago(peticionVentaTADto.getTiposPago()[i].getFnMontoPago());
                    tipoPagoRI.setFnNumeroVales(peticionVentaTADto.getTiposPago()[i].getFnNumeroVales());
                    tipoPagoRI.setPaPagoTarjetaIdBus(peticionVentaTADto.getTiposPago()[i].getPaPagoTarjetaIdBus());
                    tipoPagoRI.setImporteAdicional(peticionVentaTADto.getTiposPago()[i].getImporteAdicional());

                    tiposPagoRI[i]=tipoPagoRI;
                }

                peticionRI.setTiposPago(tiposPagoRI);
            }else{
                SION.log(Modulo.VENTA, "getTiposPago NULL", Level.INFO);
            }
            
           
           if(nombreCompania.toUpperCase().contains("TELCEL")){
               companiaRecargale = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "RECARGA.INTERNET.TELCEL.ID"));
           }
           
           if(nombreCompania.toUpperCase().contains("MOVISTAR")){
               companiaRecargale = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "RECARGA.INTERNET.MOVISTAR.ID"));
           }
            
           SION.log(Modulo.VENTA, "COMPANIAREC : " + nombreCompania, Level.INFO);
           RespuestaVentaRecInternetDto respuestaVentaRIDto =  clienteVentaRecInternet.registrarVentaRecInternet(peticionRI,companiaRecargale);
            
           respuestaVentaTADto = new RespuestaVentaDto();
            
           respuestaVentaTADto.setPaCdgError(respuestaVentaRIDto.getPaCdgError());
           respuestaVentaTADto.setPaDescError(respuestaVentaRIDto.getPaDescError());
           respuestaVentaTADto.setPaTaNumOperacion(respuestaVentaRIDto.getPaTaNumOperacion());
           respuestaVentaTADto.setPaTransaccionId(respuestaVentaRIDto.getPaTransaccionId());
           respuestaVentaTADto.setTiendaId(respuestaVentaRIDto.getTiendaId());
            
           if(respuestaVentaRIDto.getPaTypCursorBlqs()!=null){
                BloqueoDto[] bloqueos = new BloqueoDto[respuestaVentaRIDto.getPaTypCursorBlqs().length];
                BloqueoDto bloqueo;
                for(int i= 0 ; i<respuestaVentaRIDto.getPaTypCursorBlqs().length; i++){
                    bloqueo = new BloqueoDto();

                    bloqueo.setFiAvisosFalt(respuestaVentaRIDto.getPaTypCursorBlqs()[i].getFiAvisosFalt());
                    bloqueo.setFiEstatusBloqueoId(respuestaVentaRIDto.getPaTypCursorBlqs()[i].getFiEstatusBloqueoId());
                    bloqueo.setFiNumAvisos(respuestaVentaRIDto.getPaTypCursorBlqs()[i].getFiNumAvisos());
                    bloqueo.setFiTipoPagoId(respuestaVentaRIDto.getPaTypCursorBlqs()[i].getFiTipoPagoId());

                    bloqueos[i] = bloqueo;
                }
            
           respuestaVentaTADto.setPaTypCursorBlqs(bloqueos);
           }
            
            
            SION.log(Modulo.VENTA, "Respuesta de recarga Internet recibida: " + respuestaVentaTADto.toString(), Level.INFO);
            
        }else{
            
            SION.log(Modulo.VENTA, "Enviando petición de Venta de Tiempo Aire", Level.INFO);

            respuestaVentaTADto = null;
            respuestaVentaTADto = new RespuestaVentaDto();
            
           

            SION.log(Modulo.VENTA, "Peticion de recarga TA: " + peticionVentaTADto.toString(), Level.INFO);
            respuestaVentaTADto = consumirVentaImp.registrarVentaTA(peticionVentaTADto);
            SION.log(Modulo.VENTA, "Respuesta de recarga TA recibida: " + respuestaVentaTADto.toString(), Level.INFO);
        }

        

    }

    public void registrarVenta() throws SionException {
        SION.log(Modulo.VENTA, "Enviando petición de Venta", Level.INFO);
        respuestaVentaDto = null;
        respuestaVentaDto = new RespuestaVentaDto();
        respuestaVentaDto = consumirVentaImp.registrarVenta(peticionVentaDto);
        SION.log(Modulo.VENTA, "Respuesta a la petición de venta recibida exitosamente : " + respuestaVentaDto.toString(), Level.INFO);
    }
    
    public void registrarVentaPagaTodo() throws SionException 
    {
        //Venta normal
        SION.log(Modulo.VENTA, "Enviando petición de Venta PAGA TODO", Level.INFO);
        respuestaVentaDto = null;
        respuestaVentaDto = new RespuestaVentaDto();
        //RespuestaPagoTarjetaDto respuestaVentatarDto=null;
        //boolean ventaCompletada = false;
        
        //Intento de pago PAGA TODO
        try{
            
            
            ConsumirTarjetaPT_ISO8583NIP clientePagatodoNip = new ConsumirTarjetaPT_ISO8583NIPImp();
            
            
            obtenerTarjetaPagaTodo().getSolicitudPagaTdo().setNoTicket( ""+getTransaccion() );
            obtenerTarjetaPagaTodo().getSolicitudPagaTdo().setIdOperador( Long.toString(VistaBarraUsuario.getSesion().getUsuarioId()) );
            //obtenerTarjetaPagaTodo().getSolicitudPagaTdo().setNip("1234");
            
            SION.log(Modulo.VENTA, "Inicia envío de informacion a WS Paga todo...", Level.INFO);
            
            SION.log(Modulo.VENTA, "normal: "+peticionVentaDto, Level.INFO);
            SION.log(Modulo.VENTA, "ptdo: "+obtenerTarjetaPagaTodo().getSolicitudPagaTdo(), Level.INFO);
            SION.log(Modulo.VENTA, "tarjeta: "+obtenerTarjetaPagaTodo().getPeticionPagoTarjeta(), Level.INFO);
            
            
            neto.sion.venta.tarjeta.pt.iso8583.nip.dto.RespuestaCombinadaVentaDto respCombinadaVenta = 
                    clientePagatodoNip.registrarVentaTarjetaPagaTodo(obtenerTarjetaPagaTodo().getPeticionPagoTarjeta() , 
                                                                     peticionVentaDto, 
                                                                     obtenerTarjetaPagaTodo().getSolicitudPagaTdo());
            
            
            if( respCombinadaVenta != null ){
                SION.log(Modulo.VENTA, "getRespuestaVentaNormal: "+respCombinadaVenta.getRespuestaVentaNormal(), Level.INFO);
                if( respCombinadaVenta.getRespuestaVentaNormal() != null ){
                    //respuestaVentaDto = respCombinadaVenta.getRespuestaVentaNormal();
                    respuestaVentaDto.setPaCdgError(respCombinadaVenta.getRespuestaVentaNormal().getPaCdgError());
                    respuestaVentaDto.setPaDescError(respCombinadaVenta.getRespuestaVentaNormal().getPaDescError());
                    
                    respuestaVentaDto.setPaTaNumOperacion(respCombinadaVenta.getRespuestaVentaNormal().getPaTaNumOperacion());
                    respuestaVentaDto.setPaTransaccionId(respCombinadaVenta.getRespuestaVentaNormal().getPaTransaccionId());
                    
                    List<neto.sion.venta.servicios.cliente.dto.BloqueoDto> bloqueos = new ArrayList<neto.sion.venta.servicios.cliente.dto.BloqueoDto>();
                    if( respCombinadaVenta.getRespuestaVentaNormal().getPaTypCursorBlqs() != null ){                                            
                        for ( neto.sion.venta.tarjeta.pt.iso8583.nip.dto.BloqueoDto bloqueo : 
                                respCombinadaVenta.getRespuestaVentaNormal().getPaTypCursorBlqs()){
                            BloqueoDto bloqueox = new BloqueoDto();
                            bloqueox.setFiAvisosFalt(bloqueo.getFiAvisosFalt());
                            bloqueox.setFiEstatusBloqueoId(bloqueo.getFiEstatusBloqueoId());
                            bloqueox.setFiNumAvisos(bloqueo.getFiNumAvisos());
                            bloqueox.setFiTipoPagoId(bloqueo.getFiTipoPagoId());
                            //bloqueox.setTiendaId(bloqueo.get);
                            bloqueos.add(bloqueox);
                        }
                    }
                    respuestaVentaDto.setPaTypCursorBlqs(bloqueos.toArray(new neto.sion.venta.servicios.cliente.dto.BloqueoDto[0]));
                    
                    
                    long confirmacionIdPagatodo = respuestaVentaDto.getPaTaNumOperacion();
                    for(int i=0; i<getListaPagosTarjeta().size(); i++){
                        
                        //if( getListaPagosTarjeta().get(i).getRespuestaPago().getSolicitudPagaTdo() != null ){
                        SION.log(Modulo.VENTA, "getDescTarjeta" +  getListaPagosTarjeta().get(i).getRespuestaPago().getDescTarjeta(), Level.INFO);
                        if( getListaPagosTarjeta().get(i).getRespuestaPago().getDescTarjeta().equals("PAGATODO") ){
                            
                            if (respCombinadaVenta.getRespuestaTarjetaPTDto()!=null){
                                getListaPagosTarjeta().get(i).getRespuestaPago().setNumeroAutorizacion("Aut:"+respCombinadaVenta.getRespuestaTarjetaPTDto().getrNoConfirmacion()+"/ IDPT: " + respCombinadaVenta.getRespuestaTarjetaPTDto().getrNoSecUnicoPT());
                                getListaPagosTarjeta().get(i).getRespuestaPago().setProgramaGob(respCombinadaVenta.getRespuestaTarjetaPTDto().getrDescMsgObligatorio());
                                
                                
                            }else{
                               getListaPagosTarjeta().get(i).getRespuestaPago().setNumeroAutorizacion( String.valueOf(confirmacionIdPagatodo)); 
                            }
                            
                        }
                        
                        //getListaPagosTarjeta().get(i).getRespuestaPago().setNumeroAutorizacion( String.valueOf(confirmacionIdPagatodo)); 
                        
                    }
                    respuestaVentaDto.setPaTaNumOperacion(0);
                }
                SION.log(Modulo.VENTA, "RespuestaVentaTarjeta: "+respCombinadaVenta.getRespuestaVentaTarjeta(), Level.INFO);
                
            }else{
                SION.log(Modulo.VENTA, "Respuesta combinada nula...", Level.INFO);
            }
            
            SION.log(Modulo.VENTA, "Consumo de WS paga todo completado...", Level.INFO);
        }catch(Exception e){
            respuestaVentaDto = new RespuestaVentaDto();
            respuestaVentaDto.setPaCdgError(-1);
            respuestaVentaDto.setPaDescError("No fue posible realizar la venta, se produjo un error en la comunicación con PAGA TODO.");
            SION.logearExcepcion(Modulo.VENTA, e, "WSError al consultar el cliente > "+e.getMessage()+ " >> "+getStackTrace(e));
        }catch(Error e){
            respuestaVentaDto = new RespuestaVentaDto();
            respuestaVentaDto.setPaCdgError(-1);
            respuestaVentaDto.setPaDescError("No fue posible realizar la venta, se produjo un error en la comunicación con PAGA TODO.");
            SION.logearExcepcion(Modulo.VENTA, new Exception(e), "Error al consultar el cliente > "+e.getMessage()+ " >> "+getStackTrace(e));
        }
    }
    
    public static String getStackTrace(Throwable aThrowable) {
            Writer result = new StringWriter();
            PrintWriter printWriter = new PrintWriter(result);
            aThrowable.printStackTrace(printWriter);
            return result.toString();
    }

    public void limpiarDatosVenta() {
        peticionVentaDto = null;
        respuestaVentaDto = null;
    }

    public void imprimir(int tipoTicket, double efectivoCambio, double montoRecibido, boolean esVentaFueraLinea, long conciliacion, boolean esConfirmacionRecarga) {

		SION.log(Modulo.VENTA, "Imprimiendo ticket", Level.INFO);
        boolean esSuperPrecio = false;
        
        llenarPeticionParametro(
                   VistaBarraUsuario.getSesion().getPaisId(), 
                   Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ticket.superprecio.sistema")), 
                   Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ticket.superprecio.modulo")), 
                   Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ticket.superprecio.submodulo")), 
                   Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ticket.superprecio.configuracion")));

        if ((consultarParametro() != null) && 
	      (getCodigoErrorParametroOperacion() == 0) && 
	      (getValorConfiguracionParametroOperacion() != 1.0D)) {
	      esSuperPrecio = true;
	    }

        fueImpresionExitosa = false;
        fueImpresionTaExitoso = false;
        fueVoucherExitoso = false;

        String calleNumeroTienda = "";
        calleNumeroTienda = calleNumeroTienda.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getCalle()));
        calleNumeroTienda = calleNumeroTienda.concat(" ");
        calleNumeroTienda = calleNumeroTienda.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroExterior()));
        if (!"".equals(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior()) && !"0".equals(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior()) && VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior() != null) {
            calleNumeroTienda = calleNumeroTienda.concat("-");
            calleNumeroTienda = calleNumeroTienda.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior()));
        }

        String delegacionCiudadEstado = "";
        delegacionCiudadEstado = delegacionCiudadEstado.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getMunicipio()).concat(" ").concat( String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getEstado())).concat(" ").concat( String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getCodigoPostal())));

        if (tipoTicket == TiposImpresion.TICKET_NORMAL || 
                tipoTicket == TiposImpresion.TICKET_PAGOS_ELETRONICOS) 
        {

            SION.log(Modulo.VENTA, "Tipo de ticket : Venta Gral.", Level.INFO);

            VentaNormalTicketDto ticket = new VentaNormalTicketDto();
            ticket.setCaja(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));
            ticket.setCajero(String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()));
            ticket.setCalleNumeroTienda(calleNumeroTienda);
            ticket.setColoniaTienda(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getColonia()));
            ticket.setDelegacionCiudadEstadoCP(delegacionCiudadEstado);
            String fechaD = obtenerFecha();
            ticket.setFecha(fechaD.substring(0, 10));
            ticket.setHora(fechaD.substring(10));
            if (esSuperPrecio) {
                ticket.setEncabezado(SION.obtenerMensaje(Modulo.VENTA, "ENCABEZADO.TICKET.SUPR"));
            } else {
                ticket.setEncabezado(SION.obtenerMensaje(Modulo.VENTA, "ENCABEZADO.TICKET"));
            }

            if (esVentaFueraLinea) {
                ticket.setFolio(String.valueOf(conciliacion).concat("O"));
            } else {
                ticket.setFolio(String.valueOf(respuestaVentaDto.getPaTransaccionId()).concat("I"));
            }
            ticket.setNombreTienda(String.valueOf(VistaBarraUsuario.getSesion().getNombreTienda()));
            ticket.setTiendaId(String.valueOf(VistaBarraUsuario.getSesion().getTiendaId()));
            ticket.setTransaccion(String.valueOf(respuestaVentaDto.getPaTransaccionId()));
            ticket.setArticulos(impresionVenta.articulosDtoaTicket(listaOrdenada));
            
            SION.log(Modulo.VENTA, "Lista ordenadat: " + listaOrdenada.toString(), Level.INFO);
            
            double importeDescuento = 0;
            for(ArticuloDto a : listaOrdenada){
                importeDescuento += (a.getFnDescuento() * a.getFnCantidad());
            }
                        
            SION.log(Modulo.VENTA, ">> "+ Arrays.toString( listaTiposPago.toArray(new TipoPagoDto[listaTiposPago.size()]) ), 
                    Level.INFO);
            
            ticket.setTiposPago(impresionVenta.tipoPagoDtoaTicket(listaTiposPago, getImporteVenta(), montoRecibido, efectivoCambio, importeDescuento));
            ticket.setTotalArticulos();
            ticket.setTotalLetras(String.valueOf(impresionVenta.getTotalLetras()));
            ticket.setIvaTotal();

            SION.log(Modulo.VENTA, "Datos del ticket: " + ticket.toString(), Level.INFO);

            try {
                SION.log(Modulo.VENTA, "Intentando imprimir ticket de venta ", Level.INFO);
                impresionVenta.imprimirTicketVenta(ticket);
                fueImpresionExitosa = true;
            } catch (Exception e) {
                SION.logearExcepcion(Modulo.VENTA, e, "Ocurrió una excepcion al imprimir el ticket de venta : "+getStackTrace(e));
            } catch (Error e) {
                SION.log(Modulo.VENTA, "ocurrio un error al imprimir ticket: " + getStackTrace(e), Level.SEVERE);
            }
            SION.log(Modulo.VENTA, "Impresión de ticket de venta exitoso? " + fueImpresionExitosa, Level.INFO);
            //
            if (tipoTicket == TiposImpresion.TICKET_PAGOS_ELETRONICOS) {

                SION.log(Modulo.VENTA, "Se procede a llenar voucher de tarjetas encontradas en el arreglo de tipos de pago", Level.FINEST);
                //listaTiposPago.get(0).
                for (int j = 0; j < listaPagosTarjeta.size(); j++) {

                    VentaTarjetaVoucher voucher = new VentaTarjetaVoucher();
                    voucher.setCaja(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));
                    voucher.setCajero(String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()));
                    voucher.setCalleNumeroTienda(calleNumeroTienda);
                    voucher.setColoniaTienda(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getColonia()));
                    voucher.setDelegacionCiudadEstadoCP(delegacionCiudadEstado);
                    voucher.setFecha(fechaD.substring(0, 10));
                    voucher.setHora(fechaD.substring(10));
                    if (esVentaFueraLinea) {
                        voucher.setFolio(String.valueOf(conciliacion).concat("O"));
                    } else {
                        voucher.setFolio(String.valueOf(respuestaVentaDto.getPaTransaccionId()).concat("I"));
                    }
                    voucher.setNombreTienda(String.valueOf(VistaBarraUsuario.getSesion().getNombreTienda()));
                    voucher.setTiendaId(String.valueOf(VistaBarraUsuario.getSesion().getTiendaId()));
                    String tipo = listaPagosTarjeta.get(j).getRespuestaPago().getDescTarjeta();
                    //String tipo = "Tarjeta:";
                    voucher.setTipoTarjeta(tipo);
                    if (esSuperPrecio) {
                        voucher.setEncabezado(SION.obtenerMensaje(Modulo.VENTA, "ENCABEZADO.TICKET.SUPR"));
                    } else {
                        voucher.setEncabezado(SION.obtenerMensaje(Modulo.VENTA, "ENCABEZADO.TICKET"));
                    }
                    //voucher.setTipoTarjeta(InicioVistaModelo.listaTarjetas.get(j).getTipotarjeta());
                    String numeroTarjeta = listaPagosTarjeta.get(j).getRespuestaPago().getNumerotarjeta();

                    voucher.setNumeroTarjeta(numeroTarjeta.substring(numeroTarjeta.length() - 4));
                    
                    if(voucher.getTipoTarjeta().equals("PAGATODO")){
                        voucher.setAfiliacion(listaPagosTarjeta.get(j).getRespuestaPago().getProgramaGob());
                    }else{
                       voucher.setAfiliacion(listaPagosTarjeta.get(j).getRespuestaPago().getPeticionReverso().getAfiliacion()); 
                    }
                    
                    
                    voucher.setAutorizacion(String.valueOf(listaPagosTarjeta.get(j).getRespuestaPago().getNumeroAutorizacion()));
                    voucher.setImporte(String.valueOf(listaPagosTarjeta.get(j).getPeticionPago().getMonto() + listaPagosTarjeta.get(j).getRespuestaPago().getPeticionReverso().getComision()));
                    voucher.setTotalLetras(String.valueOf(listaPagosTarjeta.get(j).getPeticionPago().getMonto() + listaPagosTarjeta.get(j).getRespuestaPago().getPeticionReverso().getComision()));
                    voucher.setTitularCuenta(listaPagosTarjeta.get(j).getRespuestaPago().getNombreUsuario() == null ? "" : listaPagosTarjeta.get(j).getRespuestaPago().getNombreUsuario());
                    //voucher.setAID(listaPagosTarjeta.get(j).getRespuestaPago().getAID());        
                    //voucher.setARQC(listaPagosTarjeta.get(j).getRespuestaPago().getARQC());

                    try {
                        SION.log(Modulo.VENTA, "Intentando imprimir voucher de tarjeta " + voucher.toString(), Level.INFO);
                        impresionVenta.imprimirVoucher(voucher);
                        fueVoucherExitoso = true;
                    } catch (Exception e) {
                        SION.logearExcepcion(Modulo.VENTA, e, "Excepcion al imprimir voucher");
                    } catch (Error e) {
                        SION.log(Modulo.VENTA, "Error al imprimir voucher: " + e.getLocalizedMessage(), Level.SEVERE);
                    }
                    SION.log(Modulo.VENTA, "Se imprimiero voucher correctamente? " + fueVoucherExitoso, Level.INFO);
                }
            }

        } else {

            SION.log(Modulo.VENTA, "Tipo de ticket : Recarga TA.", Level.INFO);

            VentaTATicket ticket = new VentaTATicket();
            ticket.setCaja(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));
            ticket.setCajero(String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()));
            ticket.setCalleNumeroTienda(calleNumeroTienda);
            ticket.setColoniaTienda(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getColonia()));
            ticket.setDelegacionCiudadEstadoCP(delegacionCiudadEstado);
            String fechaD = obtenerFecha();
            ticket.setFecha(fechaD.substring(0, 10));
            ticket.setHora(fechaD.substring(10));
            ticket.setNombreTienda(String.valueOf(VistaBarraUsuario.getSesion().getNombreTienda()));
            ticket.setTiendaId(String.valueOf(VistaBarraUsuario.getSesion().getTiendaId()));
            ticket.setEfectivo(String.valueOf(montoRecibido));
            //ticket.setNumeroOperacion(String.valueOf(respuestaVentaTADto.getPaTransaccionId()));
            ticket.setNumeroOperacion(String.valueOf(peticionVentaTADto.getNumFolio()));
            ticket.setNumeroTelefono(String.valueOf(peticionVentaTADto.getTelefono()));
            if (esSuperPrecio) {
                ticket.setEncabezado(SION.obtenerMensaje(Modulo.VENTA, "ENCABEZADO.TICKET.SUPR"));
            } else {
                ticket.setEncabezado(SION.obtenerMensaje(Modulo.VENTA, "ENCABEZADO.TICKET"));
            }

            if (esConfirmacionRecarga) {
                ticket.setFolio(String.valueOf(respuestaConsultaTransaccionTADto.getNumTransaccion())+"I");
                ticket.setNumeroReferencia(String.valueOf(respuestaConsultaTransaccionTADto.getNumAutorizacion()));
                ticket.setTransaccion(String.valueOf(respuestaConsultaTransaccionTADto.getNumTransaccion()));
            } else {
                ticket.setFolio(String.valueOf(respuestaVentaTADto.getPaTransaccionId())+"I");
                ticket.setNumeroReferencia(String.valueOf(respuestaVentaTADto.getPaTaNumOperacion()));
                ticket.setTransaccion(String.valueOf(respuestaVentaTADto.getPaTransaccionId()));
            }

            if( compania.getProveedorId() == 6 || compania.getProveedorId() == 12 ){
                
                String strCompania = compania.getNombre();
                
                
                
                if ((strCompania.toUpperCase().contains("TELCEL") || strCompania.toUpperCase().contains("MOVISTAR")) && (compania.getId()== 5 || compania.getId()== 6 || compania.getId()== 7 ||compania.getId()== 8)){
                    strCompania = SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.datos.paquete"+compania.getId()+".nombre");
                }
                
                ticket.setCompania(strCompania);
                
                String mensajeRecarga = "";
                String textoAutorizacion = "";
               
                if(compania.getId()==1){
                    
                    //if (esConfirmacionRecarga) {
                        //ticket.setMensajeCompania(SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania1.mensaje.vigencia").replace("1%", getDiasVigencia()));
                        
                    try{
                        mensajeRecarga = SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania"+compania.getId()+".mensaje");
                        textoAutorizacion = SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania.msjAutorizacionGeneral");
                    }catch(Exception ex){
                        mensajeRecarga ="";
                    }
                    
                    ticket.setMensajeCompania(compania.getNombre()+"\n"+mensajeRecarga+"\n\n"+textoAutorizacion);
                        
                    /*} else {
                        ticket.setMensajeCompania(respuestaVentaTADto.getPaDescError());
                    }*/
                }else{
                    
                    if (strCompania.toUpperCase().contains("TELCEL") && ((compania.getId()== 2 || compania.getId()== 5 || compania.getId()== 6) && compania.getProveedorId()== 6)){
                        compania.setId(1);
                    }
                    if (strCompania.toUpperCase().contains("MOVISTAR") && ((compania.getId()== 4 || compania.getId()== 7 || compania.getId()== 8) && compania.getProveedorId()== 12)){
                        
                        compania.setId(12);
                    }
                    
                    SION.log(Modulo.VENTA, "compania.setId: " + compania.toString(), Level.INFO);
                    
                    try{
                        mensajeRecarga = SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania"+compania.getId()+".mensaje");
                        textoAutorizacion = SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania.msjAutorizacionGeneral");
                    }catch(Exception ex){
                        mensajeRecarga ="";
                    }
                    
                    ticket.setMensajeCompania(compania.getNombre()+"\n"+mensajeRecarga+"\n\n"+textoAutorizacion);
                }
                
            }else{

                
                
                if(listaOrdenada.get(0).getFcNombreArticulo().toUpperCase().contains("TELCEL")){
                    compania.setId( Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "RECARGA.INTERNET.TELCEL.ID")) );
                }
                
                if(listaOrdenada.get(0).getFcNombreArticulo().toUpperCase().contains("MOVISTAR")){
                   compania.setId( Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "RECARGA.INTERNET.MOVISTAR.ID")) );
                }
				
				                
                
                //switch (peticionVentaTADto.getCompaniaTelefonicaId()) {
                switch (compania.getId()) {
                    case 1:
                    case 6:
                        if (esConfirmacionRecarga) {
                            ticket.setMensajeCompania(SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania1.mensaje.vigencia").replace("1%", getDiasVigencia()));
                        } else {
                            ticket.setMensajeCompania(respuestaVentaTADto.getPaDescError());
                        }
                        ticket.setCompania(SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania1"));
                        break;
                    case 2:
                        ticket.setMensajeCompania(SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania2.mensaje"));
                        ticket.setCompania(SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania2"));
                        break;
                    case 3:
                        ticket.setMensajeCompania(SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania3.mensaje"));
                        ticket.setCompania(SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania3"));
                        break;
                    case 4:
                        ticket.setMensajeCompania(SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania4.mensaje"));
                        ticket.setCompania(SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania4"));
                        break;
                    case 5:
                        ticket.setMensajeCompania(SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania5.mensaje"));
                        ticket.setCompania(SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania5"));
                        break;
                        
                    default:
                        ticket.setMensajeCompania(SION.obtenerMensaje(Modulo.VENTA, "ventas.ta.compania5.mensaje"));
                }
            }

            ArticuloTicketDto[] articulos = new ArticuloTicketDto[1];
            
            /*SION.log(Modulo.VENTA, "imprime art0: " + listaOrdenada.get(0).toString(), Level.INFO);
            SION.log(Modulo.VENTA, "ticket.getCompania " + ticket.getCompania(), Level.INFO);
            SION.log(Modulo.VENTA, "getProveedorId " + compania.getProveedorId(), Level.INFO);
            SION.log(Modulo.VENTA, "getId " + compania.getId(), Level.INFO);*/
            
            if (compania.getProveedorId()==8){
                articulos[0] = impresionVenta.articulosTADtoaTicket(listaOrdenada.get(0), listaOrdenada.get(0).getFcNombreArticulo());
            }else{
                articulos[0] = impresionVenta.articulosTADtoaTicket(listaOrdenada.get(0), ticket.getCompania());
            }
            
            ticket.setArticulos(articulos);

            ticket.setTotalEfectivo(quitarFormatoCantidad(ticket.getArticulos()[0].getPrecio()));
            ticket.setIvaTotalVenta(String.valueOf(ticket.getArticulos()[0].getIva()));
            ticket.setTotalDescripcion(quitarFormatoCantidad(ticket.getArticulos()[0].getPrecio()));
            ticket.setTotalArticulos();
            ticket.setCambio(String.valueOf(montoRecibido - (Double.valueOf(quitarFormatoCantidad(ticket.getArticulos()[0].getPrecio())) + ticket.getArticulos()[0].getIva())));

            SION.log(Modulo.VENTA, "Datos del ticket de venta TA: " + ticket.toString(), Level.INFO);

            try {
                 impresionVenta.imprimirTicketTA(ticket);
                /*if (compania.getId() == 1 || compania.getId() == 6) {
                //if (peticionVentaTADto.getCompaniaTelefonicaId() == 1 || peticionVentaTADto.getCompaniaTelefonicaId() == 6) {
                    impresionVenta.imprimirTicketTATelcel(ticket);
                } else {
                    impresionVenta.imprimirTicketTA(ticket);
                }*/
                fueImpresionTaExitoso = true;
            } catch (Exception e) {
                SION.logearExcepcion(Modulo.VENTA, e, "Excepcion al imprimir ticket de venta ta");
            } catch (Error e) {
                SION.log(Modulo.VENTA, "Error al imprimir ticket de venta ta", Level.SEVERE);
            }

            SION.log(Modulo.VENTA, "Se imprimió ticket de Venta TA correctamente? " + fueImpresionTaExitoso, Level.INFO);
        }
    }

    public String getDiasVigencia() {
        String cadena = "";

        switch ((int) peticionVentaTADto.getPaMontoTotalVta()) {
            case 20:
                cadena += "10";
                break;
            case 30:
                cadena += "15";
                break;
            case 40:
                cadena += "30";
                break;
            default:
                cadena += "60";
                break;
        }

        return cadena;
    }

    public String quitarFormatoCantidad(String cantidad) {

        String cadenaFormato;

        cadenaFormato = cantidad.replaceAll(",", "");

        return cadenaFormato;
    }

    public boolean seCompletoTicketVenta() {
        return fueImpresionExitosa;
    }

    public boolean seCompletoTicketVentaTA() {
        return fueImpresionTaExitoso;
    }

    public boolean seCompletoVoucherTarjeta() {
        return fueVoucherExitoso;
    }

    public void llenarListaArticulosVentaTA() {

        SION.log(Modulo.VENTA, "Llenando arreglo ordenado de artículos en venta TA", Level.FINEST);

        listaOrdenada.add(new ArticuloDto(
                listaArticulosTA.get(0).getPaarticuloid(),
                listaArticulosTA.get(0).getPacdgbarras(),
                1,//Cantidad
                listaArticulosTA.get(0).getPaprecio(),
                listaArticulosTA.get(0).getPacosto(),
                listaArticulosTA.get(0).getPadescuento(),
                listaArticulosTA.get(0).getPaiva(),
                listaArticulosTA.get(0).getPanombrearticulo(),
                listaArticulosTA.get(0).getPagranel(),
                listaArticulosTA.get(0).getPaIepsId(),
                listaArticulosTA.get(0).getTipoDescuento()
                ));

    }

    public void llenarListaArticulosVenta(int idOperacion) {

        SION.log(Modulo.VENTA, "Llenando arreglo ordenado de artículos", Level.FINEST);

        int idArticuloRecarga = 0;


        int i = 0;
        while (i < listaArticulos.size()) {

            ////proceso para llenar lista ordenada de articulos
            int j = 0, id = 0;
            while (j < listaOrdenada.size()) {
                if (listaArticulos.get(i).getPacdgbarras().equals(listaOrdenada.get(j).getFcCdGbBarras())
                        && listaArticulos.get(i).getDevueltos() < listaArticulos.get(i).getCantidad()) {
                    //si se encuenra articulo en ultima linea
                    id = 1;
                    listaOrdenada.get(j).setFnCantidad(listaOrdenada.get(j).getFnCantidad() + (listaArticulos.get(i).getCantidad() - listaArticulos.get(i).getDevueltos()));
                    j = listaOrdenada.size();
                }
                j++;
            }
            if (id == 0) {

                if (listaArticulos.get(i).getDevueltos() < listaArticulos.get(i).getCantidad()) {

                    double cantidadArticulos = Double.parseDouble(utilerias.getNumeroFormateado(listaArticulos.get(i).getCantidad() - listaArticulos.get(i).getDevueltos()));

                    if (cantidadArticulos > 0 || idOperacion == 1 || idOperacion == 3 || idOperacion == 4) {//valida k sea venta o devolucion no vacia
                        listaOrdenada.add(new ArticuloDto(
                                listaArticulos.get(i).getPaarticuloid(),
                                listaArticulos.get(i).getPacdgbarras(),
                                cantidadArticulos,
                                listaArticulos.get(i).getPaprecio(),
                                listaArticulos.get(i).getPacosto(),
                                listaArticulos.get(i).getPadescuento(),
                                listaArticulos.get(i).getPaiva(),
                                listaArticulos.get(i).getPanombrearticulo(),
                                listaArticulos.get(i).getPagranel(),
                                listaArticulos.get(i).getPaIepsId(),
                                listaArticulos.get(i).getTipoDescuento()
                                        ));
                    }
                }

            }
            i++;
        }

        SION.log(Modulo.VENTA, "Lista ordenada de artículos finalizada correctamente", Level.FINEST);

    }

    public void llenarpeticionVentaTADto(double montoRecibido, double montoVenta, int tipoMov, long conciliacion, CompaniaBean _compania, int folioVentaTA) {

        SION.log(Modulo.VENTA, "Entrando a método de envío de petición de venta de Tiempo Aire", Level.INFO);

        compania = _compania;
        peticionVentaTADto = null;
        peticionVentaTADto = new PeticionVentaTADto();
        sesion = VistaBarraUsuario.getSesion();

        String t = sesion.getIpEstacion();

        peticionVentaTADto.setArticulos(listaOrdenada.toArray(new ArticuloDto[0]));
        peticionVentaTADto.setPaFechaOper(obtenerFecha());
        peticionVentaTADto.setPaMontoTotalVta(Double.parseDouble(utilerias.getNumeroFormateado(montoVenta)));
        peticionVentaTADto.setPaPaisId(sesion.getPaisId());
        peticionVentaTADto.setTiendaId(sesion.getTiendaId());
        t = t.concat("|CAJA");
        t = t.concat(String.valueOf(sesion.getNumeroEstacion()));
        peticionVentaTADto.setPaTerminal(t);
        peticionVentaTADto.setPaTipoMovto(tipoMov);
        peticionVentaTADto.setPaUsuarioId(sesion.getUsuarioId());
        peticionVentaTADto.setTiposPago(listaTiposPago.toArray(new TipoPagoDto[0]));
        peticionVentaTADto.setPaConciliacionId(conciliacion);
        peticionVentaTADto.setNumFolio(folioVentaTA);
        
        peticionVentaTADto.setTelefono(RecargasTADBinding.numero);
        peticionVentaTADto.setCompaniaTelefonicaId(compania.getProveedorId());

        SION.log(Modulo.VENTA, "Datos para la petición de venta de TA : " + peticionVentaTADto.toString(), Level.INFO);

    }

    public void llenarPeticionVentaDto(double montoRecibido, double montoVenta, int tipoMov, long conciliacion) {

        SION.log(Modulo.VENTA, "Entrando a método de envío de petición de venta ", Level.INFO);

        peticionVentaDto = null;
        peticionVentaDto = new PeticionVentaDto();
        sesion = VistaBarraUsuario.getSesion();

        String t = sesion.getIpEstacion();
        peticionVentaDto.setPaUsuarioId(sesion.getUsuarioId());
        peticionVentaDto.setArticulos(listaOrdenada.toArray(new ArticuloDto[0]));
        peticionVentaDto.setPaFechaOper(obtenerFecha());
        peticionVentaDto.setPaMontoTotalVta(Double.parseDouble(utilerias.getNumeroFormateado(montoVenta)));
        peticionVentaDto.setPaPaisId(sesion.getPaisId());
        peticionVentaDto.setTiendaId(sesion.getTiendaId());
        t = t.concat("|CAJA");
        t = t.concat(String.valueOf(sesion.getNumeroEstacion()));
        peticionVentaDto.setPaTerminal(t);
        peticionVentaDto.setPaTipoMovto(tipoMov);
        //peticionVentaDto.setPaUsuarioId(sesion.getUsuarioId());
        peticionVentaDto.setTiposPago(listaTiposPago.toArray(new TipoPagoDto[0]));
        //peticionVentaDto.setTiposPago(listaTiposPago);
        peticionVentaDto.setPaConciliacionId(conciliacion);

        SION.log(Modulo.VENTA, "Datos para la petición de venta : " + peticionVentaDto.toString(), Level.INFO);

    }

    public String agregarTiposPago() {

        String estatusLectura = "";
        SION.log(Modulo.VENTA, "Agregando registros de pago al arreglo DTO.", Level.INFO);


        for (Iterator<DatosFormasPagoBean> it = listaDatosFormasPago.iterator(); it.hasNext();) {
            DatosFormasPagoBean pago = it.next();

            if (pago.getFitipopagoid() == 2) {
                if (estatusLectura.isEmpty()) {
                    estatusLectura = estatusLectura.concat(pago.getEstatusLectura().get().trim());
                } else {
                    estatusLectura = estatusLectura.concat("|").concat(pago.getEstatusLectura().get().trim());
                }
            }
            SION.log(Modulo.VENTA, "TipoPagoDto > "+
            "getFitipopagoid = "+pago.getFitipopagoid()+
            "[getFnmontopago = "+pago.getFnmontopago()+", "+
            "Formateado : getFnmontopago = "+Double.parseDouble(utilerias.getNumeroFormateado(pago.getFnmontopago()))+"]"+
            "getFnnumerovales = "+pago.getFnnumerovales()+
            "getTarjetaidbus = "+pago.getTarjetaidbus()+
            "getComisiontarjeta = "+Double.parseDouble(utilerias.getNumeroFormateado(pago.getComisiontarjeta()))
            , Level.INFO);
            
            listaTiposPago.add(new TipoPagoDto(
                    pago.getFitipopagoid(),
                    Double.parseDouble(utilerias.getNumeroFormateado(pago.getFnmontopago())),
                    pago.getFnnumerovales(),
                    pago.getTarjetaidbus(),
                    Double.parseDouble(utilerias.getNumeroFormateado(pago.getComisiontarjeta()))));

        }

        return estatusLectura;

    }

    public void insertarRegistroArticulo(int index, int id, boolean esEmpaquetado, double unidad, double precio, double cantidad, double importe) {

        if (listaArticulos.get(listaArticulos.size() - 1).getPacdgbarras().equals(listaArticulos.get(index).getPacdgbarras())) {

            //se encuentra en el ultimo registro
            if (id == 1) {//sumar

                if (listaArticulos.get(listaArticulos.size() - 1).getImporte() > 0) {
                    if (esEmpaquetado) {
                        listaArticulos.get(listaArticulos.size() - 1).setCantidad(listaArticulos.get(listaArticulos.size() - 1).getCantidad() + unidad);
                        listaArticulos.get(listaArticulos.size() - 1).setImporte(listaArticulos.get(listaArticulos.size() - 1).getImporte() + (unidad * precio));
                    } else {
                        listaArticulos.get(listaArticulos.size() - 1).setCantidad(listaArticulos.get(listaArticulos.size() - 1).getCantidad() + 1);
                        listaArticulos.get(listaArticulos.size() - 1).setImporte(listaArticulos.get(listaArticulos.size() - 1).getImporte() + (precio));
                    }
                } else {
                    agregarArticulo(index, cantidad, importe, 0);
                }
            } else {
                agregarArticulo(index, cantidad, importe, listaArticulos.get(index).getDevueltos());
            }
        } else {//insertar nueva fila
            if (id == 1) {//si suma

                agregarArticulo(index, cantidad, importe, 0);
            } else {

                agregarArticulo(index, cantidad, importe, listaArticulos.get(index).getDevueltos());
            }

        }

    }

    public void insertarRegistroFormaPago(DatosFormasPagoBean _pago) {

        SION.log(Modulo.VENTA, "Insertando nuevo registro de forma de pago: " + _pago, Level.INFO);
        listaDatosFormasPago.add(_pago);

    }

    public void agregarArticulo(int index, double cantidad, double importe, double devueltos) {

        listaArticulos.add(new ArticuloBean(
                //InicioVistaModelo.listaArticulosVenta.get(index).getCantidad(),
                cantidad,
                listaArticulos.get(index).getPacdgbarras(),
                listaArticulos.get(index).getPaprecio(),
                listaArticulos.get(index).getPaarticuloid(),
                listaArticulos.get(index).getPanombrearticulo(),
                listaArticulos.get(index).getPafamiliaid(),
                listaArticulos.get(index).getPacosto(),
                listaArticulos.get(index).getPaiva(),
                listaArticulos.get(index).getPadescuento(),
                listaArticulos.get(index).getPagranel(),
                listaArticulos.get(index).getPaUnidadMedida(),
                listaArticulos.get(index).getPaCdgBarrasPadre(),
                listaArticulos.get(index).getPacdgerror(),
                listaArticulos.get(index).getPadescerror(),
                importe,
                listaArticulos.get(index).getCompaniaid(),
                listaArticulos.get(index).getDescripcioncompania(),
                listaArticulos.get(index).getDevcantidad(),
                listaArticulos.get(index).getDevimporte(),
                devueltos,
                listaArticulos.get(index).getPaunidad(),
                listaArticulos.get(index).getPaIepsId(),
                listaArticulos.get(index).getMetodoEntrada()));
    }
    
    public void actualizarListaArticulos(final Set<ArticuloBean> articulos){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                SION.log(Modulo.VENTA, "Actualizando descuento de artículos ", Level.INFO);
                for (ArticuloBean articuloBean : listaArticulos )
                {
                    articuloBean.setPadescuento(0);
                    articuloBean.setImporte(
                            (articuloBean.getPaprecio()-articuloBean.getPadescuento()) * 
                                articuloBean.getCantidad() * (articuloBean.getImporte()<0? -1:1));
                    for ( Iterator<ArticuloBean> it = articulos.iterator(); it.hasNext(); )
                    {
                        ArticuloBean articuloVBean = it.next();
                        
                        if( articuloVBean.getPaarticuloid() == articuloBean.getPaarticuloid() &&
                            articuloVBean.getPacdgbarras().equals(articuloBean.getPacdgbarras()) )
                        {
                            SION.log(Modulo.VENTA, "Artículo por actuaizar: "+articuloBean.getPacdgbarras()+
                                    ", descuento: "+articuloVBean.getPadescuento(), Level.INFO);
                            
                            double cantidad = articuloBean.getCantidad() * (articuloBean.getImporte()<0? -1:1);
                            Integer unidad = articuloBean.getPaunidad();
                            
                            double precio = articuloVBean.getPaprecio();                            
                            double descuento= articuloVBean.getPadescuento();
                            
                            boolean esEmpaquetado = false;//articuloVBean.get;
                            double importe=0;

                            if (esEmpaquetado) {
                                cantidad = unidad;
                                importe = (precio-descuento) * unidad;
                            }

                            importe = (precio-descuento) * cantidad;
                            
                            articuloBean.setPaprecio(precio);
                            articuloBean.setPadescuento(descuento);
                            articuloBean.setImporte(importe);
                            
                            break;
                        }
                    }
                }
                
            }
        });
    }

    public void modificarListaArticulos(int index, int id) {

        if (index >= 0) {

            if (listaArticulos.get(index).getImporte() > 0 &&
                    //revisa el importe y que no se haya devuelto el total del registro
                (listaArticulos.get(index).getCantidad() > listaArticulos.get(index).getDevueltos()))
            {
                double importe = 0;
                double cantidad = 0;
                double devueltos = 0;
                double precio = 0;
                double unidad = 0;
                double descuento = 0;
                boolean esEmpaquetado = false;

                cantidad = listaArticulos.get(index).getCantidad();
                devueltos = listaArticulos.get(index).getDevueltos();
                importe = listaArticulos.get(index).getImporte();
                precio = listaArticulos.get(index).getPaprecio();
                unidad = listaArticulos.get(index).getPaunidad();
                descuento= listaArticulos.get(index).getPadescuento();

                if (listaArticulos.get(index).getPaunidad() > 1) {
                    esEmpaquetado = true;
                }

                if (id == 0) {//suprimir
                    cantidad = cantidad - devueltos;
                    importe = (precio-descuento) * cantidad;
                    importe = importe * -1;
                    
                    if (listaArticulos.get(index).getPagranel() != 0) {
                        devueltos = listaArticulos.get(index).getCantidad();
                    } else {
                        devueltos = (devueltos + (cantidad));
                    }
                    listaArticulos.get(index).setDevueltos(devueltos);
                } else if (id == 2) {//restar 1 o si es caja su contenido
                    if (devueltos < cantidad) {
                        if (esEmpaquetado) {

                            cantidad = unidad;
                            importe = unidad * (precio-descuento);
                            if (listaArticulos.get(index).getPagranel() != 0) {
                                devueltos = listaArticulos.get(index).getCantidad();
                            } else {
                                devueltos = (devueltos + unidad);
                            }
                        } else {

                            cantidad = 1;
                            if (listaArticulos.get(index).getPagranel() != 0) {
                                devueltos = listaArticulos.get(index).getCantidad();
                            } else {
                                devueltos += 1;
                            }
                            importe = (precio-descuento);
                        }

                        importe *= -1;
                        listaArticulos.get(index).setDevueltos(devueltos);
                    }

                    if (listaArticulos.get(index).getPagranel() != 0) {
                        cantidad = listaArticulos.get(index).getCantidad();
                        importe = ((precio-descuento) * listaArticulos.get(index).getCantidad()) * -1;
                        listaArticulos.get(index).setDevueltos(listaArticulos.get(index).getCantidad());
                    }

                } else if (id == 1) {//sumar

                    if (esEmpaquetado) {
                        cantidad = unidad;
                        importe = (precio-descuento) * unidad;
                    } else {
                        cantidad = 1;
                        importe = (precio-descuento);
                    }
                }

                
                if (id == 0 || id == 2) {
                    getCtrlTarjetasDescuento().setNecesitaValidarArticulos(true);
                    insertarRegistroArticulo(index, id, esEmpaquetado, unidad, precio,
                            Double.parseDouble(utilerias.getNumeroFormateado(cantidad)), utilerias.RedondearImporte(cantidad, precio, descuento) * -1 /*
                             * Double.parseDouble(utilerias.getNumeroFormateado(importe))
                             */);
                } else {
                    if( getCtrlTarjetasDescuento().getUltimoImporteAceptado() == 0 )
                        getCtrlTarjetasDescuento().setNecesitaValidarArticulos(true);
                    
                    insertarRegistroArticulo(index, id, esEmpaquetado, unidad, precio,
                            Double.parseDouble(utilerias.getNumeroFormateado(cantidad)), Double.parseDouble(utilerias.getNumeroFormateado(importe)));
                }

                if ( hayPromociones(TiposDescuento.Redondeo) ){
                    eliminarArticulosPromocion(TiposDescuento.Redondeo);
                    AdministraVentanas.mostrarAlerta("Se eliminó el artículo agregado en la promoción.", TipoMensaje.ADVERTENCIA);
                }
            }
        }
    }

    public int getCantidadTarjetasRecibidas(int id) {
        int contador = 0;
        int i = 0;

        while (i < listaPagosTarjeta.size()) {
            if (listaPagosTarjeta.get(i).getRespuestaPago().getTipoTarjeta() == id) {
                contador++;
            }
            i++;
        }

        return contador;
    }

    public double getSumaImporteTarjetas(int tipoTarjeta) {//si tipo tarjeta =0 devuelve monto total
        int i = 0, TamanioLista;
        double suma = 0.0;
        while (i < listaPagosTarjeta.size()) {
            if (tipoTarjeta != 0) {
                //System.out.println("comparando: "+listaPagosTarjeta.get(i).getRespuestaPago().getTipoTarjeta()+" - "+
                //tipoTarjeta+" || "+"0"+ " - "+String.valueOf(listaPagosTarjeta.get(i).getRespuestaPago().getCodigoError()));
                if (listaPagosTarjeta.get(i).getRespuestaPago().getTipoTarjeta() == tipoTarjeta && "0".equals(String.valueOf(listaPagosTarjeta.get(i).getRespuestaPago().getCodigoError()))) {
                    suma += listaPagosTarjeta.get(i).getPeticionPago().getMonto();
                }
            } else {
                suma += listaPagosTarjeta.get(i).getPeticionPago().getMonto();
            }
            i++;
        }


        //System.out.println("suma tarjetas "+tipoTarjeta+" :"+suma);
        return suma;
    }

    public double getSumaComisionTarjetas() {
        int i = 0, TamanioLista;
        double suma = 0.0;
        while (i < listaPagosTarjeta.size()) {
            suma += listaPagosTarjeta.get(i).getRespuestaPago().getPeticionReverso().getComision();
            i++;
        }
        return suma;
    }

    public double getImporteVentaTA() {
        int i = 0, TamanioLista;
        double suma = 0.0;
        TamanioLista = listaArticulosTA.size();
        while (i < TamanioLista) {
            suma += listaArticulosTA.get(i).getImporte();
            i++;
        }

        if (suma > 0) {
            suma = Double.parseDouble(utilerias.getNumeroFormateado(suma));
        }

        return suma;
    }

    public double getImporteVenta() {
        int i = 0, TamanioLista;
        double suma = 0.0;
        TamanioLista = listaArticulos.size();
        while (i < TamanioLista) {
            //SION.log(Modulo.VENTA, "> "+listaArticulos.get(i).toString(), Level.INFO);
            suma += Double.parseDouble(utilerias.getNumeroFormateado(listaArticulos.get(i).getImporte()));
            i++;
        }
        if (suma > 0) {
            suma = Double.parseDouble(utilerias.getNumeroFormateado(suma));
        }
        //ctrlTarjetasDescuento.actualizaArticulosXValidar(listaArticulos);
        return suma;
    }

    public double getImporteDevolucion() {
        int i = 0, TamanioLista;
        double suma = 0.0;
        TamanioLista = listaArticulos.size();
        while (i < TamanioLista) {
            suma += listaArticulos.get(i).getDevimporte();
            i++;
        }
        return suma;
    }

    public String obtenerFecha() {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar Cal = Calendar.getInstance();
        return dateFormat.format(new Date());
    }

    //conciliacion
    public void reversarConciliacionVentaTA(long conciliacion) {

        SION.log(Modulo.VENTA, "Entrando a método de reverso de conciliación de ventaTA", Level.FINEST);

        peticionActTransaccionLocalBean.setPaConciliacionId(conciliacion);
        peticionActTransaccionLocalBean.setPaTransaccionVenta(getTransaccionTA());

        conciliacionLocalControlador.updReversaConciliacionTa(peticionActTransaccionLocalBean);

    }

    public void cancelarConciliacionVenta(long conciliacion) {

        SION.log(Modulo.VENTA, "Entrando a método de reverso de conciliación de venta", Level.FINEST);

        peticionActTransaccionLocalBean.setPaConciliacionId(conciliacion);
        peticionActTransaccionLocalBean.setPaTransaccionVenta(getTransaccion());

        conciliacionLocalControlador.updCancelacionConciliacion(peticionActTransaccionLocalBean);

    }

    public void actualizarTransacciónLocal(long conciliacion, int sistema, int modulo, int submodulo, boolean esVentaTA, int tipoActualizacion) {

        SION.log(Modulo.VENTA, "Entrando a método de actualización de la transacción en BD local", Level.FINEST);

        if (esVentaTA) {
            SION.log(Modulo.VENTA, "Llenando petición de actualización de transacción local de venta TA", Level.FINEST);
            peticionActTransaccionLocalBean.setPaConciliacionId(conciliacion);
            if (tipoActualizacion == ACTUALIZACION_LOCAL) {
                peticionActTransaccionLocalBean.setPaTransaccionVenta(getTransaccionTA());
            } else {
                peticionActTransaccionLocalBean.setPaTransaccionVenta(getTransaccionConfirmacionTA());
            }
        } else {
            SION.log(Modulo.VENTA, "Llenando petición de actualización de transacción local de venta", Level.FINEST);
            peticionActTransaccionLocalBean.setPaConciliacionId(conciliacion);
            if (tipoActualizacion == ACTUALIZACION_LOCAL) {
                peticionActTransaccionLocalBean.setPaTransaccionVenta(getTransaccion());
            } else {
                peticionActTransaccionLocalBean.setPaTransaccionVenta(getTransaccionConfirmacionTA());
            }
        }

        SION.log(Modulo.VENTA, "Enviando petición de actualización de transacción local : " + peticionActTransaccionLocalBean.toString(), Level.INFO);

        respuestaActualizacionLocalBean = conciliacionLocalControlador.actualizarTransaccionLocal(peticionActTransaccionLocalBean);

        int contador = 0;

        while ( (contador < 3) && 
            (respuestaActualizacionLocalBean.getPaCdgError() != 0)) 
		{
                respuestaActualizacionLocalBean = conciliacionLocalControlador.actualizarTransaccionLocal(peticionActTransaccionLocalBean);
            

            contador++;
        }

        SION.log(Modulo.VENTA, "recibiendo respuesta de la petición de actualización de la transacción local : " + respuestaActualizacionLocalBean.toString(), Level.INFO);


    }

    public void llenarPeticionActializacionEstatusReversado(long conciliacion) {

        peticionActTransaccionLocalBean.setPaConciliacionId(conciliacion);

        SION.log(Modulo.VENTA, "Insertando petición de actualización de reverso : " + peticionActTransaccionLocalBean.toString(), Level.FINEST);

        respuestaActualizacionLocalBean = conciliacionLocalControlador.actualizarEstatusReversado(peticionActTransaccionLocalBean);

        int contador = 0;

        SION.log(Modulo.VENTA, "recibiendo respuesta de actualización de reverso : " + respuestaActualizacionLocalBean.toString(), Level.FINEST);

    }

    public long obtenerRespuestaConciliacion() {

        SION.log(Modulo.VENTA, "obteniendo respuesta a la petición de conciliación de la venta ", Level.FINEST);

        respuestaConciliacionLocalBean = conciliacionLocalControlador.conciliacionVentaLocal(peticionConciliacion);

        SION.log(Modulo.VENTA, "Valores de conciliación de venta obtenidos en la respuesta : " + respuestaConciliacionLocalBean.toString(), Level.INFO);

        return respuestaConciliacionLocalBean.getPaConciliacionId();

    }

    public void llenarPeticionConciliacion(int sistema, int modulo, int submodulo) {

        SION.log(Modulo.VENTA, "Entrando a método de inserción de datos de conciliación de venta: ", Level.FINEST);

        peticionConciliacion.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
        //conciliacionLocalBean.setPaTiendaId(903);
        peticionConciliacion.setPaTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
        peticionConciliacion.setPaUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());
        peticionConciliacion.setPaSistemaId(sistema);
        peticionConciliacion.setPaModulo(modulo);
        peticionConciliacion.setPaSubmoduloId(submodulo);

        SION.log(Modulo.VENTA, "Datos que se usarán para la conciliación : " + peticionConciliacion.toString(), Level.INFO);

    }

    public boolean hayMarcacionFueraDeLinea() {
        return hayMarcacionFueraLinea;
    }

    /////fuera de linea
    public void llenarPeticionVentaFueraLinea(double montoVenta, int sistema, int modulo, int submodulo, long conciliacion, String _usuarioAutoriza, String _estatusLectura) {

        SION.log(Modulo.VENTA, "Llenando petición de venta fuera de línea", Level.INFO);

        sesion = VistaBarraUsuario.getSesion();
        String t = sesion.getIpEstacion();

        t = t.concat("|CAJA");
        t = t.concat(String.valueOf(sesion.getNumeroEstacion()));
        peticionVentaLocalBean.setPaTerminal(t);
        peticionVentaLocalBean.setPaUsuarioId(sesion.getUsuarioId());
        if (_usuarioAutoriza.isEmpty()) {
            peticionVentaLocalBean.setPaUsuarioAutorizaId(0);
        } else {
            try {
                peticionVentaLocalBean.setPaUsuarioAutorizaId(Long.parseLong(_usuarioAutoriza));
            } catch (NumberFormatException nfe) {
                SION.logearExcepcion(Modulo.VENTA, nfe, "Error al setear usuario que autoriza la venta");
            }
        }
        peticionVentaLocalBean.setPaTotalVenta(montoVenta);
        peticionVentaLocalBean.setPaPaisId(sesion.getPaisId());
        peticionVentaLocalBean.setPaTiendaId(sesion.getTiendaId());
        peticionVentaLocalBean.setPaSistemaId(sistema);
        peticionVentaLocalBean.setPaModuloId(modulo);
        peticionVentaLocalBean.setPaSubModuloId(submodulo);
        peticionVentaLocalBean.setPaIndicador(1);
        peticionVentaLocalBean.setPaConciliacionId(conciliacion);
        //peticionVentaLocalBean.setPaEstatusLectura(_estatusLectura);

        SION.log(Modulo.VENTA, "Datos de la petición de venta fuera de línea : " + peticionVentaLocalBean.toString(), Level.INFO);

    }

    public void llenarListaArticulosVentaFueraLinea() {

        int i = 0;

        articuloBeans = new ArticuloFLBean[listaOrdenada.size()];

        for (Iterator<ArticuloDto> it = listaOrdenada.iterator(); it.hasNext();) {
            ArticuloDto listaArticulosDto = it.next();

            ArticuloFLBean articuloFL = new ArticuloFLBean();

            articuloFL.setFiArticuloId(listaArticulosDto.getFiArticuloId());
            articuloFL.setFcCdgBarras(listaArticulosDto.getFcCdGbBarras());
            articuloFL.setFnCantidad(listaArticulosDto.getFnCantidad());
            articuloFL.setFnPrecio(listaArticulosDto.getFnPrecio());
            articuloFL.setFnCosto(listaArticulosDto.getFnCosto());
            articuloFL.setFnDescuento(listaArticulosDto.getFnDescuento());
            articuloFL.setFnIva(listaArticulosDto.getFnIva());
            articuloFL.setFnIeps(listaArticulosDto.getFnIepsId());
            articuloFL.setTipoDescuento(listaArticulosDto.getTipoDescuento());
            
            articuloBeans[i] = articuloFL;

            i++;
        }

        peticionVentaLocalBean.setPaDatosArticulo(articuloBeans);

        SION.log(Modulo.VENTA, "Artículos en la petición de fuera de línea : ", Level.INFO);
        for (i = 0; i < listaOrdenada.size(); i++) {
            SION.log(Modulo.VENTA, articuloBeans[i].toString(), Level.INFO);
        }

    }

    public void llenarListaTiposPagoVentaFueraLinea() {

        int i = 0;

        tipoPagoBeans = new TipoPagoBean[listaTiposPago.size()];


        for (Iterator<TipoPagoDto> it = listaTiposPago.iterator(); it.hasNext();) {
            TipoPagoDto pagoDto = it.next();

            TipoPagoBean pagoFL = new TipoPagoBean();

            pagoFL.setFiTipoPagoId(pagoDto.getFiTipoPagoId());
            pagoFL.setFcReferenciaPagoTarjetas(String.valueOf(pagoDto.getPaPagoTarjetaIdBus()));
            pagoFL.setFnNumeroVales(pagoDto.getFnNumeroVales());
            pagoFL.setFnImporteAdicional(pagoDto.getImporteAdicional());
            pagoFL.setFnMontoPago(pagoDto.getFnMontoPago());

            tipoPagoBeans[i] = pagoFL;

            i++;
        }

        SION.log(Modulo.VENTA, "Tipos de pago en la petición de venta fuera de línea : ", Level.INFO);
        for (i = 0; i < tipoPagoBeans.length; i++) {
            SION.log(Modulo.VENTA, tipoPagoBeans[i].toString(), Level.INFO);
        }

        peticionVentaLocalBean.setPaDatosTiposPago(tipoPagoBeans);

    }

    public boolean marcarVentaFueraDeLinea() {

        boolean hayVenta = false;


        SION.log(Modulo.VENTA, "Intentando recibir respuesta de la venta fuera de línea", Level.INFO);

        respuestaVentaLocalBean = ventaFueraLineaControlador.spServicioVentaLocaL(peticionVentaLocalBean);

        SION.log(Modulo.VENTA, "Respuesta recibida : " + respuestaVentaLocalBean.toString(), Level.INFO);


        if (respuestaVentaLocalBean.getPaCdgError() == 0 && respuestaVentaLocalBean.getPaConciliacionId() != 0) {

            hayVenta = true;

        }

        return hayVenta;
    }
    
    public RespuestaActualizaConciliacionPendienteBean actualizaConciliacionPendiente() {
        SION.log(Modulo.VENTA, "Intentando recibir respuesta de la venta fuera de línea", Level.INFO);
        PeticionActualizaConciliacionPendienteBean solicitud = new PeticionActualizaConciliacionPendienteBean();
        solicitud.setPaPaisId   (peticionVentaDto.getPaPaisId());
        solicitud.setPaTiendaId ((int)peticionVentaDto.getTiendaId());
        solicitud.setPaConciliacionId(peticionVentaDto.getPaConciliacionId());
        RespuestaActualizaConciliacionPendienteBean respActConPend = validacionTarjetaDao.actualizaConciliacionPendiente(solicitud);

        SION.log(Modulo.VENTA, "Respuesta recibida : " + respActConPend.toString(), Level.INFO);

        return respActConPend;
    }

    public int getErrorFueraLinea() {
        return respuestaVentaLocalBean.getPaCdgError();
    }

    public String getDescErrorFueraLinea() {
        return respuestaVentaLocalBean.getPaDescError();
    }

    public long getConciliacionIdFueraLinea() {
        return respuestaVentaLocalBean.getPaConciliacionId();
    }

    //parametros venta
    //consulta de parametros
    public void consultaParametrosOperacion(int pais, int sistema, int modulo, int submodulo, int configuracion) {

        listaParametros.clear();
        listaParametros.add(parametrosDevolucionesDAO.consultaParametrosOperacion(pais, sistema, modulo, submodulo, configuracion));
    }

    public int consultaErrorParametro() {
        return listaParametros.get(0).getErrorId();
    }

    public String consultaDescErrorParametro() {
        return listaParametros.get(0).getDescripcionError();
    }

    public double consultaValorConfiguracionParametro() {
        return listaParametros.get(0).getValorConfiguracion();
    }

    //bloqueos
    public void agregarObjetoSesionTA() {

        SION.log(Modulo.VENTA, "Agregando objetos a sesión de venta TA.", Level.INFO);

        if (respuestaVentaTADto.getPaTypCursorBlqs() != null) {

            datosSesion = (BloqueoVentaBean) VistaBarraUsuario.getSesion().getBloqueosVenta();

            datosSesion.setMensajeBloqueo(bloqueoVenta.bloqueDtoWSaSesion(respuestaVentaTADto.getPaTypCursorBlqs()));

            datosSesion.setCdgError(0);
            VistaBarraUsuario.getSesion().setBloqueosVenta(datosSesion);


        } else if (respuestaVentaTADto.getPaCdgError() == 0) {

            //datosesion = new BloqueoVentaBean();
            datosSesion.setCdgError(-1);
            VistaBarraUsuario.getSesion().setBloqueosVenta(datosSesion);
        } else {
            VistaBarraUsuario.getSesion().setBloqueosVenta(null);
        }

    }

    public void agregarObjetoSesion() {

        SION.log(Modulo.VENTA, "Agregando objetos a sesión.", Level.FINEST);

        if (respuestaVentaDto.getPaTypCursorBlqs() != null) {

            BloqueoVentaBean nuevosDatos = (BloqueoVentaBean) VistaBarraUsuario.getSesion().getBloqueosVenta();
            nuevosDatos.setMensajeBloqueo(bloqueoVenta.bloqueDtoWSaSesion(respuestaVentaDto.getPaTypCursorBlqs()));
            nuevosDatos.setCdgError(0);
            VistaBarraUsuario.getSesion().setBloqueosVenta(nuevosDatos);

        } else if (respuestaVentaDto.getPaCdgError() == 0) {
            BloqueoVentaBean nuevosDatos = new BloqueoVentaBean();
            nuevosDatos.setCdgError(-1);
            VistaBarraUsuario.getSesion().setBloqueosVenta(nuevosDatos);
        } else {
            VistaBarraUsuario.getSesion().setBloqueosVenta(null);
        }

    }

    public void inicializarVariablesBloqueo() {
        existeBloqueo = false;
        existeAvisoBloqueo = false;
        existe2oAvisoBloqueo = false;
    }

    public boolean existeAvisoBloqueo() {
        return existeAvisoBloqueo;
    }

    public String getMensajeAvisoBloqueo() {
        return mensajeAvisoBloqueo;
    }

    public boolean existe2oAvisoBloqueo() {
        return existe2oAvisoBloqueo;
    }

    public String get2oMensajeAvisoBloqueo() {
        return mensaje2oAvisoBloqueo;
    }

    public boolean existeBloqueo() {
        return existeBloqueo;
    }

    public String getMensajeBloqueo() {
        return mensajeBloqueo;
    }

    public void evaluarBloqueoVenta(Button boton, int idPantalla) {

        SION.log(Modulo.VENTA, "Evaluando bloqueo de la venta.", Level.INFO);
        existeAvisoBloqueo = false;
        mensajeAvisoBloqueo = "";

        datosSesion = VistaBarraUsuario.getSesion().getBloqueosVenta();
        existenDatosSesion = false;
        
        if (datosSesion == null) {
            SION.log(Modulo.VENTA, "No se encontraron Datos en sesión", Level.INFO);
            existenDatosSesion = bloqueoVenta.existenDatosSesion(null);
        } else {
            SION.log(Modulo.VENTA, "Se encontraron Datos en sesión.", Level.INFO);
            existenDatosSesion = bloqueoVenta.existenDatosSesion((BloqueoVentaBean) datosSesion);
        }

        //Validamos la comunicación
        boolean hayComunicacion = neto.sion.tienda.genericos.utilidades.Utilerias.evaluarConexionCentral(SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice"), 3);
        if( hayComunicacion ){
            SION.log(Modulo.VENTA, "Hay comunicación a central se validaran los bloqueos...", Level.INFO);
            
            if (existenDatosSesion && ((BloqueoVentaBean) datosSesion).getCdgError() != -1) {
                //Si hay datos en sesion
                //Evaluar datos bloqueo
                //BloqueoVentaBean datosSesion = (BloqueoVentaBean) VistaBarraUsuario.getSesion().getBloqueosVenta();
                mensajeBloqueoAviso = bloqueoVenta.buscarBloqueo(datosSesion.getMensajeBloqueo(), VistaBarraUsuario.getSesion().getNombreUsuario());
                if (mensajeBloqueoAviso.estaBloqueado()) {
                    SION.log(Modulo.VENTA, "Obteniendo Datos de bloqueo", Level.INFO);
                    obtenerDatosBloqueos(bloqueoVenta, boton);
                } else {
                    if (mensajeBloqueoAviso.existeAviso()) {
                        SION.log(Modulo.VENTA, "Se encontró mensaje de aviso de bloqueo " + mensajeBloqueoAviso.getMensajeAviso(), Level.INFO);

                        existeAvisoBloqueo = true;
                        mensajeAvisoBloqueo = mensajeBloqueoAviso.getMensajeAviso();

                    }
                }

            } else if (existenDatosSesion && ((BloqueoVentaBean) datosSesion).getCdgError() == -1) {

                SION.log(Modulo.VENTA, "No existen Datos de Bloqueo", Level.INFO);
            } else {
                SION.log(Modulo.VENTA, "Se encontraron Datos de bloqueo", Level.INFO);
                obtenerDatosBloqueos(bloqueoVenta, boton);
            }
        }else{
            SION.log(Modulo.VENTA, "No se validan los bloqueos debido a que no hay comunicación", Level.INFO);
        }
    }

    public void obtenerDatosBloqueos(BloqueoVenta bloqueoVenta, final Button boton) {
        SION.log(Modulo.VENTA, "Entrando a obtener datos del bloqueo", Level.INFO);

        mensajeBloqueo = "";
        existeBloqueo = false;

        peticionBloqueoDto.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
        //peticion.setPaTienda(VistaBarraUsuario.getSesion().getTiendaId());
        peticionBloqueoDto.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
        peticionBloqueoDto.setPaUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());

        datosSesion = null;
        datosBloqueo = null;

        try {
            datosSesion = bloqueoVenta.existeComunicacionCentral(peticionBloqueoDto);
        } catch (Exception e) {
            SION.log(Modulo.VENTA, "Ocurrió un error al intentar comunicarse con central: " + e.toString(), Level.FINEST);
        }

        boolean comunicacion = datosSesion.existeComunicacion();
        if (comunicacion) {
            //Subimos datos a sesion
            SION.log(Modulo.VENTA, "Subiendo Datos a sesión", Level.INFO);

            VistaBarraUsuario.getSesion().setBloqueosVenta(datosSesion);
            datosBloqueo = datosSesion.getMensajeBloqueo();
            if (datosBloqueo == null) {
                SION.log(Modulo.VENTA, "Los datos de bloqueo vienen nulos", Level.INFO);

                //BloqueoVentaBean datosSesion = new BloqueoVentaBean();
                datosSesion.setCdgError(-1);
                VistaBarraUsuario.getSesion().setBloqueosVenta(datosSesion);

                return;
            }
            SION.log(Modulo.VENTA, "Se procede a verificar si el usuario se encuentra bloqueado", Level.FINEST);
            mensajeBloqueoAviso = bloqueoVenta.buscarBloqueo(datosBloqueo, VistaBarraUsuario.getSesion().getNombreUsuario());
            if (mensajeBloqueoAviso.estaBloqueado()) {

                existeBloqueo = true;
                mensajeBloqueo = mensajeBloqueoAviso.getMensajeBloqueo();
                SION.log(Modulo.VENTA, "Usuario bloqueado. Lanzando aplicación de traspasos", Level.INFO);


            } else {
                if (mensajeBloqueoAviso.existeAviso()) {

                    existe2oAvisoBloqueo = true;
                    mensaje2oAvisoBloqueo = mensajeBloqueoAviso.getMensajeAviso();
                    SION.log(Modulo.VENTA, "El usuario no se encuentra bloqueado pero se encontraron avisos. "
                            + "Mostrando aviso de bloqueo", Level.INFO);

                } else {
                    SION.log(Modulo.VENTA, "No se encontraron bloqueos para el usuario", Level.FINEST);
                }
            }


        }
    }

    //limpiar
    public void limpiarListasDto() {

        SION.log(Modulo.VENTA, "Limpiando listas DTO", Level.INFO);

        listaOrdenada.clear();
        listaTiposPago.clear();

    }

    public void limpiarListasBean() {

        SION.log(Modulo.VENTA, "Limpiando listas de datos", Level.INFO);

        listaArticulosTA.clear();
        listaDatosFormasPago.clear();
        listaDatosFormasPagoTarjeta.clear();
        listaDatosTiempoAire.clear();
        listaArticulos.clear();
        listaArticulosCancelados.clear();
        listaPagosTarjeta.clear();

        AdministraVentanas.agregarESCEventHandler();
    }

    
    public void borrarRegistrosValesEfectivo() {
        for (int i = 0; i < listaDatosFormasPago.size(); i++) {
            if (listaDatosFormasPago.get(i).getFitipopagoid() == 1 || listaDatosFormasPago.get(i).getFitipopagoid() == 3) {
                listaDatosFormasPago.remove(i);
            }
        }
    }
    
    public void validaTarjetaPagaTodo(){
        hayTarjetaPagaTodo=false;
        if( listaDatosFormasPago != null ){
            for (int i = 0; i < listaDatosFormasPago.size(); i++) {
                if( listaDatosFormasPago.get(i).isEsTarjetaPagaTodo() ){
                    hayTarjetaPagaTodo = true;
                    break;
                }
            }
        }
    }
    
    public DatosFormasPagoBean obtenerTarjetaPagaTodo(){
        DatosFormasPagoBean tpt = null;
        if( listaDatosFormasPago != null ){
            for (int i = 0; i < listaDatosFormasPago.size(); i++) {
                if( listaDatosFormasPago.get(i).isEsTarjetaPagaTodo() ){
                    tpt = listaDatosFormasPago.get(i);
                    break;
                }
            }
        }
        return tpt;
    }
    
    public void removerPagoPagaTodo(){
        int ixdRemover = -1;
        for (int i = 0; i < getListaFormasPago().size(); i++) {
            if( listaDatosFormasPago.get(i).isEsTarjetaPagaTodo() ){
               ixdRemover = i;
            }
            //listaDatosFormasPagoTarjeta.get(0).;
        }
        if( ixdRemover >= 0){
            listaDatosFormasPago.remove(ixdRemover);
            SION.log(Modulo.VENTA, ixdRemover+": indice PT removido", Level.INFO);
        }else{
            SION.log(Modulo.VENTA, "Pago PT no encontrado", Level.INFO);
        }
        
        ixdRemover = -1;
        for (int i = 0; i < getListaPagosTarjeta().size(); i++) {
            OperacionTarjetaBean c = getListaPagosTarjeta().get(i);
            if( c.getRespuestaPago().getDescTarjeta().equals("PAGATODO") ){
                ixdRemover =i;
            }
        }
        if( ixdRemover >= 0){
            getListaPagosTarjeta().remove(ixdRemover);
             SION.log(Modulo.VENTA, ixdRemover+": indice PT removido", Level.INFO);
        }else{
            SION.log(Modulo.VENTA, "Pago PT no encontrado", Level.INFO);
        }
        
       
    }
    
    private boolean hayTarjetaPagaTodo;
    public boolean hayTarjetaPagaTodo() {
        return hayTarjetaPagaTodo;
    }
    public void resetHayTarjetaPagaTodo() {
        hayTarjetaPagaTodo = false;
    }

    /*
     * ------------------------------------------------------------------------
     */
    ////////////consulta articulo local
    public void llenarPeticionArticuloLocal(double cantidad, String codigoBarras) {
        peticionArticuloLocalBean = null;
        peticionArticuloLocalBean = new PeticionDetalleArticuloBean();
        peticionArticuloLocalBean.setCantidad(cantidad);
        peticionArticuloLocalBean.setCodigoBarras(codigoBarras);
        peticionArticuloLocalBean.setVentana(VENTANA_VENTA);

    }

    public RespuestaDetalleArticuloBean consultarArticuloLocal() {
        respuestaArticuloLocalBean = null;
        if (peticionArticuloLocalBean != null) {
            respuestaArticuloLocalBean = new RespuestaDetalleArticuloBean();
            respuestaArticuloLocalBean = daoArticulos.consultaArticulo(peticionArticuloLocalBean);
        }
        return respuestaArticuloLocalBean;
    }
    
     public RespuestaDetalleArticuloBean consultarArticuloLocalConDescuento() {
        if (peticionArticuloLocalBean != null) {
            respuestaArticuloLocalBean = null;
            respuestaArticuloLocalBean = new RespuestaDetalleArticuloBean();
            respuestaArticuloLocalBean = daoArticulos.consultaArticulo(peticionArticuloLocalBean);
            
            ArticuloBean aDesc = ctrlTarjetasDescuento.buscarArticuloXCBDescuento(peticionArticuloLocalBean.getCodigoBarras());
            if( aDesc != null ){
                respuestaArticuloLocalBean.getArticulos().setPadescuento(aDesc.getPadescuento());
                respuestaArticuloLocalBean.getArticulos().setPaprecio(aDesc.getPaprecio());
                respuestaArticuloLocalBean.getArticulos().setImporte(
                        (aDesc.getPaprecio()-aDesc.getPadescuento()) * aDesc.getCantidad() );
            }
        }
        return respuestaArticuloLocalBean;
    }

    public int getCodigoErrorRespuestaArticuloLocal() {
        return respuestaArticuloLocalBean.getCodigoError();
    }

    public ArticuloBean getArticuloLocal() {
        return respuestaArticuloLocalBean.getArticulos();
    }
    ////////fin consulta articulo local

    //// insecion de articulos de venta
    public void articuloDtoaBean(ArticuloDto dto, int index) {
        ArticuloVendidoBean bean = new ArticuloVendidoBean();
        bean.setArticuloId(dto.getFiArticuloId());
        bean.setCantidad(dto.getFnCantidad());
        bean.setCodigoBarras(dto.getFcCdGbBarras());
        bean.setCosto(dto.getFnCosto());
        bean.setDescuento(dto.getFnDescuento());
        bean.setIva(dto.getFnIva());
        bean.setPrecio(dto.getFnPrecio());
        bean.setIeps(dto.getFnIepsId());
        bean.setTipoDescuento(dto.getTipoDescuento());
        articuloVendidoBeans[index] = bean;
        bean = null;
    }

    public void llenarPeticionVentaDelDia() {
        int contador = 0;
        peticionArticulosVendidosBean = null;
        articuloVendidoBeans = null;
        articuloVendidoBeans = new ArticuloVendidoBean[listaOrdenada.size()];

        for (ArticuloDto dto : listaOrdenada) {
            articuloDtoaBean(dto, contador);
            contador++;
        }

        peticionArticulosVendidosBean = new PeticionArticulosVendidosBean();
        peticionArticulosVendidosBean.setArticuloVendidoBeans(articuloVendidoBeans);
    }

    public void insertarVentaDelDia() {
        respuestaArticulosVendidosBean = null;
        respuestaArticulosVendidosBean = new RespuestaArticulosVendidosBean();
        respuestaArticulosVendidosBean = vendidosDao.insertaArticulosVendidos(peticionArticulosVendidosBean);
        if (respuestaArticulosVendidosBean != null) {
            if (respuestaArticulosVendidosBean.getCodigoError() == 0) {
                SION.log(Modulo.VENTA, "Articulos de venta insertados en Bd ", Level.INFO);
            } else {
                SION.log(Modulo.VENTA, "No se pudieron insertar los artículos de venta", Level.SEVERE);
            }
        } else {
            SION.log(Modulo.VENTA, "instancia de respuesta de articulos vendidos para reporte nula", Level.SEVERE);
        }
    }

    ////fin insercion de articulos de venta
    ////confirmacion de venta tiempo aire
    public void llenarPeticionConfirmacion(long conciliacion, int sistema, int modulo, int submodulo) {

        SION.log(Modulo.VENTA, "Entrando a método de envío de petición de actualización de transacción central", Level.FINEST);

        peticionTransaccionCentralDto = null;
        peticionTransaccionCentralDto = new PeticionTransaccionCentralDto();
        sesion = VistaBarraUsuario.getSesion();

        peticionTransaccionCentralDto.setPaPaisId(sesion.getPaisId());
        peticionTransaccionCentralDto.setTiendaId(sesion.getTiendaId());
        peticionTransaccionCentralDto.setPaConciliacionId(conciliacion);
        peticionTransaccionCentralDto.setPaUsuarioId(sesion.getUsuarioId());
        peticionTransaccionCentralDto.setPaSistemaId(sistema);
        peticionTransaccionCentralDto.setPaModuloId(modulo);
        peticionTransaccionCentralDto.setPaSubModuloId(submodulo);

        SION.log(Modulo.VENTA, "Petición de actualización  : " + peticionTransaccionCentralDto.toString(), Level.FINEST);

    }

    public long respuestaTransaccionCentral() {

        SION.log(Modulo.VENTA, "Consultando transacción asociada a la conciliación", Level.FINEST);

        hayMarcacionFueraLinea = false;

        try {
            respuestaConsultaTransaccionTADto = consumirVentaImp.consultarTransaccionTACentral(peticionTransaccionCentralDto);
            SION.log(Modulo.VENTA, "Se recibió respuesta de transacción: " + respuestaConsultaTransaccionTADto.toString(), Level.INFO);
        } catch (SionException ex) {
            SION.log(Modulo.VENTA, "Ocurrió un error al intentar obtener respuesta de la transacción: " + ex.getMensajeFront().replace("##", String.valueOf(ex.getCodigoAccion())), Level.SEVERE);

            hayMarcacionFueraLinea = true;
        }

        return respuestaConsultaTransaccionTADto.getCdgError();
    }

    public long getTransaccionConfirmacionTA() {
        return respuestaConsultaTransaccionTADto.getNumTransaccion();
    }

    public long getAutorizacionConfirmacionTA() {
        return respuestaConsultaTransaccionTADto.getNumAutorizacion();
    }
    ////fin de confirmacion de tiempo aire

    /// parametros de operacion
    public void llenarPeticionParametro(int pais, int sistema, int modulo, int submodulo, int configuracionId) {
        peticionParametroBean = null;
        peticionParametroBean = new PeticionParametroBean();

        peticionParametroBean.setConfiguracionId(configuracionId);
        peticionParametroBean.setModuloId(modulo);
        peticionParametroBean.setPaisId(pais);
        peticionParametroBean.setSistemaId(sistema);
        peticionParametroBean.setSubmoduloId(submodulo);
    }
    
    public static boolean hayParametroFueraLineaActivo(){
        boolean existeParametroVentaFueraLinea = false;
        
	PeticionParametroBean peticionParametroBean = new PeticionParametroBean();

	peticionParametroBean.setPaisId             (VistaBarraUsuario.getSesion().getPaisId());
	peticionParametroBean.setSistemaId          (Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.sistema")));
	peticionParametroBean.setModuloId           (Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.modulo")));
	peticionParametroBean.setSubmoduloId        (Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.submodulo")));
	peticionParametroBean.setConfiguracionId    (Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.configuracion")));
	
	RespuestaParametroBean respuestaParametroBean = null;
        respuestaParametroBean = new RespuestaParametroBean();
        ParametroDao parametroDao = new ParametroDao();
        if (peticionParametroBean != null) {
            respuestaParametroBean = parametroDao.consultaParametroOperacion(peticionParametroBean);
        }
        
	if (respuestaParametroBean != null){
		if ((respuestaParametroBean.getCodigoError() == 0) && 
		((int)respuestaParametroBean.getValorConfiguracion() == 1))
		{
			existeParametroVentaFueraLinea = true;
		} else {
			existeParametroVentaFueraLinea = false;
		}
	} else {
		existeParametroVentaFueraLinea = false;
	}
        
        return existeParametroVentaFueraLinea;
    }

    public RespuestaParametroBean consultarParametro() {
        respuestaParametroBean = null;
        respuestaParametroBean = new RespuestaParametroBean();
        if (peticionParametroBean != null) {
            respuestaParametroBean = parametroDao.consultaParametroOperacion(peticionParametroBean);
        }
        return respuestaParametroBean;
    }

    public int getCodigoErrorParametroOperacion() {
        return respuestaParametroBean.getCodigoError();
    }

    public double getValorConfiguracionParametroOperacion() {
        return respuestaParametroBean.getValorConfiguracion();
    }

    public String getDescErrorParametroBean() {
        return respuestaParametroBean.getDescripcionError();
    }

    ///fin parametros de operacion
    ///cancelacion de la venta
    public ObservableList<ArticuloBean> getListaArticulosCancelados() {
        return listaArticulosCancelados;
    }

    public int getCantidadArticulosCancelados() {
        int contador = 0;
        int i = 0;
        while (i < listaArticulosCancelados.size()) {
            contador += listaArticulosCancelados.get(i).getCantidad();
            i++;
        }
        return contador;
    }

    public int getCantidadLineasCanceladas() {
        int contador = 0;
        int i = 0;

        while (i < listaArticulos.size()) {
            if (listaArticulos.get(i).getImporte() < 0) {
                contador++;
            }
            i++;
        }
        return contador;
    }

    public void llenarListaArticulosCancelados() {

        int i = 0;
        boolean seEncontroArticuloEnLista = false;

        while (i < listaArticulos.size()) {

            ///proceso para llenar lista de articulos cancelados
            if (listaArticulos.get(i).getImporte() < 0) {

                for (int j = 0; j < listaArticulosCancelados.size(); j++) {
                    if (listaArticulosCancelados.get(j).getPacdgbarras().equals(listaArticulos.get(i).getPacdgbarras())) {
                        seEncontroArticuloEnLista = true;
                        listaArticulos.get(i).setImporte(listaArticulos.get(i).getImporte() * -1);
                        listaArticulosCancelados.get(j).setCantidad( listaArticulosCancelados.get(j).getCantidad() + listaArticulos.get(i).getCantidad());
                        //i=listaArticulos.size();
                    }
                }

                if (!seEncontroArticuloEnLista) {
                    listaArticulos.get(i).setImporte(listaArticulos.get(i).getImporte() * -1);
                    listaArticulosCancelados.add(listaArticulos.get(i));
                }
            }
            seEncontroArticuloEnLista = false;
            i++;
        }

        if (listaArticulosCancelados != null && listaArticulosCancelados.size() > 0) {
            SION.log(Modulo.VENTA, "Lista de artículos cancelados: " + listaArticulosCancelados.toString(), Level.INFO);
        }
    }

    public void llenarPeticionArticulosCancelados(long _usuarioCancela, long _usuarioAutoriza, long _transaccion, String _fecha, int _lineasCanceladas, int _ventasCanceladas) {
        peticionCancelacionVentaBean = null;
        peticionCancelacionVentaBean = new PeticionCancelacionVentaBean();
        peticionCancelacionVentaBean.setFecha(_fecha);
        peticionCancelacionVentaBean.setLineasCanceladas(_lineasCanceladas);
        peticionCancelacionVentaBean.setNumeroTransaccion(_transaccion);
        peticionCancelacionVentaBean.setUsuarioAutoriza(_usuarioAutoriza);
        peticionCancelacionVentaBean.setUsuarioCancela(_usuarioCancela);
        peticionCancelacionVentaBean.setVentasCanceladas(_ventasCanceladas);

    }

    public void insertarArticulosCancelados() {
        respuestaCancelacionVentaBean = null;
        if (peticionCancelacionVentaBean != null) {
            respuestaCancelacionVentaBean = new RespuestaCancelacionVentaBean();
            respuestaCancelacionVentaBean = cancelacionVentaDao.insertaCancelacionVenta(peticionCancelacionVentaBean);
        }
    }

    public int getCodigoErrorCancelacionVenta() {
        return respuestaCancelacionVentaBean.getCodigoError();
    }

    public String getDescErrorCancelacionVenta() {
        return respuestaCancelacionVentaBean.getDescripcionError();
    }
    ///fin cancelacion de la venta

    ///bloqueos fuera de linea
    public List<ObjetoBloqueo> getArregloBloqueosLocal() {
        return arregloBloqueosLocal;
    }

    public List<ObjetoBloqueo> consultaBloqueosLocal(long _conciliacion) {
        ///verificar bloqueos fuera de linea
        SION.log(Modulo.VENTA, "Consultando bloqueos bd local", Level.INFO);

        existeBloqueoLocal = false;
        existeAvisoLocal = false;
        tipoBloqueo = 0;

        arregloBloqueosLocal = bloqueoLocal.consultaStatusBloqueo(VistaBarraUsuario.getSesion().getUsuarioId());

        SION.log(Modulo.VENTA, "Arreglo de bloqueos recibido: " + arregloBloqueosLocal.toString(), Level.INFO);

        SION.log(Modulo.VENTA, "Estableciendo datos de bloqueo en sesion", Level.INFO);

        BloqueoBean[] arregloBloqueosSesion = new BloqueoBean[arregloBloqueosLocal.size()];
        int contadorBloqueo = 0;

        for (ObjetoBloqueo bloqueo : arregloBloqueosLocal) {
            BloqueoBean bean = new BloqueoBean();
            bean.setAvisosRestantes(bloqueo.getNumAvisosRestantes());
            bean.setFiEstatusBloqueoId(bloqueo.getStatusBloqueo());
            bean.setFiNumAvisos(bloqueo.getNumAvisos());
            bean.setFiTipoPagoId(bloqueo.getTipoPagoId());
            arregloBloqueosSesion[contadorBloqueo] = bean;
            contadorBloqueo++;
        }

        try {
            SION.log(Modulo.VENTA, "Cargando datos de bloqueos de venta: " + Arrays.toString(arregloBloqueosSesion), Level.INFO);
            VistaBarraUsuario.getSesion().setBloqueosVenta(bloqueoVenta.evaluaBloqueosLocal(arregloBloqueosSesion, VistaBarraUsuario.getSesion().getNombreUsuario()));
            SION.log(Modulo.VENTA, "Datos de sesion cargados exitosamente", Level.INFO);
            SION.log(Modulo.VENTA, "codigo de bloque de venta actualizado en sesion: " + VistaBarraUsuario.getSesion().getBloqueosVenta().getCdgError(), Level.INFO);
        } catch (Exception e) {
            arregloBloqueosLocal = null;
            SION.log(Modulo.VENTA, "Ocurrió un error al establecer datos de sesion " + e.toString(), Level.SEVERE);
        }


        for (ObjetoBloqueo bloqueo : arregloBloqueosLocal) {
            if (bloqueo.getStatusBloqueo() == 1) {
                tipoBloqueo = bloqueo.getTipoPagoId();
                existeBloqueoLocal = true;
                break;
            } else if (bloqueo.getNumAvisosRestantes() > 0 && bloqueo.getNumAvisosRestantes() <= 3) {
                tipoBloqueo = bloqueo.getTipoPagoId();
                existeAvisoLocal = true;
                avisosRestantesBloqueoLocal = bloqueo.getNumAvisosRestantes();
                break;
            }
        }
        return arregloBloqueosLocal;


    }

    public boolean existeBloqueoLocal() {
        return existeBloqueoLocal;
    }

    public boolean existeAvisoBloqueoLocal() {
        return existeAvisoLocal;
    }

    public int getTipoBloqueolocal() {
        return tipoBloqueo;
    }

    public int getAvisosRestantesLocal() {
        return avisosRestantesBloqueoLocal;
    }
    ///fin bloqueos fuera de linea

    public boolean esTarjetaValida(String _numeroTarjeta) {
        boolean esValida = false;

        peticionValidacionTarjeta = null;
        respuestaValidacionTarjeta = null;

        peticionValidacionTarjeta = new PeticionValidacionTarjetaBean();
        respuestaValidacionTarjeta = new RespuestaValidacionTarjetaBean();

        peticionValidacionTarjeta.setNumerotarjeta(_numeroTarjeta);

        respuestaValidacionTarjeta = validacionTarjetaDao.validartarjeta(peticionValidacionTarjeta);

        if (respuestaValidacionTarjeta.getCodigoError() == 0) {
            esValida = true;
            SION.log(Modulo.VENTA, "Tarjeta valida, se continua con la venta", Level.INFO);
        } else {
            SION.log(Modulo.VENTA, "Tarjeta no validada, se cancela la venta", Level.WARNING);
        }

        return esValida;
    }

    public RespuestaValidacionTarjetaBean getRespuestaValidacionTarjeta() {
        return respuestaValidacionTarjeta;
    }
    
    
    public PeticionPagoTarjetaBean generPeticionTarjetaDescuento(){
        SION.log(Modulo.VENTA, "Generando petición de tarjeta de descuento.", Level.INFO);
        PeticionPagoTarjetaBean peticion = new PeticionPagoTarjetaBean();
        
        peticion.setSePermiteCreditoDebito(true);
        peticion.setSePermiteVales(true);
        peticion.setSeGeneroPeticion(true);
        peticion.setMonto(0);
        peticion.setMensaje("Petición generada exitosamente.");
        peticion.setNumTerminal     (agregaCeros(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion())));
        peticion.setPaisId          (VistaBarraUsuario.getSesion().getPaisId());
        peticion.setTiendaId        (VistaBarraUsuario.getSesion().getTiendaId());
        peticion.setCanal           (VistaBarraUsuario.getSesion().getDatosTienda().getCanalTarjeta());
        peticion.setAfiliacion      (VistaBarraUsuario.getSesion().getDatosTienda().getAfiliacion().trim());
        
        return peticion;
    }

    ///////////////implementacion chip\\\\\\\\\\\\\\\
    public PeticionPagoTarjetaBean generaPeticionPagoTarjeta(double _monto) {
        SION.log(Modulo.VENTA, "Generando petición de tarjeta para componente Chip", Level.INFO);
        PeticionPagoTarjetaBean peticion = new PeticionPagoTarjetaBean();

        peticion.setSePermiteCreditoDebito(true);
        peticion.setSePermiteVales(true);

        boolean ocurrioErrorEnConsultaParametrosTarjetas = false;
        RespuestaParametroBean respuestaParametro = new RespuestaParametroBean();
        double comisionTarjetaCredito = 0;
        double comisionTarjetaDebito = 0;
        int maximoTarjetasCreditoDebito = 0;
        int maximoTarjetasValesE = 0;

        //consultando monto maximo de tarjetas de credito/debito permitidas por operacion
        //respuestaParametro = utilerias.consultaParametro(UtileriasVenta.Accion.LIMITE_TARJETAS, Modulo.VENTA, Modulo.VENTA);
		llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteTarjetas.sistema").trim()), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteTarjetas.modulo").trim()), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteTarjetas.submodulo".trim())), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteTarjetas.configuracion").trim()));
		respuestaParametro = consultarParametro();
        if (respuestaParametro != null) {
            if (respuestaParametro.getCodigoError() == 0) {
                maximoTarjetasCreditoDebito = (int) respuestaParametro.getValorConfiguracion();
                if (maximoTarjetasCreditoDebito <= getCantidadPagosTarjetas(TARJETA_CREDITO)) {
                    peticion.setSePermiteCreditoDebito(false);
                }
            } else {
                SION.log(Modulo.VENTA, respuestaParametro.getDescripcionError().concat(", no se pudo obtener máximo permitido para tarjetas de crédito/débito"), Level.SEVERE);
                ocurrioErrorEnConsultaParametrosTarjetas = true;
            }
        } else {
            SION.log(Modulo.VENTA, "Respuesta de parametro nula, no se pudo obtener máximo permitido para tarjetas de crédito/débito", Level.SEVERE);
            ocurrioErrorEnConsultaParametrosTarjetas = true;
        }

        //consultando monto maximo de tarjetas de vales electronicos permitidas por operacion
        llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteValesE.sistema").trim()), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteValesE.modulo").trim()), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteValesE.submodulo".trim())), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "limiteValesE.configuracion").trim()));
   

	    respuestaParametro = consultarParametro();
        if (respuestaParametro != null) {
            if (respuestaParametro.getCodigoError() == 0) {
                maximoTarjetasValesE = (int) respuestaParametro.getValorConfiguracion();
                if (maximoTarjetasValesE <= getCantidadPagosTarjetas(VALE_ELECTRONICO)) {
                    peticion.setSePermiteVales(false);
                }
            } else {
                SION.log(Modulo.VENTA, respuestaParametro.getDescripcionError().concat(", no se pudo obtener máximo permitido para tarjetas de vale electrónico"), Level.SEVERE);
                ocurrioErrorEnConsultaParametrosTarjetas = true;
            }
        } else {
            SION.log(Modulo.VENTA, "Respuesta de parametro nula, no se pudo obtener máximo permitido para tarjetas de vale electrónico", Level.SEVERE);
            ocurrioErrorEnConsultaParametrosTarjetas = true;
        }

        if (maximoTarjetasCreditoDebito > 0 && maximoTarjetasValesE > 0) {


            SION.log(Modulo.VENTA, "Lista de tarjetas: " + listaPagosTarjeta.toString(), Level.INFO);
            if (listaPagosTarjeta.size() < (maximoTarjetasCreditoDebito + maximoTarjetasValesE)) {
                if (!ocurrioErrorEnConsultaParametrosTarjetas) {
                    //peticion.setUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());
                    peticion.setMonto(_monto);
                    peticion.setNumTerminal(agregaCeros(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion())));
                    peticion.setPaisId(VistaBarraUsuario.getSesion().getPaisId());
                    peticion.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
                    peticion.setCanal(VistaBarraUsuario.getSesion().getDatosTienda().getCanalTarjeta());
                    peticion.setSeGeneroPeticion(true);
                    peticion.setMensaje("Petición generada exitosamente.");
                    peticion.setAfiliacion(VistaBarraUsuario.getSesion().getDatosTienda().getAfiliacion().trim());
                } else {
                    peticion.setSeGeneroPeticion(false);
                    peticion.setMensaje("Ocurrió un error al consultar comisiones de tarjetas bancarias. Por favor contacte a soporte técnico");
                }
            } else {
                peticion.setSeGeneroPeticion(false);
                peticion.setMensaje("Se ha alcanzado el máximo de tarjetas para esta operación.");
            }

        } else {
            peticion.setSeGeneroPeticion(false);
            peticion.setMensaje("Por el momento no esta permitida la venta con tarjetas. Por favor intente más tarde.");
        }

        SION.log(Modulo.VENTA, "Petición generada: " + peticion.toString(), Level.INFO);

        return peticion;
    }
    
    public boolean hayTarjetaDescuento(){
        return getCtrlTarjetasDescuento().hayTarjetasDesceunto();
    }
    
    public boolean creartarjetaDescuento(DatosTarjeta tarjeta){
       return getCtrlTarjetasDescuento().creartarjetaDescuento(tarjeta);                         
    }
    
    public void actualizaArticulosXValidar(List<ArticuloBean> articulos){
        getCtrlTarjetasDescuento().actualizaArticulosXValidar(articulos);
    }

    public int getCantidadPagosTarjetas(int _tipoTarjeta) {
        int contador = 0;
        for (OperacionTarjetaBean operacionTarjeta : listaPagosTarjeta) {
            switch (_tipoTarjeta) {
                case TARJETA_CREDITO:
                    if (operacionTarjeta.getRespuestaPago().getTipoTarjeta() == 1
                            || operacionTarjeta.getRespuestaPago().getTipoTarjeta() == 2) {
                        contador++;
                    }
                    break;
                case VALE_ELECTRONICO:
                    if (operacionTarjeta.getRespuestaPago().getTipoTarjeta() == 3) {
                        contador++;
                    }
                    break;
            }
        }
        return contador;
    }
    
    public RespuestaLeerTarjetaDescuentoBean leerTarjeta(){
        controladorPinpad = null;
	controladorPinpad = new ControladorTarjeta();
        SION.log(Modulo.VENTA, "Petición leer tarjeta enviada a componente Pinpad.", Level.INFO);
        RespuestaLeerTarjetaDescuentoBean respuesta = controladorPinpad.leerTarjetaDescuento();
        
        return respuesta;
    }


    
    public RespuestaPagoTarjetaBean aplicarPagoTarjeta(PeticionPagoTarjetaBean _peticion) {
        
        if (_peticion != null){
            RespuestaPagoTarjetaBean respuestaTarjeta = new RespuestaPagoTarjetaBean();
	    SION.log(Modulo.VENTA, "Aplicando pago con tarjeta", Level.INFO);
            controladorPinpad = null;
	    controladorPinpad = new ControladorTarjeta();
              
              //solicitudPagaTdo = new neto.sion.tienda.venta.wssion.dto.PeticionPagatodoDto();
              //peticionPagoTar = new PeticionPagoTarjetaDto();

	    SION.log(Modulo.VENTA, "Petición enviada a componente Pinpad: " + _peticion.toString(), Level.INFO);
              
              //RespuestaPagoTarjetaFrontBean respuestaPeticionPinPad = controladorPinpad.peticionPago(peticionPago2PeticionPagoComponente(_peticion));
            RespuestaPagoTarjetaFrontBean respuestaPeticionPinPad = controladorPinpad.peticionPago(peticionPago2PeticionPagoComponente(_peticion));
              
            
              
              
              
	    respuestaTarjeta = respuestaComponenteChip2RespuestaTarjeta(respuestaPeticionPinPad);
              //respuestaTarjeta = respuestaComponenteChip2RespuestaTarjeta(controladorPinpad.peticionPago(peticionPago2PeticionPagoComponente(_peticion)));
              
              //SION.log(Modulo.VENTA, "Solicitud de paga todo:" + solicitudPagaTdo, Level.INFO);
              //SION.log(Modulo.VENTA, "Respuesta recibida de paga todo:" + peticionPagoTar, Level.INFO);
              
	      if (respuestaTarjeta.getCodigoError() == 0)
	      {
	        SION.log(Modulo.VENTA, "Respuesta recibida de componente Chip:" + respuestaTarjeta.toString(), Level.INFO);
	        OperacionTarjetaBean operacion = new OperacionTarjetaBean();
	        operacion.setPeticionPago(_peticion);
	        operacion.setRespuestaPago(respuestaTarjeta);
	      }
	      controladorPinpad = null;
	      System.gc();
      
	      return respuestaTarjeta;
	    }
	    return null;
    }
/*
    public RespuestaPagoTarjetaBean armaRespuestaTest(RespuestaPagoTarjetaBean _respuesta) {

        Random r = new Random();

        _respuesta.setCodigoError(0);
        _respuesta.setDescError("Aceptada");
        _respuesta.setNombreUsuario("");
        _respuesta.setNumeroAutorizacion(String.valueOf(r.nextInt(10000)));
        _respuesta.setNumerotarjeta("************3615");
        _respuesta.setTipoTarjeta(2);
        _respuesta.setDescTarjeta("");
        _respuesta.setEstatusLectura("1");
        //_respuesta.setAID("A0000000041010");
        //_respuesta.setARQC("B533C3E65198DA08");

        PeticionReversoTarjetaBean peticionReverso = new PeticionReversoTarjetaBean();
        /peticionReverso.setAfiliacion("7902067");
        peticionReverso.setChip(true);
        peticionReverso.setComision(0.05);
        peticionReverso.setFalloLecturaChip(false);
        peticionReverso.setFechaLocal("0401");
        peticionReverso.setHoraLocal("135307");
        peticionReverso.setMonto(50);
        peticionReverso.setPaConciliacionId(0);////////////////
        peticionReverso.setPaPaisId(1);
        peticionReverso.setPagoTarjetaId(r.nextInt(100));//////////////////
        peticionReverso.setTerminal("01");
        peticionReverso.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
        peticionReverso.setTrace("671812");
        peticionReverso.setTrack2("ñ4762020201713615D17032210000007100000Ff_");

        _respuesta.setPeticionReverso(peticionReverso);/


        return _respuesta;
    }*/

    public RespuestaReversoTarjetaDto armarRespuestaReversoTest() {
        RespuestaReversoTarjetaDto respuesta = new RespuestaReversoTarjetaDto();

        respuesta.setAfiliacion(VistaBarraUsuario.getSesion().getDatosTienda().getAfiliacion());
        respuesta.setAutorizacion(null);
        respuesta.setBitMap("323800000AC00000");
        respuesta.setCodError(0);
        respuesta.setCodProceso("000000");
        respuesta.setCodRespuesta("00");
        respuesta.setTrace("672088");
        respuesta.setDescError("Operación Exitosa");
        respuesta.setDescripcion(null);
        respuesta.setFechaHora("0522221255");
        respuesta.setFechaLocal("0522");
        respuesta.setHoraLocal("171223");
        respuesta.setMensaje("Aprobada");
        respuesta.setMonto("000000001000");
        respuesta.setReferencia("546909672088");
        respuesta.setTarjetaId(null);
        respuesta.setTerminal("54690903");
        respuesta.setTipoMensaje("0430");

        SION.log(Modulo.VENTA, "Respuesta reverso tarjeta: " + respuesta, Level.INFO);

        return respuesta;
    }

    public void reversarPagoTarjeta() {

        SION.log(Modulo.VENTA, "Inicia proceso de reversos de pago con tarjeta", Level.INFO);
	    for (int i = 0; i < listaPagosTarjeta.size(); i++)
	    {
	      controladorPinpad = null;
	      controladorPinpad = new ControladorTarjeta();
      
	      SION.log(Modulo.VENTA, "Intentando reversar tarjeta: " + ((OperacionTarjetaBean)listaPagosTarjeta.get(i)).getRespuestaPago().getPeticionReverso(), Level.INFO);
      
	      RespuestaReversoDto respuestaReverso = new RespuestaReversoDto();
	      try
	      {
                  if( !((OperacionTarjetaBean)listaPagosTarjeta.get(i)).getRespuestaPago().getDescTarjeta().equals("PAGATODO") ){
                    controladorPinpad.reversoPago(respuestaPagoTarjetaBean2RespuestaPagoTarjetaFrontBean(((OperacionTarjetaBean)listaPagosTarjeta.get(i)).getRespuestaPago()));
                  }else{
                    SION.log(Modulo.VENTA, "Para tarjetas PAGA TODO no hay reversos desde local...", Level.INFO);
                  }
	      }
	      catch (Exception ex)
	      {
	        //ex.printStackTrace();
	        SION.logearExcepcion(Modulo.VENTA, ex, new String[] { "Excepcion al aplicar reversos de tarjeta" });
	      }
	      catch (Error ex)
	      {
	        //ex.printStackTrace();
	        SION.log(Modulo.VENTA, "Error al aplicar reversos de tarjeta", Level.SEVERE);
	      }
	    }
	    listaPagosTarjeta.clear();
	    limpiarListasBean();
	    limpiarListasDto();

    }

    private PeticionPagoTarjetaFrontBean peticionPago2PeticionPagoComponente(PeticionPagoTarjetaBean _peticion) {
        if (_peticion != null) {
            PeticionPagoTarjetaFrontBean peticion = new PeticionPagoTarjetaFrontBean();

            peticion.setMonto(_peticion.getMonto());
            peticion.setNumTerminal(_peticion.getNumTerminal());
            peticion.setPaisId(_peticion.getPaisId());
            peticion.setSePermitenBancarias(_peticion.isSePermiteCreditoDebito());
            peticion.setSePermitenVales(_peticion.isSePermiteVales());
            peticion.setTiendaId(_peticion.getTiendaId());
            peticion.setCanalPago(_peticion.getCanal());
            peticion.setAfiliacion(_peticion.getAfiliacion());

            return peticion;
        } else {
            return null;
        }
    }

   private RespuestaPagoTarjetaFrontBean respuestaPagoTarjetaBean2RespuestaPagoTarjetaFrontBean(RespuestaPagoTarjetaBean _respuesta) {
        RespuestaPagoTarjetaFrontBean respuesta = new RespuestaPagoTarjetaFrontBean();

        respuesta.setCodError(_respuesta.getCodigoError());
        respuesta.setDescError(_respuesta.getDescError());
        respuesta.setNombreUsuario(_respuesta.getNombreUsuario());
        respuesta.setNumAutorizacion(String.valueOf(_respuesta.getNumeroAutorizacion()));
        respuesta.setNumeroTarjeta(_respuesta.getNumerotarjeta());
        respuesta.setPeticionReverso(peticionReverso2PeticionReversoComponenteChip2(_respuesta.getPeticionReverso()));
        respuesta.setTipoTarjeta(_respuesta.getTipoTarjeta());
        //respuesta.setAID(_respuesta.getAID());
        //respuesta.setARQC(_respuesta.getARQC());

        return respuesta;
    }

    private RespuestaPagoTarjetaBean respuestaComponenteChip2RespuestaTarjeta(RespuestaPagoTarjetaFrontBean _respuesta) {
        if (_respuesta != null) {

            try {
                SION.log(Modulo.VENTA, "Respuesta recibida de componente: " + _respuesta.toString(), Level.INFO);
            } catch (Exception e) {
                SION.log(Modulo.VENTA, "Error en toString", Level.WARNING);
            }

            RespuestaPagoTarjetaBean respuesta = new RespuestaPagoTarjetaBean();

            if (_respuesta.getCodError() == 0) {
                respuesta.setNombreUsuario(_respuesta.getNombreUsuario());
                try {
                    respuesta.setNumeroAutorizacion(_respuesta.getNumAutorizacion().trim());
                } catch (Exception e) {
                    if (_respuesta.getNumAutorizacion() != null) {
                        SION.logearExcepcion(Modulo.VENTA, e, "Error al parsear, número de autorización inválido: " + _respuesta.getNumAutorizacion());
                    } else {
                        SION.logearExcepcion(Modulo.VENTA, e, "Error al parsear, número de autorización inválido: NULL.");
                    }
                }
                respuesta.setNumerotarjeta(_respuesta.getNumeroTarjeta());
                respuesta.setPeticionReverso(peticionReversoComponenteChip2PeticionReverso(_respuesta.getPeticionReverso()));
                respuesta.setTipoTarjeta(_respuesta.getTipoTarjeta());
                respuesta.setDescTarjeta(_respuesta.getNombreTipoTarjeta());
                respuesta.setEstatusLectura(String.valueOf(_respuesta.getTipoLectura()));
                respuesta.setSolicitudPagaTdo(_respuesta.getSolicitudPagaTodoNip());
                respuesta.setPeticionPagoTarjeta(_respuesta.getPeticionPagoTarjeta());
                
                //respuesta.setAID(_respuesta.getAID());
                //respuesta.setARQC(_respuesta.getARQC());
            }

            if (_respuesta.getDescError() != null) {
                respuesta.setDescError(_respuesta.getDescError());
            } else {
                respuesta.setDescError("Por el momento no se encuentra disponible el servicio. Por favor intente mas tarde.");
            }

            respuesta.setCodigoError(_respuesta.getCodError());

            //if (respuesta.getCodigoError() == 0) {
                SION.log(Modulo.VENTA, "Respuesta despues de parsear: " + respuesta.toString(), Level.INFO);
            /*} else {
                SION.log(Modulo.VENTA, "Respuesta despues de parsear: " + respuesta._toString(), Level.INFO);
            }*/

            return respuesta;
        } else {
            return null;
        }
    }

    private PeticionReversoTarjetaBean peticionReversoComponenteChip2PeticionReverso(PeticionReversoTarjetaFrontBean _peticion) {
        if (_peticion != null) {
            PeticionReversoTarjetaBean peticion = new PeticionReversoTarjetaBean();

            peticion.setAfiliacion(_peticion.getAfiliacion());
            peticion.setChip(_peticion.isChip());
            peticion.setComision(_peticion.getComision());
            peticion.setFalloLecturaChip(_peticion.isFalloLecturaChip());
            peticion.setFechaLocal(_peticion.getFechaLocal());
            peticion.setHoraLocal(_peticion.getHoraLocal());
            peticion.setMonto(_peticion.getMonto());
            peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
            peticion.setPaPaisId(_peticion.getPaPaisId());
            peticion.setPagoTarjetaId(_peticion.getPagoTarjetaId());
            peticion.setTerminal(_peticion.getTerminal());
            peticion.setTiendaId(_peticion.getTiendaId());
            peticion.setTrace(_peticion.getTrace());
            peticion.setTrack2(_peticion.getTrack2());
            peticion.setCanalPago(_peticion.getCanalPago());

            return peticion;
        } else {
            return null;
        }
    }

    private PeticionReversoTarjetaFrontBean peticionReverso2PeticionReversoComponenteChip2(PeticionReversoTarjetaBean _peticion) {
        if (_peticion != null) {
            PeticionReversoTarjetaFrontBean peticion = new PeticionReversoTarjetaFrontBean();

            peticion.setAfiliacion(_peticion.getAfiliacion());
            peticion.setChip(_peticion.isChip());
            peticion.setComision(_peticion.getComision());
            peticion.setFalloLecturaChip(_peticion.isFalloLecturaChip());
            peticion.setFechaLocal(_peticion.getFechaLocal());
            peticion.setHoraLocal(_peticion.getHoraLocal());
            peticion.setMonto(_peticion.getMonto());
            peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
            peticion.setPaPaisId(_peticion.getPaPaisId());
            peticion.setPagoTarjetaId(_peticion.getPagoTarjetaId());
            peticion.setTerminal(_peticion.getTerminal());
            peticion.setTiendaId(_peticion.getTiendaId());
            peticion.setTrace(_peticion.getTrace());
            peticion.setTrack2(_peticion.getTrack2());
            peticion.setCanalPago(_peticion.getCanalPago());

            return peticion;
        } else {
            return null;
        }
    }

    private String agregaCeros(String _cadena) {
        SION.log(Modulo.VENTA, "Agrega ceros cadena recibida: " + _cadena, Level.INFO);
        String cadena = "";
        int longitud = _cadena.length();

        if (_cadena == null) {
            SION.log(Modulo.VENTA, "NULL encontrado en sesion Cadena devuelta 00", Level.INFO);
            return "00";
        } else {

            switch (longitud) {
                case 0:
                    cadena = "00";
                    break;
                case 1:
                    cadena = "0".concat(_cadena);
                    break;
                case 2:
                    cadena = _cadena;
                    break;
                default:
                    cadena = _cadena.substring(0, 2);
                    break;
            }

            SION.log(Modulo.VENTA, "Cadena devuelta: " + cadena, Level.INFO);
            return cadena;

        }
    }

    //*//
    private void procesarCambioVentaFL(int _valor) {
		llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.sistema")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.modulo")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.submodulo")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "ventaFueraLinea.configuracion")));

        
		respuestaParametroBean = null;
		respuestaParametroBean = new RespuestaParametroBean();

        if (peticionParametroBean != null){
            SION.log(Modulo.VENTA, "Procesando cambio de valor de parametro de venta FL, Valor por asignar: " + _valor, Level.INFO);

            respuestaParametroBean = parametroDao.actualizaParametro(peticionParametroBean, _valor);

            //insercion de historico
            respuestaParametroBean = null;
            respuestaParametroBean = new RespuestaParametroBean();
            respuestaParametroBean = parametroDao.insertaHistoricoCambioParametroDAO(peticionParametroBean, _valor);


        }

    }

    private boolean seCumpleCondicionCambio() {

    	boolean seCumpleCondicion = false;
	    numeroVentasFL += 1;
	    llenarPeticionParametro(VistaBarraUsuario.getSesion().getPaisId(), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTAFL.NUMEROVENTAS.ENCENDERPARAMETRO.SISTEMA")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTAFL.NUMEROVENTAS.ENCENDERPARAMETRO.MODULO")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTAFL.NUMEROVENTAS.ENCENDERPARAMETRO.SUBMODULO")), Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTAFL.NUMEROVENTAS.ENCENDERPARAMETRO.CONFIGURACION")));
	    if (consultarParametro() != null) {
	      if (getCodigoErrorParametroOperacion() == 0)
	      {
	        SION.log(Modulo.VENTA, "Parametro número ventas para VENTA FL encontrado: " + getValorConfiguracionParametroOperacion() + "\nNumero de ventas FL hasta el momento: " + numeroVentasFL, Level.INFO);
	        if (getValorConfiguracionParametroOperacion() == numeroVentasFL)
	        {
	          SION.log(Modulo.VENTA, "Condicion cumplida para cambio de parametro de vental FL", Level.INFO);
	          seCumpleCondicion = true;
	          numeroVentasFL = 0;
	        }
	      }
	      else
	      {
	        SION.log(Modulo.VENTA, "Parametro número ventas para VENTA FL desactivado, se asigna desde archivo de propiedades", Level.INFO);
	      }
	    }
	    return seCumpleCondicion;

    }

    public void verificarCambioFL() {
        if (seCumpleCondicionCambio()) {
            procesarCambioVentaFL(1);
        }
    }

    public void llenarPeticionMetodosEntrada() {
        peticionMetodoEntrada = null;
        peticionMetodoEntrada = new PeticionMetodosEntradaArticuloBean();

        peticionMetodoEntrada.setMetodosEntrada(generaListaMetodosEntradaArticulos());
    }

    public void insertarMetodosEntrada() {
        respuestaMetodoEntrada = null;
        respuestaMetodoEntrada = new RespuestaMetodosEntradaArticuloBean();
        try {
            respuestaMetodoEntrada = metodosEntradaArticulosDao.guardarArticulosXMetodoEntrada(peticionMetodoEntrada);
        } catch (Exception e) {
            e.printStackTrace();
            SION.logearExcepcion(Modulo.VENTA, e, "Ocurrió un error al registrar el método de entrada de los artículos");
        }
    }

    private MetodoEntradaArticuloBean generaMetodoEntrada(ArticuloBean _articulo) {
        MetodoEntradaArticuloBean metodoEntradaArticulo = new MetodoEntradaArticuloBean();
        metodoEntradaArticulo.setArticuloId(_articulo.getPaarticuloid());
        metodoEntradaArticulo.setCodigoBarras(_articulo.getPacdgbarras());
        if (_articulo.getCantidad() % 1 == 0) {
            metodoEntradaArticulo.setCantidad((int) _articulo.getCantidad());
        } else {
            metodoEntradaArticulo.setCantidad(1);
        }
        metodoEntradaArticulo.setFecha(obtenerFecha());
        metodoEntradaArticulo.setMetodoEntradaId(_articulo.getMetodoEntrada());
        metodoEntradaArticulo.setPaisId(VistaBarraUsuario.getSesion().getPaisId());
        metodoEntradaArticulo.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
        metodoEntradaArticulo.setUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());

        return metodoEntradaArticulo;
    }
    
    
    public boolean hayPromociones(TiposDescuento tipoDescuento){
        int hayPromos = 0;
        if( listaArticulos != null){
            for (ArticuloBean articulo : listaArticulos) {
                hayPromos += ( articulo.getTipoDescuento() == tipoDescuento.ordinal() )? 1 : 0;
            }
        }
        //SION.log(Modulo.VENTA, "Hay "+hayPromos+" promociones asignadas actualmente", Level.INFO);
        return hayPromos>0;
    }
    
    public void eliminarArticulosPromocion(TiposDescuento tipoDescuento){
        for (ArticuloBean articulo : listaArticulos) {
            if( articulo.getTipoDescuento() == tipoDescuento.ordinal() ){
                final int indexRemove  = listaArticulos.indexOf(articulo);
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        listaArticulos.remove(indexRemove);
                    }
                });
                
                break;
            }
        }
    }

    public MetodoEntradaArticuloBean[] generaListaMetodosEntradaArticulos() {
        ArrayList<MetodoEntradaArticuloBean> articulos = new ArrayList<MetodoEntradaArticuloBean>();



        boolean seEncontroRegistro = false;

        for (ArticuloBean articulo : listaArticulos) {

            if (articulos.isEmpty()) {
                //primer registro se inserta sin validar
                articulos.add(generaMetodoEntrada(articulo));

            } else {
                //si hay registros se busca si hay anteriores para actualizar solo la cantidad
                for (MetodoEntradaArticuloBean metodo : articulos) {
                    if (metodo.getArticuloId() == articulo.getPaarticuloid()
                            && metodo.getMetodoEntradaId() == articulo.getMetodoEntrada()) {
                        if (articulo.getCantidad() % 1 == 0) {
                            metodo.setCantidad((int) metodo.getCantidad() + 1);
                        } else {
                            metodo.setCantidad((int) metodo.getCantidad() + (int) articulo.getCantidad());
                        }
                        seEncontroRegistro = true;
                    }

                    if (seEncontroRegistro) {
                        break;
                    }
                }

                if (!seEncontroRegistro) {
                    articulos.add(generaMetodoEntrada(articulo));
                }
            }
        }



        return articulos.toArray(new MetodoEntradaArticuloBean[0]);
    }
}
