
package neto.sion.venta.tarjeta.pt.iso8583.nip.dto;

import org.codehaus.jackson.annotate.JsonProperty;

import neto.sion.clientews.tarjeta.dto.RespuestaPagoTarjetaDto;
import neto.sion.venta.servicios.cliente.dto.RespuestaVentaDto;


public class RespuestaCombinadaVentaDto {
	@JsonProperty("respuestaVentaNormal")
    private RespuestaVentaNormalDto respuestaVentaNormal;
	@JsonProperty("respuestaVentaTarjeta")
    private neto.sion.clientews.tarjeta.dto.RespuestaPagoTarjetaDto respuestaVentaTarjeta;
	@JsonProperty("respuestaTarjetaPTDto")
    private RespuestaPagatodoDto respuestaTarjetaPTDto;

    public RespuestaVentaNormalDto getRespuestaVentaNormal() {
        return respuestaVentaNormal;
    }

    public void setRespuestaVentaNormal(RespuestaVentaNormalDto respuestaVentaNormal) {
        this.respuestaVentaNormal = respuestaVentaNormal;
    }

    public RespuestaPagoTarjetaDto getRespuestaVentaTarjeta() {
        return respuestaVentaTarjeta;
    }

    public void setRespuestaVentaTarjeta(RespuestaPagoTarjetaDto respuestaVentaTarjeta) {
        this.respuestaVentaTarjeta = respuestaVentaTarjeta;
    }

	public RespuestaPagatodoDto getRespuestaTarjetaPTDto() {
		return respuestaTarjetaPTDto;
	}

	public void setRespuestaTarjetaPTDto(RespuestaPagatodoDto respuestaTarjetaPTDto) {
		this.respuestaTarjetaPTDto = respuestaTarjetaPTDto;
	}

	@Override
	public String toString() {
		return "RespuestaCombinadaVentaDto [respuestaVentaNormal="
				+ respuestaVentaNormal + ", respuestaVentaTarjeta="
				+ respuestaVentaTarjeta + ", respuestaTarjetaPTDto="
				+ respuestaTarjetaPTDto + "]";
	}
    
    
}
