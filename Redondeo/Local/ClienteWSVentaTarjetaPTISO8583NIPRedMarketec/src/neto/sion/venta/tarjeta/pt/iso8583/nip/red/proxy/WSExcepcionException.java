
/**
 * WSExcepcionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

package neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy;

public class WSExcepcionException extends java.lang.Exception{
    
    private neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.WSExcepcionE faultMessage;

    
        public WSExcepcionException() {
            super("WSExcepcionException");
        }

        public WSExcepcionException(java.lang.String s) {
           super(s);
        }

        public WSExcepcionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public WSExcepcionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.WSExcepcionE msg){
       faultMessage = msg;
    }
    
    public neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.WSExcepcionE getFaultMessage(){
       return faultMessage;
    }
}
    