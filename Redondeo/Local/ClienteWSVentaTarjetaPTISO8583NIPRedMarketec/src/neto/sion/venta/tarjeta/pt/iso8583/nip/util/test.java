

package neto.sion.venta.tarjeta.pt.iso8583.nip.util;

import neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto;
import neto.sion.venta.servicios.cliente.dto.ArticuloDto;
import neto.sion.venta.servicios.cliente.dto.TipoPagoDto;
import neto.sion.venta.tarjeta.pt.iso8583.nip.dto.PeticionPagatodoNipDto;
import neto.sion.venta.tarjeta.pt.iso8583.nip.dto.RespuestaCombinadaVentaDto;
import neto.sion.venta.tarjeta.pt.iso8583.nip.facade.ConsumirTarjetaPT_ISO8583NIP;
import neto.sion.venta.tarjeta.pt.iso8583.nip.facade.ConsumirTarjetaPT_ISO8583NIPImp;


public class test {
    public static void main(String[] args) {
    	try {
			ConsumirTarjetaPT_ISO8583NIP cliente = new ConsumirTarjetaPT_ISO8583NIPImp();
			
						
			PeticionPagatodoNipDto requestPT = new PeticionPagatodoNipDto();
			requestPT.setFechaHoraTransaccion("09/09/2019 17:45:00");
			requestPT.setIdAgente("39999");
			requestPT.setIdGrupo("1");
			requestPT.setIdSucursal("3725");
			requestPT.setIdTerminal("03");
			requestPT.setNoTicket("");
			requestPT.setIdOperador("1528");
			requestPT.setSku("7500048099194");
			requestPT.setReferencia("6395092490004337=290550100000831");
			requestPT.setMonto("10");
			requestPT.setFormaLectura("902");
			requestPT.setNoConfirmacion("");
			requestPT.setIdCliente("");
			requestPT.setNoSecUnicoPT("");
			requestPT.setCategoria("");
			requestPT.setListaProductos(new String[0]);
			requestPT.setNip("");
			
			neto.sion.venta.servicios.cliente.dto.PeticionVentaDto peticionVentaArticulos = new neto.sion.venta.servicios.cliente.dto.PeticionVentaDto();
			
			peticionVentaArticulos.setFueraLinea(0);
			peticionVentaArticulos.setPaTerminal("03");	
			peticionVentaArticulos.setPaConciliacionId(100001);
			peticionVentaArticulos.setPaFechaOper("09/09/2019");
			peticionVentaArticulos.setPaMontoTotalVta(100);
			peticionVentaArticulos.setPaPaisId(1);
			peticionVentaArticulos.setPaTerminal("03");
			peticionVentaArticulos.setPaTipoMovto(1);
			peticionVentaArticulos.setPaUsuarioAutorizaId(0);
			peticionVentaArticulos.setPaUsuarioId(152886);
			peticionVentaArticulos.setTiendaId(3725);
			//peticionVentaArticulos.setId(10000001);
			ArticuloDto[] articulos = new ArticuloDto[1];
			articulos[0] = new ArticuloDto();
			articulos[0].setFcCdGbBarras("7501059225237");
			articulos[0].setFcNombreArticulo("XXX");
			articulos[0].setFiAgranel(0);
			articulos[0].setFiArticuloId(1040002388);
			articulos[0].setFnIepsId(0);
			articulos[0].setTipoDescuento(0);
			articulos[0].setFnCantidad(3);
			articulos[0].setFnCosto(10);
			articulos[0].setFnDescuento(0);
			articulos[0].setFnIva(69);
			articulos[0].setFnPrecio(185.0);
			peticionVentaArticulos.setArticulos(articulos);

			TipoPagoDto[] tiposPago = new TipoPagoDto[1];
			tiposPago[0] = new TipoPagoDto();
			tiposPago[0].setFiTipoPagoId(2);
			tiposPago[0].setFnMontoPago(555);
			tiposPago[0].setFnNumeroVales(0);
			tiposPago[0].setImporteAdicional(0);
			tiposPago[0].setTiendaId(3725);
			tiposPago[0].setFiMontoRecibido(600);
			tiposPago[0].setPaPagoTarjetaIdBus(0);
			peticionVentaArticulos.setTiposPago(tiposPago  );
			
			PeticionPagoTarjetaDto _peticionVenta = new PeticionPagoTarjetaDto();
			
			_peticionVenta.setTrack2("123456789=987654321");
			_peticionVenta.setC63("123456789");
			_peticionVenta.setMonto(555);
			_peticionVenta.setComision(0);
			_peticionVenta.setNumTerminal("03");
			_peticionVenta.setPaPaisId(1);
			_peticionVenta.setC55("9876543210");
						
			RespuestaCombinadaVentaDto res = cliente.registrarVentaTarjetaPagaTodo(_peticionVenta, peticionVentaArticulos, requestPT);
			
			
			System.out.println(res.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
