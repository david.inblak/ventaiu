package neto.sion.venta.tarjeta.pt.iso8583.nip.dto;


import org.codehaus.jackson.annotate.JsonProperty;

//import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.PeticionPagatodoNipBean;

public class PeticionPagatodoNipDto {
	
	@JsonProperty("idAgente") 
	private String idAgente;
	@JsonProperty("fechaHoraTransaccion") 
	private String fechaHoraTransaccion;
	@JsonProperty("idGrupo") 
	private String idGrupo;
	@JsonProperty("idSucursal") 
	private String idSucursal;
	@JsonProperty("idTerminal") 
	private String idTerminal;
	@JsonProperty("idOperador") 
	private String idOperador;
	@JsonProperty("noTicket") 
	private String noTicket;
	@JsonProperty("sku") 
	private String sku;
	@JsonProperty("referencia") 
	private String referencia;
	@JsonProperty("idCliente") 
	private String idCliente;
	@JsonProperty("monto") 
	private String monto;
	@JsonProperty("formaLectura") 
	private String formaLectura;
	@JsonProperty("noConfirmacion") 
	private String noConfirmacion;
	@JsonProperty("noSecUnicoPT") 
	private String noSecUnicoPT;
	@JsonProperty("categoria") 
	private String categoria;
	@JsonProperty(  "listaProductos") 
	private String[] listaProductos;
	@JsonProperty("tipoTarjetaId") 
	private int    tipoTarjetaId;
	@JsonProperty("nip") 
	private String nip;

    

    public String getIdAgente() {
            return idAgente;
    }
    public void setIdAgente(String idAgente) {
            this.idAgente = idAgente;
    }
    public String getFechaHoraTransaccion() {
            return fechaHoraTransaccion;
    }
    public void setFechaHoraTransaccion(String fechaHoraTransaccion) {
            this.fechaHoraTransaccion = fechaHoraTransaccion;
    }
    public String getIdGrupo() {
            return idGrupo;
    }
    public void setIdGrupo(String idGrupo) {
            this.idGrupo = idGrupo;
    }
    public String getIdSucursal() {
            return idSucursal;
    }
    public void setIdSucursal(String idSucursal) {
            this.idSucursal = idSucursal;
    }
    public String getIdTerminal() {
            return idTerminal;
    }
    public void setIdTerminal(String idTerminal) {
            this.idTerminal = idTerminal;
    }
    public String getIdOperador() {
            return idOperador;
    }
    public void setIdOperador(String idOperador) {
            this.idOperador = idOperador;
    }
    public String getNoTicket() {
            return noTicket;
    }
    public void setNoTicket(String noTicket) {
            this.noTicket = noTicket;
    }
    public String getSku() {
            return sku;
    }
    public void setSku(String sku) {
            this.sku = sku;
    }
    public String getReferencia() {
            return referencia;
    }
    public void setReferencia(String referencia) {
            this.referencia = referencia;
    }
    public String getIdCliente() {
            return idCliente;
    }
    public void setIdCliente(String idCliente) {
            this.idCliente = idCliente;
    }
    public String getMonto() {
            return monto;
    }
    public void setMonto(String monto) {
        this.monto = monto;
    }
    public String getFormaLectura() {
            return formaLectura;
    }
    public void setFormaLectura(String formaLectura) {
            this.formaLectura = formaLectura;
    }
    public String getNoConfirmacion() {
            return noConfirmacion;
    }
    public void setNoConfirmacion(String noConfirmacion) {
            this.noConfirmacion = noConfirmacion;
    }
    public String getNoSecUnicoPT() {
            return noSecUnicoPT;
    }
    public void setNoSecUnicoPT(String noSecUnicoPT) {
            this.noSecUnicoPT = noSecUnicoPT;
    }
    public String getCategoria() {
            return categoria;
    }
    public void setCategoria(String categoria) {
            this.categoria = categoria;
    }
    public String[] getListaProductos() {
            return listaProductos;
    }
    public void setListaProductos(String[] listaProductos) {
            this.listaProductos = listaProductos;
    }
    
    public int getTipoTarjetaId() {
        return tipoTarjetaId;
    }

    public void setTipoTarjetaId(int tipoTarjetaId) {
        this.tipoTarjetaId = tipoTarjetaId;
    }
    
    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    @Override
    public String toString() {
        return "PeticionPagatodoDto{" + "idAgente=" + idAgente + 
        							  ", fechaHoraTransaccion=" + fechaHoraTransaccion + 
        							  ", idGrupo=" + idGrupo + 
        							  ", idSucursal=" + idSucursal + 
        							  ", idTerminal=" + idTerminal + 
        							  ", idOperador=" + idOperador + 
        							  ", noTicket=" + noTicket + 
        							  ", sku=" + sku + 
        							  ", referencia=" + referencia + 
        							  ", idCliente=" + idCliente + 
        							  ", monto=" + monto + 
        							  ", formaLectura=" + formaLectura + 
        							  ", noConfirmacion=" + noConfirmacion + 
        							  ", noSecUnicoPT=" + noSecUnicoPT + 
        							  ", categoria=" + categoria + 
        							  ", listaProductos=" + listaProductos +
        							  ", tipoTarjetaId=" + tipoTarjetaId +
        							  ", nip=" + nip + '}';
    }
    

    public PeticionPagatodoNipDto toClienteSolicitud() {
    	PeticionPagatodoNipDto solicitud = new PeticionPagatodoNipDto();
                   
        solicitud.setIdAgente               (this.getIdAgente               ());
        solicitud.setFechaHoraTransaccion   (this.getFechaHoraTransaccion   ());
        solicitud.setIdGrupo                (this.getIdGrupo                ());
        solicitud.setIdSucursal             (this.getIdSucursal             ());
        solicitud.setIdTerminal             (this.getIdTerminal             ());
        solicitud.setIdOperador             (this.getIdOperador             ());
        solicitud.setNoTicket               (this.getNoTicket               ());
        solicitud.setSku                    (this.getSku                    ());
        solicitud.setReferencia             (this.getReferencia             ());
        solicitud.setIdCliente              (this.getIdCliente              ());
        solicitud.setMonto                  (this.getMonto                  ());
        solicitud.setFormaLectura           (this.getFormaLectura           ());
        solicitud.setNoConfirmacion         (this.getNoConfirmacion         ());
        solicitud.setNoSecUnicoPT           (this.getNoSecUnicoPT           ());
        solicitud.setCategoria              (this.getCategoria              ());
        solicitud.setListaProductos         (this.getListaProductos         ());
        solicitud.setTipoTarjetaId          (this.getTipoTarjetaId          ());
        solicitud.setNip                    (this.getNip                    ());
    
        
        return solicitud;
    }


    
}
