/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.redondeo;

import java.util.Set;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesPresentador;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesRepositorio;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesVista;
import neto.sion.tienda.venta.promociones.redondeo.modelo.ModeloArticulo;
import neto.sion.tienda.venta.promociones.redondeo.util.Util;
import neto.sion.tienda.venta.promociones.redondeo.vista.UIArticuloPromocion;

/**
 *
 * @author dramirezr
 */
public class VistaPromociones implements IPromocionesVista {
    
    private IPromocionesRepositorio repositorio;
    private IPromocionesPresentador presentador;
        
    private VBox vBoxPrincipal;
    private AnchorPane anchorPane = new AnchorPane();
    private HBox hboxScroll = new HBox(20);
    private Pane paneLoading = new Pane();
    private Pane paneError = new Pane();
    
    private Text txtMensajeError;
        
    private TextField textInputTotal= new TextField("");
    private TextField textInputCambio= new TextField(""); 
    private TextField textInputRecibido= new TextField(""); 
    private HBox cntGrey = new HBox();
    
    
    Label keyCodeF11 = new Label("F11 ");
    Label keyCodeF12 = new Label("F12 ");
    Label keyCodeF10 = new Label("F10 ");
    
    Button btnFinalizaVenta = new Button("Continuar Venta");    
    Button btnCancelar = new Button("No acepta promoción");
    
    private final KeyCombination f8 = new KeyCodeCombination(KeyCode.F8);
    private final KeyCombination f9 = new KeyCodeCombination(KeyCode.F9);
    private final KeyCombination f10 = new KeyCodeCombination(KeyCode.F10);
    private final KeyCombination f11 = new KeyCodeCombination(KeyCode.F11);
    private final KeyCombination f12 = new KeyCodeCombination(KeyCode.F12);
    
    
    public VistaPromociones  (IPromocionesRepositorio _repositorio) {
        Label kc11 = new Label();
        kc11.textProperty().bind(keyCodeF10.textProperty());
        kc11.setStyle("-fx-text-fill: #e5e5e5;");
        Label kc12 = new Label();
        kc12.textProperty().bind(keyCodeF10.textProperty());
        kc12.setStyle("-fx-text-fill: #e5e5e5;");
        
        try{                       
            keyCodeF11.setStyle("-fx-font: 13pt System;");
            String txt = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.btnAceptaPromo");
            btnFinalizaVenta = new Button( (txt==null||"".contains(txt))? "Continuar Venta":txt.trim(), keyCodeF11);
        }catch(Exception e){
            btnFinalizaVenta = new Button("Continuar Venta", keyCodeF11);
        }
                
        try{           
            keyCodeF12.setStyle("-fx-font: 13pt System;");
            String txt = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.btnCancelaPromo");
            btnCancelar = new Button((txt==null||"".contains(txt))? "No acepta promoción":txt.trim(), keyCodeF12);
        }catch(Exception e){
            btnCancelar = new Button("No acepta promoción", keyCodeF12);
        }
        
        String verShortCut = "";
        try {  verShortCut = SION.obtenerParametro(Modulo.VENTA, "VENTA.REDONDEO.VERFS"); } catch (Exception ex) {}
        
            if( "0".equals( verShortCut) ){
                keyCodeF10.setText("");
                keyCodeF10.setVisible(false);
                keyCodeF11.setText("");
                keyCodeF11.setVisible(false);
                keyCodeF12.setText("");
                keyCodeF12.setVisible(false);
            }
        
                
        this.repositorio = _repositorio;
        
        textInputRecibido.setFocusTraversable(true);
        btnFinalizaVenta.setFocusTraversable(true);
        btnCancelar.setFocusTraversable(true);
        ///-------- Loading
        
        paneLoading.setPrefWidth(950);
        paneLoading.setPrefHeight(600);        
        paneLoading.setStyle("-fx-text-fill: white; -fx-font: 35pt System;");
        AnchorPane.setLeftAnchor(paneLoading, 0.0);
        AnchorPane.setRightAnchor(paneLoading, 0.0);
        AnchorPane.setBottomAnchor(paneLoading, 0.0);
        AnchorPane.setTopAnchor(paneLoading, 0.0);
        
        ImageView imgLoading = new ImageView(  );
        paneLoading.getChildren().add(imgLoading);
        paneLoading.setVisible(false);
        
        ///-------- Error
        
        paneError.setPrefWidth(750);
        paneError.setPrefHeight(600);
        paneError.setStyle("-fx-text-fill: white; -fx-font: 35pt System;");
        AnchorPane.setLeftAnchor(paneError, 0.0);
        AnchorPane.setRightAnchor(paneError, 0.0);
        AnchorPane.setBottomAnchor(paneError, 0.0);
        AnchorPane.setTopAnchor(paneError, 0.0);
        paneError.setVisible(false);
        
        //--- Contenedor Mensaje
        VBox vb2 = new VBox(20);
        vb2.setAlignment(Pos.CENTER);
        vb2.setFillWidth(true);
        vb2.setPrefHeight(120);
        vb2.setPrefWidth(500);
        vb2.setMinHeight(70);
        vb2.setStyle("-fx-background-color:#f93535;");
        
        txtMensajeError = new Text("");
        txtMensajeError.setFill(Color.WHITE);
        txtMensajeError.setTextAlignment(TextAlignment.CENTER);
        txtMensajeError.setWrappingWidth(450);
        txtMensajeError.setStyle("-fx-font: 17pt System;");
        VBox.setVgrow(txtMensajeError, Priority.ALWAYS);
        
        vb2.getChildren().add(txtMensajeError);
        
        Button btn1 = new Button("Aceptar");
        btn1.setStyle("-fx-background-color: #9c1421;-fx-border-color: #ffffff;-fx-text-color: #FFFFFF;-fx-font: 15pt System;");
                
        vb2.getChildren().add(btn1);
        
        ///------- Contenedor principal
        vBoxPrincipal = new VBox(10);
        vBoxPrincipal.alignmentProperty().setValue(Pos.TOP_CENTER);
        vBoxPrincipal.setPrefWidth(900);
        vBoxPrincipal.setPrefHeight(600);
        vBoxPrincipal.setStyle("-fx-background-color: #ffffff;-fx-border-color: #cccccc;-fx-background-radius: 10 10 10 10;");
        vBoxPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent arg0) {                    
                    if( arg0.getCode().equals(KeyCode.ESCAPE)){
                        rechazarPromociones();
                    }
                } 
            });
        
        vBoxPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent ke) {
                if (f12.match(ke)) {
                    btnCancelar.fire();
                }else if (f11.match(ke)) {
                    btnFinalizaVenta.fire();
                }else if (f10.match(ke)) {
                    textInputRecibido.requestFocus();
                }else if (f9.match(ke)) {
                    
                }

            }
        });
        
        
        ///------- Titulo
        HBox hbTitulo= new HBox();
        hbTitulo.alignmentProperty().setValue(Pos.CENTER);
        hbTitulo.setPrefWidth(200);
        hbTitulo.setPrefHeight(300);
        hbTitulo.setStyle("-fx-background-color: #EA4335;");
        String msj ="";
        try{ msj = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana"); }catch(Exception e){}
        Label lblTitulo = new Label(msj);
        lblTitulo.setStyle("-fx-text-fill: white; -fx-font: 35pt System;");
        VBox.setVgrow(hbTitulo, Priority.SOMETIMES); 
        
        hbTitulo.getChildren().add(lblTitulo);
        vBoxPrincipal.getChildren().add(hbTitulo);
        
        //-------- Scroll articulos
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPrefHeight(350);
        scrollPane.setPrefWidth(650);
        scrollPane.setMinHeight(350);
        scrollPane.setMaxHeight(350);
        VBox.setMargin(scrollPane, new Insets(0, 10, 0, 10));
        
        
        anchorPane.setPrefHeight(330);
        anchorPane.setPrefWidth(750);
        
        
        hboxScroll.setPrefHeight(330);
        hboxScroll.setPrefWidth(850);
        hboxScroll.setAlignment(Pos.CENTER);
        
        vBoxPrincipal.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<javafx.scene.input.KeyEvent>() {
            @Override
            public void handle(javafx.scene.input.KeyEvent ke) {
                if( ke.getCode().equals(KeyCode.ALT) ){
                    if( keyCodeF10.isVisible() ){
                        keyCodeF10.setText("");
                        keyCodeF11.setText("");
                        keyCodeF12.setText("");
                        keyCodeF10.setVisible(false);
                        keyCodeF11.setVisible(false);
                        keyCodeF12.setVisible(false);
                    }else{
                        keyCodeF10.setText("F10 ");
                        keyCodeF11.setText("F11 ");
                        keyCodeF12.setText("F12 ");
                        keyCodeF10.setVisible(true);
                        keyCodeF11.setVisible(true);
                        keyCodeF12.setVisible(true);
                    }
                }
            }
        });
        
        anchorPane.getChildren().add(hboxScroll);
        
         final ListChangeListener listener = new ListChangeListener<Node>(){

            @Override
            public void onChanged(ListChangeListener.Change<? extends Node> nodo) {
                while(nodo.next()){
                    if( nodo.wasAdded()){
                        UIArticuloPromocion nodoUIA = (UIArticuloPromocion)nodo.getAddedSubList().get(0);
                        nodoUIA.asignarIndice( hboxScroll.getChildren().size() );
                        vBoxPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, nodoUIA.getFiltroEventosF() );
                        vBoxPrincipal.addEventFilter(KeyEvent.KEY_RELEASED, nodoUIA.getFiltroEventosAltRelease());
                    }
                }
            }
            
        };
        hboxScroll.getChildren().addListener(listener);
        
        scrollPane.setContent(anchorPane);
        
        vBoxPrincipal.getChildren().add(scrollPane);

        ///------- Importes
        HBox contImportes = new HBox(15);
        contImportes.setAlignment(Pos.TOP_CENTER);
        contImportes.setPrefHeight(200);
        contImportes.setMaxHeight(200);
        VBox.setVgrow(contImportes, Priority.SOMETIMES);    
                
        VBox titImportes = new VBox(10);
        titImportes.setAlignment(Pos.TOP_RIGHT);
        titImportes.setPrefHeight(200);
        titImportes.setPrefWidth(100);
        
        HBox.setHgrow(titImportes, Priority.ALWAYS);
            // Total
            HBox hbxTotal = new HBox();
            hbxTotal.setPrefHeight(200);
            hbxTotal.setPrefWidth(100);
            hbxTotal.setAlignment(Pos.TOP_RIGHT);

            Label lblTotal = new Label("Total:");
            lblTotal.setStyle("-fx-font: 30pt System ;");

            hbxTotal.getChildren().add(lblTotal);

            titImportes.getChildren().add(hbxTotal);
            
            // Recibido
            HBox hbxRecibido = new HBox();
            hbxRecibido.setPrefHeight(200);
            hbxRecibido.setPrefWidth(100);
            hbxRecibido.setAlignment(Pos.TOP_RIGHT);

            Label lblRecibido = new Label("Importe recibido:");
            lblRecibido.setStyle("-fx-font: 25pt System ;");

            hbxRecibido.getChildren().add(lblRecibido);

            titImportes.getChildren().add(hbxRecibido);
            
            
            // Cambio
            HBox hbxCambio = new HBox();
            hbxCambio.setPrefHeight(200);
            hbxCambio.setPrefWidth(100);
            hbxCambio.setAlignment(Pos.TOP_RIGHT);

            Label lblCambio = new Label("Cambio:");
            lblCambio.setStyle("-fx-font: 25pt System ;");

            hbxCambio.getChildren().add(lblCambio);

            titImportes.getChildren().add(hbxCambio);
        
        contImportes.getChildren().add(titImportes);
        
        
        
        VBox inputImportes = new VBox(10);
        inputImportes.setAlignment(Pos.TOP_LEFT);
        inputImportes.setPrefHeight(200);
        inputImportes.setPrefWidth(100);
        
        HBox.setHgrow(inputImportes, Priority.ALWAYS);
            // Total
            HBox hbxInputTotal = new HBox();
            hbxInputTotal.setPrefHeight(200);
            hbxInputTotal.setPrefWidth(100);
            HBox.setHgrow(hbxInputTotal, Priority.SOMETIMES);

            textInputTotal.setFocusTraversable(false);
            textInputTotal.setPromptText("0.00");
            textInputTotal.setEditable(false);            
            textInputTotal.setPrefHeight(35);
            textInputTotal.setPrefWidth(100);
            textInputTotal.setStyle("-fx-background-color: #e5e5e5;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
            Label pesos = new Label(" $ ");
            pesos.setStyle("-fx-font: 25pt System;");
            
            HBox cntGrey1 = new HBox();
            cntGrey1.setStyle("-fx-background-color: #e5e5e5;-fx-background-radius: 5 5 5 5;");
            cntGrey1.getChildren().addAll(pesos,textInputTotal, kc11);

            hbxInputTotal.getChildren().addAll(cntGrey1);

            inputImportes.getChildren().add(hbxInputTotal);
            
            // Recibido           
            HBox hbxInputRecibido = new HBox();
            hbxInputRecibido.setPrefHeight(200);
            hbxInputRecibido.setPrefWidth(100);
            HBox.setHgrow(hbxInputRecibido, Priority.SOMETIMES);

            //textInputRecibido.requestFocus();
            textInputRecibido.setPromptText("0.00");
            textInputRecibido.setEditable(true);
            textInputRecibido.setPrefHeight(35);
            textInputRecibido.setPrefWidth(100);
            textInputRecibido.setStyle("-fx-background-color: #e5e5e5;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
            
            textInputRecibido.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent arg0) {                    
                    if(arg0.getCharacter().equals("+")){
                        arg0.consume();
                        textInputRecibido.textProperty().set( textInputTotal.textProperty().get().replace("$", "").trim() );
                    }else if( !arg0.getCharacter().matches("[0-9]|\\.")){
                        arg0.consume();
                    }
                }                
            });
            textInputRecibido.textProperty().addListener(new ChangeListener<String>(){
                @Override
                public void changed(ObservableValue<? extends String> arg0, String viejoValor, String nuevoVal) {
                    if( nuevoVal.matches("^[0]{2,}$") ){
                        textInputRecibido.setText("0");
                    }else if( nuevoVal.matches("^[0]{1,}[0-9]$") ){
                        textInputRecibido.setText(nuevoVal.replace("0", ""));
                    }else if( !nuevoVal.matches("\\d{0,5}([\\.]\\d{0,2})?") ){
                        textInputRecibido.setText(viejoValor);
                    }
                    calcularCambio(nuevoVal);
                }
            });
            textInputRecibido.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent arg0) {                    
                    if( arg0.getCode().equals(KeyCode.ENTER)){
                        aceptarPromociones();
                    }
                } 
            });
                        
            Label pesos1 = new Label(" $ ");
            pesos1.setStyle("-fx-font: 25pt System;");
            
            keyCodeF10.setStyle("-fx-font: 12pt System;");
                        
            cntGrey.setStyle("-fx-background-color: #e5e5e5;-fx-background-radius: 5 5 5 5;");            
            cntGrey.getChildren().addAll(pesos1,textInputRecibido, keyCodeF10);
            
            hbxInputRecibido.getChildren().addAll(cntGrey);
            
           
            
            textInputRecibido.requestFocus();

            inputImportes.getChildren().add(hbxInputRecibido);
            
            // Cambio
            HBox hbxInputCambio = new HBox();
            hbxInputCambio.setPrefHeight(200);
            hbxInputCambio.setPrefWidth(100);
            
            HBox.setHgrow(hbxInputCambio, Priority.SOMETIMES);

            textInputCambio.setFocusTraversable(false);
            textInputCambio.setPromptText("0.00");
            textInputCambio.setEditable(false);
            textInputCambio.setPrefHeight(35);
            textInputCambio.setPrefWidth(100);
            textInputCambio.setStyle("-fx-background-color: #e5e5e5;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
            Label pesos2 = new Label(" $ ");
            pesos2.setStyle("-fx-font: 25pt System;");
            
            HBox cntGrey2 = new HBox();
            cntGrey2.setStyle("-fx-background-color: #e5e5e5;-fx-background-radius: 5 5 5 5;");
            cntGrey2.getChildren().addAll(pesos2,textInputCambio, kc12);
            
            hbxInputCambio.getChildren().addAll(cntGrey2);           

            inputImportes.getChildren().add(hbxInputCambio);
        
        contImportes.getChildren().add(inputImportes);
        
        vBoxPrincipal.getChildren().add(contImportes);
        
        ///------- Botones
        HBox hbxPie = new HBox(30);
        hbxPie.setAlignment(Pos.CENTER);
        hbxPie.setPrefHeight(100);
        hbxPie.setMinHeight(100);
        VBox.setVgrow(hbxPie, Priority.SOMETIMES);
        
        //btnFinalizaVenta.setDisable(true);
        btnFinalizaVenta.setStyle(
                  "-fx-background-color: #0f3b84; -fx-text-fill: white; -fx-font: 25pt System; "
                + "-fx-faint-focus-color: transparent; -fx-focus-color:#e08906;");
        btnFinalizaVenta.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                aceptarPromociones();
            }
        });
        
        
        btnCancelar.setStyle("-fx-background-color: #e08906; -fx-text-fill: white; -fx-font: 25pt System;");
        btnCancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                rechazarPromociones();
            }
        });
        
        hbxPie.getChildren().add(btnFinalizaVenta);
        hbxPie.getChildren().add(btnCancelar);
        
        vBoxPrincipal.getChildren().add(hbxPie);
        
        
    }
    
    @Override
    public void setPresentador(IPromocionesPresentador _presentador){
        this.presentador = _presentador;
    }
    
    public void generarArticuloPromocion(final ModeloArticulo articulo){
        
            Platform.runLater(new Runnable(){
                @Override
                public void run(){
                    try{ 
                        //SION.log(Modulo.VENTA, "Generando vista articulo : " + articulo.getNombre(), Level.INFO);
                        final UIArticuloPromocion art = new UIArticuloPromocion(articulo);
                        //SION.log(Modulo.VENTA, "Constructor pasado ", Level.INFO);
                        art.setEventoBoton(new EventHandler<ActionEvent>(){
                            @Override
                            public void handle(ActionEvent arg0) {
                                presentador.agregarPromocion( art.getModelo() );
                            }
                        });

                        hboxScroll.getChildren().add( art );

                        recalcularTamanoScroll(art.getPrefWidth());
                    }catch(Exception e){
                        SION.logearExcepcion(Modulo.VENTA, e, "generarArticuloPromocion :: " + Util.getStackTrace(e));
                    }
                }
            });
         
    }
    
    private void limpiarArticulosPromocion(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                hboxScroll.getChildren().removeAll(hboxScroll.getChildren());
            }
        });
        
    }
    
    private void recalcularTamanoScroll(Double widthPromocion){
        if( hboxScroll.getChildren().size() > 3 ){
            final Double incremento = ((widthPromocion+hboxScroll.getSpacing()) * (hboxScroll.getChildren().size() - 3) );
            Platform.runLater(new Runnable() {
                @Override
                public void run() {                   
                    anchorPane.setPrefWidth(850 + incremento.intValue() );
                    hboxScroll.setPrefWidth(850 + incremento.intValue() );
                }
            });
            
        } 
    }
   
    
    @Override
    public void cargarPromociones(){
        
        limpiarArticulosPromocion();
        
        Service servicioCargaArticulos = new Service() {
            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {
                        mostrarLoading();
                        Set<ModeloArticulo> articulosRedondeo = (Set<ModeloArticulo>)repositorio.getPromocionesDisponibles();
                        //SION.log(Modulo.VENTA, "Vistas de articulos por generar : " + articulosRedondeo.size(), Level.INFO);
                        for( ModeloArticulo art : articulosRedondeo){
                            generarArticuloPromocion( art );
                        }
                        
                       // SION.log(Modulo.VENTA, "Se generaron las vistas de los articulos.", Level.INFO);
                        return "OK";
                    }
                };
            }
        };
        servicioCargaArticulos.stateProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                if( arg2.equals(Worker.State.SUCCEEDED) || arg2.equals(Worker.State.CANCELLED) || arg2.equals(Worker.State.FAILED)){
                   ocultarLoading();                   
                }
                //SION.log(Modulo.VENTA, "El servicio cambio a estatus: "+arg2, Level.INFO);
            }
        });
        servicioCargaArticulos.exceptionProperty().addListener(new ChangeListener<Exception>() {
            @Override
            public void changed(ObservableValue<? extends Exception> arg0, Exception arg1, Exception nuevoVal) {
                SION.logearExcepcion(Modulo.VENTA, arg1, "servicioCargaArticulos"); 
                SION.logearExcepcion(Modulo.VENTA, nuevoVal, "servicioCargaArticulos");
            }
            
        });
        servicioCargaArticulos.start();
        
    }
    
    
    @Override
    public void desactivarPromociones(Object promocionSeleccionada){
        ModeloArticulo modeloSeleccionado = (ModeloArticulo)promocionSeleccionada;
        
        for(Node nodo : hboxScroll.getChildren()){                    
            UIArticuloPromocion nodoUIA = (UIArticuloPromocion)nodo;
            if( nodoUIA.getModelo() == modeloSeleccionado )
                nodoUIA.cambiarEstatusSeleccionado();
            else
                nodoUIA.cambiarEstatusNoSeleccionado();
        }
        Platform.runLater(new Runnable() {
            @Override
            public void run() {    }
        });
    }
    
    @Override
    public void mostrarError(final String mensaje){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {                   
                paneError.setVisible(true);
                txtMensajeError.textProperty().set(mensaje);
            }
        });
    }
    
    @Override
    public void ocultarError(){        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                paneError.setVisible(true);
                txtMensajeError.textProperty().set("");
            }
        });  
    }
    
    

    @Override
    public void mostrarLoading() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                 paneLoading.setVisible(true);
            }
        });        
    }

    @Override
    public void ocultarLoading() {        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                 paneLoading.setVisible(false);
            }
        });
    }

    @Override
    public Object obtenerVista() {
        return vBoxPrincipal;
    }


    @Override
    public void mostrarCostoPromocionesSeleccionadas(Object promocionesSeleccionadas ){
        Set<ModeloArticulo> promos = (Set<ModeloArticulo>) promocionesSeleccionadas;
        ModeloArticulo promo = null;
        if( promos.iterator().hasNext() ){
           promo = promos.iterator().next();
           setImporteTotal( String.format("%,.2f", promo.getTotal() ));
           setFocusImporteRecibido();           
        }
        
    }
    
    
    @Override
    public void aceptarPromociones(){
        if( !btnFinalizaVenta.isDisabled() ){
            presentador.aceptarPromociones();
        }
    }
    
    @Override
    public void rechazarPromociones(){
            presentador.rechazarPromociones();
    }
     
     
    private void calcularCambio(String _recibido){
        try{
            Double total = Double.parseDouble( textInputTotal.textProperty().get().replace("$", "") );
            Double recibido = Double.parseDouble( "0"+_recibido );

            Double cambio = recibido - total ;
            if( cambio < 0.0 ){
                setImporteCambio( "" );
                deshabilitarFinalizarVenta();
            }else{
                setImporteCambio( String.format("%.2f", cambio) );
                habilitarFinalizarVenta();
            }
            
        }catch(Exception e){
            System.out.println("Error al calcular el cambio  ");
        }
        
    }
    
    private void setImporteTotal(final String total){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                textInputTotal.textProperty().set(total);
                calcularCambio(textInputRecibido.textProperty().get());
            }
        });
        
    }
    
   /* private void setImporteRecibido(final String recibido){
       
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                 textInputTotal.textProperty().set(recibido);
            }
        });
    }*/
    
    private void setImporteCambio(final String importe){        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                 textInputCambio.textProperty().set(importe);
            }
            
        });
    }

    private void deshabilitarFinalizarVenta() {
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                textInputRecibido.setStyle("-fx-background-color: #dd9d9d;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
                cntGrey.setStyle("-fx-background-color: #dd9d9d;-fx-background-radius: 5 5 5 5;");
            }        
        });
    }

    private void habilitarFinalizarVenta() {
        Platform.runLater(new Runnable(){
            @Override
            public void run() {               
                textInputRecibido.setStyle("-fx-background-color: #e5e5e5;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
                cntGrey.setStyle("-fx-background-color: #e5e5e5;-fx-background-radius: 5 5 5 5;");
            }        
        });
    }
    
   

    private void setFocusImporteRecibido() {
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
               textInputRecibido.requestFocus();
            }        
        });        
    }

    @Override
    public void setImporteRecibido(final Double importe) {
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                textInputRecibido.textProperty().set(String.format("%.2f", importe));
            }            
        });  
    }

    @Override
    public void setImporteTotal(final Double importe) {
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                textInputTotal.textProperty().set(String.format("%.2f", importe));
            }            
        });        
    }

    @Override
    public Double getImporteRecibido() {
        return Double.parseDouble( "0"+textInputRecibido.textProperty().get() );
    }
    
}
