
package neto.sion.venta.tarjeta.pt.iso8583.nip.facade;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.SocketTimeoutException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.venta.tarjeta.pt.iso8583.nip.dto.*;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSExcepcionException;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.ConsultaSaldoPTdo;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.ConsultaSaldoPTdoResponse;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.ConsultarTransaccionCentral;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.ConsultarTransaccionCentralResponse;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.PeticionPagatodoNipBean;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.PeticionVentaArticulosBean;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.RegistrarVentaTarjetaPagaTodoNIPRed;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.RegistrarVentaTarjetaPagaTodoNIPRedResponse;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.RespuestaPagatodoNipBean;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.RespuestaPagatodoNipBeanE;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.RespuestaVentaCombinadaBean;
import neto.sion.venta.tarjeta.pt.iso8583.nip.util.ControlRespuestas;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.ServiceClient;


public class ConsumirTarjetaPT_ISO8583NIPImp implements ConsumirTarjetaPT_ISO8583NIP{
	static final int NOERRORCODE = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "noerror.codigo")).intValue();
	  static final int TERMINAL_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "terminalerror.codigo")).intValue();
	  static final int FECHAOPERACION_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "fechaoperacionerror.codigo")).intValue();
	  static final int ARTICULOS_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "articuloserror.codigo")).intValue();
	  static final int PAGOS_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "pagoserror.codigo")).intValue();
	  static final int MONTOVENTA_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "montoventaerror.codigo")).intValue();
	  static final int PAIS_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "paiserror.codigo")).intValue();
	  static final int TIENDA_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.codigo")).intValue();
	  static final int USUARIO_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "usuarioerror.codigo")).intValue();
	  static final int TIPOMOV_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "tipomovimientoerror.codigo")).intValue();
	  static final int NUMTRANSACCION_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "numtransaccionerror.codigo")).intValue();
	  static final int MONTORECIBIDO_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "montorecibidoerror.codigo")).intValue();
	  static final int COMPANIATEL_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "companiatelefonicaerror.codigo")).intValue();
	  static final int NUMTEL_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "numtelefonicoerror.codigo")).intValue();
	  static final int MAXTELID = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "companiatelefonica.maxid")).intValue();
	  static final int MINTELID = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "companiatelefonica.minid")).intValue();
	  static final int TIPOPROCESO_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "tipoprocesoerror.codigo")).intValue();
	  static final int VOUCHER_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "arreglovouchererror.codigo")).intValue();
	  static final int CONCILIACIONID_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "conciliacionerror.codigo")).intValue();
	  static final int VENTA_TA = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "arreglo.ventata.codigo")).intValue();
	  static final int MOV_ID = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "movimientoiderror.codigo")).intValue();
	  static long TIEMPO_ESPERA_TIMEOUT_PAGO;
	    /*private final String IDAGENTE;
	    private final String SKU_CONSULTA;
	    private final String FORMALECTURA;
	    private final String IDSUCURSAL;
	    private final String IDTERMINAL;
	    private final String IDOPERADOR;*/
	    private String END_POINT;
	    private String END_POINT_TIMEOUT_SALDO;
	    private String END_POINT_TIMEOUT_VENTA;
	    private String END_POINT_TIMEOUT_CONF;
	    private long timeOut = 3000;
	    
	    WSVentaTarjetaPTISO8583NIPRedServiceStub stubSaldo=null;
	    WSVentaTarjetaPTISO8583NIPRedServiceStub stubVenta=null;
	    WSVentaTarjetaPTISO8583NIPRedServiceStub stubConfir=null;
	    ServiceClient scS=null;
	    ServiceClient scV=null;
	    ServiceClient scC=null;
	    
	    ControlRespuestas controlResp;
	    
	    private void llenarParametrosSolicitudPagaTodo(final PeticionPagatodoNipDto request){
	        SION.log(Modulo.VENTA, "llenando datos de la solicitud.", Level.INFO);
	        SimpleDateFormat datef = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	        try{
	            request.setIdAgente  ( SION.obtenerParametro(Modulo.VENTA, "PAGATODO.DATOSCONSULTA.IDAGENTE") );
	            request.setIdGrupo   ( SION.obtenerParametro(Modulo.VENTA, "PAGATODO.DATOSCONSULTA.IDGRUPO") );
	            //request.setIdSucursal( SION.obtenerParametro(Modulo.VENTA, "PAGATODO.DATOSCONSULTA.IDSUCURSAL") );
	            //request.setIdTerminal( SION.obtenerParametro(Modulo.VENTA, "PAGATODO.DATOSCONSULTA.IDTERMINAL") );
	            //request.setIdOperador( SION.obtenerParametro(Modulo.VENTA, "PAGATODO.DATOSCONSULTA.IDOPERADOR") );
	            request.setSku       ( SION.obtenerParametro(Modulo.VENTA, "PAGATODO.DATOSCONSULTA.SKU_CONSULTA") );
	            request.setFormaLectura( SION.obtenerParametro(Modulo.VENTA, "PAGATODO.DATOSCONSULTA.FORMALECTURA") );
	            request.setFechaHoraTransaccion(datef.format(new Date()));
	            
	            /*request.setCategoria(END_POINT);
	            request.setFechaHoraTransaccion(END_POINT);
	            request.setIdCliente(END_POINT);
	            request.setNoSecUnicoPT(END_POINT);*/
	            
	        }catch(Exception e){
	            SION.logearExcepcion(Modulo.VENTA, e, "No se encontro uno de los parametros para la consulta de saldo en paga todo.");
	        }
	    }
	    
	    public ConsumirTarjetaPT_ISO8583NIPImp() throws Exception {
	        END_POINT    = SION.obtenerParametro(Modulo.VENTA, "PAGATODO.SERVICIO.ISO8583.NIP.RED.ENDPOINT");
	        TIEMPO_ESPERA_TIMEOUT_PAGO = Long.parseLong(SION.obtenerParametro(Modulo.VENTA, "PAGATODO.SERVICIO.ISO8583.ESPERA.VENTA.CONFIRMACION"));
	        
	        try {
	            //timeOut = Long.parseLong(END_POINT_TIMEOUT);
	            END_POINT_TIMEOUT_SALDO = SION.obtenerParametro(Modulo.VENTA, "PAGATODO.SERVICIO.ISO8583.ENDPOINT_TIMEOUT.SALDO");
	            END_POINT_TIMEOUT_VENTA = SION.obtenerParametro(Modulo.VENTA, "PAGATODO.SERVICIO.ISO8583.ENDPOINT_TIMEOUT.PAGO");
	            END_POINT_TIMEOUT_CONF = SION.obtenerParametro(Modulo.VENTA, "PAGATODO.SERVICIO.ISO8583.ENDPOINT_TIMEOUT.CONFIRMACION");
	    
	            stubSaldo = new WSVentaTarjetaPTISO8583NIPRedServiceStub( SION.obtenerConfigurationContext(), END_POINT);
	            stubSaldo._getServiceClient().getOptions().setTimeOutInMilliSeconds(Long.parseLong(END_POINT_TIMEOUT_SALDO));
	            scS = stubSaldo._getServiceClient();
	            scS.engageModule("rampart");
	            
	            scS.getOptions().setTimeOutInMilliSeconds(Long.parseLong(END_POINT_TIMEOUT_SALDO));
	            
	            
	            
	            stubVenta = new WSVentaTarjetaPTISO8583NIPRedServiceStub( SION.obtenerConfigurationContext(), END_POINT);
	            stubVenta._getServiceClient().getOptions().setTimeOutInMilliSeconds(Long.parseLong(END_POINT_TIMEOUT_VENTA));
	            scV = stubVenta._getServiceClient();
	            scV.engageModule("rampart");
	            
	            scV.getOptions().setTimeOutInMilliSeconds(Long.parseLong(END_POINT_TIMEOUT_VENTA));
	            
	            
	                
	            stubConfir = new WSVentaTarjetaPTISO8583NIPRedServiceStub( SION.obtenerConfigurationContext(), END_POINT);
	            stubConfir._getServiceClient().getOptions().setTimeOutInMilliSeconds(Long.parseLong(END_POINT_TIMEOUT_CONF));
	            scC = stubConfir._getServiceClient();
	            scC.engageModule("rampart");
	            
	            scC.getOptions().setTimeOutInMilliSeconds(Long.parseLong(END_POINT_TIMEOUT_CONF));
	            
	            
	            SION.log(Modulo.VENTA, "WS PagaTodoNipRedondeo Endpoint: " + END_POINT, Level.INFO);
	            SION.log(Modulo.VENTA, "Timed out con que se aplicara la venta pagatodo: " + timeOut + " ms", Level.INFO);
	        } catch (Exception ex) {
	           //Logger.getLogger(ConsumirTarjetaPTdoImp.class.getName()).log(Level.SEVERE, null, ex);
	             SION.logearExcepcion(Modulo.VENTA, ex, getStackTrace(ex));
	        }
	    }
	    
	    @Override
	    public RespuestaPagatodoDto consultaSaldoPTdo (PeticionPagatodoNipDto _peticionConSaldo) throws WSException {
	    	
	        RespuestaPagatodoDto  metRespuesta=new RespuestaPagatodoDto();
	        RespuestaPagatodoNipBeanE respuesta = null;
	        
	        try{
	        	
	        	llenarParametrosSolicitudPagaTodo(_peticionConSaldo);
	            SION.log(Modulo.VENTA, "Solicitud consulta saldo paga todo: " + _peticionConSaldo, Level.INFO);
	            
	            PeticionPagatodoNipBean _request = new PeticionPagatodoNipBean();
	            
	            _request = _peticionConSaldo.toClienteSolicitud();
	            //_request.set_peticionConSaldo(_peticionConSaldo.toClienteSolicitud());
	            
	            respuesta = stubSaldo.consultaSaldoPTdo(_request);
	            
	            //respuesta = stubSaldo.consultaSaldoPTdo( _peticionConSaldo.toClienteSolicitud() );
	            
	            metRespuesta.crearRespuesta(respuesta);
	            SION.log(Modulo.VENTA, "Respuesta consulta saldo paga todo: " + metRespuesta, Level.INFO);
	        }catch(WSExcepcionException wse){
	            respuesta = new RespuestaPagatodoNipBeanE();
	            respuesta.setRDescRespuesta("Se presento un error al consulta el saldo de la tarjeta.");
	            SION.logearExcepcion(Modulo.VENTA, wse, getStackTrace(wse));
	        }catch(Exception e){
	            respuesta = new RespuestaPagatodoNipBeanE();
	            respuesta.setRDescRespuesta("Se presento un error al consulta el saldo de la tarjeta.");
	            SION.logearExcepcion(Modulo.VENTA, e, getStackTrace(e));
	        }
	        return metRespuesta;
	    }

	    @Override
	    public RespuestaCombinadaVentaDto registrarVentaTarjetaPTdo(PeticionVentaDto _peticionVenta, 
	    															neto.sion.venta.servicios.cliente.dto.PeticionVentaDto _peticionVentaN, 
	    															PeticionPagatodoNipDto _requestPT) throws WSException {
	    	
	    	RespuestaCombinadaVentaDto respMetodo = new RespuestaCombinadaVentaDto();
	    	RespuestaVentaCombinadaBean resCombinada = null;
	    	controlResp = new ControlRespuestas();
	    	
	    	llenarParametrosSolicitudPagaTodo(_requestPT);
	    	    
	    	//RespuestaVentaDto respuestaTarjeta = new RespuestaVentaDto();
	    	RespuestaVentaNormalDto respuestaNormal = new RespuestaVentaNormalDto();
	    	RespuestaPagatodoDto respuestaPagatodoDto = null;
	    	boolean esVentaTimeout = false;
	    	boolean esConfTransTimneout = false;
	    	
	    	try{
	    		
	    		
	    		SION.log(Modulo.VENTA, "Solicitud pago paga todo: ", Level.INFO);
	    	    SION.log(Modulo.VENTA, "" + _peticionVenta, Level.INFO);
	    	    SION.log(Modulo.VENTA, "" + _peticionVentaN, Level.INFO);
	    	    SION.log(Modulo.VENTA, "" + _requestPT.toClienteSolicitud(), Level.INFO);
	    	    
	    	    RegistrarVentaTarjetaPagaTodoNIPRed regVentaTarjetaPagaTodo = new RegistrarVentaTarjetaPagaTodoNIPRed();
	    	    RegistrarVentaTarjetaPagaTodoNIPRedResponse response = new RegistrarVentaTarjetaPagaTodoNIPRedResponse();
	    	      
	    	    regVentaTarjetaPagaTodo.set_peticionVenta(_peticionVenta.toClienteSolicitud());
	    	    regVentaTarjetaPagaTodo.set_peticionVentaArticulos(getPeticionVentaArticulosBean(_peticionVentaN));
	    	      
	    	    regVentaTarjetaPagaTodo.set_requestPT(_requestPT.toClienteSolicitud());
	    	      
	    	    SION.log(Modulo.VENTA, "Cliente envia venta con ieps a servicio registrarVentaTarjetaPagaTodo ", Level.INFO);
	    	    resCombinada = this.stubVenta.registrarVentaTarjetaPagaTodoNIPRed(regVentaTarjetaPagaTodo.get_peticionVenta(),
	    	    														   regVentaTarjetaPagaTodo.get_peticionVentaArticulos(),
	    	    														   regVentaTarjetaPagaTodo.get_requestPT());
	    	      
	    	    //resCombinada = response.get_return();
	    	      
	    	    SION.log(Modulo.VENTA, "1.. Validando objeto de respuesta recibida", Level.INFO);
	    	    if ((resCombinada == null) || (resCombinada.getRespuestaVentaNormal() == null)){
	    	        SION.log(Modulo.VENTA, "2.. objeto de respuesta recibida es nulo", Level.INFO);
	    	        respuestaNormal.setPaCdgError(-1);
	    	        respuestaNormal.setPaDescError(SION.obtenerMensaje(Modulo.VENTA, "venta.pagatodo.sinrespuesta"));
	    	    }else{
	    	    	
	    	        SION.log(Modulo.VENTA, "3.. separando respuestas", Level.INFO);
	    	        respuestaNormal.crearRespuesta(resCombinada.getRespuestaVentaNormal());
	    	        respuestaPagatodoDto = controlResp.respPagatodo_Bean2Dto(resCombinada.getRespuestaTarjeta().getRespuestaPagaTodo());
	    	        
	    	        if (respuestaNormal.getPaDescError().contains("[100]")) {
	    	        	respuestaNormal.setPaDescError(SION.obtenerMensaje(Modulo.VENTA, "venta.pagatodo.timedoutdoble") + ",\n (IdConciliacion: " + _peticionVentaN.getPaConciliacionId() + ")");
	    	        }
	    	        SION.log(Modulo.VENTA, "Respuesta venta :: normal :: " + respuestaNormal, Level.INFO);
	    	    }
	    	}catch (AxisFault se){
	    		SION.log(Modulo.VENTA, "AxisFault", Level.INFO);
	    	    SION.logearExcepcion(Modulo.VENTA, se, new String[0]);
	    	    
	    	    if ((se.getCause() != null) && ((se.getCause() instanceof SocketTimeoutException))){
	    	    	SION.log(Modulo.VENTA, "Es de tipo Timeout", Level.INFO);
	    	        esVentaTimeout = true;
	    	    }else{
	    	    	SION.log(Modulo.VENTA, "No es de tipo Timeout", Level.INFO);
	    	    }
	    	}catch (RemoteException rex){
	    		SION.log(Modulo.VENTA, "RemoteException", Level.INFO);
	    	    SION.logearExcepcion(Modulo.VENTA, rex, new String[0]);
	    	}catch (Exception wse){
	    		SION.log(Modulo.VENTA, "4.. Ocurrio un error no es RemoteException", Level.INFO);
	    	    SION.logearExcepcion(Modulo.VENTA, wse, new String[] { getStackTrace(wse) });
	    	    respuestaNormal.setPaCdgError(-1);
	    	    respuestaNormal.setPaDescError(SION.obtenerMensaje(Modulo.VENTA, "venta.pagatodo.sinrespuesta"));
	    	}
	    	
	    	
	    	if ((resCombinada == null) || (resCombinada.getRespuestaVentaNormal() == null)){
	    		SION.log(Modulo.VENTA, "5.. Respuesta vacia", Level.INFO);
	    	    respuestaNormal = new RespuestaVentaNormalDto();
	    	    respuestaNormal.setPaCdgError(-1);
	    	    respuestaNormal.setPaDescError(SION.obtenerMensaje(Modulo.VENTA, "venta.pagatodo.sinrespuesta"));
	    	}
	    	
	    	
	    	SION.log(Modulo.VENTA, "6.. preparando respuesta", Level.INFO);
	    	respMetodo.setRespuestaVentaNormal(respuestaNormal.crearRespuestaVentaNormalDtoActual());
	    	if(respuestaPagatodoDto!=null){
	    		respMetodo.setRespuestaTarjetaPTDto(respuestaPagatodoDto);
	    	}else{
	    		respMetodo.setRespuestaTarjetaPTDto(null);
	    	}
	    	
	    	SION.log(Modulo.VENTA, "7.. ", Level.INFO);
	    	
	    	if (esVentaTimeout){
	    		
	    		try{
	    	        long inicial = System.currentTimeMillis();
	    	        SION.log(Modulo.VENTA, "En espera por " + TIEMPO_ESPERA_TIMEOUT_PAGO, Level.INFO);
	    	        Thread.sleep(TIEMPO_ESPERA_TIMEOUT_PAGO);
	    	        SION.log(Modulo.VENTA, "Tiempo esperado " + (System.currentTimeMillis() - inicial), Level.INFO);
	    	    }catch (InterruptedException ex){
	    	    	Logger.getLogger(ConsumirTarjetaPT_ISO8583NIPImp.class.getName()).log(Level.SEVERE, null, ex);
	    	    }
	    		
	    		SION.log(Modulo.VENTA, "Se intenta solicitar el id de la transaccion.", Level.INFO);
	    	    
	    		try{
	    			
	    	        PeticionTransaccionCentralDto peticionConfirmaTransaccion = new PeticionTransaccionCentralDto();
	    	        peticionConfirmaTransaccion.setPaConciliacionId(_peticionVentaN.getPaConciliacionId());
	    	        peticionConfirmaTransaccion.setPaPaisId(_peticionVentaN.getPaPaisId());
	    	        peticionConfirmaTransaccion.setPaModuloId(Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.timedout.modulo")));
	    	        peticionConfirmaTransaccion.setPaSistemaId(Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.timedout.sistema")));
	    	        peticionConfirmaTransaccion.setPaSubModuloId(Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "venta.timedout.submodulo")));
	    	        peticionConfirmaTransaccion.setPaUsuarioId(_peticionVentaN.getPaUsuarioId());
	    	        peticionConfirmaTransaccion.setTiendaId(_peticionVentaN.getTiendaId());
	    	        
	    	        long transaccionId  = this.stubConfir.consultarTransaccionCentral(peticionConfirmaTransaccion.toClienteSolicitud());
	    	        
	    	        
	    	        SION.log(Modulo.VENTA, "Respuesta de confirmacion: " + transaccionId, Level.INFO);
	    	        if (transaccionId > 0){
	    	        	respuestaNormal.setPaCdgError(0);
	    	        	respuestaNormal.setPaTransaccionId(transaccionId);
	    	        	respuestaNormal.setPaDescError("OK");
	    	        	respuestaNormal.setPaTaNumOperacion(0);
	    	          
	    	        	BloqueoDto[] paTypCursorBlqs = new BloqueoDto[0];
	    	        	respuestaNormal.setPaTypCursorBlqs(paTypCursorBlqs);
	    	          
	    	        	respMetodo.setRespuestaVentaNormal(respuestaNormal.crearRespuestaVentaNormalDtoActual());
	    	        }else{
	    	        	respuestaNormal.setPaCdgError(-1);
	    	        	respuestaNormal.setPaTransaccionId(transaccionId);
	    	        	respuestaNormal.setPaTaNumOperacion(0);
	    	        	respuestaNormal.setPaDescError(SION.obtenerMensaje(Modulo.VENTA, "venta.pagatodo.timedoutdoble") + ",\n (IdConciliacion: " + _peticionVentaN.getPaConciliacionId() + ")");
	    	          
	    	        	BloqueoDto[] paTypCursorBlqs = new BloqueoDto[0];
	    	        	respuestaNormal.setPaTypCursorBlqs(paTypCursorBlqs);
	    	          
	    	        	respMetodo.setRespuestaVentaNormal(respuestaNormal.crearRespuestaVentaNormalDtoActual());
	    	        }
	    	        
	    	        SION.log(Modulo.VENTA, "Consumo de WS paga todo completado...", Level.INFO);
	    	      }catch (AxisFault se){
	    	    	  
	    	    	  SION.log(Modulo.VENTA, "AxisFault", Level.INFO);
	    	    	  SION.logearExcepcion(Modulo.VENTA, se, new String[] { getStackTrace(se) });
	    	    	  
	    	    	  if ((se.getCause() != null) && ((se.getCause() instanceof SocketTimeoutException))) {
	    	    		  SION.log(Modulo.VENTA, "Es de tipo Timeout", Level.INFO);
	    	    	  } else {
	    	    		  SION.log(Modulo.VENTA, "No es de tipo Timeout", Level.INFO);
	    	    	  }
	    	    	  esConfTransTimneout = true;
	    	      }catch (Exception e){
	    	    	  SION.logearExcepcion(Modulo.VENTA, e, new String[] { "Error al consultar la transaccion > " + e.getMessage() });
	    	      }
	    		
	    	      if (esConfTransTimneout){
	    	    	  
	    	    	  respuestaNormal.setPaCdgError(-100);
	    	    	  respuestaNormal.setPaTransaccionId(0);
	    	    	  respuestaNormal.setPaDescError(SION.obtenerMensaje(Modulo.VENTA, "venta.pagatodo.timedoutdoble") + ",\n (IdConciliacion: " + _peticionVentaN.getPaConciliacionId() + ")");
	    	        
	    	    	  respuestaNormal.setPaTaNumOperacion(0);
	    	        
	    	    	  BloqueoDto[] paTypCursorBlqs = new BloqueoDto[0];
	    	    	  respuestaNormal.setPaTypCursorBlqs(paTypCursorBlqs);
	    	        
	    	    	  respMetodo.setRespuestaVentaNormal(respuestaNormal.crearRespuestaVentaNormalDtoActual());
	    	      }
	    	}
	    	
	    	return respMetodo;
	    }
	    
	    @Override
	    public RespuestaCombinadaVentaDto registrarVentaTarjetaPagaTodo(PeticionPagoTarjetaDto _petVentaTarjeta, 
	            														neto.sion.venta.servicios.cliente.dto.PeticionVentaDto _petVentaN, 
	            														PeticionPagatodoNipDto _requestPT) throws WSException {
	    	
	    	RespuestaCombinadaVentaDto respuestaCombinada = new RespuestaCombinadaVentaDto();

	    	PeticionVentaDto _peticionVentaTarjeta = new PeticionVentaDto();
	    	PeticionVentaNormalDto _peticionVentaNormal = new PeticionVentaNormalDto();

	    	String tarjeta = _petVentaTarjeta.getTrack2().replace("�", "").split("�")[0];

	    	_peticionVentaTarjeta.setC63(_petVentaTarjeta.getC63());
	    	_peticionVentaTarjeta.setMonto(_petVentaTarjeta.getMonto());
	    	_peticionVentaTarjeta.setComision(_petVentaTarjeta.getComision());
	    	_peticionVentaTarjeta.setNumTerminal(_petVentaTarjeta.getNumTerminal());
	    	_peticionVentaTarjeta.setC55(_petVentaTarjeta.getC55());
	    	_peticionVentaTarjeta.setNumeroTarjeta(tarjeta);
	    	_peticionVentaTarjeta.setPaisId(_petVentaTarjeta.getPaPaisId());
	    	_peticionVentaTarjeta.setTiendaId((int)_petVentaN.getTiendaId());
	    	_peticionVentaTarjeta.setUsuarioId(_petVentaN.getPaUsuarioId());

	    	_peticionVentaTarjeta.setPinEntryCapability(0);
	    	_peticionVentaTarjeta.setEstatusLecturaId(0);
	    	_peticionVentaTarjeta.setMetodoLectura(0);

	    	_peticionVentaNormal.setuId(_petVentaN.getUId());
	    	_peticionVentaNormal.setIpTerminal(_petVentaN.getIpTerminal());
	    	_peticionVentaNormal.setTiendaId(_petVentaN.getTiendaId());
	    	_peticionVentaNormal.setPaPaisId(_petVentaN.getPaPaisId());
	    	_peticionVentaNormal.setPaConciliacionId(_petVentaN.getPaConciliacionId());
	    	_peticionVentaNormal.setPaTerminal(_petVentaN.getPaTerminal());
	    	_peticionVentaNormal.setPaUsuarioId(_petVentaN.getPaUsuarioId());
	    	_peticionVentaNormal.setPaUsuarioAutorizaId(_petVentaN.getPaUsuarioAutorizaId());
	    	_peticionVentaNormal.setPaFechaOper(_petVentaN.getPaFechaOper());
	    	_peticionVentaNormal.setPaTipoMovto(_petVentaN.getPaTipoMovto());
	    	_peticionVentaNormal.setPaMontoTotalVta(_petVentaN.getPaMontoTotalVta());
	    	_peticionVentaNormal.setFueraLinea(_petVentaN.getFueraLinea());

	    	neto.sion.venta.servicios.cliente.dto.ArticuloDto[] articulos = _petVentaN.getArticulos();
	    	neto.sion.venta.servicios.cliente.dto.TipoPagoDto[] tiposPago = _petVentaN.getTiposPago();

	    	ArticuloDto[] arts = new ArticuloDto[articulos.length];
	    	TipoPagoDto[] tips = new TipoPagoDto[tiposPago.length];
	    	for (int i = 0; i < articulos.length; i++){
	    		ArticuloDto arti = new ArticuloDto();
	    		arti.setFcCdGbBarras(articulos[i].getFcCdGbBarras());
	    		arti.setFcNombreArticulo(articulos[i].getFcNombreArticulo());
	    		arti.setFiAgranel(articulos[i].getFiAgranel());
	    		arti.setFiArticuloId(articulos[i].getFiArticuloId());
	    		arti.setFnCantidad(articulos[i].getFnCantidad());
	    		arti.setFnCosto(articulos[i].getFnCosto());
	    		arti.setFnDescuento(articulos[i].getFnDescuento());
	    		arti.setFiTipoDescuento(articulos[i].getTipoDescuento());
	    		arti.setFnIva(articulos[i].getFnIva());
	    		arti.setFnPrecio(articulos[i].getFnPrecio());

	    		arts[i] = arti;
	    	}
	    	
	    	for (int j = 0; j < tiposPago.length; j++){
	    		
	    		TipoPagoDto tip = new TipoPagoDto();

	    		tip.setFiTipoPagoId(tiposPago[j].getFiTipoPagoId());
	    		tip.setFnMontoPago(tiposPago[j].getFnMontoPago());
	    		tip.setFnNumeroVales(tiposPago[j].getFnNumeroVales());
	    		tip.setImporteAdicional(tiposPago[j].getImporteAdicional());
	    		tip.setPaPagoTarjetaIdBus(tiposPago[j].getPaPagoTarjetaIdBus());
	    		tips[j] = tip;
	    		
	    	}

	    	_peticionVentaNormal.setArticulos(arts);
	    	_peticionVentaNormal.setTiposPago(tips);

	    	SION.log(Modulo.VENTA, "Se envia Venta con IEPS NIP Redondeo", Level.INFO);
	    	respuestaCombinada = registrarVentaTarjetaPTdo(_peticionVentaTarjeta, _petVentaN, _requestPT);

	    	return respuestaCombinada;
	    }

	    @Override
	    public long consultarTransaccionCentral(
	                    PeticionTransaccionCentralDto _peticion)
	                                    throws WSException
	    {
	        long respuesta = -1;
	        try{
	        	ConsultarTransaccionCentral _req = new ConsultarTransaccionCentral();
	        	
	        	_req.set_peticion(_peticion.toClienteSolicitud());
	        	
	        	respuesta =  stubConfir.consultarTransaccionCentral(_peticion.toClienteSolicitud());
	        	
	            
	        }catch(WSExcepcionException wse){
	            SION.logearExcepcion(Modulo.VENTA, wse); 
	        }catch(Exception e){
	            SION.logearExcepcion(Modulo.VENTA, e);
	        }
	        return respuesta;
	    }

	    
	    private RespuestaVentaDto validarPeticionVentaNormal( neto.sion.venta.servicios.cliente.dto.PeticionVentaDto _peticion ){
	        RespuestaVentaDto respuesta = new RespuestaVentaDto();
	        double monto = 0.0D;
	        for (neto.sion.venta.servicios.cliente.dto.TipoPagoDto tipo : _peticion.getTiposPago()) {
	            if (tipo != null) {
	                monto += tipo.getFnMontoPago();
	            }
	        }
	        if (monto == 0.0D)
	        {
	            respuesta.setCodigoError(MONTOVENTA_ERROR);
	            respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "montoventaerror.mensaje"));
	            return respuesta;
	        }
	        if ((_peticion.getPaTerminal() == null) || (_peticion.getPaTerminal().trim().equals("")))
	        {
	        respuesta.setCodigoError(TERMINAL_ERROR);
	        respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "terminalformatoerror.mensaje"));
	        return respuesta;
	        }
	        if ((_peticion.getPaFechaOper() == null) || (_peticion.getPaFechaOper().trim().equals("")))
	        {
	        respuesta.setCodigoError(FECHAOPERACION_ERROR);
	        respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "fechaoperacionerror.mensaje"));
	        return respuesta;
	        }
	        if ((_peticion.getArticulos() == null) || (_peticion.getArticulos().length == 0))
	        {
	        respuesta.setCodigoError(ARTICULOS_ERROR);
	        respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "articuloserror.mensaje"));
	        return respuesta;
	        }
	        if ((_peticion.getTiposPago() == null) || (_peticion.getTiposPago().length == 0))
	        {
	        respuesta.setCodigoError(PAGOS_ERROR);
	        respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "pagoserror.mensaje"));
	        return respuesta;
	        }
	        if (_peticion.getPaPaisId() == 0)
	        {
	        respuesta.setCodigoError(PAIS_ERROR);
	        respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "paiserror.mensaje"));
	        return respuesta;
	        }
	        if (_peticion.getTiendaId() == 0L)
	        {
	        respuesta.setCodigoError(TIENDA_ERROR);
	        respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.mensaje"));
	        return respuesta;
	        }
	        if (_peticion.getPaUsuarioId() == 0L)
	        {
	        respuesta.setCodigoError(USUARIO_ERROR);
	        respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "usuarioerror.mensaje"));
	        return respuesta;
	        }
	        if ((_peticion.getPaTipoMovto() == 0) || (_peticion.getPaTipoMovto() != 1))
	        {
	        respuesta.setCodigoError(TIPOMOV_ERROR);
	        respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "tipomovimientoerror.mensaje"));
	        return respuesta;
	        }
	        
	        return null;
	    }
	    
	    private PeticionVentaArticulosBean getPeticionVentaArticulosBean(neto.sion.venta.servicios.cliente.dto.PeticionVentaDto peticionVentaDto){
	    	
	    	PeticionVentaArticulosBean peticionVentaArticulosBean = new PeticionVentaArticulosBean();
	    	
	    	peticionVentaArticulosBean.setFueraLinea(peticionVentaDto.getFueraLinea());
	    	peticionVentaArticulosBean.setIpTerminal(peticionVentaDto.getIpTerminal());
	    	peticionVentaArticulosBean.setPaConciliacionId(peticionVentaDto.getPaConciliacionId());
	    	peticionVentaArticulosBean.setPaFechaOper(peticionVentaDto.getPaFechaOper());
	    	peticionVentaArticulosBean.setPaMontoTotalVta(peticionVentaDto.getPaMontoTotalVta());
	    	peticionVentaArticulosBean.setPaPaisId(peticionVentaDto.getPaPaisId());
	    	peticionVentaArticulosBean.setPaTerminal(peticionVentaDto.getPaTerminal());
	    	peticionVentaArticulosBean.setPaTipoMovto(peticionVentaDto.getPaTipoMovto());
	    	peticionVentaArticulosBean.setPaUsuarioAutorizaId(peticionVentaDto.getPaUsuarioAutorizaId());
	    	peticionVentaArticulosBean.setPaUsuarioId(peticionVentaDto.getPaUsuarioId());
	    	peticionVentaArticulosBean.setTiendaId(peticionVentaDto.getTiendaId());
	    	peticionVentaArticulosBean.setUId(peticionVentaDto.getUId());
	    	
	    	WSVentaTarjetaPTISO8583NIPRedServiceStub.TipoPagoBean[] tiposPagoBean = new WSVentaTarjetaPTISO8583NIPRedServiceStub.TipoPagoBean[peticionVentaDto.getTiposPago().length];
	    	
	    	int countArr = 0;
	    	for (neto.sion.venta.servicios.cliente.dto.TipoPagoDto tipoPagoDto : peticionVentaDto.getTiposPago()){
	    		
	    		WSVentaTarjetaPTISO8583NIPRedServiceStub.TipoPagoBean tipoPagoBean = new WSVentaTarjetaPTISO8583NIPRedServiceStub.TipoPagoBean();
	    		tipoPagoBean.setEsPagaTodo(true);
	    		tipoPagoBean.setFiTipoPagoId(tipoPagoDto.getFiTipoPagoId());
	    		tipoPagoBean.setFnMontoPago(tipoPagoDto.getFnMontoPago());
	    		tipoPagoBean.setFnNumeroVales(tipoPagoDto.getFnNumeroVales());
	    		tipoPagoBean.setImporteAdicional(tipoPagoDto.getImporteAdicional());
	    		tipoPagoBean.setPaPagoTarjetaIdBus(tipoPagoDto.getPaPagoTarjetaIdBus());
	        
	    		tiposPagoBean[countArr] = tipoPagoBean;
	    		countArr++;
	    	}
	    	
	    	peticionVentaArticulosBean.setTiposPago(tiposPagoBean);
	      
	    	countArr = 0;
	      
	    	WSVentaTarjetaPTISO8583NIPRedServiceStub.ArticuloPeticionVentaBean[] articuloPeticionVentaBeans = new WSVentaTarjetaPTISO8583NIPRedServiceStub.ArticuloPeticionVentaBean[peticionVentaDto.getArticulos().length];
	      
	    	for (neto.sion.venta.servicios.cliente.dto.ArticuloDto articuloDto : peticionVentaDto.getArticulos()){
	    		
	    		WSVentaTarjetaPTISO8583NIPRedServiceStub.ArticuloPeticionVentaBean articuloBean = new WSVentaTarjetaPTISO8583NIPRedServiceStub.ArticuloPeticionVentaBean();
	    		articuloBean.setFcCdGbBarras(articuloDto.getFcCdGbBarras());
	    		articuloBean.setFcNombreArticulo(articuloDto.getFcNombreArticulo());
	    		articuloBean.setFiAgranel(articuloDto.getFiAgranel());
	    		articuloBean.setFiArticuloId(articuloDto.getFiArticuloId());
	    		articuloBean.setFiIepsId(articuloDto.getFnIepsId());
	    		articuloBean.setFnCantidad(articuloDto.getFnCantidad());
	    		articuloBean.setFnCosto(articuloDto.getFnCosto());
	    		articuloBean.setFnDescuento(articuloDto.getFnDescuento());
	    		articuloBean.setFnIva(articuloDto.getFnIva());
	    		articuloBean.setFnPrecio(articuloDto.getFnPrecio());
	    		articuloBean.setFiTipoDescuento(articuloDto.getTipoDescuento());
	        
	    		articuloPeticionVentaBeans[countArr] = articuloBean;
	        
	    		countArr++;
	    	}
	    	
	      peticionVentaArticulosBean.setArticulos(articuloPeticionVentaBeans);
	      
	      return peticionVentaArticulosBean;
	    }
	    
	    public static String getStackTrace(Throwable aThrowable) {
	            Writer result = new StringWriter();
	            PrintWriter printWriter = new PrintWriter(result);
	            aThrowable.printStackTrace(printWriter);
	            return result.toString();
	    }
	}