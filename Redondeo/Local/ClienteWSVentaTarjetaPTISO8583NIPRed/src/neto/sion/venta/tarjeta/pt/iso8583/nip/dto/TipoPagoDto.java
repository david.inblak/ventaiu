package neto.sion.venta.tarjeta.pt.iso8583.nip.dto;

import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.TipoPagoBean;

public class TipoPagoDto {

    private int fiTipoPagoId;
    private double fnMontoPago;
    private int fnNumeroVales;
    private long paPagoTarjetaIdBus;
    private double importeAdicional;

    public double getImporteAdicional() {
            return importeAdicional;
    }

    public void setImporteAdicional(double importeAdicional) {
            this.importeAdicional = importeAdicional;
    }

    public long getPaPagoTarjetaIdBus() {
            return paPagoTarjetaIdBus;
    }

    public void setPaPagoTarjetaIdBus(long paPagoTarjetaIdBus) {
            this.paPagoTarjetaIdBus = paPagoTarjetaIdBus;
    }

    public int getFiTipoPagoId() {
            return fiTipoPagoId;
    }

    public void setFiTipoPagoId(int fiTipoPagoId) {
            this.fiTipoPagoId = fiTipoPagoId;
    }

    public double getFnMontoPago() {
            return fnMontoPago;
    }

    public void setFnMontoPago(double fnMontoPago) {
            this.fnMontoPago = fnMontoPago;
    }

    public int getFnNumeroVales() {
            return fnNumeroVales;
    }

    public void setFnNumeroVales(int fnNumeroVales) {
            this.fnNumeroVales = fnNumeroVales;
    }

    public TipoPagoBean toClienteSolicitud() {
        TipoPagoBean solicitud = new TipoPagoBean();
        solicitud.setFiTipoPagoId	(this.getFiTipoPagoId		());
        solicitud.setFnMontoPago        (this.getFnMontoPago        ());
        solicitud.setFnNumeroVales      (this.getFnNumeroVales      ());
        solicitud.setPaPagoTarjetaIdBus (this.getPaPagoTarjetaIdBus ());
        solicitud.setImporteAdicional   (this.getImporteAdicional   ());
        return solicitud;
    }

    @Override
    public String toString() {
        return "TipoPagoDto{" + "fiTipoPagoId=" + fiTipoPagoId + ", fnMontoPago=" + fnMontoPago + ", fnNumeroVales=" + fnNumeroVales + ", paPagoTarjetaIdBus=" + paPagoTarjetaIdBus + ", importeAdicional=" + importeAdicional + '}';
    }

}
