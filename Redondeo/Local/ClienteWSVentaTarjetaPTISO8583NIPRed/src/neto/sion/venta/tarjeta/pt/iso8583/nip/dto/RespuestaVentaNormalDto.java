

package neto.sion.venta.tarjeta.pt.iso8583.nip.dto;

import java.util.Arrays;

import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.RespuestaVentaBeanE;


public class RespuestaVentaNormalDto {
        private long paTransaccionId;
	private int paCdgError;
	private String paDescError;
	private long paTaNumOperacion;
	private BloqueoDto[] paTypCursorBlqs;

	public BloqueoDto[] getPaTypCursorBlqs() {
		return paTypCursorBlqs;
	}

	public void setPaTypCursorBlqs(BloqueoDto[] paTypCursorBlqs) {
		this.paTypCursorBlqs = paTypCursorBlqs;
	}

	public long getPaTransaccionId() {
		return paTransaccionId;
	}

	public void setPaTransaccionId(long paTransaccionId) {
		this.paTransaccionId = paTransaccionId;
	}

	public int getPaCdgError() {
		return paCdgError;
	}

	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}

	public String getPaDescError() {
		return paDescError;
	}

	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}

	public long getPaTaNumOperacion() {
		return paTaNumOperacion;
	}

	public void setPaTaNumOperacion(long paTaNumOperacion) {
		this.paTaNumOperacion = paTaNumOperacion;
	}

	@Override
	public String toString() {
		return "RespuestaVentaBean [paTransaccionId=" + paTransaccionId
				+ ", paCdgError=" + paCdgError + ", paDescError=" + paDescError
				+ ", paTaNumOperacion=" + paTaNumOperacion
				+ ", paTypCursorBlqs=" + Arrays.toString(paTypCursorBlqs) + "]";
	}

    public void crearRespuesta(RespuestaVentaBeanE  respuestaVentaNormal) {
        this.setPaCdgError(respuestaVentaNormal.getPaCdgError());
        this.setPaDescError(respuestaVentaNormal.getPaDescError());
        this.setPaTaNumOperacion(respuestaVentaNormal.getPaTaNumOperacion());
        this.setPaTransaccionId(respuestaVentaNormal.getPaTransaccionId());
        
        if( this.getPaTypCursorBlqs() != null ){
            BloqueoDto[] bloqueos = new BloqueoDto[respuestaVentaNormal.getPaTypCursorBlqs().length];
            for (int i = 0; i < respuestaVentaNormal.getPaTypCursorBlqs().length; i++) {
                BloqueoDto b= new BloqueoDto();
                respuestaVentaNormal.getPaTypCursorBlqs()[i].getFiAvisosFalt();
                respuestaVentaNormal.getPaTypCursorBlqs()[i].getFiEstatusBloqueoId();
                respuestaVentaNormal.getPaTypCursorBlqs()[i].getFiNumAvisos();
                respuestaVentaNormal.getPaTypCursorBlqs()[i].getFiTipoPagoId();
                bloqueos[i] = b;
            }

            this.setPaTypCursorBlqs(bloqueos);
        }else{
           this.setPaTypCursorBlqs(new BloqueoDto[0]); 
        }
    }
    
    public neto.sion.venta.servicios.cliente.dto.RespuestaVentaDto crearRespuestaVentaNormalDtoActual(){
        neto.sion.venta.servicios.cliente.dto.RespuestaVentaDto res = 
                new neto.sion.venta.servicios.cliente.dto.RespuestaVentaDto();
        
        res.setPaCdgError(this.getPaCdgError());
        res.setPaDescError(this.getPaDescError());
        res.setPaTaNumOperacion(this.getPaTaNumOperacion());
        res.setPaTransaccionId(this.getPaTransaccionId());
        
        if( this.getPaTypCursorBlqs() != null ){
            neto.sion.venta.servicios.cliente.dto.BloqueoDto[] bloqueos = 
                    new neto.sion.venta.servicios.cliente.dto.BloqueoDto[this.getPaTypCursorBlqs().length];

            for (int i = 0; i < this.getPaTypCursorBlqs().length; i++) {
                neto.sion.venta.servicios.cliente.dto.BloqueoDto b= new neto.sion.venta.servicios.cliente.dto.BloqueoDto();
                b.setFiAvisosFalt       (this.getPaTypCursorBlqs()[i].getFiAvisosFalt());
                b.setFiEstatusBloqueoId (this.getPaTypCursorBlqs()[i].getFiEstatusBloqueoId());
                b.setFiNumAvisos        (this.getPaTypCursorBlqs()[i].getFiNumAvisos());
                b.setFiTipoPagoId       (this.getPaTypCursorBlqs()[i].getFiTipoPagoId());
                bloqueos[i] = b;
            }

            res.setPaTypCursorBlqs(bloqueos);
        }else{
            res.setPaTypCursorBlqs(new neto.sion.venta.servicios.cliente.dto.BloqueoDto[0]);
        }
        
        return res;
    }
}
