package neto.sion.venta.tarjeta.pt.iso8583.nip.dto;

import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.RespuestaVentaBAZBean;

public class RespuestaVentaBAZDto {
        private String longitudMensaje210; // longitud del mensaje
	private String tipoMensaje; // 4-MSG_TYPE-MTI
	private String bitmap; // 16-PRIMARY_BIT_MAP-000
	private String codigoProceso; // 6-PROC_CODE-003
	private String montoOperacion; // 12-TRANS_AMT-004
	private String fechaOperacion; // 10-XMT_DATE_TIME-007
	private String trace; // 6-STAN-011
	private String horaLocal; // 6_LOC_TIME-012
	private String fechaLocal; // 4-LOC_DATE-013
	private String campo23Bandera; //3-BANDERA-023
	private String referencia; // 12-RRN-037
	private String numAutorizacion; // 6-AUTH_ID_RESP-038
	private String codigoRespuesta; // 2-RESP_CODE-039
	private String terminal; // 8-CARD_ACCPT_TRMNL_ID-041
	private String afiliacion; // 15-CARD_ACCPT_ID-042
	private String longitudTramaBanco; // 3-BANK_LEN-048
	private String tramaBanco; // Longitud variable indicada
								// enlongitudTramaBanco-BANK-048
	private String longitudC55; // 3-IC card system related data_Len-055
	private String C55_TAGS_EMV_FULL; // Longitud variable indicada
										// en:longitudC55-IC card system related
										// data-055
	private String longitudC63; // 3-TOKEN_LEN-063
	private String C63_desc_token_ES_EZ; // Varibale, definida en:
	
	private boolean esPagoExitoso;
        
        public  RespuestaVentaBAZDto() {}

     public RespuestaVentaBAZDto(RespuestaVentaBAZBean solicitud) {
            this.setLongitudMensaje210	   (solicitud.getLongitudMensaje210		 ());
            this.setTipoMensaje                (solicitud.getTipoMensaje                ());
            this.setBitmap                     (solicitud.getBitmap                     ());
            this.setCodigoProceso              (solicitud.getCodigoProceso              ());
            this.setMontoOperacion             (solicitud.getMontoOperacion             ());
            this.setFechaOperacion             (solicitud.getFechaOperacion             ());
            this.setTrace                      (solicitud.getTrace                      ());
            this.setHoraLocal                  (solicitud.getHoraLocal                  ());
            this.setFechaLocal                 (solicitud.getFechaLocal                 ());
            this.setCampo23Bandera             (solicitud.getCampo23Bandera             ());
            this.setReferencia                 (solicitud.getReferencia                 ());
            this.setNumAutorizacion            (solicitud.getNumAutorizacion            ());
            this.setCodigoRespuesta            (solicitud.getCodigoRespuesta            ());
            this.setTerminal                   (solicitud.getTerminal                   ());
            this.setAfiliacion                 (solicitud.getAfiliacion                 ());
            this.setLongitudTramaBanco         (solicitud.getLongitudTramaBanco         ());
            this.setTramaBanco                 (solicitud.getTramaBanco                 ());
            this.setLongitudC55                (solicitud.getLongitudC55                ());
            this.setC55_TAGS_EMV_FULL          (solicitud.getC55_TAGS_EMV_FULL          ());
            this.setLongitudC63                (solicitud.getLongitudC63                ());
            this.setC63_desc_token_ES_EZ       (solicitud.getC63_desc_token_ES_EZ       ());
            this.setEsPagoExitoso              (solicitud.getEsPagoExitoso              ());
    }

    

	// (longitudC63)-TOKEN_DATA-063
	public String getLongitudMensaje210() {
		return longitudMensaje210;
	}

	public void setLongitudMensaje210(String longitudMensaje210) {
		this.longitudMensaje210 = longitudMensaje210;
	}

	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	public String getBitmap() {
		return bitmap;
	}

	public void setBitmap(String bitmap) {
		this.bitmap = bitmap;
	}

	public String getCodigoProceso() {
		return codigoProceso;
	}

	public void setCodigoProceso(String codigoProceso) {
		this.codigoProceso = codigoProceso;
	}

	public String getMontoOperacion() {
		return montoOperacion;
	}

	public void setMontoOperacion(String montoOperacion) {
		this.montoOperacion = montoOperacion;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	public String getHoraLocal() {
		return horaLocal;
	}

	public void setHoraLocal(String horaLocal) {
		this.horaLocal = horaLocal;
	}

	public String getFechaLocal() {
		return fechaLocal;
	}

	public void setFechaLocal(String fechaLocal) {
		this.fechaLocal = fechaLocal;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public String getLongitudTramaBanco() {
		return longitudTramaBanco;
	}

	public void setLongitudTramaBanco(String longitudTramaBanco) {
		this.longitudTramaBanco = longitudTramaBanco;
	}

	public String getTramaBanco() {
		return tramaBanco;
	}

	public void setTramaBanco(String tramaBanco) {
		this.tramaBanco = tramaBanco;
	}

	public String getLongitudC55() {
		return longitudC55;
	}

	public void setLongitudC55(String longitudC55) {
		this.longitudC55 = longitudC55;
	}

	public String getC55_TAGS_EMV_FULL() {
		return C55_TAGS_EMV_FULL;
	}

	public void setC55_TAGS_EMV_FULL(String c55TAGSEMVFULL) {
		C55_TAGS_EMV_FULL = c55TAGSEMVFULL;
	}

	public String getLongitudC63() {
		return longitudC63;
	}

	public void setLongitudC63(String longitudC63) {
		this.longitudC63 = longitudC63;
	}

	public String getC63_desc_token_ES_EZ() {
		return C63_desc_token_ES_EZ;
	}

	public void setC63_desc_token_ES_EZ(String c63DescTokenESEZ) {
		C63_desc_token_ES_EZ = c63DescTokenESEZ;
	}

	public String getCampo23Bandera() {
		return campo23Bandera;
	}

	public void setCampo23Bandera(String campo23Bandera) {
		this.campo23Bandera = campo23Bandera;
	}

	public boolean isEsPagoExitoso() {
		return esPagoExitoso;
	}

	public void setEsPagoExitoso(boolean esPagoExitoso) {
		this.esPagoExitoso = esPagoExitoso;
	}
}
