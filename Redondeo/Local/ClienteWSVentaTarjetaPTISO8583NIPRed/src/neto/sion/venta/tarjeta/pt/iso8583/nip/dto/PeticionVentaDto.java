

package neto.sion.venta.tarjeta.pt.iso8583.nip.dto;

import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.PeticionVentaBean;

public class PeticionVentaDto {

	private long uId;
	private int paisId;
	private int tiendaId;
	private long usuarioId;
	private String numeroTarjeta;
	private int estatusLecturaId;
	private double comision;
	private int metodoLectura;
	private double monto;
	private int pinEntryCapability;
	private String numTerminal;
	private String c55;
	private String c63;

    public String getC55() {
        return c55;
    }

    public void setC55(String c55) {
        this.c55 = c55;
    }

    public String getC63() {
        return c63;
    }

    public void setC63(String c63) {
        this.c63 = c63;
    }

    public double getComision() {
        return comision;
    }

    public void setComision(double comision) {
        this.comision = comision;
    }

    public int getEstatusLecturaId() {
        return estatusLecturaId;
    }

    public void setEstatusLecturaId(int estatusLecturaId) {
        this.estatusLecturaId = estatusLecturaId;
    }

    public int getMetodoLectura() {
        return metodoLectura;
    }

    public void setMetodoLectura(int metodoLectura) {
        this.metodoLectura = metodoLectura;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getNumTerminal() {
        return numTerminal;
    }

    public void setNumTerminal(String numTerminal) {
        this.numTerminal = numTerminal;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public int getPaisId() {
        return paisId;
    }

    public void setPaisId(int paisId) {
        this.paisId = paisId;
    }

    public int getPinEntryCapability() {
        return pinEntryCapability;
    }

    public void setPinEntryCapability(int pinEntryCapability) {
        this.pinEntryCapability = pinEntryCapability;
    }

    public int getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(int tiendaId) {
        this.tiendaId = tiendaId;
    }

    public long getuId() {
        return uId;
    }

    public void setuId(long uId) {
        this.uId = uId;
    }

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public String toString() {
        return "PeticionVentaDto{" + "uId=" + uId + ", paisId=" + paisId + ", tiendaId=" + tiendaId + ", usuarioId=" + usuarioId + ", numeroTarjeta=" + numeroTarjeta + ", estatusLecturaId=" + estatusLecturaId + ", comision=" + comision + ", metodoLectura=" + metodoLectura + ", monto=" + monto + ", pinEntryCapability=" + pinEntryCapability + ", numTerminal=" + numTerminal + ", c55=" + c55 + ", c63=" + c63 + '}';
    }
    
    public PeticionVentaBean toClienteSolicitud() {
        PeticionVentaBean solicitud = new PeticionVentaBean();
        solicitud.setUId			(this.getuId			());
	solicitud.setPaisId			(this.getPaisId			());
	solicitud.setTiendaId			(this.getTiendaId		());
	solicitud.setUsuarioId			(this.getUsuarioId		());
	solicitud.setC63			(this.getC63			());
	solicitud.setNumeroTarjeta		(this.getNumeroTarjeta          ());
	solicitud.setEstatusLecturaId		(this.getEstatusLecturaId	());
	solicitud.setComision			(this.getComision		());
	solicitud.setMetodoLectura		(this.getMetodoLectura          ());
	solicitud.setMonto			(this.getMonto			());
	solicitud.setPinEntryCapability         (this.getPinEntryCapability     ());
	solicitud.setNumTerminal		(this.getNumTerminal		());
	solicitud.setC55			(this.getC55			());
        
        return solicitud;
    }
}
