package neto.sion.venta.tarjeta.pt.iso8583.nip.dto;

import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.ArticuloBean;

public class ArticuloDto {
    private long fiArticuloId;
    private String fcCdGbBarras;
    private double fnCantidad;
    private double fnPrecio;
    private double fnCosto;
    private double fnDescuento;
    private double fnIva;
    private String fcNombreArticulo;
    private double fiAgranel;
    private int fiTipoDescuento;

    public double getFiAgranel() {
            return fiAgranel;
    }

    public void setFiAgranel(double fiAgranel) {
            this.fiAgranel = fiAgranel;
    }

    public String getFcNombreArticulo() {
            return fcNombreArticulo;
    }

    public void setFcNombreArticulo(String fcNombreArticulo) {
            this.fcNombreArticulo = fcNombreArticulo;
    }

    public long getFiArticuloId() {
            return fiArticuloId;
    }

    public void setFiArticuloId(long fiArticuloId) {
            this.fiArticuloId = fiArticuloId;
    }

    public String getFcCdGbBarras() {
            return fcCdGbBarras;
    }

    public void setFcCdGbBarras(String fcCdGbBarras) {
            this.fcCdGbBarras = fcCdGbBarras;
    }

    public double getFnCantidad() {
            return fnCantidad;
    }

    public void setFnCantidad(double fnCantidad) {
            this.fnCantidad = fnCantidad;
    }

    public double getFnPrecio() {
            return fnPrecio;
    }

    public void setFnPrecio(double fnPrecio) {
            this.fnPrecio = fnPrecio;
    }

    public double getFnCosto() {
            return fnCosto;
    }

    public void setFnCosto(double fnCosto) {
            this.fnCosto = fnCosto;
    }

    public double getFnDescuento() {
            return fnDescuento;
    }

    public void setFnDescuento(double fnDescuento) {
            this.fnDescuento = fnDescuento;
    }

    public double getFnIva() {
            return fnIva;
    }

    public void setFnIva(double fnIva) {
            this.fnIva = fnIva;
    }
            
    public int getFiTipoDescuento() {
		return fiTipoDescuento;
	}

	public void setFiTipoDescuento(int fiTipoDescuento) {
		this.fiTipoDescuento = fiTipoDescuento;
	}

	public ArticuloBean toClienteSolicitud(){
        ArticuloBean solicitud = new ArticuloBean();
        solicitud.setFiArticuloId		(this.getFiArticuloId		());
        solicitud.setFcCdGbBarras       (this.getFcCdGbBarras       ());
        solicitud.setFnCantidad         (this.getFnCantidad         ());
        solicitud.setFnPrecio           (this.getFnPrecio           ());
        solicitud.setFnCosto            (this.getFnCosto            ());
        solicitud.setFnDescuento        (this.getFnDescuento        ());
        solicitud.setFnIva              (this.getFnIva              ());
        solicitud.setFcNombreArticulo   (this.getFcNombreArticulo   ());
        solicitud.setFiAgranel          (this.getFiAgranel          ());
        solicitud.setFiTipoDescuento    (this.getFiTipoDescuento    ());
    
        return solicitud;
    }

    
    
    
}
