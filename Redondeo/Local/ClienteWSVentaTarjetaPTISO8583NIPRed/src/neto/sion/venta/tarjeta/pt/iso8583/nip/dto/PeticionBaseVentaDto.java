
package neto.sion.venta.tarjeta.pt.iso8583.nip.dto;

import neto.sion.tienda.genericos.dto.Base;

public class PeticionBaseVentaDto extends Base {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int paPaisId;
    private long paConciliacionId;

    public int getPaPaisId()
    {
        return paPaisId;
    }

    public void setPaPaisId(int paPaisId)
    {
        this.paPaisId = paPaisId;
    }

    public long getPaConciliacionId()
    {
        return paConciliacionId;
    }

    public void setPaConciliacionId(long paConciliacionId)
    {
        this.paConciliacionId = paConciliacionId;
    }

    @Override
    public String toString() {
        return "PeticionBaseVentaDto{" + "paPaisId=" + paPaisId + ", paConciliacionId=" + paConciliacionId + '}';
    }

  
}
