package neto.sion.venta.tarjeta.pt.iso8583.nip.dto;

import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.RespuestaVentaBean;

public class RespuestaVentaDto {
    private PeticionVentaBAZDto peticionBAZ;
    private RespuestaVentaBAZDto respuestaBAZ;
    private RespuestaReversoBAZDto respuestaReversoBAZ;
    private boolean seEnviaReverso;
    private String modoEntrada;
    private long trace;
    private int codigoError;
    private String descError;
    private long uId;
    private boolean pagoExitoso;

    public RespuestaVentaBAZDto getRespuestaBAZ() {
            return respuestaBAZ;
    }

    public void setRespuestaBAZ(RespuestaVentaBAZDto respuestaBAZ) {
            this.respuestaBAZ = respuestaBAZ;
    }

    public int getCodigoError() {
            return codigoError;
    }

    public void setCodigoError(int codigoError) {
            this.codigoError = codigoError;
    }

    public String getDescError() {
            return descError;
    }

    public void setDescError(String descError) {
            this.descError = descError;
    }

    public RespuestaReversoBAZDto getRespuestaReversoBAZ() {
            return respuestaReversoBAZ;
    }

    public void setRespuestaReversoBAZ(
                    RespuestaReversoBAZDto respuestaReversoBAZ) {
            this.respuestaReversoBAZ = respuestaReversoBAZ;
    }

    public String getModoEntrada() {
            return modoEntrada;
    }

    public void setModoEntrada(String modoEntrada) {
            this.modoEntrada = modoEntrada;
    }

    public PeticionVentaBAZDto getPeticionBAZ() {
            return peticionBAZ;
    }

    public void setPeticionBAZ(PeticionVentaBAZDto peticionBAZ) {
            this.peticionBAZ = peticionBAZ;
    }

    public boolean isSeEnviaReverso() {
            return seEnviaReverso;
    }

    public void setSeEnviaReverso(boolean seEnviaReverso) {
            this.seEnviaReverso = seEnviaReverso;
    }

    public long getTrace() {
            return trace;
    }

    public void setTrace(long trace) {
            this.trace = trace;
    }

    public long getuId() {
            return uId;
    }

    public void setuId(long uId) {
            this.uId = uId;
    }



    public boolean isPagoExitoso() {
            return pagoExitoso;
    }

    public void setPagoExitoso(boolean pagoExitoso) {
            this.pagoExitoso = pagoExitoso;
    }

    @Override
    public String toString() {
            return "RespuestaVentaBean [codigoError=" + codigoError
                            + ", descError=" + descError + ", modoEntrada=" + modoEntrada
                            + ", peticionBAZ=" + peticionBAZ + ", respuestaBAZ="
                            + respuestaBAZ + ", respuestaReversoBAZ=" + respuestaReversoBAZ
                            + ", seEnviaReverso=" + seEnviaReverso + ", trace=" + trace
                            + ", uId=" + uId + "]";
    }

    public void crearRespuesta(RespuestaVentaBean respuesta) {
	this.setPeticionBAZ            (new PeticionVentaBAZDto(respuesta.getPeticionBAZ	    ()));
        this.setRespuestaBAZ           (new RespuestaVentaBAZDto(respuesta.getRespuestaBAZ           ()));
        this.setRespuestaReversoBAZ    (new RespuestaReversoBAZDto(respuesta.getRespuestaReversoBAZ    ()));
        this.setSeEnviaReverso         (respuesta.getSeEnviaReverso         ());
        this.setModoEntrada            (respuesta.getModoEntrada            ());
        this.setTrace                  (respuesta.getTrace                  ());
        this.setCodigoError            (respuesta.getCodigoError            ());
        this.setDescError              (respuesta.getDescError              ());
        this.setuId                    (respuesta.getUId                    ());
        this.setPagoExitoso            (respuesta.getPagoExitoso            ());
    }
}
