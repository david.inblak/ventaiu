package neto.sion.venta.ws.tarjeta.pt.iso8583.nip.red.ctrl;

import java.text.SimpleDateFormat;
import java.util.Date;

import Com.Elektra.Definition.Config.ConfigSION;

import neto.sion.Logs;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaRegistroPagoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaCombinadaBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.ctrl.ControladorPrincipal;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.ctrl.VentaControlador;

import neto.sion.venta.ventageneral.iso8583Red.bean.PeticionPagatodoNipBean;
import neto.sion.venta.ventageneral.iso8583Red.bean.RespuestaPagatodoNipBean;
import neto.sion.venta.ventageneral.iso8583Red.bean.WSExcepcion;

import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionConfirmaVentaArticulosBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionConfirmaVentaNormalBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionTransaccionCentralBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaArticulosBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaNormalBean;

public class WSVentaTarjetaPTISO8583NIPCtrl {
	
		
	/**
	 * 
	 * @param _peticionVenta
	 * @return
	 */
	public RespuestaPagatodoNipBean consultaSaldoPTdo (PeticionPagatodoNipBean _peticionConSaldo)
			throws WSExcepcion {
		System.out.println(_peticionConSaldo==null? "WSContrl > Peticion nula ":"WSContrl > Peticion no nula");
		ControladorPrincipal appControlador = new ControladorPrincipal(Logs.getIdentificador());
		return appControlador.consultaSaldoPTdo(_peticionConSaldo);
	}
	
	/**
	 * 
	 * @param _peticionVentaTarjeta
	 * @deprecated NO UTILIZAR POR QUE NO GRABA IEPS
	 * @return
	 */
	public RespuestaVentaCombinadaBean registrarVentaTarjetaPTdo(
			PeticionVentaBean _peticionVentaTarjeta, 
			PeticionVentaNormalBean _peticionVentaN,
			PeticionPagatodoNipBean _requestPT)
			throws WSExcepcion
	{
		Logs logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
		logeo.setIdLog( Logs.getIdentificador() );
		
		int VENTA_EXITOSA = 0;
		int PAGO_ENLINEA = 0;
		int TIPOMOVTO_VENTA_INICIAL = 1;
		int TIPOMOVTO_VENTA_CONFIRMACION = 3;
		
		int TIPODEVOL_CONFIRMACION = 2;
		
		SimpleDateFormat sd = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		
		RespuestaVentaCombinadaBean respCombinada = new RespuestaVentaCombinadaBean();
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		
		VentaControlador appControladorVenta = new VentaControlador(logeo.getIdLog());
		neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean 
		respVentaFinal = null;
		
		_peticionVentaN.setFueraLinea(PAGO_ENLINEA);
		_peticionVentaN.setPaTipoMovto(TIPOMOVTO_VENTA_INICIAL);
		logeo.log("Venta incial: Se registra la venta antes de enviar pago de tarjeta.");
		logeo.log("Venta incial :: peticion : "+_peticionVentaN);
		neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean 
			respVentaInicial = appControladorVenta.registrarVenta(_peticionVentaN);
		logeo.log("Venta incial :: respuesta : "+respVentaInicial);
		respCombinada.setRespuestaVentaNormal(respVentaInicial);
		
		if( respVentaInicial.getPaCdgError() == VENTA_EXITOSA ){
			ControladorPrincipal appControlador = new ControladorPrincipal(logeo.getIdLog());
			RespuestaRegistroPagoBean resultOperacionTarjeta=new RespuestaRegistroPagoBean();
			RespuestaPagatodoNipBean _respuestaPTdo = new RespuestaPagatodoNipBean();

			logeo.log("Venta incial :: Se registr� correctamente");
			
			String strTransaccionId = String.valueOf(respVentaInicial.getPaTransaccionId());
			_peticionVentaN.setTransaccionDev(respVentaInicial.getPaTransaccionId());
			
			
			if( strTransaccionId.length()>12 ){
				_requestPT.setNoTicket( strTransaccionId.substring( strTransaccionId.length() - 12, strTransaccionId.length()) );
			}
			
			_requestPT.setFechaHoraTransaccion( sd.format(new Date()) );
			
			logeo.log("pago tarjeta :: Iniciando registro de venta con tarjeta.");
			respuesta = appControlador.registrarVentaTarjeta(_peticionVentaTarjeta, _requestPT, resultOperacionTarjeta, _respuestaPTdo);
			logeo.log("pago tarjeta :: respuesta : "+respuesta);
			logeo.log("pago tarjeta :: respuesta : "+respuesta.getRespuestaPagaTodo());
			
			if(respuesta != null && respuesta.isPagoExitoso()){
				logeo.log("pago tarjeta :: respondio a la solicitud.");
				
				logeo.log("Venta tarjeta: se registro la venta con tarjeta con �xito.");
				//Confirmacion de venta pagatodo
				respVentaInicial.setPaTaNumOperacion( Long.parseLong(_respuestaPTdo.getrNoConfirmacion()) );
				PeticionConfirmaVentaNormalBean peticionVentaNormal = new PeticionConfirmaVentaNormalBean(_peticionVentaN, respVentaInicial.getPaTransaccionId());
				peticionVentaNormal.setNumTransaccion( respVentaInicial.getPaTransaccionId() );
				
				neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean respuestaProcesoPagosTarjeta = 
					appControladorVenta.procesaTiposPago(_peticionVentaN, respVentaInicial);/**/
				
				logeo.log("pago tarjeta :: solicitud :: 3 :: "+_peticionVentaTarjeta);
				appControlador.confirmaPagoTarjeta(
						_peticionVentaTarjeta,resultOperacionTarjeta,_respuestaPTdo, _requestPT, respVentaInicial.getPaTransaccionId());
						
				appControladorVenta.registrarConfirmacionVenta(peticionVentaNormal );
				//RespuestaPagatodoBean respuestaPTdo;
				//respCombinada.setRespuestaVentaNormal(respVentaInicial);
			}else{
				neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean respuestaProcesoPagosTarjeta = 
						appControladorVenta.procesaTiposPago(_peticionVentaN, respVentaInicial);
				
				logeo.log("Venta tarjeta: rechazado por emisor.");
				RespuestaRegistroPagoBean respCancelacion = appControlador.cancelaPagoTarjeta(
						_peticionVentaTarjeta, resultOperacionTarjeta, _respuestaPTdo, _requestPT, respVentaInicial.getPaTransaccionId());
				
				_peticionVentaN.setFueraLinea(PAGO_ENLINEA);
				_peticionVentaN.setPaTipoMovto(TIPOMOVTO_VENTA_CONFIRMACION);
				_peticionVentaN.setTipoDevolucion(TIPODEVOL_CONFIRMACION);
				_peticionVentaN.setTransaccionDev(respVentaInicial.getPaTransaccionId());//TODO: Confirmar con hector ya que no se envia transaccion dev
								
				//Registra venta con estatus de no aplicada
				//respuesta = appControlador.registrarReversoTarjeta(_peticionVentaTarjeta, respuesta );
				logeo.log("Venta incial :: Se env�a cancelaci�n.");
				respVentaFinal = appControladorVenta.registrarCancelacionVenta(_peticionVentaN);
				
				logeo.log("Venta tarjeta :: respuesta paga todo :: "+respuesta.getRespuestaPagaTodo());
				
				
				if( respuesta != null ){
					respVentaFinal.setPaCdgError (respuesta.getCodigoError());
					respVentaFinal.setPaDescError(respuesta.getDescError());
				}/*else{
					respVentaFinal.setPaCdgError (-1);
					respVentaFinal.setPaDescError("Pago rechazado por el emisor.");
				}*/
				
				logeo.log("Venta incial :: respuesta : "+respVentaFinal);
				respCombinada.setRespuestaVentaNormal(respVentaFinal);
			}
		}else{
			logeo.log("Venta incial: Hubo un problema al registrar la venta.");
			respuesta = new RespuestaVentaBean();
			respuesta.setCodigoError(respVentaInicial.getPaCdgError());
			respuesta.setDescError(respVentaInicial.getPaDescError());
		}
		
		respCombinada.setRespuestaTarjeta(respuesta);
		
		return respCombinada;
	}
	
	/**
	 * @param _peticionVentaTarjeta
	 * @return
	 */
	public RespuestaVentaCombinadaBean registrarVentaTarjetaPTdo(PeticionVentaBean _peticionVentaTarjeta, 
																 PeticionVentaArticulosBean _peticionVentaArticulos,
																 PeticionPagatodoNipBean _requestPT) throws WSExcepcion{
		Logs logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
		logeo.setIdLog( Logs.getIdentificador() );
		
		int VENTA_EXITOSA = 0;
		int PAGO_ENLINEA = 0;
		int TIPOMOVTO_VENTA_INICIAL = 1;
		int TIPOMOVTO_VENTA_CONFIRMACION = 3;
		int TIPOMOVTO_VENTA_CANCELACION = 4;
		
		int TIPODEVOL_CONFIRMACION = 2;
		
		SimpleDateFormat sd = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		//TODO: Registra un pago de tarjeta para pagatodo antes de registrar la venta
		
		RespuestaVentaCombinadaBean respCombinada = new RespuestaVentaCombinadaBean();
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		
		VentaControlador appControladorVenta = new VentaControlador(logeo.getIdLog());
		neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean respVentaFinal = null;
		
		_peticionVentaArticulos.setFueraLinea(PAGO_ENLINEA);
		_peticionVentaArticulos.setPaTipoMovto(TIPOMOVTO_VENTA_INICIAL);
		_peticionVentaArticulos.getTiposPago();
		
		Long idPagoTarjetaPT = null;
		
		logeo.log("Venta incial: Se registra la venta antes de enviar pago de tarjeta.");
		logeo.log("Venta incial :: peticion : "+_peticionVentaArticulos);
		neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean respVentaInicial = appControladorVenta.registrarVenta(_peticionVentaArticulos, idPagoTarjetaPT);
		
		logeo.log("Venta incial :: respuesta : "+respVentaInicial);
		respCombinada.setRespuestaVentaNormal(respVentaInicial);
		
		if( respVentaInicial.getPaCdgError() == VENTA_EXITOSA ){
			ControladorPrincipal appControlador = new ControladorPrincipal(logeo.getIdLog());
			RespuestaRegistroPagoBean resultOperacionTarjeta=new RespuestaRegistroPagoBean();
			RespuestaPagatodoNipBean _respuestaPTdo = new RespuestaPagatodoNipBean();
			
			logeo.log("Venta incial :: Se registro correctamente");
			
			String strTransaccionId = String.valueOf(respVentaInicial.getPaTransaccionId());	
			//_peticionVentaArticulos.setTransaccionDev(respVentaInicial.getPaTransaccionId());
			
			
			_requestPT.setNoTicket(strTransaccionId);

			/*if( strTransaccionId.length()>12 ){
				_requestPT.setNoTicket( strTransaccionId.substring( strTransaccionId.length() - 12, strTransaccionId.length()) );
			}*/
			
			_requestPT.setFechaHoraTransaccion( sd.format(new Date()) );		
			
			logeo.log("pago tarjeta :: Iniciando registro de venta con tarjeta.");			
			respuesta = appControlador.registrarVentaTarjeta(_peticionVentaTarjeta, _requestPT, resultOperacionTarjeta, _respuestaPTdo);
			logeo.log("pago tarjeta :: respuesta : "+respuesta);
			logeo.log("pago tarjeta :: respuesta : "+respuesta.getRespuestaPagaTodo().toString());
			
			if(respuesta != null && respuesta.isPagoExitoso()){
				logeo.log("pago tarjeta :: respondio a la solicitud.");
				
				logeo.log("Venta tarjeta: se registro la venta con tarjeta con exito.");
				//Confirmacion de venta pagatodo
				PeticionConfirmaVentaArticulosBean peticionVentaArticulos = new PeticionConfirmaVentaArticulosBean(_peticionVentaArticulos, respVentaInicial.getPaTransaccionId());
				
				peticionVentaArticulos.setNumTransaccion( respVentaInicial.getPaTransaccionId() );
				
				/*neto.sion.venta.ventanormal.pagatodo.bean.RespuestaVentaBean 
					respuestaProcesoPagosTarjeta = appControladorVenta.procesaTiposPago(_peticionVentaN, respVentaInicial);*/
				logeo.log("pago tarjeta :: solicitud :: 3 :: "+_peticionVentaTarjeta);
				RespuestaRegistroPagoBean respConfirmPagotarjeta = appControlador.confirmaPagoTarjeta(
						_peticionVentaTarjeta, resultOperacionTarjeta, _respuestaPTdo,  _requestPT,respVentaInicial.getPaTransaccionId());
				
				appControladorVenta.registrarConfirmacionVenta(peticionVentaArticulos );
				//RespuestaPagatodoBean respuestaPTdo;
				//respCombinada.setRespuestaVentaNormal(respVentaInicial);
			}else{
				
				logeo.log("Venta tarjeta: Hubo un problema al registrar la venta.");
				appControlador.cancelaPagoTarjeta(_peticionVentaTarjeta, resultOperacionTarjeta, 
												  respuesta.getRespuestaPagaTodo(),_requestPT, 
												  respVentaInicial.getPaTransaccionId());
				
				_peticionVentaArticulos.setFueraLinea(PAGO_ENLINEA);
				//_peticionVentaArticulos.setPaTipoMovto(TIPOMOVTO_VENTA_CONFIRMACION);
				_peticionVentaArticulos.setPaTipoMovto(TIPOMOVTO_VENTA_CANCELACION);
				
				//_peticionVentaArticulos.setTipoDevolucion(TIPODEVOL_CONFIRMACION);
				
				//_peticionVentaArticulos.setTransaccionDev(respVentaInicial.getPaTransaccionId());//TODO: Confirmar con hector ya que no se envia transaccion dev
								
				//Registra venta con estatus de no aplicada
				//respuesta = appControlador.registrarReversoTarjeta(_peticionVentaTarjeta, respuesta );
				logeo.log("Venta incial :: Se envia cancelacion de Transaccion ["+respVentaInicial.getPaTransaccionId()+"]");
				respVentaFinal = appControladorVenta.registrarCancelacionVenta(_peticionVentaArticulos, TIPOMOVTO_VENTA_CONFIRMACION,respVentaInicial.getPaTransaccionId());
								
				logeo.log("Venta tarjeta :: respuesta paga todo :: Transaccion ["+respVentaInicial.getPaTransaccionId()+"] "+respuesta.getRespuestaPagaTodo());
				
				
				if( respuesta != null ){
					respVentaFinal.setPaCdgError (respuesta.getCodigoError());
					respVentaFinal.setPaDescError(respuesta.getDescError());
				}/*else{
					respVentaFinal.setPaCdgError (-1);
					respVentaFinal.setPaDescError("Pago rechazado por el emisor.");
				}*/
				
				logeo.log("Venta incial :: respuesta : Transaccion ["+respVentaInicial.getPaTransaccionId()+"] "+respVentaFinal);
				respCombinada.setRespuestaVentaNormal(respVentaFinal);
			}
		}else{
			logeo.log("Venta incial: Hubo un problema al registrar la venta.");
			respuesta = new RespuestaVentaBean();
			respuesta.setCodigoError(respVentaInicial.getPaCdgError());
			respuesta.setDescError(respVentaInicial.getPaDescError());
		}
		
		respCombinada.setRespuestaTarjeta(respuesta);
		
		return respCombinada;
	}
	
	
	/**
	 * 
	 * @param _peticionReverso
	 * @return
	 */
	public long consultaTransaccionId( PeticionTransaccionCentralBean _peticion )
			 throws WSExcepcion {
		VentaControlador appControlador = new VentaControlador(Logs.getIdentificador());
		return appControlador.consultarTransaccionCentral(_peticion);
	}
}
