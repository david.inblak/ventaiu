package neto.sion.venta.ws.tarjeta.pt.iso8583.nip.red.servicio;

import javax.jws.WebMethod;
import javax.jws.WebService;

import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaCombinadaBean;
import neto.sion.venta.ventageneral.iso8583Red.bean.PeticionPagatodoNipBean;
import neto.sion.venta.ventageneral.iso8583Red.bean.RespuestaPagatodoNipBean;
import neto.sion.venta.ventageneral.iso8583Red.bean.WSExcepcion;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionTransaccionCentralBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaArticulosBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaNormalBean;
import neto.sion.venta.ws.tarjeta.pt.iso8583.nip.red.ctrl.WSVentaTarjetaPTISO8583NIPCtrl;

@WebService (name="WSVentaTarjetaPTISO8583NIPRed")
public class WSVentaTarjetaPTISO8583NIPRed{
	
	private WSVentaTarjetaPTISO8583NIPCtrl ctrl;
	
	public WSVentaTarjetaPTISO8583NIPRed(){
		ctrl = new WSVentaTarjetaPTISO8583NIPCtrl();
	}
	
	
	/** Metodo encargado de registrar en BD y enviar a PAGATODO un pago de tarjeta NIP
	 * @param _peticion
	 */
	@WebMethod 
	public RespuestaPagatodoNipBean consultaSaldoPTdo(PeticionPagatodoNipBean _peticionConSaldo)	throws WSExcepcion{
		return ctrl.consultaSaldoPTdo(_peticionConSaldo);
	}
	
	/** Metodo encargado de registrar en BD y enviar a PAGATODO un pago de tarjeta 
	 * @param _peticion
	 * @deprecated NO UTILIZAR POR QUE NO REGISTRA IEPS
	 */
	@WebMethod
	public RespuestaVentaCombinadaBean registrarVentaTarjetaPTdo(
				PeticionVentaBean _peticionVenta, 
				PeticionVentaNormalBean _peticionVentaN,
				PeticionPagatodoNipBean _requestPT)
					throws WSExcepcion{
		return ctrl.registrarVentaTarjetaPTdo( _peticionVenta, _peticionVentaN, _requestPT );
	}
	
	/** Metodo encargado de registrar en BD y enviar a PAGATODO un pago de tarjeta 
	 * @param _peticion
	 * 
	 */
	@WebMethod
	public RespuestaVentaCombinadaBean registrarVentaTarjetaPagaTodoNIPRed(PeticionVentaBean _peticionVenta, 
																	    PeticionVentaArticulosBean _peticionVentaArticulos,
																	    PeticionPagatodoNipBean _requestPT) throws WSExcepcion{
		return ctrl.registrarVentaTarjetaPTdo( _peticionVenta, _peticionVentaArticulos, _requestPT );
	}
	
	 
	/** Metodo encargado de registrar en BD y enviar a BAZ un reverso de tarjeta 
	 * @param _peticion
	 */
	@WebMethod
	public long consultarTransaccionCentral(PeticionTransaccionCentralBean _peticion) throws WSExcepcion{
		return ctrl.consultaTransaccionId(_peticion);
	}
	
}

