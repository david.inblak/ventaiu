package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

public class PeticionActReimpTicketBean extends PeticionBaseVentaBean {
	private long paMovimientoId;

	public long getPaMovimientoId() {
		return paMovimientoId;
	}

	public void setPaMovimientoId(long paMovimientoId) {
		this.paMovimientoId = paMovimientoId;
	}

	@Override
	public String toString() {
		return "PeticionActReimpTicketBean [paMovimientoId=" + paMovimientoId
				+ super.toString() + "]";
	}

}
