package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

/***
 * Clase que represenra la respuesta del marcado de pago con tarjeta
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class RespuestaMarcadoPagoTarjetaBean {
	private long paPagoTarjetasId;
	private int paCdgError;
	private String paDescError;

	public long getPaPagoTarjetasId() {
		return paPagoTarjetasId;
	}

	public void setPaPagoTarjetasId(long paPagoTarjetasId) {
		this.paPagoTarjetasId = paPagoTarjetasId;
	}

	public int getPaCdgError() {
		return paCdgError;
	}

	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}

	public String getPaDescError() {
		return paDescError;
	}

	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}

	@Override
	public String toString() {
		return "RespuestaMarcadoPagoTarjetaBean[paPagoTarjetasId="
				+ paPagoTarjetasId + ",paCdgError=" + paCdgError
				+ ",paDescError=" + paDescError + "]";
	}
}
