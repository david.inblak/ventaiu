package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

/***
 * Clase que representa la peticion para el marcado de pago con tarjeta
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class PeticionMarcadoPagoTarjetaBean extends PeticionBaseVentaBean {
	private int paProcesoExc;
	private long paPagoTarjetaIdBus;
	private int paTipoPagoId;
	private long paNumTransaccion;
	private int paEstatusTarjetaId;
	private double paComision;

	public double getPaComision() {
		return paComision;
	}

	public void setPaComision(double paComision) {
		this.paComision = paComision;
	}

	public int getPaProcesoExc() {
		return paProcesoExc;
	}

	public void setPaProcesoExc(int paProcesoExc) {
		this.paProcesoExc = paProcesoExc;
	}

	public long getPaPagoTarjetaIdBus() {
		return paPagoTarjetaIdBus;
	}

	public void setPaPagoTarjetaIdBus(long paPagoTarjetaIdBus) {
		this.paPagoTarjetaIdBus = paPagoTarjetaIdBus;
	}

	public int getPaTipoPagoId() {
		return paTipoPagoId;
	}

	public void setPaTipoPagoId(int paTipoPagoId) {
		this.paTipoPagoId = paTipoPagoId;
	}

	public long getPaNumTransaccion() {
		return paNumTransaccion;
	}

	public void setPaNumTransaccion(long paNumTransaccion) {
		this.paNumTransaccion = paNumTransaccion;
	}

	public int getPaEstatusTarjetaId() {
		return paEstatusTarjetaId;
	}

	public void setPaEstatusTarjetaId(int paEstatusTarjetaId) {
		this.paEstatusTarjetaId = paEstatusTarjetaId;
	}

	@Override
	public String toString() {
		return "PeticionMarcadoPagoTarjetaBean [" 
				+ "paProcesoExc=" + paProcesoExc
				+ ", paPagoTarjetaIdBus=" + paPagoTarjetaIdBus
				+ ", paTipoPagoId=" + paTipoPagoId 
				+ ", paNumTransaccion=" + paNumTransaccion 
				+ ", paEstatusTarjetaId=" + paEstatusTarjetaId 
				+ ", paComision=" + paComision
				+ ", paisId=" + getPaPaisId()
				+ ", tiendaId=" + getTiendaId()
				+ super.toString() + "]";
	}

}
