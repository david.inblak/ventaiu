package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

/***
 * Clase que representa la respuesta de actualizacion de reimpresion de voucher.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class RespuestaActReimpVoucherBean {
	private int paCdgError;
	private String paDescError;

	public int getPaCdgError() {
		return paCdgError;
	}

	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}

	public String getPaDescError() {
		return paDescError;
	}

	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}

	@Override
	public String toString() {
		return "RespuestaActReimpVoucherBean [paCdgErrorr=" + paCdgError
				+ ", paDescError=" + paDescError + "]";
	}

}
