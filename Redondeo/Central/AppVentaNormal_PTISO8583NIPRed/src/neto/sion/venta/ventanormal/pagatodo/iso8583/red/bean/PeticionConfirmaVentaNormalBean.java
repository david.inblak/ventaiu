package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

public class PeticionConfirmaVentaNormalBean extends PeticionBaseVentaBean {
	private long numTransaccion;
	private long usuarioId;
	
	
	public PeticionConfirmaVentaNormalBean(
			PeticionVentaNormalBean _peticionVentaN,
			long transaccionId) {
		this.setIpTerminal(_peticionVentaN.getIpTerminal() );
		this.setPaConciliacionId(_peticionVentaN.getPaConciliacionId() );
		this.setPaPaisId(_peticionVentaN.getPaPaisId() );
		this.setTiendaId(_peticionVentaN.getTiendaId() );
		this.setNumTransaccion(transaccionId);
		this.setUsuarioId(_peticionVentaN.getPaUsuarioId());
	}
	
	public long getNumTransaccion() {
		return numTransaccion;
	}
	public void setNumTransaccion(long numTransaccion) {
		this.numTransaccion = numTransaccion;
	}
	public long getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(long usuarioId) {
		this.usuarioId = usuarioId;
	}
	@Override
	public String toString() {
		return "PeticionConfirmaVentaNormalBean [numTransaccion="
				+ numTransaccion + ", usuarioId=" + usuarioId
				+ ", getPaPaisId()=" + getPaPaisId()
				+ ", getPaConciliacionId()=" + getPaConciliacionId()
				+ ", toString()=" + super.toString() + ", getuId()=" + getuId()
				+ ", getIpTerminal()=" + getIpTerminal() + ", getTiendaId()="
				+ getTiendaId() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}
	
	
}
