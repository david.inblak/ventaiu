package neto.sion.venta.ventanormal.pagatodo.iso8583.red.ctrl;


import Com.Elektra.Definition.Config.ConfigSION;

//import neto.sion.genericos.log.Logeo;
import neto.sion.venta.ventageneral.iso8583Red.bean.PeticionPagatodoBean;
import neto.sion.venta.ventageneral.iso8583Red.bean.WSExcepcion;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionConfirmaVentaArticulosBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionConfirmaVentaNormalBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionTransaccionCentralBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaArticulosBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaNormalBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.dao.VentaNormalDAO;

/***
 * Clase controlador de venta normal.
 * 
 * @author Carlos V. Perez L.
 * 
 */

public class VentaNormalControlador {

	private VentaNormalDAO ventaNormalDAO;
	/*private int INTENTOS = Integer.valueOf(ConfigSION.obtenerParametro("intentos.devolucion"));
	private static char EN_LINEA;
	private static char FUERA_LINEA;
	private static int TRANSACCION_FORMATO_ERROR_CDG;
	private static String TRANSACCION_FORMATO_ERROR_DESC;
	private static int EN_lINEA_CDG;
	private static int FUERA_LINEA_CDG;*/

	public VentaNormalControlador(String idLog) {
		ventaNormalDAO = new VentaNormalDAO(idLog);
		/*EN_LINEA = ConfigSION.obtenerParametro(
				"venta.transaccion.enlinea.caracter").charAt(0);
		FUERA_LINEA = ConfigSION.obtenerParametro(
				"venta.transaccion.fueralinea.caracter").charAt(0);
		TRANSACCION_FORMATO_ERROR_CDG = Integer.valueOf(ConfigSION
				.obtenerParametro("venta.transaccionerror.codigo"));
		TRANSACCION_FORMATO_ERROR_DESC = ConfigSION
				.obtenerParametro("venta.transaccionerror.mensaje");
		EN_lINEA_CDG = Integer.valueOf(ConfigSION
				.obtenerParametro("venta.transaccion.enlinea.codigo"));
		FUERA_LINEA_CDG = Integer.valueOf(ConfigSION
				.obtenerParametro("venta.transaccion.fueralinea.codigo"));*/
	}

	

	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar la
	 * venta de T.A.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta de T.A.
	 * @throws AxisFault
	 
	public RespuestaVentaBean registrarVentaTA(PeticionVentaTABean _peticion)
			throws AxisFault {
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION
				.obtenerParametro("venta.enlinea.id")));
		return ventaNormalDAO.registrarVentaTA(_peticion);
	}*/

	

	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar
	 * una devolucion de venta de t.a.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la devolucion.
	 * @throws AxisFault
	 
	public RespuestaVentaBean registrarDevolucionTA(
			PeticionVentaTABean _peticion) throws AxisFault {
		AxisFault ex = null;
		RespuestaVentaBean respuesta = null;
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION
				.obtenerParametro("venta.enlinea.id")));
		int cont = 0;
		boolean flag = true;
		do {
			Logeo.log("Numero de intentos de devolucion: " + (cont + 1));
			try {
				respuesta = ventaNormalDAO.registrarDevolucionTA(_peticion);
			} catch (AxisFault e) {
				flag = false;
				ex = e;
			}
			cont++;
		} while (cont < INTENTOS && flag == false);
		if (flag == false && respuesta == null) {
			throw ex;
		}

		return respuesta;
	}*/

	/***
	 * Realiza una llamada al metodo consultaBloqueos de la clase VentaNormalDAO
	 * 
	 * @param _cajeroId
	 * @return Un arreglo de objetos de BloqueoBean .
	 * @throws AxisFault
	 
	public BloqueoBean[] consultaBloqueos(PeticionBloqueoBean _peticion)
			throws AxisFault {
		return ventaNormalDAO.consultaBloqueos(_peticion);
	}*/

	/***
	 * M�todo utilizada para actualizar los datos necesarios de la reimpresion
	 * de vouchers
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la actualizacion de los datos.
	 * @throws AxisFault
	 
	public RespuestaActReimpVoucherBean actualizarReimpresionVoucher(
			PeticionReimpresionBean _peticion) throws AxisFault {
		char parametro = _peticion.getPaNumTransaccion().charAt(
				_peticion.getPaNumTransaccion().length() - 1);
		String transaccion = _peticion.getPaNumTransaccion().substring(0,
				_peticion.getPaNumTransaccion().length() - 1);
		if (parametro == EN_LINEA) {
			_peticion.setPaParamTipoTrans(EN_lINEA_CDG);
		} else if (parametro == FUERA_LINEA) {
			_peticion.setPaParamTipoTrans(FUERA_LINEA_CDG);
		} else {
			RespuestaActReimpVoucherBean respuesta = new RespuestaActReimpVoucherBean();
			respuesta.setPaCdgError(TRANSACCION_FORMATO_ERROR_CDG);
			respuesta.setPaDescError(TRANSACCION_FORMATO_ERROR_DESC);
			return respuesta;
		}
		_peticion.setPaNumTransaccion(transaccion);
		return ventaNormalDAO.actualizarReimpresionVoucher(_peticion);
	}*/

	/***
	 * Metodo encargado de consultar el numero de transaccion de la base central
	 * 
	 * @param _peticion
	 * @return Un long 0 => No se encontro el registro, otro =>Numero de
	 *         transaccion.
	 * @throws AxisFault
	 */
	public long consultarTransaccionCentral(
			PeticionTransaccionCentralBean _peticion) throws WSExcepcion {
		return ventaNormalDAO.consultarTransaccionCentral(_peticion);
	}

	/****
	 * Metodo que consulta el numero de transaccion de una venta de TA ademas
	 * del posible numero de Autorizacion
	 * 
	 * @param _peticion
	 * @return
	 * @throws AxisFault
	 

	public RespuestaConsultaTransaccionTABean consultarTransaccionTACentral(
			PeticionTransaccionCentralBean _peticion) throws AxisFault {
		RespuestaConsultaTransaccionTABean respuesta = new RespuestaConsultaTransaccionTABean();

		long transaccion = ventaNormalDAO
				.consultarTransaccionCentral(_peticion);
		if (transaccion == 0) {
			respuesta.setCdgError(1);
			respuesta.setDescError(ConfigSION
					.obtenerParametro("VENTA.TRANSACCIONTA.ERROR"));
		} else {
			RespuestaAutorizacionTABean respConsulta = null;
			respConsulta = ventaNormalDAO.obtenerAutorizacionTA(
					_peticion.getPaPaisId(), _peticion.getTiendaId(),
					transaccion);
			if (respConsulta.getCdgError() == 0) {
				respuesta.setCdgError(respConsulta.getCdgError());
				respuesta.setDescError(respConsulta.getDescError());
				respuesta.setNumTransaccion(transaccion);
				respuesta.setNumAutorizacion(respConsulta.getTransaccionTA());
			} else {
				respuesta.setCdgError(respConsulta.getCdgError());
				respuesta.setDescError(respConsulta.getDescError());
			}
		}
		return respuesta;
	}*/

	/***
	 * Metodo encargado de confirmar la reimpresion de un ticket.
	 * 
	 * @param _peticion
	 * @return Un objeto con el detalle de la reimpresion.
	 * @throws AxisFault
	 
	public RespuestaActReimpTicketBean confirmarReimpresionTicket(
			PeticionActReimpTicketBean _peticion) throws AxisFault {
		return ventaNormalDAO.confirmarReimpresionTicket(_peticion);
	}*/

	/***
	 * Metodo encargado de consultar datos de una tarjeta.
	 * 
	 * @param _peticion
	 * @return Un objeto con el detalle de la tarjeta.
	 * @throws AxisFault
	 
	public RespuestaTarjetaBean consultarTarjeta(PeticionTarjetaBean _peticion)
			throws AxisFault {
		return ventaNormalDAO.consultarTarjeta(_peticion);
	}*/

	/**
	 * M�todo encargado de consultar las �ltimas 10 transacciones generadas en
	 * BD Central
	 * 
	 * @return Un objeto de respuesta con las consultas
	 * @throws AxisFault
	 

	public RespuestaUltimasTransaccionesVentaBean consultarTransacciones(
			PeticionUltimasTransaccionesVentaBean _peticion) throws AxisFault {
		return ventaNormalDAO.consultaUltimasTransacciones(_peticion);
	}*/

	
	
	/**************************************************************/
	/******************remover despues de ieps************************/
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormalDAO.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 
	public RespuestaVentaBean registrarVenta(PeticionVentaNormalBean _peticion)
			throws AxisFault {
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION
				.obtenerParametro("venta.enlinea.id")));
		return ventaNormalDAO.registrarVenta(_peticion);
	}*/
	
		
	/***
	 * @Deprecated Se reemplaza por el metodo cuyo argumento es PeticionVentaArticulosBean 
	 * NO UTILIZAR
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormalDAO.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarVenta(PeticionVentaNormalBean _peticion)
			throws WSExcepcion {
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION.obtenerParametro("venta.enlinea.id")));
		
		return ventaNormalDAO.registrarVenta(_peticion);
	}
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormalDAO.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarVenta(PeticionVentaArticulosBean _peticion, Long idPagoTarjetaPT)
			throws WSExcepcion {
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION.obtenerParametro("venta.enlinea.id")));
		
		return ventaNormalDAO.registrarVenta(_peticion, idPagoTarjetaPT);
	}
	
	public RespuestaVentaBean procesaTiposPago(PeticionVentaNormalBean _peticion, RespuestaVentaBean respuesta)
			throws WSExcepcion {
		return ventaNormalDAO.procesaTiposPago(_peticion, respuesta);
	}
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormalDAO.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 * @deprecated NO UTILIZAR, NO GRABA IEPS
	 */
	public RespuestaVentaBean registrarCancelacionVenta(PeticionVentaNormalBean _peticion)
			throws WSExcepcion {
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION.obtenerParametro("venta.enlinea.id")));
		
		return ventaNormalDAO.registrarCancelacionVenta(_peticion);
	}
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormalDAO.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarCancelacionVenta(PeticionVentaArticulosBean _peticion, int _tipoDevolucion, long paTransaccionVentaId)
			throws WSExcepcion {
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION.obtenerParametro("venta.enlinea.id")));
		
		return ventaNormalDAO.registrarCancelacionVenta(_peticion, _tipoDevolucion, paTransaccionVentaId);
	}
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormalDAO.
	 * 
	 * @param _peticion
	 * @deprecated NO UTILIZAR, NO GRABA IEPS
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarConfirmacionVenta(PeticionConfirmaVentaNormalBean _peticion)
			throws WSExcepcion {
		
		return ventaNormalDAO.registrarConfirmacionVenta(_peticion);
	}
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormalDAO.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarConfirmacionVenta(PeticionConfirmaVentaArticulosBean _peticion)
			throws WSExcepcion {
		
		return ventaNormalDAO.registrarConfirmacionVenta(_peticion);
	}
	
	
	
	/**
	 * M�todo encargado de consultar el detalle de un ticket para su reimpresi�n
	 * 
	 * @param _peticion
	 *            un objeto de petici�n de detalle del ticket
	 * @return Un objeto de respuesta cone ld etalle delt ticket
	 * @throws AxisFault
	 * 
	 

	public RespuestaTicketReimpresionBean consultarTicketReimpresion(
			PeticionTicketReimpresionBean _peticion) throws AxisFault {
		return ventaNormalDAO.consultarDetalleTicket(_peticion);
	}*/
	
	/***
	 * Realiza una llama al metodo obtenerDetalleVenta de la clase
	 * VentaNormalDAO.
	 * --no contiene ieps, eliminar una vez implementado este
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la venta.
	 * @throws AxisFault
	
	public RespuestaDetalleVentaBean obtenerDetalleVenta(
			PeticionDetalleVentaBean _peticion) throws AxisFault {
		char parametro = _peticion.getPaNumTransaccion().charAt(
				_peticion.getPaNumTransaccion().length() - 1);
		String transaccion = _peticion.getPaNumTransaccion().substring(0,
				_peticion.getPaNumTransaccion().length() - 1);
		int tipo = 0;
		if (parametro == EN_LINEA) {
			tipo = EN_lINEA_CDG;
		} else if (parametro == FUERA_LINEA) {
			tipo = FUERA_LINEA_CDG;
		} else {
			RespuestaDetalleVentaBean respuesta = new RespuestaDetalleVentaBean();
			respuesta.setPaCdgError(TRANSACCION_FORMATO_ERROR_CDG);
			respuesta.setPaDescError(TRANSACCION_FORMATO_ERROR_DESC);
			return respuesta;
		}
		_peticion.setPaNumTransaccion(transaccion);
		return ventaNormalDAO.obtenerDetalleVenta(_peticion, tipo);
	} */

	/***
	 * Realiza una llamada al metodo registrarDevolucion de la clase
	 * VentaNormalDAO.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la devolucion.
	 * @throws AxisFault
	 
	public RespuestaVentaBean registrarDevolucion(
			PeticionDevolucionBean _peticion) throws AxisFault {
		AxisFault ex = null;
		RespuestaVentaBean respuesta = null;
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION
				.obtenerParametro("venta.enlinea.id")));
		int cont = 0;
		boolean flag = true;
		do {
			Logeo.log("Numero de intentos de devolucion: " + (cont + 1));
			try {
				respuesta = ventaNormalDAO.registrarDevolucion(_peticion);
				flag = true;
			} catch (AxisFault e) {
				flag = false;
				ex = e;
			}
			cont++;
		} while (cont < INTENTOS && flag == false);
		if (flag == false && respuesta == null) {
			throw ex;
		}

		return respuesta;
	}*/
	
	/************************************************************/
	/******************facturacion peter**************************/
	
	/**
	 * M�todo encargado de consultar el detalle de un ticket para su reimpresi�n, se agrega campo tipoCajaId
	 * @param _peticion
	 * @return
	 * @throws AxisFault
	
	public RespuestaDetalleTicketBean consultarTicketventa(
			PeticionTicketReimpresionBean _peticion) throws AxisFault{
		return ventaNormalDAO.consultarTicketVenta(_peticion);
	} */
	
	
	/*********************************************************/
	/***************nuevos m�todos ieps***********************/
	
	/***
	 * Realiza una llama al metodo obtenerDetalleVenta de la clase
	 * VentaNormalDAO.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la venta.
	 * @throws AxisFault
	
	public RespuestaConsultaVentaBean consultarDetalleVenta(
			PeticionDetalleVentaBean _peticion) throws AxisFault {
		char parametro = _peticion.getPaNumTransaccion().charAt(
				_peticion.getPaNumTransaccion().length() - 1);
		String transaccion = _peticion.getPaNumTransaccion().substring(0,
				_peticion.getPaNumTransaccion().length() - 1);
		int tipo = 0;
		if (parametro == EN_LINEA) {
			tipo = EN_lINEA_CDG;
		} else if (parametro == FUERA_LINEA) {
			tipo = FUERA_LINEA_CDG;
		} else {
			RespuestaConsultaVentaBean respuesta = new RespuestaConsultaVentaBean();
			respuesta.setPaCdgError(TRANSACCION_FORMATO_ERROR_CDG);
			respuesta.setPaDescError(TRANSACCION_FORMATO_ERROR_DESC);
			return respuesta;
		}
		_peticion.setPaNumTransaccion(transaccion);
		return ventaNormalDAO.consultarDetalleVenta(_peticion, tipo);
	} */

	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormalDAO.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 
	public RespuestaVentaBean registrarVenta(PeticionVentaArticulosBean _peticion)
			throws AxisFault {
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION
				.obtenerParametro("venta.enlinea.id")));
		return ventaNormalDAO.registrarVenta(_peticion);
	}*/
	
	/***
	 * Realiza una llamada al metodo registrarDevolucion de la clase
	 * VentaNormalDAO.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la devolucion.
	 * @throws AxisFault
	 
	public RespuestaVentaBean registrarDevolucion(
			PeticionDevolucionArticulosBean _peticion) throws AxisFault {
		AxisFault ex = null;
		RespuestaVentaBean respuesta = null;
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION
				.obtenerParametro("venta.enlinea.id")));
		int cont = 0;
		boolean flag = true;
		do {
			Logeo.log("Numero de intentos de devolucion: " + (cont + 1));
			try {
				respuesta = ventaNormalDAO.registrarDevolucion(_peticion);
				flag = true;
			} catch (AxisFault e) {
				flag = false;
				ex = e;
			}
			cont++;
		} while (cont < INTENTOS && flag == false);
		if (flag == false && respuesta == null) {
			throw ex;
		}

		return respuesta;
	}*/
	
	
	/************reimpresion devoluciones***************/
	
	/**
	 * M�todo que consulta el detalle de una operacion para su reimpresion, contempla devolucion
	 * quitar cuando antad este generico
	 * @param _peticion
	 * @return
	 * @throws AxisFault
	
	public RespuestaTicketReimpresionBean consultarDetalleTicket (PeticionTicketReimpresionBean _peticion) throws AxisFault{
		return ventaNormalDAO.consultarDetalleTicket(_peticion);
	} */
	
	
	/*REIMPRESION DE PS ANTAD
	public RespuestaReimpresionBean consultarTicket(PeticionTicketReimpresionBean _peticion) throws AxisFault{
		return ventaNormalDAO.consultarTicket(_peticion);
	}*/
}
