package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import java.util.Arrays;

/***
 * Clase que representa la peticion de una venta.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class PeticionVentaNormalBean {
	private long uId;
    private String ipTerminal;
    private long tiendaId;
    
	private int paPaisId;
	private long paConciliacionId;
	  
	private String paTerminal;
	private long paUsuarioId;
	private long paUsuarioAutorizaId;
	private String paFechaOper;
	private int paTipoMovto;
	private double paMontoTotalVta;
	private ArticuloBean[] articulos;
	private TipoPagoBean[] tiposPago;
	
	private int tipoDevolucion;
	private long transaccionDev;

	private int fueraLinea;

	public long getuId() {
		return uId;
	}

	public void setuId(long uId) {
		this.uId = uId;
	}

	public String getIpTerminal() {
		return ipTerminal;
	}

	public void setIpTerminal(String ipTerminal) {
		this.ipTerminal = ipTerminal;
	}

	public long getTiendaId() {
		return tiendaId;
	}

	public void setTiendaId(long tiendaId) {
		this.tiendaId = tiendaId;
	}

	public int getPaPaisId() {
		return paPaisId;
	}

	public void setPaPaisId(int paPaisId) {
		this.paPaisId = paPaisId;
	}

	public long getPaConciliacionId() {
		return paConciliacionId;
	}

	public void setPaConciliacionId(long paConciliacionId) {
		this.paConciliacionId = paConciliacionId;
	}

	public String getPaTerminal() {
		return paTerminal;
	}

	public void setPaTerminal(String paTerminal) {
		this.paTerminal = paTerminal;
	}

	public long getPaUsuarioId() {
		return paUsuarioId;
	}

	public void setPaUsuarioId(long paUsuarioId) {
		this.paUsuarioId = paUsuarioId;
	}

	public long getPaUsuarioAutorizaId() {
		return paUsuarioAutorizaId;
	}

	public void setPaUsuarioAutorizaId(long paUsuarioAutorizaId) {
		this.paUsuarioAutorizaId = paUsuarioAutorizaId;
	}

	public String getPaFechaOper() {
		return paFechaOper;
	}

	public void setPaFechaOper(String paFechaOper) {
		this.paFechaOper = paFechaOper;
	}

	public int getPaTipoMovto() {
		return paTipoMovto;
	}

	public void setPaTipoMovto(int paTipoMovto) {
		this.paTipoMovto = paTipoMovto;
	}

	public double getPaMontoTotalVta() {
		return paMontoTotalVta;
	}

	public void setPaMontoTotalVta(double paMontoTotalVta) {
		this.paMontoTotalVta = paMontoTotalVta;
	}

	public ArticuloBean[] getArticulos() {
		return articulos;
	}

	public void setArticulos(ArticuloBean[] articulos) {
		this.articulos = articulos;
	}

	public TipoPagoBean[] getTiposPago() {
		return tiposPago;
	}

	public void setTiposPago(TipoPagoBean[] tiposPago) {
		this.tiposPago = tiposPago;
	}

	public int getTipoDevolucion() {
		return tipoDevolucion;
	}

	public void setTipoDevolucion(int tipoDevolucion) {
		this.tipoDevolucion = tipoDevolucion;
	}

	public long getTransaccionDev() {
		return transaccionDev;
	}

	public void setTransaccionDev(long transaccionDev) {
		this.transaccionDev = transaccionDev;
	}

	public int getFueraLinea() {
		return fueraLinea;
	}

	public void setFueraLinea(int fueraLinea) {
		this.fueraLinea = fueraLinea;
	}

	@Override
	public String toString() {
		return "PeticionVentaNormalBean [uId=" + uId + ", ipTerminal="
				+ ipTerminal + ", tiendaId=" + tiendaId + ", paPaisId="
				+ paPaisId + ", paConciliacionId=" + paConciliacionId
				+ ", paTerminal=" + paTerminal + ", paUsuarioId=" + paUsuarioId
				+ ", paUsuarioAutorizaId=" + paUsuarioAutorizaId
				+ ", paFechaOper=" + paFechaOper + ", paTipoMovto="
				+ paTipoMovto + ", paMontoTotalVta=" + paMontoTotalVta
				+ ", articulos=" + Arrays.toString(articulos) + ", tiposPago="
				+ Arrays.toString(tiposPago) + ", tipoDevolucion="
				+ tipoDevolucion + ", transaccionDev=" + transaccionDev
				+ ", fueraLinea=" + fueraLinea + "]";
	}

	
}
