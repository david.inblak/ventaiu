package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import java.util.Arrays;

public class RespuestaReimpresionVoucherBean {
	private int cdgError;
	private String descError;
	private long paUsuarioId;
	private String paFechaCreacion;
	private String paEstacionTrabajo;
	private long paMovimientoId;
	private ReimpresionTarjetaBean[] paReimpresionesTar;

	public int getCdgError() {
		return cdgError;
	}

	public void setCdgError(int cdgError) {
		this.cdgError = cdgError;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	public long getPaUsuarioId() {
		return paUsuarioId;
	}

	public void setPaUsuarioId(long paUsuarioId) {
		this.paUsuarioId = paUsuarioId;
	}

	public String getPaFechaCreacion() {
		return paFechaCreacion;
	}

	public void setPaFechaCreacion(String paFechaCreacion) {
		this.paFechaCreacion = paFechaCreacion;
	}

	public String getPaEstacionTrabajo() {
		return paEstacionTrabajo;
	}

	public void setPaEstacionTrabajo(String paEstacionTrabajo) {
		this.paEstacionTrabajo = paEstacionTrabajo;
	}

	public ReimpresionTarjetaBean[] getPaReimpresionesTar() {
		return paReimpresionesTar;
	}

	public void setPaReimpresionesTar(
			ReimpresionTarjetaBean[] paReimpresionesTar) {
		this.paReimpresionesTar = paReimpresionesTar;
	}

	public long getPaMovimientoId() {
		return paMovimientoId;
	}

	public void setPaMovimientoId(long paMovimientoId) {
		this.paMovimientoId = paMovimientoId;
	}

	@Override
	public String toString() {
		return "ReimpresionVoucherBean [cdgError=" + cdgError + ", descError="
				+ descError + ", paUsuarioId=" + paUsuarioId
				+ ", paFechaCreacion=" + paFechaCreacion
				+ ", paEstacionTrabajo=" + paEstacionTrabajo
				+ ", paMovimientoId=" + paMovimientoId
				+ ", paReimpresionesTar=" + Arrays.toString(paReimpresionesTar)
				+ "]";
	}

}
