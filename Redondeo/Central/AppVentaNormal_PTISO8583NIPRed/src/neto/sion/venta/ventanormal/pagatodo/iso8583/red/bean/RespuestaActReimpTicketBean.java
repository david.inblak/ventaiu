package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

public class RespuestaActReimpTicketBean {
	private int paCdgError;
	private String paDescError;
	public int getPaCdgError() {
		return paCdgError;
	}
	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}
	public String getPaDescError() {
		return paDescError;
	}
	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}
	@Override
	public String toString() {
		return "RespuestaActReimpTicketBean [paCdgError=" + paCdgError
				+ ", paDescError=" + paDescError + "]";
	}
	
}
