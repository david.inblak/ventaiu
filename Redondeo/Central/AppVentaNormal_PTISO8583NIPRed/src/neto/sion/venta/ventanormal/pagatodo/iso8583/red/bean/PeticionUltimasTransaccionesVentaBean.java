package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

public class PeticionUltimasTransaccionesVentaBean {

	private int paPaisId;
	private long paTiendaId;

	public int getPaPaisId() {
		return paPaisId;
	}

	public void setPaPaisId(int paPaisId) {
		this.paPaisId = paPaisId;
	}

	public long getPaTiendaId() {
		return paTiendaId;
	}

	public void setPaTiendaId(long paTiendaId) {
		this.paTiendaId = paTiendaId;
	}

	@Override
	public String toString() {
		return "PeticionUltimasTransaccionesVentaBean [paPaisId=" + paPaisId
				+ ", paTiendaId=" + paTiendaId + "]";
	}

}
