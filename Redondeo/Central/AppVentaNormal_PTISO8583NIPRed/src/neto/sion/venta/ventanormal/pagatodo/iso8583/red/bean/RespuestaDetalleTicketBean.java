package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import java.util.Arrays;

public class RespuestaDetalleTicketBean {

	private long paMoviminetoId;
	private long fiUsuarioId;
	private String paNombreUsuario;
	private String paFechaCreacion;
	private String paEstacionTrabajo;
	private ArticuloTAReimpresionBean[] articuloTAReimpresionBeans;
	private ArticuloFacturacionBean[] articuloFacturacionBeans;
	private ServicioReimpresionBean[] serviciosReimpresionBeans;
	private TipoPagoReimpresionBean[] tipoPagoReimpresionBeans;
	private int paCdgError;
	private String paDescError;
	private TarjetaReimpresionBean[] tarjetaReimpresionBeans;
	private int paPermiteTicket;
	private int paTipoMovimiento;
	private int paTipoCajaId;

	public long getPaMoviminetoId() {
		return paMoviminetoId;
	}

	public void setPaMoviminetoId(long paMoviminetoId) {
		this.paMoviminetoId = paMoviminetoId;
	}

	public long getFiUsuarioId() {
		return fiUsuarioId;
	}

	public void setFiUsuarioId(long fiUsuarioId) {
		this.fiUsuarioId = fiUsuarioId;
	}

	public String getPaNombreUsuario() {
		return paNombreUsuario;
	}

	public void setPaNombreUsuario(String paNombreUsuario) {
		this.paNombreUsuario = paNombreUsuario;
	}

	public String getPaFechaCreacion() {
		return paFechaCreacion;
	}

	public void setPaFechaCreacion(String paFechaCreacion) {
		this.paFechaCreacion = paFechaCreacion;
	}

	public String getPaEstacionTrabajo() {
		return paEstacionTrabajo;
	}

	public void setPaEstacionTrabajo(String paEstacionTrabajo) {
		this.paEstacionTrabajo = paEstacionTrabajo;
	}

	public ArticuloTAReimpresionBean[] getArticuloTAReimpresionBeans() {
		return articuloTAReimpresionBeans;
	}

	public void setArticuloTAReimpresionBeans(
			ArticuloTAReimpresionBean[] articuloTAReimpresionBeans) {
		this.articuloTAReimpresionBeans = articuloTAReimpresionBeans;
	}

	public ArticuloFacturacionBean[] getArticuloFacturacionBeans() {
		return articuloFacturacionBeans;
	}

	public void setArticuloFacturacionBeans(
			ArticuloFacturacionBean[] articuloFacturacionBeans) {
		this.articuloFacturacionBeans = articuloFacturacionBeans;
	}

	public ServicioReimpresionBean[] getServiciosReimpresionBeans() {
		return serviciosReimpresionBeans;
	}

	public void setServiciosReimpresionBeans(
			ServicioReimpresionBean[] serviciosReimpresionBeans) {
		this.serviciosReimpresionBeans = serviciosReimpresionBeans;
	}

	public TipoPagoReimpresionBean[] getTipoPagoReimpresionBeans() {
		return tipoPagoReimpresionBeans;
	}

	public void setTipoPagoReimpresionBeans(
			TipoPagoReimpresionBean[] tipoPagoReimpresionBeans) {
		this.tipoPagoReimpresionBeans = tipoPagoReimpresionBeans;
	}

	public int getPaCdgError() {
		return paCdgError;
	}

	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}

	public String getPaDescError() {
		return paDescError;
	}

	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}

	public TarjetaReimpresionBean[] getTarjetaReimpresionBeans() {
		return tarjetaReimpresionBeans;
	}

	public void setTarjetaReimpresionBeans(
			TarjetaReimpresionBean[] tarjetaReimpresionBeans) {
		this.tarjetaReimpresionBeans = tarjetaReimpresionBeans;
	}

	public int getPaPermiteTicket() {
		return paPermiteTicket;
	}

	public void setPaPermiteTicket(int paPermiteTicket) {
		this.paPermiteTicket = paPermiteTicket;
	}

	public int getPaTipoMovimiento() {
		return paTipoMovimiento;
	}

	public void setPaTipoMovimiento(int paTipoMovimiento) {
		this.paTipoMovimiento = paTipoMovimiento;
	}

	public int getPaTipoCajaId() {
		return paTipoCajaId;
	}

	public void setPaTipoCajaId(int paTipoCajaId) {
		this.paTipoCajaId = paTipoCajaId;
	}

	@Override
	public String toString() {
		return "RespuestaDetalleTicketBean [paMoviminetoId=" + paMoviminetoId
				+ ", fiUsuarioId=" + fiUsuarioId + ", paNombreUsuario="
				+ paNombreUsuario + ", paFechaCreacion=" + paFechaCreacion
				+ ", paEstacionTrabajo=" + paEstacionTrabajo
				+ ", articuloTAReimpresionBeans="
				+ Arrays.toString(articuloTAReimpresionBeans)
				+ ", articuloFacturacionBeans="
				+ Arrays.toString(articuloFacturacionBeans)
				+ ", serviciosReimpresionBeans="
				+ Arrays.toString(serviciosReimpresionBeans)
				+ ", tipoPagoReimpresionBeans="
				+ Arrays.toString(tipoPagoReimpresionBeans) + ", paCdgError="
				+ paCdgError + ", paDescError=" + paDescError
				+ ", tarjetaReimpresionBeans="
				+ Arrays.toString(tarjetaReimpresionBeans)
				+ ", paPermiteTicket=" + paPermiteTicket
				+ ", paTipoMovimiento=" + paTipoMovimiento + ", paTipoCajaId="
				+ paTipoCajaId + "]";
	}

}
