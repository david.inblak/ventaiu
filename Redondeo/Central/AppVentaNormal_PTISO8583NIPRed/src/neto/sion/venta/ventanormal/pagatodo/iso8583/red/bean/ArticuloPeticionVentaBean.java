package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

public class ArticuloPeticionVentaBean {

	private long fiArticuloId;
	private String fcCdGbBarras;
	private double fnCantidad;
	private double fnPrecio;
	private double fnCosto;
	private double fnDescuento;
	private double fnIva;
	private String fcNombreArticulo;
	private double fiAgranel;
	private int fiIepsId;
	private int fiTipoDescuento;

	public double getFiAgranel() {
		return fiAgranel;
	}

	public void setFiAgranel(double fiAgranel) {
		this.fiAgranel = fiAgranel;
	}

	public String getFcNombreArticulo() {
		return fcNombreArticulo;
	}

	public void setFcNombreArticulo(String fcNombreArticulo) {
		this.fcNombreArticulo = fcNombreArticulo;
	}

	public long getFiArticuloId() {
		return fiArticuloId;
	}

	public void setFiArticuloId(long fiArticuloId) {
		this.fiArticuloId = fiArticuloId;
	}

	public String getFcCdGbBarras() {
		return fcCdGbBarras;
	}

	public void setFcCdGbBarras(String fcCdGbBarras) {
		this.fcCdGbBarras = fcCdGbBarras;
	}

	public double getFnCantidad() {
		return fnCantidad;
	}

	public void setFnCantidad(double fnCantidad) {
		this.fnCantidad = fnCantidad;
	}

	public double getFnPrecio() {
		return fnPrecio;
	}

	public void setFnPrecio(double fnPrecio) {
		this.fnPrecio = fnPrecio;
	}

	public double getFnCosto() {
		return fnCosto;
	}

	public void setFnCosto(double fnCosto) {
		this.fnCosto = fnCosto;
	}

	public double getFnDescuento() {
		return fnDescuento;
	}

	public void setFnDescuento(double fnDescuento) {
		this.fnDescuento = fnDescuento;
	}

	public double getFnIva() {
		return fnIva;
	}

	public void setFnIva(double fnIva) {
		this.fnIva = fnIva;
	}

	public int getFiIepsId() {
		return fiIepsId;
	}

	public void setFiIepsId(int fiIepsId) {
		this.fiIepsId = fiIepsId;
	}

	public int getFiTipoDescuento() {
		return fiTipoDescuento;
	}

	public void setFiTipoDescuento(int fiTipoDescuento) {
		this.fiTipoDescuento = fiTipoDescuento;
	}

	@Override
	public String toString() {
		return "ArticuloPeticionVentaBean [fiArticuloId=" + fiArticuloId
				+ ", fcCdGbBarras=" + fcCdGbBarras + ", fnCantidad="
				+ fnCantidad + ", fnPrecio=" + fnPrecio + ", fnCosto="
				+ fnCosto + ", fnDescuento=" + fnDescuento + ", fnIva=" + fnIva
				+ ", fcNombreArticulo=" + fcNombreArticulo + ", fiAgranel="
				+ fiAgranel + ", fiIepsId=" + fiIepsId + ", fiTipoDescuento="
				+ fiTipoDescuento + "]";
	}

	
	
}
