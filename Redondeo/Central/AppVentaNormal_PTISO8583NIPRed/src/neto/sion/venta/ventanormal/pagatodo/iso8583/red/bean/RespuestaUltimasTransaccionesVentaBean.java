package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import java.util.Arrays;

public class RespuestaUltimasTransaccionesVentaBean {

	private UltimasTransaccionesVentaBean[] transaccionesVentaBeans;

	public UltimasTransaccionesVentaBean[] getTransaccionesVentaBeans() {
		return transaccionesVentaBeans;
	}

	public void setTransaccionesVentaBeans(
			UltimasTransaccionesVentaBean[] transaccionesVentaBeans) {
		this.transaccionesVentaBeans = transaccionesVentaBeans;
	}

	@Override
	public String toString() {
		return "RespuestaUltimasTransaccionesVentaBean [transaccionesVentaBeans="
				+ Arrays.toString(transaccionesVentaBeans) + "]";
	}

}
