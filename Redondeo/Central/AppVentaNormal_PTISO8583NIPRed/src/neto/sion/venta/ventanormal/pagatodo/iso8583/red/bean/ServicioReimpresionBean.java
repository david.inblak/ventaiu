package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

public class ServicioReimpresionBean {

	private double monto;
	private double impuesto;
	private double comision;
	private long numAutorizacion;
	private String referencia;
	private String mensaje;
	private String nomArticulo;
	
	public ServicioReimpresionBean() {
		super();
	}
	
	public ServicioReimpresionBean(double monto, double impuesto,
			double comision, long numAutorizacion, String referencia,
			String mensaje, String nomArticulo) {
		super();
		this.monto = monto;
		this.impuesto = impuesto;
		this.comision = comision;
		this.numAutorizacion = numAutorizacion;
		this.referencia = referencia;
		this.mensaje = mensaje;
		this.nomArticulo = nomArticulo;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public double getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(double impuesto) {
		this.impuesto = impuesto;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public long getNumAutorizacion() {
		return numAutorizacion;
	}

	public void setNumAutorizacion(long numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getNomArticulo() {
		return nomArticulo;
	}

	public void setNomArticulo(String nomArticulo) {
		this.nomArticulo = nomArticulo;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	@Override
	public String toString() {
		return "ServicioReimpresionBean [monto=" + monto + ", impuesto="
				+ impuesto + ", comision=" + comision + ", numAutorizacion="
				+ numAutorizacion + ", referencia=" + referencia + ", mensaje="
				+ mensaje + ", nomArticulo=" + nomArticulo + "]";
	}
}
