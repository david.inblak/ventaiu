package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

public class PeticionTarjetaBean {
	
	private int fiPaisId;
	private long paTiendaId;
	private String paNumTarjeta;
	private String paFecha;
	
	public int getFiPaisId() {
		return fiPaisId;
	}
	public void setFiPaisId(int fiPaisId) {
		this.fiPaisId = fiPaisId;
	}
	public long getPaTiendaId() {
		return paTiendaId;
	}
	public void setPaTiendaId(long paTiendaId) {
		this.paTiendaId = paTiendaId;
	}
	public String getPaNumTarjeta() {
		return paNumTarjeta;
	}
	public void setPaNumTarjeta(String paNumTarjeta) {
		this.paNumTarjeta = paNumTarjeta;
	}
	public String getPaFecha() {
		return paFecha;
	}
	public void setPaFecha(String paFecha) {
		this.paFecha = paFecha;
	}
	
	@Override
	public String toString() {
		return "PeticionTarjetaBean [fiPaisId=" + fiPaisId + ", paTiendaId="
				+ paTiendaId + ", paNumTarjeta=" + paNumTarjeta + ", paFecha="
				+ paFecha + "]";
	}
	
	
	
	

}
