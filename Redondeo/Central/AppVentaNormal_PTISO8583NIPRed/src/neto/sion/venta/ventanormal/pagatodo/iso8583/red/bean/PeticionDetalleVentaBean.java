package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

/***
 * Clase que representa la peticion del detalle de una venta.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class PeticionDetalleVentaBean extends PeticionBaseVentaBean{

	private String paNumTransaccion;
	private int paParamTipoTrans;

	public int getPaParamTipoTrans() {
		return paParamTipoTrans;
	}

	public void setPaParamTipoTrans(int paParamTipoTrans) {
		this.paParamTipoTrans = paParamTipoTrans;
	}

	public String getPaNumTransaccion() {
		return paNumTransaccion;
	}

	public void setPaNumTransaccion(String paNumTransaccion) {
		this.paNumTransaccion = paNumTransaccion;
	}

	@Override
	public String toString() {
		return "PeticionDetalleVentaBean [paNumTransaccion=" + paNumTransaccion
				+ ", paParamTipoTrans=" + paParamTipoTrans + super.toString()
				+ "]";
	}

}
