package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

public class ArticuloReimpresionTicketBean {

	private double fnCantidad;
	private double fnPrecio;
	private double fnIva;
	private double fnDescuento;
	private int fiIepsId;
	private String fcNombreArticulo;

	public double getFnCantidad() {
		return fnCantidad;
	}

	public void setFnCantidad(double fnCantidad) {
		this.fnCantidad = fnCantidad;
	}

	public double getFnPrecio() {
		return fnPrecio;
	}

	public void setFnPrecio(double fnPrecio) {
		this.fnPrecio = fnPrecio;
	}

	public double getFnIva() {
		return fnIva;
	}

	public void setFnIva(double fnIva) {
		this.fnIva = fnIva;
	}

	public double getFnDescuento() {
		return fnDescuento;
	}

	public void setFnDescuento(double fnDescuento) {
		this.fnDescuento = fnDescuento;
	}

	public int getFiIepsId() {
		return fiIepsId;
	}

	public void setFiIepsId(int fiIepsId) {
		this.fiIepsId = fiIepsId;
	}

	public String getFcNombreArticulo() {
		return fcNombreArticulo;
	}

	public void setFcNombreArticulo(String fcNombreArticulo) {
		this.fcNombreArticulo = fcNombreArticulo;
	}

	@Override
	public String toString() {
		return "ArticuloReimpresionBean [fnCantidad=" + fnCantidad
				+ ", fnPrecio=" + fnPrecio + ", fnIva=" + fnIva
				+ ", fnDescuento=" + fnDescuento + ", fiIepsId=" + fiIepsId
				+ ", fcNombreArticulo=" + fcNombreArticulo + "]";
	}

}
