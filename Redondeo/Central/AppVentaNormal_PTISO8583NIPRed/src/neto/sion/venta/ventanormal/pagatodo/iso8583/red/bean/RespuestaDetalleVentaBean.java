package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import java.util.Arrays;

/***
 * Clase que representa la respuesta del detalle de una venta
 * 
 * @author Carlos Vicente Perez L.
 * 
 */
public class RespuestaDetalleVentaBean {
	private long paMovimientoId;
	private ArticuloBean[] paCurDetalleVenta;
	private int paCdgError;
	private String paDescError;
	private double paMontoDev;

	public double getPaMontoDev() {
		return paMontoDev;
	}

	public void setPaMontoDev(double paMontoDev) {
		this.paMontoDev = paMontoDev;
	}

	public long getPaMovimientoId() {
		return paMovimientoId;
	}

	public void setPaMovimientoId(long paMovimientoId) {
		this.paMovimientoId = paMovimientoId;
	}

	public ArticuloBean[] getPaCurDetalleVenta() {
		return paCurDetalleVenta;
	}

	public void setPaCurDetalleVenta(ArticuloBean[] paCurDetalleVenta) {
		this.paCurDetalleVenta = paCurDetalleVenta;
	}

	public int getPaCdgError() {
		return paCdgError;
	}

	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}

	public String getPaDescError() {
		return paDescError;
	}

	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}

	@Override
	public String toString() {
		return "RespuestaDetalleVentaBean [paMovimientoId=" + paMovimientoId
				+ ", paCurDetalleVenta=" + Arrays.toString(paCurDetalleVenta)
				+ ", paCdgError=" + paCdgError + ", paDescError=" + paDescError
				+ ", paMontoDev=" + paMontoDev + "]";
	}

}
