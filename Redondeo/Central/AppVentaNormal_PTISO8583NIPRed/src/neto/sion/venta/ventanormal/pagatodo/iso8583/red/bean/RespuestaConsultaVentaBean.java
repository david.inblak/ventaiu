package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import java.util.Arrays;

public class RespuestaConsultaVentaBean {

	private long paMovimientoId;
	private ArticuloPeticionVentaBean[] paCurArticulosVenta;
	private int paCdgError;
	private String paDescError;
	private double paMontoDev;

	public long getPaMovimientoId() {
		return paMovimientoId;
	}

	public void setPaMovimientoId(long paMovimientoId) {
		this.paMovimientoId = paMovimientoId;
	}

	public ArticuloPeticionVentaBean[] getPaCurArticulosVenta() {
		return paCurArticulosVenta;
	}

	public void setPaCurArticulosVenta(
			ArticuloPeticionVentaBean[] paCurArticulosVenta) {
		this.paCurArticulosVenta = paCurArticulosVenta;
	}

	public int getPaCdgError() {
		return paCdgError;
	}

	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}

	public String getPaDescError() {
		return paDescError;
	}

	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}

	public double getPaMontoDev() {
		return paMontoDev;
	}

	public void setPaMontoDev(double paMontoDev) {
		this.paMontoDev = paMontoDev;
	}

	@Override
	public String toString() {
		return "RespuestaConsultaVentaBean [paMovimientoId=" + paMovimientoId
				+ ", paCurArticulosVenta="
				+ Arrays.toString(paCurArticulosVenta) + ", paCdgError="
				+ paCdgError + ", paDescError=" + paDescError + ", paMontoDev="
				+ paMontoDev + "]";
	}

}
