package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

public class RespuestaAutorizacionTABean {
	private long transaccionTA;
	private int cdgError;
	private String descError;

	public long getTransaccionTA() {
		return transaccionTA;
	}

	public void setTransaccionTA(long transaccionTA) {
		this.transaccionTA = transaccionTA;
	}

	public int getCdgError() {
		return cdgError;
	}

	public void setCdgError(int cdgError) {
		this.cdgError = cdgError;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	@Override
	public String toString() {
		return "RespuestaAutorizacionTABean [transaccionTA=" + transaccionTA
				+ ", cdgError=" + cdgError + ", descError=" + descError + "]";
	}

}
