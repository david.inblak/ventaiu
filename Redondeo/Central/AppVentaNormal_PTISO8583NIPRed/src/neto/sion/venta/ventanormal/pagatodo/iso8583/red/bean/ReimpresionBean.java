package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

/***
 * Clase que representa el objeto VELIT.TYPOBJREIMPRESIONES de oracle.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class ReimpresionBean {
	private long fiPagoTarjeta;

	public long getFiPagoTarjeta() {
		return fiPagoTarjeta;
	}

	public void setFiPagoTarjeta(long fiPagoTarjeta) {
		this.fiPagoTarjeta = fiPagoTarjeta;
	}

	@Override
	public String toString() {
		return "ReimpresionesBean [fiPagoTarjeta=" + fiPagoTarjeta + "]";
	}

}
