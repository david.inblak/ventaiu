package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

public class PeticionConfirmaVentaArticulosBean extends PeticionBaseVentaBean {
	private long numTransaccion;
	private long usuarioId;
	
	
	public PeticionConfirmaVentaArticulosBean(
			PeticionVentaArticulosBean _peticionVentaArticulos,
			long transaccionId) {
		this.setIpTerminal(_peticionVentaArticulos.getIpTerminal() );
		this.setPaConciliacionId(_peticionVentaArticulos.getPaConciliacionId() );
		this.setPaPaisId(_peticionVentaArticulos.getPaPaisId() );
		this.setTiendaId(_peticionVentaArticulos.getTiendaId() );
		this.setNumTransaccion(transaccionId);
		this.setUsuarioId(_peticionVentaArticulos.getPaUsuarioId());
	}
	
	public long getNumTransaccion() {
		return numTransaccion;
	}
	public void setNumTransaccion(long numTransaccion) {
		this.numTransaccion = numTransaccion;
	}
	public long getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(long usuarioId) {
		this.usuarioId = usuarioId;
	}
	@Override
	public String toString() {
		return "PeticionConfirmaVentaNormalBean [numTransaccion="
				+ numTransaccion + ", usuarioId=" + usuarioId
				+ ", getPaPaisId()=" + getPaPaisId()
				+ ", getPaConciliacionId()=" + getPaConciliacionId()
				+ ", toString()=" + super.toString() + ", getuId()=" + getuId()
				+ ", getIpTerminal()=" + getIpTerminal() + ", getTiendaId()="
				+ getTiendaId() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}
	
	
}
