package neto.sion.venta.ventanormal.pagatodo.iso8583.red.servicio;

import neto.sion.venta.ventageneral.iso8583Red.bean.WSExcepcion;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionConfirmaVentaArticulosBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionConfirmaVentaNormalBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionTransaccionCentralBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaArticulosBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaNormalBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.ctrl.VentaNormalControlador;

/***
 * Clase Fachada de venta normal.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class VentaNormal {
	private VentaNormalControlador controlador;

	public VentaNormal(String idLog) {
		controlador = new VentaNormalControlador(idLog);
	}


	/***
	 * Metodo encargado de consultar el numero de transaccion de la base central
	 * 
	 * @param _peticion
	 * @return Un long 0 => No se encontro el registro, otro =>Numero de
	 *         transaccion.
	 * @throws AxisFault
	 */
	public long consultarTransaccionCentral(
			PeticionTransaccionCentralBean _peticion) throws WSExcepcion {
		return controlador.consultarTransaccionCentral(_peticion);
	}

	
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase
	 * VentaNormalControlador. NO GRABA IEPS
	 * 
	 * @param _peticion
	 * @deprecated NO UTILIZAR, NO GRABA IEPS
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarVenta(PeticionVentaNormalBean _peticion)
			throws WSExcepcion {
		return controlador.registrarVenta(_peticion );
	}
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase
	 * VentaNormalControlador.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarVenta(PeticionVentaArticulosBean _peticion, Long idPagoTarjetaPT)
			throws WSExcepcion {
		return controlador.registrarVenta(_peticion, idPagoTarjetaPT );
	}
	
	public RespuestaVentaBean procesaTiposPago(PeticionVentaNormalBean _peticion, RespuestaVentaBean respuesta)
			throws WSExcepcion {
		return controlador.procesaTiposPago( _peticion, respuesta );
	}
    
	/**
	 * 
	 * @param _peticion
	 * @return
	 * @throws WSExcepcion
	 * @deprecated NO UTILIZAR, NOGRABA IEPS
	 */
	public RespuestaVentaBean registrarCancelacionVenta(PeticionVentaNormalBean _peticion)
			throws WSExcepcion {
		return controlador.registrarCancelacionVenta(_peticion );
	}
	
	public RespuestaVentaBean registrarCancelacionVenta(PeticionVentaArticulosBean _peticion, int _tipoDevolucion, long paTransaccionVentaId)
			throws WSExcepcion {
		return controlador.registrarCancelacionVenta(_peticion,_tipoDevolucion, paTransaccionVentaId);
	}
	
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase
	 * VentaNormalControlador.
	 * 
	 * @param _peticion
	 * @deprecated NO UTILIZAR, NO GRABA IEPS
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarConfirmacionVenta(PeticionConfirmaVentaNormalBean _peticion)
			throws WSExcepcion {
		return controlador.registrarConfirmacionVenta(_peticion);
	}
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase
	 * VentaNormalControlador.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarConfirmacionVenta(PeticionConfirmaVentaArticulosBean _peticion)
			throws WSExcepcion {
		return controlador.registrarConfirmacionVenta(_peticion);
	}
	
	
	
}
