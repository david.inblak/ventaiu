package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

public class ArticuloDevReimpresionBean {
	private double precio;
	private double cantidad;
	private double iva;
	private int iepsId;
	private String nombre;

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public int getIepsId() {
		return iepsId;
	}

	public void setIepsId(int iepsId) {
		this.iepsId = iepsId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "ArticuloDevReimpresionBean [precio=" + precio + ", cantidad="
				+ cantidad + ", iva=" + iva + ", iepsId=" + iepsId
				+ ", nombre=" + nombre + "]";
	}

}
