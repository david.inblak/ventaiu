package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import java.util.Arrays;

/****
 * Clase que representa la respuesta de una venta mixta.
 * @author Carlos V. Perez L.
 *
 */
public class RespuestaVentaMixtaBean {
	private long paTransaccionId;
	private int paCdgError;
	private String paDescError;
	private BloqueoBean[] paTypCursorBlqs;
	private RespuestaRecargaTABean[] respuestaRecargasTA;

	public int getPaCdgError() {
		return paCdgError;
	}
	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}
	public String getPaDescError() {
		return paDescError;
	}
	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}
	public RespuestaRecargaTABean[] getRespuestaRecargasTA() {
		return respuestaRecargasTA;
	}
	public void setRespuestaRecargasTA(RespuestaRecargaTABean[] respuestaRecargasTA) {
		this.respuestaRecargasTA = respuestaRecargasTA;
	}
	public long getPaTransaccionId() {
		return paTransaccionId;
	}
	public void setPaTransaccionId(long paTransaccionId) {
		this.paTransaccionId = paTransaccionId;
	}
	public BloqueoBean[] getPaTypCursorBlqs() {
		return paTypCursorBlqs;
	}
	public void setPaTypCursorBlqs(BloqueoBean[] paTypCursorBlqs) {
		this.paTypCursorBlqs = paTypCursorBlqs;
	}
	@Override
	public String toString() {
		return "RespuestaVentaMixtaBean [paTransaccionId=" + paTransaccionId
				+ ", paCdgError=" + paCdgError + ", paDescError=" + paDescError
				+ ", paTypCursorBlqs=" + Arrays.toString(paTypCursorBlqs)
				+ ", respuestaRecargasTA="
				+ Arrays.toString(respuestaRecargasTA) + "]";
	}

}
