package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import java.util.Arrays;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

/***
 * Clase que representa la peticion de una venta.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class PeticionVentaBean extends PeticionBaseVentaBean {
	private String paTerminal;
	private long paUsuarioId;
	private long paUsuarioAutorizaId;
	private String paFechaOper;
	private int paTipoMovto;
	private double paMontoTotalVta;
	private ArticuloBean[] articulos;
	private TipoPagoBean[] tiposPago;

	private int fueraLinea;

	public String getPaTerminal() {
		return paTerminal;
	}

	public void setPaTerminal(String paTerminal) {
		this.paTerminal = paTerminal;
	}

	public long getPaUsuarioId() {
		return paUsuarioId;
	}

	public void setPaUsuarioId(long paUsuarioId) {
		this.paUsuarioId = paUsuarioId;
	}

	public String getPaFechaOper() {
		return paFechaOper;
	}

	public void setPaFechaOper(String paFechaOper) {
		this.paFechaOper = paFechaOper;
	}

	public int getPaTipoMovto() {
		return paTipoMovto;
	}

	public void setPaTipoMovto(int paTipoMovto) {
		this.paTipoMovto = paTipoMovto;
	}

	public double getPaMontoTotalVta() {
		return paMontoTotalVta;
	}

	public void setPaMontoTotalVta(double paMontoTotalVta) {
		this.paMontoTotalVta = paMontoTotalVta;
	}

	public ArticuloBean[] getArticulos() {
		return articulos;
	}

	public void setArticulos(ArticuloBean[] articulos) {
		this.articulos = articulos;
	}

	public TipoPagoBean[] getTiposPago() {
		return tiposPago;
	}

	public void setTiposPago(TipoPagoBean[] tiposPago) {
		this.tiposPago = tiposPago;
	}

	public int getFueraLinea() {
		return fueraLinea;
	}

	public void setFueraLinea(int fueraLinea) {
		this.fueraLinea = fueraLinea;
	}

	public long getPaUsuarioAutorizaId() {
		return paUsuarioAutorizaId;
	}

	public void setPaUsuarioAutorizaId(long paUsuarioAutorizaId) {
		this.paUsuarioAutorizaId = paUsuarioAutorizaId;
	}

	@Override
	public String toString() {
		return "PeticionVentaBean [paTerminal=" + paTerminal + ", paUsuarioId="
				+ paUsuarioId + ", paUsuarioAutorizaId=" + paUsuarioAutorizaId
				+ ", paFechaOper=" + paFechaOper + ", paTipoMovto="
				+ paTipoMovto + ", paMontoTotalVta=" + paMontoTotalVta
				+ ", articulos=" + Arrays.toString(articulos) + ", tiposPago="
				+ Arrays.toString(tiposPago) + ", fueraLinea=" + fueraLinea
				+ "]";
	}

}
