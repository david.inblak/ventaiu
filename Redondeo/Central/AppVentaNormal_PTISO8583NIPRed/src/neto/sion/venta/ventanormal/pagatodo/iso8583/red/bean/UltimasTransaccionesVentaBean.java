package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

public class UltimasTransaccionesVentaBean {

	private int fiTipoMovtoCajaId;
	private long fiNumTransaccion;
	private int fiSubModuloId;
	private String fdFechaMovimiento;

	public int getFiTipoMovtoCajaId() {
		return fiTipoMovtoCajaId;
	}

	public void setFiTipoMovtoCajaId(int fiTipoMovtoCajaId) {
		this.fiTipoMovtoCajaId = fiTipoMovtoCajaId;
	}

	public long getFiNumTransaccion() {
		return fiNumTransaccion;
	}

	public void setFiNumTransaccion(long fiNumTransaccion) {
		this.fiNumTransaccion = fiNumTransaccion;
	}

	public int getFiSubModuloId() {
		return fiSubModuloId;
	}

	public void setFiSubModuloId(int fiSubModuloId) {
		this.fiSubModuloId = fiSubModuloId;
	}

	public String getFdFechaMovimiento() {
		return fdFechaMovimiento;
	}

	public void setFdFechaMovimiento(String fdFechaMovimiento) {
		this.fdFechaMovimiento = fdFechaMovimiento;
	}

	@Override
	public String toString() {
		return "UltimasTransaccionesVentaBean [fiTipoMovtoCajaId="
				+ fiTipoMovtoCajaId + ", fiNumTransaccion=" + fiNumTransaccion
				+ ", fiSubModuloId=" + fiSubModuloId + ", fdFechaMovimiento="
				+ fdFechaMovimiento + "]";
	}

}
