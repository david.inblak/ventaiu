package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import java.util.Arrays;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

/***
 * Clase que representa la peticion de los datos para la reimpresion de un
 * ticket.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class PeticionReimpresionBean extends PeticionBaseVentaBean {

	private String paNumTransaccion;
	private int paParamTipoTrans;
	private long paMovimientoId;
	private ReimpresionBean[] paArrReimpresiones;

	public String getPaNumTransaccion() {
		return paNumTransaccion;
	}

	public void setPaNumTransaccion(String paNumTransaccion) {
		this.paNumTransaccion = paNumTransaccion;
	}

	public int getPaParamTipoTrans() {
		return paParamTipoTrans;
	}

	public void setPaParamTipoTrans(int paParamTipoTrans) {
		this.paParamTipoTrans = paParamTipoTrans;
	}

	public ReimpresionBean[] getPaArrReimpresiones() {
		return paArrReimpresiones;
	}

	public void setPaArrReimpresiones(ReimpresionBean[] paArrReimpresiones) {
		this.paArrReimpresiones = paArrReimpresiones;
	}

	public long getPaMovimientoId() {
		return paMovimientoId;
	}

	public void setPaMovimientoId(long paMovimientoId) {
		this.paMovimientoId = paMovimientoId;
	}

	@Override
	public String toString() {
		return "PeticionReimpresionBean [paNumTransaccion=" + paNumTransaccion
				+ ", paParamTipoTrans=" + paParamTipoTrans
				+ ", paMovimientoId=" + paMovimientoId
				+ ", paArrReimpresiones=" + Arrays.toString(paArrReimpresiones)
				+ "]";
	}

}
