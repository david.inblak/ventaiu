package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

/***
 * Clase que representa la peticion de los datos de bloqueo de venta
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class PeticionBloqueoBean extends PeticionBaseVentaBean {

	private long paUsuarioId;

	public long getPaUsuarioId() {
		return paUsuarioId;
	}

	public void setPaUsuarioId(long paUsuarioId) {
		this.paUsuarioId = paUsuarioId;
	}

	@Override
	public String toString() {
		return "PeticionBloqueoBean [paUsuarioId=" + paUsuarioId
				+ super.toString() + "]";
	}

}
