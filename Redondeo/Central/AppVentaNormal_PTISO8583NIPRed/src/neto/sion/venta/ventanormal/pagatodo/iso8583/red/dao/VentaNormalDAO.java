package neto.sion.venta.ventanormal.pagatodo.iso8583.red.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import neto.sion.Logs;
import neto.sion.genericos.sql.Conexion;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.ArticuloBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.ArticuloDevReimpresionBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.ArticuloFacturacionBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.ArticuloPeticionVentaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.ArticuloReimpresionTicketBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.ArticuloTAReimpresionBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.BloqueoBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionActReimpTicketBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionBloqueoBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionConfirmaVentaArticulosBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionConfirmaVentaNormalBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionDetalleVentaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionDevolucionArticulosBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionDevolucionBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionMarcadoPagoTarjetaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionReimpresionBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionTarjetaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionTicketReimpresionBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionTransaccionCentralBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionUltimasTransaccionesVentaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaArticulosBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaNormalBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaTABean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.ReimpresionBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaActReimpTicketBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaActReimpVoucherBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaAutorizacionTABean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaConsultaVentaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaDetalleTicketBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaDetalleVentaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaMarcadoPagoTarjetaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaReimpresionBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaTarjetaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaTicketReimpresionBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaUltimasTransaccionesVentaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.ServicioReimpresionBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.TarjetaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.TarjetaReimpresionBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.TipoPagoBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.TipoPagoReimpresionBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.UltimasTransaccionesVentaBean;
import neto.sion.venta.ventageneral.iso8583Red.bean.WSExcepcion;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

import Com.Elektra.Definition.Config.ConfigSION;

/**
 * Clase dao de venta normal.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class VentaNormalDAO extends Conexion {
	private static int TARJETA_ID;
	private Logs logeo;

	public VentaNormalDAO(String idLog) {
		super(ConfigSION.obtenerParametro("dataSource"));
		TARJETA_ID = Integer.valueOf(ConfigSION
				.obtenerParametro("venta.tarjeta.id"));
		logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
		logeo.setIdLog(idLog);
	}
	
	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar la
	 * venta de T.A.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta de T.A.
	 * @throws WSExcepcion
	 */
	public RespuestaVentaBean registrarVentaTA(PeticionVentaTABean _peticion)
			throws WSExcepcion {
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		Connection conn = null;
		
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		logeo.log("Peticion de registro de venta:" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPSERVICIOVENTA");
			conn = obtenerConexion();
			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

			Object[] articulos = new Object[_peticion.getArticulos().length];
			StructDescriptor estructuraArticulo = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOARTICULOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOARTICULOSORACLE"),
							oconn);
			int contador = 0;
			for (ArticuloBean articulo : _peticion.getArticulos()) {
				
				Object[] objeto = new Object[] { articulo.getFiArticuloId(),
						articulo.getFcCdGbBarras(), articulo.getFnCantidad(),
						articulo.getFnPrecio(), articulo.getFnCosto(),
						articulo.getFnDescuento(), articulo.getFnIva(),
						0 };
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,
						objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,
					articulos);

			ArrayList<TipoPagoBean> tiposPagoLista = new ArrayList<TipoPagoBean>();
			StructDescriptor estructuraTiposPago = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"),
							oconn);
			TipoPagoBean tarjetas = new TipoPagoBean();
			for (TipoPagoBean tipoPago : _peticion.getTiposPago()) {
				
				if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
					PeticionMarcadoPagoTarjetaBean marcadoVenta = new PeticionMarcadoPagoTarjetaBean();
					marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION
							.obtenerParametro("venta.proceso.4.id")));
					marcadoVenta
							.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
									.obtenerParametro("venta.proceso.4.estatus.tarjeta.5")));
					marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
					marcadoVenta.setTiendaId(_peticion.getTiendaId());
					marcadoVenta.setPaPagoTarjetaIdBus(tipoPago
							.getPaPagoTarjetaIdBus());
					marcadoVenta.setPaComision(tipoPago.getImporteAdicional());
					RespuestaMarcadoPagoTarjetaBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
					if (respuestaMarcado.getPaCdgError() != 0) {
						logeo.log("Resultado del marcado de pago con tarjeta:"
								+ marcadoVenta);
					}
					tarjetas.setFiTipoPagoId(TARJETA_ID);
					tarjetas.setFnMontoPago(tarjetas.getFnMontoPago()
							+ tipoPago.getFnMontoPago());
					tarjetas.setImporteAdicional(tarjetas.getImporteAdicional()
							+ tipoPago.getImporteAdicional());
					tiposPagoLista.add(tarjetas);
				} else {
					tiposPagoLista.add(tipoPago);
				}
			}
			contador = 0;
			Object[] tiposPago = new Object[tiposPagoLista.size()];
			for (TipoPagoBean tipoPago : tiposPagoLista) {
				Object[] objeto = new Object[] { tipoPago.getFiTipoPagoId(),
						tipoPago.getFnMontoPago(), tipoPago.getFnNumeroVales(),
						tipoPago.getImporteAdicional() };
				tiposPago[contador] = new STRUCT(estructuraTiposPago, oconn,
						objeto);
				contador++;
			}
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,
					tiposPago);
			cstmt.setString(1, _peticion.getPaTerminal());
			cstmt.setLong(2, _peticion.getPaPaisId());
			cstmt.setLong(3, _peticion.getTiendaId());
			cstmt.setLong(4, _peticion.getPaUsuarioId());
			cstmt.setLong(5, _peticion.getPaUsuarioAutorizaId());
			cstmt.setString(6, _peticion.getPaFechaOper());
			cstmt.setLong(7, _peticion.getPaTipoMovto());
			cstmt.setDouble(8, _peticion.getPaMontoTotalVta());
			cstmt.setARRAY(9, arregloArticulos);
			cstmt.setARRAY(10, arregloTiposPago);
			cstmt.setInt(11, 0);
			cstmt.setLong(12, 0);
			cstmt.setLong(13, 0);
			cstmt.setLong(14, _peticion.getPaConciliacionId());
			cstmt.setInt(15, _peticion.getFueraLinea());
			cstmt.registerOutParameter(16, OracleTypes.NUMBER);
			cstmt.registerOutParameter(17, OracleTypes.CURSOR);
			cstmt.registerOutParameter(18, OracleTypes.NUMBER);
			cstmt.registerOutParameter(19, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(18)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(19)));
			respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(16)));
			if (respuesta.getPaTransaccionId() == 0
					&& respuesta.getPaCdgError() == 0) {
				respuesta.setPaCdgError(Integer.valueOf(ConfigSION
						.obtenerParametro("transaccion.cero.codigo")));
				respuesta.setPaDescError(ConfigSION
						.obtenerParametro("transaccion.cero.mensaje"));
			}
			if (respuesta.getPaCdgError() == 0) {
				for (TipoPagoBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
						PeticionMarcadoPagoTarjetaBean marcadoVenta = new PeticionMarcadoPagoTarjetaBean();
						marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION
								.obtenerParametro("venta.proceso.3.id")));
						marcadoVenta
								.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
										.obtenerParametro("venta.proceso.3.estatus.tarjeta.7")));
						marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
						marcadoVenta.setTiendaId(_peticion.getTiendaId());
						marcadoVenta.setPaPagoTarjetaIdBus(tipoPago
								.getPaPagoTarjetaIdBus());
						marcadoVenta.setPaNumTransaccion(respuesta
								.getPaTransaccionId());
						marcadoVenta
								.setPaTipoPagoId(tipoPago.getFiTipoPagoId());
						RespuestaMarcadoPagoTarjetaBean actualizacionMarcado = marcadoPagoTarjeta(marcadoVenta);
						if (actualizacionMarcado.getPaCdgError() != 0) {
							logeo.log("Resultado del marcado de pago con tarjeta:"
									+ marcadoVenta);
						}
					}
				}
				bloqueos = (ResultSet) cstmt.getCursor(17);
				ArrayList<BloqueoBean> bloqueosList = new ArrayList<BloqueoBean>();
				while (bloqueos.next()) {
					BloqueoBean bloqueoBean = new BloqueoBean();
					bloqueoBean.setFiTipoPagoId(bloqueos.getInt(1));
					bloqueoBean.setFiNumAvisos(bloqueos.getInt(2));
					bloqueoBean.setFiAvisosFalt(bloqueos.getInt(3));
					bloqueoBean.setFiEstatusBloqueoId(bloqueos.getInt(4));
					bloqueosList.add(bloqueoBean);
				}
				if (bloqueosList.size() != 0) {
					BloqueoBean[] bloqueosBean = new BloqueoBean[bloqueosList
							.size()];
					bloqueosList.toArray(bloqueosBean);
					respuesta.setPaTypCursorBlqs(bloqueosBean);
				} else {
					respuesta.setPaTypCursorBlqs(null);
				}

			} else {
				for (TipoPagoBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
						PeticionMarcadoPagoTarjetaBean marcadoVenta = new PeticionMarcadoPagoTarjetaBean();
						marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION
								.obtenerParametro("venta.proceso.4.id")));
						marcadoVenta
								.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
										.obtenerParametro("venta.proceso.4.estatus.tarjeta.6")));
						marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
						marcadoVenta.setTiendaId(_peticion.getTiendaId());
						marcadoVenta.setPaPagoTarjetaIdBus(tipoPago
								.getPaPagoTarjetaIdBus());
						RespuestaMarcadoPagoTarjetaBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
						if (respuestaMarcado.getPaCdgError() != 0) {
							logeo.log("Resultado del marcado de pago con tarjeta:"
									+ marcadoVenta);
						}
					}
				}
				respuesta.setPaTypCursorBlqs(null);
			}
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {

			Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
			Conexion.cerrarConexion(conn);

		}
		return respuesta;
	}

	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar
	 * una devolucion de venta de t.a.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la devolucion.
	 * @throws WSExcepcion
	 */
	public RespuestaVentaBean registrarDevolucionTA(
			PeticionVentaTABean _peticion) throws WSExcepcion {
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		logeo.log("Detalle de la peticion de devolucion:"
				+ _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPSERVICIOVENTA");
			conn = obtenerConexion();
			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			Object[] articulos = new Object[_peticion.getArticulos().length];
			StructDescriptor estructuraArticulo = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOARTICULOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOARTICULOSORACLE"),
							oconn);
			int contador = 0;
			for (ArticuloBean articulo : _peticion.getArticulos()) {
				
				Object[] objeto = new Object[] { articulo.getFiArticuloId(),
						articulo.getFcCdGbBarras(), articulo.getFnCantidad(),
						articulo.getFnPrecio(), articulo.getFnCosto(),
						articulo.getFnDescuento(), articulo.getFnIva(),
						0};
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,
						objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,
					articulos);
			Object[] tiposPago = new Object[1];
			StructDescriptor estructuraTiposPago = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"),
							oconn);

			Object[] objeto = new Object[] { 1, _peticion.getPaMontoTotalVta(),
					0, 0 };
			tiposPago[0] = new STRUCT(estructuraTiposPago, oconn, objeto);
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,
					tiposPago);

			cstmt.setString(1, _peticion.getPaTerminal());
			cstmt.setLong(2, _peticion.getPaPaisId());
			cstmt.setLong(3, _peticion.getTiendaId());
			cstmt.setLong(4, _peticion.getPaUsuarioId());
			cstmt.setLong(5, _peticion.getPaUsuarioId());
			cstmt.setString(6, _peticion.getPaFechaOper());
			cstmt.setLong(7, _peticion.getPaTipoMovto());
			cstmt.setDouble(8, _peticion.getPaMontoTotalVta());
			cstmt.setARRAY(9, arregloArticulos);
			cstmt.setARRAY(10, arregloTiposPago);
			cstmt.setInt(11, 0);
			cstmt.setLong(12, _peticion.getPaTransaccionDev());
			cstmt.setLong(13, 0);
			cstmt.setLong(14, _peticion.getPaConciliacionId());
			cstmt.setInt(15, _peticion.getFueraLinea());
			cstmt.registerOutParameter(16, OracleTypes.NUMBER);
			cstmt.registerOutParameter(17, OracleTypes.CURSOR);
			cstmt.registerOutParameter(18, OracleTypes.NUMBER);
			cstmt.registerOutParameter(19, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(18)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(19)));
			respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(16)));
			logeo.log("Respuesta de la peticion de devolucion:"
					+ respuesta.toString());
			if (respuesta.getPaTransaccionId() == 0
					&& respuesta.getPaCdgError() == 0) {
				respuesta.setPaCdgError(Integer.valueOf(ConfigSION
						.obtenerParametro("transaccion.cero.codigo")));
				respuesta.setPaDescError(ConfigSION
						.obtenerParametro("transaccion.cero.mensaje"));
			}
			if (respuesta.getPaCdgError() == 0) {
				bloqueos = (ResultSet) cstmt.getCursor(17);
				ArrayList<BloqueoBean> bloqueosList = new ArrayList<BloqueoBean>();
				while (bloqueos.next()) {
					BloqueoBean bloqueoBean = new BloqueoBean();
					bloqueoBean.setFiTipoPagoId(bloqueos.getInt(1));
					bloqueoBean.setFiNumAvisos(bloqueos.getInt(2));
					bloqueoBean.setFiAvisosFalt(bloqueos.getInt(3));
					bloqueoBean.setFiEstatusBloqueoId(bloqueos.getInt(4));
					bloqueosList.add(bloqueoBean);
				}
				if (bloqueosList.size() != 0) {
					BloqueoBean[] bloqueosBean = new BloqueoBean[bloqueosList
							.size()];
					bloqueosList.toArray(bloqueosBean);
					respuesta.setPaTypCursorBlqs(bloqueosBean);
				} else {
					respuesta.setPaTypCursorBlqs(null);
				}
			} else {
				respuesta.setPaTypCursorBlqs(null);
			}

		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
			Conexion.cerrarConexion(conn);
		}
		return respuesta;
	}

	
	/***
	 * Realiza una llamada al procedimiento almacenado encargado de almacenar y
	 * actualizar el marcado de pago con tarjeta.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del marcado del pago.
	 * @throws WSExcepcion
	 */
	public RespuestaMarcadoPagoTarjetaBean marcadoPagoTarjeta(
			PeticionMarcadoPagoTarjetaBean _peticion) throws WSExcepcion {
		RespuestaMarcadoPagoTarjetaBean respuesta = new RespuestaMarcadoPagoTarjetaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		logeo.log("Peticion de marcado pago tarjeta:" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPINSPAGOSTARJETA");
			conn = obtenerConexion();
			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setInt(1, _peticion.getPaProcesoExc());
			cstmt.setInt(2, _peticion.getPaPaisId());
			cstmt.setLong(3, _peticion.getTiendaId());
			cstmt.setString(4, "");
			cstmt.setDouble(5, 0.0);
			cstmt.setDouble(6, _peticion.getPaComision());
			cstmt.setLong(7, _peticion.getPaPagoTarjetaIdBus());
			cstmt.setInt(8, 0);
			cstmt.setInt(9, _peticion.getPaEstatusTarjetaId());
			cstmt.setString(10, "");
			cstmt.setInt(11, _peticion.getPaTipoPagoId());
			cstmt.setLong(12, _peticion.getPaNumTransaccion());
			cstmt.setInt(13, 0);
			cstmt.registerOutParameter(14, OracleTypes.NUMBER);
			cstmt.registerOutParameter(15, OracleTypes.NUMBER);
			cstmt.registerOutParameter(16, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaPagoTarjetasId(Long.valueOf(cstmt.getLong(14)));
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(15)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(16)));
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(oconn, cstmt, null);
			Conexion.cerrarConexion(conn);

		}

		return respuesta;
	}

	
	/**
	 * Metodo utilizado para la obtencion de los datos de bloqueo de venta
	 * 
	 * @param _cajeroId
	 * @return Un arreglo de objetos de BloqueoBean .
	 * @throws WSExcepcion
	 */
	public BloqueoBean[] consultaBloqueos(PeticionBloqueoBean _peticion)
			throws WSExcepcion {

		Connection conn = null;
		OracleConnection oconn = null;
		OracleCallableStatement cstmt = null;
		String sql = null;
		BloqueoBean[] respuesta = null;
		ResultSet bloqueos = null;
		logeo.log("peticion:" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("GENERICO.SPCARGABLOQUEOS");
			conn = obtenerConexion();
			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setLong(1, _peticion.getPaUsuarioId());
			cstmt.setLong(2, _peticion.getTiendaId());
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.registerOutParameter(4, OracleTypes.NUMERIC);
			cstmt.registerOutParameter(5, OracleTypes.VARCHAR);
			cstmt.execute();
			bloqueos = (ResultSet) cstmt.getCursor(3);
			ArrayList<BloqueoBean> bloqueosList = new ArrayList<BloqueoBean>();
			while (bloqueos.next()) {
				if (bloqueos.getInt(1) == 1) {
					BloqueoBean bloqueoBean = new BloqueoBean();
					bloqueoBean.setFiTipoPagoId(bloqueos.getInt(5));
					bloqueoBean.setFiNumAvisos(bloqueos.getInt(8));
					bloqueoBean.setFiEstatusBloqueoId(bloqueos.getInt(9));
					bloqueoBean.setFiAvisosFalt(bloqueos.getInt(15));
					logeo.log("Mensaje de consulta de bloqueo:"
							+ bloqueoBean.toString());
					bloqueosList.add(bloqueoBean);
				}
			}
			if (bloqueosList.size() != 0) {
				BloqueoBean[] bloqueosBean = new BloqueoBean[bloqueosList
						.size()];
				bloqueosList.toArray(bloqueosBean);
				respuesta = bloqueosBean;
			} else {
				respuesta = null;
			}
			
			if(respuesta!=null){
				logeo.log("Respuesta de bloqueos recibida: "+respuesta.toString());
			}else{
				logeo.log("Respuesta enviada a front nula");
			}

		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
			Conexion.cerrarConexion(conn);
		}
		
		
		return respuesta;
	}

	
	/***
	 * M�todo utilizada para actualizar los datos necesarios de la reimpresion
	 * de vouchers
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la actualizacion de los datos.
	 * @throws WSExcepcion
	 */
	public RespuestaActReimpVoucherBean actualizarReimpresionVoucher(
			PeticionReimpresionBean _peticion) throws WSExcepcion {
		RespuestaActReimpVoucherBean respuesta = new RespuestaActReimpVoucherBean();
		Connection conn = null;
		OracleConnection oconn = null;
		OracleCallableStatement cstmt = null;
		String sql = null;
		ResultSet ReimpresionesTar = null;
		ARRAY arregloReimpresiones = null;
		logeo.log("Peticion de datos de actualizacion de reimpresion de vouchers:"
				+ _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPUPDVOUCHERVENTA");
			conn = obtenerConexion();
			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setInt(1, _peticion.getPaPaisId());
			cstmt.setLong(2, _peticion.getTiendaId());
			cstmt.setLong(3, _peticion.getPaMovimientoId());
			StructDescriptor estructuraReimpresion = StructDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOREIMPRESIONESORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloReimpresion = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOREIMPRESIONESORACLE"),
							oconn);
			Object[] articulos = new Object[_peticion.getPaArrReimpresiones().length];

			int contador = 0;

			for (ReimpresionBean reimpresion : _peticion
					.getPaArrReimpresiones()) {
				
				Object[] objeto = new Object[] { reimpresion.getFiPagoTarjeta() };
				articulos[contador] = new STRUCT(estructuraReimpresion, oconn,
						objeto);
				contador++;

			}
			arregloReimpresiones = new ARRAY(descriptorArregloReimpresion,
					oconn, articulos);
			cstmt.setARRAY(4, arregloReimpresiones);
			cstmt.registerOutParameter(5, OracleTypes.NUMBER);
			cstmt.registerOutParameter(6, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(cstmt.getInt(5));
			respuesta.setPaDescError(cstmt.getString(6));
			logeo.log("Respuesta de la peticion de datos para la actulizacion de reimpresion de vouchers:"
					+ respuesta.getPaCdgError());
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarResultSet(ReimpresionesTar);
			Conexion.cerrarStatement(cstmt);
			Conexion.cerrarConexion(conn);
			Conexion.cerrarConexion(oconn);
		}
		return respuesta;
	}

	
	/***
	 * Metodo encargado de consultar el numero de transaccion de la base central
	 * 
	 * @param _peticion
	 * @return Un long 0 => No se encontro el registro, otro =>Numero de
	 *         transaccion.
	 * @throws WSExcepcion
	 */
	public long consultarTransaccionCentral(
			PeticionTransaccionCentralBean _peticion) throws WSExcepcion {
		long respuesta = 0;
		Connection conn = null;
		OracleConnection oconn = null;
		OracleCallableStatement cstmt = null;
		String sql = null;
		logeo.log("Consulta de transaccion en central:" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.FNCONSULTATRANSACCION");
			conn = obtenerConexion();
			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.registerOutParameter(1, OracleTypes.NUMBER);
			cstmt.setInt(2, _peticion.getPaPaisId());
			cstmt.setLong(3, _peticion.getTiendaId());
			cstmt.setLong(4, _peticion.getPaConciliacionId());
			cstmt.setLong(5, _peticion.getPaUsuarioId());
			cstmt.setInt(6, _peticion.getPaSistemaId());
			cstmt.setInt(7, _peticion.getPaModuloId());
			cstmt.setInt(8, _peticion.getPaSubModuloId());
			cstmt.execute();
			respuesta = cstmt.getLong(1);
			logeo.log("Respuesta de la consulta de transaccion en central:"
					+ respuesta);
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarStatement(cstmt);
			Conexion.cerrarConexion(conn);
			Conexion.cerrarConexion(oconn);
		}
		return respuesta;

	}

	
	/***
	 * Metodo encargado de confirmar la reimpresion de un ticket.
	 * 
	 * @param _peticion
	 * @return Un objeto con el detalle de la reimpresion.
	 * @throws WSExcepcion
	 */
	public RespuestaActReimpTicketBean confirmarReimpresionTicket(
			PeticionActReimpTicketBean _peticion) throws WSExcepcion {
		RespuestaActReimpTicketBean respuesta = new RespuestaActReimpTicketBean();
		Connection conn = null;
		OracleConnection oconn = null;
		OracleCallableStatement cstmt = null;
		String sql = null;
		logeo.log("Peticion de confirmacion de reimpresion de ticket:"
				+ _peticion.toString());
		try {
			sql = ConfigSION
					.obtenerParametro("VENTA.CONFIRMARREIMPRESIONTICKET");
			conn = obtenerConexion();
			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setInt(1, _peticion.getPaPaisId());
			cstmt.setLong(2, _peticion.getTiendaId());
			cstmt.setLong(3, _peticion.getPaMovimientoId());
			cstmt.registerOutParameter(4, OracleTypes.NUMBER);
			cstmt.registerOutParameter(5, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(cstmt.getInt(4));
			respuesta.setPaDescError(cstmt.getString(5));
			logeo.log("Respuesta de la confirmacion de reimpresion de ticket:"
					+ respuesta);
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarStatement(cstmt);
			Conexion.cerrarConexion(conn);
			Conexion.cerrarConexion(oconn);
		}

		return respuesta;
	}

	/***
	 * Metodo encargado de consultar el detalle de una tarjeta.
	 * 
	 * @param _peticion
	 * @return Un objeto con el detalle de la tarjeta.
	 * @throws WSExcepcion
	 */
	public RespuestaTarjetaBean consultarTarjeta(PeticionTarjetaBean _peticion)
			throws WSExcepcion {

		RespuestaTarjetaBean respuesta = new RespuestaTarjetaBean();

		Connection conn = null;
		OracleConnection oconn = null;
		OracleCallableStatement cstmt = null;
		ResultSet resultSetTarjetas = null;
		String sql = null;
		logeo.log("Peticion de confirmacion de consulta de tarjeta:"
				+ _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPOBTENERREGISTROSTARJETAS");
			conn = obtenerConexion();
			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setLong(1, _peticion.getFiPaisId());
			cstmt.setLong(2, _peticion.getPaTiendaId());
			cstmt.setString(3, _peticion.getPaNumTarjeta());
			cstmt.setString(4, _peticion.getPaFecha());
			cstmt.registerOutParameter(5, OracleTypes.CURSOR);
			cstmt.registerOutParameter(6, OracleTypes.NUMBER);
			cstmt.registerOutParameter(7, OracleTypes.VARCHAR);
			cstmt.execute();

			respuesta.setPeticionTarjetaBean(_peticion);
			respuesta.setCdgError(cstmt.getInt(6));
			respuesta.setDescError(cstmt.getString(7));

			if (respuesta.getCdgError() == 0) {
				resultSetTarjetas = (ResultSet) cstmt.getCursor(5);
				ArrayList<TarjetaBean> tarjetasList = new ArrayList<TarjetaBean>();
				while (resultSetTarjetas.next()) {
					TarjetaBean tarjetaBean = new TarjetaBean();
					tarjetaBean.setFiPagoTarjetasId(resultSetTarjetas
							.getLong(1));
					tarjetaBean.setFdFechaoperacion(resultSetTarjetas
							.getString(2));
					tarjetaBean.setFiNumTransaccion(resultSetTarjetas
							.getLong(3));
					tarjetaBean.setFiEstatustarjetaId(resultSetTarjetas
							.getInt(4));
					tarjetaBean.setFcEstatus(resultSetTarjetas.getString(5));
					tarjetaBean.setFcNumeroAutorizacion(resultSetTarjetas
							.getString(6));
					tarjetaBean.setFnMontoTarjeta(resultSetTarjetas
							.getDouble(7));
					tarjetaBean.setFiSubmoduloId(resultSetTarjetas.getInt(8));
					tarjetasList.add(tarjetaBean);
				}
				if (tarjetasList.size() != 0) {
					TarjetaBean[] tarjetasArreglo = new TarjetaBean[tarjetasList
							.size()];
					tarjetasList.toArray(tarjetasArreglo);
					respuesta.setTarjetaBeans(tarjetasArreglo);
				} else {
					respuesta.setTarjetaBeans(null);
				}
			} else {
				respuesta.setTarjetaBeans(null);
			}

			logeo.log("Respuesta de la confirmacion consulta de tarjeta:"
					+ respuesta);

		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarResultSet(resultSetTarjetas);
			Conexion.cerrarStatement(cstmt);
			Conexion.cerrarConexion(conn);
			Conexion.cerrarConexion(oconn);
		}

		return respuesta;
	}

	
	/****
	 * Metodo que consulta el numero de transaccion de una venta de TA ademas
	 * del posible numero de Autorizacion
	 * 
	 * @param _peticion
	 * @return
	 * @throws WSExcepcion
	 */
	public RespuestaAutorizacionTABean obtenerAutorizacionTA(int pais,
			long tiendaId, long transaccionId) throws WSExcepcion {
		RespuestaAutorizacionTABean respuesta = new RespuestaAutorizacionTABean();
		Connection conn = null;
		OracleConnection oconn = null;
		OracleCallableStatement cstmt = null;
		String sql = null;
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPCONSULTATRANSACCIONTA");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setInt(1, pais);
			cstmt.setLong(2, tiendaId);
			cstmt.setLong(3, transaccionId);
			cstmt.registerOutParameter(4, OracleTypes.NUMBER);
			cstmt.registerOutParameter(5, OracleTypes.NUMBER);
			cstmt.registerOutParameter(6, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setCdgError(cstmt.getInt(5));
			respuesta.setDescError(cstmt.getString(6));
			if (respuesta.getCdgError() == 0) {
				respuesta.setTransaccionTA(cstmt.getLong(4));
			}
		} catch (Exception ex) {
			logeo.logearExcepcion(ex, sql, "Pais:" + pais + ",tiendaId:"
					+ tiendaId + ",transaccionId:" + transaccionId);
			throw new WSExcepcion(ex.getLocalizedMessage(), ex);
		} finally {
			Conexion.cerrarStatement(cstmt);
			Conexion.cerrarConexion(conn);
			Conexion.cerrarConexion(oconn);
		}
		return respuesta;
	}

	
	/**
	 * M�todo que consulta las 10 �ltimas transacciones de venta grabadas en BD
	 * central
	 * 
	 * @return Un objeto de respuesta de consulta de trnasacciones
	 * @throws WSExcepcion
	 */
	public RespuestaUltimasTransaccionesVentaBean consultaUltimasTransacciones(PeticionUltimasTransaccionesVentaBean _peticion)
			throws WSExcepcion {
		RespuestaUltimasTransaccionesVentaBean respuesta = new RespuestaUltimasTransaccionesVentaBean();

		logeo.log("Comienza m�todo DAO de consulta de �ltimas transacciones");

		Connection conn = null;
		OracleConnection oconn = null;
		OracleCallableStatement cstmt = null;
		ResultSet resultSetVentas = null;
		String sql = null;

		try {
			sql = ConfigSION.obtenerParametro("VENTA.FNULTIMAS10TRANSACCIONES");
			conn = obtenerConexion();
			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setInt(2, _peticion.getPaPaisId());
			cstmt.setLong(3, _peticion.getPaTiendaId());
			cstmt.execute();

			resultSetVentas = (ResultSet) cstmt.getCursor(1);

			if (resultSetVentas != null) {

				ArrayList<UltimasTransaccionesVentaBean> ventasList = new ArrayList<UltimasTransaccionesVentaBean>();
				while (resultSetVentas.next()) {
					UltimasTransaccionesVentaBean ventaBean = new UltimasTransaccionesVentaBean();
					ventaBean.setFiTipoMovtoCajaId(resultSetVentas.getInt(1));
					ventaBean.setFiNumTransaccion(resultSetVentas.getLong(2));
					ventaBean.setFiSubModuloId(resultSetVentas.getInt(3));
					ventaBean
							.setFdFechaMovimiento(resultSetVentas.getString(4));
					ventasList.add(ventaBean);
				}
				if (ventasList.size() != 0) {
					UltimasTransaccionesVentaBean[] ventasArreglo = new UltimasTransaccionesVentaBean[ventasList
							.size()];
					ventasList.toArray(ventasArreglo);
					respuesta.setTransaccionesVentaBeans(ventasArreglo);
				} else {
					respuesta.setTransaccionesVentaBeans(null);
				}

			} else {
				respuesta.setTransaccionesVentaBeans(null);
			}

			logeo.log("Respuesta de servicio: " + respuesta.toString());

		} catch (Exception e) {
			respuesta.setTransaccionesVentaBeans(null);
			logeo.logearExcepcion(e, sql);
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarResultSet(resultSetVentas);
			Conexion.cerrarStatement(cstmt);
			Conexion.cerrarConexion(conn);
			Conexion.cerrarConexion(oconn);
		}

		return respuesta;
	}
	
	
	/******************************************************************************/
	/************************Remover despues de ieps*******************************/

	
	/***
	 * Realiza una llamada al procedimiento almacenado encargado de obtener el
	 * detalle de una venta.
	 * --No contempla ieps borrar al implementarlo
	 * 
	 * @param _peticion
	 * @return Un objeto con el detalle de la venta.
	 * @throws WSExcepcion
	 */
	public RespuestaDetalleVentaBean obtenerDetalleVenta(
			PeticionDetalleVentaBean _peticion, int tipo) throws WSExcepcion {
		RespuestaDetalleVentaBean respuesta = new RespuestaDetalleVentaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ResultSet resultado = null;
		logeo.log("Peticion de obtencion del detalle de la venta:"
				+ _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPSELDETALLEVENTA");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setInt(1, _peticion.getPaPaisId());
			cstmt.setLong(2, _peticion.getTiendaId());
			cstmt.setLong(3, Long.valueOf(_peticion.getPaNumTransaccion()));
			cstmt.setInt(4, _peticion.getPaParamTipoTrans());
			cstmt.registerOutParameter(5, OracleTypes.NUMBER);
			cstmt.registerOutParameter(6, OracleTypes.NUMBER);
			cstmt.registerOutParameter(7, OracleTypes.CURSOR);
			cstmt.registerOutParameter(8, OracleTypes.NUMBER);
			cstmt.registerOutParameter(9, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaMontoDev(cstmt.getDouble(5));
			respuesta.setPaMovimientoId(cstmt.getLong(6));
			respuesta.setPaCdgError(cstmt.getInt(8));
			respuesta.setPaDescError(cstmt.getString(9));
			logeo.log("Resultado de la peticion del detalle de la venta:"
					+ respuesta.getPaDescError());
			if (respuesta.getPaCdgError() == 0) {
				resultado = (ResultSet) cstmt.getCursor(7);
				ArrayList<ArticuloBean> articulosList = new ArrayList<ArticuloBean>();
				while (resultado.next()) {
					ArticuloBean articulo = new ArticuloBean();
					articulo.setFiArticuloId(resultado.getLong(1));
					articulo.setFcCdGbBarras(resultado.getString(2));
					articulo.setFnCantidad(resultado.getInt(3));
					articulo.setFnPrecio(resultado.getDouble(4));
					articulo.setFnCosto(resultado.getDouble(5));
					articulo.setFnDescuento(resultado.getDouble(6));
					articulo.setFnIva(resultado.getDouble(7));
					articulo.setFcNombreArticulo(resultado.getString(9));
					articulo.setFiAgranel(resultado.getDouble(10));
					articulosList.add(articulo);
				}
				if (articulosList.size() != 0) {
					ArticuloBean[] articulos = new ArticuloBean[articulosList
							.size()];
					articulosList.toArray(articulos);
					respuesta.setPaCurDetalleVenta(articulos);
				} else {
					respuesta.setPaCurDetalleVenta(null);
				}
			} else {
				respuesta.setPaCurDetalleVenta(null);
			}
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(oconn, cstmt, resultado);
			Conexion.cerrarConexion(conn);
		}
		return respuesta;
	}
	
	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar la
	 * venta de productos. NO GRABA IEPS
	 * 
	 * @param _peticion
	 * @deprecated NO UTILIZAR,NO GRABA IEPS
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws WSExcepcion
	 */
	public RespuestaVentaBean registrarConfirmacionVenta(PeticionConfirmaVentaNormalBean _peticion)
			throws WSExcepcion {
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		
		String sql = null;
		
		logeo.log("Peticion de registro de confirmacion de venta:" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPCONFIRMACONCILIACION");
			
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			
			cstmt.setInt(1, _peticion.getPaPaisId());
			cstmt.setLong(2, _peticion.getTiendaId());
			cstmt.setLong(3, _peticion.getNumTransaccion());
			cstmt.setLong(4, _peticion.getPaConciliacionId());
			cstmt.setLong(5, _peticion.getUsuarioId());
			cstmt.registerOutParameter(6, OracleTypes.NUMBER);
			cstmt.registerOutParameter(7, OracleTypes.VARCHAR);
			
			cstmt.execute();
			logeo.log("Ejecusion completada:" + respuesta.toString());
			
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(6)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(7)));
			
			logeo.log("Respuesta:" + respuesta.toString());
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(conn, cstmt, null);

		}
		return respuesta;
	}
	
	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar la
	 * venta de productos. 
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws WSExcepcion
	 */
	public RespuestaVentaBean registrarConfirmacionVenta(PeticionConfirmaVentaArticulosBean _peticion)
			throws WSExcepcion {
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		
		String sql = null;
		
		logeo.log("Peticion de registro de confirmacion de venta:" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPCONFIRMACONCILIACION");
			
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			
			cstmt.setInt(1, _peticion.getPaPaisId());
			cstmt.setLong(2, _peticion.getTiendaId());
			cstmt.setLong(3, _peticion.getNumTransaccion());
			cstmt.setLong(4, _peticion.getPaConciliacionId());
			cstmt.setLong(5, _peticion.getUsuarioId());
			cstmt.registerOutParameter(6, OracleTypes.NUMBER);
			cstmt.registerOutParameter(7, OracleTypes.VARCHAR);
			
			cstmt.execute();
			logeo.log("Ejecusion completada:" + respuesta.toString());
			
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(6)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(7)));
			
			logeo.log("Respuesta:" + respuesta.toString());
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(oconn, cstmt, null);

		}
		return respuesta;
	}


	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar
	 * una devolucion de productos.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la devolucion.
	 * @throws WSExcepcion
	 */
	public RespuestaVentaBean registrarDevolucion(
			PeticionDevolucionBean _peticion) throws WSExcepcion {
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		logeo.log("Detalle de la peticion de devolucion:"
				+ _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPSERVICIOVENTA");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			Object[] articulos = new Object[_peticion.getArticulos().length];
			StructDescriptor estructuraArticulo = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOARTICULOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOARTICULOSORACLE"),
							oconn);
			int contador = 0;
			for (ArticuloBean articulo : _peticion.getArticulos()) {
				
				Object[] objeto = new Object[] { articulo.getFiArticuloId(),
						articulo.getFcCdGbBarras(), articulo.getFnCantidad(),
						articulo.getFnPrecio(), articulo.getFnCosto(),
						articulo.getFnDescuento(), articulo.getFnIva(),
						0};
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,
						objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,
					articulos);
			Object[] tiposPago = new Object[1];
			StructDescriptor estructuraTiposPago = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"),
							oconn);

			Object[] objeto = new Object[] { 1, _peticion.getPaMontoTotalVta(),
					0, 0 };
			tiposPago[0] = new STRUCT(estructuraTiposPago, oconn, objeto);
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,
					tiposPago);

			cstmt.setString(1, _peticion.getPaTerminal());
			cstmt.setLong(2, _peticion.getPaPaisId());
			cstmt.setLong(3, _peticion.getTiendaId());
			cstmt.setLong(4, _peticion.getPaUsuarioId());
			cstmt.setLong(5, _peticion.getPaUsuarioAutorizaId());
			cstmt.setString(6, _peticion.getPaFechaOper());
			cstmt.setLong(7, _peticion.getPaTipoMovto());
			cstmt.setDouble(8, _peticion.getPaMontoTotalVta());
			cstmt.setARRAY(9, arregloArticulos);
			cstmt.setARRAY(10, arregloTiposPago);
			cstmt.setInt(11, _peticion.getPaTipoDevolucion());
			cstmt.setLong(12, _peticion.getPaTransaccionDev());
			cstmt.setLong(13, _peticion.getPaMovimientoIdDev());
			cstmt.setLong(14, _peticion.getPaConciliacionId());
			cstmt.setInt(15, _peticion.getFueraLinea());
			cstmt.registerOutParameter(16, OracleTypes.NUMBER);
			cstmt.registerOutParameter(17, OracleTypes.CURSOR);
			cstmt.registerOutParameter(18, OracleTypes.NUMBER);
			cstmt.registerOutParameter(19, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(18)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(19)));
			respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(16)));
			logeo.log("Respuesta de la peticion de devolucion:"
					+ respuesta.toString());
			if (respuesta.getPaTransaccionId() == 0
					&& respuesta.getPaCdgError() == 0) {
				respuesta.setPaCdgError(Integer.valueOf(ConfigSION
						.obtenerParametro("transaccion.cero.codigo")));
				respuesta.setPaDescError(ConfigSION
						.obtenerParametro("transaccion.cero.mensaje"));
			}
			if (respuesta.getPaCdgError() == 0) {
				bloqueos = (ResultSet) cstmt.getCursor(17);
				ArrayList<BloqueoBean> bloqueosList = new ArrayList<BloqueoBean>();
				while (bloqueos.next()) {
					BloqueoBean bloqueoBean = new BloqueoBean();
					bloqueoBean.setFiTipoPagoId(bloqueos.getInt(1));
					bloqueoBean.setFiNumAvisos(bloqueos.getInt(2));
					bloqueoBean.setFiAvisosFalt(bloqueos.getInt(3));
					bloqueoBean.setFiEstatusBloqueoId(bloqueos.getInt(4));
					bloqueosList.add(bloqueoBean);
				}
				if (bloqueosList.size() != 0) {
					BloqueoBean[] bloqueosBean = new BloqueoBean[bloqueosList
							.size()];
					bloqueosList.toArray(bloqueosBean);
					respuesta.setPaTypCursorBlqs(bloqueosBean);
				} else {
					respuesta.setPaTypCursorBlqs(null);
				}
			} else {
				respuesta.setPaTypCursorBlqs(null);
			}

		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
			Conexion.cerrarConexion(conn);
		}
		return respuesta;
	}
	
	/********************Facturacion Peter******************************/
	
	/**
	 * M�todo que consulta el detalle de una venta o recarga de tiempo aire que ser� reimpresa por el m�dulo de facturaci�n
	 *  
	 * @param _peticion
	 *            Un objeto de petici�n de reimpresi�n de ticket
	 * @return Un objeto con el detalle del ticket a reimprimir
	 * @throws WSExcepcion
	 */

	public RespuestaDetalleTicketBean consultarTicketVenta(
			PeticionTicketReimpresionBean _peticion) throws WSExcepcion {
		if (_peticion != null) {
			RespuestaDetalleTicketBean respuesta = new RespuestaDetalleTicketBean();

			logeo.log("Petici�n detalle de ticket (Facturaci�n): " + _peticion);

			Connection conn = null;
			OracleConnection oconn = null;
			OracleCallableStatement cstmt = null;
			ResultSet resultSet = null;
			String sql = null;

			try {
				sql = ConfigSION.obtenerParametro("VENTA.SPREIMPRESIONTICKETVENTA");
				conn = obtenerConexion();
				oconn = (OracleConnection) conn;
				cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

				cstmt.setInt(1, _peticion.getPaPaisId());
				cstmt.setLong(2, _peticion.getPaTiendaId());
				cstmt.setLong(3, _peticion.getPaNumTransaccion());
				cstmt.setInt(4, _peticion.getPaParamTipoTrans());
				cstmt.registerOutParameter(5, OracleTypes.NUMBER);
				cstmt.registerOutParameter(6, OracleTypes.NUMBER);
				cstmt.registerOutParameter(7, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(8, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(9, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(10, OracleTypes.CURSOR);
				cstmt.registerOutParameter(11, OracleTypes.CURSOR);
				cstmt.registerOutParameter(12, OracleTypes.CURSOR);
				cstmt.registerOutParameter(13, OracleTypes.NUMBER);
				cstmt.registerOutParameter(14, OracleTypes.NUMBER);
				cstmt.registerOutParameter(15, OracleTypes.NUMBER);
				cstmt.registerOutParameter(16, OracleTypes.NUMBER);
				cstmt.registerOutParameter(17, OracleTypes.VARCHAR);
				cstmt.execute();

				respuesta.setPaMoviminetoId(cstmt.getLong(5));
				respuesta.setFiUsuarioId(cstmt.getLong(6));
				respuesta.setPaNombreUsuario(cstmt.getString(7));
				respuesta.setPaFechaCreacion(cstmt.getString(8));
				respuesta.setPaEstacionTrabajo(cstmt.getString(9));
				respuesta.setPaPermiteTicket(cstmt.getInt(13));
				respuesta.setPaTipoMovimiento(cstmt.getInt(14));
				respuesta.setPaTipoCajaId(cstmt.getInt(15));
				respuesta.setPaCdgError(cstmt.getInt(16));
				respuesta.setPaDescError(cstmt.getString(17));

				if (respuesta.getPaCdgError() == 0) {

					if (respuesta.getPaTipoMovimiento() == 9) {
						// leer cursor articulos
						resultSet = (ResultSet) cstmt.getCursor(10);
						if (resultSet != null) {
							ArrayList<ArticuloFacturacionBean> articulosList = new ArrayList<ArticuloFacturacionBean>();
							while (resultSet.next()) {
								ArticuloFacturacionBean articuloBean = new ArticuloFacturacionBean();
								articuloBean.setFnCantidad(resultSet.getDouble(1));
								articuloBean.setFnPrecio(resultSet.getDouble(2));
								articuloBean.setFnIva(resultSet.getDouble(3));
								articuloBean.setFnDescuento(resultSet.getDouble(4));
								articuloBean.setFiIepsId(resultSet.getInt(5));
								articuloBean.setFcCdgBarras(resultSet.getString(6));
								articuloBean.setFiArticuloId(resultSet.getLong(7));
								articuloBean.setFcNombreArticulo(resultSet.getString(8));
								articulosList.add(articuloBean);
							}
							if (articulosList.size() != 0) {
								ArticuloFacturacionBean[] articulosArreglo = new ArticuloFacturacionBean[articulosList
										.size()];
								articulosList.toArray(articulosArreglo);
								respuesta
										.setArticuloFacturacionBeans(articulosArreglo);
							} else {
								respuesta.setArticuloFacturacionBeans(null);
							}

						} else {
							respuesta.setArticuloFacturacionBeans(null);
						}
						respuesta.setArticuloTAReimpresionBeans(null);
						respuesta.setServiciosReimpresionBeans(null);
					} else if (respuesta.getPaTipoMovimiento() == 10) {
						// leer cursor articulos tiempo aire
						resultSet = (ResultSet) cstmt.getCursor(10);
						if (resultSet != null) {
							ArrayList<ArticuloTAReimpresionBean> articulosTAList = new ArrayList<ArticuloTAReimpresionBean>();
							while (resultSet.next()) {
								ArticuloTAReimpresionBean articuloTABean = new ArticuloTAReimpresionBean();
								articuloTABean.setNumOperacion(resultSet
										.getLong(1));
								articuloTABean.setNumReferencia(resultSet
										.getLong(2));
								articuloTABean
										.setNumero(resultSet.getString(3));
								articuloTABean.setFiEmpresaId(resultSet
										.getInt(4));
								articuloTABean.setCompania(resultSet
										.getString(5));
								articuloTABean.setFcNombreArticulo(resultSet
										.getString(6));
								articulosTAList.add(articuloTABean);
							}
							if (articulosTAList.size() != 0) {
								ArticuloTAReimpresionBean[] articulosTAArreglo = new ArticuloTAReimpresionBean[articulosTAList
										.size()];
								articulosTAList.toArray(articulosTAArreglo);
								respuesta
										.setArticuloTAReimpresionBeans(articulosTAArreglo);
							} else {
								respuesta.setArticuloTAReimpresionBeans(null);
							}

						} else {
							respuesta.setArticuloTAReimpresionBeans(null);
						}
						respuesta.setArticuloFacturacionBeans(null);
						respuesta.setServiciosReimpresionBeans(null);
					}else if(respuesta.getPaTipoMovimiento() == 19){
						//Leer cursor de Servicios
						resultSet = (ResultSet) cstmt.getObject(10);
						if(resultSet != null){
							List<ServicioReimpresionBean> servicios = new ArrayList<ServicioReimpresionBean>();
							ServicioReimpresionBean srv = null;
							while(resultSet.next()){
								srv = new ServicioReimpresionBean();
								srv.setMonto(resultSet.getDouble(1));
								srv.setImpuesto(resultSet.getDouble(2));
								srv.setComision(resultSet.getDouble(3));
								srv.setNumAutorizacion(resultSet.getLong(4));
								srv.setReferencia(resultSet.getString(5));
								srv.setMensaje(resultSet.getString(6));
								srv.setNomArticulo(resultSet.getString(7));
								
								servicios.add(srv);
							}
							if(servicios.size() != 0)
								respuesta.setServiciosReimpresionBeans(servicios.toArray(new ServicioReimpresionBean[0]));
							else
								respuesta.setServiciosReimpresionBeans(null);
						}else {
							respuesta.setServiciosReimpresionBeans(null);
						}
						respuesta.setArticuloFacturacionBeans(null);
						respuesta.setArticuloTAReimpresionBeans(null);
					}

					// leer cursor tipos de pago
					boolean esPagoConTarjeta = false;
					resultSet = (ResultSet) cstmt.getCursor(11);
					if (resultSet != null) {
						ArrayList<TipoPagoReimpresionBean> pagosList = new ArrayList<TipoPagoReimpresionBean>();
						while (resultSet.next()) {
							TipoPagoReimpresionBean pagoBean = new TipoPagoReimpresionBean();
							pagoBean.setFiTipoPagoId(resultSet.getInt(1));
							pagoBean.setMontoPago(resultSet.getDouble(2));
							pagoBean.setFnMontoRecibido(resultSet.getDouble(3));

							if (pagoBean.getFiTipoPagoId() == 2) {
								esPagoConTarjeta = true;
							}

							pagosList.add(pagoBean);

						}
						if (pagosList.size() != 0) {
							TipoPagoReimpresionBean[] pagosArreglo = new TipoPagoReimpresionBean[pagosList
									.size()];
							pagosList.toArray(pagosArreglo);
							respuesta.setTipoPagoReimpresionBeans(pagosArreglo);
						} else {
							respuesta.setTipoPagoReimpresionBeans(null);
						}

					} else {
						respuesta.setTipoPagoReimpresionBeans(null);
					}

					if (esPagoConTarjeta) {
						// leer cursor tarjetas
						resultSet = (ResultSet) cstmt.getCursor(12);
						if (resultSet != null) {
							ArrayList<TarjetaReimpresionBean> tarjetasList = new ArrayList<TarjetaReimpresionBean>();
							while (resultSet.next()) {
								TarjetaReimpresionBean tarjetaBean = new TarjetaReimpresionBean();
								tarjetaBean.setFiPagoTarjetasId(resultSet
										.getLong(1));
								tarjetaBean.setImporteComision(resultSet
										.getDouble(2));
								tarjetaBean.setFcNumeroTarjeta(resultSet
										.getString(3));
								tarjetaBean.setFcNumeroAutorizacion(resultSet
										.getString(4));
								tarjetaBean.setFiAfiliacion(resultSet
										.getLong(5));
								tarjetaBean.setFcDescripcion(resultSet
										.getString(6));
								tarjetasList.add(tarjetaBean);
							}
							if (tarjetasList.size() != 0) {
								TarjetaReimpresionBean[] tarjetasArreglo = new TarjetaReimpresionBean[tarjetasList
										.size()];
								tarjetasList.toArray(tarjetasArreglo);
								respuesta
										.setTarjetaReimpresionBeans(tarjetasArreglo);
							} else {
								respuesta.setTarjetaReimpresionBeans(null);
							}

						} else {
							respuesta.setTarjetaReimpresionBeans(null);
						}
					} else {
						respuesta.setTarjetaReimpresionBeans(null);
					}

				}else{
					respuesta.setArticuloFacturacionBeans(null);
					respuesta.setArticuloTAReimpresionBeans(null);
					respuesta.setTarjetaReimpresionBeans(null);
					respuesta.setTipoPagoReimpresionBeans(null);
					respuesta.setServiciosReimpresionBeans(null);
				}

				logeo.log("Respuesta a la petici�n de detalle de ticket (facturaci�n): " + respuesta.toString());

			} catch (Exception e) {
				respuesta.setTipoPagoReimpresionBeans(null);
				respuesta.setArticuloFacturacionBeans(null);
				respuesta.setTarjetaReimpresionBeans(null);
				respuesta.setServiciosReimpresionBeans(null);
				logeo.logearExcepcion(e, sql);
				throw new WSExcepcion(e.getLocalizedMessage(), e);
			} finally {

				Conexion.cerrarResultSet(resultSet);
				Conexion.cerrarStatement(cstmt);
				Conexion.cerrarConexion(conn);
				Conexion.cerrarConexion(oconn);
			}

			return respuesta;
		} else {
			return null;
		}
	}
	
	/****************************************************/
	/********************Nuevos M�todos con Ieps*******/
						

	/***
	 * Realiza una llamada al procedimiento almacenado encargado de obtener el detalle de una venta.
	 * Este m�todo es usado para consultar el detalle de una venta a la cual se pretende realizar una devoluci�n por ticket
	 * 
	 * @param _peticion
	 * @return Un objeto con el detalle de la venta.
	 * @throws WSExcepcion
	 */
	public RespuestaConsultaVentaBean consultarDetalleVenta(
			PeticionDetalleVentaBean _peticion, int tipo) throws WSExcepcion {
		RespuestaConsultaVentaBean respuesta = new RespuestaConsultaVentaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ResultSet resultado = null;
		logeo.log("Peticion de detalle de la venta para devoluci�n:"
				+ _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPSELDETALLEVENTA");
			conn = obtenerConexion();
			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;
			//oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setInt(1, _peticion.getPaPaisId());
			cstmt.setLong(2, _peticion.getTiendaId());
			cstmt.setLong(3, Long.valueOf(_peticion.getPaNumTransaccion()));
			cstmt.setInt(4, _peticion.getPaParamTipoTrans());
			cstmt.registerOutParameter(5, OracleTypes.NUMBER);
			cstmt.registerOutParameter(6, OracleTypes.NUMBER);
			cstmt.registerOutParameter(7, OracleTypes.CURSOR);
			cstmt.registerOutParameter(8, OracleTypes.NUMBER);
			cstmt.registerOutParameter(9, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaMontoDev(cstmt.getDouble(5));
			respuesta.setPaMovimientoId(cstmt.getLong(6));
			respuesta.setPaCdgError(cstmt.getInt(8));
			respuesta.setPaDescError(cstmt.getString(9));
			
			if (respuesta.getPaCdgError() == 0) {
				resultado = (ResultSet) cstmt.getCursor(7);
				ArrayList<ArticuloPeticionVentaBean> articulosList = new ArrayList<ArticuloPeticionVentaBean>();
				while (resultado.next()) {
					ArticuloPeticionVentaBean articulo = new ArticuloPeticionVentaBean();
					articulo.setFiArticuloId(resultado.getLong(1));
					articulo.setFcCdGbBarras(resultado.getString(2));
					articulo.setFnCantidad(resultado.getInt(3));
					articulo.setFnPrecio(resultado.getDouble(4));
					articulo.setFnCosto(resultado.getDouble(5));
					articulo.setFnDescuento(resultado.getDouble(6));
					articulo.setFnIva(resultado.getDouble(7));
					articulo.setFiIepsId(resultado.getInt(8));
					articulo.setFcNombreArticulo(resultado.getString(9));
					articulo.setFiAgranel(resultado.getDouble(10));
					articulosList.add(articulo);
				}
				if (articulosList.size() != 0) {
					ArticuloPeticionVentaBean[] articulos = new ArticuloPeticionVentaBean[articulosList.size()];
					articulosList.toArray(articulos);
					respuesta.setPaCurArticulosVenta(articulos);
				} else {
					respuesta.setPaCurArticulosVenta(null);
				}
			} else {
				respuesta.setPaCurArticulosVenta(null);
			}
			logeo.log("Respuesta a la petici�n del detalle de venta para devoluci�n: "+ respuesta.toString());
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(oconn, cstmt, resultado);
			Conexion.cerrarConexion(conn);
		}
		return respuesta;
	}

	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar la
	 * venta de productos
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws WSExcepcion
	 */
	public RespuestaVentaBean registrarVenta(PeticionVentaArticulosBean _peticion, Long idpagotarjetaPT )
			throws WSExcepcion {
		int ID_FLUJO_PAGATODO = 1;
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		
		logeo.log("Peticion de registro de venta paga todo:" + _peticion.toString());
		
		try {			
			sql = ConfigSION.obtenerParametro( "VENTA.SPVENTAPAGATODOTD" );
			conn = obtenerConexion();
			//if(conn.isWrapperFor(OracleConnection.class))
			//	oconn = conn.unwrap(OracleConnection.class);
			//else
				oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

			Object[] articulos = new Object[_peticion.getArticulos().length];
			StructDescriptor estructuraArticulo = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOARTICULO.TIPODESCTO"),//VENTA.OBJETOARTICULOORACLE
							oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOARTICULOS.TIPODESCTO"), //VENTA.OBJETOARREGLOARTICULOSORACLE
							oconn);
			logeo.log(_peticion.getArticulos().length+" Articulos para la venta");
			int contador = 0;
			for (ArticuloPeticionVentaBean articulo : _peticion.getArticulos()) {
				
				Object[] objeto = new Object[] { articulo.getFiArticuloId(),
						articulo.getFcCdGbBarras(), articulo.getFnCantidad(),
						articulo.getFnPrecio(), articulo.getFnCosto(),
						articulo.getFnDescuento(), articulo.getFnIva(),
						articulo.getFiIepsId(), articulo.getFiTipoDescuento()};
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,
						objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,
					articulos);
			
			ArrayList<TipoPagoBean> tiposPagoLista = new ArrayList<TipoPagoBean>();
			StructDescriptor estructuraTiposPago = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"),
							oconn);
			TipoPagoBean tarjetas = new TipoPagoBean();
			logeo.log(_peticion.getTiposPago().length+" Tipos de pago para la venta");
			for (TipoPagoBean tipoPago : _peticion.getTiposPago()) {
				if( tipoPago.getPaPagoTarjetaIdBus() == 0 ){ //Tarjeta pagatodo
					//idpagotarjetaPT = ;
				}
					tiposPagoLista.add(tipoPago);
			}
			contador = 0;
			Object[] tiposPago = new Object[tiposPagoLista.size()];
			for (TipoPagoBean tipoPago : tiposPagoLista) {
				Object[] objeto = new Object[] { tipoPago.getFiTipoPagoId(),
						tipoPago.getFnMontoPago(), tipoPago.getFnNumeroVales(),
						tipoPago.getImporteAdicional() };
				tiposPago[contador] = new STRUCT(estructuraTiposPago, oconn,
						objeto);
				contador++;
			}
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,
					tiposPago);
			
			cstmt.setString(1, _peticion.getPaTerminal());
			cstmt.setLong(2, _peticion.getPaPaisId());
			cstmt.setLong(3, _peticion.getTiendaId());
			cstmt.setLong(4, _peticion.getPaUsuarioId());
			cstmt.setLong(5, _peticion.getPaUsuarioAutorizaId());
			cstmt.setString(6, _peticion.getPaFechaOper());
			cstmt.setLong(7, _peticion.getPaTipoMovto());
			cstmt.setDouble(8, _peticion.getPaMontoTotalVta());
			cstmt.setARRAY(9, arregloArticulos);
			cstmt.setARRAY(10, arregloTiposPago);
			cstmt.setInt(11, 0);
			cstmt.setLong(12, 0);
			cstmt.setLong(13, 0);
			cstmt.setLong(14, _peticion.getPaConciliacionId());
			cstmt.setInt(15, _peticion.getFueraLinea());
			cstmt.setInt(16, ID_FLUJO_PAGATODO);
			cstmt.registerOutParameter(17, OracleTypes.NUMBER);
			cstmt.registerOutParameter(18, OracleTypes.CURSOR);
			cstmt.registerOutParameter(19, OracleTypes.NUMBER);
			cstmt.registerOutParameter(20, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(19)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(20)));
			respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(17)));
			if (respuesta.getPaTransaccionId() == 0
					&& respuesta.getPaCdgError() == 0) {
				respuesta.setPaCdgError(Integer.valueOf(ConfigSION
						.obtenerParametro("transaccion.cero.codigo")));
				respuesta.setPaDescError(ConfigSION
						.obtenerParametro("transaccion.cero.mensaje"));
			}
			logeo.log("Respuesta:" + respuesta.toString());
			if (respuesta.getPaCdgError() == 0) {
				bloqueos = (ResultSet) cstmt.getCursor(18);
				ArrayList<BloqueoBean> bloqueosList = new ArrayList<BloqueoBean>();
				while (bloqueos != null && bloqueos.next()) {
					BloqueoBean bloqueoBean = new BloqueoBean();
					bloqueoBean.setFiTipoPagoId(bloqueos.getInt(1));
					bloqueoBean.setFiNumAvisos(bloqueos.getInt(2));
					bloqueoBean.setFiAvisosFalt(bloqueos.getInt(3));
					bloqueoBean.setFiEstatusBloqueoId(bloqueos.getInt(4));
					bloqueosList.add(bloqueoBean);
				}
				if (bloqueosList.size() != 0) {
					BloqueoBean[] bloqueosBean = new BloqueoBean[bloqueosList
							.size()];
					bloqueosList.toArray(bloqueosBean);
					respuesta.setPaTypCursorBlqs(bloqueosBean);
				} else {
					respuesta.setPaTypCursorBlqs(null);
				}

			} else {
				respuesta.setPaTypCursorBlqs(null);
			}
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
			Conexion.cerrarConexion(conn);

		}
		return respuesta;
	}
	
	
	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar la
	 * venta de productos. NO GRABA IEPS
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws WSExcepcion
	 */
	public RespuestaVentaBean registrarVenta(PeticionVentaNormalBean _peticion)
			throws WSExcepcion {
		int ID_FLUJO_PAGATODO = 1;
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		logeo.log("Peticion de registro de venta paga todo:" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPVENTAPAGATODO");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

			Object[] articulos = new Object[_peticion.getArticulos().length];
			StructDescriptor estructuraArticulo = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOARTICULOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOARTICULOSORACLE"),
							oconn);
			logeo.log(_peticion.getArticulos().length+" Articulos para la venta");
			int contador = 0;
			for (ArticuloBean articulo : _peticion.getArticulos()) {
				
				Object[] objeto = new Object[] { articulo.getFiArticuloId(),
						articulo.getFcCdGbBarras(), articulo.getFnCantidad(),
						articulo.getFnPrecio(), articulo.getFnCosto(),
						articulo.getFnDescuento(), articulo.getFnIva(),
						0};
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,
						objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,
					articulos);
			
			ArrayList<TipoPagoBean> tiposPagoLista = new ArrayList<TipoPagoBean>();
			StructDescriptor estructuraTiposPago = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"),
							oconn);
			//TipoPagoBean tarjetas = new TipoPagoBean();
			logeo.log(_peticion.getTiposPago().length+" Tipos de pago para la venta");
			for (TipoPagoBean tipoPago : _peticion.getTiposPago()) {
				tiposPagoLista.add(tipoPago);
			}
			contador = 0;
			//double importeTotalVenta = 0;
			Object[] tiposPago = new Object[tiposPagoLista.size()];
			//BigDecimal dineroConv = new BigDecimal(0.0);
			for (TipoPagoBean tipoPago : tiposPagoLista) {
				
				logeo.log(" Tipo pagos >> TipoPago>" + tipoPago.getFiTipoPagoId()+ ", MontoPago>"+ tipoPago.getFnMontoPago()+ 
						", NumeroVales>"+				 tipoPago.getFnNumeroVales()+ ", ImporteAdicional>"+	tipoPago.getImporteAdicional());
				//importeTotalVenta += tipoPago.getFnMontoPago();
				//dineroConv.add(new BigDecimal(tipoPago.getFnMontoPago()) );
				Object[] objeto = new Object[] { tipoPago.getFiTipoPagoId(),
						tipoPago.getFnMontoPago(), tipoPago.getFnNumeroVales(),
						tipoPago.getImporteAdicional() };
				tiposPago[contador] = new STRUCT(estructuraTiposPago, oconn,
						objeto);
				contador++;
			}
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,
					tiposPago);
			
			cstmt.setString(1, _peticion.getPaTerminal());
			cstmt.setLong(2, _peticion.getPaPaisId());
			cstmt.setLong(3, _peticion.getTiendaId());
			cstmt.setLong(4, _peticion.getPaUsuarioId());
			cstmt.setLong(5, _peticion.getPaUsuarioAutorizaId());
			cstmt.setString(6, _peticion.getPaFechaOper());
			cstmt.setLong(7, _peticion.getPaTipoMovto());
			cstmt.setDouble(8, _peticion.getPaMontoTotalVta()); //importeTotalVenta);//dineroConv.doubleValue());//
			cstmt.setARRAY(9, arregloArticulos);
			cstmt.setARRAY(10, arregloTiposPago);
			cstmt.setInt(11, 0);
			cstmt.setLong(12, 0);
			cstmt.setLong(13, 0);
			cstmt.setLong(14, _peticion.getPaConciliacionId());
			cstmt.setInt(15, _peticion.getFueraLinea());
			cstmt.setInt(16, ID_FLUJO_PAGATODO);
			cstmt.registerOutParameter(17, OracleTypes.NUMBER);
			cstmt.registerOutParameter(18, OracleTypes.CURSOR);
			cstmt.registerOutParameter(19, OracleTypes.NUMBER);
			cstmt.registerOutParameter(20, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(19)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(20)));
			respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(17)));
			if (respuesta.getPaTransaccionId() == 0
					&& respuesta.getPaCdgError() == 0) {
				respuesta.setPaCdgError(Integer.valueOf(ConfigSION
						.obtenerParametro("transaccion.cero.codigo")));
				respuesta.setPaDescError(ConfigSION
						.obtenerParametro("transaccion.cero.mensaje"));
			}
			logeo.log("Respuesta:" + respuesta.toString());
			if (respuesta.getPaCdgError() == 0) {
				bloqueos = (ResultSet) cstmt.getCursor(18);
				ArrayList<BloqueoBean> bloqueosList = new ArrayList<BloqueoBean>();
				while (bloqueos != null && bloqueos.next()) {
					BloqueoBean bloqueoBean = new BloqueoBean();
					bloqueoBean.setFiTipoPagoId(bloqueos.getInt(1));
					bloqueoBean.setFiNumAvisos(bloqueos.getInt(2));
					bloqueoBean.setFiAvisosFalt(bloqueos.getInt(3));
					bloqueoBean.setFiEstatusBloqueoId(bloqueos.getInt(4));
					bloqueosList.add(bloqueoBean);
				}
				if (bloqueosList.size() != 0) {
					BloqueoBean[] bloqueosBean = new BloqueoBean[bloqueosList
							.size()];
					bloqueosList.toArray(bloqueosBean);
					respuesta.setPaTypCursorBlqs(bloqueosBean);
				} else {
					respuesta.setPaTypCursorBlqs(null);
				}

			} else {
				respuesta.setPaTypCursorBlqs(null);
			}
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
			Conexion.cerrarConexion(conn);
		}
		return respuesta;
	}
	
	
	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar la
	 * venta de productos. NO GRABA IEPS
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws WSExcepcion
	 */
	public RespuestaVentaBean procesaTiposPago(
			PeticionVentaNormalBean _peticion, 
			RespuestaVentaBean respuesta )
			throws WSExcepcion {
		
		logeo.log("Peticion de registro de venta paga todo:" + _peticion.toString());
		try {
			if (respuesta.getPaCdgError() == 0) {
				for (TipoPagoBean tipoPago : _peticion.getTiposPago()) {
					if ( tipoPago.getFiTipoPagoId() == TARJETA_ID ){//&& tipoPago.getPaPagoTarjetaIdBus() > 0L ) {
						PeticionMarcadoPagoTarjetaBean marcadoVenta = new PeticionMarcadoPagoTarjetaBean();
						marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION.obtenerParametro("venta.proceso.3.id")));
						marcadoVenta.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION.obtenerParametro("venta.proceso.3.estatus.tarjeta.7")));
						marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
						marcadoVenta.setTiendaId(_peticion.getTiendaId());
						marcadoVenta.setPaPagoTarjetaIdBus(tipoPago.getPaPagoTarjetaIdBus());
						marcadoVenta.setPaNumTransaccion(respuesta.getPaTransaccionId());
						marcadoVenta.setPaTipoPagoId(tipoPago.getFiTipoPagoId());
						
						RespuestaMarcadoPagoTarjetaBean actualizacionMarcado = marcadoPagoTarjeta(marcadoVenta);
						if (actualizacionMarcado.getPaCdgError() != 0) {
							logeo.log("Resultado del marcado de pago con tarjeta:" + marcadoVenta);
						}
					}
				}
			} else {
				for (TipoPagoBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID ){// && tipoPago.getPaPagoTarjetaIdBus() > 0L ) {
						PeticionMarcadoPagoTarjetaBean marcadoVenta = new PeticionMarcadoPagoTarjetaBean();
						marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION.obtenerParametro("venta.proceso.4.id")));
						marcadoVenta.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION.obtenerParametro("venta.proceso.4.estatus.tarjeta.6")));
						marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
						marcadoVenta.setTiendaId(_peticion.getTiendaId());
						marcadoVenta.setPaPagoTarjetaIdBus(tipoPago.getPaPagoTarjetaIdBus());
						RespuestaMarcadoPagoTarjetaBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
						if (respuestaMarcado.getPaCdgError() != 0) {
							logeo.log("Resultado del marcado de pago con tarjeta:" + marcadoVenta);
						}
					}
				}
				respuesta.setPaTypCursorBlqs(null);
			}
		} catch (Exception e) {
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		}
		return respuesta;
	}
	
	
	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar la
	 * venta de productos. NO GRABA IEPS
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws WSExcepcion
	 * @deprecated NO UTILIZAR, NO GRABA IEPS
	 */
	public RespuestaVentaBean registrarCancelacionVenta(PeticionVentaNormalBean _peticion)
			throws WSExcepcion {
		int ID_FLUJO_PAGATODO = 1;
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		logeo.log("Peticion de registro de venta:" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPVENTACONVALIDACION");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

			Object[] articulos = new Object[_peticion.getArticulos().length];
			StructDescriptor estructuraArticulo = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOARTICULOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOARTICULOSORACLE"),
							oconn);
			logeo.log(_peticion.getArticulos().length+" Articulos para la venta");
			int contador = 0;
			for (ArticuloBean articulo : _peticion.getArticulos()) {
				
				Object[] objeto = new Object[] { articulo.getFiArticuloId(),
						articulo.getFcCdGbBarras(), articulo.getFnCantidad(),
						articulo.getFnPrecio(), articulo.getFnCosto(),
						articulo.getFnDescuento(), articulo.getFnIva(),
						0};
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,
						objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,
					articulos);
			
			ArrayList<TipoPagoBean> tiposPagoLista = new ArrayList<TipoPagoBean>();
			StructDescriptor estructuraTiposPago = StructDescriptor
					.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"), oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor.createDescriptor(
					ConfigSION.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"), oconn);
			TipoPagoBean tarjetas = new TipoPagoBean();
			logeo.log(_peticion.getTiposPago().length+" Tipos de pago para la venta");
			for (TipoPagoBean tipoPago : _peticion.getTiposPago()) {
				
				/*if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
					PeticionMarcadoPagoTarjetaBean marcadoVenta = new PeticionMarcadoPagoTarjetaBean();
					marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION
							.obtenerParametro("venta.proceso.4.id")));
					marcadoVenta
							.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
									.obtenerParametro("venta.proceso.4.estatus.tarjeta.5")));
					marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
					marcadoVenta.setTiendaId(_peticion.getTiendaId());
					marcadoVenta.setPaPagoTarjetaIdBus(tipoPago
							.getPaPagoTarjetaIdBus());
					marcadoVenta.setPaComision(tipoPago.getImporteAdicional());
					RespuestaMarcadoPagoTarjetaBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
					if (respuestaMarcado.getPaCdgError() != 0) {
						logeo.log("Resultado del marcado de pago con tarjeta:"
								+ marcadoVenta);
					}
					tarjetas.setFiTipoPagoId(TARJETA_ID);

					tarjetas.setFnMontoPago(tarjetas.getFnMontoPago()
							+ tipoPago.getFnMontoPago());
					tarjetas.setImporteAdicional(tarjetas.getImporteAdicional()
							+ tipoPago.getImporteAdicional());
					tiposPagoLista.remove(tarjetas);
					tiposPagoLista.add(tarjetas);
				} else {*/
					tiposPagoLista.add(tipoPago);
				//}
			}
			contador = 0;
			Object[] tiposPago = new Object[tiposPagoLista.size()];
			for (TipoPagoBean tipoPago : tiposPagoLista) {
				Object[] objeto = new Object[] { tipoPago.getFiTipoPagoId(),
						tipoPago.getFnMontoPago(), tipoPago.getFnNumeroVales(),
						tipoPago.getImporteAdicional() };
				tiposPago[contador] = new STRUCT(estructuraTiposPago, oconn,
						objeto);
				contador++;
			}
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,
					tiposPago);
			
			cstmt.setString(1, _peticion.getPaTerminal());
			cstmt.setLong(2, _peticion.getPaPaisId());
			cstmt.setLong(3, _peticion.getTiendaId());
			cstmt.setLong(4, _peticion.getPaUsuarioId());
			cstmt.setLong(5, _peticion.getPaUsuarioAutorizaId());
			cstmt.setString(6, _peticion.getPaFechaOper());
			cstmt.setLong(7, _peticion.getPaTipoMovto());
			cstmt.setDouble(8, _peticion.getPaMontoTotalVta());
			cstmt.setARRAY(9, arregloArticulos);
			cstmt.setARRAY(10, arregloTiposPago);
			cstmt.setInt(11, _peticion.getTipoDevolucion());
			cstmt.setLong(12, _peticion.getTransaccionDev());
			cstmt.setLong(13, _peticion.getPaConciliacionId());
			cstmt.setInt(14, _peticion.getFueraLinea());
			cstmt.registerOutParameter(15, OracleTypes.NUMBER);
			cstmt.registerOutParameter(16, OracleTypes.CURSOR);
			cstmt.registerOutParameter(17, OracleTypes.NUMBER);
			cstmt.registerOutParameter(18, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(17)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(18)));
			//respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(17)));
			
			logeo.log("Respuesta:" + respuesta.toString());
			/*
			for (TipoPagoBean tipoPago : _peticion.getTiposPago()) {
				if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
					PeticionMarcadoPagoTarjetaBean marcadoVenta = new PeticionMarcadoPagoTarjetaBean();
					marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION
							.obtenerParametro("venta.proceso.4.id")));
					marcadoVenta
							.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
									.obtenerParametro("venta.proceso.4.estatus.tarjeta.6")));
					marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
					marcadoVenta.setTiendaId(_peticion.getTiendaId());
					marcadoVenta.setPaPagoTarjetaIdBus(tipoPago
							.getPaPagoTarjetaIdBus());
					RespuestaMarcadoPagoTarjetaBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
					if (respuestaMarcado.getPaCdgError() != 0) {
						logeo.log("Resultado del marcado de pago con tarjeta:"
								+ marcadoVenta);
					}
				}
			}*/
			respuesta.setPaTypCursorBlqs(null);
			
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
			Conexion.cerrarConexion(conn);
		}
		return respuesta;
	}
   
	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar la
	 * venta de productos.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws WSExcepcion
	 */
	public RespuestaVentaBean registrarCancelacionVenta(PeticionVentaArticulosBean _peticion, int _tipoDevolucion, long paTransaccionVentaId)
			throws WSExcepcion {
		int ID_FLUJO_PAGATODO = 1;
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		logeo.log("registrar Cancelacion Venta:: Transacci�n :"+paTransaccionVentaId+", tipoDevolucion :: " + _tipoDevolucion + ", Peticion:"+ _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPVENTACONVALIDACION");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

			Object[] articulos = new Object[_peticion.getArticulos().length];
			StructDescriptor estructuraArticulo = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOARTICULOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOARTICULOSORACLE"),
							oconn);
			logeo.log(_peticion.getArticulos().length+" Articulos para la venta");
			int contador = 0;
			for (ArticuloPeticionVentaBean articulo : _peticion.getArticulos()) {
				
				Object[] objeto = new Object[] { articulo.getFiArticuloId(),
						articulo.getFcCdGbBarras(), articulo.getFnCantidad(),
						articulo.getFnPrecio(), articulo.getFnCosto(),
						articulo.getFnDescuento(), articulo.getFnIva(),
					articulo.getFiIepsId()};
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,
						objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,
					articulos);
			
			ArrayList<TipoPagoBean> tiposPagoLista = new ArrayList<TipoPagoBean>();
			StructDescriptor estructuraTiposPago = StructDescriptor
					.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"), oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor.createDescriptor(
					ConfigSION.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"), oconn);
			TipoPagoBean tarjetas = new TipoPagoBean();
			logeo.log(_peticion.getTiposPago().length+" Tipos de pago para la venta");
			for (TipoPagoBean tipoPago : _peticion.getTiposPago()) {
				
				/*if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
					PeticionMarcadoPagoTarjetaBean marcadoVenta = new PeticionMarcadoPagoTarjetaBean();
					marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION
							.obtenerParametro("venta.proceso.4.id")));
					marcadoVenta
							.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
									.obtenerParametro("venta.proceso.4.estatus.tarjeta.5")));
					marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
					marcadoVenta.setTiendaId(_peticion.getTiendaId());
					marcadoVenta.setPaPagoTarjetaIdBus(tipoPago
							.getPaPagoTarjetaIdBus());
					marcadoVenta.setPaComision(tipoPago.getImporteAdicional());
					RespuestaMarcadoPagoTarjetaBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
					if (respuestaMarcado.getPaCdgError() != 0) {
						logeo.log("Resultado del marcado de pago con tarjeta:"
								+ marcadoVenta);
					}
					tarjetas.setFiTipoPagoId(TARJETA_ID);

					tarjetas.setFnMontoPago(tarjetas.getFnMontoPago()
							+ tipoPago.getFnMontoPago());
					tarjetas.setImporteAdicional(tarjetas.getImporteAdicional()
							+ tipoPago.getImporteAdicional());
					tiposPagoLista.remove(tarjetas);
					tiposPagoLista.add(tarjetas);
				} else {*/
					tiposPagoLista.add(tipoPago);
				//}
			}
			contador = 0;
			Object[] tiposPago = new Object[tiposPagoLista.size()];
			for (TipoPagoBean tipoPago : tiposPagoLista) {
				
				logeo.log("tipoPago: " + tipoPago.toString());
				
				Object[] objeto = new Object[] { tipoPago.getFiTipoPagoId(),
						tipoPago.getFnMontoPago(), tipoPago.getFnNumeroVales(),
						tipoPago.getImporteAdicional() };
				tiposPago[contador] = new STRUCT(estructuraTiposPago, oconn,
						objeto);
				contador++;
			}
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,
					tiposPago);
			
			cstmt.setString	(1, _peticion.getPaTerminal());
			cstmt.setLong	(2, _peticion.getPaPaisId());
			cstmt.setLong	(3, _peticion.getTiendaId());
			cstmt.setLong	(4, _peticion.getPaUsuarioId());
			cstmt.setLong	(5, _peticion.getPaUsuarioAutorizaId());
			cstmt.setString	(6, _peticion.getPaFechaOper());
			cstmt.setLong	(7, _peticion.getPaTipoMovto());//4 - Error con PT
			cstmt.setDouble	(8, _peticion.getPaMontoTotalVta());
			cstmt.setARRAY	(9, arregloArticulos);
			cstmt.setARRAY (10, arregloTiposPago);
			cstmt.setInt   (11, _tipoDevolucion);//3 - Timeout o error
			cstmt.setLong  (12, paTransaccionVentaId);
			cstmt.setLong  (13, _peticion.getPaConciliacionId());
			cstmt.setInt   (14, _peticion.getFueraLinea());
			cstmt.registerOutParameter(15, OracleTypes.NUMBER);
			cstmt.registerOutParameter(16, OracleTypes.CURSOR);
			cstmt.registerOutParameter(17, OracleTypes.NUMBER);
			cstmt.registerOutParameter(18, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(17)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(18)));
			//respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(17)));
			
			logeo.log("Respuesta:" + respuesta.toString());
			/*
			for (TipoPagoBean tipoPago : _peticion.getTiposPago()) {
				if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
					PeticionMarcadoPagoTarjetaBean marcadoVenta = new PeticionMarcadoPagoTarjetaBean();
					marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION
							.obtenerParametro("venta.proceso.4.id")));
					marcadoVenta
							.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
									.obtenerParametro("venta.proceso.4.estatus.tarjeta.6")));
					marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
					marcadoVenta.setTiendaId(_peticion.getTiendaId());
					marcadoVenta.setPaPagoTarjetaIdBus(tipoPago
							.getPaPagoTarjetaIdBus());
					RespuestaMarcadoPagoTarjetaBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
					if (respuestaMarcado.getPaCdgError() != 0) {
						logeo.log("Resultado del marcado de pago con tarjeta:"
								+ marcadoVenta);
					}
				}
			}*/
			respuesta.setPaTypCursorBlqs(null);
			
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {			
			Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
			Conexion.cerrarConexion(conn);
		}
		return respuesta;
	}

	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar
	 * una devolucion de productos.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la devolucion.
	 * @throws WSExcepcion
	 */
	public RespuestaVentaBean registrarDevolucion(
			PeticionDevolucionArticulosBean _peticion) throws WSExcepcion {
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		logeo.log("Petici�n de devolucion:"
				+ _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPSERVICIOVENTA");
			conn = obtenerConexion();
			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;
			//oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			Object[] articulos = new Object[_peticion.getArticulos().length];
			StructDescriptor estructuraArticulo = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOARTICULOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOARTICULOSORACLE"),
							oconn);
			int contador = 0;
			for (ArticuloPeticionVentaBean articulo : _peticion.getArticulos()) {
				
				Object[] objeto = new Object[] { articulo.getFiArticuloId(),
						articulo.getFcCdGbBarras(), articulo.getFnCantidad(),
						articulo.getFnPrecio(), articulo.getFnCosto(),
						articulo.getFnDescuento(), articulo.getFnIva(),
						articulo.getFiIepsId()};
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,
						objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,
					articulos);
			Object[] tiposPago = new Object[1];
			StructDescriptor estructuraTiposPago = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"),
							oconn);

			Object[] objeto = new Object[] { 1, _peticion.getPaMontoTotalVta(),
					0, 0 };
			tiposPago[0] = new STRUCT(estructuraTiposPago, oconn, objeto);
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,
					tiposPago);

			cstmt.setString(1, _peticion.getPaTerminal());
			cstmt.setLong(2, _peticion.getPaPaisId());
			cstmt.setLong(3, _peticion.getTiendaId());
			cstmt.setLong(4, _peticion.getPaUsuarioId());
			cstmt.setLong(5, _peticion.getPaUsuarioAutorizaId());
			cstmt.setString(6, _peticion.getPaFechaOper());
			cstmt.setLong(7, _peticion.getPaTipoMovto());
			cstmt.setDouble(8, _peticion.getPaMontoTotalVta());
			cstmt.setARRAY(9, arregloArticulos);
			cstmt.setARRAY(10, arregloTiposPago);
			cstmt.setInt(11, _peticion.getPaTipoDevolucion());
			cstmt.setLong(12, _peticion.getPaTransaccionDev());
			cstmt.setLong(13, _peticion.getPaMovimientoIdDev());
			cstmt.setLong(14, _peticion.getPaConciliacionId());
			cstmt.setInt(15, _peticion.getFueraLinea());
			cstmt.registerOutParameter(16, OracleTypes.NUMBER);
			cstmt.registerOutParameter(17, OracleTypes.CURSOR);
			cstmt.registerOutParameter(18, OracleTypes.NUMBER);
			cstmt.registerOutParameter(19, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(18)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(19)));
			respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(16)));
			logeo.log("Respuesta a la peticion de devolucion:"
					+ respuesta.toString());
			if (respuesta.getPaTransaccionId() == 0
					&& respuesta.getPaCdgError() == 0) {
				respuesta.setPaCdgError(Integer.valueOf(ConfigSION
						.obtenerParametro("transaccion.cero.codigo")));
				respuesta.setPaDescError(ConfigSION
						.obtenerParametro("transaccion.cero.mensaje"));
			}
			if (respuesta.getPaCdgError() == 0) {
				bloqueos = (ResultSet) cstmt.getCursor(17);
				ArrayList<BloqueoBean> bloqueosList = new ArrayList<BloqueoBean>();
				while (bloqueos.next()) {
					BloqueoBean bloqueoBean = new BloqueoBean();
					bloqueoBean.setFiTipoPagoId(bloqueos.getInt(1));
					bloqueoBean.setFiNumAvisos(bloqueos.getInt(2));
					bloqueoBean.setFiAvisosFalt(bloqueos.getInt(3));
					bloqueoBean.setFiEstatusBloqueoId(bloqueos.getInt(4));
					bloqueosList.add(bloqueoBean);
				}
				if (bloqueosList.size() != 0) {
					BloqueoBean[] bloqueosBean = new BloqueoBean[bloqueosList
							.size()];
					bloqueosList.toArray(bloqueosBean);
					respuesta.setPaTypCursorBlqs(bloqueosBean);
				} else {
					respuesta.setPaTypCursorBlqs(null);
				}
			} else {
				respuesta.setPaTypCursorBlqs(null);
			}

		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
			Conexion.cerrarConexion(conn);
			
		}
		return respuesta;
	}

	/*************REIMPRESION DE DEVOLUCIONES*************/
	
	/**			-BORRAR CUANDO ESTE EN GENERICO REIMPRESION PS ANTAD-
	 * M�todo que consulta el detalle de una venta o recarga de tiempo aire que ser� reimpresa por el front de venta
	 * 
	 * 
	 * @param _peticion
	 *            Un objeto de petici�n de reimpresi�n de ticket
	 * @return Un objeto con el detalle del ticket a reimprimir
	 * @throws WSExcepcion
	 */

	public RespuestaTicketReimpresionBean consultarDetalleTicket(
			PeticionTicketReimpresionBean _peticion) throws WSExcepcion {
		if (_peticion != null) {
			RespuestaTicketReimpresionBean respuesta = new RespuestaTicketReimpresionBean();

			logeo.log("Petici�n de reimpresi�n de ticket: " + _peticion.toString());

			Connection conn = null;
			OracleConnection oconn = null;
			OracleCallableStatement cstmt = null;
			ResultSet resultSet = null;
			String sql = null;

			try {
				sql = ConfigSION.obtenerParametro("VENTA.SPREIMPRESIONTICKETVENTA");
				conn = obtenerConexion();
				if(conn.isWrapperFor(OracleConnection.class))
					oconn = conn.unwrap(OracleConnection.class);
				else
					oconn = (OracleConnection) conn;
				cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

				cstmt.setInt(1, _peticion.getPaPaisId());
				cstmt.setLong(2, _peticion.getPaTiendaId());
				cstmt.setLong(3, _peticion.getPaNumTransaccion());
				cstmt.setInt(4, _peticion.getPaParamTipoTrans());
				cstmt.registerOutParameter(5, OracleTypes.NUMBER);
				cstmt.registerOutParameter(6, OracleTypes.NUMBER);
				cstmt.registerOutParameter(7, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(8, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(9, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(10, OracleTypes.CURSOR);
				cstmt.registerOutParameter(11, OracleTypes.CURSOR);
				cstmt.registerOutParameter(12, OracleTypes.CURSOR);
				cstmt.registerOutParameter(13, OracleTypes.NUMBER);
				cstmt.registerOutParameter(14, OracleTypes.NUMBER);
				cstmt.registerOutParameter(15, OracleTypes.NUMBER);
				cstmt.registerOutParameter(16, OracleTypes.NUMBER);
				cstmt.registerOutParameter(17, OracleTypes.VARCHAR);
				cstmt.execute();

				respuesta.setPaMoviminetoId(cstmt.getLong(5));
				respuesta.setFiUsuarioId(cstmt.getLong(6));
				respuesta.setPaNombreUsuario(cstmt.getString(7));
				respuesta.setPaFechaCreacion(cstmt.getString(8));
				respuesta.setPaEstacionTrabajo(cstmt.getString(9));
				respuesta.setPaPermiteTicket(cstmt.getInt(13));
				respuesta.setPaTipoMovimiento(cstmt.getInt(14));
				//respuesta.setPaTipoMovimiento(cstmt.getInt(15));  En este campo va TipoCajaId
				respuesta.setPaCdgError(cstmt.getInt(16));
				respuesta.setPaDescError(cstmt.getString(17));

				if (respuesta.getPaCdgError() == 0) {

					if (respuesta.getPaTipoMovimiento() == 9) {
						// leer cursor articulos
						resultSet = (ResultSet) cstmt.getCursor(10);
						if (resultSet != null) {
							ArrayList<ArticuloReimpresionTicketBean> articulosList = new ArrayList<ArticuloReimpresionTicketBean>();
							while (resultSet.next()) {
								ArticuloReimpresionTicketBean articuloBean = new ArticuloReimpresionTicketBean();
								articuloBean.setFnCantidad(resultSet.getDouble(1));
								articuloBean.setFnPrecio(resultSet.getDouble(2));
								articuloBean.setFnIva(resultSet.getDouble(3));
								articuloBean.setFnDescuento(resultSet.getDouble(4));
								articuloBean.setFiIepsId(resultSet.getInt(5));
								articuloBean.setFcNombreArticulo(resultSet.getString(8));
								articulosList.add(articuloBean);
							}
							if (articulosList.size() != 0) {
								ArticuloReimpresionTicketBean[] articulosArreglo = new ArticuloReimpresionTicketBean[articulosList
										.size()];
								articulosList.toArray(articulosArreglo);
								respuesta.setArticuloReimpresionBeans(articulosArreglo);
							} else {
								respuesta.setArticuloReimpresionBeans(null);
							}

						} else {
							respuesta.setArticuloReimpresionBeans(null);
						}
						respuesta.setArticuloTAReimpresionBeans(null);
						respuesta.setServiciosReimpresionBeans(null);
						respuesta.setArregloArticulosDevolucion(null);
					} else if (respuesta.getPaTipoMovimiento() == 10) {
						// leer cursor articulos tiempo aire
						resultSet = (ResultSet) cstmt.getCursor(10);
						if (resultSet != null) {
							ArrayList<ArticuloTAReimpresionBean> articulosTAList = new ArrayList<ArticuloTAReimpresionBean>();
							while (resultSet.next()) {
								ArticuloTAReimpresionBean articuloTABean = new ArticuloTAReimpresionBean();
								articuloTABean.setNumOperacion(resultSet
										.getLong(1));
								articuloTABean.setNumReferencia(resultSet
										.getLong(2));
								articuloTABean
										.setNumero(resultSet.getString(3));
								articuloTABean.setFiEmpresaId(resultSet
										.getInt(4));
								articuloTABean.setCompania(resultSet
										.getString(5));
								articuloTABean.setFcNombreArticulo(resultSet
										.getString(6));
								articulosTAList.add(articuloTABean);
							}
							if (articulosTAList.size() != 0) {
								ArticuloTAReimpresionBean[] articulosTAArreglo = new ArticuloTAReimpresionBean[articulosTAList
										.size()];
								articulosTAList.toArray(articulosTAArreglo);
								respuesta
										.setArticuloTAReimpresionBeans(articulosTAArreglo);
							} else {
								respuesta.setArticuloTAReimpresionBeans(null);
							}

						} else {
							respuesta.setArticuloTAReimpresionBeans(null);
						}
						respuesta.setArticuloReimpresionBeans(null);
						respuesta.setServiciosReimpresionBeans(null);
						respuesta.setArregloArticulosDevolucion(null);
					}else if(respuesta.getPaTipoMovimiento() == 19){
						//Leer cursor de Servicios
						resultSet = (ResultSet) cstmt.getObject(10);
						if(resultSet != null){
							List<ServicioReimpresionBean> servicios = new ArrayList<ServicioReimpresionBean>();
							ServicioReimpresionBean srv = null;
							while(resultSet.next()){
								srv = new ServicioReimpresionBean();
								srv.setMonto(resultSet.getDouble(1));
								srv.setImpuesto(resultSet.getDouble(2));
								srv.setComision(resultSet.getDouble(3));
								srv.setNumAutorizacion(resultSet.getLong(4));
								srv.setReferencia(resultSet.getString(5));
								srv.setMensaje(resultSet.getString(6));
								srv.setNomArticulo(resultSet.getString(7));
								
								//cambio antad - quitar folio XCD de la referencia
								if(srv.getReferencia().contains("|||")){
									
									String[] cadenaReferenciaFolioXCD = srv.getReferencia().split("\\|\\|\\|");
									String referencia = cadenaReferenciaFolioXCD[0];
									
									srv.setReferencia(referencia);
								}
								
								servicios.add(srv);
							}
	
							if(servicios.size() != 0)
								respuesta.setServiciosReimpresionBeans(servicios.toArray(new ServicioReimpresionBean[0]));
							else
								respuesta.setServiciosReimpresionBeans(null);
						}else {
							respuesta.setServiciosReimpresionBeans(null);
						}
						respuesta.setArticuloReimpresionBeans(null);
						respuesta.setArticuloTAReimpresionBeans(null);
						respuesta.setArregloArticulosDevolucion(null);
					}else if(respuesta.getPaTipoMovimiento() == 58){
						resultSet = (ResultSet) cstmt.getObject(10);
						if(resultSet != null){
							List<ArticuloDevReimpresionBean> devoluciones = new ArrayList<ArticuloDevReimpresionBean>();
							ArticuloDevReimpresionBean dev = null;
							while(resultSet.next()){
								dev = new ArticuloDevReimpresionBean();
								dev.setPrecio(resultSet.getDouble(1));
								dev.setCantidad(resultSet.getDouble(2));
								dev.setIva(resultSet.getDouble(3));
								dev.setIepsId(resultSet.getInt(4));
								dev.setNombre(resultSet.getString(5));
								devoluciones.add(dev);
							}
							if(devoluciones.size() != 0){
								respuesta.setArregloArticulosDevolucion(devoluciones.toArray(new ArticuloDevReimpresionBean[0]));
							}
							else{
								respuesta.setArregloArticulosDevolucion(null);
							}
						}else{
							respuesta.setArregloArticulosDevolucion(null);
						}
						respuesta.setArticuloReimpresionBeans(null);
						respuesta.setArticuloTAReimpresionBeans(null);
						respuesta.setServiciosReimpresionBeans(null);
					}

					// leer cursor tipos de pago
					boolean esPagoConTarjeta = false;
					resultSet = (ResultSet) cstmt.getCursor(11);
					if (resultSet != null) {
						ArrayList<TipoPagoReimpresionBean> pagosList = new ArrayList<TipoPagoReimpresionBean>();
						while (resultSet.next()) {
							TipoPagoReimpresionBean pagoBean = new TipoPagoReimpresionBean();
							pagoBean.setFiTipoPagoId(resultSet.getInt(1));
							pagoBean.setMontoPago(resultSet.getDouble(2));
							pagoBean.setFnMontoRecibido(resultSet.getDouble(3));

							if (pagoBean.getFiTipoPagoId() == 2) {
								esPagoConTarjeta = true;
							}

							pagosList.add(pagoBean);

						}
						if (pagosList.size() != 0) {
							TipoPagoReimpresionBean[] pagosArreglo = new TipoPagoReimpresionBean[pagosList
									.size()];
							pagosList.toArray(pagosArreglo);
							respuesta.setTipoPagoReimpresionBeans(pagosArreglo);
						} else {
							respuesta.setTipoPagoReimpresionBeans(null);
						}

					} else {
						respuesta.setTipoPagoReimpresionBeans(null);
					}

					if (esPagoConTarjeta) {
						// leer cursor tarjetas
						resultSet = (ResultSet) cstmt.getCursor(12);
						if (resultSet != null) {
							ArrayList<TarjetaReimpresionBean> tarjetasList = new ArrayList<TarjetaReimpresionBean>();
							while (resultSet.next()) {
								TarjetaReimpresionBean tarjetaBean = new TarjetaReimpresionBean();
								tarjetaBean.setFiPagoTarjetasId(resultSet
										.getLong(1));
								tarjetaBean.setImporteComision(resultSet
										.getDouble(2));
								tarjetaBean.setFcNumeroTarjeta(resultSet
										.getString(3));
								tarjetaBean.setFcNumeroAutorizacion(resultSet
										.getString(4));
								tarjetaBean.setFiAfiliacion(resultSet
										.getLong(5));
								tarjetaBean.setFcDescripcion(resultSet
										.getString(6));
								tarjetasList.add(tarjetaBean);
							}
							if (tarjetasList.size() != 0) {
								TarjetaReimpresionBean[] tarjetasArreglo = new TarjetaReimpresionBean[tarjetasList
										.size()];
								tarjetasList.toArray(tarjetasArreglo);
								respuesta
										.setTarjetaReimpresionBeans(tarjetasArreglo);
							} else {
								respuesta.setTarjetaReimpresionBeans(null);
							}

						} else {
							respuesta.setTarjetaReimpresionBeans(null);
						}
					} else {
						respuesta.setTarjetaReimpresionBeans(null);
					}

				}else{
					respuesta.setArticuloReimpresionBeans(null);
					respuesta.setArticuloTAReimpresionBeans(null);
					respuesta.setTarjetaReimpresionBeans(null);
					respuesta.setTipoPagoReimpresionBeans(null);
					respuesta.setServiciosReimpresionBeans(null);
				}

				logeo.log("Respuesta de reimpresi�n de ticket: " + respuesta.toString());

			} catch (Exception e) {
				respuesta.setTipoPagoReimpresionBeans(null);
				respuesta.setArticuloReimpresionBeans(null);
				respuesta.setTarjetaReimpresionBeans(null);
				respuesta.setServiciosReimpresionBeans(null);
				logeo.logearExcepcion(e, sql);
				throw new WSExcepcion(e.getLocalizedMessage(), e);
			} finally {
				Conexion.cerrarStatement(cstmt);
				Conexion.cerrarResultSet(resultSet);
				Conexion.cerrarConexion(oconn);
				Conexion.cerrarConexion(conn);
			}

			return respuesta;
		} else {
			return null;
		}
			
	}
	
	/*REIMPRESION DE PS ANTAD*/
	
	public RespuestaReimpresionBean consultarTicket(
			PeticionTicketReimpresionBean _peticion) throws WSExcepcion {
		if (_peticion != null) {
			RespuestaReimpresionBean respuesta = new RespuestaReimpresionBean();

			logeo.log("Petici�n de reimpresi�n de ticket: " + _peticion.toString());

			Connection conn = null;
			OracleConnection oconn = null;
			OracleCallableStatement cstmt = null;
			ResultSet resultSet = null;
			String sql = null;

			try {
				sql = ConfigSION.obtenerParametro("VENTA.SPREIMPRESIONTICKETVENTA");
				conn = obtenerConexion();
				if(conn.isWrapperFor(OracleConnection.class))
					oconn = conn.unwrap(OracleConnection.class);
				else
					oconn = (OracleConnection) conn;
				cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

				cstmt.setInt(1, _peticion.getPaPaisId());
				cstmt.setLong(2, _peticion.getPaTiendaId());
				cstmt.setLong(3, _peticion.getPaNumTransaccion());
				cstmt.setInt(4, _peticion.getPaParamTipoTrans());
				cstmt.registerOutParameter(5, OracleTypes.NUMBER);
				cstmt.registerOutParameter(6, OracleTypes.NUMBER);
				cstmt.registerOutParameter(7, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(8, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(9, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(10, OracleTypes.CURSOR);
				cstmt.registerOutParameter(11, OracleTypes.CURSOR);
				cstmt.registerOutParameter(12, OracleTypes.CURSOR);
				cstmt.registerOutParameter(13, OracleTypes.NUMBER);
				cstmt.registerOutParameter(14, OracleTypes.NUMBER);
				cstmt.registerOutParameter(15, OracleTypes.NUMBER);
				cstmt.registerOutParameter(16, OracleTypes.NUMBER);
				cstmt.registerOutParameter(17, OracleTypes.VARCHAR);
				cstmt.execute();

				respuesta.setPaMoviminetoId(cstmt.getLong(5));
				respuesta.setFiUsuarioId(cstmt.getLong(6));
				respuesta.setPaNombreUsuario(cstmt.getString(7));
				respuesta.setPaFechaCreacion(cstmt.getString(8));
				respuesta.setPaEstacionTrabajo(cstmt.getString(9));
				respuesta.setPaPermiteTicket(cstmt.getInt(13));
				respuesta.setPaTipoMovimiento(cstmt.getInt(14));
				//respuesta.setPaTipoMovimiento(cstmt.getInt(15));  En este campo va TipoCajaId
				respuesta.setPaCdgError(cstmt.getInt(16));
				respuesta.setPaDescError(cstmt.getString(17));

				if (respuesta.getPaCdgError() == 0) {

					if (respuesta.getPaTipoMovimiento() == 9) {
						// leer cursor articulos
						resultSet = (ResultSet) cstmt.getCursor(10);
						if (resultSet != null) {
							ArrayList<ArticuloReimpresionTicketBean> articulosList = new ArrayList<ArticuloReimpresionTicketBean>();
							while (resultSet.next()) {
								ArticuloReimpresionTicketBean articuloBean = new ArticuloReimpresionTicketBean();
								articuloBean.setFnCantidad(resultSet.getDouble(1));
								articuloBean.setFnPrecio(resultSet.getDouble(2));
								articuloBean.setFnIva(resultSet.getDouble(3));
								articuloBean.setFnDescuento(resultSet.getDouble(4));
								articuloBean.setFiIepsId(resultSet.getInt(5));
								articuloBean.setFcNombreArticulo(resultSet.getString(8));
								articulosList.add(articuloBean);
							}
							if (articulosList.size() != 0) {
								ArticuloReimpresionTicketBean[] articulosArreglo = new ArticuloReimpresionTicketBean[articulosList
										.size()];
								articulosList.toArray(articulosArreglo);
								respuesta.setArticuloReimpresionBeans(articulosArreglo);
							} else {
								respuesta.setArticuloReimpresionBeans(null);
							}

						} else {
							respuesta.setArticuloReimpresionBeans(null);
						}
						respuesta.setArticuloTAReimpresionBeans(null);
						respuesta.setServiciosReimpresionBeans(null);
						respuesta.setArregloArticulosDevolucion(null);
					} else if (respuesta.getPaTipoMovimiento() == 10) {
						// leer cursor articulos tiempo aire
						resultSet = (ResultSet) cstmt.getCursor(10);
						if (resultSet != null) {
							ArrayList<ArticuloTAReimpresionBean> articulosTAList = new ArrayList<ArticuloTAReimpresionBean>();
							while (resultSet.next()) {
								ArticuloTAReimpresionBean articuloTABean = new ArticuloTAReimpresionBean();
								articuloTABean.setNumOperacion(resultSet
										.getLong(1));
								articuloTABean.setNumReferencia(resultSet
										.getLong(2));
								articuloTABean
										.setNumero(resultSet.getString(3));
								articuloTABean.setFiEmpresaId(resultSet
										.getInt(4));
								articuloTABean.setCompania(resultSet
										.getString(5));
								articuloTABean.setFcNombreArticulo(resultSet
										.getString(6));
								articulosTAList.add(articuloTABean);
							}
							if (articulosTAList.size() != 0) {
								ArticuloTAReimpresionBean[] articulosTAArreglo = new ArticuloTAReimpresionBean[articulosTAList
										.size()];
								articulosTAList.toArray(articulosTAArreglo);
								respuesta
										.setArticuloTAReimpresionBeans(articulosTAArreglo);
							} else {
								respuesta.setArticuloTAReimpresionBeans(null);
							}

						} else {
							respuesta.setArticuloTAReimpresionBeans(null);
						}
						respuesta.setArticuloReimpresionBeans(null);
						respuesta.setServiciosReimpresionBeans(null);
						respuesta.setArregloArticulosDevolucion(null);
					}else if(respuesta.getPaTipoMovimiento() == 19){
						//Leer cursor de Servicios
						resultSet = (ResultSet) cstmt.getObject(10);
						if(resultSet != null){
							List<ServicioReimpresionBean> servicios = new ArrayList<ServicioReimpresionBean>();
							ServicioReimpresionBean srv = null;
							while(resultSet.next()){
								srv = new ServicioReimpresionBean();
								srv.setMonto(resultSet.getDouble(1));
								srv.setImpuesto(resultSet.getDouble(2));
								srv.setComision(resultSet.getDouble(3));
								srv.setNumAutorizacion(resultSet.getLong(4));
								srv.setReferencia(resultSet.getString(5));
								srv.setMensaje(resultSet.getString(6));
								srv.setNomArticulo(resultSet.getString(7));
								
								servicios.add(srv);
							}
							if(servicios.size() != 0)
								respuesta.setServiciosReimpresionBeans(servicios.toArray(new ServicioReimpresionBean[0]));
							else
								respuesta.setServiciosReimpresionBeans(null);
						}else {
							respuesta.setServiciosReimpresionBeans(null);
						}
						respuesta.setArticuloReimpresionBeans(null);
						respuesta.setArticuloTAReimpresionBeans(null);
						respuesta.setArregloArticulosDevolucion(null);
					}else if(respuesta.getPaTipoMovimiento() == 58){
						resultSet = (ResultSet) cstmt.getObject(10);
						if(resultSet != null){
							List<ArticuloDevReimpresionBean> devoluciones = new ArrayList<ArticuloDevReimpresionBean>();
							ArticuloDevReimpresionBean dev = null;
							while(resultSet.next()){
								dev = new ArticuloDevReimpresionBean();
								dev.setPrecio(resultSet.getDouble(1));
								dev.setCantidad(resultSet.getDouble(2));
								dev.setIva(resultSet.getDouble(3));
								dev.setIepsId(resultSet.getInt(4));
								dev.setNombre(resultSet.getString(5));
								devoluciones.add(dev);
							}
							if(devoluciones.size() != 0){
								respuesta.setArregloArticulosDevolucion(devoluciones.toArray(new ArticuloDevReimpresionBean[0]));
							}
							else{
								respuesta.setArregloArticulosDevolucion(null);
							}
						}else{
							respuesta.setArregloArticulosDevolucion(null);
						}
						respuesta.setArticuloReimpresionBeans(null);
						respuesta.setArticuloTAReimpresionBeans(null);
						respuesta.setServiciosReimpresionBeans(null);
					}

					// leer cursor tipos de pago
					boolean esPagoConTarjeta = false;
					resultSet = (ResultSet) cstmt.getCursor(11);
					if (resultSet != null) {
						ArrayList<TipoPagoReimpresionBean> pagosList = new ArrayList<TipoPagoReimpresionBean>();
						while (resultSet.next()) {
							TipoPagoReimpresionBean pagoBean = new TipoPagoReimpresionBean();
							pagoBean.setFiTipoPagoId(resultSet.getInt(1));
							pagoBean.setMontoPago(resultSet.getDouble(2));
							pagoBean.setFnMontoRecibido(resultSet.getDouble(3));

							if (pagoBean.getFiTipoPagoId() == 2) {
								esPagoConTarjeta = true;
							}

							pagosList.add(pagoBean);

						}
						if (pagosList.size() != 0) {
							TipoPagoReimpresionBean[] pagosArreglo = new TipoPagoReimpresionBean[pagosList
									.size()];
							pagosList.toArray(pagosArreglo);
							respuesta.setTipoPagoReimpresionBeans(pagosArreglo);
						} else {
							respuesta.setTipoPagoReimpresionBeans(null);
						}

					} else {
						respuesta.setTipoPagoReimpresionBeans(null);
					}

					if (esPagoConTarjeta) {
						// leer cursor tarjetas
						resultSet = (ResultSet) cstmt.getCursor(12);
						if (resultSet != null) {
							ArrayList<TarjetaReimpresionBean> tarjetasList = new ArrayList<TarjetaReimpresionBean>();
							while (resultSet.next()) {
								TarjetaReimpresionBean tarjetaBean = new TarjetaReimpresionBean();
								tarjetaBean.setFiPagoTarjetasId(resultSet
										.getLong(1));
								tarjetaBean.setImporteComision(resultSet
										.getDouble(2));
								tarjetaBean.setFcNumeroTarjeta(resultSet
										.getString(3));
								tarjetaBean.setFcNumeroAutorizacion(resultSet
										.getString(4));
								tarjetaBean.setFiAfiliacion(resultSet
										.getLong(5));
								tarjetaBean.setFcDescripcion(resultSet
										.getString(6));
								tarjetasList.add(tarjetaBean);
							}
							if (tarjetasList.size() != 0) {
								TarjetaReimpresionBean[] tarjetasArreglo = new TarjetaReimpresionBean[tarjetasList
										.size()];
								tarjetasList.toArray(tarjetasArreglo);
								respuesta
										.setTarjetaReimpresionBeans(tarjetasArreglo);
							} else {
								respuesta.setTarjetaReimpresionBeans(null);
							}

						} else {
							respuesta.setTarjetaReimpresionBeans(null);
						}
					} else {
						respuesta.setTarjetaReimpresionBeans(null);
					}

				}else{
					respuesta.setArticuloReimpresionBeans(null);
					respuesta.setArticuloTAReimpresionBeans(null);
					respuesta.setTarjetaReimpresionBeans(null);
					respuesta.setTipoPagoReimpresionBeans(null);
					respuesta.setServiciosReimpresionBeans(null);
				}

				logeo.log("Respuesta de reimpresi�n de ticket: " + respuesta.toString());

			} catch (Exception e) {
				respuesta.setTipoPagoReimpresionBeans(null);
				respuesta.setArticuloReimpresionBeans(null);
				respuesta.setTarjetaReimpresionBeans(null);
				respuesta.setServiciosReimpresionBeans(null);
				logeo.logearExcepcion(e, sql);
				throw new WSExcepcion(e.getLocalizedMessage(), e);
			} finally {
				Conexion.cerrarResultSet(resultSet);
				Conexion.cerrarStatement(cstmt);
				Conexion.cerrarConexion(conn);
				Conexion.cerrarConexion(oconn);
			}

			return respuesta;
		} else {
			return null;
		}
			
	}
	
	
}
