package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import java.util.Arrays;

public class RespuestaTarjetaBean {
	
	private PeticionTarjetaBean peticionTarjetaBean;
	private TarjetaBean[] tarjetaBeans;
	private int cdgError;
	private String descError;
		
	public TarjetaBean[] getTarjetaBeans() {
		return tarjetaBeans;
	}
	public void setTarjetaBeans(TarjetaBean[] tarjetaBeans) {
		this.tarjetaBeans = tarjetaBeans;
	}
	public int getCdgError() {
		return cdgError;
	}
	public void setCdgError(int cdgError) {
		this.cdgError = cdgError;
	}
	public String getDescError() {
		return descError;
	}
	public void setDescError(String descError) {
		this.descError = descError;
	}
	
	public PeticionTarjetaBean getPeticionTarjetaBean() {
		return peticionTarjetaBean;
	}
	public void setPeticionTarjetaBean(PeticionTarjetaBean peticionTarjetaBean) {
		this.peticionTarjetaBean = peticionTarjetaBean;
	}
	
	@Override
	public String toString() {
		return "RespuestaTarjetaBean [peticionTarjetaBean="
				+ peticionTarjetaBean + ", tarjetaBeans="
				+ Arrays.toString(tarjetaBeans) + ", cdgError=" + cdgError
				+ ", descError=" + descError + "]";
	}
	
	
	
	
	
	
	
	
}
