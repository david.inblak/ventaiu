package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

public class ArticuloFacturacionBean {

	private double fnCantidad;
	private double fnPrecio;
	private double fnIva;
	private double fnDescuento;
	private String fcCdgBarras;
	private long fiArticuloId;
	private String fcNombreArticulo;
	private int fiIepsId;

	public double getFnCantidad() {
		return fnCantidad;
	}

	public void setFnCantidad(double fnCantidad) {
		this.fnCantidad = fnCantidad;
	}

	public double getFnPrecio() {
		return fnPrecio;
	}

	public void setFnPrecio(double fnPrecio) {
		this.fnPrecio = fnPrecio;
	}

	public double getFnIva() {
		return fnIva;
	}

	public void setFnIva(double fnIva) {
		this.fnIva = fnIva;
	}

	public double getFnDescuento() {
		return fnDescuento;
	}

	public void setFnDescuento(double fnDescuento) {
		this.fnDescuento = fnDescuento;
	}

	public String getFcCdgBarras() {
		return fcCdgBarras;
	}

	public void setFcCdgBarras(String fcCdgBarras) {
		this.fcCdgBarras = fcCdgBarras;
	}

	public long getFiArticuloId() {
		return fiArticuloId;
	}

	public void setFiArticuloId(long fiArticuloId) {
		this.fiArticuloId = fiArticuloId;
	}

	public String getFcNombreArticulo() {
		return fcNombreArticulo;
	}

	public void setFcNombreArticulo(String fcNombreArticulo) {
		this.fcNombreArticulo = fcNombreArticulo;
	}

	public int getFiIepsId() {
		return fiIepsId;
	}

	public void setFiIepsId(int fiIepsId) {
		this.fiIepsId = fiIepsId;
	}

	@Override
	public String toString() {
		return "ArticuloFacturacionBean [fnCantidad=" + fnCantidad
				+ ", fnPrecio=" + fnPrecio + ", fnIva=" + fnIva
				+ ", fnDescuento=" + fnDescuento + ", fcCdgBarras="
				+ fcCdgBarras + ", fiArticuloId=" + fiArticuloId
				+ ", fcNombreArticulo=" + fcNombreArticulo + ", fiIepsId="
				+ fiIepsId + "]";
	}

}
