package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import java.util.Arrays;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

public class PeticionVentaMixtaBean extends PeticionBaseVentaBean {
	private String paTerminal;
	private long paUsuarioId;
	private String paFechaOper;
	private int paTipoMovto;
	private double paMontoTotalVta;
	private ArticuloBean[] articulos;
	private TipoPagoBean[] tiposPago;
	private int fueraLinea;
	private PeticionVentaTABean[] recargasTA;

	public String getPaTerminal() {
		return paTerminal;
	}

	public void setPaTerminal(String paTerminal) {
		this.paTerminal = paTerminal;
	}

	public long getPaUsuarioId() {
		return paUsuarioId;
	}

	public void setPaUsuarioId(long paUsuarioId) {
		this.paUsuarioId = paUsuarioId;
	}

	public String getPaFechaOper() {
		return paFechaOper;
	}

	public void setPaFechaOper(String paFechaOper) {
		this.paFechaOper = paFechaOper;
	}

	public int getPaTipoMovto() {
		return paTipoMovto;
	}

	public void setPaTipoMovto(int paTipoMovto) {
		this.paTipoMovto = paTipoMovto;
	}

	public double getPaMontoTotalVta() {
		return paMontoTotalVta;
	}

	public void setPaMontoTotalVta(double paMontoTotalVta) {
		this.paMontoTotalVta = paMontoTotalVta;
	}

	public ArticuloBean[] getArticulos() {
		return articulos;
	}

	public void setArticulos(ArticuloBean[] articulos) {
		this.articulos = articulos;
	}

	public TipoPagoBean[] getTiposPago() {
		return tiposPago;
	}

	public void setTiposPago(TipoPagoBean[] tiposPago) {
		this.tiposPago = tiposPago;
	}

	public int getFueraLinea() {
		return fueraLinea;
	}

	public void setFueraLinea(int fueraLinea) {
		this.fueraLinea = fueraLinea;
	}

	public PeticionVentaTABean[] getRecargasTA() {
		return recargasTA;
	}

	public void setRecargasTA(PeticionVentaTABean[] recargasTA) {
		this.recargasTA = recargasTA;
	}

	@Override
	public String toString() {
		return "PeticionVentaMixtaBean [paTerminal=" + paTerminal
				+ ", paUsuarioId=" + paUsuarioId + ", paFechaOper="
				+ paFechaOper + ", paTipoMovto=" + paTipoMovto
				+ ", paMontoTotalVta=" + paMontoTotalVta + ", articulos="
				+ Arrays.toString(articulos) + ", tiposPago="
				+ Arrays.toString(tiposPago) + ", fueraLinea=" + fueraLinea
				+ ", recargasTA=" + Arrays.toString(recargasTA) + "]";
	}

}
