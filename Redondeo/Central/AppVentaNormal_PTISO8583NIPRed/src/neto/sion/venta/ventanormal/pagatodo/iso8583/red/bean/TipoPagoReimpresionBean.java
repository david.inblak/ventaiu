package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

public class TipoPagoReimpresionBean {

	private int fiTipoPagoId;
	private double montoPago;
	private double fnMontoRecibido;

	public int getFiTipoPagoId() {
		return fiTipoPagoId;
	}

	public void setFiTipoPagoId(int fiTipoPagoId) {
		this.fiTipoPagoId = fiTipoPagoId;
	}

	public double getMontoPago() {
		return montoPago;
	}

	public void setMontoPago(double montoPago) {
		this.montoPago = montoPago;
	}

	public double getFnMontoRecibido() {
		return fnMontoRecibido;
	}

	public void setFnMontoRecibido(double fnMontoRecibido) {
		this.fnMontoRecibido = fnMontoRecibido;
	}

	@Override
	public String toString() {
		return "TipoPagoReimpresionBean [fiTipoPagoId=" + fiTipoPagoId
				+ ", montoPago=" + montoPago + ", fnMontoRecibido="
				+ fnMontoRecibido + "]";
	}

}
