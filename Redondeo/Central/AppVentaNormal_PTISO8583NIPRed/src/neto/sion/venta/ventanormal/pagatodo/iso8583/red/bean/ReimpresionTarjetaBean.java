package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

/***
 * Clase que representa los datos de reimpresion para una venta con tarjetas
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class ReimpresionTarjetaBean {
	private long fiPagoTarjetasId;
	private double importeComision;
	private String fcNumTarjeta;
	private String fcNumeroAutorizacion;
	private int fiAfiliacion;
	private String tipoTarjeta;

	public long getFiPagoTarjetasId() {
		return fiPagoTarjetasId;
	}

	public void setFiPagoTarjetasId(long fiPagoTarjetasId) {
		this.fiPagoTarjetasId = fiPagoTarjetasId;
	}

	public double getImporteComision() {
		return importeComision;
	}

	public void setImporteComision(double importeComision) {
		this.importeComision = importeComision;
	}

	public String getFcNumTarjeta() {
		return fcNumTarjeta;
	}

	public void setFcNumTarjeta(String fcNumTarjeta) {
		this.fcNumTarjeta = fcNumTarjeta;
	}

	public String getFcNumeroAutorizacion() {
		return fcNumeroAutorizacion;
	}

	public void setFcNumeroAutorizacion(String fcNumeroAutorizacion) {
		this.fcNumeroAutorizacion = fcNumeroAutorizacion;
	}

	public int getFiAfiliacion() {
		return fiAfiliacion;
	}

	public void setFiAfiliacion(int fiAfiliacion) {
		this.fiAfiliacion = fiAfiliacion;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	@Override
	public String toString() {
		return "ReimpresionTarjetaBean [fiPagoTarjetasId=" + fiPagoTarjetasId
				+ ", importeComision=" + importeComision + ", fcNumTarjeta="
				+ fcNumTarjeta + ", fcNumeroAutorizacion="
				+ fcNumeroAutorizacion + ", fiAfiliacion=" + fiAfiliacion
				+ ", tipoTarjeta=" + tipoTarjeta + "]";
	}

}
