package neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

/***
 * Clase que representa los datos necesarios para una consulta de transaccion en
 * central
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class PeticionTransaccionCentralBean extends PeticionBaseVentaBean {

	private static final long serialVersionUID = 1L;
	
	private long paUsuarioId;
	private int paSistemaId;
	private int paModuloId;
	private int paSubModuloId;

	public long getPaUsuarioId() {
		return paUsuarioId;
	}

	public void setPaUsuarioId(long paUsuarioId) {
		this.paUsuarioId = paUsuarioId;
	}

	public int getPaSistemaId() {
		return paSistemaId;
	}

	public void setPaSistemaId(int paSistemaId) {
		this.paSistemaId = paSistemaId;
	}

	public int getPaModuloId() {
		return paModuloId;
	}

	public void setPaModuloId(int paModuloId) {
		this.paModuloId = paModuloId;
	}

	public int getPaSubModuloId() {
		return paSubModuloId;
	}

	public void setPaSubModuloId(int paSubModuloId) {
		this.paSubModuloId = paSubModuloId;
	}

	@Override
	public String toString() {
		return "PeticionTransaccionCentralBean [paUsuarioId=" + paUsuarioId
				+ ", paSistemaId=" + paSistemaId + ", paModuloId=" + paModuloId
				+ ", paSubModuloId=" + paSubModuloId + super.toString() + "]";
	}

}
