package neto.sion.venta.fueralineared.bean;

import java.util.Arrays;

public class PeticionVentaCanceladaBean {

	private int paPaisId;
	private long paTiendaId;
	private long paConciliacionId;
	private long paUsuarioId;
	private int paSistemaId;
	private int paModuloId;
	private int paSubModuloId;
	private VentaCanceladaBean[] ventaCanceladaBeans;

	public int getPaPaisId() {
		return paPaisId;
	}

	public void setPaPaisId(int paPaisId) {
		this.paPaisId = paPaisId;
	}

	public long getPaTiendaId() {
		return paTiendaId;
	}

	public void setPaTiendaId(long paTiendaId) {
		this.paTiendaId = paTiendaId;
	}

	public long getPaConciliacionId() {
		return paConciliacionId;
	}

	public void setPaConciliacionId(long paConciliacionId) {
		this.paConciliacionId = paConciliacionId;
	}

	public long getPaUsuarioId() {
		return paUsuarioId;
	}

	public void setPaUsuarioId(long paUsuarioId) {
		this.paUsuarioId = paUsuarioId;
	}

	public int getPaSistemaId() {
		return paSistemaId;
	}

	public void setPaSistemaId(int paSistemaId) {
		this.paSistemaId = paSistemaId;
	}

	public int getPaModuloId() {
		return paModuloId;
	}

	public void setPaModuloId(int paModuloId) {
		this.paModuloId = paModuloId;
	}

	public int getPaSubModuloId() {
		return paSubModuloId;
	}

	public void setPaSubModuloId(int paSubModuloId) {
		this.paSubModuloId = paSubModuloId;
	}

	public VentaCanceladaBean[] getVentaCanceladaBeans() {
		return ventaCanceladaBeans;
	}

	public void setVentaCanceladaBeans(VentaCanceladaBean[] ventaCanceladaBeans) {
		this.ventaCanceladaBeans = ventaCanceladaBeans;
	}

	@Override
	public String toString() {
		return "PeticionVentaCanceladaBean [paPaisId=" + paPaisId
				+ ", paTiendaId=" + paTiendaId + ", paConciliacionId="
				+ paConciliacionId + ", paUsuarioId=" + paUsuarioId
				+ ", paSistemaId=" + paSistemaId + ", paModuloId=" + paModuloId
				+ ", paSubModuloId=" + paSubModuloId + ", ventaCanceladaBeans="
				+ Arrays.toString(ventaCanceladaBeans) + "]";
	}

}
