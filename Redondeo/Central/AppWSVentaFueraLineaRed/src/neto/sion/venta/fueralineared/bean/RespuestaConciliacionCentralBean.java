package neto.sion.venta.fueralineared.bean;

import java.util.Arrays;

/***
 * Clase que representa la respuesta de la llamada al store de insercion de
 * conciliaciones pendientes
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class RespuestaConciliacionCentralBean {
	private int paCdgError;
	private String paDescError;
	private ConciliacionLocalBean[] curConciliacionesLocal;

	public int getPaCdgError() {
		return paCdgError;
	}

	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}

	public String getPaDescError() {
		return paDescError;
	}

	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}

	public ConciliacionLocalBean[] getCurConciliacionesLocal() {
		return curConciliacionesLocal;
	}

	public void setCurConciliacionesLocal(
			ConciliacionLocalBean[] curConciliacionesLocal) {
		this.curConciliacionesLocal = curConciliacionesLocal;
	}

	@Override
	public String toString() {
		return "RespuestaConciliacionCentralBean [paCdgError=" + paCdgError
				+ ", paDescError=" + paDescError + ", curConciliacionesLocal="
				+ Arrays.toString(curConciliacionesLocal) + "]";
	}

}
