package neto.sion.venta.fueralineared.bean;

public class RespuestaInsercionLineasBean {
	private int cgdError;
	private String descError;

	public int getCgdError() {
		return cgdError;
	}

	public void setCgdError(int cgdError) {
		this.cgdError = cgdError;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	@Override
	public String toString() {
		return "RespuestaInsercionLineasBean [cgdError=" + cgdError
				+ ", descError=" + descError + "]";
	}

}
