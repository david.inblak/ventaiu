package neto.sion.venta.fueralineared.bean;

import java.util.Arrays;

/***
 * Clase que representa la respuesta de una venta.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class RespuestaVentaFLBloqueoBean {
	private long paTransaccionId;
	private BloqueoBean[] bloqueos;
	private int paCdgError;
	private String paDescError;

	public long getPaTransaccionId() {
		return paTransaccionId;
	}

	public void setPaTransaccionId(long paTransaccionId) {
		this.paTransaccionId = paTransaccionId;
	}

	public int getPaCdgError() {
		return paCdgError;
	}

	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}

	public String getPaDescError() {
		return paDescError;
	}

	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}

	public BloqueoBean[] getBloqueos() {
		return bloqueos;
	}

	public void setBloqueos(BloqueoBean[] bloqueos) {
		this.bloqueos = bloqueos;
	}

	@Override
	public String toString() {
		return "RespuestaVentaFLBloqueoBean [paTransaccionId="
				+ paTransaccionId + ", bloqueos=" + Arrays.toString(bloqueos)
				+ ", paCdgError=" + paCdgError + ", paDescError=" + paDescError
				+ "]";
	}
	
}
