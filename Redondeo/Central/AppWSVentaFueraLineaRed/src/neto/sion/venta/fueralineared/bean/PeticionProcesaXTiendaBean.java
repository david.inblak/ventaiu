package neto.sion.venta.fueralineared.bean;

public class PeticionProcesaXTiendaBean {
	
	private String Fecha;
	private long Tiendaid;
	
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		Fecha = fecha;
	}
	public long getTiendaid() {
		return Tiendaid;
	}
	public void setTiendaid(long tiendaid) {
		Tiendaid = tiendaid;
	}
	@Override
	public String toString() {
		return "PeticionProcesaXTiendaBean [Fecha=" + Fecha + ", Tiendaid="
				+ Tiendaid + "]";
	}

	
}
