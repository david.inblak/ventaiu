package neto.sion.venta.fueralineared.bean;

/***
 * Clase que representa la respuesta de una venta.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class RespuestaVentaFLBean {
	private long paTransaccionId;
	private int paCdgError;
	private String paDescError;

	public long getPaTransaccionId() {
		return paTransaccionId;
	}

	public void setPaTransaccionId(long paTransaccionId) {
		this.paTransaccionId = paTransaccionId;
	}

	public int getPaCdgError() {
		return paCdgError;
	}

	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}

	public String getPaDescError() {
		return paDescError;
	}

	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}

	@Override
	public String toString() {
		return "RespuestaVentaFLBean [paTransaccionId=" + paTransaccionId
				+ ", paCdgError=" + paCdgError + ", paDescError=" + paDescError
				+ "]";
	}

}
