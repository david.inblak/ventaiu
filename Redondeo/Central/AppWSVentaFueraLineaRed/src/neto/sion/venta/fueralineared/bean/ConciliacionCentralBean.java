package neto.sion.venta.fueralineared.bean;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

/****
 * Clase que representa el objeto de oracle ConciliacionLocal
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class ConciliacionCentralBean extends PeticionBaseVentaBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4903986079494222139L;
	private int fiSistemaId;
	private int fiModuloId;
	private int fiSubModuloId;
	private long fiNumTransaccionId;
	private long fiUsuarioId;
	private int fiEstatusConciliacionId;

	public int getFiSistemaId() {
		return fiSistemaId;
	}

	public void setFiSistemaId(int fiSistemaId) {
		this.fiSistemaId = fiSistemaId;
	}

	public int getFiModuloId() {
		return fiModuloId;
	}

	public void setFiModuloId(int fiModuloId) {
		this.fiModuloId = fiModuloId;
	}

	public int getFiSubModuloId() {
		return fiSubModuloId;
	}

	public void setFiSubModuloId(int fiSubModuloId) {
		this.fiSubModuloId = fiSubModuloId;
	}

	public long getFiNumTransaccionId() {
		return fiNumTransaccionId;
	}

	public void setFiNumTransaccionId(long fiNumTransaccionId) {
		this.fiNumTransaccionId = fiNumTransaccionId;
	}

	public long getFiUsuarioId() {
		return fiUsuarioId;
	}

	public void setFiUsuarioId(long fiUsuarioId) {
		this.fiUsuarioId = fiUsuarioId;
	}

	public int getFiEstatusConciliacionId() {
		return fiEstatusConciliacionId;
	}

	public void setFiEstatusConciliacionId(int fiEstatusConciliacionId) {
		this.fiEstatusConciliacionId = fiEstatusConciliacionId;
	}

	@Override
	public String toString() {
		return "ConciliacionCentralBean [fiSistemaId=" + fiSistemaId
				+ ", fiModuloId=" + fiModuloId + ", fiSubModuloId="
				+ fiSubModuloId + ", fiNumTransaccionId=" + fiNumTransaccionId
				+ ", fiUsuarioId=" + fiUsuarioId + ", fiEstatusConciliacionId="
				+ fiEstatusConciliacionId + super.toString() + "]";
	}

}
