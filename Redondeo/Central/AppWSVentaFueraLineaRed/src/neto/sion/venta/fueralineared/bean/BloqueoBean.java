package neto.sion.venta.fueralineared.bean;

/***
 * Clase que representa un los datos de un bloqueo venta.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class BloqueoBean{
	private int fiTipoPagoId;
	private int fiNumAvisos;
	private int fiEstatusBloqueoId;
	private int fiAvisosFalt;

	public int getFiTipoPagoId() {
		return fiTipoPagoId;
	}

	public void setFiTipoPagoId(int fiTipoPagoId) {
		this.fiTipoPagoId = fiTipoPagoId;
	}

	public int getFiNumAvisos() {
		return fiNumAvisos;
	}

	public void setFiNumAvisos(int fiNumAvisos) {
		this.fiNumAvisos = fiNumAvisos;
	}

	public int getFiEstatusBloqueoId() {
		return fiEstatusBloqueoId;
	}

	public void setFiEstatusBloqueoId(int fiEstatusBloqueoId) {
		this.fiEstatusBloqueoId = fiEstatusBloqueoId;
	}

	public int getFiAvisosFalt() {
		return fiAvisosFalt;
	}

	public void setFiAvisosFalt(int fiAvisosFalt) {
		this.fiAvisosFalt = fiAvisosFalt;
	}

	@Override
	public String toString() {
		return "BloqueoBean [fiTipoPagoId=" + fiTipoPagoId + ", fiNumAvisos="
				+ fiNumAvisos + ", fiEstatusBloqueoId=" + fiEstatusBloqueoId
				+ ", fiAvisosFalt=" + fiAvisosFalt + "]";
	}

}
