package neto.sion.venta.fueralineared.bean;

public class ArticuloActFLBean {

	private long fiArticuloId;
	private String fcCdGbBarras;
	private double fnCantidad;
	private double fnPrecio;
	private double fnCosto;
	private double fnDescuento;
	private double fnIva;
	private int	   fiIeps;	
	private String fcNombreArticulo;
	private double fiAgranel;
	private int fiTipoDescuento;

	public double getFiAgranel() {
		return fiAgranel;
	}

	public void setFiAgranel(double fiAgranel) {
		this.fiAgranel = fiAgranel;
	}

	public String getFcNombreArticulo() {
		return fcNombreArticulo;
	}

	public void setFcNombreArticulo(String fcNombreArticulo) {
		this.fcNombreArticulo = fcNombreArticulo;
	}

	public long getFiArticuloId() {
		return fiArticuloId;
	}

	public void setFiArticuloId(long fiArticuloId) {
		this.fiArticuloId = fiArticuloId;
	}

	public String getFcCdGbBarras() {
		return fcCdGbBarras;
	}

	public void setFcCdGbBarras(String fcCdGbBarras) {
		this.fcCdGbBarras = fcCdGbBarras;
	}

	public double getFnCantidad() {
		return fnCantidad;
	}

	public void setFnCantidad(double fnCantidad) {
		this.fnCantidad = fnCantidad;
	}

	public double getFnPrecio() {
		return fnPrecio;
	}

	public void setFnPrecio(double fnPrecio) {
		this.fnPrecio = fnPrecio;
	}

	public double getFnCosto() {
		return fnCosto;
	}

	public void setFnCosto(double fnCosto) {
		this.fnCosto = fnCosto;
	}

	public double getFnDescuento() {
		return fnDescuento;
	}

	public void setFnDescuento(double fnDescuento) {
		this.fnDescuento = fnDescuento;
	}

	public double getFnIva() {
		return fnIva;
	}

	public void setFnIva(double fnIva) {
		this.fnIva = fnIva;
	}

	public int getFiIeps() {
		return fiIeps;
	}

	public void setFiIeps(int fiIeps) {
		this.fiIeps = fiIeps;
	}
	
	

	public int getFiTipoDescuento() {
		return fiTipoDescuento;
	}

	public void setFiTipoDescuento(int fiTipoDescuento) {
		this.fiTipoDescuento = fiTipoDescuento;
	}

	@Override
	public String toString() {
		return "ArticuloActFLBean [fiArticuloId=" + fiArticuloId
				+ ", fcCdGbBarras=" + fcCdGbBarras + ", fnCantidad="
				+ fnCantidad + ", fnPrecio=" + fnPrecio + ", fnCosto="
				+ fnCosto + ", fnDescuento=" + fnDescuento + ", fnIva=" + fnIva
				+ ", fiIeps=" + fiIeps + ", fcNombreArticulo="
				+ fcNombreArticulo + ", fiAgranel=" + fiAgranel
				+ ", fiTipoDescuento=" + fiTipoDescuento + "]";
	}

	


}