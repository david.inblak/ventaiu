package neto.sion.venta.fueralineared.bean;

import java.util.Arrays;
import neto.sion.venta.base.bean.PeticionBaseVentaBean;

public class PeticionActVentaFLBean extends PeticionBaseVentaBean{

	private static final long serialVersionUID = 7498898917843220985L;
	private String paTerminal;
	private long paUsuarioId;
	private long paUsuarioAutorizaId;
	private String paFechaOper;
	private int paTipoMovto;
	private double paMontoTotalVta;
	private long paMovimientoIdDev;
	private int paTipoDevolucion;
	private ArticuloActFLBean[] articulos;
	private TipoPagoFLBean[] tiposPago;
	private int fueraLinea;
	private int estatus;

	public String getPaTerminal() {
		return paTerminal;
	}

	public void setPaTerminal(String paTerminal) {
		this.paTerminal = paTerminal;
	}

	public long getPaUsuarioId() {
		return paUsuarioId;
	}

	public void setPaUsuarioId(long paUsuarioId) {
		this.paUsuarioId = paUsuarioId;
	}

	public long getPaUsuarioAutorizaId() {
		return paUsuarioAutorizaId;
	}

	public void setPaUsuarioAutorizaId(long paUsuarioAutorizaId) {
		this.paUsuarioAutorizaId = paUsuarioAutorizaId;
	}

	public String getPaFechaOper() {
		return paFechaOper;
	}

	public void setPaFechaOper(String paFechaOper) {
		this.paFechaOper = paFechaOper;
	}

	public int getPaTipoMovto() {
		return paTipoMovto;
	}

	public void setPaTipoMovto(int paTipoMovto) {
		this.paTipoMovto = paTipoMovto;
	}

	public double getPaMontoTotalVta() {
		return paMontoTotalVta;
	}

	public void setPaMontoTotalVta(double paMontoTotalVta) {
		this.paMontoTotalVta = paMontoTotalVta;
	}

	public ArticuloActFLBean[] getArticulos() {
		return articulos;
	}

	public void setArticulos(ArticuloActFLBean[] articulos) {
		this.articulos = articulos;
	}

	public TipoPagoFLBean[] getTiposPago() {
		return tiposPago;
	}

	public void setTiposPago(TipoPagoFLBean[] tiposPago) {
		this.tiposPago = tiposPago;
	}

	public int getFueraLinea() {
		return fueraLinea;
	}

	public void setFueraLinea(int fueraLinea) {
		this.fueraLinea = fueraLinea;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	public long getPaMovimientoIdDev() {
		return paMovimientoIdDev;
	}

	public void setPaMovimientoIdDev(long paMovimientoIdDev) {
		this.paMovimientoIdDev = paMovimientoIdDev;
	}

	public int getPaTipoDevolucion() {
		return paTipoDevolucion;
	}

	public void setPaTipoDevolucion(int paTipoDevolucion) {
		this.paTipoDevolucion = paTipoDevolucion;
	}

	@Override
	public String toString() {
		return "PeticionVentaFLBean [paTerminal=" + paTerminal
				+ ", paUsuarioId=" + paUsuarioId + ", paUsuarioAutorizaId="
				+ paUsuarioAutorizaId + ", paFechaOper=" + paFechaOper
				+ ", paTipoMovto=" + paTipoMovto + ", paMontoTotalVta="
				+ paMontoTotalVta + ", paMovimientoIdDev=" + paMovimientoIdDev
				+ ", paTipoDevolucion=" + paTipoDevolucion + ", articulos="
				+ Arrays.toString(articulos) + ", tiposPago="
				+ Arrays.toString(tiposPago) + ", fueraLinea=" + fueraLinea
				+ ", estatus=" + estatus + "]";
	}

}

