package neto.sion.venta.fueralineared.bean;

public class VentaCanceladaBean {
	
	private long fiUsuarioCancelaId;
	private String fdFechaRegistro;
	private long fiUsuarioAutorizaId;
	private long fiNumTransaccion;
	private int fiTotalLineasCanceladas;
	private int fiTotalVentasCanceladas;
	
	public long getFiUsuarioCancelaId() {
		return fiUsuarioCancelaId;
	}
	public void setFiUsuarioCancelaId(long fiUsuarioCancelaId) {
		this.fiUsuarioCancelaId = fiUsuarioCancelaId;
	}
	public String getFdFechaRegistro() {
		return fdFechaRegistro;
	}
	public void setFdFechaRegistro(String fdFechaRegistro) {
		this.fdFechaRegistro = fdFechaRegistro;
	}
	public long getFiUsuarioAutorizaId() {
		return fiUsuarioAutorizaId;
	}
	public void setFiUsuarioAutorizaId(long fiUsuarioAutorizaId) {
		this.fiUsuarioAutorizaId = fiUsuarioAutorizaId;
	}
	public long getFiNumTransaccion() {
		return fiNumTransaccion;
	}
	public void setFiNumTransaccion(long fiNumTransaccion) {
		this.fiNumTransaccion = fiNumTransaccion;
	}
	public int getFiTotalLineasCanceladas() {
		return fiTotalLineasCanceladas;
	}
	public void setFiTotalLineasCanceladas(int fiTotalLineasCanceladas) {
		this.fiTotalLineasCanceladas = fiTotalLineasCanceladas;
	}
	public int getFiTotalVentasCanceladas() {
		return fiTotalVentasCanceladas;
	}
	public void setFiTotalVentasCanceladas(int fiTotalVentasCanceladas) {
		this.fiTotalVentasCanceladas = fiTotalVentasCanceladas;
	}
	
	@Override
	public String toString() {
		return "VentaCanceladaBean [fiUsuarioCancelaId=" + fiUsuarioCancelaId
				+ ", fdFechaRegistro=" + fdFechaRegistro
				+ ", fiUsuarioAutorizaId=" + fiUsuarioAutorizaId
				+ ", fiNumTransaccion=" + fiNumTransaccion
				+ ", fiTotalLineasCanceladas=" + fiTotalLineasCanceladas
				+ ", fiTotalVentasCanceladas=" + fiTotalVentasCanceladas + "]";
	}
	
	

}
