package neto.sion.venta.fueralineared.bean;

public class LineasEliminadasBean {
	private int usuarioId;
	private String fecha;
	private int usuarioAutorizacionId;
	private long numTransaccion;
	private int totalLineas;

	private int totalCancelacionesVenta;

	public int getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getUsuarioAutorizacionId() {
		return usuarioAutorizacionId;
	}

	public void setUsuarioAutorizacionId(int usuarioAutorizacionId) {
		this.usuarioAutorizacionId = usuarioAutorizacionId;
	}

	public long getNumTransaccion() {
		return numTransaccion;
	}

	public void setNumTransaccion(long numTransaccion) {
		this.numTransaccion = numTransaccion;
	}

	public int getTotalLineas() {
		return totalLineas;
	}

	public void setTotalLineas(int totalLineas) {
		this.totalLineas = totalLineas;
	}

	public int getTotalCancelacionesVenta() {
		return totalCancelacionesVenta;
	}

	public void setTotalCancelacionesVenta(int totalCancelacionesVenta) {
		this.totalCancelacionesVenta = totalCancelacionesVenta;
	}

	@Override
	public String toString() {
		return "LineasEliminadasBean [usuarioId=" + usuarioId + ", fecha="
				+ fecha + ", usuarioAutorizacionId=" + usuarioAutorizacionId
				+ ", numTransaccion=" + numTransaccion + ", totalLineas="
				+ totalLineas + ", totalCancelacionesVenta="
				+ totalCancelacionesVenta + "]";
	}

}
