package neto.sion.venta.fueralineared.bean;

import java.util.Arrays;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

public class PeticionInsercionLineasBean extends PeticionBaseVentaBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fecha;

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	private LineasEliminadasBean[] lineasEliminadas;

	public LineasEliminadasBean[] getLineasEliminadas() {
		return lineasEliminadas;
	}

	public void setLineasEliminadas(LineasEliminadasBean[] lineasEliminadas) {
		this.lineasEliminadas = lineasEliminadas;
	}

	@Override
	public String toString() {
		return "PeticionInsercionLineasBean [fecha=" + fecha
				+ ", lineasEliminadas=" + Arrays.toString(lineasEliminadas)
				+ "]";
	}

}
