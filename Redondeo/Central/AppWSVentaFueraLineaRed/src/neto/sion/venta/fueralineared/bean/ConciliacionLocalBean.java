package neto.sion.venta.fueralineared.bean;

/***
 * Clase que representa el cursor ConciliacionLocal
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class ConciliacionLocalBean {
	private long fiConciliacionId;
	private int fiEstatusConciliacionId;

	public long getFiConciliacionId() {
		return fiConciliacionId;
	}

	public void setFiConciliacionId(long fiConciliacionId) {
		this.fiConciliacionId = fiConciliacionId;
	}

	public int getFiEstatusConciliacionId() {
		return fiEstatusConciliacionId;
	}

	public void setFiEstatusConciliacionId(int fiEstatusConciliacionId) {
		this.fiEstatusConciliacionId = fiEstatusConciliacionId;
	}

	@Override
	public String toString() {
		return "ConciliacionLocalBean [fiConciliacionId=" + fiConciliacionId
				+ ", fiEstatusConciliacionId=" + fiEstatusConciliacionId + "]";
	}

}
