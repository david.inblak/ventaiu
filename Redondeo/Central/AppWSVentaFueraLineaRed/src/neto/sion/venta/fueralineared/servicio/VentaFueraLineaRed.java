package neto.sion.venta.fueralineared.servicio;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import neto.sion.venta.fueralineared.bean.PeticionActVentaFLBean;
import neto.sion.venta.fueralineared.bean.PeticionConciliacionCentralBean;
import neto.sion.venta.fueralineared.bean.PeticionTransaccionCentralBean;
import neto.sion.venta.fueralineared.bean.PeticionVentaBean;
import neto.sion.venta.fueralineared.bean.PeticionVentaCanceladaBean;
import neto.sion.venta.fueralineared.bean.PeticionVentaFLBean;
import neto.sion.venta.fueralineared.bean.RespuestaConciliacionCentralBean;
import neto.sion.venta.fueralineared.bean.RespuestaVentaCanceladaBean;
import neto.sion.venta.fueralineared.bean.RespuestaVentaFLBean;
import neto.sion.venta.fueralineared.bean.RespuestaVentaFLBloqueoBean;
import neto.sion.venta.fueralineared.controlador.VentaFueraLineaControlador;

import neto.sion.venta.pago.servicios.bean.PeticionVentaServiciosBean;
import neto.sion.venta.pago.servicios.bean.RespuestaVentaServiciosBean;

import org.apache.axis2.AxisFault;

/***
 * Clase fachada del WS de venta fuera de linea.
 * 
 * @author Carlos V. Perez L.
 * 
 */
@WebService
public class VentaFueraLineaRed {
	private VentaFueraLineaControlador controlador;

	public VentaFueraLineaRed() {
		controlador = new VentaFueraLineaControlador();
	}

	/***
	 * Metodo encargado insertar en la tabla puente de registros por conciliar ,
	 * ademas de regresar los registros listos para marcar como confirmados o
	 * reversados en la BD Local.
	 * 
	 * @param _peticion
	 * @return Un objeto con la respuesta de la conciliacion en central.
	 * @throws AxisFault
	 */
	@WebMethod
	public RespuestaConciliacionCentralBean insertaConciliacionesPendientes(
			@WebParam(name = "_peticion") PeticionConciliacionCentralBean _peticion)
			throws AxisFault {
		return controlador.insertaConciliacionesPendientes(_peticion);
	}

	/***
	 * Metodo encargado de confirmar o registrar una venta fuera de linea.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la confirmacio o el registro de la
	 *         venta.
	 * @throws AxisFault
	 */
	@WebMethod
	public RespuestaVentaFLBean confirmarVenta(
			@WebParam(name = "_peticion") PeticionVentaFLBean _peticion)
			throws AxisFault {
		return controlador.confirmarVenta(_peticion);
	}

	/***
	 * Metodo encargado de insertar lineas eliminadas en central.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la insercion de las lineas.
	 * @throws AxisFault
	 */
	@WebMethod
	public RespuestaVentaCanceladaBean insertarVentasCanceladas(
			@WebParam(name = "_peticion") PeticionVentaCanceladaBean _peticion)
			throws AxisFault {
		return controlador.insertarVentasCanceladas(_peticion);
	}

	/**
	 * M�todo encargado de consultar un n�mero de conciliaci�n en BD Central
	 * 
	 * @param _peticion
	 * @return Un n�mero de conciliaci�n
	 * @throws AxisFault
	 */
	@WebMethod
	public long consultaConciliacionCentral(
			@WebParam(name = "_peticion") PeticionTransaccionCentralBean _peticion)
			throws AxisFault {
		return controlador.consultaConciliacion(_peticion);
	}

	/***
	 * Metodo encargado de confirmar o registrar una venta fuera de linea
	 * implementando bloqueos.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la confirmacio o el registro de la
	 *         venta y bloqueos.
	 * @throws AxisFault
	 */
	@WebMethod
	public RespuestaVentaFLBloqueoBean confirmarVentaBloqueos(
			@WebParam(name = "_peticion") PeticionVentaFLBean _peticion)
			throws AxisFault {
		return controlador.confirmarVentaBloqueo(_peticion);
	}

	/***
	 * Metodo encargado de confirmar o registrar un pago fuera de linea
	 * implementando bloqueos.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la confirmacion o el registro de la
	 *         venta y bloqueos.
	 * @throws AxisFault
	 */
	@WebMethod
	public RespuestaVentaServiciosBean confirmarPagoServiciosBloqueos(
			@WebParam(name = "_peticion") PeticionVentaServiciosBean _peticion)
			throws AxisFault {
		return controlador.confirmarPagoServiciosBloqueo(_peticion);
	}

	
	// M�todos con IEPS - quitar cuando este implementacion de chip en generico
	
	/***
	 * Metodo encargado de procesar las ventas realizadas fuera de linea.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la confirmacion del proceso.
	 * @throws AxisFault
	 */
	@WebMethod
	public RespuestaVentaFLBloqueoBean confirmarVentaBloqueosAct(
			@WebParam(name = "_peticion") PeticionActVentaFLBean _peticion)
			throws AxisFault {
		return controlador.confirmarVentaBloqueoAct(_peticion);
	}

	@WebMethod
	public RespuestaVentaFLBean confirmarVentaAct(
			@WebParam(name = "_peticion") PeticionActVentaFLBean _peticion)
			throws AxisFault {
		return controlador.confirmarVentaAct(_peticion);
	}

	// ----------------------------------------------------------------------------

	/*
	 * @WebMethod public RespuestaProcesaXTiendaBean
	 * procesarDevolucionesXTienda(
	 * 
	 * @WebParam(name = "_peticion") PeticionProcesaXTiendaBean _peticion)
	 * throws AxisFault { return
	 * controlador.procesaDevolucionesXTienda(_peticion); }
	 */

	/*
	 * @WebMethod public RespuestaProcesaXTiendaBean procesarVentasXTienda(
	 * 
	 * @WebParam(name = "_peticion") PeticionProcesaXTiendaBean _peticion)
	 * throws AxisFault { return controlador.procesaVentaXTienda(_peticion); }
	 */

	/**
	 * M�todo que registra una venta fuera de l�nea en BD Central - implementacion de chip
	 * @param _peticion
	 * @return
	 * @throws AxisFault
	 */
	@WebMethod
	public RespuestaVentaFLBloqueoBean aplicarVentaFL(
			@WebParam(name = "_peticion") PeticionVentaBean _peticion)
			throws AxisFault {
		return controlador.confirmarVentaBloqueoAct(_peticion);
	}

}
