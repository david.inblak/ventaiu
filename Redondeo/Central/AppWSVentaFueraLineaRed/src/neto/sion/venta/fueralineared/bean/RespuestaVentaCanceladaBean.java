package neto.sion.venta.fueralineared.bean;

public class RespuestaVentaCanceladaBean {
	
	private String paMensajeError;
	private int paCdgError;
	
	public String getPaMensajeError() {
		return paMensajeError;
	}
	public void setPaMensajeError(String paMensajeError) {
		this.paMensajeError = paMensajeError;
	}
	public int getPaCdgError() {
		return paCdgError;
	}
	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}
	
	@Override
	public String toString() {
		return "RespuestaVentaCanceladaBean [paMensajeError=" + paMensajeError
				+ ", paCdgError=" + paCdgError + "]";
	}
	
	

}
