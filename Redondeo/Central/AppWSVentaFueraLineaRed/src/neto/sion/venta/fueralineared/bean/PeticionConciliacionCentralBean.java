package neto.sion.venta.fueralineared.bean;

import java.util.Arrays;

import neto.sion.venta.base.bean.PeticionBaseVentaBean;

/***
 * Clase que representa una peticion de conciliacion de la venta en central.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class PeticionConciliacionCentralBean extends PeticionBaseVentaBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3212542342770219691L;
	private ConciliacionCentralBean[] conciliacionesPendientes;

	public ConciliacionCentralBean[] getConciliacionesPendientes() {
		return conciliacionesPendientes;
	}

	public void setConciliacionesPendientes(
			ConciliacionCentralBean[] conciliacionesPendientes) {
		this.conciliacionesPendientes = conciliacionesPendientes;
	}

	@Override
	public String toString() {
		return "PeticionConciliacionCentralBean [conciliacionesPendientes="
				+ Arrays.toString(conciliacionesPendientes) + super.toString()
				+ "]";
	}

}
