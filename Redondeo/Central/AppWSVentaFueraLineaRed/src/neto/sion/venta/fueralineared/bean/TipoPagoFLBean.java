package neto.sion.venta.fueralineared.bean;

/***
 * Clase que representa un tipo de pago.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class TipoPagoFLBean {

	private int fiTipoPagoId;
	private double fnMontoPago;
	private int fnNumeroVales;
	private String paPagoTarjetaIdBus;
	private double importeAdicional;

	public double getImporteAdicional() {
		return importeAdicional;
	}

	public void setImporteAdicional(double importeAdicional) {
		this.importeAdicional = importeAdicional;
	}

	public int getFiTipoPagoId() {
		return fiTipoPagoId;
	}

	public void setFiTipoPagoId(int fiTipoPagoId) {
		this.fiTipoPagoId = fiTipoPagoId;
	}

	public double getFnMontoPago() {
		return fnMontoPago;
	}

	public void setFnMontoPago(double fnMontoPago) {
		this.fnMontoPago = fnMontoPago;
	}

	public int getFnNumeroVales() {
		return fnNumeroVales;
	}

	public void setFnNumeroVales(int fnNumeroVales) {
		this.fnNumeroVales = fnNumeroVales;
	}

	public String getPaPagoTarjetaIdBus() {
		return paPagoTarjetaIdBus;
	}

	public void setPaPagoTarjetaIdBus(String paPagoTarjetaIdBus) {
		this.paPagoTarjetaIdBus = paPagoTarjetaIdBus;
	}

	@Override
	public String toString() {
		return "TipoPagoFLBean [fiTipoPagoId=" + fiTipoPagoId
				+ ", fnMontoPago=" + fnMontoPago + ", fnNumeroVales="
				+ fnNumeroVales + ", paPagoTarjetaIdBus=" + paPagoTarjetaIdBus
				+ ", importeAdicional=" + importeAdicional + "]";
	}

}
