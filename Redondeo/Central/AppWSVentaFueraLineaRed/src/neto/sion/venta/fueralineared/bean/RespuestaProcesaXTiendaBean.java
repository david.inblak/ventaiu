package neto.sion.venta.fueralineared.bean;

public class RespuestaProcesaXTiendaBean {
	
	private int paCdgError;
	private String paDescError;
	
	public int getPaCdgError() {
		return paCdgError;
	}
	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}
	public String getPaDescError() {
		return paDescError;
	}
	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}
	@Override
	public String toString() {
		return "RespuestaProcesaDevolucionesXTienda [paCdgError=" + paCdgError
				+ ", paDescError=" + paDescError + "]";
	}
}
