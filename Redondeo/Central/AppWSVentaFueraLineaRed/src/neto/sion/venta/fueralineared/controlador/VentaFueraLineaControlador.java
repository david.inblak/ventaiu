package neto.sion.venta.fueralineared.controlador;

import org.apache.axis2.AxisFault;

import Com.Elektra.Definition.Config.ConfigSION;
//import neto.sion.genericos.log.logeo;
import neto.sion.Logs;
import neto.sion.venta.fueralineared.bean.PeticionActVentaFLBean;
import neto.sion.venta.fueralineared.bean.PeticionConciliacionCentralBean;
import neto.sion.venta.fueralineared.bean.PeticionProcesaXTiendaBean;
import neto.sion.venta.fueralineared.bean.PeticionTransaccionCentralBean;
import neto.sion.venta.fueralineared.bean.PeticionVentaBean;
import neto.sion.venta.fueralineared.bean.PeticionVentaCanceladaBean;
import neto.sion.venta.fueralineared.bean.PeticionVentaFLBean;
import neto.sion.venta.fueralineared.bean.RespuestaConciliacionCentralBean;
import neto.sion.venta.fueralineared.bean.RespuestaProcesaXTiendaBean;
import neto.sion.venta.fueralineared.bean.RespuestaVentaCanceladaBean;
import neto.sion.venta.fueralineared.bean.RespuestaVentaFLBean;
import neto.sion.venta.fueralineared.bean.RespuestaVentaFLBloqueoBean;
import neto.sion.venta.fueralineared.dao.VentaFueraLineaDAO;
import neto.sion.venta.pago.servicios.bean.PeticionVentaServiciosBean;
import neto.sion.venta.pago.servicios.bean.RespuestaVentaServiciosBean;
import neto.sion.venta.pago.servicios.controlador.AppPagoServiciosControlador;

/***
 * Clase controlador del WS de venta fuera de linea.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class VentaFueraLineaControlador {
	private VentaFueraLineaDAO dao;
	private Logs logeo;

	public VentaFueraLineaControlador() {
		dao = new VentaFueraLineaDAO();
		logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.FL"));
	}

	/***
	 * Metodo encargado de registrar una venta fuera de linea.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaFLBean registrarVenta(PeticionVentaFLBean _peticion)
			throws AxisFault {
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION
				.obtenerParametro("venta.fueralinea.id")));
		return dao.registrarVenta(_peticion);
	}

	/***
	 * Metodo encargado de registrar una venta fuera de linea y bloqueos.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaFLBloqueoBean registrarVentaBloqueos(PeticionVentaFLBean _peticion)
			throws AxisFault {
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION
				.obtenerParametro("venta.fueralinea.id")));
		return dao.registrarVentaBloqueos(_peticion);
	}
	
	/***
	 * Metodo encargado insertar en la tabla puente de registros por conciliar ,
	 * ademas de regresar los registros listos para marcar como confirmados o
	 * reversados en la BD Local.
	 * 
	 * @param _peticion
	 * @return Un objeto con la respuesta de la conciliacion en central.
	 * @throws AxisFault
	 */
	public RespuestaConciliacionCentralBean insertaConciliacionesPendientes(
			PeticionConciliacionCentralBean _peticion) throws AxisFault {
		return dao.insertaConciliacionesPendientes(_peticion);
	}

	/***
	 * Metodo encargado de confirmar o registrar una venta fuera de linea.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la confirmacion o el registro de la
	 *         venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaFLBean confirmarVenta(PeticionVentaFLBean _peticion)
			throws AxisFault {
		PeticionTransaccionCentralBean _peticionConfirmacion = new PeticionTransaccionCentralBean();
		RespuestaVentaFLBean respuesta = new RespuestaVentaFLBean();
		_peticionConfirmacion.setTiendaId(_peticion.getTiendaId());
		_peticionConfirmacion.setPaPaisId(_peticion.getPaPaisId());
		_peticionConfirmacion.setPaConciliacionId(_peticion.getPaConciliacionId());
		_peticionConfirmacion.setPaUsuarioId(_peticion.getPaUsuarioId());
		long transaccionId = 0;
		if (_peticion.getPaTipoMovto() != 1) {
			_peticionConfirmacion.setPaSistemaId(1);
			_peticionConfirmacion.setPaModuloId(2);
			_peticionConfirmacion.setPaSubModuloId(5);
			transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			if (transaccionId == 0) {
				_peticionConfirmacion.setPaSistemaId(1);
				_peticionConfirmacion.setPaModuloId(2);
				_peticionConfirmacion.setPaSubModuloId(2);
				transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			}

		} else {
			_peticionConfirmacion.setPaSistemaId(1);
			_peticionConfirmacion.setPaModuloId(2);
			_peticionConfirmacion.setPaSubModuloId(1);
			transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			if (transaccionId == 0) {
				_peticionConfirmacion.setPaSistemaId(1);
				_peticionConfirmacion.setPaModuloId(2);
				_peticionConfirmacion.setPaSubModuloId(4);
				transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			}
		}

		if (transaccionId != 0) {
			respuesta.setPaCdgError(0);
			respuesta.setPaTransaccionId(transaccionId);
			respuesta.setPaDescError(ConfigSION
					.obtenerParametro("venta.transaccion.existente")
					+ "|"
					+ _peticion.getPaConciliacionId());
		} else {
			respuesta = registrarVenta(_peticion);
		}
		return respuesta;
	}

	/***
	 * Metodo encargado de insertar ventas eliminadas en central.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la insercion de las ventas.
	 * @throws AxisFault
	 */
	public RespuestaVentaCanceladaBean insertarVentasCanceladas(PeticionVentaCanceladaBean _peticion) throws AxisFault{
		return dao.insertarVentasCanceladas(_peticion);
	}
	
	/**
	 * M�todo encargado de consultar una conciliaci�n en BD Central
	 * @param _peticion Objeto de tipo PeticionTransaccionCentralBean
	 * @return Un n�mero de conciliaci�n
	 * @throws AxisFault
	 */
	public long consultaConciliacion(PeticionTransaccionCentralBean _peticion)throws AxisFault{
		return dao.consultarTransaccionCentral(_peticion);
	}
	
	/**
	 * M�todo encargado de confirmar o registrar una venta de fuera de linea implementando bloqueos FL
	 * @param _peticion
	 * @return
	 * @throws AxisFault
	 */
	public RespuestaVentaFLBloqueoBean confirmarVentaBloqueo(PeticionVentaFLBean _peticion)
			throws AxisFault {
		PeticionTransaccionCentralBean _peticionConfirmacion = new PeticionTransaccionCentralBean();
		RespuestaVentaFLBloqueoBean respuesta = new RespuestaVentaFLBloqueoBean();
		_peticionConfirmacion.setTiendaId(_peticion.getTiendaId());
		_peticionConfirmacion.setPaPaisId(_peticion.getPaPaisId());
		_peticionConfirmacion.setPaConciliacionId(_peticion.getPaConciliacionId());
		_peticionConfirmacion.setPaUsuarioId(_peticion.getPaUsuarioId());
		long transaccionId = 0;
		if (_peticion.getPaTipoMovto() != 1) {
			_peticionConfirmacion.setPaSistemaId(1);
			_peticionConfirmacion.setPaModuloId(2);
			_peticionConfirmacion.setPaSubModuloId(5);
			transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			if (transaccionId == 0) {
				_peticionConfirmacion.setPaSistemaId(1);
				_peticionConfirmacion.setPaModuloId(2);
				_peticionConfirmacion.setPaSubModuloId(2);
				transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			}

		} else {
			_peticionConfirmacion.setPaSistemaId(1);
			_peticionConfirmacion.setPaModuloId(2);
			_peticionConfirmacion.setPaSubModuloId(1);
			transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			if (transaccionId == 0) {
				_peticionConfirmacion.setPaSistemaId(1);
				_peticionConfirmacion.setPaModuloId(2);
				_peticionConfirmacion.setPaSubModuloId(4);
				transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);

			}
		}

		if (transaccionId != 0) {
			respuesta.setPaCdgError(0);
			respuesta.setPaTransaccionId(transaccionId);
			respuesta.setPaDescError(ConfigSION.obtenerParametro("venta.transaccion.existente")+ "|"+ _peticion.getPaConciliacionId());
			respuesta.setBloqueos(null);
		} else {
			respuesta = registrarVentaBloqueos(_peticion);
		}
		
		logeo.log("Respuesta de confirmacion de venta: "+respuesta.toString());
		
		return respuesta;
	}
	
	/**
	 * M�todo encargado de confirmar o registrar una pago de servicios fuera de linea implementando bloqueos FL
	 * @param _peticion
	 * @return
	 * @throws AxisFault
	 */
	public RespuestaVentaServiciosBean confirmarPagoServiciosBloqueo(PeticionVentaServiciosBean _peticion)
			throws AxisFault {
		
		PeticionTransaccionCentralBean _peticionConfirmacion = new PeticionTransaccionCentralBean();
		RespuestaVentaServiciosBean respuesta = new RespuestaVentaServiciosBean();
		
		_peticionConfirmacion.setTiendaId(_peticion.getTiendaId());
		_peticionConfirmacion.setPaPaisId(_peticion.getPaPaisId());
		_peticionConfirmacion.setPaConciliacionId(_peticion.getPaConciliacionId());
		_peticionConfirmacion.setPaUsuarioId(_peticion.getPaUsuarioId());
		_peticionConfirmacion.setPaSistemaId(Integer.parseInt(ConfigSION.obtenerParametro("PAGO.SISTEMA")));
		_peticionConfirmacion.setPaModuloId(Integer.parseInt(ConfigSION.obtenerParametro("PAGO.MODULO")));
		_peticionConfirmacion.setPaSubModuloId(Integer.parseInt(ConfigSION.obtenerParametro("PAGO.SUBMODULO")));
		
		long transaccionId = 0;
		transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
		
		if (transaccionId != 0) {
			respuesta.setPaCdgError(0);
			respuesta.setPaTransaccionId(transaccionId);
			respuesta.setPaDescError(ConfigSION.obtenerParametro("venta.transaccion.existente")+ "|"+ _peticion.getPaConciliacionId());
			respuesta.setPaArrayBloqueos(null);
		} else {
			_peticionConfirmacion.setPaSubModuloId(Integer.parseInt(ConfigSION.obtenerParametro("PAGO.SUBMODULOFL")));
			transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			if (transaccionId != 0) {
				respuesta.setPaCdgError(0);
				respuesta.setPaTransaccionId(transaccionId);
				respuesta.setPaDescError(ConfigSION.obtenerParametro("venta.transaccion.existente")+ "|"+ _peticion.getPaConciliacionId());
				respuesta.setPaArrayBloqueos(null);
			}else{
				///pago de servicios no encontrado en BD, se marca directamente en el switch
				AppPagoServiciosControlador pagoServicios = new AppPagoServiciosControlador();
				respuesta = pagoServicios.registrarVentaServiciosCt(_peticion);
			}
			
		}
		
		logeo.log("Respuesta de confirmacion de pago de servicios: "+respuesta.toString());
		
		return respuesta;
	}
	
	/***
	 * Metodo encargado de procesar las devoluciones realizadas fuera de l�nea
	 * 
	 * @param _peticion
	 * @return Un objeto con la respuesta del reproceso en central.
	 * @throws AxisFault
	 */
	public RespuestaProcesaXTiendaBean procesaDevolucionesXTienda(PeticionProcesaXTiendaBean _peticion) throws AxisFault {
		return dao.procesaDevolucionesXTienda(_peticion);
	}
	
	/***
	 * Metodo encargado de procesar las ventas realizadas fuera de l�nea
	 * 
	 * @param _peticion
	 * @return Un objeto con la respuesta del reproceso en central.
	 * @throws AxisFault
	 */
	public RespuestaProcesaXTiendaBean procesaVentaXTienda(PeticionProcesaXTiendaBean _peticion) throws AxisFault {
			return dao.procesaVentaXTienda(_peticion);
	}

// M�todos con IEPS
	
	public RespuestaVentaFLBean confirmarVentaAct(PeticionActVentaFLBean _peticion)
			throws AxisFault {
		PeticionTransaccionCentralBean _peticionConfirmacion = new PeticionTransaccionCentralBean();
		RespuestaVentaFLBean respuesta = new RespuestaVentaFLBean();
		_peticionConfirmacion.setTiendaId(_peticion.getTiendaId());
		_peticionConfirmacion.setPaPaisId(_peticion.getPaPaisId());
		_peticionConfirmacion.setPaConciliacionId(_peticion.getPaConciliacionId());
		_peticionConfirmacion.setPaUsuarioId(_peticion.getPaUsuarioId());
		long transaccionId = 0;
		if (_peticion.getPaTipoMovto() != 1) {
			_peticionConfirmacion.setPaSistemaId(1);
			_peticionConfirmacion.setPaModuloId(2);
			_peticionConfirmacion.setPaSubModuloId(5);
			transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			if (transaccionId == 0) {
				_peticionConfirmacion.setPaSistemaId(1);
				_peticionConfirmacion.setPaModuloId(2);
				_peticionConfirmacion.setPaSubModuloId(2);
				transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			}

		} else {
			_peticionConfirmacion.setPaSistemaId(1);
			_peticionConfirmacion.setPaModuloId(2);
			_peticionConfirmacion.setPaSubModuloId(1);
			transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			if (transaccionId == 0) {
				_peticionConfirmacion.setPaSistemaId(1);
				_peticionConfirmacion.setPaModuloId(2);
				_peticionConfirmacion.setPaSubModuloId(4);
				transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			}
		}

		if (transaccionId != 0) {
			respuesta.setPaCdgError(0);
			respuesta.setPaTransaccionId(transaccionId);
			respuesta.setPaDescError(ConfigSION
					.obtenerParametro("venta.transaccion.existente")
					+ "|"
					+ _peticion.getPaConciliacionId());
		} else {
			respuesta = registrarVentaAct(_peticion);
		}
		return respuesta;
	}
	
	public RespuestaVentaFLBean registrarVentaAct(PeticionActVentaFLBean _peticion)
			throws AxisFault {
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION
				.obtenerParametro("venta.fueralinea.id")));
		return dao.registrarVentaAct(_peticion);
	}
	
	/*
	 * M�todo que confirma y en su caso marca una venta fuera de linea - este m�todo es consumido en tiendas donde
	 * no esta la implementacion de chip - quitar despues
	 */
	public RespuestaVentaFLBloqueoBean confirmarVentaBloqueoAct(PeticionActVentaFLBean _peticion)
			throws AxisFault {
		PeticionTransaccionCentralBean _peticionConfirmacion = new PeticionTransaccionCentralBean();
		RespuestaVentaFLBloqueoBean respuesta = new RespuestaVentaFLBloqueoBean();
		_peticionConfirmacion.setTiendaId(_peticion.getTiendaId());
		_peticionConfirmacion.setPaPaisId(_peticion.getPaPaisId());
		_peticionConfirmacion.setPaConciliacionId(_peticion.getPaConciliacionId());
		_peticionConfirmacion.setPaUsuarioId(_peticion.getPaUsuarioId());
		long transaccionId = 0;
		if (_peticion.getPaTipoMovto() != 1) {
			_peticionConfirmacion.setPaSistemaId(1);
			_peticionConfirmacion.setPaModuloId(2);
			_peticionConfirmacion.setPaSubModuloId(5);
			transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			if (transaccionId == 0) {
				_peticionConfirmacion.setPaSistemaId(1);
				_peticionConfirmacion.setPaModuloId(2);
				_peticionConfirmacion.setPaSubModuloId(2);
				transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			}

		} else {
			_peticionConfirmacion.setPaSistemaId(1);
			_peticionConfirmacion.setPaModuloId(2);
			_peticionConfirmacion.setPaSubModuloId(1);
			transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			if (transaccionId == 0) {
				_peticionConfirmacion.setPaSistemaId(1);
				_peticionConfirmacion.setPaModuloId(2);
				_peticionConfirmacion.setPaSubModuloId(4);
				transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);

			}
		}

		if (transaccionId != 0) {
			respuesta.setPaCdgError(0);
			respuesta.setPaTransaccionId(transaccionId);
			respuesta.setPaDescError(ConfigSION.obtenerParametro("venta.transaccion.existente")+ "|"+ _peticion.getPaConciliacionId());
			respuesta.setBloqueos(null);
		} else {
			respuesta = registrarVentaBloqueosAct(_peticion);
		}
		
		logeo.log("Respuesta de confirmacion de venta: "+respuesta.toString());
		
		return respuesta;
	}
	
	/*
	 * M�todo que invoca dao para marcar venta fuera de linea - quitar cuando implementacion de chip este en generico
	 */
	public RespuestaVentaFLBloqueoBean registrarVentaBloqueosAct(PeticionActVentaFLBean _peticion)
			throws AxisFault {
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION
				.obtenerParametro("venta.fueralinea.id")));
		return dao.registrarVentaBloqueosAct(_peticion);
	}
	
	/*****CHIP*******/
	
	/*
	 * M�todo que confirma y en su caso marca una venta fuera de linea - este m�todo es consumido en tiendas donde
	 * no esta la implementacion de chip - quitar despues
	 */
	public RespuestaVentaFLBloqueoBean confirmarVentaBloqueoAct(PeticionVentaBean _peticion)
			throws AxisFault {
		PeticionTransaccionCentralBean _peticionConfirmacion = new PeticionTransaccionCentralBean();
		RespuestaVentaFLBloqueoBean respuesta = new RespuestaVentaFLBloqueoBean();
		_peticionConfirmacion.setTiendaId(_peticion.getTiendaId());
		_peticionConfirmacion.setPaPaisId(_peticion.getPaPaisId());
		_peticionConfirmacion.setPaConciliacionId(_peticion.getPaConciliacionId());
		_peticionConfirmacion.setPaUsuarioId(_peticion.getPaUsuarioId());
		long transaccionId = 0;
		if (_peticion.getPaTipoMovto() != 1) {
			_peticionConfirmacion.setPaSistemaId(1);
			_peticionConfirmacion.setPaModuloId(2);
			_peticionConfirmacion.setPaSubModuloId(5);
			transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			if (transaccionId == 0) {
				_peticionConfirmacion.setPaSistemaId(1);
				_peticionConfirmacion.setPaModuloId(2);
				_peticionConfirmacion.setPaSubModuloId(2);
				transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			}

		} else {
			_peticionConfirmacion.setPaSistemaId(1);
			_peticionConfirmacion.setPaModuloId(2);
			_peticionConfirmacion.setPaSubModuloId(1);
			transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);
			if (transaccionId == 0) {
				_peticionConfirmacion.setPaSistemaId(1);
				_peticionConfirmacion.setPaModuloId(2);
				_peticionConfirmacion.setPaSubModuloId(4);
				transaccionId = dao.consultarTransaccionCentral(_peticionConfirmacion);

			}
		}

		if (transaccionId != 0) {
			respuesta.setPaCdgError(0);
			respuesta.setPaTransaccionId(transaccionId);
			respuesta.setPaDescError(ConfigSION.obtenerParametro("venta.transaccion.existente")+ "|"+ _peticion.getPaConciliacionId());
			respuesta.setBloqueos(null);
		} else {
			respuesta = registrarVentaBloqueosAct(_peticion);
		}
		
		logeo.log("Respuesta de confirmacion de venta: "+respuesta.toString());
		
		return respuesta;
	}
	
	/*
	 * M�todo que invoca dao para marcar venta fuera de linea - quitar cuando implementacion de chip este en generico
	 */
	public RespuestaVentaFLBloqueoBean registrarVentaBloqueosAct(PeticionVentaBean _peticion)
			throws AxisFault {
		_peticion.setFueraLinea(Integer.valueOf(ConfigSION
				.obtenerParametro("venta.fueralinea.id")));
		return dao.registrarVentaBloqueosAct(_peticion);
	}
}
