package neto.sion.venta.fueralineared.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import neto.sion.Logs;
//import neto.sion.genericos.log.logeo;
import neto.sion.genericos.sql.Conexion;
import neto.sion.venta.fueralineared.bean.ArticuloActFLBean;
import neto.sion.venta.fueralineared.bean.ArticuloFLBean;
import neto.sion.venta.fueralineared.bean.BloqueoBean;
import neto.sion.venta.fueralineared.bean.ConciliacionCentralBean;
import neto.sion.venta.fueralineared.bean.ConciliacionLocalBean;
import neto.sion.venta.fueralineared.bean.PeticionActVentaFLBean;
import neto.sion.venta.fueralineared.bean.PeticionConciliacionCentralBean;
import neto.sion.venta.fueralineared.bean.PeticionMarcadoPagoTarjetaFLBean;
import neto.sion.venta.fueralineared.bean.PeticionProcesaXTiendaBean;
import neto.sion.venta.fueralineared.bean.PeticionTransaccionCentralBean;
import neto.sion.venta.fueralineared.bean.PeticionVentaBean;
import neto.sion.venta.fueralineared.bean.PeticionVentaCanceladaBean;
import neto.sion.venta.fueralineared.bean.PeticionVentaFLBean;
import neto.sion.venta.fueralineared.bean.RespuestaConciliacionCentralBean;
import neto.sion.venta.fueralineared.bean.RespuestaMarcadoPagoTarjetaFLBean;
import neto.sion.venta.fueralineared.bean.RespuestaProcesaXTiendaBean;
import neto.sion.venta.fueralineared.bean.RespuestaVentaCanceladaBean;
import neto.sion.venta.fueralineared.bean.RespuestaVentaFLBean;
import neto.sion.venta.fueralineared.bean.RespuestaVentaFLBloqueoBean;
import neto.sion.venta.fueralineared.bean.TipoPagoFLBean;
import neto.sion.venta.fueralineared.bean.VentaCanceladaBean;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

import org.apache.axis2.AxisFault;

import Com.Elektra.Definition.Config.ConfigSION;

/***
 * Clase DAO del WS de Venta fuera de linea.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class VentaFueraLineaDAO extends Conexion {
	private static int TARJETA_ID;
	private Logs logeo;

	public VentaFueraLineaDAO() {
		super(ConfigSION.obtenerParametro("dataSource"));
		TARJETA_ID = Integer.valueOf(ConfigSION
				.obtenerParametro("venta.tarjeta.id"));
		logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.FL"));
	}

	/***
	 * Metodo encargado insertar en la tabla puente de registros por conciliar ,
	 * ademas de regresar los registros listos para marcar como confirmados o
	 * reversados en la BD Local.
	 * 
	 * @param _peticion
	 * @return Un objeto con la respuesta de la conciliacion en central.
	 * @throws AxisFault
	 */
	public RespuestaConciliacionCentralBean insertaConciliacionesPendientes(
			PeticionConciliacionCentralBean _peticion) throws AxisFault {
		RespuestaConciliacionCentralBean respuesta = new RespuestaConciliacionCentralBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ResultSet conciliacionesLocal = null;
		ARRAY arregloConciliaciones = null;
		logeo.log("Peticion de insercion de conciliaciones pendientes:"
				+ _peticion.toString());
		try {
			sql = ConfigSION
					.obtenerParametro("VENTA.FUERALINEA.SPINSCONCILIACIONESPENDIENTES");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			conn = obtenerConexion();
			Object[] conciliaciones = new Object[_peticion
					.getConciliacionesPendientes().length];
			StructDescriptor estructuraConciliacion = StructDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.FUERALINEA.OBJETOCONCILIACIONLOCALORACLE"),
							oconn);
			ArrayDescriptor estructuraArreglo = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.FUERALINEA.OBJETOARREGLOCONCILIACIONLOCALORACLE"),
							oconn);
			int cont = 0;
			for (ConciliacionCentralBean conciliacionBean : _peticion
					.getConciliacionesPendientes()) {
				Object[] objeto = new Object[] {
						conciliacionBean.getPaPaisId(),
						conciliacionBean.getTiendaId(),
						conciliacionBean.getFiSistemaId(),
						conciliacionBean.getFiModuloId(),
						conciliacionBean.getFiSubModuloId(),
						conciliacionBean.getPaConciliacionId(),
						conciliacionBean.getFiNumTransaccionId(),
						conciliacionBean.getFiUsuarioId(),
						conciliacionBean.getFiEstatusConciliacionId() };
				conciliaciones[cont] = new STRUCT(estructuraConciliacion,
						oconn, objeto);
				cont++;
			}
			arregloConciliaciones = new ARRAY(estructuraArreglo, oconn,
					conciliaciones);
			cstmt.setARRAY(1, arregloConciliaciones);
			cstmt.registerOutParameter(2, OracleTypes.ARRAY, ConfigSION
					.obtenerParametro("VENTA.OBJETOCOCONCILIACIONCENTRAL"));
			cstmt.registerOutParameter(3, OracleTypes.NUMBER);
			cstmt.registerOutParameter(4, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(cstmt.getInt(3));
			respuesta.setPaDescError(cstmt.getString(4));
			if (respuesta.getPaCdgError() == 0) {
				ArrayList<ConciliacionLocalBean> listaConciliaciones = new ArrayList<ConciliacionLocalBean>();
				conciliacionesLocal = ((ARRAY) cstmt.getArray(2))
						.getResultSet();
				while (conciliacionesLocal.next()) {
					STRUCT estructuraConciliacionResp = (STRUCT) conciliacionesLocal
							.getObject(2);
					Object[] datosConciliacionResp = estructuraConciliacionResp
							.getAttributes();
					ConciliacionLocalBean conciliacionBean = new ConciliacionLocalBean();
					conciliacionBean
							.setFiConciliacionId(((BigDecimal) datosConciliacionResp[0])
									.longValue());
					conciliacionBean
							.setFiEstatusConciliacionId(((BigDecimal) datosConciliacionResp[1])
									.intValue());
					listaConciliaciones.add(conciliacionBean);
				}
				if (!listaConciliaciones.isEmpty()) {
					ConciliacionLocalBean[] arregloConciliacionesBean = new ConciliacionLocalBean[listaConciliaciones
							.size()];
					listaConciliaciones.toArray(arregloConciliacionesBean);
					respuesta
							.setCurConciliacionesLocal(arregloConciliacionesBean);
				} else {
					respuesta.setCurConciliacionesLocal(null);
				}
			} else {
				respuesta.setCurConciliacionesLocal(null);
			}

			logeo.log("Respuesta de insercion de conciliaciones pendientes:"
					+ respuesta.toString());
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new AxisFault(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarRecursos(conn, cstmt, conciliacionesLocal);
			Conexion.cerrarConexion(oconn);
		}
		return respuesta;
	}

	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar la
	 * venta de productos.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaFLBean registrarVenta(PeticionVentaFLBean _peticion)
			throws AxisFault {

		RespuestaVentaFLBean respuesta = new RespuestaVentaFLBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		logeo.log("Peticion de registro de venta:" + _peticion.toString());
		try {
			sql = ConfigSION
					.obtenerParametro("VENTA.FUERALINEA.SPSERVICIOVENTAFL");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

			Object[] articulos = new Object[_peticion.getArticulos().length];
			StructDescriptor estructuraArticulo = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOARTICULOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOARTICULOSORACLE"),
							oconn);
			int contador = 0;
			for (ArticuloFLBean articulo : _peticion.getArticulos()) {
				Object[] objeto = new Object[] { 
						 articulo.getFiArticuloId(),
						 articulo.getFcCdGbBarras(),
						 articulo.getFnCantidad(),
						 articulo.getFnPrecio(),
						 articulo.getFnCosto(),
						 articulo.getFnDescuento(),
						 articulo.getFnIva(),
						 0};
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,
						objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,
					articulos);
			ArrayList<TipoPagoFLBean> tiposPagoLista = new ArrayList<TipoPagoFLBean>();
			StructDescriptor estructuraTiposPago = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"),
							oconn);
			for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
				if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {

					String[] arregloTarjetasIdBus = tipoPago
							.getPaPagoTarjetaIdBus().split("\\|");
					for (String tarjetaIdBus : arregloTarjetasIdBus) {
						PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
						marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION
								.obtenerParametro("venta.proceso.4.id")));
						marcadoVenta
								.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
										.obtenerParametro("venta.proceso.4.estatus.tarjeta.5")));
						marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
						marcadoVenta.setTiendaId(_peticion.getTiendaId());
						marcadoVenta.setPaPagoTarjetaIdBus(Long
								.valueOf(tarjetaIdBus));
						marcadoVenta.setPaComision(tipoPago
								.getImporteAdicional());
						RespuestaMarcadoPagoTarjetaFLBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
						if (respuestaMarcado.getPaCdgError() != 0) {
							logeo.log("Resultado del marcado de pago con tarjeta:"
									+ marcadoVenta);
						}

					}

				}
				tiposPagoLista.add(tipoPago);

			}
			contador = 0;
			Object[] tiposPago = new Object[tiposPagoLista.size()];
			for (TipoPagoFLBean tipoPago : tiposPagoLista) {
				Object[] objeto = new Object[] { tipoPago.getFiTipoPagoId(),
						tipoPago.getFnMontoPago(), tipoPago.getFnNumeroVales(),
						tipoPago.getImporteAdicional() };
				tiposPago[contador] = new STRUCT(estructuraTiposPago, oconn,
						objeto);
				contador++;
			}
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,
					tiposPago);

			cstmt.setString	(1, _peticion.getPaTerminal());
			cstmt.setLong	(2, _peticion.getPaPaisId());
			cstmt.setLong	(3, _peticion.getTiendaId());
			cstmt.setLong	(4, _peticion.getPaUsuarioId());
			cstmt.setLong	(5, _peticion.getPaUsuarioAutorizaId());
			cstmt.setString	(6, _peticion.getPaFechaOper());
			cstmt.setLong	(7, _peticion.getPaTipoMovto());
			cstmt.setDouble	(8, _peticion.getPaMontoTotalVta());
			cstmt.setARRAY	(9, arregloArticulos);
			cstmt.setARRAY	(10, arregloTiposPago);
			cstmt.setInt	(11, _peticion.getPaTipoDevolucion());
			cstmt.setLong	(12, 0);
			cstmt.setLong	(13, _peticion.getPaMovimientoIdDev());
			cstmt.setLong	(14, _peticion.getPaConciliacionId());
			cstmt.setInt	(15, _peticion.getFueraLinea());
			cstmt.registerOutParameter(16, OracleTypes.NUMBER);
			cstmt.registerOutParameter(17, OracleTypes.CURSOR);
			cstmt.registerOutParameter(18, OracleTypes.NUMBER);
			cstmt.registerOutParameter(19, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(18)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(19)));
			respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(16)));
			logeo.log("Salida:" + respuesta.toString());
			if (respuesta.getPaTransaccionId() == 0
					&& respuesta.getPaCdgError() == 0) {
				respuesta.setPaCdgError(Integer.valueOf(ConfigSION
						.obtenerParametro("transaccion.cero.codigo")));
				respuesta.setPaDescError(ConfigSION
						.obtenerParametro("transaccion.cero.mensaje"));
			}
			if (respuesta.getPaCdgError() == 0
					&& respuesta.getPaTransaccionId() != 0) {
				respuesta.setPaDescError(respuesta.getPaDescError() + "|"
						+ _peticion.getPaConciliacionId());
			}
			if (respuesta.getPaCdgError() == 0) {
				bloqueos = cstmt.getCursor(17);
				for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
						String[] arregloTarjetasIdBus = tipoPago
								.getPaPagoTarjetaIdBus().split("\\|");
						for (String tarjetaIdBus : arregloTarjetasIdBus) {

							PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
							marcadoVenta
									.setPaProcesoExc(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.3.id")));
							marcadoVenta
									.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.3.estatus.tarjeta.7")));
							marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
							marcadoVenta.setTiendaId(_peticion.getTiendaId());
							marcadoVenta.setPaPagoTarjetaIdBus(Long
									.valueOf(tarjetaIdBus));
							marcadoVenta.setPaNumTransaccion(respuesta
									.getPaTransaccionId());
							marcadoVenta.setPaTipoPagoId(tipoPago
									.getFiTipoPagoId());
							RespuestaMarcadoPagoTarjetaFLBean actualizacionMarcado = marcadoPagoTarjeta(marcadoVenta);
							if (actualizacionMarcado.getPaCdgError() != 0) {
								logeo.log("Resultado del marcado de pago con tarjeta:"
										+ marcadoVenta);
							}
						}
					}
				}
			} else {
				for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
						String[] arregloTarjetasIdBus = tipoPago
								.getPaPagoTarjetaIdBus().split("\\|");
						for (String tarjetaIdBus : arregloTarjetasIdBus) {

							PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
							marcadoVenta
									.setPaProcesoExc(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.4.id")));
							marcadoVenta
									.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.4.estatus.tarjeta.6")));
							marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
							marcadoVenta.setTiendaId(_peticion.getTiendaId());
							marcadoVenta.setPaPagoTarjetaIdBus(Long
									.valueOf(tarjetaIdBus));
							RespuestaMarcadoPagoTarjetaFLBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
							if (respuestaMarcado.getPaCdgError() != 0) {
								logeo.log("Resultado del marcado de pago con tarjeta:"
										+ marcadoVenta);

							}
						}
					}
				}
			}
			logeo.log("Respuesta de la peticion de registro de venta fuera de linea:"
					+ respuesta.toString());
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			respuesta.setPaCdgError(1);
			respuesta.setPaDescError(e.getLocalizedMessage() + e.getClass());
		} finally {
			Conexion.cerrarConexion(conn);
			Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
		}
		return respuesta;
	}

	/***
	 * Realiza una llamada al procedimiento almacenado encargado de almacenar y
	 * actualizar el marcado de pago con tarjeta.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del marcado del pago.
	 * @throws AxisFault
	 */
	public RespuestaMarcadoPagoTarjetaFLBean marcadoPagoTarjeta(
			PeticionMarcadoPagoTarjetaFLBean _peticion) throws AxisFault {
		RespuestaMarcadoPagoTarjetaFLBean respuesta = new RespuestaMarcadoPagoTarjetaFLBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		logeo.log("Peticion de marcado pago tarjeta:" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPINSPAGOSTARJETA");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setInt(1, _peticion.getPaProcesoExc());
			cstmt.setInt(2, _peticion.getPaPaisId());
			cstmt.setLong(3, _peticion.getTiendaId());
			cstmt.setString(4, "");
			cstmt.setDouble(5, 0.0);
			cstmt.setDouble(6, _peticion.getPaComision());
			cstmt.setLong(7, _peticion.getPaPagoTarjetaIdBus());
			cstmt.setInt(8, 0);
			cstmt.setInt(9, _peticion.getPaEstatusTarjetaId());
			cstmt.setString(10, "");
			cstmt.setInt(11, _peticion.getPaTipoPagoId());
			cstmt.setLong(12, _peticion.getPaNumTransaccion());
			cstmt.setInt(13, 0);
			cstmt.registerOutParameter(14, OracleTypes.NUMBER);
			cstmt.registerOutParameter(15, OracleTypes.NUMBER);
			cstmt.registerOutParameter(16, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaPagoTarjetasId(Long.valueOf(cstmt.getLong(14)));
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(15)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(16)));
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new AxisFault(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarConexion(conn);
			Conexion.cerrarRecursos(oconn, cstmt, null);
		}

		return respuesta;
	}

	/***
	 * Metodo encargado de consultar el numero de transaccion de la base central
	 * 
	 * @param _peticion
	 * @return Un long 0 => No se encontro el registro, otro =>Numero de
	 *         transaccion.
	 * @throws AxisFault
	 */
	public long consultarTransaccionCentral(
			PeticionTransaccionCentralBean _peticion) throws AxisFault {
		long respuesta = 0;
		Connection conn = null;
		OracleConnection oconn = null;
		OracleCallableStatement cstmt = null;
		String sql = null;
		logeo.log("Consulta de transaccion en central:" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.FNCONSULTATRANSACCION");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.registerOutParameter(1, OracleTypes.NUMBER);
			cstmt.setInt(2, _peticion.getPaPaisId());
			cstmt.setLong(3, _peticion.getTiendaId());
			cstmt.setLong(4, _peticion.getPaConciliacionId());
			cstmt.setLong(5, _peticion.getPaUsuarioId());
			cstmt.setInt(6, _peticion.getPaSistemaId());
			cstmt.setInt(7, _peticion.getPaModuloId());
			cstmt.setInt(8, _peticion.getPaSubModuloId());
			cstmt.execute();
			respuesta = cstmt.getLong(1);
			logeo.log("Respuesta de la consulta de transaccion en central:"
					+ respuesta);
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new AxisFault(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarStatement(cstmt);
			Conexion.cerrarConexion(conn);
			Conexion.cerrarConexion(oconn);
		}
		return respuesta;

	}

	/***
	 * Metodo encargado de insertar ventas eliminadas en central.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la insercion de las ventas.
	 * @throws AxisFault
	 */
	public RespuestaVentaCanceladaBean insertarVentasCanceladas(
			PeticionVentaCanceladaBean _peticion) throws AxisFault {
		RespuestaVentaCanceladaBean respuesta = new RespuestaVentaCanceladaBean();

		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloVentasCanceladas = null;

		logeo.log("Se recibe peticion de insercion de ventas eliminadas en central ");

		try {

			sql = ConfigSION
					.obtenerParametro("VENTA.FUERALINEA.SPCANCELACIONLINEASVENTA");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

			Object[] ventasCanceladas = new Object[_peticion
					.getVentaCanceladaBeans().length];
			StructDescriptor estructuraVenta = StructDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOTIPOVENTACANCELADAORACLE"),
							oconn);
			ArrayDescriptor descriptorVentas = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOTIPOVENTACANCELADAORACLE"),
							oconn);

			int contador = 0;
			for (VentaCanceladaBean venta : _peticion.getVentaCanceladaBeans()) {
				Object[] objeto = new Object[] { venta.getFiUsuarioCancelaId(),
						venta.getFdFechaRegistro(),
						venta.getFiUsuarioAutorizaId(),
						venta.getFiNumTransaccion(),
						venta.getFiTotalLineasCanceladas(),
						venta.getFiTotalVentasCanceladas() };
				ventasCanceladas[contador] = new STRUCT(estructuraVenta, oconn,
						objeto);
				contador++;
			}

			arregloVentasCanceladas = new ARRAY(descriptorVentas, oconn,
					ventasCanceladas);

			cstmt.setInt(1, _peticion.getPaPaisId());
			cstmt.setLong(2, _peticion.getPaTiendaId());
			cstmt.setLong(3, _peticion.getPaConciliacionId());
			cstmt.setLong(4, _peticion.getPaUsuarioId());
			cstmt.setInt(5, _peticion.getPaSistemaId());
			cstmt.setInt(6, _peticion.getPaModuloId());
			cstmt.setInt(7, _peticion.getPaSubModuloId());
			cstmt.setArray(8, arregloVentasCanceladas);
			cstmt.registerOutParameter(10, OracleTypes.VARCHAR);
			cstmt.registerOutParameter(9, OracleTypes.NUMBER);
			cstmt.execute();

			respuesta.setPaCdgError(cstmt.getInt(9));
			respuesta.setPaMensajeError(cstmt.getString(10));

			logeo.log("Respuesta de servicio: " + respuesta.toString());

		} catch (Exception e) {
			logeo.logearExcepcion(e, sql);
		} finally {
			Conexion.cerrarConexion(conn);
			Conexion.cerrarCallableStatement(cstmt);
			Conexion.cerrarConexion(oconn);
		}

		return respuesta;
	}

	/***
	 * Realiza una llamada al procedimiento almacenado encargado de registrar la
	 * venta de productos.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaFLBloqueoBean registrarVentaBloqueos(
			PeticionVentaFLBean _peticion) throws AxisFault {
		RespuestaVentaFLBloqueoBean respuesta = new RespuestaVentaFLBloqueoBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		logeo.log("Peticion de registro de venta:" + _peticion.toString());
		try {
			sql = ConfigSION
					.obtenerParametro("VENTA.FUERALINEA.SPSERVICIOVENTAFL");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

			Object[] articulos = new Object[_peticion.getArticulos().length];
			StructDescriptor estructuraArticulo = StructDescriptor.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOARTICULOORACLE"),oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOARREGLOARTICULOSORACLE"),oconn);
			int contador = 0;
			for (ArticuloFLBean articulo : _peticion.getArticulos()) {
				Object[] objeto = new Object[] { 
						articulo.getFiArticuloId()
						,articulo.getFcCdGbBarras()
						,articulo.getFnCantidad()
						,articulo.getFnPrecio()
						,articulo.getFnCosto()
						,articulo.getFnDescuento()
						,articulo.getFnIva()
						,0};
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,articulos);
			
			
			ArrayList<TipoPagoFLBean> tiposPagoLista = new ArrayList<TipoPagoFLBean>();
			StructDescriptor estructuraTiposPago = StructDescriptor.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"),oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"),oconn);
			for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
				if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {

					String[] arregloTarjetasIdBus = tipoPago
							.getPaPagoTarjetaIdBus().split("\\|");
					for (String tarjetaIdBus : arregloTarjetasIdBus) {
						PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
						marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION
								.obtenerParametro("venta.proceso.4.id")));
						marcadoVenta
								.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
										.obtenerParametro("venta.proceso.4.estatus.tarjeta.5")));
						marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
						marcadoVenta.setTiendaId(_peticion.getTiendaId());
						marcadoVenta.setPaPagoTarjetaIdBus(Long
								.valueOf(tarjetaIdBus));
						marcadoVenta.setPaComision(tipoPago
								.getImporteAdicional());
						RespuestaMarcadoPagoTarjetaFLBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
						if (respuestaMarcado.getPaCdgError() != 0) {
							logeo.log("Resultado del marcado de pago con tarjeta:"
									+ marcadoVenta);
						}

					}

				}
				tiposPagoLista.add(tipoPago);

			}
			contador = 0;
			Object[] tiposPago = new Object[tiposPagoLista.size()];
			for (TipoPagoFLBean tipoPago : tiposPagoLista) {
				Object[] objeto = new Object[] { tipoPago.getFiTipoPagoId(),
						tipoPago.getFnMontoPago(), tipoPago.getFnNumeroVales(),
						tipoPago.getImporteAdicional() };
				tiposPago[contador] = new STRUCT(estructuraTiposPago, oconn,
						objeto);
				contador++;
			}
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,tiposPago);

			cstmt.setString	(1, _peticion.getPaTerminal());
			cstmt.setLong	(2, _peticion.getPaPaisId());
			cstmt.setLong	(3, _peticion.getTiendaId());
			cstmt.setLong	(4, _peticion.getPaUsuarioId());
			cstmt.setLong	(5, _peticion.getPaUsuarioAutorizaId());
			cstmt.setString	(6, _peticion.getPaFechaOper());
			cstmt.setLong	(7, _peticion.getPaTipoMovto());
			cstmt.setDouble	(8, _peticion.getPaMontoTotalVta());
			cstmt.setARRAY	(9, arregloArticulos);
			cstmt.setARRAY	(10, arregloTiposPago);
			cstmt.setInt	(11, _peticion.getPaTipoDevolucion());
			cstmt.setLong	(12, 0);
			cstmt.setLong	(13, _peticion.getPaMovimientoIdDev());
			cstmt.setLong	(14, _peticion.getPaConciliacionId());
			cstmt.setInt	(15, _peticion.getFueraLinea());
			cstmt.registerOutParameter(16, OracleTypes.NUMBER);
			cstmt.registerOutParameter(17, OracleTypes.CURSOR);
			cstmt.registerOutParameter(18, OracleTypes.NUMBER);
			cstmt.registerOutParameter(19, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(18)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(19)));
			respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(16)));
			logeo.log("Salida:" + respuesta.toString());
			if (respuesta.getPaTransaccionId() == 0
					&& respuesta.getPaCdgError() == 0) {
				respuesta.setPaCdgError(Integer.valueOf(ConfigSION
						.obtenerParametro("transaccion.cero.codigo")));
				respuesta.setPaDescError(ConfigSION
						.obtenerParametro("transaccion.cero.mensaje"));
			}
			if (respuesta.getPaCdgError() == 0
					&& respuesta.getPaTransaccionId() != 0) {
				respuesta.setPaDescError(respuesta.getPaDescError() + "|"
						+ _peticion.getPaConciliacionId());
			}
			if (respuesta.getPaCdgError() == 0) {
				bloqueos = cstmt.getCursor(17);

				ArrayList<BloqueoBean> bloqueosList = new ArrayList<BloqueoBean>();
				while (bloqueos.next()) {
					BloqueoBean bloqueoBean = new BloqueoBean();
					bloqueoBean.setFiTipoPagoId(bloqueos.getInt(1));
					bloqueoBean.setFiNumAvisos(bloqueos.getInt(2));
					bloqueoBean.setFiAvisosFalt(bloqueos.getInt(3));
					bloqueoBean.setFiEstatusBloqueoId(bloqueos.getInt(4));
					bloqueosList.add(bloqueoBean);
				}

				if (bloqueosList.size() != 0) {
					BloqueoBean[] bloqueosBean = new BloqueoBean[bloqueosList
							.size()];
					bloqueosList.toArray(bloqueosBean);
					respuesta.setBloqueos(bloqueosBean);
				} else {
					respuesta.setBloqueos(null);
				}

				for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
						String[] arregloTarjetasIdBus = tipoPago
								.getPaPagoTarjetaIdBus().split("\\|");
						for (String tarjetaIdBus : arregloTarjetasIdBus) {

							PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
							marcadoVenta
									.setPaProcesoExc(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.3.id")));
							marcadoVenta
									.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.3.estatus.tarjeta.7")));
							marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
							marcadoVenta.setTiendaId(_peticion.getTiendaId());
							marcadoVenta.setPaPagoTarjetaIdBus(Long
									.valueOf(tarjetaIdBus));
							marcadoVenta.setPaNumTransaccion(respuesta
									.getPaTransaccionId());
							marcadoVenta.setPaTipoPagoId(tipoPago
									.getFiTipoPagoId());
							RespuestaMarcadoPagoTarjetaFLBean actualizacionMarcado = marcadoPagoTarjeta(marcadoVenta);
							if (actualizacionMarcado.getPaCdgError() != 0) {
								logeo.log("Resultado del marcado de pago con tarjeta:"
										+ marcadoVenta);
							}
						}
					}
				}
			} else {
				for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
						String[] arregloTarjetasIdBus = tipoPago
								.getPaPagoTarjetaIdBus().split("\\|");
						for (String tarjetaIdBus : arregloTarjetasIdBus) {

							PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
							marcadoVenta
									.setPaProcesoExc(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.4.id")));
							marcadoVenta
									.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.4.estatus.tarjeta.6")));
							marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
							marcadoVenta.setTiendaId(_peticion.getTiendaId());
							marcadoVenta.setPaPagoTarjetaIdBus(Long
									.valueOf(tarjetaIdBus));
							RespuestaMarcadoPagoTarjetaFLBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
							if (respuestaMarcado.getPaCdgError() != 0) {
								logeo.log("Resultado del marcado de pago con tarjeta:"
										+ marcadoVenta);

							}
						}
					}
				}
			}
			logeo.log("Respuesta de la peticion de registro de venta fuera de linea:"
					+ respuesta.toString());
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			respuesta.setPaCdgError(1);
			respuesta.setPaDescError(e.getLocalizedMessage() + e.getClass());
		} finally {
			try {
				Conexion.cerrarConexion(conn);
				if (bloqueos != null && !bloqueos.isClosed())
					Conexion.cerrarResultSet(bloqueos);
				Conexion.cerrarCallableStatement(cstmt);
				Conexion.cerrarConexion(oconn);
				// Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
			} catch (SQLException ex) {
				logeo.logearExcepcion(
						ex,
						"Ocurrio un error al cerrar los recursos de BD "
								+ ex.getMessage());
			}
		}
		return respuesta;
	}
	
	/***
	 * Realiza una llamada al procedimiento almacenado encargado de procesar las
	 * ventas por tienda.
	 * 
	 * @param procesar
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaProcesaXTiendaBean procesaVentaXTienda(PeticionProcesaXTiendaBean _peticion)
			throws AxisFault {
		RespuestaProcesaXTiendaBean respuesta = new RespuestaProcesaXTiendaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		logeo.log("Peticion :" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.FUERALINEA.SPPROCESAVENTASXTIENDA");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setString(1,_peticion.getFecha());
			cstmt.setLong(2,_peticion.getTiendaid());
			cstmt.registerOutParameter(3, OracleTypes.NUMBER);
			cstmt.registerOutParameter(4, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(3)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(4)));
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new AxisFault(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarConexion(conn);
			Conexion.cerrarRecursos(oconn, cstmt, null);
		}

		return respuesta;
	}

	/***
	 * Realiza una llamada al procedimiento almacenado encargado de procesar las
	 * devoluciones por tienda.
	 * 
	 * @param procesar
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaProcesaXTiendaBean procesaDevolucionesXTienda(PeticionProcesaXTiendaBean _peticion)
			throws AxisFault {
		RespuestaProcesaXTiendaBean respuesta = new RespuestaProcesaXTiendaBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		logeo.log("Peticion para procesar devoluciones :" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.FUERALINEA.SPPROCESADEVOLUCIONESXTIENDA");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setString(1,_peticion.getFecha());
			cstmt.setLong(2,_peticion.getTiendaid());
			cstmt.registerOutParameter(3, OracleTypes.NUMBER);
			cstmt.registerOutParameter(4, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(3)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(4)));
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			throw new AxisFault(e.getLocalizedMessage(), e);
		} finally {
			Conexion.cerrarConexion(conn);
			Conexion.cerrarRecursos(oconn, cstmt, null);
		}

		return respuesta;
	}

	//M�todos con IEPS
	
	public RespuestaVentaFLBean registrarVentaAct(PeticionActVentaFLBean _peticion)
			throws AxisFault {

		RespuestaVentaFLBean respuesta = new RespuestaVentaFLBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		logeo.log("Peticion de registro de venta:" + _peticion.toString());
		try {
			sql = ConfigSION
					.obtenerParametro("VENTA.SPSERVICIOVENTAFLTD");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

			Object[] articulos = new Object[_peticion.getArticulos().length];
			StructDescriptor estructuraArticulo = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOARTICULO.TIPODESCTO"),
							oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOARTICULOS.TIPODESCTO"),
							oconn);
			int contador = 0;
			for (ArticuloActFLBean articulo : _peticion.getArticulos()) {
				Object[] objeto = new Object[] { 
						 articulo.getFiArticuloId(),
						 articulo.getFcCdGbBarras(),
						 articulo.getFnCantidad(),
						 articulo.getFnPrecio(),
						 articulo.getFnCosto(),
						 articulo.getFnDescuento(),
						 articulo.getFnIva(),
						 articulo.getFiIeps(),
						 articulo.getFiTipoDescuento()
						};
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,
						objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,
					articulos);
			ArrayList<TipoPagoFLBean> tiposPagoLista = new ArrayList<TipoPagoFLBean>();
			StructDescriptor estructuraTiposPago = StructDescriptor
					.createDescriptor(ConfigSION
							.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"),
							oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor
					.createDescriptor(
							ConfigSION
									.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"),
							oconn);
			for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
				if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {

					String[] arregloTarjetasIdBus = tipoPago
							.getPaPagoTarjetaIdBus().split("\\|");
					for (String tarjetaIdBus : arregloTarjetasIdBus) {
						PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
						marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION
								.obtenerParametro("venta.proceso.4.id")));
						marcadoVenta
								.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
										.obtenerParametro("venta.proceso.4.estatus.tarjeta.5")));
						marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
						marcadoVenta.setTiendaId(_peticion.getTiendaId());
						marcadoVenta.setPaPagoTarjetaIdBus(Long
								.valueOf(tarjetaIdBus));
						marcadoVenta.setPaComision(tipoPago
								.getImporteAdicional());
						RespuestaMarcadoPagoTarjetaFLBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
						if (respuestaMarcado.getPaCdgError() != 0) {
							logeo.log("Resultado del marcado de pago con tarjeta:"
									+ marcadoVenta);
						}

					}

				}
				tiposPagoLista.add(tipoPago);

			}
			contador = 0;
			Object[] tiposPago = new Object[tiposPagoLista.size()];
			for (TipoPagoFLBean tipoPago : tiposPagoLista) {
				Object[] objeto = new Object[] { tipoPago.getFiTipoPagoId(),
						tipoPago.getFnMontoPago(), tipoPago.getFnNumeroVales(),
						tipoPago.getImporteAdicional() };
				tiposPago[contador] = new STRUCT(estructuraTiposPago, oconn,
						objeto);
				contador++;
			}
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,
					tiposPago);

			cstmt.setString	(1, _peticion.getPaTerminal());
			cstmt.setLong	(2, _peticion.getPaPaisId());
			cstmt.setLong	(3, _peticion.getTiendaId());
			cstmt.setLong	(4, _peticion.getPaUsuarioId());
			cstmt.setLong	(5, _peticion.getPaUsuarioAutorizaId());
			cstmt.setString	(6, _peticion.getPaFechaOper());
			cstmt.setLong	(7, _peticion.getPaTipoMovto());
			cstmt.setDouble	(8, _peticion.getPaMontoTotalVta());
			cstmt.setARRAY	(9, arregloArticulos);
			cstmt.setARRAY	(10, arregloTiposPago);
			cstmt.setInt	(11, _peticion.getPaTipoDevolucion());
			cstmt.setLong	(12, 0);
			cstmt.setLong	(13, _peticion.getPaMovimientoIdDev());
			cstmt.setLong	(14, _peticion.getPaConciliacionId());
			cstmt.setInt	(15, _peticion.getFueraLinea());
			cstmt.registerOutParameter(16, OracleTypes.NUMBER);
			cstmt.registerOutParameter(17, OracleTypes.CURSOR);
			cstmt.registerOutParameter(18, OracleTypes.NUMBER);
			cstmt.registerOutParameter(19, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(18)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(19)));
			respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(16)));
			logeo.log("Salida:" + respuesta.toString());
			if (respuesta.getPaTransaccionId() == 0
					&& respuesta.getPaCdgError() == 0) {
				respuesta.setPaCdgError(Integer.valueOf(ConfigSION
						.obtenerParametro("transaccion.cero.codigo")));
				respuesta.setPaDescError(ConfigSION
						.obtenerParametro("transaccion.cero.mensaje"));
			}
			if (respuesta.getPaCdgError() == 0
					&& respuesta.getPaTransaccionId() != 0) {
				respuesta.setPaDescError(respuesta.getPaDescError() + "|"
						+ _peticion.getPaConciliacionId());
			}
			if (respuesta.getPaCdgError() == 0) {
				bloqueos = cstmt.getCursor(17);
				for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
						String[] arregloTarjetasIdBus = tipoPago
								.getPaPagoTarjetaIdBus().split("\\|");
						for (String tarjetaIdBus : arregloTarjetasIdBus) {

							PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
							marcadoVenta
									.setPaProcesoExc(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.3.id")));
							marcadoVenta
									.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.3.estatus.tarjeta.7")));
							marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
							marcadoVenta.setTiendaId(_peticion.getTiendaId());
							marcadoVenta.setPaPagoTarjetaIdBus(Long
									.valueOf(tarjetaIdBus));
							marcadoVenta.setPaNumTransaccion(respuesta
									.getPaTransaccionId());
							marcadoVenta.setPaTipoPagoId(tipoPago
									.getFiTipoPagoId());
							RespuestaMarcadoPagoTarjetaFLBean actualizacionMarcado = marcadoPagoTarjeta(marcadoVenta);
							if (actualizacionMarcado.getPaCdgError() != 0) {
								logeo.log("Resultado del marcado de pago con tarjeta:"
										+ marcadoVenta);
							}
						}
					}
				}
			} else {
				for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
						String[] arregloTarjetasIdBus = tipoPago
								.getPaPagoTarjetaIdBus().split("\\|");
						for (String tarjetaIdBus : arregloTarjetasIdBus) {

							PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
							marcadoVenta
									.setPaProcesoExc(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.4.id")));
							marcadoVenta
									.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.4.estatus.tarjeta.6")));
							marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
							marcadoVenta.setTiendaId(_peticion.getTiendaId());
							marcadoVenta.setPaPagoTarjetaIdBus(Long
									.valueOf(tarjetaIdBus));
							RespuestaMarcadoPagoTarjetaFLBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
							if (respuestaMarcado.getPaCdgError() != 0) {
								logeo.log("Resultado del marcado de pago con tarjeta:"
										+ marcadoVenta);

							}
						}
					}
				}
			}
			logeo.log("Respuesta de la peticion de registro de venta fuera de linea:"
					+ respuesta.toString());
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			respuesta.setPaCdgError(1);
			respuesta.setPaDescError(e.getLocalizedMessage() + e.getClass());
		} finally {
			Conexion.cerrarConexion(conn);
			Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
		}
		return respuesta;
	}

	/*
	 * M�todo usado para marcar una venta fuera de l�nea, este m�todo inserta valor por default 
	 * en estatus de lectura tarjetas - quitar cuando implemntacion de chip se encuentre en generico
	 */
	public RespuestaVentaFLBloqueoBean registrarVentaBloqueosAct(PeticionActVentaFLBean _peticion) throws AxisFault {
		RespuestaVentaFLBloqueoBean respuesta = new RespuestaVentaFLBloqueoBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		logeo.log("Peticion de registro de venta:" + _peticion.toString());
		try {
			sql = ConfigSION
					.obtenerParametro("VENTA.SPSERVICIOVENTAFLTD");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			
			Object[] articulos = new Object[_peticion.getArticulos().length];
			StructDescriptor estructuraArticulo = StructDescriptor.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOARTICULO.TIPODESCTO"),oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOARREGLOARTICULOS.TIPODESCTO"),oconn);
			int contador = 0;
			for (ArticuloActFLBean articulo : _peticion.getArticulos()) {
				Object[] objeto = new Object[] { 
						articulo.getFiArticuloId()
						,articulo.getFcCdGbBarras()
						,articulo.getFnCantidad()
						,articulo.getFnPrecio()
						,articulo.getFnCosto()
						,articulo.getFnDescuento()
						,articulo.getFnIva()
						,articulo.getFiIeps()
						,articulo.getFiTipoDescuento()
						};
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,articulos);
			
			
			ArrayList<TipoPagoFLBean> tiposPagoLista = new ArrayList<TipoPagoFLBean>();
			StructDescriptor estructuraTiposPago = StructDescriptor.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"),oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"),oconn);
			for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
				if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {

					String[] arregloTarjetasIdBus = tipoPago
							.getPaPagoTarjetaIdBus().split("\\|");
					for (String tarjetaIdBus : arregloTarjetasIdBus) {
						PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
						marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION
								.obtenerParametro("venta.proceso.4.id")));
						marcadoVenta
								.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
										.obtenerParametro("venta.proceso.4.estatus.tarjeta.5")));
						marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
						marcadoVenta.setTiendaId(_peticion.getTiendaId());
						marcadoVenta.setPaPagoTarjetaIdBus(Long
								.valueOf(tarjetaIdBus));
						marcadoVenta.setPaComision(tipoPago
								.getImporteAdicional());
						RespuestaMarcadoPagoTarjetaFLBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
						if (respuestaMarcado.getPaCdgError() != 0) {
							logeo.log("Resultado del marcado de pago con tarjeta:"
									+ marcadoVenta);
						}

					}

				}
				tiposPagoLista.add(tipoPago);

			}
			contador = 0;
			Object[] tiposPago = new Object[tiposPagoLista.size()];
			for (TipoPagoFLBean tipoPago : tiposPagoLista) {
				Object[] objeto = new Object[] { tipoPago.getFiTipoPagoId(),
						tipoPago.getFnMontoPago(), tipoPago.getFnNumeroVales(),
						tipoPago.getImporteAdicional() };
				tiposPago[contador] = new STRUCT(estructuraTiposPago, oconn,
						objeto);
				contador++;
			}
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,tiposPago);

			cstmt.setString	(1, _peticion.getPaTerminal());
			cstmt.setLong	(2, _peticion.getPaPaisId());
			cstmt.setLong	(3, _peticion.getTiendaId());
			cstmt.setLong	(4, _peticion.getPaUsuarioId());
			cstmt.setLong	(5, _peticion.getPaUsuarioAutorizaId());
			cstmt.setString	(6, _peticion.getPaFechaOper());
			cstmt.setLong	(7, _peticion.getPaTipoMovto());
			cstmt.setDouble	(8, _peticion.getPaMontoTotalVta());
			cstmt.setARRAY	(9, arregloArticulos);
			cstmt.setARRAY	(10, arregloTiposPago);
			cstmt.setInt	(11, _peticion.getPaTipoDevolucion());
			cstmt.setLong	(12, 0);
			cstmt.setLong	(13, _peticion.getPaMovimientoIdDev());
			cstmt.setLong	(14, _peticion.getPaConciliacionId());
			cstmt.setInt	(15, _peticion.getFueraLinea());
			cstmt.registerOutParameter(16, OracleTypes.NUMBER);
			cstmt.registerOutParameter(17, OracleTypes.CURSOR);
			cstmt.registerOutParameter(18, OracleTypes.NUMBER);
			cstmt.registerOutParameter(19, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(18)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(19)));
			respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(16)));
			logeo.log("Salida:" + respuesta.toString());
			if (respuesta.getPaTransaccionId() == 0
					&& respuesta.getPaCdgError() == 0) {
				respuesta.setPaCdgError(Integer.valueOf(ConfigSION
						.obtenerParametro("transaccion.cero.codigo")));
				respuesta.setPaDescError(ConfigSION
						.obtenerParametro("transaccion.cero.mensaje"));
			}
			if (respuesta.getPaCdgError() == 0
					&& respuesta.getPaTransaccionId() != 0) {
				respuesta.setPaDescError(respuesta.getPaDescError() + "|"
						+ _peticion.getPaConciliacionId());
			}
			if (respuesta.getPaCdgError() == 0) {
				bloqueos = cstmt.getCursor(17);

				ArrayList<BloqueoBean> bloqueosList = new ArrayList<BloqueoBean>();
				while (bloqueos.next()) {
					BloqueoBean bloqueoBean = new BloqueoBean();
					bloqueoBean.setFiTipoPagoId(bloqueos.getInt(1));
					bloqueoBean.setFiNumAvisos(bloqueos.getInt(2));
					bloqueoBean.setFiAvisosFalt(bloqueos.getInt(3));
					bloqueoBean.setFiEstatusBloqueoId(bloqueos.getInt(4));
					bloqueosList.add(bloqueoBean);
				}

				if (bloqueosList.size() != 0) {
					BloqueoBean[] bloqueosBean = new BloqueoBean[bloqueosList
							.size()];
					bloqueosList.toArray(bloqueosBean);
					respuesta.setBloqueos(bloqueosBean);
				} else {
					respuesta.setBloqueos(null);
				}

				for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
						String[] arregloTarjetasIdBus = tipoPago
								.getPaPagoTarjetaIdBus().split("\\|");
						for (String tarjetaIdBus : arregloTarjetasIdBus) {

							PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
							marcadoVenta
									.setPaProcesoExc(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.3.id")));
							marcadoVenta
									.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.3.estatus.tarjeta.7")));
							marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
							marcadoVenta.setTiendaId(_peticion.getTiendaId());
							marcadoVenta.setPaPagoTarjetaIdBus(Long
									.valueOf(tarjetaIdBus));
							marcadoVenta.setPaNumTransaccion(respuesta
									.getPaTransaccionId());
							marcadoVenta.setPaTipoPagoId(tipoPago
									.getFiTipoPagoId());
							RespuestaMarcadoPagoTarjetaFLBean actualizacionMarcado = marcadoPagoTarjeta(marcadoVenta);
							if (actualizacionMarcado.getPaCdgError() != 0) {
								logeo.log("Resultado del marcado de pago con tarjeta:"
										+ marcadoVenta);
							}
						}
					}
				}
			} else {
				for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
						String[] arregloTarjetasIdBus = tipoPago
								.getPaPagoTarjetaIdBus().split("\\|");
						for (String tarjetaIdBus : arregloTarjetasIdBus) {

							PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
							marcadoVenta
									.setPaProcesoExc(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.4.id")));
							marcadoVenta
									.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.4.estatus.tarjeta.6")));
							marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
							marcadoVenta.setTiendaId(_peticion.getTiendaId());
							marcadoVenta.setPaPagoTarjetaIdBus(Long
									.valueOf(tarjetaIdBus));
							RespuestaMarcadoPagoTarjetaFLBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
							if (respuestaMarcado.getPaCdgError() != 0) {
								logeo.log("Resultado del marcado de pago con tarjeta:"
										+ marcadoVenta);

							}
						}
					}
				}
			}
			logeo.log("Respuesta de la peticion de registro de venta fuera de linea:"
					+ respuesta.toString());
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			respuesta.setPaCdgError(1);
			respuesta.setPaDescError(e.getLocalizedMessage() + e.getClass());
		} finally {
			try {
				Conexion.cerrarConexion(conn);
				if (bloqueos != null && !bloqueos.isClosed())
					Conexion.cerrarResultSet(bloqueos);
				Conexion.cerrarCallableStatement(cstmt);
				Conexion.cerrarConexion(oconn);
				// Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
			} catch (SQLException ex) {
				logeo.logearExcepcion(
						ex,
						"Ocurrio un error al cerrar los recursos de BD "
								+ ex.getMessage());
			}
		}
		return respuesta;
	}
	
	//M�todos chip
		
	/*
	 * Este m�todo se usa para grabar una venta fuera de l�nea - este m�todo recibe desde tienda el estatus de lectura de tarjetas 
	 * para grabarlo en central, se manda llamar por las tiendas con implementacion de lectura de chip
	 */
	public RespuestaVentaFLBloqueoBean registrarVentaBloqueosAct(PeticionVentaBean _peticion) throws AxisFault {
		RespuestaVentaFLBloqueoBean respuesta = new RespuestaVentaFLBloqueoBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		ARRAY arregloArticulos = null;
		ARRAY arregloTiposPago = null;
		ResultSet bloqueos = null;
		logeo.log("Peticion de registro de venta:" + _peticion.toString());
		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPSERVICIOVENTAFLTD");
			conn = obtenerConexion();
			oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);

			Object[] articulos = new Object[_peticion.getArticulos().length];
			
			StructDescriptor estructuraArticulo = StructDescriptor.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOARTICULO.TIPODESCTO"),oconn);
			ArrayDescriptor descriptorArregloArticulos = ArrayDescriptor.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOARREGLOARTICULOS.TIPODESCTO"),oconn);
			int contador = 0;
			for (ArticuloActFLBean articulo : _peticion.getArticulos()) {
				Object[] objeto = new Object[] { 
						articulo.getFiArticuloId()
						,articulo.getFcCdGbBarras()
						,articulo.getFnCantidad()
						,articulo.getFnPrecio()
						,articulo.getFnCosto()
						,articulo.getFnDescuento()
						,articulo.getFnIva()
						,articulo.getFiIeps()
						,articulo.getFiTipoDescuento()
						};
				articulos[contador] = new STRUCT(estructuraArticulo, oconn,objeto);
				contador++;
			}
			arregloArticulos = new ARRAY(descriptorArregloArticulos, oconn,articulos);
						
			ArrayList<TipoPagoFLBean> tiposPagoLista = new ArrayList<TipoPagoFLBean>();
			StructDescriptor estructuraTiposPago = StructDescriptor.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOTIPOPAGOORACLE"),oconn);
			ArrayDescriptor descriptorArregloTiposPago = ArrayDescriptor.createDescriptor(ConfigSION.obtenerParametro("VENTA.OBJETOARREGLOTIPOSPAGOORACLE"),oconn);
			for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
				if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {

					String[] arregloEstatusLecturaTarjetas = _peticion.getEstatusLecturaTarjetas().split("\\|");
					String[] arregloTarjetasIdBus = tipoPago.getPaPagoTarjetaIdBus().split("\\|");
					int contadorPagos = 0;
					
					for (String tarjetaIdBus : arregloTarjetasIdBus) {
						PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
						marcadoVenta.setPaProcesoExc(Integer.valueOf(ConfigSION.obtenerParametro("venta.proceso.4.id")));
						marcadoVenta.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION.obtenerParametro("venta.proceso.4.estatus.tarjeta.5")));
						marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
						marcadoVenta.setTiendaId(_peticion.getTiendaId());
						marcadoVenta.setPaPagoTarjetaIdBus(Long.valueOf(tarjetaIdBus));
						marcadoVenta.setPaComision(tipoPago.getImporteAdicional());
						marcadoVenta.setPaEstatusTarjetaId(Integer.parseInt(arregloEstatusLecturaTarjetas[contadorPagos]));
						RespuestaMarcadoPagoTarjetaFLBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
						if (respuestaMarcado.getPaCdgError() != 0) {
							logeo.log("Resultado del marcado de pago con tarjeta:"+ marcadoVenta);
						}
						contadorPagos++;
					}

				}
				tiposPagoLista.add(tipoPago);

			}
			contador = 0;
			Object[] tiposPago = new Object[tiposPagoLista.size()];
			for (TipoPagoFLBean tipoPago : tiposPagoLista) {
				Object[] objeto = new Object[] { tipoPago.getFiTipoPagoId(),
						tipoPago.getFnMontoPago(), tipoPago.getFnNumeroVales(),
						tipoPago.getImporteAdicional() };
				tiposPago[contador] = new STRUCT(estructuraTiposPago, oconn,
						objeto);
				contador++;
			}
			arregloTiposPago = new ARRAY(descriptorArregloTiposPago, oconn,tiposPago);

			cstmt.setString	(1, _peticion.getPaTerminal());
			cstmt.setLong	(2, _peticion.getPaPaisId());
			cstmt.setLong	(3, _peticion.getTiendaId());
			cstmt.setLong	(4, _peticion.getPaUsuarioId());
			cstmt.setLong	(5, _peticion.getPaUsuarioAutorizaId());
			cstmt.setString	(6, _peticion.getPaFechaOper());
			cstmt.setLong	(7, _peticion.getPaTipoMovto());
			cstmt.setDouble	(8, _peticion.getPaMontoTotalVta());
			cstmt.setARRAY	(9, arregloArticulos);
			cstmt.setARRAY	(10, arregloTiposPago);
			cstmt.setInt	(11, _peticion.getPaTipoDevolucion());
			cstmt.setLong	(12, 0);
			cstmt.setLong	(13, _peticion.getPaMovimientoIdDev());
			cstmt.setLong	(14, _peticion.getPaConciliacionId());
			cstmt.setInt	(15, _peticion.getFueraLinea());
			cstmt.registerOutParameter(16, OracleTypes.NUMBER);
			cstmt.registerOutParameter(17, OracleTypes.CURSOR);
			cstmt.registerOutParameter(18, OracleTypes.NUMBER);
			cstmt.registerOutParameter(19, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setPaCdgError(Integer.valueOf(cstmt.getInt(18)));
			respuesta.setPaDescError(String.valueOf(cstmt.getString(19)));
			respuesta.setPaTransaccionId(Long.valueOf(cstmt.getLong(16)));
			logeo.log("Salida:" + respuesta.toString());
			if (respuesta.getPaTransaccionId() == 0
					&& respuesta.getPaCdgError() == 0) {
				respuesta.setPaCdgError(Integer.valueOf(ConfigSION
						.obtenerParametro("transaccion.cero.codigo")));
				respuesta.setPaDescError(ConfigSION
						.obtenerParametro("transaccion.cero.mensaje"));
			}
			if (respuesta.getPaCdgError() == 0
					&& respuesta.getPaTransaccionId() != 0) {
				respuesta.setPaDescError(respuesta.getPaDescError() + "|"
						+ _peticion.getPaConciliacionId());
			}
			if (respuesta.getPaCdgError() == 0) {
				bloqueos = cstmt.getCursor(17);

				ArrayList<BloqueoBean> bloqueosList = new ArrayList<BloqueoBean>();
				while (bloqueos.next()) {
					BloqueoBean bloqueoBean = new BloqueoBean();
					bloqueoBean.setFiTipoPagoId(bloqueos.getInt(1));
					bloqueoBean.setFiNumAvisos(bloqueos.getInt(2));
					bloqueoBean.setFiAvisosFalt(bloqueos.getInt(3));
					bloqueoBean.setFiEstatusBloqueoId(bloqueos.getInt(4));
					bloqueosList.add(bloqueoBean);
				}

				if (bloqueosList.size() != 0) {
					BloqueoBean[] bloqueosBean = new BloqueoBean[bloqueosList
							.size()];
					bloqueosList.toArray(bloqueosBean);
					respuesta.setBloqueos(bloqueosBean);
				} else {
					respuesta.setBloqueos(null);
				}

				for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
						String[] arregloTarjetasIdBus = tipoPago
								.getPaPagoTarjetaIdBus().split("\\|");
						for (String tarjetaIdBus : arregloTarjetasIdBus) {

							PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
							marcadoVenta
									.setPaProcesoExc(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.3.id")));
							marcadoVenta
									.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.3.estatus.tarjeta.7")));
							marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
							marcadoVenta.setTiendaId(_peticion.getTiendaId());
							marcadoVenta.setPaPagoTarjetaIdBus(Long
									.valueOf(tarjetaIdBus));
							marcadoVenta.setPaNumTransaccion(respuesta
									.getPaTransaccionId());
							marcadoVenta.setPaTipoPagoId(tipoPago
									.getFiTipoPagoId());
							RespuestaMarcadoPagoTarjetaFLBean actualizacionMarcado = marcadoPagoTarjeta(marcadoVenta);
							if (actualizacionMarcado.getPaCdgError() != 0) {
								logeo.log("Resultado del marcado de pago con tarjeta:"
										+ marcadoVenta);
							}
						}
					}
				}
			} else {
				for (TipoPagoFLBean tipoPago : _peticion.getTiposPago()) {
					if (tipoPago.getFiTipoPagoId() == TARJETA_ID) {
						String[] arregloTarjetasIdBus = tipoPago
								.getPaPagoTarjetaIdBus().split("\\|");
						for (String tarjetaIdBus : arregloTarjetasIdBus) {

							PeticionMarcadoPagoTarjetaFLBean marcadoVenta = new PeticionMarcadoPagoTarjetaFLBean();
							marcadoVenta
									.setPaProcesoExc(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.4.id")));
							marcadoVenta
									.setPaEstatusTarjetaId(Integer.valueOf(ConfigSION
											.obtenerParametro("venta.proceso.4.estatus.tarjeta.6")));
							marcadoVenta.setPaPaisId(_peticion.getPaPaisId());
							marcadoVenta.setTiendaId(_peticion.getTiendaId());
							marcadoVenta.setPaPagoTarjetaIdBus(Long
									.valueOf(tarjetaIdBus));
							RespuestaMarcadoPagoTarjetaFLBean respuestaMarcado = marcadoPagoTarjeta(marcadoVenta);
							if (respuestaMarcado.getPaCdgError() != 0) {
								logeo.log("Resultado del marcado de pago con tarjeta:"
										+ marcadoVenta);

							}
						}
					}
				}
			}
			logeo.log("Respuesta de la peticion de registro de venta fuera de linea:"
					+ respuesta.toString());
		} catch (Exception e) {
			logeo.logearExcepcion(e, sql, _peticion.toString());
			respuesta.setPaCdgError(1);
			respuesta.setPaDescError(e.getLocalizedMessage() + e.getClass());
		} finally {
			try {
				Conexion.cerrarConexion(conn);
				if (bloqueos != null && !bloqueos.isClosed())
					Conexion.cerrarResultSet(bloqueos);
				Conexion.cerrarCallableStatement(cstmt);
				Conexion.cerrarConexion(oconn);
				// Conexion.cerrarRecursos(oconn, cstmt, bloqueos);
			} catch (SQLException ex) {
				logeo.logearExcepcion(
						ex,
						"Ocurrio un error al cerrar los recursos de BD "
								+ ex.getMessage());
			}
		}
		return respuesta;
	}

}
