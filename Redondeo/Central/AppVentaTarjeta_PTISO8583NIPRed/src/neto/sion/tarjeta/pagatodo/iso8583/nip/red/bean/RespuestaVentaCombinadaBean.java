package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

public class RespuestaVentaCombinadaBean {
	private neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean respuestaVentaNormal;
	private neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaBean respuestaTarjeta;
	
	public neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean getRespuestaVentaNormal() {
		return respuestaVentaNormal;
	}
	public void setRespuestaVentaNormal(
			neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean respuestaVentaNormal) {
		this.respuestaVentaNormal = respuestaVentaNormal;
	}
	public neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaBean getRespuestaTarjeta() {
		return respuestaTarjeta;
	}
	public void setRespuestaTarjeta(
			neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaBean respuestaTarjeta) {
		this.respuestaTarjeta = respuestaTarjeta;
	}
}
