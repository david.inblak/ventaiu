package neto.sion.tarjeta.pagatodo.iso8583.nip.red.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;


import neto.sion.Logs;
import neto.sion.genericos.sql.Conexion;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionMensajesOperacionBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionRegistroPagoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaMensajesOperacionBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaRegistroPagoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.util.Utilerias;
import neto.sion.venta.ventageneral.iso8583Red.bean.WSExcepcion;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import Com.Elektra.Definition.Config.ConfigSION;

public class VentaTarjetaDao extends Conexion{
	//Logs logeo;
	private Utilerias utilerias;
	
	public VentaTarjetaDao(Utilerias _utilerias, String idLog)
	{		
		super(ConfigSION.obtenerParametro("dataSource"));
		this.utilerias = _utilerias;
		//logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
		//logeo.setIdLog(idLog);
	}
	
	/**
	 * M�todo encargado de registrar en BD un pago de tarjeta
	 * @param _peticion
	 * @param tipoLectura
	 * @return
	 */
	public RespuestaRegistroPagoBean registraPagoTarjeta(PeticionRegistroPagoBean _peticion)
	{
		
		//logeo.log("Petici�n de registro de pago en BD: " + _peticion.toString());
		
		RespuestaRegistroPagoBean respuesta = new RespuestaRegistroPagoBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;

		try
		{
			sql = ConfigSION.obtenerParametro("VENTA.SPINSPAGOSTARJETA");
			conn = obtenerConexion();

			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;

			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setInt(1, _peticion.getPaProcesoExc());
			cstmt.setInt(2, _peticion.getPaisId());
			cstmt.setLong(3, _peticion.getTiendaId());
			cstmt.setString(4, _peticion.getNumeroTarjeta());
			cstmt.setDouble(5, _peticion.getImporte());
			cstmt.setDouble(6, _peticion.getComision());
			cstmt.setLong(7, _peticion.getPagoTarjetaId());
			
			if (_peticion.getCodigoRespuestaId()!=null && _peticion.getCodigoRespuestaId().length()==4){
				cstmt.setString(8, "PTI"+_peticion.getCodigoRespuestaId().substring(1, 4));
			}else{
				cstmt.setString(8, "PTI"+_peticion.getCodigoRespuestaId());
			}
			
			
			
			cstmt.setInt(9, _peticion.getEstatusTarjetaId());
			cstmt.setString(10, _peticion.getNumeroAutorizacionBAZ());
			cstmt.setInt(11, _peticion.getTipoPagoId());
			cstmt.setLong(12, _peticion.getFolioTrasaccionVenta());
			cstmt.setInt(13, _peticion.getEstatusLecturaId());
			
			cstmt.registerOutParameter(14, OracleTypes.NUMBER);
			cstmt.registerOutParameter(15, OracleTypes.NUMBER);
			cstmt.registerOutParameter(16, OracleTypes.VARCHAR);
			
			cstmt.execute();
			
			respuesta.setDescError(cstmt.getString(16));
			respuesta.setCodigoError(cstmt.getInt(15));
			respuesta.setTrace(cstmt.getLong(14));
		}
		catch (Exception e)
		{
			//logeo.logearExcepcion(e, ">> "+sql);
			StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
			respuesta.setCodigoError(utilerias.ERROR_BD);
			respuesta.setDescError(exceptionAsString);
			respuesta.setTrace(0);
		}
		finally
		{

			Conexion.cerrarRecursos(oconn, cstmt, null);
			Conexion.cerrarConexion(conn);
		}
		
		//logeo.log("Respuesta de registro de pago en BD: " + respuesta.toString());
		
		return respuesta;
	}
	
	/**
	 * M�todo encargado de obtener en BD la descripci�n de un c�digo de error recibido de Switch
	 * @param codigo
	 * @return
	 */
	public String getMensajeRespuesta(String codigo)
	{
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;
		String descripcion = null;
		try
		{
			sql = ConfigSION.obtenerParametro("VENTA.SPSELDESCRIPCIONRESPUESTA");
			conn = obtenerConexion();
			if(conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setString(1, codigo);
			cstmt.registerOutParameter(2, OracleTypes.VARCHAR);
			cstmt.execute();
			descripcion = cstmt.getString(2);
			//logeo.log("Descripci�n de respuesta: " + descripcion);
		}
		catch (Exception e)
		{
			//logeo.logearExcepcion(e, sql, descripcion);
			descripcion = e.getMessage();
			e.printStackTrace();
		}
		finally
		{
			Conexion.cerrarRecursos(oconn, cstmt, null);
			Conexion.cerrarConexion(conn);
		}
		return descripcion;
	}
	
	/**
	 * Metodo utilizado para el registro de mensajes
	 * 
	 * @param _peticion
	 * @return un objeto con la descripcion del mensaje
	 * @throws AxisFault
	 */
	public RespuestaMensajesOperacionBean registraMensajeOperacion(
			PeticionMensajesOperacionBean _peticion) throws WSExcepcion {

		RespuestaMensajesOperacionBean respuesta = new RespuestaMensajesOperacionBean();
		Connection conn = null;
		OracleCallableStatement cstmt = null;
		OracleConnection oconn = null;
		String sql = null;

		//logeo.log("Registrando mensaje de operacion en BD: " + _peticion.toString());

		try {
			sql = ConfigSION.obtenerParametro("VENTA.SPINSMENSAJESOPERACIONES");
			conn = obtenerConexion();

			if (conn.isWrapperFor(OracleConnection.class))
				oconn = conn.unwrap(OracleConnection.class);
			else
				oconn = (OracleConnection) conn;

			// oconn = (OracleConnection) conn;
			cstmt = (OracleCallableStatement) oconn.prepareCall(sql);
			cstmt.setInt(1, _peticion.getPaisId());
			cstmt.setLong(2, _peticion.getTiendaId());
			cstmt.setInt(3, _peticion.getOperacionId());
			cstmt.setString(4, _peticion.getMensaje());
			cstmt.setLong(5, _peticion.getReferenciaId());
			cstmt.registerOutParameter(6, OracleTypes.NUMBER);
			cstmt.registerOutParameter(7, OracleTypes.NUMBER);
			cstmt.registerOutParameter(8, OracleTypes.VARCHAR);
			cstmt.execute();
			respuesta.setMensajeSalida(Long.valueOf(cstmt.getLong(6)));
			respuesta.setCodigoError(Integer.valueOf(cstmt.getInt(7)));
			respuesta.setDescError(String.valueOf(cstmt.getString(8)));

			//logeo.log("Respuesta de mensaje registrado en BD: "
			//		+ respuesta.toString());

		} catch (Exception e) {
			//logeo.logearExcepcion(e, sql,
			//		"Ocurri� un error al intentar registrar mensaje de operacion.");
			throw new WSExcepcion(e.getLocalizedMessage(), e);
		} finally {

			Conexion.cerrarRecursos(oconn, cstmt, null);
			Conexion.cerrarConexion(conn);

		}

		return respuesta;
	}
	
}
