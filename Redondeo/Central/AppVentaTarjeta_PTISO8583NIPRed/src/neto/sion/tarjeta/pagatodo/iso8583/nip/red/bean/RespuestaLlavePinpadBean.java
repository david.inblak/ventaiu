package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

/**
 * Clase que representa respuesta a una solicitud de llave que se env�a a tienda
 * 
 * @author fvegap
 * 
 */
public class RespuestaLlavePinpadBean {

	private RespuestaLlaveBAZBean respuestaBAZ;
	private long uId;
	private int codigoError;
	private String descError;

	public RespuestaLlaveBAZBean getRespuestaBAZ() {
		return respuestaBAZ;
	}

	public void setRespuestaBAZ(RespuestaLlaveBAZBean respuestaBAZ) {
		this.respuestaBAZ = respuestaBAZ;
	}

	public long getuId() {
		return uId;
	}

	public void setuId(long uId) {
		this.uId = uId;
	}

	public int getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	@Override
	public String toString() {
		return "RespuestaLlavePinpadBean [codigoError=" + codigoError
				+ ", descError=" + descError + ", respuestaBAZ=" + respuestaBAZ
				+ ", uId=" + uId + "]";
	}

}
