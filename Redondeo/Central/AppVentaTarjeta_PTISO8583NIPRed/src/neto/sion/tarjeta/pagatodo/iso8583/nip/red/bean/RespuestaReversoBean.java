package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

public class RespuestaReversoBean {

	private RespuestaReversoBAZBean respuestaBAZ;
	private long uId;
	private int codigoError;
	private String descError;

	public RespuestaReversoBAZBean getRespuestaBAZ() {
		return respuestaBAZ;
	}

	public void setRespuestaBAZ(RespuestaReversoBAZBean respuestaBAZ) {
		this.respuestaBAZ = respuestaBAZ;
	}

	public int getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	public long getuId() {
		return uId;
	}

	public void setuId(long uId) {
		this.uId = uId;
	}

	@Override
	public String toString() {
		return "RespuestaReversoBean [codigoError=" + codigoError
				+ ", descError=" + descError + ", respuestaBAZ=" + respuestaBAZ
				+ ", uId=" + uId + "]";
	}

}
