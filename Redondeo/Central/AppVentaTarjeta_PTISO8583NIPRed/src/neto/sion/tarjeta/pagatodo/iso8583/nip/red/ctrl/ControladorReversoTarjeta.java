package neto.sion.tarjeta.pagatodo.iso8583.nip.red.ctrl;

import java.net.Socket;

import Com.Elektra.Definition.Config.ConfigSION;

import neto.sion.Logs;
import neto.sion.genericos.log.Logeo;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionRegistroPagoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionReversoBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionReversoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaRegistroPagoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaReversoBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaReversoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.ctrl.ControladorVentaTarjeta.EstatusRegistroPago;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.dao.VentaTarjetaDao;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.mensajeria.GeneradorMensajePeticion;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.mensajeria.GeneradorRespuestaMensaje;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.socket.MySingletonSocket;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.socket.SocketBAZ;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.socket.SocketBAZ.TipoSolicitud;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.util.Utilerias;

public class ControladorReversoTarjeta {	
	
	private Logs logeo;
	
	private final int TIPO_PAGO_TARJETA = 2;

	private VentaTarjetaDao dao;
	private GeneradorMensajePeticion generadorMensaje;
	private GeneradorRespuestaMensaje generadorRespuesta;
	//private SocketBAZ socketBAZ;
	private Utilerias utilerias;
	
	private final int ESTATUS_REVERSO_SOLICITADO = 8;
	private final int ESTATUS_REVERSO_EXITOSO = 9;
	private final int ESTATUS_REVERSO_RECHAZADO = 10;
	private final int ESTATUS_REVERSO_ERROR = 12;
	
	private final int PROCESO_EXC_REVERSO = 4;
	
	private final int OP_REVERSO = 43;
	private final int OP_REVERSO_RESPONSE = 44;

	public ControladorReversoTarjeta(Utilerias _utilerias, String idLog) {
		this.utilerias = _utilerias;
		this.generadorMensaje = new GeneradorMensajePeticion(utilerias);
		this.generadorRespuesta = new GeneradorRespuestaMensaje(utilerias);
		this.dao = new VentaTarjetaDao(utilerias, idLog);
		logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
		logeo.setIdLog(idLog);
	}
	
	public ControladorReversoTarjeta(String idLog){
		
		this.generadorMensaje = new GeneradorMensajePeticion(utilerias);
		this.generadorRespuesta = new GeneradorRespuestaMensaje(utilerias);
		this.dao = new VentaTarjetaDao(utilerias, idLog);
		logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
		logeo.setIdLog(idLog);
		this.utilerias = new Utilerias(logeo.getIdLog());
	}
	
	/**
	 * Este m�todo se ejecuta desde TDA
	 * @param _peticionTDA
	 * @return
	 */
	public RespuestaReversoBAZBean enviarPeticionReverso(PeticionReversoBean _peticionTDA){
		RespuestaReversoBAZBean respuestaBAZ = new RespuestaReversoBAZBean();
		
		//generar peticion que se enviara a BAZ a partir de la peticion recibida desde tienda
		PeticionReversoBAZBean peticionBAZ = generadorMensaje.generarPeticionReverso(_peticionTDA);
		
		//generar cadena 420 de reverso que se va enviar a BAZ
		String msj420 = peticionBAZ.generarMensaje();
		
		// loguea peticion en BD
		try {
			utilerias.insertaMensajeOperacion(_peticionTDA.getPaisId(), _peticionTDA.getTiendaId(), OP_REVERSO, Long.parseLong(_peticionTDA.getTrace()), utilerias.generaMensaje420(peticionBAZ));
		} catch (Exception e) {
			Logeo.logearExcepcion(e,
					"Ocurri� un error al insertar mensaje de operaci�n Petici�n Carga de llave con Uid: "+_peticionTDA.getuId());
		}
		
		//enviar peticion por socket y obtiene mensaje430 de respuesta
		String mensaje430 = null;
		boolean seRecibioMensaje = false;
		try{
			SocketBAZ socketBAZ = new SocketBAZ();
			mensaje430 = socketBAZ.enviaMensaje(TipoSolicitud.SOLICITUD_REVERSO, msj420);   
			seRecibioMensaje = true;
		}catch(Exception e){
			mensaje430 = null;
		}
		
		if(seRecibioMensaje){
			//analizar mensaje del socket y generar una respuesta que se devuleva a tienda
			respuestaBAZ = generadorRespuesta.generarRespuestaReverso(mensaje430);
			
			// loguea peticion en BD
			try {
				utilerias.insertaMensajeOperacion(_peticionTDA.getPaisId(), _peticionTDA.getTiendaId(), OP_REVERSO_RESPONSE, Long.parseLong(_peticionTDA.getTrace()), utilerias.generaMensaje430(respuestaBAZ));
			} catch (Exception e) {
				Logeo.logearExcepcion(e,
						"Ocurri� un error al insertar mensaje de operaci�n Petici�n Carga de llave con Uid: "+_peticionTDA.getuId());
			}
			
		}else{
			respuestaBAZ = null;
		}
		
		return respuestaBAZ;
	}
	
	/**
	 * Este m�todo se ejecuta en el WS cuando no se logra registrar el pago en caja
	 * @param _peticionVentaBAZ
	 * @param _respuestaVentaBAZ
	 * @return
	 */
	public RespuestaReversoBAZBean enviarPeticionReverso (PeticionVentaBean _peticionTDA, long  _trace, PeticionVentaBAZBean _peticionVentaBAZ, RespuestaVentaBAZBean _respuestaVentaBAZ){
		
		RespuestaReversoBAZBean respuestaReversoBAZ = new RespuestaReversoBAZBean();
		
		//genera peticion de reverso a partir de los datos de una operacion previa, se le envian peticion y respuesta de la venta
		PeticionReversoBAZBean peticionBAZ = generadorMensaje.generarPeticionReverso(_peticionVentaBAZ, _respuestaVentaBAZ);
		
		//generar cadena 420 de reverso que se va enviar a BAZ
		String msj420 = peticionBAZ.generarMensaje();
		
		// loguea peticion en BD
		try {
			utilerias.insertaMensajeOperacion(_peticionTDA.getPaisId(), _peticionTDA.getTiendaId(), OP_REVERSO, _trace, utilerias.generaMensaje420(peticionBAZ));
		} catch (Exception e) {
			Logeo.logearExcepcion(e,
					"Ocurri� un error al insertar mensaje de operaci�n Petici�n Carga de llave con Uid: "+_peticionTDA.getuId());
		}
		
		//enviar peticion por socket y obtiene mensaje430 de respuesta
		String mensaje430 = null;
		boolean seRecibioMensaje = false;
		try{
			SocketBAZ socketBAZ = new SocketBAZ();
			mensaje430 = socketBAZ.enviaMensaje(TipoSolicitud.SOLICITUD_REVERSO, msj420);
			//socketBAZ.cerrarConexiones();
			if(mensaje430!=null){
				seRecibioMensaje = true;
			}
		}catch(Exception e){
			mensaje430 = null;
		}
		
		if(seRecibioMensaje){
			//analizar mensaje del socket y generar una respuesta que se devuleva a tienda
			respuestaReversoBAZ = generadorRespuesta.generarRespuestaReverso(mensaje430);
			
			// loguea peticion en BD
			try {
				utilerias.insertaMensajeOperacion(_peticionTDA.getPaisId(), _peticionTDA.getTiendaId(), OP_REVERSO_RESPONSE, _trace, utilerias.generaMensaje430(respuestaReversoBAZ));
			} catch (Exception e) {
				Logeo.logearExcepcion(e,
						"Ocurri� un error al insertar mensaje de operaci�n Petici�n Carga de llave con Uid: "+_peticionTDA.getuId());
			}
			
		}else{
			respuestaReversoBAZ = null;
		}
		
		return respuestaReversoBAZ;
	}
	
	/**
	 * M�todo encargado de registrar en BD la solicitud de reverso del pago
	 * @param _peticion
	 * @return
	 */
	public RespuestaRegistroPagoBean registrarPago(PeticionVentaBean _peticionTDA, long _trace){
		
		RespuestaRegistroPagoBean respuesta = new RespuestaRegistroPagoBean();
		
		//primaro armar peticion de registro en BD a partir de la peticion recibida desde TDA
		PeticionRegistroPagoBean peticionRegistro = new PeticionRegistroPagoBean();
		
		peticionRegistro.setComision(_peticionTDA.getComision()); // no se ocupa
		peticionRegistro.setEstatusLecturaId(_peticionTDA.getEstatusLecturaId());  // no se ocupa
		peticionRegistro.setFolioTrasaccionVenta(0); // 0 no se ocupa
		peticionRegistro.setImporte(_peticionTDA.getMonto()); // no se ocupa
		peticionRegistro.setNumeroAutorizacionBAZ("0"); // no se ocupa
		peticionRegistro.setNumeroTarjeta(_peticionTDA.getNumeroTarjeta()); // no se ocupa
		peticionRegistro.setPagoTarjetaId(_trace); // el mismo con el que se realizo o intento realizar el pago
		peticionRegistro.setPaisId(_peticionTDA.getPaisId()); // pais con el que se realizo el pago
		peticionRegistro.setPaProcesoExc(PROCESO_EXC_REVERSO); // porceso exc 4 necesario para el sp
		peticionRegistro.setTiendaId(_peticionTDA.getTiendaId()); // tienda con la que se realizo el pago
		peticionRegistro.setTipoPagoId(TIPO_PAGO_TARJETA); // no se ocupa
		peticionRegistro.setCodigoRespuestaId(""); // no se ocupa
		peticionRegistro.setEstatusTarjetaId(ESTATUS_REVERSO_SOLICITADO); // estatus al que se actualizara el pago
		
		//registrar en BD el pago
		
		respuesta = dao.registraPagoTarjeta(peticionRegistro);
		
		return respuesta;
		
	}
	
	public RespuestaRegistroPagoBean actualizarReverso(PeticionReversoBean _peticionTDA, EstatusRegistroPago _estatus){
		
		RespuestaRegistroPagoBean respuesta = new RespuestaRegistroPagoBean();
		
		PeticionRegistroPagoBean peticionRegistro = new PeticionRegistroPagoBean();
		
		switch(_estatus){
			case REVERSO_EXITOSO:
			
				peticionRegistro.setPaProcesoExc(PROCESO_EXC_REVERSO); 
				peticionRegistro.setEstatusTarjetaId(ESTATUS_REVERSO_EXITOSO);
			
				break;
			case REVERSO_RECHAZADO:
         
			
				peticionRegistro.setPaProcesoExc(PROCESO_EXC_REVERSO); 
				peticionRegistro.setEstatusTarjetaId(ESTATUS_REVERSO_RECHAZADO);
			
			break;
			case REVERSO_ERROR:
         
			
				peticionRegistro.setPaProcesoExc(PROCESO_EXC_REVERSO); 
				peticionRegistro.setEstatusTarjetaId(ESTATUS_REVERSO_ERROR);
			
			break;
		}
		
		peticionRegistro.setPagoTarjetaId(Long.parseLong(_peticionTDA.getTrace())); //trace obtenido en el registro del pago
		peticionRegistro.setTiendaId(_peticionTDA.getTiendaId()); // misma tienda que en el pago
		peticionRegistro.setPaisId(_peticionTDA.getPaisId()); // mismo pais que en el pago
		
		peticionRegistro.setNumeroAutorizacionBAZ(""); // no se ocupa
		peticionRegistro.setCodigoRespuestaId(_peticionTDA.getCodigoRespuesta()); // no se ocupa
		peticionRegistro.setComision(0); // no se ocupa
		peticionRegistro.setFolioTrasaccionVenta(0); // no se ocupa
		peticionRegistro.setImporte(0); // no se ocupa
		peticionRegistro.setNumeroTarjeta(""); // no se ocupa
		peticionRegistro.setEstatusLecturaId(0); // no se ocupa
		peticionRegistro.setTipoPagoId(TIPO_PAGO_TARJETA); // no se ocupa
		
		//registrar en BD el pago
		
		respuesta = dao.registraPagoTarjeta(peticionRegistro);
		
		return respuesta;
	}
	
	/**
	 * M�todo encargado de actualizar en BD un reverso registrado previamente y que ya fue enviado a BAZ, se manda llamar desde el WS
	 * @param _peticionTDA
	 * @param _respuestaRegistroBD
	 * @return
	 */
	public RespuestaRegistroPagoBean actualizarReverso(PeticionVentaBean _peticionTDA, 
													RespuestaVentaBAZBean _respuestaBAZ,
													long _trace,
													EstatusRegistroPago _estatus
													){
		
		Logeo.log("Trace por actualizar: "+_trace);
		
		RespuestaRegistroPagoBean respuesta = new RespuestaRegistroPagoBean();
		
		//se arma la peticion de actualizacion usando los datos de respuesta del registro del pago en bd
		//mas la peticion original de tda
		
		PeticionRegistroPagoBean peticionRegistro = new PeticionRegistroPagoBean();
		
		switch(_estatus){
			case REVERSO_EXITOSO:
				
				peticionRegistro.setPaProcesoExc(PROCESO_EXC_REVERSO); 
				peticionRegistro.setEstatusTarjetaId(ESTATUS_REVERSO_EXITOSO);
				
				break;
			case REVERSO_RECHAZADO:
	         
				
				peticionRegistro.setPaProcesoExc(PROCESO_EXC_REVERSO); 
				peticionRegistro.setEstatusTarjetaId(ESTATUS_REVERSO_RECHAZADO);
				
				break;
			case REVERSO_ERROR:
	         
				
				peticionRegistro.setPaProcesoExc(PROCESO_EXC_REVERSO); 
				peticionRegistro.setEstatusTarjetaId(ESTATUS_REVERSO_ERROR);
				
				break;
		}
		
		peticionRegistro.setPagoTarjetaId(_trace); //trace obtenido en el registro del pago
		peticionRegistro.setTiendaId(_peticionTDA.getTiendaId()); // misma tienda que en el pago
		peticionRegistro.setPaisId(_peticionTDA.getPaisId()); // mismo pais que en el pago
		
		peticionRegistro.setNumeroAutorizacionBAZ(""); // no se ocupa
		peticionRegistro.setCodigoRespuestaId(_respuestaBAZ.getCodigoRespuesta()); // no se ocupa
		peticionRegistro.setComision(_peticionTDA.getComision()); // no se ocupa
		peticionRegistro.setFolioTrasaccionVenta(0); // no se ocupa
		peticionRegistro.setImporte(_peticionTDA.getMonto()); // no se ocupa
		peticionRegistro.setNumeroTarjeta(_peticionTDA.getNumeroTarjeta()); // no se ocupa
		peticionRegistro.setEstatusLecturaId(_peticionTDA.getEstatusLecturaId()); // no se ocupa
		peticionRegistro.setTipoPagoId(TIPO_PAGO_TARJETA); // no se ocupa
		
		//registrar en BD el pago
		
		respuesta = dao.registraPagoTarjeta(peticionRegistro);
		
		return respuesta;
		
	}
	
	/**
	 * Se invoca desde TDA
	 * @param _respuestaReversoSwitch
	 * @return
	 */
	public RespuestaReversoBean generarRespuestaServicio(PeticionReversoBean _peticionTDA, RespuestaReversoBAZBean _respuestaReversoSwitch){
		RespuestaReversoBean respuesta = new RespuestaReversoBean();
		
		if(_respuestaReversoSwitch != null){
			
			respuesta.setCodigoError(-3);
			respuesta.setDescError("Respuesta de reverso: "+dao.getMensajeRespuesta(_respuestaReversoSwitch.getCodigoRespuesta())+". Por favor intente m�s tarde");
			respuesta.setRespuestaBAZ(_respuestaReversoSwitch);
			respuesta.setuId(_peticionTDA.getuId());
			
		}else{
			
			respuesta.setCodigoError(-4);
			respuesta.setDescError("Ocurri� un error al realizar el reverso de la tarjeta. Esta ser� tomada m�s tarde por el cron de reversos de tarjeta;");
			respuesta.setRespuestaBAZ(null);
			respuesta.setuId(_peticionTDA.getuId());
			
		}
		
		return respuesta;
	}
	
	/**
	 * Se ejecuta en flujo desde WS
	 * @param _respuestaReversoSwitch
	 * @return
	 */
	public RespuestaVentaBean generarRespuestaServicio(RespuestaVentaBAZBean _repuestaPagoSwitch, RespuestaReversoBAZBean _respuestaReversoSwitch){
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		
		if(_respuestaReversoSwitch != null){
			
			if(_respuestaReversoSwitch.getCodigoProceso().equals("00")){
				
				respuesta.setCodigoError(utilerias.REVERSO_EXITOSO);
				respuesta.setDescError("["+utilerias.REVERSO_EXITOSO+"] Ocurri� un error al aplicar el pago. Por favor intente m�s tarde.");
				respuesta.setRespuestaReversoBAZ(_respuestaReversoSwitch);
				respuesta.setRespuestaBAZ(_repuestaPagoSwitch);
				
			}else{
				
				respuesta.setCodigoError(utilerias.REVERSO_ERROR_RECHAZO_BAZ);
				respuesta.setDescError("["+utilerias.REVERSO_ERROR_RECHAZO_BAZ+"] Ocurri� un error al realizar el pago. Por favor realize consulta de tarjeta e intente m�s tarde.");
				respuesta.setRespuestaReversoBAZ(_respuestaReversoSwitch);
				respuesta.setRespuestaBAZ(_repuestaPagoSwitch);
				
			}
			
		}else{
			
			respuesta.setCodigoError(utilerias.REVRESO_ERROR_COMUNICACION_BAZ);
			respuesta.setDescError("["+utilerias.REVRESO_ERROR_COMUNICACION_BAZ+"] Ocurri� un error al aplicar el pago. Por favor realize consulta de tarjeta e intente m�s tarde.");
			respuesta.setRespuestaBAZ(_repuestaPagoSwitch);
			respuesta.setRespuestaReversoBAZ(null);
			
		}
		
		return respuesta;
	}
	
	

}
