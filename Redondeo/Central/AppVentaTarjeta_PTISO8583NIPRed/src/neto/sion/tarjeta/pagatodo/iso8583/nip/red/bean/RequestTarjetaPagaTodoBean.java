package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

import neto.sion.venta.ventageneral.iso8583Red.bean.PeticionPagatodoBean;

public class RequestTarjetaPagaTodoBean {
	private PeticionVentaBean peticionTarjeta; 
	private PeticionPagatodoBean peticionPagaTodo;
	
	public RequestTarjetaPagaTodoBean(){
		peticionTarjeta = new PeticionVentaBean();
		peticionPagaTodo = new PeticionPagatodoBean();
	}
	
	public RequestTarjetaPagaTodoBean(PeticionVentaBean peticionTarjeta,
			PeticionPagatodoBean peticionPagaTodo) {
		super();
		this.peticionTarjeta = peticionTarjeta;
		this.peticionPagaTodo = peticionPagaTodo;
	}

	public PeticionVentaBean getPeticionTarjeta() {
		return peticionTarjeta;
	}
	public void setPeticionTarjeta(PeticionVentaBean peticionTarjeta) {
		this.peticionTarjeta = peticionTarjeta;
	}
	public PeticionPagatodoBean getPeticionPagaTodo() {
		return peticionPagaTodo;
	}
	public void setPeticionPagaTodo(PeticionPagatodoBean peticionPagaTodo) {
		this.peticionPagaTodo = peticionPagaTodo;
	}
	
}
