package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

/**
 * Clase que representa mensaje 210 de respuesta de Llaves de parte BAZ,en comentario info del
 * campo en contrato de interfaz, tama�o, nombre y no. de campo
 * @author fvegap
 *
 */
public class RespuestaLlaveBAZBean {

	private String longitudMensaje210; //longitud del mensaje
	private String tipoMensaje; // 4-MSG_TYPE-MTI
	private String bitmap; // 16-PRIMARY_BIT_MAP-000
	private String codigoProceso; // 6-PROC_CODE-003
	private String montoOperacion; // 12-TRANS_AMT-004
	private String fechaOperacion; // 10-XMT_DATE_TIME-007
	private String trace; // 6-STAN-011
	private String horaLocal; // 6_LOC_TIME-012
	private String fechaLocal; // 4-LOC_DATE-013
	private String referencia; // 12-RRN-037
	private String numAutorizacion; // 6-AUTH_ID_RESP-038
	private String codigoRespuesta; // 2-RESP_CODE-039
	private String terminal; // 8-CARD_ACCPT_TRMNL_ID-041
	private String afiliacion; // 15-CARD_ACCPT_ID-042
	private String longitudTramaBanco; // 3-BANK_LEN-048
	private String tramaBanco; // Long variable indicada en
	// longitudTramaBanco-BANK-048
	private String longitudToken; // 3-Long. Token-063
	private String descToken_ER_EX; // Long variable indicada en

	// longitudToken-Valor-TokenER_ES-063

	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public String getLongitudMensaje210() {
		return longitudMensaje210;
	}

	public void setLongitudMensaje210(String longitudMensaje210) {
		this.longitudMensaje210 = longitudMensaje210;
	}

	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	public String getBitmap() {
		return bitmap;
	}

	public void setBitmap(String bitmap) {
		this.bitmap = bitmap;
	}

	public String getCodigoProceso() {
		return codigoProceso;
	}

	public void setCodigoProceso(String codigoProceso) {
		this.codigoProceso = codigoProceso;
	}

	public String getMontoOperacion() {
		return montoOperacion;
	}

	public void setMontoOperacion(String montoOperacion) {
		this.montoOperacion = montoOperacion;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	public String getHoraLocal() {
		return horaLocal;
	}

	public void setHoraLocal(String horaLocal) {
		this.horaLocal = horaLocal;
	}

	public String getFechaLocal() {
		return fechaLocal;
	}

	public void setFechaLocal(String fechaLocal) {
		this.fechaLocal = fechaLocal;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public String getLongitudTramaBanco() {
		return longitudTramaBanco;
	}

	public void setLongitudTramaBanco(String longitudTramaBanco) {
		this.longitudTramaBanco = longitudTramaBanco;
	}

	public String getTramaBanco() {
		return tramaBanco;
	}

	public void setTramaBanco(String tramaBanco) {
		this.tramaBanco = tramaBanco;
	}

	public String getLongitudToken() {
		return longitudToken;
	}

	public void setLongitudToken(String longitudToken) {
		this.longitudToken = longitudToken;
	}

	public String getDescToken_ER_EX() {
		return descToken_ER_EX;
	}

	public void setDescToken_ER_EX(String descTokenEREX) {
		descToken_ER_EX = descTokenEREX;
	}

	@Override
	public String toString() {
		return "RespuestaLlaveBAZBean [afiliacion=" + afiliacion + ", bitmap="
				+ bitmap + ", codigoProceso=" + codigoProceso
				+ ", codigoRespuesta=" + codigoRespuesta + ", descToken_ER_EX="
				+ descToken_ER_EX + ", fechaLocal=" + fechaLocal
				+ ", fechaOperacion=" + fechaOperacion + ", horaLocal="
				+ horaLocal + ", longitudMensaje210=" + longitudMensaje210
				+ ", longitudToken=" + longitudToken + ", longitudTramaBanco="
				+ longitudTramaBanco + ", montoOperacion=" + montoOperacion
				+ ", numAutorizacion=" + numAutorizacion + ", referencia="
				+ referencia + ", terminal=" + terminal + ", tipoMensaje="
				+ tipoMensaje + ", trace=" + trace + ", tramaBanco="
				+ tramaBanco + "]";
	}

}
