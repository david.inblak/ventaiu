package neto.sion.tarjeta.pagatodo.iso8583.nip.red.ctrl;

//import neto.sion.Logs;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionReversoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaRegistroPagoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaReversoBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaReversoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.ctrl.ControladorVentaTarjeta.EstatusRegistroPago;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.socket.NoSocketPagatodo;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.util.Utilerias;
import neto.sion.venta.ventageneral.iso8583Red.bean.PeticionPagatodoNipBean;
import neto.sion.venta.ventageneral.iso8583Red.bean.RespuestaPagatodoNipBean;
import neto.sion.venta.ventageneral.iso8583Red.bean.WSExcepcion;
import Com.Elektra.Definition.Config.ConfigSION;

/**
 * 
 * @author fvegap
 *
 */
public class ControladorPrincipal {
	//private Logs logeo;

	//private VentaControlador controlador;
	
	public ControladorPrincipal(String idLog){
		//controlador = new VentaControlador();
		//logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
		//logeo.setIdLog(idLog);
	}
	
	
	/**
	 * M�todo encargado de obtener llave para pinpad
	 * @param _peticionLlave
	 * @return
	 */
	public RespuestaPagatodoNipBean consultaSaldoPTdo (PeticionPagatodoNipBean _peticionConSaldo)
			 throws WSExcepcion {
		NoSocketPagatodo connPagatodo = new NoSocketPagatodo(/*logeo.getIdLog()*/);
		return connPagatodo.consultaSaldo(_peticionConSaldo) ;
	}
	
	
	/**
	 * M�todo encargado de registrar un pago con tarjeta
	 * @param _peticionTDARespuestaVentaBean
	 * @return
	 */
	public RespuestaVentaBean registrarVentaTarjeta (PeticionVentaBean _peticionTDA,PeticionPagatodoNipBean _requestPT,
													 final RespuestaRegistroPagoBean _resBDUltTrans,final RespuestaPagatodoNipBean _respuestaPTdo)
													 throws WSExcepcion {
				
		
		Utilerias utilerias = new Utilerias(logeo.getIdLog());
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		
		RespuestaPagatodoNipBean respuestaPTdo = null; //respuesta del SOCKET DE BAZ
		RespuestaRegistroPagoBean respuestaActualizacionBD = new RespuestaRegistroPagoBean(); //cuando actualizamoms en BD una vez que nos contesta el socket
		
		boolean seObtuvoRespuestaPAGATODO = false;  //nos indica que se obtuvo respuesta de BAZ, cual sea el resultado
		boolean fuePagoExitosoPAGATODO = false; //nos indica que el pago aplicado en BAZ fue exitoso
		
		//Registrar pago en BD con estatus 1-> Generado
		ControladorVentaTarjeta controladorVenta = new ControladorVentaTarjeta(utilerias, logeo.getIdLog());
		PeticionVentaBAZBean peticionVentaBAZ = null;
		//se obtiene al registrar pago en BD por primera vez antes de ir al switch
		logeo.log("pago tarjeta :: solicitud :: 1 :: "+_peticionTDA);
		RespuestaRegistroPagoBean respBD_RegPagoTar = controladorVenta.registrarPago(_peticionTDA, _requestPT);
		
		_resBDUltTrans.setCodigoError(respBD_RegPagoTar.getCodigoError());
		_resBDUltTrans.setDescError(respBD_RegPagoTar.getDescError());
		_resBDUltTrans.setTrace(respBD_RegPagoTar.getTrace());
		
		respuesta.setTrace(respBD_RegPagoTar.getTrace());
				
		//Registro BD exitoso se envia a switch, sino se devuelve codigo de error a TDA
		if( respBD_RegPagoTar.getCodigoError()==0){
			//se envia peticion a servicio paga todo
			peticionVentaBAZ = new PeticionVentaBAZBean();
			respuestaPTdo = controladorVenta.enviarPeticionVenta(_peticionTDA, respBD_RegPagoTar, _requestPT);
			
			if(respuestaPTdo !=null){
				
				seObtuvoRespuestaPAGATODO = true;
			
				
				respuesta.setCodigoError(Integer.parseInt(respuestaPTdo.getrCodigoRespuesta()));
				respuesta.setDescError(respuestaPTdo.getrDescRespuesta());
				
				logeo.log("pago tarjeta :: respuestaPT :: generada :: "+respuestaPTdo);
				
				respuesta.setRespuestaPagaTodo(respuestaPTdo);
				
				
				peticionVentaBAZ = controladorVenta.getPeticionBAZ();
			}else{
				respuesta.setCodigoError(utilerias.ERROR_BD);
				respuesta.setDescError("["+utilerias.ERROR_BD+"] "+respBD_RegPagoTar.getDescError());
				respuesta.setRespuestaBAZ(null);
				logeo.log("pago tarjeta :: respuestaPT :: "+respuestaPTdo);
			}
		}else{
			logeo.log("pago tarjeta :: respuestaPT :: error de base "+respBD_RegPagoTar);
			//No se logro crear registro en BD, se cancela el pago sin ir a BAZ
			respuesta.setCodigoError(utilerias.ERROR_BD);
			respuesta.setDescError("["+utilerias.ERROR_BD+"] "+respBD_RegPagoTar.getDescError());
			respuesta.setRespuestaBAZ(null);
			//si entra a este caso termina el flujo
			//regresar respuesta de error
			
			//Aqu� se debe registrar el pago con error (pendiente)
			return respuesta;
			
		}
		
		//variable que nos indica cuando se debe enviar un reverso de la operacion
		boolean seDebeEnviarReverso = false;
		
		//Si el pago a BAZ es exitoso actualizar en BD a estatus exitoso, de lo contrario actualizar estatus error
		if(seObtuvoRespuestaPAGATODO){
			
			if(respuesta.getRespuestaPagaTodo().isEsPagoExitoso()){
				
				//actualizar en BD el pago a estatus exitoso
				fuePagoExitosoPAGATODO = true;
				logeo.log("Pago tarjeta :: respuesta :: 2 :: EXITOSO :: "+_peticionTDA);
				respuestaActualizacionBD = controladorVenta.actualizarPago(
						_peticionTDA, respBD_RegPagoTar, respuesta.getRespuestaPagaTodo(), _requestPT, EstatusRegistroPago.EXITOSO);
				
				//logeo.log("respuesta paga todo: "+respuesta.getRespuestaPagaTodo().toString());
				//se valida la respuesta de actualizacion de registro, si esta no se completa hay q reversar el pago
				if(respuestaActualizacionBD==null){
					seDebeEnviarReverso = true; 
				}else{
					if(respuestaActualizacionBD.getCodigoError()==0){
						//actualizacion exitosa -   se manda reverso solo para prueba
						//seDebeEnviarReverso = true;
						respuesta = controladorVenta.generarRespuestaServicio(peticionVentaBAZ, respuesta.getRespuestaPagaTodo(), respBD_RegPagoTar.getTrace());
					}else{
						//entra aqui cuando ocurre un error al actualizar
						seDebeEnviarReverso = true;
					}
				}
			}else if(respuestaPTdo.isEsPagoError()){//sino se obtuvo respuesta de socket tambien se actualiza a error
				logeo.log("pago tarjeta :: respuesta :: 2 :: ERROR :: "+_peticionTDA);
				respuestaActualizacionBD = controladorVenta.actualizarPago(
							_peticionTDA, respBD_RegPagoTar, respuesta.getRespuestaPagaTodo(), _requestPT, EstatusRegistroPago.ERROR);
				//generar respuesta que se enviar a front
				respuesta = controladorVenta.generarRespuestaServicio(peticionVentaBAZ,null,0);
				//seDebeEnviarReverso = true;
			}else{
				//actualizar a rechazado
				logeo.log("Pago tarjeta :: respuesta :: 2 :: RECHAZADO ::"+_peticionTDA);
				respuestaActualizacionBD = controladorVenta.actualizarPago(
						_peticionTDA, respBD_RegPagoTar, respuesta.getRespuestaPagaTodo(), _requestPT, EstatusRegistroPago.RECHAZADO);
				//generar respuesta que se enviara a front
				respuesta = controladorVenta.generarRespuestaServicio(peticionVentaBAZ, respuesta.getRespuestaPagaTodo(), respBD_RegPagoTar.getTrace());
			}	
		}else {//sino se obtuvo respuesta de socket tambien se actualiza a error
			logeo.log("pago tarjeta :: respuesta :: 2 :: ERROR : "+_peticionTDA);
			respuestaActualizacionBD = controladorVenta.actualizarPago(
						_peticionTDA, respBD_RegPagoTar, respuesta.getRespuestaPagaTodo(), _requestPT, EstatusRegistroPago.ERROR);
			//generar respuesta que se enviar a front
			respuesta = controladorVenta.generarRespuestaServicio(peticionVentaBAZ,null,0);
			//seDebeEnviarReverso = true;
		}
		
		respuesta.setSeEnviaReverso(seDebeEnviarReverso);
		respuesta.setPagoExitoso( fuePagoExitosoPAGATODO );
		
		
		if(respuesta.getRespuestaPagaTodo()==null){
			logeo.log("getRespuestaPagaTodoNULL");
		}else{
			logeo.log(respuesta.getRespuestaPagaTodo().toString());
		}
		
		
		return respuesta;
	}
	
	
	public RespuestaVentaBean registrarVentaTarjetasPT (
			PeticionVentaBean _peticionTDA, 
			PeticionPagatodoNipBean _requestPT, 
			final RespuestaRegistroPagoBean _resBDUltTrans,
			final RespuestaPagatodoNipBean _respuestaPTdo)
		 throws WSExcepcion 
	{
		Utilerias utilerias = new Utilerias(logeo.getIdLog());
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		
		RespuestaPagatodoNipBean respuestaPTdo = null; //respuesta del SOCKET DE BAZ
		RespuestaRegistroPagoBean respuestaActualizacionBD = new RespuestaRegistroPagoBean(); //cuando actualizamoms en BD una vez que nos contesta el socket
		
		boolean seObtuvoRespuestaPAGATODO = false;  //nos indica que se obtuvo respuesta de BAZ, cual sea el resultado
		boolean fuePagoExitosoPAGATODO = false; //nos indica que el pago aplicado en BAZ fue exitoso
		
		//Registrar pago en BD con estatus 1-> Generado
		ControladorVentaTarjeta controladorVenta = new ControladorVentaTarjeta(utilerias, logeo.getIdLog());
		PeticionVentaBAZBean peticionVentaBAZ = null;
		//se obtiene al registrar pago en BD por primera vez antes de ir al switch
		logeo.log("pago tarjeta :: solicitud :: 1 :: "+_peticionTDA);
		RespuestaRegistroPagoBean respBD_RegPagoTar = controladorVenta.registrarPago(_peticionTDA, _requestPT);
		
		_resBDUltTrans.setCodigoError(respBD_RegPagoTar.getCodigoError());
		_resBDUltTrans.setDescError(respBD_RegPagoTar.getDescError());
		_resBDUltTrans.setTrace(respBD_RegPagoTar.getTrace());
		
		//Registro BD exitoso se envia a switch, sino se devuelve codigo de error a TDA
		if( respBD_RegPagoTar.getCodigoError()==0){
			//se envia peticion a servicio paga todo
			peticionVentaBAZ = new PeticionVentaBAZBean();
			respuestaPTdo = controladorVenta.enviarPeticionVenta(_peticionTDA, respBD_RegPagoTar, _requestPT);
			
			if(respuestaPTdo !=null){
				seObtuvoRespuestaPAGATODO = true;
				
				_respuestaPTdo.setrNoConfirmacion(respuestaPTdo.getrNoConfirmacion());
				_respuestaPTdo.setrCodigoRespuesta(respuestaPTdo.getrCodigoRespuesta());
				_respuestaPTdo.setrDescRespuesta(respuestaPTdo.getrDescRespuesta());
				_respuestaPTdo.setrDescMsgObligatorio(respuestaPTdo.getrDescMsgObligatorio());
				_respuestaPTdo.setrNoConfirmacion(respuestaPTdo.getrNoConfirmacion());
				
				respuesta.setCodigoError(Integer.parseInt(respuestaPTdo.getrCodigoRespuesta()));
				respuesta.setDescError(respuestaPTdo.getrDescRespuesta());
				
				logeo.log("pago tarjeta :: respuestaPT :: generada :: "+_respuestaPTdo);
				
				respuesta.setRespuestaPagaTodo(respuestaPTdo);
				peticionVentaBAZ = controladorVenta.getPeticionBAZ();
			}else{
				respuesta.setCodigoError(utilerias.ERROR_BD);
				respuesta.setDescError("["+utilerias.ERROR_BD+"] "+respBD_RegPagoTar.getDescError());
				respuesta.setRespuestaBAZ(null);
				logeo.log("pago tarjeta :: respuestaPT :: null :: "+_respuestaPTdo);
			}
		}else{
			logeo.log("pago tarjeta :: respuestaPT :: error de base "+respBD_RegPagoTar);
			//No se logro crear registro en BD, se cancela el pago sin ir a BAZ
			respuesta.setCodigoError(utilerias.ERROR_BD);
			respuesta.setDescError("["+utilerias.ERROR_BD+"] "+respBD_RegPagoTar.getDescError());
			respuesta.setRespuestaBAZ(null);
			//si entra a este caso termina el flujo
			//regresar respuesta de error
			
			//Aqu� se debe registrar el pago con error (pendiente)
			return respuesta;
			
		}
		
		//variable que nos indica cuando se debe enviar un reverso de la operacion
		boolean seDebeEnviarReverso = false;
		
		//Si el pago a BAZ es exitoso actualizar en BD a estatus exitoso, de lo contrario actualizar estatus error
		if(seObtuvoRespuestaPAGATODO){
			
			if(respuesta.getRespuestaPagaTodo().isEsPagoExitoso()){
				
				//actualizar en BD el pago a estatus exitoso
				fuePagoExitosoPAGATODO = true;
				logeo.log("Pago tarjeta :: respuesta :: 2 :: EXITOSO :: "+_peticionTDA);
				respuestaActualizacionBD = controladorVenta.actualizarPago(
						_peticionTDA, respBD_RegPagoTar, respuesta.getRespuestaPagaTodo(), _requestPT, EstatusRegistroPago.EXITOSO);
				
				//logeo.log("respuesta paga todo: "+respuesta.getRespuestaPagaTodo().toString());
				//se valida la respuesta de actualizacion de registro, si esta no se completa hay q reversar el pago
				if(respuestaActualizacionBD==null){
					seDebeEnviarReverso = true; 
				}else{
					if(respuestaActualizacionBD.getCodigoError()==0){
						//actualizacion exitosa -   se manda reverso solo para prueba
						//seDebeEnviarReverso = true;
						respuesta = controladorVenta.generarRespuestaServicio(peticionVentaBAZ, respuesta.getRespuestaPagaTodo(), respBD_RegPagoTar.getTrace());
					}else{
						//entra aqui cuando ocurre un error al actualizar
						seDebeEnviarReverso = true;
					}
				}
			}else if(respuestaPTdo.isEsPagoError()){//sino se obtuvo respuesta de socket tambien se actualiza a error
			logeo.log("pago tarjeta :: respuesta :: 2 :: ERROR :: "+_peticionTDA);
			respuestaActualizacionBD = controladorVenta.actualizarPago(
						_peticionTDA, respBD_RegPagoTar, respuesta.getRespuestaPagaTodo(), _requestPT, EstatusRegistroPago.ERROR);
			//generar respuesta que se enviar a front
			respuesta = controladorVenta.generarRespuestaServicio(peticionVentaBAZ,null,0);
			//seDebeEnviarReverso = true;
			}else{
				//actualizar a rechazado
				logeo.log("Pago tarjeta :: respuesta :: 2 :: RECHAZADO ::"+_peticionTDA);
				respuestaActualizacionBD = controladorVenta.actualizarPago(
						_peticionTDA, respBD_RegPagoTar, respuesta.getRespuestaPagaTodo(), _requestPT, EstatusRegistroPago.RECHAZADO);
				//generar respuesta que se enviara a front
				respuesta = controladorVenta.generarRespuestaServicio(peticionVentaBAZ, respuesta.getRespuestaPagaTodo(), respBD_RegPagoTar.getTrace());
			}	
		}else {//sino se obtuvo respuesta de socket tambien se actualiza a error
			logeo.log("pago tarjeta :: respuesta :: 2 :: ERROR : "+_peticionTDA);
			respuestaActualizacionBD = controladorVenta.actualizarPago(
						_peticionTDA, respBD_RegPagoTar, respuesta.getRespuestaPagaTodo(), _requestPT, EstatusRegistroPago.ERROR);
			//generar respuesta que se enviar a front
			respuesta = controladorVenta.generarRespuestaServicio(peticionVentaBAZ,null,0);
			//seDebeEnviarReverso = true;
		}
		
		respuesta.setSeEnviaReverso(seDebeEnviarReverso);
		respuesta.setPagoExitoso( fuePagoExitosoPAGATODO );
		
		return respuesta;
	}
	
	/**
	 * M�todo encargado de registrar un pago con tarjeta
	 * @param _peticionTDA
	 * @return
	 */
	public RespuestaRegistroPagoBean confirmaPagoTarjeta (
				PeticionVentaBean _peticionTDA,
				RespuestaRegistroPagoBean _resBDUltTrans,
				RespuestaPagatodoNipBean respuestaPTdo,
				PeticionPagatodoNipBean _requestPT,
				Long transaccionId)
			 throws WSExcepcion 
	{
		ControladorVentaTarjeta controladorVenta = new ControladorVentaTarjeta(new Utilerias(logeo.getIdLog()), logeo.getIdLog());
		RespuestaRegistroPagoBean respuestaActualizacionBD = 
				controladorVenta.confirmaPagoTarjeta(_peticionTDA, _resBDUltTrans, respuestaPTdo, _requestPT,transaccionId);
		
		return respuestaActualizacionBD;
	}
	
	public RespuestaRegistroPagoBean cancelaPagoTarjeta (PeticionVentaBean _peticionTDA,RespuestaRegistroPagoBean _resBDUltTrans,
			RespuestaPagatodoNipBean respuestaPTdo,PeticionPagatodoNipBean _requestPT,
														 long transaccionId)throws WSExcepcion {
		
		ControladorVentaTarjeta controladorVenta = new ControladorVentaTarjeta(new Utilerias(logeo.getIdLog()), logeo.getIdLog());
		
		RespuestaRegistroPagoBean respuestaActualizacionBD = controladorVenta.cancelaPagoTarjeta(_peticionTDA, 
																								 _resBDUltTrans, 
																								 respuestaPTdo, 
																								 _requestPT, 
																								 transaccionId);
	
		return respuestaActualizacionBD;
	}
	
	
	/**
	 * M�todo encargado de registrar un reverso a un cobro de tarjeta, este m�todo es invocado desde el mismo WS cuando un pago exitoso en BAZ no se logra registrar en caja
	 * @return
	 */
	public RespuestaVentaBean registrarReversoTarjeta(PeticionVentaBean _peticionTDA, RespuestaVentaBean _respuesta )
			 throws WSExcepcion {
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		
		logeo.log("Se procede a enviar reverso del pago con uId"+ _peticionTDA.getuId() + "ya que no fue posible obtener una respuesta de BAZ");
		
		//actalizar registro de pago antes de enviar reverso a BAZ
		boolean seRegistroEstatusReversoEnBD = false;
		int cantidadIntentosRegistro = 3;
		int intento  = 1;
		ControladorReversoTarjeta controladorReverso = null;
		Utilerias utilerias = new Utilerias(logeo.getIdLog());
		
		RespuestaRegistroPagoBean respuestaReversoBD = new RespuestaRegistroPagoBean();
		do{
			try{
				controladorReverso = new ControladorReversoTarjeta(utilerias, logeo.getIdLog());
				respuestaReversoBD = controladorReverso.registrarPago( _peticionTDA, _respuesta.getTrace());
				if(respuestaReversoBD.getCodigoError()==0){
					seRegistroEstatusReversoEnBD = true;
				}
			}catch(Exception e){
				logeo.log("Ocurri� un error al intentar registrar solictud de reverso con trace: "+_respuesta.getRespuestaBAZ().getTrace()+". Intento "+intento);
			}
			intento++;
		}while(!seRegistroEstatusReversoEnBD && intento<cantidadIntentosRegistro);
		
		//variable que guarda registro del reverso en la BD
		RespuestaRegistroPagoBean respuestaActualizacionReversoBD = new RespuestaRegistroPagoBean();
		
		if(seRegistroEstatusReversoEnBD){
		
			//se arma mensaje de reverso y se envia a BAZ
			RespuestaReversoBAZBean respuestaReversoBAZ = new RespuestaReversoBAZBean();
			respuestaReversoBAZ = controladorReverso.enviarPeticionReverso(_peticionTDA, _respuesta.getTrace() ,_respuesta.getPeticionBAZ(), _respuesta.getRespuestaBAZ());
		
			//se valida la respuesta de BAZ para actualizar el estatus en BD
			if(respuestaReversoBAZ != null){
				if(respuestaReversoBAZ.getCodigoRespuesta().equals("00")){
			
					//actualizar en BD reverso exitoso del pago
					respuestaActualizacionReversoBD = controladorReverso.actualizarReverso(_peticionTDA, _respuesta.getRespuestaBAZ(), _respuesta.getTrace(), EstatusRegistroPago.REVERSO_EXITOSO);
			
					//generar respuesta que se enviara a front
					respuesta = controladorReverso.generarRespuestaServicio(_respuesta.getRespuestaBAZ(), respuestaReversoBAZ);
					
					//se setea el trace obtenido de bd en caso de que este haya sido cortado
					respuesta.getRespuestaReversoBAZ().setTrace(String.valueOf(_respuesta.getTrace()));
			
				}else{
			
					//actualizar en BD reverso fallido
					respuestaActualizacionReversoBD = controladorReverso.actualizarReverso(_peticionTDA, _respuesta.getRespuestaBAZ(), _respuesta.getTrace(), EstatusRegistroPago.REVERSO_RECHAZADO);
			
					//generar respuesta que se enviara a front
					respuesta = controladorReverso.generarRespuestaServicio(_respuesta.getRespuestaBAZ(), respuestaReversoBAZ);
					
					//se setea el trace obtenido de bd en caso de que este haya sido cortado
					respuesta.getRespuestaReversoBAZ().setTrace(String.valueOf(_respuesta.getTrace()));
			
				}
			}else{
			
				//actualizar en BD reverso fallido
				respuestaActualizacionReversoBD = controladorReverso.actualizarReverso(_peticionTDA, _respuesta.getRespuestaBAZ(), _respuesta.getTrace(), EstatusRegistroPago.REVERSO_ERROR);
			
				//generar respuesta que se enviara a front
				respuesta = controladorReverso.generarRespuestaServicio(_respuesta.getRespuestaBAZ(), null);
			
			}
		
		}else{
			
			//actualizar en BD reverso fallido
			respuestaActualizacionReversoBD = controladorReverso.actualizarReverso(_peticionTDA, _respuesta.getRespuestaBAZ(), _respuesta.getTrace(), EstatusRegistroPago.REVERSO_ERROR);

			
			//generar respuesta que se enviara a front
			respuesta = controladorReverso.generarRespuestaServicio(_respuesta.getRespuestaBAZ(), null);
			
		}
		
		return respuesta;
	}
	
	/**
	 * M�todo encargado de registrar un reverso a un cobro de tarjeta, este m�todo es invocado desde TDA
	 * @param _peticion
	 * @return
	 */
	public RespuestaReversoBean registrarReversoTarjeta(PeticionReversoBean _peticionTDA)
			 throws WSExcepcion {
		
		logeo.log("Inicia Proceso de reverso de pago con tarjeta");
		
		Utilerias utilerias = new Utilerias(logeo.getIdLog());
		
		RespuestaReversoBean respuestaFront = new RespuestaReversoBean();
		ControladorReversoTarjeta controladorReverso = new ControladorReversoTarjeta(logeo.getIdLog());
		
		//primero obtener respuesta de BAZ
		RespuestaReversoBAZBean respuestaBAZ = new RespuestaReversoBAZBean();
		respuestaBAZ = controladorReverso.enviarPeticionReverso(_peticionTDA);
		
		//validar la respuesta obtenida
		if(respuestaBAZ != null){
			if(respuestaBAZ.getCodigoRespuesta().equals("00")){
				
				//actualizar en BD reverso exitoso del pago
				RespuestaRegistroPagoBean respuestaActualizacionReversoBD = new RespuestaRegistroPagoBean();
				respuestaActualizacionReversoBD = controladorReverso.actualizarReverso(_peticionTDA, EstatusRegistroPago.REVERSO_EXITOSO);
				
				//generar respuesta que se enviara a front
				respuestaFront = controladorReverso.generarRespuestaServicio(_peticionTDA, respuestaBAZ);


			}else{
				
				//actualizar en BD reverso exitoso del pago
				RespuestaRegistroPagoBean respuestaActualizacionReversoBD = new RespuestaRegistroPagoBean();
				respuestaActualizacionReversoBD = controladorReverso.actualizarReverso(_peticionTDA, EstatusRegistroPago.REVERSO_RECHAZADO);
				
				//generar respuesta que se enviara a front
				respuestaFront = controladorReverso.generarRespuestaServicio(_peticionTDA, respuestaBAZ);
			}
		}else{
			
			//actualizar en BD reverso exitoso del pago
			RespuestaRegistroPagoBean respuestaActualizacionReversoBD = new RespuestaRegistroPagoBean();
			respuestaActualizacionReversoBD = controladorReverso.actualizarReverso(_peticionTDA, EstatusRegistroPago.REVERSO_ERROR);
			
			//generar respuesta que se enviara a front
			respuestaFront = controladorReverso.generarRespuestaServicio(_peticionTDA, respuestaBAZ);

		}
		
		
		logeo.log("Respuesta enviada a TDA: "+respuestaFront.toString());
		logeo.log("Finaliza Proceso de reverso de pago con tarjeta");
		
		return respuestaFront;
		
	}
	
}
