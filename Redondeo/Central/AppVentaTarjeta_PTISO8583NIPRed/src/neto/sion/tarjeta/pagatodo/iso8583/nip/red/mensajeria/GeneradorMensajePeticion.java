package neto.sion.tarjeta.pagatodo.iso8583.nip.red.mensajeria;

import Com.Elektra.Definition.Config.ConfigSION;
import neto.sion.Logs;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionLLaveBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionLLavePinpadBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionReversoBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionReversoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaRegistroPagoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaReversoBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.util.Utilerias;

public class GeneradorMensajePeticion {
	
	private Utilerias utilerias;
	
	//Valores de entrada de pinpad
	private final int METODO_ENTRADA_CHIP = 1;
	private final int METODO_ENTRADA_BANDA_MAGNETICA = 2;
	private final int METODO_ENTRADA_FALLBACK = 3;
	
	//Valores fijos por tipo de peticion
	
	//Peticion de LLave
	private final String LLAVE_TIPO_MENSAJE = "0200";
	private final String LLAVE_BITMAP = "3220040000A08002";
	private final String LLAVE_CODIGO_PROCESO = "000000";
	private final String LLAVE_MONTO_OPERACION = "000000000000";
	private final String LLAVE_METODO_ENTRADA_CHIP = "05";
	private final String LLAVE_METODO_ENTRADA_BANDA_MAGNETICA = "90";
	private final String LLAVE_METODO_ENTRADA_FALLBACK = "80";
	private final String LLAVE_TIPO_MONEDA = "484";
	private final String LLAVE_CANAL_PAGO = "50";
	private final String LLAVE_NOMBRE_COMERCIO = "CARGA_LLAVE                             ";
	
	//peticion de Venta
	private final String VENTA_TIPO_MENSAJE = "0200";
	private final String VENTA_BITMAP_CHIP = "3220040000A08202";
	private final String VENTA_BITMAP_BANDA = "3220040000A08002";
	private final String VENTA_CODIGO_PROCESO = "000000";
	private final String VENTA_METODO_ENTRADA_CHIP = "05";
	private final String VENTA_METODO_ENTRADA_BANDA_MAGNETICA = "90";
	private final String VENTA_METODO_ENTRADA_FALLBACK = "80";
	private final String VENTA_TIPO_MONEDA = "484";
	private final String VENTA_CANAL_PAGO = "50";
	private final String VENTA_NOMBRE_COMERCIO = "TIENDAS NETO          MEXICO        DFMX";
	
	//peticion de reverso
	private final String REVERSO_TIPO_MENSAJE = "0420";
	private final String REVERSO_BITMAP = "3238040002E08002";
	private final String REVERSO_CODIGO_PROCESO = "000000";

	private Logs logeo;
	
	/**
	 * Constructor de clase
	 */
	public GeneradorMensajePeticion(Utilerias _utilerias){
		this.utilerias = _utilerias;
		logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
	}
	
	
	/**
	 * M�todo que genera una petici�n de LLaves que se enviar� a Switch BAZ por medio de socket
	 * @param _peticionPinpadTDA
	 * @return
	 */
	public PeticionLLaveBAZBean generarPeticionLLave(PeticionLLavePinpadBean _peticionPinpadTDA){
		PeticionLLaveBAZBean peticion = new PeticionLLaveBAZBean();
		
		logeo.log("Petici�n de llave recibida desde Tienda: "+ _peticionPinpadTDA.toString());
		
		peticion.setTipoMensaje(LLAVE_TIPO_MENSAJE);
		peticion.setBitmap(LLAVE_BITMAP);
		peticion.setCodigoProceso(LLAVE_CODIGO_PROCESO);
		peticion.setMontoOperacion(LLAVE_MONTO_OPERACION); //Siempre se manda 0 para llave
		peticion.setFecha(utilerias.obtenerFecha()); 
		peticion.setTrace(String.valueOf(_peticionPinpadTDA.getTrace())); //TDA
		
		switch(_peticionPinpadTDA.getTipoEntrada()){  //Para llave se acordo mandar siempre 90 desde TDA
			case METODO_ENTRADA_CHIP:
				peticion.setModoEntrada(LLAVE_METODO_ENTRADA_CHIP.concat(String.valueOf(_peticionPinpadTDA.getPinEntryCapability())).trim()); 
				break;
			case METODO_ENTRADA_BANDA_MAGNETICA:
				peticion.setModoEntrada(LLAVE_METODO_ENTRADA_BANDA_MAGNETICA.concat(String.valueOf(_peticionPinpadTDA.getPinEntryCapability())).trim());
				break;
			case METODO_ENTRADA_FALLBACK:
				peticion.setModoEntrada(LLAVE_METODO_ENTRADA_FALLBACK.concat(String.valueOf(_peticionPinpadTDA.getPinEntryCapability())).trim());
				break;
		}
		
		peticion.setTerminal(LLAVE_CANAL_PAGO.concat(_peticionPinpadTDA.getNumTerminal().trim())); // Se genera con la sig info: 1:2->Canal, 
																								   // 3:4->tipoTerminal, 5:8->Sucursal 
		                   																		   // Total 8 posiciones
		
		peticion.setNombreComercio(LLAVE_NOMBRE_COMERCIO); 
		peticion.setTipoMoneda(LLAVE_TIPO_MONEDA);
		peticion.setLongToken(utilerias.formateaInt(String.valueOf(_peticionPinpadTDA.getToken_ES_EW().length()), 3));
		peticion.setDescToken_ES_EW(_peticionPinpadTDA.getToken_ES_EW());
		
		//al final setear la longitud de la cadena 
		peticion.setLongitudMensaje200(utilerias.formateaInt(String.valueOf(utilerias.calculaLongitudPeticionLLaves(peticion)), 4));
		
		logeo.log("Petici�n de llave generada que se enviara a BAZ: "+ peticion.toString());
		
		return peticion;
	}
	
	/**
	 * M�todo que genera una petici�n de Venta que se enviara a Switch BAZ por medio de socket
	 * @param _peticionTDA
	 * @return
	 */
	public PeticionVentaBAZBean generarPeticionVenta(PeticionVentaBean _peticionTDA, RespuestaRegistroPagoBean _respuestaBD){
		PeticionVentaBAZBean peticion = new PeticionVentaBAZBean();
		
		peticion.setTipoMensaje(VENTA_TIPO_MENSAJE);
		peticion.setCodigoProceso(VENTA_CODIGO_PROCESO);
		peticion.setMontoOperacion(utilerias.formatearDouble(_peticionTDA.getMonto(),12)); //EL q se obtiene desde TDA
		peticion.setFecha(utilerias.obtenerFecha()); 
		peticion.setTrace(utilerias.formatearLong(_respuestaBD.getTrace(), 6)); //Se obtiene al insertar registro en BD Central
		
		peticion.setTerminal(VENTA_CANAL_PAGO.concat(_peticionTDA.getNumTerminal().trim())); // Se genera con la sig info: 1:2->Canal, 
		   // 3:4->caja, 5:8->Sucursal 
		   // Total 8 posiciones
		
		
		peticion.setNombreComercio(VENTA_NOMBRE_COMERCIO); 
		peticion.setTipoMoneda(VENTA_TIPO_MONEDA);

		//campo c63
		peticion.setLongitudC63(utilerias.formateaInt(String.valueOf(_peticionTDA.getC63().length()), 3));
		peticion.setC63_desc_token_ES_EZ(_peticionTDA.getC63());
		
		switch(_peticionTDA.getMetodoLectura()){  //
		
			case METODO_ENTRADA_CHIP:
				
				logeo.log("Generando petici�n de venta (Chip)");
				
				peticion.setModoEntrada(VENTA_METODO_ENTRADA_CHIP.concat(String.valueOf(_peticionTDA.getPinEntryCapability())).trim()); 
				peticion.setBitmap(VENTA_BITMAP_CHIP);
				//campo c55
				peticion.setLongitudC55(utilerias.formateaInt(String.valueOf(_peticionTDA.getC55().length()), 3));
				peticion.setC55_TAGS_EMV_FULL(_peticionTDA.getC55());
				//al final setear la longitud de la cadena 
				peticion.setLongitudMensaje200(utilerias.formateaInt(String.valueOf(utilerias.calculaLongitudPeticionVenta(peticion,METODO_ENTRADA_CHIP)), 4));
				
			    break;
			    
			case METODO_ENTRADA_BANDA_MAGNETICA:
				
				logeo.log("Generando Petici�n de venta (Banda Magnetica)");
				
				peticion.setModoEntrada(VENTA_METODO_ENTRADA_BANDA_MAGNETICA.concat(String.valueOf(_peticionTDA.getPinEntryCapability())).trim());
				peticion.setBitmap(VENTA_BITMAP_BANDA);
				//al final setear la longitud de la cadena 
				peticion.setLongitudMensaje200(utilerias.formateaInt(String.valueOf(utilerias.calculaLongitudPeticionVenta(peticion,METODO_ENTRADA_BANDA_MAGNETICA)), 4));
				
				break;
				
			case METODO_ENTRADA_FALLBACK:
				
				logeo.log("Generando Petici�n de venta (Fallback)");
				
				peticion.setModoEntrada(VENTA_METODO_ENTRADA_FALLBACK.concat(String.valueOf(_peticionTDA.getPinEntryCapability())).trim());
				peticion.setBitmap(VENTA_BITMAP_BANDA);
				//al final setear la longitud de la cadena 
				peticion.setLongitudMensaje200(utilerias.formateaInt(String.valueOf(utilerias.calculaLongitudPeticionVenta(peticion,METODO_ENTRADA_BANDA_MAGNETICA)), 4));
				
				break;
				
		}
		
		
		
		logeo.log("Petici�n de pago que se enviara a BAZ: "+ peticion.toString());
		
		return peticion;
	}
	
	/**
	 * M�todo que genera una petici�n de Reverso de reverso que se enviara a Switch BAZ por medio de socket, este m�todo es invocado desde central
	 * @param _peticionTDA
	 * @param _respuestaBAZ
	 * @return
	 */
	public PeticionReversoBAZBean generarPeticionReverso(PeticionVentaBAZBean _peticionBAZ, RespuestaVentaBAZBean _respuestaBAZ){
		PeticionReversoBAZBean peticion = new PeticionReversoBAZBean();
		
			
		//caso cuando se tiene respuesta de WS y el error ocurrio en BD l registrar el pago
		peticion.setTipoMensaje(REVERSO_TIPO_MENSAJE);
		peticion.setBitmap(REVERSO_BITMAP);
		peticion.setCodigoProceso(REVERSO_CODIGO_PROCESO);
		peticion.setMontoOperacion(_respuestaBAZ.getMontoOperacion());
		peticion.setFecha(utilerias.obtenerFecha());
		peticion.setTrace(_respuestaBAZ.getTrace());
		peticion.setHoraLocal(_respuestaBAZ.getHoraLocal());
		peticion.setFechaLocal(_respuestaBAZ.getFechaLocal());
		peticion.setModoEntrada(_peticionBAZ.getModoEntrada());
		peticion.setCodigoRespuesta(_respuestaBAZ.getCodigoRespuesta());
		peticion.setTerminal(_peticionBAZ.getTerminal());
		peticion.setAfiliacion(_respuestaBAZ.getAfiliacion());
		peticion.setNombreComercio(_peticionBAZ.getNombreComercio());
		peticion.setTipoMoneda(_peticionBAZ.getTipoMoneda());
		peticion.setLongitudC63(_peticionBAZ.getLongitudC63());
		peticion.setC63_desc_token_ES_EZ(_peticionBAZ.getC63_desc_token_ES_EZ());
		peticion.setLongitudMensaje420(utilerias.formateaInt(String.valueOf(utilerias.calculaLongitudPeticionReverso(peticion)), 4));
		
		logeo.log("Peticion de reverso: "+peticion.toString());
		
		return peticion;
	}
	
	/**
	 * M�todo que genera una petici�n de reverso que se enviara a Switch BAZ por medio de socket, este m�todo es invocado desde tienda
	 * @param _peticionTDA
	 * @return
	 */
	public PeticionReversoBAZBean generarPeticionReverso(PeticionReversoBean _peticionTDA){
		PeticionReversoBAZBean peticion = new PeticionReversoBAZBean();
		
		peticion.setTipoMensaje(REVERSO_TIPO_MENSAJE);
		peticion.setBitmap(REVERSO_BITMAP);
		peticion.setCodigoProceso(REVERSO_CODIGO_PROCESO);
		peticion.setMontoOperacion(_peticionTDA.getMontoOperacion());
		peticion.setFecha(utilerias.obtenerFecha());
		peticion.setTrace(utilerias.formatearLong(Long.parseLong(_peticionTDA.getTrace()), 6));
		peticion.setHoraLocal(_peticionTDA.getHoralocal());
		peticion.setFechaLocal(_peticionTDA.getFechaLocal());
		peticion.setModoEntrada(_peticionTDA.getModoEntrada());
		peticion.setCodigoRespuesta(_peticionTDA.getCodigoRespuesta());
		peticion.setTerminal(_peticionTDA.getTerminal());
		peticion.setAfiliacion(_peticionTDA.getAfiliacion());
		peticion.setNombreComercio(VENTA_NOMBRE_COMERCIO);
		peticion.setTipoMoneda(VENTA_TIPO_MONEDA);
		peticion.setLongitudC63(_peticionTDA.getLongitudC63());
		peticion.setC63_desc_token_ES_EZ(_peticionTDA.getC63_desc_token_ES_EZ());
		
		peticion.setLongitudMensaje420(utilerias.formateaInt(String.valueOf(utilerias.calculaLongitudPeticionReverso(peticion)), 4));
		
		return peticion;
	}

}
