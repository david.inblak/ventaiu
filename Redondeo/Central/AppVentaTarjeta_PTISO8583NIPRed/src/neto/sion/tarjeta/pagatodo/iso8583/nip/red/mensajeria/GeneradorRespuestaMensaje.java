package neto.sion.tarjeta.pagatodo.iso8583.nip.red.mensajeria;

import Com.Elektra.Definition.Config.ConfigSION;
import neto.sion.Logs;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionLLavePinpadBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaLlaveBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaReversoBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.util.Utilerias;
import neto.sion.venta.ventageneral.iso8583Red.bean.RespuestaPagatodoBean;

public class GeneradorRespuestaMensaje {

	private Utilerias utilerias;

	// Valores de entrada de pinpad
	private final int METODO_ENTRADA_CHIP = 1;
	private final int METODO_ENTRADA_BANDA_MAGNETICA = 2;
	private final int METODO_ENTRADA_FALLBACK = 3;
	
	//
	private final String VENTA_BITMAP_BANDA = "323800000EC10002";
	private final String VENTA_BITMAP_CHIP = "323802000EC10202";
	private final String REVERSO_BITMAP_BANDA = "323800000AC00000";
	private final String REVERSO_BITMAP_CHIP = "323802000AC00200";
	private final String PAGATODO_EXITO = "00";

	private Logs logeo;
	

	public GeneradorRespuestaMensaje(Utilerias _utilerias) {
		this.utilerias = _utilerias;
		logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
	}

	/**
	 * 
	 * @param _mensaje210
	 * @return
	 */
	public RespuestaLlaveBAZBean generarRespuestaLlave(String _mensaje210) {

		RespuestaLlaveBAZBean respuestaLlave = new RespuestaLlaveBAZBean();

		//logeo.log("Analizando cadena de respuesta de Llave." + _mensaje210);

		respuestaLlave.setLongitudMensaje210(_mensaje210.substring(0, 4)); // 4
		respuestaLlave.setTipoMensaje(_mensaje210.substring(4, 8)); // 4
		respuestaLlave.setBitmap(_mensaje210.substring(8, 24)); // 16
		respuestaLlave.setCodigoProceso(_mensaje210.substring(24, 30)); // 6
		respuestaLlave.setMontoOperacion(_mensaje210.substring(30, 42)); // 12
		respuestaLlave.setFechaOperacion(_mensaje210.substring(42, 52)); // 10
		respuestaLlave.setTrace(_mensaje210.substring(52, 58)); // 6
		respuestaLlave.setHoraLocal(_mensaje210.substring(58, 64)); // 6
		respuestaLlave.setFechaLocal(_mensaje210.substring(64, 68)); // 4
		respuestaLlave.setReferencia(_mensaje210.substring(68, 80)); // 12
		respuestaLlave.setNumAutorizacion(_mensaje210.substring(80, 86)); // 6
		respuestaLlave.setCodigoRespuesta(_mensaje210.substring(86, 88)); // 2
		respuestaLlave.setTerminal(_mensaje210.substring(88, 96)); // 8
		respuestaLlave.setAfiliacion(_mensaje210.substring(96, 111)); // 15
		respuestaLlave.setLongitudTramaBanco(_mensaje210.substring(111, 114)); // 3
		int posicionFinTramaBanco = (115 + Integer.parseInt(respuestaLlave.getLongitudTramaBanco()) - 1);
		respuestaLlave.setTramaBanco(_mensaje210.substring(114,posicionFinTramaBanco)); // variable - se indica en longitudTramaBanco
		respuestaLlave.setLongitudToken(_mensaje210.substring(posicionFinTramaBanco, posicionFinTramaBanco + 3)); // 3
		int posicionInicioToken = posicionFinTramaBanco + 3;
		respuestaLlave.setDescToken_ER_EX(_mensaje210.substring(posicionInicioToken, _mensaje210.length())); // variable - se indica en longitudToken
		
		return respuestaLlave;

	}

	/**
	 * 
	 * @param _mensaje210
	 * @param _peticionPinpadTDA
	 * @return
	 */
	public RespuestaVentaBAZBean generarRespuestaVenta(String _mensaje210, PeticionVentaBean _peticionPinpadTDA) {

		RespuestaVentaBAZBean respuestaVenta = new RespuestaVentaBAZBean();

		int posicionFinTramaBanco = 0;

		boolean seArmoRespuesta = false;

		try {

			respuestaVenta.setLongitudMensaje210(_mensaje210.substring(0, 4)); // 4
			//logeo.log("longitud mensaje 210:"+ respuestaVenta.getLongitudMensaje210() + ",");
			respuestaVenta.setTipoMensaje(_mensaje210.substring(4, 8)); // 4
			//logeo.log("tipo mensaje 210:" + respuestaVenta.getTipoMensaje()+ ",");
			respuestaVenta.setBitmap(_mensaje210.substring(8, 24)); // 16
			//logeo.log("bitmap:" + respuestaVenta.getBitmap() + ",");
			respuestaVenta.setCodigoProceso(_mensaje210.substring(24, 30)); // 6
			//logeo.log("codigo proceso:" + respuestaVenta.getCodigoProceso()+ ",");
			respuestaVenta.setMontoOperacion(_mensaje210.substring(30, 42)); // 12
			//logeo.log("monto:" + respuestaVenta.getMontoOperacion() + ",");
			respuestaVenta.setFechaOperacion(_mensaje210.substring(42, 52)); // 10
			//logeo.log("fecha ooperacion:" + respuestaVenta.getFechaOperacion()+ ",");
			respuestaVenta.setTrace(_mensaje210.substring(52, 58)); // 6
			//logeo.log("trace:" + respuestaVenta.getTrace() + ",");
			respuestaVenta.setHoraLocal(_mensaje210.substring(58, 64)); // 6
			//logeo.log("hora local:" + respuestaVenta.getHoraLocal() + ",");
			respuestaVenta.setFechaLocal(_mensaje210.substring(64, 68)); // 4
			//logeo.log("fecha local:" + respuestaVenta.getFechaLocal() + ",");

			int posicionInicioToken = 0;
			switch (_peticionPinpadTDA.getMetodoLectura()) {
			case METODO_ENTRADA_CHIP:
				
				if(respuestaVenta.getBitmap().equals(VENTA_BITMAP_CHIP)){
					
					logeo.log("BITMAP Chip validado");

					respuestaVenta.setCampo23Bandera(_mensaje210.substring(68, 71)); // 3
					//logeo.log("campo23:"+ respuestaVenta.getCampo23Bandera() + ",");
					respuestaVenta.setReferencia(_mensaje210.substring(71, 83)); // 12
					//logeo.log("referencia:"+ respuestaVenta.getReferencia() + ",");
					respuestaVenta.setNumAutorizacion(_mensaje210.substring(83, 89)); // 6
					//logeo.log("autorizacion:"+ respuestaVenta.getNumAutorizacion() + ",");
					respuestaVenta.setCodigoRespuesta(_mensaje210.substring(89, 91)); // 2
					//logeo.log("codigo respuesta:"+ respuestaVenta.getCodigoRespuesta() + ",");
					respuestaVenta.setTerminal(_mensaje210.substring(91, 99)); // 8
					//logeo.log("terminal:" + respuestaVenta.getTerminal()+ ",");
					respuestaVenta.setAfiliacion(_mensaje210.substring(99, 114)); // 15
					//logeo.log("afiliacion:"+ respuestaVenta.getAfiliacion() + ",");
					respuestaVenta.setLongitudTramaBanco(_mensaje210.substring(114,117)); // 3
					//logeo.log("longitud trama banco campo 48:"+ respuestaVenta.getLongitudTramaBanco() + ",");
					posicionFinTramaBanco = (118 + Integer.parseInt(respuestaVenta.getLongitudTramaBanco()) - 1);
					// logeo.log("posicionFin campo 48:"+posicionFinTramaBanco+",");
					respuestaVenta.setTramaBanco(_mensaje210.substring(117,posicionFinTramaBanco)); // variable - se indica en longitudTramaBanco
					//logeo.log("campo 48:" + respuestaVenta.getTramaBanco()+ ",");
					respuestaVenta.setLongitudC55(_mensaje210.substring(posicionFinTramaBanco, posicionFinTramaBanco + 3));
					//logeo.log("longitud campo 55:"+ respuestaVenta.getLongitudC55() + ",");
					posicionInicioToken = posicionFinTramaBanco + 3; // define el inicio del token c55
					respuestaVenta.setC55_TAGS_EMV_FULL(_mensaje210.substring(posicionInicioToken, posicionInicioToken + Integer.parseInt(respuestaVenta.getLongitudC55())));
					//logeo.log("campo 55:"+ respuestaVenta.getC55_TAGS_EMV_FULL() + ",");
					posicionInicioToken = posicionInicioToken+ Integer.parseInt(respuestaVenta.getLongitudC55()); // define el inicio del token c63
					//logeo.log("Inicio token 63: "+posicionInicioToken);
					respuestaVenta.setLongitudC63(_mensaje210.substring(posicionInicioToken, posicionInicioToken + 3));
					//logeo.log("longitud campo 63:"+ respuestaVenta.getLongitudC63() + ",");
					respuestaVenta.setC63_desc_token_ES_EZ(_mensaje210.substring(posicionInicioToken + 3, _mensaje210.length()));
					//logeo.log("campo 63:"+ respuestaVenta.getC63_desc_token_ES_EZ() + ",");
				
				}else{
					
					//cae en este caso cuando se recibe codigo de error generico 71 por parte de switch
					logeo.log("BITMAP de Banda Magn�tica encontrado, cambiando la lectura de campos de operaci�n.");
					
					respuestaVenta.setReferencia(_mensaje210.substring(68, 80)); // 12
					//logeo.log("referencia:"+ respuestaVenta.getReferencia() + ",");
					respuestaVenta.setNumAutorizacion(_mensaje210.substring(80, 86)); // 6
					//logeo.log("autorizacion:"+ respuestaVenta.getNumAutorizacion() + ",");
					respuestaVenta.setCodigoRespuesta(_mensaje210.substring(86, 88)); // 2
					//logeo.log("codigo respuesta:"+ respuestaVenta.getCodigoRespuesta() + ",");
					respuestaVenta.setTerminal(_mensaje210.substring(88, 96)); // 8
					//logeo.log("terminal:" + respuestaVenta.getTerminal()+ ",");
					respuestaVenta.setAfiliacion(_mensaje210.substring(96, 111)); // 15
					//logeo.log("afiliacion:"+ respuestaVenta.getAfiliacion() + ",");
					respuestaVenta.setLongitudTramaBanco(_mensaje210.substring(111,114)); // 3
					//logeo.log("longitud trama banco campo 48:"+ respuestaVenta.getLongitudTramaBanco() + ",");
					posicionFinTramaBanco = (115 + Integer.parseInt(respuestaVenta.getLongitudTramaBanco()) - 1);
					//logeo.log("posicionFin campo 48:" + posicionFinTramaBanco+ ",");
					respuestaVenta.setTramaBanco(_mensaje210.substring(114,posicionFinTramaBanco)); // variable - se indica en longitudTramaBanco
					//logeo.log("campo 48:" + respuestaVenta.getTramaBanco()+ ",");
					respuestaVenta.setLongitudC63(_mensaje210.substring(posicionFinTramaBanco, posicionFinTramaBanco + 3)); // 3
					//logeo.log("longitud campo 63:"+ respuestaVenta.getLongitudC63() + ",");
					posicionInicioToken = posicionFinTramaBanco + 3;
					respuestaVenta.setC63_desc_token_ES_EZ(_mensaje210.substring(posicionInicioToken, _mensaje210.length())); // variable - se indica en longitudToken
					//logeo.log("campo 63 seteado:"+ respuestaVenta.getC63_desc_token_ES_EZ() + ",");
					
				}

				break;
			case METODO_ENTRADA_BANDA_MAGNETICA:

				respuestaVenta.setReferencia(_mensaje210.substring(68, 80)); // 12
				//logeo.log("referencia:"+ respuestaVenta.getReferencia() + ",");
				respuestaVenta.setNumAutorizacion(_mensaje210.substring(80, 86)); // 6
				//logeo.log("autorizacion:"+ respuestaVenta.getNumAutorizacion() + ",");
				respuestaVenta.setCodigoRespuesta(_mensaje210.substring(86, 88)); // 2
				//logeo.log("codigo respuesta:"+ respuestaVenta.getCodigoRespuesta() + ",");
				respuestaVenta.setTerminal(_mensaje210.substring(88, 96)); // 8
				//logeo.log("terminal:" + respuestaVenta.getTerminal()+ ",");
				respuestaVenta.setAfiliacion(_mensaje210.substring(96, 111)); // 15
				//logeo.log("afiliacion:"+ respuestaVenta.getAfiliacion() + ",");
				respuestaVenta.setLongitudTramaBanco(_mensaje210.substring(111,114)); // 3
				//logeo.log("longitud trama banco campo 48:"+ respuestaVenta.getLongitudTramaBanco() + ",");
				posicionFinTramaBanco = (115 + Integer.parseInt(respuestaVenta.getLongitudTramaBanco()) - 1);
				//logeo.log("posicionFin campo 48:" + posicionFinTramaBanco+ ",");
				respuestaVenta.setTramaBanco(_mensaje210.substring(114,posicionFinTramaBanco)); // variable - se indica en longitudTramaBanco
				//logeo.log("campo 48:" + respuestaVenta.getTramaBanco()+ ",");
				respuestaVenta.setLongitudC63(_mensaje210.substring(posicionFinTramaBanco, posicionFinTramaBanco + 3)); // 3
				//logeo.log("longitud campo 63:"+ respuestaVenta.getLongitudC63() + ",");
				posicionInicioToken = posicionFinTramaBanco + 3;
				respuestaVenta.setC63_desc_token_ES_EZ(_mensaje210.substring(posicionInicioToken, _mensaje210.length())); // variable - se indica en longitudToken
				//logeo.log("campo 63 seteado:"+ respuestaVenta.getC63_desc_token_ES_EZ() + ",");

				break;
			case METODO_ENTRADA_FALLBACK:

				respuestaVenta.setReferencia(_mensaje210.substring(68, 80)); // 12
				//logeo.log("referencia:"+ respuestaVenta.getReferencia() + ",");
				respuestaVenta.setNumAutorizacion(_mensaje210.substring(80, 86)); // 6
				//logeo.log("autorizacion:"+ respuestaVenta.getNumAutorizacion() + ",");
				respuestaVenta.setCodigoRespuesta(_mensaje210.substring(86, 88)); // 2
				//logeo.log("codigo respuesta:"+ respuestaVenta.getCodigoRespuesta() + ",");
				respuestaVenta.setTerminal(_mensaje210.substring(88, 96)); // 8
				//logeo.log("terminal:" + respuestaVenta.getTerminal()+ ",");
				respuestaVenta.setAfiliacion(_mensaje210.substring(96, 111)); // 15
				//logeo.log("afiliacion:"+ respuestaVenta.getAfiliacion() + ",");
				respuestaVenta.setLongitudTramaBanco(_mensaje210.substring(111,114)); // 3
				//logeo.log("longitud trama banco campo 48:"+ respuestaVenta.getLongitudTramaBanco() + ",");
				posicionFinTramaBanco = (115 + Integer.parseInt(respuestaVenta.getLongitudTramaBanco()) - 1);
				//logeo.log("posicionFin campo 48:" + posicionFinTramaBanco+ ",");
				respuestaVenta.setTramaBanco(_mensaje210.substring(114,posicionFinTramaBanco)); // variable - se indica en longitudTramaBanco
				//logeo.log("campo 48:" + respuestaVenta.getTramaBanco()+ ",");
				respuestaVenta.setLongitudC63(_mensaje210.substring(posicionFinTramaBanco + 1, posicionFinTramaBanco + 3)); // 3
				posicionInicioToken = posicionFinTramaBanco + 3;
				respuestaVenta.setC63_desc_token_ES_EZ(_mensaje210.substring(posicionInicioToken, _mensaje210.length())); // variable - se indica en longitudToken

				break;
			}
			
			seArmoRespuesta = true;
			//logeo.log("Respuesta generada a partir de mensaje 210: "+ respuestaVenta.toString());

		} catch (Exception e) {
			logeo.log("Ocurri� un error al intentar armar objeto a partir del mensaje recibido: "+e.getLocalizedMessage());
			return null;
		}

		return respuestaVenta;

	}
	
	
	public RespuestaVentaBAZBean generarRespuestaVenta(
			RespuestaPagatodoBean _respuestaPAGATODO,
			PeticionVentaBean _peticionPinpadTDA) {
		RespuestaVentaBAZBean respuestaVenta = new RespuestaVentaBAZBean();
		
		boolean seArmoRespuesta = false;
		try {

			//respuestaVenta.setLongitudMensaje210(); // 4  _mensaje210.substring(0, 4)
			//logeo.log("longitud mensaje 210:"+ respuestaVenta.getLongitudMensaje210() + ",");
			//respuestaVenta.setTipoMensaje(_respuestaPAGATODO.getrCodigoRespuesta()); // 4  _mensaje210.substring(4, 8)   TODO: Que mandar a tienda en este dato
			//logeo.log("tipo mensaje 210:" + respuestaVenta.getTipoMensaje()+ ",");
			//respuestaVenta.setBitmap(); // 16   _mensaje210.substring(8, 24)
			//logeo.log("bitmap:" + respuestaVenta.getBitmap() + ",");
			//respuestaVenta.setCodigoProceso(_respuestaPAGATODO.getrNoSecUnicoPT()); // 6   _mensaje210.substring(24, 30)
			//logeo.log("codigo proceso:" + respuestaVenta.getCodigoProceso()+ ",");
			respuestaVenta.setMontoOperacion(""+_peticionPinpadTDA.getMonto()); // 12  _mensaje210.substring(30, 42)
			//logeo.log("monto:" + respuestaVenta.getMontoOperacion() + ",");
			//respuestaVenta.setFechaOperacion(); // 10 _mensaje210.substring(42, 52)
			//logeo.log("fecha ooperacion:" + respuestaVenta.getFechaOperacion()+ ",");
			//respuestaVenta.setTrace(_respuestaPAGATODO.getrNoConfirmacion()); // 6  _mensaje210.substring(52, 58)  TODO: Averiguar que campo trae un mesaje de error 
			//logeo.log("trace:" + respuestaVenta.getTrace() + ",");
			//respuestaVenta.setHoraLocal(); // 6   _mensaje210.substring(58, 64)
			//logeo.log("hora local:" + respuestaVenta.getHoraLocal() + ",");
			//respuestaVenta.setFechaLocal(); // 4   _mensaje210.substring(64, 68)
			//logeo.log("fecha local:" + respuestaVenta.getFechaLocal() + ",");

				respuestaVenta.setReferencia(_respuestaPAGATODO.getrReferencia()); // 12  _mensaje210.substring(68, 80)
				//logeo.log("referencia:"+ respuestaVenta.getReferencia() + ",");
				respuestaVenta.setNumAutorizacion(_respuestaPAGATODO.getrNoConfirmacion()); // 6  _mensaje210.substring(80, 86)
				//logeo.log("autorizacion:"+ respuestaVenta.getNumAutorizacion() + ",");
				respuestaVenta.setCodigoRespuesta(_respuestaPAGATODO.getrCodigoRespuesta()); // 2  _mensaje210.substring(86, 88)
				//logeo.log("codigo respuesta:"+ respuestaVenta.getCodigoRespuesta() + ",");
				//respuestaVenta.setTerminal(_mensaje210.substring(88, 96)); // 8
				//logeo.log("terminal:" + respuestaVenta.getTerminal()+ ",");
				//respuestaVenta.setAfiliacion(_mensaje210.substring(96, 111)); // 15
				//logeo.log("afiliacion:"+ respuestaVenta.getAfiliacion() + ",");
				//respuestaVenta.setLongitudTramaBanco(_mensaje210.substring(111,114)); // 3
				//logeo.log("longitud trama banco campo 48:"+ respuestaVenta.getLongitudTramaBanco() + ",");
				//posicionFinTramaBanco = (115 + Integer.parseInt(respuestaVenta.getLongitudTramaBanco()) - 1);
				//logeo.log("posicionFin campo 48:" + posicionFinTramaBanco+ ",");
				//respuestaVenta.setTramaBanco(); // variable - se indica en longitudTramaBanco  _mensaje210.substring(114,posicionFinTramaBanco)
				//logeo.log("campo 48:" + respuestaVenta.getTramaBanco()+ ",");
				//respuestaVenta.setLongitudC63(_mensaje210.substring(posicionFinTramaBanco, posicionFinTramaBanco + 3)); // 3
				//logeo.log("longitud campo 63:"+ respuestaVenta.getLongitudC63() + ",");
				//posicionInicioToken = posicionFinTramaBanco + 3;
				//respuestaVenta.setC63_desc_token_ES_EZ(_mensaje210.substring(posicionInicioToken, _mensaje210.length())); // variable - se indica en longitudToken
				//logeo.log("campo 63 seteado:"+ respuestaVenta.getC63_desc_token_ES_EZ() + ",");
				respuestaVenta.setEsPagoExitoso(PAGATODO_EXITO.equals(respuestaVenta.getCodigoRespuesta()));
				
			
			seArmoRespuesta = true;
			//logeo.log("Respuesta generada a partir de mensaje 210: "+ respuestaVenta.toString());

		} catch (Exception e) {
			logeo.log("Ocurri� un error al intentar armar objeto a partir del mensaje recibido: "+e.getLocalizedMessage());
			return null;
		}

		return respuestaVenta;

	}

	/**
	 * 
	 * @param _mensaje430
	 * @return
	 */
	public RespuestaReversoBAZBean generarRespuestaReverso(String _mensaje430) {

		RespuestaReversoBAZBean respuestaReverso = new RespuestaReversoBAZBean();

		respuestaReverso.setLongitudMensaje430(_mensaje430.substring(0, 4));
		logeo.log("Longitud mensaje 430: "+respuestaReverso.getLongitudMensaje430());
		respuestaReverso.setTipoMensaje(_mensaje430.substring(4, 8));
		logeo.log("Tipo Mensaje: "+respuestaReverso.getTipoMensaje());
		respuestaReverso.setBitmap(_mensaje430.substring(8, 24));
		logeo.log("Bitmap: "+respuestaReverso.getBitmap());
		respuestaReverso.setCodigoProceso(_mensaje430.substring(24, 30));
		logeo.log("Codigo proceso: "+respuestaReverso.getCodigoProceso());
		respuestaReverso.setMontoOperacion(_mensaje430.substring(30, 42));
		logeo.log("Monto Operacion: "+respuestaReverso.getMontoOperacion());
		respuestaReverso.setFechaOperacion(_mensaje430.substring(42, 52));
		logeo.log("Fecha operacion: "+respuestaReverso.getFechaOperacion());
		respuestaReverso.setTrace(_mensaje430.substring(52, 58));
		logeo.log("Trace: "+respuestaReverso.getTrace());
		respuestaReverso.setHoraLocal(_mensaje430.substring(58, 64));
		logeo.log("Hora local: "+respuestaReverso.getHoraLocal());
		respuestaReverso.setFechaLocal(_mensaje430.substring(64, 68));
		logeo.log("Fecha local: "+respuestaReverso.getFechaLocal());
		
		//validar bitmap devuelto, si es codigo 71 regresara como si fuera banda aunque se haya mandado chip		
		if(respuestaReverso.getBitmap().equals(REVERSO_BITMAP_CHIP)){
					
			respuestaReverso.setInf3Digitos(_mensaje430.substring(68, 71));
			logeo.log("Inf 3 digitos: "+respuestaReverso.getInf3Digitos());
			respuestaReverso.setReferencia(_mensaje430.substring(71, 83));
			logeo.log("Referencia: "+respuestaReverso.getReferencia());
			respuestaReverso.setCodigoRespuesta(_mensaje430.substring(83, 85));
			logeo.log("Codigo respuesta: "+respuestaReverso.getCodigoRespuesta());
			respuestaReverso.setTerminal(_mensaje430.substring(85, 93));
			logeo.log("Terminal: "+respuestaReverso.getTerminal());
			respuestaReverso.setAfiliacion(_mensaje430.substring(93, 108));
			logeo.log("Afiliacion: "+respuestaReverso.getAfiliacion());
			respuestaReverso.setLongitudC55(_mensaje430.substring(108, 111));
			logeo.log("Longitud c55: "+respuestaReverso.getLongitudC55());
			respuestaReverso.setC55_TAGS_EMV_FULL(_mensaje430.substring(111));
			logeo.log("c55: "+respuestaReverso.getC55_TAGS_EMV_FULL());
				
		}else{
					
			respuestaReverso.setReferencia(_mensaje430.substring(68, 80));
			logeo.log("Referencia: "+respuestaReverso.getReferencia());
			respuestaReverso.setCodigoRespuesta(_mensaje430.substring(80, 82));
			logeo.log("Codigo respuesta: "+respuestaReverso.getCodigoRespuesta());
			respuestaReverso.setTerminal(_mensaje430.substring(82, 90));
			logeo.log("Terminal: "+respuestaReverso.getTerminal());
			respuestaReverso.setAfiliacion(_mensaje430.substring(90, 105));
			logeo.log("Afiliacion: "+respuestaReverso.getAfiliacion());
					
		}
		
		
		return respuestaReverso;

	}

}
