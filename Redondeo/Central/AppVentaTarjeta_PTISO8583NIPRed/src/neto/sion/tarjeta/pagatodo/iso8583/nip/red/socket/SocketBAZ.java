package neto.sion.tarjeta.pagatodo.iso8583.nip.red.socket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

import neto.sion.genericos.log.Logeo;
import Com.Elektra.Definition.Config.ConfigSION;

/**
 * 
 * @author fvegap
 *
 */
public class SocketBAZ {
	
	public enum TipoSolicitud {
		SOLICITUD_LLAVE, SOLICITUD_VENTA, SOLICITUD_REVERSO, SOLICITUD_DEVOLUCION
	}

	public SocketBAZ() {
		//this.socket = null;
	}

	public String enviaMensaje(TipoSolicitud _tipoSolicitud,
			String _mensajePeticion) throws Exception {
		
		//Logeo del mensaje a enviar dependiendo del tipo de operaci�n
		switch(_tipoSolicitud){
			case SOLICITUD_LLAVE:
				Logeo.log("Solicitud de Llave(Mensaje200): "+_mensajePeticion);
				break;
			case SOLICITUD_VENTA:
				Logeo.log("Solicitud de Venta(Mensaje200): "+_mensajePeticion);
				break;
			case SOLICITUD_REVERSO:
				Logeo.log("Solicitud de Reverso(Mensaje420): "+_mensajePeticion);
				break;
			case SOLICITUD_DEVOLUCION:
				Logeo.log("Solicitud de Devoluci�n(Mensaje200): "+_mensajePeticion);
				break;
		}
		
		String mensajeRespuesta = "";
		
		 Socket socket = null;
		 OutputStream output = null;
		 DataOutputStream dataOutput = null;
		 DataInputStream dataInput = null;

		try {
			
			InetAddress addr = InetAddress.getByName(ConfigSION.obtenerParametro("switch.tarjetas.ip").trim());

			int port = Integer.parseInt(ConfigSION.obtenerParametro("switch.tarjetas.puerto").trim());
			
			int timeout = Integer.parseInt(ConfigSION.obtenerParametro("switch.tarjetas.timeout"));
			//Logeo.log("Timed out a usar: "+timeout+" ms");
			
			SocketAddress sockaddr = new InetSocketAddress(addr, port);
			
			Logeo.log("Intentando conectar en: "+ConfigSION.obtenerParametro("switch.tarjetas.ip").trim()+" puerto "+port+" con timed out: "+timeout+" ms");

			
			boolean estaConectado = false;
			int intento = 1;
			int numeroIntentos = 3;
			
			do{
				try{
					/*if(socket!=null){
						Logeo.log("Socket no es nulo");
						if(socket.isClosed()){
							Logeo.log("socket cerrado.");*/
							socket = null;
							socket = new Socket();
							//socket.setSoTimeout(timeout);
						/*}
					}*/
					
					//Logeo.log("sockaddr: "+sockaddr.toString());
					
					if(!socket.isConnected()){
						//Logeo.log("Conectando socket");
						socket.connect(sockaddr, timeout);
						
					}else{
						//Logeo.log("Socket continua conectado");
					}
					
					if(socket.isConnected()){
						Logeo.log("Conectado en intento: "+intento+"/"+numeroIntentos);
						estaConectado = true;
					}
				}catch(Exception e){
					Logeo.log("Ocurrio un error al intentar conectar intento -> "+intento+": "+e.getLocalizedMessage());
					e.printStackTrace();
				}
				intento++;
			}while(!estaConectado && intento <= numeroIntentos);
			

			if (socket.isConnected()) {		
				
				String newMsg = null;
				try
				{
					//newMsg = new String(_mensajePeticion.getBytes(), "UTF-8");
					newMsg = new String(_mensajePeticion.trim().getBytes(), "ISO-8859-1");
					
				}
				catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}

				Logeo.log("Conectado en: "+ConfigSION.obtenerParametro("switch.tarjetas.ip").trim());
				//Logeo.log("Mensaje a enviar ISO-8859-1: " + newMsg.trim());

				//forma 1
				//out = new DataOutputStream(socket.getOutputStream());
				//out.writeUTF(newMsg);
				
				//forma 2
				//out = new OutputStreamWriter(new DataOutputStream(socket.getOutputStream()));
				//out.write(newMsg.trim());
				//out.flush();
				
				//forma 3
				output = socket.getOutputStream();
				dataOutput = new DataOutputStream(output);
				dataOutput.writeBytes(newMsg);
				dataOutput.flush();

				Logeo.log("Mensaje enviado");
							
				
				socket.setSoTimeout(timeout);
				//Logeo.log("antes de crear dataInput");
				try{
					//input = socket.getInputStream();
					if(dataInput==null){
						//Logeo.log("Se crea dataInput");
						dataInput = new DataInputStream(socket.getInputStream());
					}
				}catch(Exception e){
					Logeo.log("Exception al enviar mensaje: "+e.getLocalizedMessage());
					throw new Exception(e.getLocalizedMessage());
				}catch(Error e){
					Logeo.log("Error al enviar mensaje: "+e.getLocalizedMessage());
					throw new Exception(e.getLocalizedMessage());
				}
			    //Logeo.log("dataInput listo");
				//mensajeRespuesta = (String) dataInput.readObject();
			    
				for(int i = 0 ; i < 4 ; i++){
					//Logeo.log("dentro de primer for -> i="+i);
					mensajeRespuesta += String.valueOf((char) dataInput.readByte());
				}
				
				//Logeo.log("sale de for");
				mensajeRespuesta.replaceAll("[\\D]{0,}", "");
				//Logeo.log("sale de primer for");
				int longitud = Integer.parseInt(mensajeRespuesta);
				//Logeo.log("longitud del mensaje: "+longitud);

				for(int i = 0 ; i < longitud ; i++){
					//Logeo.log("dentro de segundo for -> i="+i);
					mensajeRespuesta += String.valueOf((char) dataInput.readByte());
				}
			
				//Logeo.log("sale de segundo for");
				if (!mensajeRespuesta.isEmpty()) {
					//Logeo.log("Se obtuvo respuesta de socket.");
				} else {
					//Logeo.log("No se pudo obtener el mensaje de socket");
				}

			}else{
				Logeo.log("No se pudo obtener el mensaje. El Socket no est� conectado");
			}

		}

		catch (UnknownHostException e) {
			e.printStackTrace();
			Logeo.log("Erorr en el socket -> Host no encontrado: " + e.getMessage());
			//throw new Exception("Ocurri� un error al intentar conectar con el host remoto: "+e.getLocalizedMessage());
		}

		catch (Exception ioe) {
			ioe.printStackTrace();
			Logeo.log("Ocurri� un error al conectar con socket: "+ioe.getLocalizedMessage());
			//throw new Exception("Ocurri� un error de conexi�n al enviar inoformaci�n: "+ioe.getLocalizedMessage());
		}

		catch (Error e) {
			e.printStackTrace();
			Logeo.log("Error al conectar con socket: "+e.getLocalizedMessage());
			//throw new Exception("Ocurri� un error de conexi�n al enviar inoformaci�n: "+ioe.getLocalizedMessage());
		}
		
		finally {
			
			try {
				output.close();
				dataOutput.close();
				//Logeo.log("dataOutput cerrado");
			} catch (Exception e) {
				//Logeo.log("Ocurri� un error al intentar cerrar outputs");
				e.printStackTrace();
			}
			
			try {
				//input.close();
				dataInput.close();
				//Logeo.log("dataInput cerrado");
				//entrada.close();
			} catch (Exception e) {
				//Logeo.log("Ocurri� un error al intentar cerrar inputs");
				e.printStackTrace();
			}
			
			try {
				socket.close();
				//Logeo.log("socket cerrado");
			} catch (Exception e) {
				//Logeo.log("Ocurri� un error al intentar cerrar socket");
				e.printStackTrace();
			}
		
			
		}
		
		//Logeo del mensaje de respuesta
		if(mensajeRespuesta!=null){
			if(!mensajeRespuesta.isEmpty()){
				
				//Logeo del mensaje de respuesta dependiendo del tipo de operaci�n
				switch(_tipoSolicitud){
					case SOLICITUD_LLAVE:
						Logeo.log("Respuesta de Llave(Mensaje210): "+mensajeRespuesta);
						break;
					case SOLICITUD_VENTA:
						Logeo.log("Respuesta de Venta(Mensaje210): "+mensajeRespuesta);
						break;
					case SOLICITUD_REVERSO:
						Logeo.log("Respuesta de Reverso(Mensaje430): "+mensajeRespuesta);
						break;
					case SOLICITUD_DEVOLUCION:
						Logeo.log("Respuesta de Devoluci�n(Mensaje200): "+mensajeRespuesta);
						break;
				}
				
				
			}else{
				Logeo.log("Se obtiene cadena vac�a. Cancelando operaci�n.");
				mensajeRespuesta = null;
			}
		}else{
			Logeo.log("Se obtiene msj de respuesta nulo. Cancelando operaci�n.");
		}

		return mensajeRespuesta;
	}
	

}
