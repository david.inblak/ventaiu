package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

/**
 * Clase que representa una solicitud de llave recibida desde tienda con la cual
 * se procedera a armar otra solicitud a BAZ
 * 
 * @author fvegap
 * 
 */
public class PeticionLLavePinpadBean {

	private long uId;
	private int paisId;
	private int tiendaId;
	private int trace;
	private int tipoEntrada;
	private String terminal;
	private int PinEntryCapability;
	private String fechaOperacion;
	private String numTerminal;
	private String token_ES_EW;
	
	public long getuId() {
		return uId;
	}

	public void setuId(long uId) {
		this.uId = uId;
	}

	public int getPaisId() {
		return paisId;
	}

	public void setPaisId(int paisId) {
		this.paisId = paisId;
	}

	public int getTiendaId() {
		return tiendaId;
	}

	public void setTiendaId(int tiendaId) {
		this.tiendaId = tiendaId;
	}

	public int getTrace() {
		return trace;
	}

	public void setTrace(int trace) {
		this.trace = trace;
	}

	public int getTipoEntrada() {
		return tipoEntrada;
	}

	public void setTipoEntrada(int tipoEntrada) {
		this.tipoEntrada = tipoEntrada;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public int getPinEntryCapability() {
		return PinEntryCapability;
	}

	public void setPinEntryCapability(int pinEntryCapability) {
		PinEntryCapability = pinEntryCapability;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getNumTerminal() {
		return numTerminal;
	}

	public void setNumTerminal(String numTerminal) {
		this.numTerminal = numTerminal;
	}

	public String getToken_ES_EW() {
		return token_ES_EW;
	}

	public void setToken_ES_EW(String tokenESEW) {
		token_ES_EW = tokenESEW;
	}

	@Override
	public String toString() {
		return "PeticionLLavePinpadBean [PinEntryCapability="
				+ PinEntryCapability + ", fechaOperacion=" + fechaOperacion
				+ ", numTerminal=" + numTerminal + ", paisId=" + paisId
				+ ", terminal=" + terminal + ", tiendaId=" + tiendaId
				+ ", tipoEntrada=" + tipoEntrada + ", token_ES_EW="
				+ token_ES_EW + ", trace=" + trace + ", uId=" + uId + "]";
	}

	

}
