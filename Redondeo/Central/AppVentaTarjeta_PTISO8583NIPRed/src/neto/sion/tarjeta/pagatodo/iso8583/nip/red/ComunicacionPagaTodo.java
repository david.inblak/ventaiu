package neto.sion.tarjeta.pagatodo.iso8583.nip.red;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import Com.Elektra.Definition.Config.ConfigSION;

import neto.sion.Logs;
import neto.sion.genericos.log.Logeo;
import neto.sion.jax.pagatodo.iso8583.nip.proxy.WSJaxPagatodo_ISO8583_NipServiceStub.Ptiso8583BeanReq;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaBAZBean;

public class ComunicacionPagaTodo {
	private Logs logeo;
	public ComunicacionPagaTodo(){
		logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
	}
	
	private void llenarParametrosSolicitudPagaTodo(final Ptiso8583BeanReq request){
        try{
            //request.setIdAgente    ( ConfigSION.obtenerParametro("PAGATODO.DATOSCONSULTA.IDAGENTE"));
            request.setIdGrupo     ( ConfigSION.obtenerParametro("PAGATODO.DATOSCONSULTA.IDGRUPO"));
            request.setIdSucursal  ( ConfigSION.obtenerParametro("PAGATODO.DATOSCONSULTA.IDSUCURSAL"));
            request.setIdTerminal  ( ConfigSION.obtenerParametro("PAGATODO.DATOSCONSULTA.IDTERMINAL"));
            request.setIdOperador  ( ConfigSION.obtenerParametro("PAGATODO.DATOSCONSULTA.IDOPERADOR"));
            //request.setSKU         ( ConfigSION.obtenerParametro("PAGATODO.DATOSCONSULTA.SKU_CONSULTA"));
            //request.setFormaLecturaTarj( ConfigSION.obtenerParametro("PAGATODO.DATOSCONSULTA.FORMALECTURA"));
        }catch(Exception e){
        	Logeo.logearExcepcion(e, "No se encontr� uno de los parametros para la consulta de saldo en paga todo.");
        }
    }
}
