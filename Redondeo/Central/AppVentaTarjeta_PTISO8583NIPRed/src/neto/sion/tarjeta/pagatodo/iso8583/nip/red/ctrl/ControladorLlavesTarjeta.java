package neto.sion.tarjeta.pagatodo.iso8583.nip.red.ctrl;

import neto.sion.genericos.log.Logeo;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionLLaveBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionLLavePinpadBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaLlaveBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.mensajeria.GeneradorMensajePeticion;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.mensajeria.GeneradorRespuestaMensaje;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.socket.SocketBAZ;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.socket.SocketBAZ.TipoSolicitud;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.util.Utilerias;

/**
 * 
 * @author fvegap
 * 
 */
public class ControladorLlavesTarjeta {

	private GeneradorMensajePeticion generadorMensaje;
	private GeneradorRespuestaMensaje generadorRespuesta;
	private SocketBAZ socketBAZ;
	private Utilerias utilerias;
	
	private final int OP_LLAVE = 39;
	private final int OP_LLAVE_RESPONSE = 40;

	public ControladorLlavesTarjeta(Utilerias _utilerias) {
		this.utilerias = _utilerias;
		this.generadorMensaje = new GeneradorMensajePeticion(utilerias);
		this.generadorRespuesta = new GeneradorRespuestaMensaje(utilerias);
		this.socketBAZ = new SocketBAZ();
	}

	public RespuestaLlaveBAZBean enviarPeticionLLave(
			PeticionLLavePinpadBean _peticionPinpadTDA) {

		RespuestaLlaveBAZBean respuestaLlave = new RespuestaLlaveBAZBean();

		// generar peticion de Llave a BAZ a partir de peticion recibida desde
		// tienda
		PeticionLLaveBAZBean peticionLlave = generadorMensaje
				.generarPeticionLLave(_peticionPinpadTDA);
		// Crea msj 200 que se enviara a BAZ
		String mensaje200 = peticionLlave.generaMensaje();
		
		// loguea peticion en BD
		try {
			utilerias.insertaMensajeOperacion(_peticionPinpadTDA.getPaisId(), _peticionPinpadTDA.getTiendaId(), OP_LLAVE, 0, utilerias.generaMensaje200(peticionLlave));
		} catch (Exception e) {
			Logeo.logearExcepcion(e,
					"Ocurri� un error al insertar mensaje de operaci�n Petici�n Carga de llave con Uid: "+_peticionPinpadTDA.getuId());
		}

		// enviar peticion por socket y obtiene mensaje210 de respuesta
		String mensaje210 = null;
		boolean seRecibioMensaje = false;
		try {
			mensaje210 = socketBAZ.enviaMensaje(TipoSolicitud.SOLICITUD_LLAVE,mensaje200);
			if(mensaje210!=null){
				seRecibioMensaje = true;
			}
		} catch (Exception e) {
			mensaje210 = null;
		}

		if (seRecibioMensaje) {
			// analizar mensaje del socket y generar una respuesta que se
			// devuleva a tienda
			
			respuestaLlave = generadorRespuesta.generarRespuestaLlave(mensaje210);
			
			// loguea peticion en BD
			try {
				utilerias.insertaMensajeOperacion(_peticionPinpadTDA.getPaisId(), _peticionPinpadTDA.getTiendaId(), OP_LLAVE_RESPONSE, 0, utilerias.generaMensaje210(respuestaLlave));
			} catch (Exception e) {
				Logeo.logearExcepcion(e,"Ocurri� un error al insertar mensaje de operaci�n Respuesta Carga de llave con Uid: "+_peticionPinpadTDA.getuId());
			}
			
			
		}else{
			respuestaLlave = null;
		}
		
		return respuestaLlave;

	}

}
