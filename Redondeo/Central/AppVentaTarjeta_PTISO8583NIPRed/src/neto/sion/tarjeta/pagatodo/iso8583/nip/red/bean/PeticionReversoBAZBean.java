package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

public class PeticionReversoBAZBean {

	private String longitudMensaje420; // 4 Inicio de cadena
	private String tipoMensaje; // 4-MSG_TYPE-MTI
	private String bitmap; // 16-PRIMARY_BIT_MAP-000
	private String codigoProceso; // 6-PROC_CODE-004
	private String montoOperacion; // 12-TRANS_AMT-004
	private String fecha; // 10-XMT_DATE_TIME-007
	private String trace; // 6_STAN-011
	private String horaLocal; // 6_LOC_TIME-012
	private String fechaLocal; // 4-LOC_DATE-013
	private String modoEntrada; // 3-POS_ENTRY_MODE-022
	private String codigoRespuesta; // 2-RESP_CODE-039
	private String terminal; // 8-CARD_ACCPT_TRMNL_ID-041
	private String afiliacion; // 15-CARD_ACCPT_ID-042
	private String nombreComercio; // 40-ACCEPTORNAME-043
	private String tipoMoneda; // 3-CURRENCY CODE-049
	private String longitudC63; // 3-TOKEN_LEN-063
	private String C63_desc_token_ES_EZ; // Varibale, definida

	// en:(longitudC63)-TOKEN_DATA-063

	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public String getLongitudMensaje420() {
		return longitudMensaje420;
	}

	public void setLongitudMensaje420(String longitudMensaje420) {
		this.longitudMensaje420 = longitudMensaje420;
	}

	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	public String getBitmap() {
		return bitmap;
	}

	public void setBitmap(String bitmap) {
		this.bitmap = bitmap;
	}

	public String getCodigoProceso() {
		return codigoProceso;
	}

	public void setCodigoProceso(String codigoProceso) {
		this.codigoProceso = codigoProceso;
	}

	public String getMontoOperacion() {
		return montoOperacion;
	}

	public void setMontoOperacion(String montoOperacion) {
		this.montoOperacion = montoOperacion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	public String getHoraLocal() {
		return horaLocal;
	}

	public void setHoraLocal(String horaLocal) {
		this.horaLocal = horaLocal;
	}

	public String getFechaLocal() {
		return fechaLocal;
	}

	public void setFechaLocal(String fechaLocal) {
		this.fechaLocal = fechaLocal;
	}

	public String getModoEntrada() {
		return modoEntrada;
	}

	public void setModoEntrada(String modoEntrada) {
		this.modoEntrada = modoEntrada;
	}

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public String getNombreComercio() {
		return nombreComercio;
	}

	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}

	public String getTipoMoneda() {
		return tipoMoneda;
	}

	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	public String getLongitudC63() {
		return longitudC63;
	}

	public void setLongitudC63(String longitudC63) {
		this.longitudC63 = longitudC63;
	}

	public String getC63_desc_token_ES_EZ() {
		return C63_desc_token_ES_EZ;
	}

	public void setC63_desc_token_ES_EZ(String c63DescTokenESEZ) {
		C63_desc_token_ES_EZ = c63DescTokenESEZ;
	}

	public String generarMensaje() {
		return longitudMensaje420.concat(tipoMensaje).concat(bitmap).concat(
				codigoProceso).concat(montoOperacion).concat(fecha).concat(
				trace).concat(horaLocal).concat(fechaLocal).concat(modoEntrada)
				.concat(codigoRespuesta).concat(terminal).concat(afiliacion)
				.concat(nombreComercio).concat(tipoMoneda).concat(longitudC63)
				.concat(C63_desc_token_ES_EZ);

	}

	@Override
	public String toString() {
		return "PeticionReversoBAZBean [C63_desc_token_ES_EZ="
				+ C63_desc_token_ES_EZ + ", afiliacion=" + afiliacion
				+ ", bitmap=" + bitmap + ", codigoProceso=" + codigoProceso
				+ ", codigoRespuesta=" + codigoRespuesta + ", fecha=" + fecha
				+ ", fechaLocal=" + fechaLocal + ", horaLocal=" + horaLocal
				+ ", longitudC63=" + longitudC63 + ", longitudMensaje420="
				+ longitudMensaje420 + ", modoEntrada=" + modoEntrada
				+ ", montoOperacion=" + montoOperacion + ", nombreComercio="
				+ nombreComercio + ", terminal=" + terminal + ", tipoMensaje="
				+ tipoMensaje + ", tipoMoneda=" + tipoMoneda + ", trace="
				+ trace + "]";
	}

}
