package neto.sion.tarjeta.pagatodo.iso8583.nip.red.socket;

import java.text.SimpleDateFormat;
import java.util.Date;

import Com.Elektra.Definition.Config.ConfigSION;

import neto.sion.Logs;
import neto.sion.jax.pagatodo.iso8583.nip.cliente.TarjetaPagaTodoISO8583NIP;
import neto.sion.jax.pagatodo.iso8583.nip.proxy.WSJaxPagatodo_ISO8583_NipServiceStub.Ptiso8583BeanResp;
import neto.sion.venta.ventageneral.iso8583Red.bean.PeticionPagatodoNipBean;
import neto.sion.venta.ventageneral.iso8583Red.bean.RespuestaPagatodoNipBean;


public class NoSocketPagatodo {
	private Logs logeo;
	public NoSocketPagatodo(String idLog) {
		logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
		logeo.setIdLog(idLog);
	}

	RespuestaPagatodoNipBean response = new RespuestaPagatodoNipBean();
    
	public RespuestaPagatodoNipBean calculaNoSocketPagatodoPago(PeticionPagatodoNipBean soliciud){
		double montoSol = Double.parseDouble(soliciud.getMonto());
		
		SimpleDateFormat sDateFormatFolio = new SimpleDateFormat("MMddHHmmss");
		Date ahora = new Date();
		
		if( montoSol > 0 && montoSol < 50){
			response.setrCodigoRespuesta("00");
		    response.setrDescRespuesta("Ok Adelante "+montoSol);
		    response.setrNoConfirmacion(sDateFormatFolio.format(ahora));
		    response.setrSaldoDisponibleTarjeta("000000017500");	
		}else if( montoSol > 50 && montoSol < 100){
			response.setrCodigoRespuesta("01");
		    response.setrDescRespuesta("Rechazado. "+montoSol);
		    response.setrNoConfirmacion(sDateFormatFolio.format(ahora));
		    response.setrSaldoDisponibleTarjeta("000000017500");
		}else{
			response.setrCodigoRespuesta("01");
		    response.setrDescRespuesta("Rechazado por saldo insuficiente."+montoSol);
		    response.setrNoConfirmacion(sDateFormatFolio.format(ahora));
		    response.setrSaldoDisponibleTarjeta("0000000017500");
		}
				
	    return response;
	}
	
	public RespuestaPagatodoNipBean calculaNoSocketPagatodoSaldo(PeticionPagatodoNipBean soliciud){
		double montoSol = Double.parseDouble(soliciud.getMonto());
		
		SimpleDateFormat sDateFormatFolio = new SimpleDateFormat("MMddHHmmss");
		Date ahora = new Date();
		
		if( montoSol > 0 && montoSol < 100){
			response.setrCodigoRespuesta("00");
		    response.setrDescRespuesta("Ok Adelante. "+montoSol);
		    response.setrNoConfirmacion(sDateFormatFolio.format(ahora));
		    response.setrSaldoDisponibleTarjeta("00000017500");	
		}else if( montoSol > 100 && montoSol < 200){
			response.setrCodigoRespuesta("01");
		    response.setrDescRespuesta("Rechazado. "+montoSol);
		    response.setrNoConfirmacion(sDateFormatFolio.format(ahora));
		    response.setrSaldoDisponibleTarjeta("00000017500");
		}else{
			response.setrCodigoRespuesta("09");
		    response.setrDescRespuesta("Rechazado por saldo insuficiente. "+montoSol);
		    response.setrNoConfirmacion(sDateFormatFolio.format(ahora));
		    response.setrSaldoDisponibleTarjeta("000000017500");
		}
				
	    return response;
	}
	
	public RespuestaPagatodoNipBean enviarPago(PeticionPagatodoNipBean soliciud){
		
		TarjetaPagaTodoISO8583NIP cliente = new TarjetaPagaTodoISO8583NIP();
		
		Ptiso8583BeanResp response_TP = new Ptiso8583BeanResp();
	    soliciud.setTipoTarjataId(tiposTarjeta.TARJETA_BANCARIA.ordinal());
        try {
        	logeo.log("Solicitud PT pago: "+soliciud);
        	response_TP = cliente.registraPagoTarjeta(soliciud.toDto());
        	if( response_TP == null || response_TP.getRCodigoRespuesta().equals("") ){
        		response_TP.setRCodigoRespuesta("-1");
        		response_TP.setRDescRespuesta("Servicio PAGA TODO disponible, por favor intente m�s tarde.");
        	}
        } catch (Exception ex) {
        	//logeo.logearExcepcion(ex, "Error al intentar consumir el m�todo de enviao de pago a PAGA TODO");
        	if( ex.getCause() != null && ex.getCause() instanceof java.net.SocketTimeoutException ){
	        	logeo.logearExcepcion(ex, "Timeout detectado en el m�todo de enviao de pago a PAGA TODO.");
	        	/*try {
					response_TP = cliente.consultaEstTransTarjeta(soliciud.toDto());
				} catch (Exception e) {
					logeo.logearExcepcion(ex, "Ocurrio un error al verificar pago PAGA TODO.");
					response_TP = new PagatodoBeanResp();
					llenarRespuestaNoDisponible(response_TP);
					logeo.logearExcepcion(ex, "Servicio paga todo no disponible...");
				}*/
        	}else{
        		//llenarRespuestaNoDisponible(response_TP);
    			logeo.logearExcepcion(ex, "Error al intentar consumir el m�todo paga todo");
        	}
        	response_TP.setRCodigoRespuesta("100");
    		response_TP.setRDescRespuesta("Servicio PAGA TODO no disponible, por favor intente m�s tarde.");
        }
        
        return new RespuestaPagatodoNipBean(response_TP);
        //return calculaNoSocketPagatodoPago(soliciud);
	}
	
	public RespuestaPagatodoNipBean consultaSaldo(PeticionPagatodoNipBean soliciud){
		TarjetaPagaTodoISO8583NIP cliente = new TarjetaPagaTodoISO8583NIP();
		Ptiso8583BeanResp response_TP = null;
		
        try {
        	logeo.log("Solicitud PT Consulta saldo: "+soliciud);
        	response_TP = cliente.consultaSaldoTarjeta(soliciud.toDto());
        	if( response_TP == null || response_TP.getRCodigoRespuesta().equals("") ){
        		response_TP.setRCodigoRespuesta("-1");
        		response_TP.setRDescRespuesta("Servicio PAGA TODO disponible, por favor intente m�s tarde.");
        	}
        } catch (Exception ex) {
        	response_TP = new Ptiso8583BeanResp();
        	llenarRespuestaNoDisponible(response_TP);
        	//response_TP.setRCodigoRespuesta("100");
    		//response_TP.setRDescRespuesta("Servicio PAGA TODO no disponible, por favor intente m�s tarde.");
        	logeo.logearExcepcion(ex, "Servicio PAGA TODO no disponible...");
        }
        
		return new RespuestaPagatodoNipBean(response_TP);
        //return calculaNoSocketPagatodoSaldo(soliciud);*/
	}
	
	private void llenarRespuestaNoDisponible(Ptiso8583BeanResp resp){
		resp.setRCodigoRespuesta("-2");
		resp.setRDescRespuesta("Servicio PAGA TODO no disponible, por favor intente m�s tarde.");
		resp.setRNoConfirmacion("0");
		resp.setRSaldoDisponibleTarjeta("00000000000");
	}

}

enum tiposTarjeta { cero, uno, TARJETA_BANCARIA }
