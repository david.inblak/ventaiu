package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;


public class PeticionRegistroPagoBean {
	private int paProcesoExc;
	private int paisId;
	private int tiendaId;
	private String numeroTarjeta;
	private double importe;
	private double comision;
	private long pagoTarjetaId; // trace
	private String codigoRespuestaId;
	private int estatusTarjetaId;
	private String numeroAutorizacionBAZ;
	private int tipoPagoId;
	private long folioTrasaccionVenta;
	private int estatusLecturaId;

	public int getPaProcesoExc() {
		return paProcesoExc;
	}

	public void setPaProcesoExc(int paProcesoExc) {
		this.paProcesoExc = paProcesoExc;
	}

	public int getPaisId() {
		return paisId;
	}

	public void setPaisId(int paisId) {
		this.paisId = paisId;
	}

	public int getTiendaId() {
		return tiendaId;
	}

	public void setTiendaId(int tiendaId) {
		this.tiendaId = tiendaId;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public long getPagoTarjetaId() {
		return pagoTarjetaId;
	}

	public void setPagoTarjetaId(long pagoTarjetaId) {
		this.pagoTarjetaId = pagoTarjetaId;
	}

	public String getCodigoRespuestaId() {
		return codigoRespuestaId;
	}

	public void setCodigoRespuestaId(String codigoRespuestaId) {
		this.codigoRespuestaId = codigoRespuestaId;
	}

	public int getEstatusTarjetaId() {
		return estatusTarjetaId;
	}

	public void setEstatusTarjetaId(int estatusTarjetaId) {
		this.estatusTarjetaId = estatusTarjetaId;
	}

	public String getNumeroAutorizacionBAZ() {
		return numeroAutorizacionBAZ;
	}

	public void setNumeroAutorizacionBAZ(String numeroAutorizacionBAZ) {
		this.numeroAutorizacionBAZ = numeroAutorizacionBAZ;
	}

	public int getTipoPagoId() {
		return tipoPagoId;
	}

	public void setTipoPagoId(int tipoPagoId) {
		this.tipoPagoId = tipoPagoId;
	}

	public long getFolioTrasaccionVenta() {
		return folioTrasaccionVenta;
	}

	public void setFolioTrasaccionVenta(long folioTrasaccionVenta) {
		this.folioTrasaccionVenta = folioTrasaccionVenta;
	}

	public int getEstatusLecturaId() {
		return estatusLecturaId;
	}

	public void setEstatusLecturaId(int estatusLecturaId) {
		this.estatusLecturaId = estatusLecturaId;
	}

	@Override
	public String toString() {
		return "PeticionRegistroPagoBean [codigoRespuestaId="
				+ codigoRespuestaId + ", comision=" + comision
				+ ", estatusLecturaId=" + estatusLecturaId
				+ ", estatusTarjetaId=" + estatusTarjetaId
				+ ", folioTrasaccionVenta=" + folioTrasaccionVenta
				+ ", importe=" + importe + ", numeroAutorizacionBAZ="
				+ numeroAutorizacionBAZ + ", numeroTarjeta=" + numeroTarjeta
				+ ", paProcesoExc=" + paProcesoExc + ", pagoTarjetaId="
				+ pagoTarjetaId + ", paisId=" + paisId + ", tiendaId="
				+ tiendaId + ", tipoPagoId=" + tipoPagoId + "]";
	}


	
}
