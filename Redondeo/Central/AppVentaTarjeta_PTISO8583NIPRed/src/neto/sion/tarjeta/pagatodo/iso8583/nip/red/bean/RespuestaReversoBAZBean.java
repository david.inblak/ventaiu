package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

public class RespuestaReversoBAZBean {

	private String longitudMensaje430; // 4 Inicio de cadena
	private String tipoMensaje; // 4-MSG_TYPE-MTI
	private String bitmap; // 16-PRIMARY_BIT_MAP-000
	private String codigoProceso; // 6-PROC_CODE-003
	private String montoOperacion; // 12-TRANS_AMT-004
	private String fechaOperacion; // 10-XMT_DATE_TIME-007
	private String trace; // 6-STAN-011
	private String horaLocal; // 6_LOC_TIME-012
	private String fechaLocal; // 4-LOC_DATE-013
	private String inf3Digitos; // 3-"Se informan 3 d�gitos."-023
	private String referencia; // 12-RRN-037
	private String codigoRespuesta; // 2-RESP_CODE-039
	private String terminal; // 8-CARD_ACCPT_TRMNL_ID-041
	private String afiliacion; // 15-CARD_ACCPT_ID-042
	private String longitudC55; // 3-IC card system related data_Len-055
	private String C55_TAGS_EMV_FULL; // Longitud variable indicada
										// en:longitudC55-IC card system related
										// data-055

	public String getLongitudMensaje430() {
		return longitudMensaje430;
	}

	public void setLongitudMensaje430(String longitudMensaje430) {
		this.longitudMensaje430 = longitudMensaje430;
	}

	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	public String getBitmap() {
		return bitmap;
	}

	public void setBitmap(String bitmap) {
		this.bitmap = bitmap;
	}

	public String getCodigoProceso() {
		return codigoProceso;
	}

	public void setCodigoProceso(String codigoProceso) {
		this.codigoProceso = codigoProceso;
	}

	public String getMontoOperacion() {
		return montoOperacion;
	}

	public void setMontoOperacion(String montoOperacion) {
		this.montoOperacion = montoOperacion;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	public String getHoraLocal() {
		return horaLocal;
	}

	public void setHoraLocal(String horaLocal) {
		this.horaLocal = horaLocal;
	}

	public String getFechaLocal() {
		return fechaLocal;
	}

	public void setFechaLocal(String fechaLocal) {
		this.fechaLocal = fechaLocal;
	}

	public String getInf3Digitos() {
		return inf3Digitos;
	}

	public void setInf3Digitos(String inf3Digitos) {
		this.inf3Digitos = inf3Digitos;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public String getLongitudC55() {
		return longitudC55;
	}

	public void setLongitudC55(String longitudC55) {
		this.longitudC55 = longitudC55;
	}

	public String getC55_TAGS_EMV_FULL() {
		return C55_TAGS_EMV_FULL;
	}

	public void setC55_TAGS_EMV_FULL(String c55TAGSEMVFULL) {
		C55_TAGS_EMV_FULL = c55TAGSEMVFULL;
	}

	@Override
	public String toString() {
		return "RespuestaReversoBAZBean [C55_TAGS_EMV_FULL="
				+ C55_TAGS_EMV_FULL + ", afiliacion=" + afiliacion
				+ ", bitmap=" + bitmap + ", codigoProceso=" + codigoProceso
				+ ", codigoRespuesta=" + codigoRespuesta + ", fechaLocal="
				+ fechaLocal + ", fechaOperacion=" + fechaOperacion
				+ ", horaLocal=" + horaLocal + ", inf3Digitos=" + inf3Digitos
				+ ", longitudC55=" + longitudC55 + ", longitudMensaje430="
				+ longitudMensaje430 + ", montoOperacion=" + montoOperacion
				+ ", referencia=" + referencia + ", terminal=" + terminal
				+ ", tipoMensaje=" + tipoMensaje + ", trace=" + trace + "]";
	}

}
