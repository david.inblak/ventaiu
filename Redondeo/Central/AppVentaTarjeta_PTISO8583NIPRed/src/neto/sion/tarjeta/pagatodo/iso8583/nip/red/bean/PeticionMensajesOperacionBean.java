package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

public class PeticionMensajesOperacionBean {
	
	private int paisId;
	private int tiendaId;
	private int operacionId;
	private String mensaje;
	private long referenciaId;

	public int getPaisId() {
		return paisId;
	}

	public void setPaisId(int paisId) {
		this.paisId = paisId;
	}

	public int getTiendaId() {
		return tiendaId;
	}

	public void setTiendaId(int tiendaId) {
		this.tiendaId = tiendaId;
	}

	public int getOperacionId() {
		return operacionId;
	}

	public void setOperacionId(int operacionId) {
		this.operacionId = operacionId;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public long getReferenciaId() {
		return referenciaId;
	}

	public void setReferenciaId(long referenciaId) {
		this.referenciaId = referenciaId;
	}

	@Override
	public String toString() {
		return "PeticionMensajesOperacionBean [mensaje=" + mensaje
				+ ", operacionId=" + operacionId + ", paisId=" + paisId
				+ ", referenciaId=" + referenciaId + ", tiendaId=" + tiendaId
				+ "]";
	}

}
