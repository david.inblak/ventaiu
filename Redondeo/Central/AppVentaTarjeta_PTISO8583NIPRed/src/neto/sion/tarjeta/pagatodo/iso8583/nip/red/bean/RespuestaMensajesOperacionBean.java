package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

public class RespuestaMensajesOperacionBean {
	
	private long mensajeSalida;
	private int codigoError;
	private String descError;

	public long getMensajeSalida() {
		return mensajeSalida;
	}

	public void setMensajeSalida(long mensajeSalida) {
		this.mensajeSalida = mensajeSalida;
	}

	public int getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	@Override
	public String toString() {
		return "RespuestaMensajesOperacionBean [codigoError=" + codigoError
				+ ", descError=" + descError + ", mensajeSalida="
				+ mensajeSalida + "]";
	};

}
