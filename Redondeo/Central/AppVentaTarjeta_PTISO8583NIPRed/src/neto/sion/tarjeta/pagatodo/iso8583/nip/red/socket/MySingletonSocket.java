package neto.sion.tarjeta.pagatodo.iso8583.nip.red.socket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

import neto.sion.genericos.log.Logeo;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.socket.SocketBAZ.TipoSolicitud;

import Com.Elektra.Definition.Config.ConfigSION;

public class MySingletonSocket extends Socket{
	
	private static Socket clientSocket;
	private static OutputStream output;
	private static DataOutputStream dataOutput;
	private static DataInputStream dataInput;

    static {
        try {
            clientSocket = new MySingletonSocket(ConfigSION.obtenerParametro("switch.tarjetas.ip").trim(), Integer.parseInt(ConfigSION.obtenerParametro("switch.tarjetas.puerto").trim()));
			output = clientSocket.getOutputStream();
			dataOutput = new DataOutputStream(output);
			dataInput = new DataInputStream(clientSocket.getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private MySingletonSocket(final String address, final int port) throws IOException {
        super(address, port);
    }

    public static final Socket getInstance() {
        return clientSocket;
    }
    
    public String enviaMensaje(TipoSolicitud _tipoSolicitud,
			String _mensajePeticion) throws Exception {
		
		//Logeo del mensaje a enviar dependiendo del tipo de operaci�n
		switch(_tipoSolicitud){
			case SOLICITUD_LLAVE:
				Logeo.log("Solicitud de Llave(Mensaje200): "+_mensajePeticion);
				break;
			case SOLICITUD_VENTA:
				Logeo.log("Solicitud de Venta(Mensaje200): "+_mensajePeticion);
				break;
			case SOLICITUD_REVERSO:
				Logeo.log("Solicitud de Reverso(Mensaje420): "+_mensajePeticion);
				break;
			case SOLICITUD_DEVOLUCION:
				Logeo.log("Solicitud de Devoluci�n(Mensaje200): "+_mensajePeticion);
				break;
		}
		
		String mensajeRespuesta = "";
		

		try {
			
			//InetAddress addr = InetAddress.getByName("10.51.218.110");
			InetAddress addr = InetAddress.getByName(ConfigSION.obtenerParametro("switch.tarjetas.ip").trim());

			int port = Integer.parseInt(ConfigSION.obtenerParametro("switch.tarjetas.puerto").trim());
			
			int timeout = Integer.parseInt(ConfigSION.obtenerParametro("switch.tarjetas.timeout"));
			
			
			SocketAddress sockaddr = new InetSocketAddress(addr, port);

			Logeo.log("Intentando conectar en: "+ConfigSION.obtenerParametro("switch.tarjetas.ip").trim()+" puerto "+port);

			boolean estaConectado = false;
			int intento = 1;
			int numeroIntentos = 3;
			
			do{
				try{
					
					Logeo.log("sockaddr: "+sockaddr.toString());

					clientSocket.connect(sockaddr, timeout);
					
					if(clientSocket.isConnected()){
						Logeo.log("Conectado en intento: "+intento);
						estaConectado = true;
					}
				}catch(Exception e){
					Logeo.log("Ocurrio un error al intentar conectar intento -> "+intento+": "+e.getLocalizedMessage());
					e.printStackTrace();
				}
				intento++;
			}while(!estaConectado && intento <= numeroIntentos);
			

			if (clientSocket.isConnected()) {
				
				
				String newMsg = null;
				try
				{
					//newMsg = new String(_mensajePeticion.getBytes(), "UTF-8");
					newMsg = new String(_mensajePeticion.trim().getBytes(), "ISO-8859-1");
					
				}
				catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}

				Logeo.log("Conectado en: "+ConfigSION.obtenerParametro("switch.tarjetas.ip").trim());
				Logeo.log("Mensaje a enviar ISO-8859-1: " + newMsg.trim());
				

				
				dataOutput.writeBytes(newMsg);
				dataOutput.flush();
				

				Logeo.log("Mensaje enviado");
				Logeo.log("Intentando leer mensaje de respuesta");

			    
				for(int i = 0 ; i < 4 ; i++){
					//Logeo.log("dentro de primer for -> i="+i);
					mensajeRespuesta += String.valueOf((char) dataInput.readByte());
				}
				
				//Logeo.log("sale de for");

				mensajeRespuesta.replaceAll("[\\D]{0,}", "");
				
				//Logeo.log("sale de primer for");

				int longitud = Integer.parseInt(mensajeRespuesta);
				
				Logeo.log("longitud del mensaje: "+longitud);

				for(int i = 0 ; i < longitud ; i++){
					//Logeo.log("dentro de segundo for -> i="+i);
					mensajeRespuesta += String.valueOf((char) dataInput.readByte());
				}

				//Logeo.log("sale de segundo for");


				if (!mensajeRespuesta.isEmpty()) {
					Logeo.log("Se obtuvo respuesta.");
				} else {
					Logeo.log("No se pudo obtener el mensaje");
				}

			}

		}

		catch (UnknownHostException e) {
			e.printStackTrace();
			Logeo.log("Host no encontrado: " + e.getMessage());
			//throw new Exception("Ocurri� un error al intentar conectar con el host remoto: "+e.getLocalizedMessage());
		}

		catch (Exception ioe) {
			ioe.printStackTrace();
			Logeo.log("Ocurri� un error al conectar con socket: "+ioe.getLocalizedMessage());
			//throw new Exception("Ocurri� un error de conexi�n al enviar inoformaci�n: "+ioe.getLocalizedMessage());
		}

		
		finally {
			
			try {
				output.close();
				dataOutput.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Logeo.log("Ocurri� un error al intentar cerrar outputs");
				e.printStackTrace();
			}
			
			try {
				//input.close();
				dataInput.close();
				//entrada.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Logeo.log("Ocurri� un error al intentar cerrar inputs");
				e.printStackTrace();
			}
			
			try {
				clientSocket.close();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Logeo.log("Ocurri� un error al intentar cerrar socket");
				e.printStackTrace();
			}
		
			
		}
		
		//Logeo del mensaje de respuesta
		if(mensajeRespuesta!=null){
			if(!mensajeRespuesta.isEmpty()){
				
				//Logeo del mensaje de respuesta dependiendo del tipo de operaci�n
				switch(_tipoSolicitud){
					case SOLICITUD_LLAVE:
						Logeo.log("Respuesta de Llave(Mensaje210): "+mensajeRespuesta);
						break;
					case SOLICITUD_VENTA:
						Logeo.log("Respuesta de Venta(Mensaje210): "+mensajeRespuesta);
						break;
					case SOLICITUD_REVERSO:
						Logeo.log("Respuesta de Reverso(Mensaje430): "+mensajeRespuesta);
						break;
					case SOLICITUD_DEVOLUCION:
						Logeo.log("Respuesta de Devoluci�n(Mensaje200): "+mensajeRespuesta);
						break;
				}
				
				
			}else{
				Logeo.log("Se obtiene cadena vac�a. Cancelando operaci�n.");
				mensajeRespuesta = null;
			}
		}else{
			Logeo.log("Se obtiene msj de respuesta nulo. Cancelando operaci�n.");
		}

		return mensajeRespuesta;
	}

}
