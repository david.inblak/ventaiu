package neto.sion.tarjeta.pagatodo.iso8583.nip.red.ctrl;




import Com.Elektra.Definition.Config.ConfigSION;
import neto.sion.Logs;
import neto.sion.venta.ventageneral.iso8583Red.bean.WSExcepcion;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionConfirmaVentaArticulosBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionConfirmaVentaNormalBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionTransaccionCentralBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaArticulosBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.PeticionVentaNormalBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.bean.RespuestaVentaBean;
import neto.sion.venta.ventanormal.pagatodo.iso8583.red.servicio.VentaNormal;


/***
 * Clase controlador del WebService de Venta. Sirve de enlace con las clases de
 * los flujos de VentaNormal y Venta de T.A.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class VentaControlador {

	private VentaNormal ventaNormal;
	private Logs logeo;

	public VentaControlador(String idLog) {		
		logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
		logeo.setIdLog(idLog);
		ventaNormal = new VentaNormal(idLog);
	}
	
	
	

	/***
	 * Realiza una llamada al metodo consultarTransaccionCentral de la clase
	 * VentaNormal
	 * 
	 * @param _peticion
	 * @return Un long 0 => No se encontro el registro, otro =>Numero de
	 *         transaccion.
	 * @throws AxisFault
	 */
	public long consultarTransaccionCentral(
			PeticionTransaccionCentralBean _peticion) throws WSExcepcion {
		return ventaNormal.consultarTransaccionCentral(_peticion);
	}

	

	
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormal.
	 * NO GRABA IEPS
	 * 
	 * @param _peticion
	 * @deprecated NO UTILIZAR, NO GRABA IEPS
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarVenta(PeticionVentaNormalBean _peticion)
			throws WSExcepcion {
		return ventaNormal.registrarVenta(_peticion);
	}
	

	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormal.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarVenta(PeticionVentaArticulosBean _peticion, Long idPagoTarjetaPT)
			throws WSExcepcion {
		return ventaNormal.registrarVenta(_peticion, idPagoTarjetaPT);
	}
	
	public RespuestaVentaBean procesaTiposPago(PeticionVentaNormalBean _peticion, RespuestaVentaBean respuesta)
			throws WSExcepcion {
		return ventaNormal.procesaTiposPago(_peticion, respuesta);
	}
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormal.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @deprecated NO UTILIZAR, NO GRABA IEPS
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarCancelacionVenta(PeticionVentaNormalBean _peticion)
			throws WSExcepcion {
		return ventaNormal.registrarCancelacionVenta(_peticion);
	}
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormal.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarCancelacionVenta(PeticionVentaArticulosBean _peticion,int _tipoDevolucion, long paTransaccionVentaId)
			throws WSExcepcion {
		return ventaNormal.registrarCancelacionVenta(_peticion,_tipoDevolucion,paTransaccionVentaId);
	}
	
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormal.
	 * 
	 * @param _peticion
	 * @deprecated
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarConfirmacionVenta(PeticionConfirmaVentaNormalBean _peticion)
			throws WSExcepcion {
		return ventaNormal.registrarConfirmacionVenta(_peticion);
	}
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormal.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarConfirmacionVenta(PeticionConfirmaVentaArticulosBean _peticion)
			throws WSExcepcion {
		return ventaNormal.registrarConfirmacionVenta(_peticion);
	}
	
	
	
}
