package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

/**
 * Clase que representa un mensaje200 de solicitud de llave a BAZ, en comentario
 * info del campo en contrato de interfaz, tama�o, nombre y no. de campo
 * 
 * @author fvegap
 * 
 */
public class PeticionLLaveBAZBean {

	private String longitudMensaje200; // 4 Inicio de cadena
	private String tipoMensaje; // 4-MSG_TYPE-MTI
	private String bitmap; // 16-PRIMARY_BIT_MAP-000
	private String codigoProceso; // 6-PROC_CODE-004
	private String montoOperacion; // 12-TRANS_AMT-004
	private String fecha; // 10-XMT_DATE_TIME-007
	private String trace; // 6_STAN-011
	private String modoEntrada; // 3-POS_ENTRY_MODE-022
	private String terminal; // 8-CARD_ACCPT_TRMNL _ID-041
	private String nombreComercio; // 40-ACCEPTORNAME-043
	private String tipoMoneda; // 3-CURRENCY CODE-049
	private String longToken; // 3-Long. Token-063
	private String descToken_ES_EW; // Variable-Valor-Token-063 ES y EW

	public String getLongitudMensaje200() {
		return longitudMensaje200;
	}

	public void setLongitudMensaje200(String longitudMensaje200) {
		this.longitudMensaje200 = longitudMensaje200;
	}

	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	public String getBitmap() {
		return bitmap;
	}

	public void setBitmap(String bitmap) {
		this.bitmap = bitmap;
	}

	public String getCodigoProceso() {
		return codigoProceso;
	}

	public void setCodigoProceso(String codigoProceso) {
		this.codigoProceso = codigoProceso;
	}

	public String getMontoOperacion() {
		return montoOperacion;
	}

	public void setMontoOperacion(String montoOperacion) {
		this.montoOperacion = montoOperacion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	public String getModoEntrada() {
		return modoEntrada;
	}

	public void setModoEntrada(String modoEntrada) {
		this.modoEntrada = modoEntrada;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getNombreComercio() {
		return nombreComercio;
	}

	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}

	public String getTipoMoneda() {
		return tipoMoneda;
	}

	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	public String getLongToken() {
		return longToken;
	}

	public void setLongToken(String longToken) {
		this.longToken = longToken;
	}

	public String getDescToken_ES_EW() {
		return descToken_ES_EW;
	}

	public void setDescToken_ES_EW(String descTokenESEW) {
		descToken_ES_EW = descTokenESEW;
	}

	public String generaMensaje() {
		return longitudMensaje200.concat(tipoMensaje).concat(bitmap).concat(
				codigoProceso).concat(montoOperacion).concat(fecha).concat(
				trace).concat(modoEntrada).concat(terminal).concat(
				nombreComercio).concat(tipoMoneda).concat(longToken).concat(
				descToken_ES_EW);
	}

	@Override
	public String toString() {
		return "PeticionLLaveBAZBean [bitmap=" + bitmap + ", codigoProceso="
				+ codigoProceso + ", descToken_ES_EW=" + descToken_ES_EW
				+ ", fecha=" + fecha + ", longToken=" + longToken
				+ ", longitudMensaje200=" + longitudMensaje200 + ", modoEntrada="
				+ modoEntrada + ", montoOperacion=" + montoOperacion
				+ ", nombreComercio=" + nombreComercio + ", terminal="
				+ terminal + ", tipoMensaje=" + tipoMensaje + ", tipoMoneda="
				+ tipoMoneda + ", trace=" + trace + "]";
	}

}
