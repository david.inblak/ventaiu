package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

import neto.sion.venta.ventageneral.iso8583Red.bean.PeticionPagatodoBean;

public class PeticionTarjetasPagaTodoBean {
	PeticionVentaBean[] peticionesTarjeta; 
	PeticionPagatodoBean[] peticionesPagaTodo;
	
	public PeticionVentaBean[] getPeticionesTarjeta() {
		return peticionesTarjeta;
	}
	public void setPeticionesTarjeta(PeticionVentaBean[] peticionesTarjeta) {
		this.peticionesTarjeta = peticionesTarjeta;
	}
	public PeticionPagatodoBean[] getPeticionesPagaTodo() {
		return peticionesPagaTodo;
	}
	public void setPeticionesPagaTodo(PeticionPagatodoBean[] peticionesPagaTodo) {
		this.peticionesPagaTodo = peticionesPagaTodo;
	}
	
	
}
