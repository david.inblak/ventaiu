package neto.sion.tarjeta.pagatodo.iso8583.nip.red.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import Com.Elektra.Definition.Config.ConfigSION;

import neto.sion.Logs;
import neto.sion.genericos.log.Logeo;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionLLaveBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionMensajesOperacionBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionReversoBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaLlaveBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaMensajesOperacionBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaReversoBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.dao.VentaTarjetaDao;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.socket.SocketBAZ.TipoSolicitud;
import neto.sion.venta.ventageneral.iso8583Red.bean.WSExcepcion;

public class Utilerias {

	private DecimalFormat f = null;
	public static final String TOKENS = "(!.)";

	public final int ERROR_BD = 1;
	public final int ERROR_COMUNICACION_BAZ = 2;
	public final int ERROR_RECHAZO_BAZ = 3;
	
	public final int REVERSO_EXITOSO = 4;
	public final int REVRESO_ERROR_COMUNICACION_BAZ = 5;
	public final int REVERSO_ERROR_RECHAZO_BAZ = 6;
	private Logs logeo;

	public static final int METODO_LECTURA_CHIP = 1;
	public static final int METODO_LECTURA_BANDA = 0;

	public Utilerias(String idLog) {
		this.f = new DecimalFormat("##.00");
		logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
		logeo.setIdLog(idLog);
	}

	public String obtenerFecha() {

		// DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		DateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");
		Calendar Cal = Calendar.getInstance();
		return dateFormat.format(new Date());
	}

	public String actualizaToken_EW_ES(String _token) {
		StringBuilder token = new StringBuilder();

		token.append(_token);

		String f63 = _token;

		token.replace(2, 7, String.format("%05d", f63.split(TOKENS).length));
		token.replace(7, 12, String.format("%05d", token.length()));

		return token.toString();
	}

	public int calculaLongitudPeticionLLaves(PeticionLLaveBAZBean _peticion) {

		//Logeo.log("Peticion -> " + _peticion.toString());

		return (_peticion.getTipoMensaje().concat(_peticion.getBitmap())
				.concat(_peticion.getCodigoProceso()).concat(
						_peticion.getMontoOperacion()).concat(
						_peticion.getFecha()).concat(_peticion.getTrace())
				.concat(_peticion.getModoEntrada()).concat(
						_peticion.getTerminal()).concat(
						_peticion.getNombreComercio()).concat(
						_peticion.getTipoMoneda()).concat(
						_peticion.getLongToken()).concat(_peticion
				.getDescToken_ES_EW())).length();
	}

	public int calculaLongitudPeticionVenta(PeticionVentaBAZBean _peticion, int _tipoLectura) {

		//Logeo.log("Peticion Longitud -> " + _peticion.toString());

		if (_tipoLectura == METODO_LECTURA_CHIP) {

			return (_peticion.getTipoMensaje().concat(_peticion.getBitmap())
					.concat(_peticion.getCodigoProceso()).concat(_peticion
					.getMontoOperacion().concat(_peticion.getFecha()).concat(
							_peticion.getTrace()).concat(
							_peticion.getModoEntrada()).concat(
							_peticion.getTerminal()).concat(
							_peticion.getNombreComercio()).concat(
							_peticion.getTipoMoneda()).concat(
							_peticion.getLongitudC55()).concat(
							_peticion.getC55_TAGS_EMV_FULL()).concat(
							_peticion.getLongitudC63()).concat(
							_peticion.getC63_desc_token_ES_EZ()))).length();
			
		}else {
			
			///lectura por banda o fallback
			
			return (_peticion.getTipoMensaje().concat(_peticion.getBitmap())
					.concat(_peticion.getCodigoProceso()).concat(_peticion
					.getMontoOperacion().concat(_peticion.getFecha()).concat(
							_peticion.getTrace()).concat(
							_peticion.getModoEntrada()).concat(
							_peticion.getTerminal()).concat(
							_peticion.getNombreComercio()).concat(
							_peticion.getTipoMoneda()).concat(
							_peticion.getLongitudC63()).concat(
							_peticion.getC63_desc_token_ES_EZ()))).length();
			
		}
	}

	public int calculaLongitudPeticionReverso(PeticionReversoBAZBean _peticion) {
		return (_peticion.getTipoMensaje().concat(_peticion.getBitmap())
				.concat(_peticion.getCodigoProceso().concat(
						_peticion.getMontoOperacion()).concat(
						_peticion.getFecha()).concat(_peticion.getTrace())
						.concat(_peticion.getHoraLocal()).concat(
								_peticion.getFechaLocal()).concat(
								_peticion.getModoEntrada()).concat(
								_peticion.getCodigoRespuesta()).concat(
								_peticion.getTerminal()).concat(
								_peticion.getAfiliacion()).concat(
								_peticion.getNombreComercio()).concat(
								_peticion.getTipoMoneda()).concat(
								_peticion.getLongitudC63()).concat(
								_peticion.getC63_desc_token_ES_EZ()))).length();
	}

	public String formateaInt(String _cadena, int _tamanioCadena) {
		String cadena = "";

		if (_cadena.length() == _tamanioCadena) {
			cadena = _cadena;
		} else if (_cadena.length() > _tamanioCadena) {
			cadena = _cadena.substring(_cadena.length() - _tamanioCadena,
					_cadena.length() - 1);
		} else if (_cadena.length() < _tamanioCadena) {
			cadena = _cadena;
			for (int i = 0; i < (_tamanioCadena - _cadena.length()); i++) {
				String cero = "0";
				cadena = cero.concat(cadena);
			}
		}

		return cadena;
	}

	public String formatearDouble(double _monto, int _tamanioCadena) {
		String monto = "";
		String cero = "0";

		monto = f.format(_monto).replace(".", "");

		if (monto.contains(",")) {
			monto = monto.replace(",", "");
		}

		for (int i = monto.length(); i < _tamanioCadena; i++) {
			monto = cero.concat(monto);
		}

		return monto;
	}

	public String formatearLong(long _numero, int _tamanioCadena) {
		String numero = "";
		String cero = "0";
		int longitudNumero = (String.valueOf(_numero)).length();

		if (longitudNumero == _tamanioCadena) {
			numero = String.valueOf(_numero);
		} else if (longitudNumero < _tamanioCadena) {
			numero = String.valueOf(_numero);
			do {
				numero = numero.concat(cero);
				longitudNumero = (String.valueOf(numero)).length();
			} while (longitudNumero < _tamanioCadena);
		} else if (longitudNumero > _tamanioCadena) {
			numero = String.valueOf(_numero).substring(longitudNumero - 6);
		}

		return numero;
	}
	
	////mensajes operacion
	
	public String generaMensaje430 (RespuestaReversoBAZBean _respuesta){
		String cadena = "<?xml version='1.0' encoding='utf-8'?><ReversoResponse>";
		
		cadena += "<Long430>";
		cadena += (_respuesta.getLongitudMensaje430().isEmpty())?"":_respuesta.getLongitudMensaje430();
		cadena += "</Long430>";
		cadena += "<MSG_TYPE>";
		cadena += (_respuesta.getTipoMensaje().isEmpty())?"":_respuesta.getTipoMensaje();
		cadena += "</MSG_TYPE>";
		cadena += "<PRIMARY_BIT_MAP>";
		cadena += (_respuesta.getBitmap().isEmpty())?"":_respuesta.getBitmap();
		cadena += "</PRIMARY_BIT_MAP>";
		cadena += "<PROC_CODE>";
		cadena += (_respuesta.getCodigoProceso().isEmpty())?"":_respuesta.getCodigoProceso();
		cadena += "</PROC_CODE>";
		cadena += "<TRANS_AMT>";
		cadena += (_respuesta.getMontoOperacion().isEmpty())?"":_respuesta.getMontoOperacion();
		cadena += "</TRANS_AMT>";
		cadena += "<XMT_DATE_TIME>";
		cadena += (_respuesta.getFechaOperacion().isEmpty())?"":_respuesta.getFechaOperacion();
		cadena += "</XMT_DATE_TIME>";
		cadena += "<STAN>";
		cadena += (_respuesta.getTrace().isEmpty())?"":_respuesta.getTrace();
		cadena += "</STAN>";
		cadena += "<LOC_TIME>";
		cadena += (_respuesta.getHoraLocal().isEmpty())?"":_respuesta.getHoraLocal();
		cadena += "</LOC_TIME>";
		cadena += "<LOC_DATE>";
		cadena += (_respuesta.getFechaLocal().isEmpty())?"":_respuesta.getFechaLocal();
		cadena += "</LOC_DATE>";
		cadena += "<FIELD23>";
		if(_respuesta.getInf3Digitos()!=null){
			cadena += (_respuesta.getInf3Digitos().isEmpty())?"":_respuesta.getInf3Digitos();
		}
		cadena += "</FIELD23>";
		cadena += "<RRN>";
		cadena += (_respuesta.getReferencia().isEmpty())?"":_respuesta.getReferencia();
		cadena += "</RRN>";
		cadena += "<RESP_CODE>";
		cadena += (_respuesta.getCodigoRespuesta().isEmpty())?"":_respuesta.getCodigoRespuesta();
		cadena += "</RESP_CODE>";
		cadena += "<CARD_ACCPT_TRMNL_ID>";
		cadena += (_respuesta.getTerminal().isEmpty())?"":_respuesta.getTerminal();
		cadena += "</CARD_ACCPT_TRMNL_ID>";
		cadena += "<CARD_ACCPT_ID>";
		cadena += (_respuesta.getAfiliacion().isEmpty())?"":_respuesta.getAfiliacion();
		cadena += "</CARD_ACCPT_ID>";
		cadena += "<LongTokenC55>";
		if(_respuesta.getLongitudC55()!=null){
			cadena += (_respuesta.getLongitudC55().isEmpty())?"":_respuesta.getLongitudC55();
		}
		cadena += "</LongTokenC55>";
		cadena += "<C55_TAGS_EMV_FULL>";
		if(_respuesta.getC55_TAGS_EMV_FULL()!=null){
			cadena += (_respuesta.getC55_TAGS_EMV_FULL().isEmpty())?"":_respuesta.getC55_TAGS_EMV_FULL();
		}
		cadena += "</C55_TAGS_EMV_FULL>";
		
		cadena += "</ReversoResponse></xml>";

		return cadena;
	}
	
	public String generaMensaje420 (PeticionReversoBAZBean _peticion){
		String cadena = "<?xml version='1.0' encoding='utf-8'?><ReversoRequest>";
		
		cadena += "<Long420>";
		cadena += (_peticion.getLongitudMensaje420().isEmpty())?"":_peticion.getLongitudMensaje420();
		cadena += "</Long420>";
		cadena += "<MSG_TYPE>";
		cadena += (_peticion.getTipoMensaje().isEmpty())?"":_peticion.getTipoMensaje();
		cadena += "</MSG_TYPE>";
		cadena += "<PRIMARY_BIT_MAP>";
		cadena += (_peticion.getBitmap().isEmpty())?"":_peticion.getBitmap();
		cadena += "</PRIMARY_BIT_MAP>";
		cadena += "<PROC_CODE>";
		cadena += (_peticion.getCodigoProceso().isEmpty())?"":_peticion.getCodigoProceso();
		cadena += "</PROC_CODE>";
		cadena += "<TRANS_AMT>";
		cadena += (_peticion.getMontoOperacion().isEmpty())?"":_peticion.getMontoOperacion();
		cadena += "</TRANS_AMT>";
		cadena += "<XMT_DATE_TIME>";
		cadena += (_peticion.getFecha().isEmpty())?"":_peticion.getFecha();
		cadena += "</XMT_DATE_TIME>";
		cadena += "<STAN>";
		cadena += (_peticion.getTrace().isEmpty())?"":_peticion.getTrace();
		cadena += "</STAN>";
		cadena += "<LOC_TIME>";
		cadena += (_peticion.getHoraLocal().isEmpty())?"":_peticion.getHoraLocal();
		cadena += "</LOC_TIME>";
		cadena += "<LOC_DATE>";
		cadena += (_peticion.getFechaLocal().isEmpty())?"":_peticion.getFechaLocal();
		cadena += "</LOC_DATE>";
		cadena += "<POS_ENTRY_MODE>";
		cadena += (_peticion.getModoEntrada().isEmpty())?"":_peticion.getModoEntrada();
		cadena += "</POS_ENTRY_MODE>";
		cadena += "<RESP_CODE>";
		cadena += (_peticion.getCodigoRespuesta().isEmpty())?"":_peticion.getCodigoRespuesta();
		cadena += "</RESP_CODE>";
		cadena += "<CARD_ACCPT_TRMNL_ID>";
		cadena += (_peticion.getTerminal().isEmpty())?"":_peticion.getTerminal();
		cadena += "</CARD_ACCPT_TRMNL_ID>";
		cadena += "<CARD_ACCPT_ID>";
		cadena += (_peticion.getAfiliacion().isEmpty())?"":_peticion.getAfiliacion();
		cadena += "</CARD_ACCPT_ID>";
		cadena += "<ACCEPTORNAME>";
		cadena += (_peticion.getNombreComercio().isEmpty())?"":_peticion.getNombreComercio();
		cadena += "</ACCEPTORNAME>";
		cadena += "<CURRENCY_CODE>";
		cadena += (_peticion.getTipoMoneda().isEmpty())?"":_peticion.getTipoMoneda();
		cadena += "</CURRENCY_CODE>";
		cadena += "<LongTokenC63>";
		cadena += (_peticion.getLongitudC63().isEmpty())?"":_peticion.getLongitudC63();
		cadena += "</LongTokenC63>";
		cadena += "<TOKEN_ES_EY_EZ>";
		cadena += (_peticion.getC63_desc_token_ES_EZ().isEmpty())?"":_peticion.getC63_desc_token_ES_EZ();
		cadena += "</TOKEN_ES_EY_EZ>";
		
		cadena += "</ReversoRequest></xml>";

		return cadena;
	}
	
	public String generaMensaje210 (RespuestaVentaBAZBean _respuesta){
		String cadena = "<?xml version='1.0' encoding='utf-8'?><VentaResponse>";
		
		cadena += "<Long210>";
		cadena += (_respuesta.getLongitudMensaje210().isEmpty())?"":_respuesta.getLongitudMensaje210();
		cadena += "</Long210>";
		cadena += "<MSG_TYPE>";
		cadena += (_respuesta.getTipoMensaje().isEmpty())?"":_respuesta.getTipoMensaje();
		cadena += "</MSG_TYPE>";
		cadena += "<PRIMARY_BIT_MAP>";
		cadena += (_respuesta.getBitmap().isEmpty())?"":_respuesta.getBitmap();
		cadena += "</PRIMARY_BIT_MAP>";
		cadena += "<PROC_CODE>";
		cadena += (_respuesta.getCodigoProceso().isEmpty())?"":_respuesta.getCodigoProceso();
		cadena += "</PROC_CODE>";
		cadena += "<TRANS_AMT>";
		cadena += (_respuesta.getMontoOperacion().isEmpty())?"":_respuesta.getMontoOperacion();
		cadena += "</TRANS_AMT>";
		cadena += "<XMT_DATE_TIME>";
		cadena += (_respuesta.getFechaOperacion().isEmpty())?"":_respuesta.getFechaOperacion();
		cadena += "</XMT_DATE_TIME>";
		cadena += "<STAN>";
		cadena += (_respuesta.getTrace().isEmpty())?"":_respuesta.getTrace();
		cadena += "</STAN>";
		cadena += "<LOC_TIME>";
		cadena += (_respuesta.getHoraLocal().isEmpty())?"":_respuesta.getHoraLocal();
		cadena += "</LOC_TIME>";
		cadena += "<LOC_DATE>";
		cadena += (_respuesta.getFechaLocal().isEmpty())?"":_respuesta.getFechaLocal();
		cadena += "</LOC_DATE>";
		cadena += "<FIELD23>";
		if(_respuesta.getCampo23Bandera()!=null){
			cadena += (_respuesta.getCampo23Bandera().isEmpty())?"":_respuesta.getCampo23Bandera();
		}else{
			cadena += "";
		}
		cadena += "</FIELD23>";
		cadena += "<RRN>";
		cadena += (_respuesta.getReferencia().isEmpty())?"":_respuesta.getReferencia();
		cadena += "</RRN>";
		cadena += "<AUTH_ID_RESP>";
		cadena += (_respuesta.getNumAutorizacion().isEmpty())?"":_respuesta.getNumAutorizacion();
		cadena += "</AUTH_ID_RESP>";
		cadena += "<RESP_CODE>";
		cadena += (_respuesta.getCodigoRespuesta().isEmpty())?"":_respuesta.getCodigoRespuesta();
		cadena += "</RESP_CODE>";
		cadena += "<CARD_ACCPT_TRMNL_ID>";
		cadena += (_respuesta.getTerminal().isEmpty())?"":_respuesta.getTerminal();
		cadena += "</CARD_ACCPT_TRMNL_ID>";
		cadena += "<CARD_ACCPT_ID>";
		cadena += (_respuesta.getAfiliacion().isEmpty())?"":_respuesta.getAfiliacion();
		cadena += "</CARD_ACCPT_ID>";
		cadena += "<BANK_LEN>";
		cadena += (_respuesta.getLongitudTramaBanco().isEmpty())?"":_respuesta.getLongitudTramaBanco();
		cadena += "</BANK_LEN>";
		cadena += "<BANK>";
		cadena += (_respuesta.getTramaBanco().isEmpty())?"":_respuesta.getTramaBanco();
		cadena += "</BANK>";
		cadena += "<LongTokenC55>";
		if(_respuesta.getLongitudC55()!=null){
			cadena += (_respuesta.getLongitudC55().isEmpty())?"":_respuesta.getLongitudC55();
		}
		cadena += "</LongTokenC55>";
		cadena += "<C55_TAGS_EMV_FULL>";
		if(_respuesta.getC55_TAGS_EMV_FULL()!=null){
			cadena += (_respuesta.getC55_TAGS_EMV_FULL().isEmpty())?"":_respuesta.getC55_TAGS_EMV_FULL();
		}
		cadena += "</C55_TAGS_EMV_FULL>";
		cadena += "<LongTokenC63>";
		cadena += (_respuesta.getLongitudC63().isEmpty())?"":_respuesta.getLongitudC63();
		cadena += "</LongTokenC63>";
		cadena += "<TOKEN_ES_EY_EZ>";
		cadena += (_respuesta.getC63_desc_token_ES_EZ().isEmpty())?"":_respuesta.getC63_desc_token_ES_EZ();
		cadena += "</TOKEN_ES_EY_EZ>";
		
		cadena += "</VentaResponse></xml>";

		return cadena;
	}
	
	public String generaMensaje200 (PeticionVentaBAZBean _peticion){
		String cadena = "<?xml version='1.0' encoding='utf-8'?><VentaRequest>";
		
		cadena += "<Long200>";
		cadena += (_peticion.getLongitudMensaje200().isEmpty())?"":_peticion.getLongitudMensaje200();
		cadena += "</Long200>";
		cadena += "<MSG_TYPE>";
		cadena += (_peticion.getTipoMensaje().isEmpty())?"":_peticion.getTipoMensaje();
		cadena += "</MSG_TYPE>";
		cadena += "<PRIMARY_BIT_MAP>";
		cadena += (_peticion.getBitmap().isEmpty())?"":_peticion.getBitmap();
		cadena += "</PRIMARY_BIT_MAP>";
		cadena += "<PROC_CODE>";
		cadena += (_peticion.getCodigoProceso().isEmpty())?"":_peticion.getCodigoProceso();
		cadena += "</PROC_CODE>";
		cadena += "<TRANS_AMT>";
		cadena += (_peticion.getMontoOperacion().isEmpty())?"":_peticion.getMontoOperacion();
		cadena += "</TRANS_AMT>";
		cadena += "<XMT_DATE_TIME>";
		cadena += (_peticion.getFecha().isEmpty())?"":_peticion.getFecha();
		cadena += "</XMT_DATE_TIME>";
		cadena += "<STAN>";
		cadena += (_peticion.getTrace().isEmpty())?"":_peticion.getTrace();
		cadena += "</STAN>";
		cadena += "<POS_ENTRY_MODE>";
		cadena += (_peticion.getModoEntrada().isEmpty())?"":_peticion.getModoEntrada();
		cadena += "</POS_ENTRY_MODE>";
		cadena += "<CARD_ACCPT_TRMNL_ID>";
		cadena += (_peticion.getTerminal().isEmpty())?"":_peticion.getTerminal();
		cadena += "</CARD_ACCPT_TRMNL_ID>";
		cadena += "<ACCEPTORNAME>";
		cadena += (_peticion.getNombreComercio().isEmpty())?"":_peticion.getNombreComercio();
		cadena += "</ACCEPTORNAME>";
		cadena += "<CURRENCY_CODE>";
		cadena += (_peticion.getTipoMoneda().isEmpty())?"":_peticion.getTipoMoneda();
		cadena += "</CURRENCY_CODE>";
		cadena += "<LongTokenC55>";
		if(_peticion.getLongitudC55()!=null){
			cadena += (_peticion.getLongitudC55().isEmpty())?"":_peticion.getLongitudC55();
		}
		cadena += "</LongTokenC55>";
		cadena += "<C55_TAGS_EMV_FULL>";
		if(_peticion.getC55_TAGS_EMV_FULL()!=null){
			cadena += (_peticion.getC55_TAGS_EMV_FULL().isEmpty())?"":_peticion.getC55_TAGS_EMV_FULL();
		}
		cadena += "</C55_TAGS_EMV_FULL>";
		cadena += "<LongTokenC63>";
		cadena += (_peticion.getLongitudC63().isEmpty())?"":_peticion.getLongitudC63();
		cadena += "</LongTokenC63>";
		cadena += "<TOKEN_ES_EY_EZ>";
		cadena += (_peticion.getC63_desc_token_ES_EZ().isEmpty())?"":_peticion.getC63_desc_token_ES_EZ();
		cadena += "</TOKEN_ES_EY_EZ>";
		
		cadena += "</VentaRequest></xml>";

		return cadena;
	}
	
	public String generaMensaje200 (PeticionLLaveBAZBean _peticion){
		String cadena = "<?xml version='1.0' encoding='utf-8'?><CargaLlaveRequest>";
		
		cadena += "<Long200>";
		cadena += (_peticion.getLongitudMensaje200().isEmpty())?"":_peticion.getLongitudMensaje200();
		cadena += "</Long200>";
		cadena += "<MSG_TYPE>";
		cadena += (_peticion.getTipoMensaje().isEmpty())?"":_peticion.getTipoMensaje();
		cadena += "</MSG_TYPE>";
		cadena += "<PRIMARY_BIT_MAP>";
		cadena += (_peticion.getBitmap().isEmpty())?"":_peticion.getBitmap();
		cadena += "</PRIMARY_BIT_MAP>";
		cadena += "<PROC_CODE>";
		cadena += (_peticion.getCodigoProceso().isEmpty())?"":_peticion.getCodigoProceso();
		cadena += "</PROC_CODE>";
		cadena += "<TRANS_AMT>";
		cadena += (_peticion.getMontoOperacion().isEmpty())?"":_peticion.getMontoOperacion();
		cadena += "</TRANS_AMT>";
		cadena += "<XMT_DATE_TIME>";
		cadena += (_peticion.getFecha().isEmpty())?"":_peticion.getFecha();
		cadena += "</XMT_DATE_TIME>";
		cadena += "<STAN>";
		cadena += (_peticion.getTrace().isEmpty())?"":_peticion.getTrace();
		cadena += "</STAN>";
		cadena += "<POS_ENTRY_MODE>";
		cadena += (_peticion.getModoEntrada().isEmpty())?"":_peticion.getModoEntrada();
		cadena += "</POS_ENTRY_MODE>";
		cadena += "<CARD_ACCPT_TRMNL_ID>";
		cadena += (_peticion.getTerminal().isEmpty())?"":_peticion.getTerminal();
		cadena += "</CARD_ACCPT_TRMNL_ID>";
		cadena += "<ACCEPTORNAME>";
		cadena += (_peticion.getNombreComercio().isEmpty())?"":_peticion.getNombreComercio();
		cadena += "</ACCEPTORNAME>";
		cadena += "<CURRENCY_CODE>";
		cadena += (_peticion.getTipoMoneda().isEmpty())?"":_peticion.getTipoMoneda();
		cadena += "</CURRENCY_CODE>";
		cadena += "<LongToken>";
		cadena += (_peticion.getLongToken().isEmpty())?"":_peticion.getLongToken();
		cadena += "</LongToken>";
		cadena += "<TOKEN_ES_EW>";
		cadena += (_peticion.getDescToken_ES_EW().isEmpty())?"":_peticion.getDescToken_ES_EW();
		cadena += "</TOKEN_ES_EW>";
		
		cadena += "</CargaLlaveRequest></xml>";

		return cadena;
	}
	
	public String generaMensaje210 (RespuestaLlaveBAZBean _respuesta){
		String cadena = "<?xml version='1.0' encoding='utf-8'?><CargaLlaveResponse>";
		
		cadena += "<Long210>";
		cadena += (_respuesta.getLongitudMensaje210().isEmpty())?"":_respuesta.getLongitudMensaje210();
		cadena += "</Long210>";
		cadena += "<MSG_TYPE>";
		cadena += (_respuesta.getTipoMensaje().isEmpty())?"":_respuesta.getTipoMensaje();
		cadena += "</MSG_TYPE>";
		cadena += "<PRIMARY_BIT_MAP>";
		cadena += (_respuesta.getBitmap().isEmpty())?"":_respuesta.getBitmap();
		cadena += "</PRIMARY_BIT_MAP>";
		cadena += "<PROC_CODE>";
		cadena += (_respuesta.getCodigoProceso().isEmpty())?"":_respuesta.getCodigoProceso();
		cadena += "</PROC_CODE>";
		cadena += "<TRANS_AMT>";
		cadena += (_respuesta.getMontoOperacion().isEmpty())?"":_respuesta.getMontoOperacion();
		cadena += "</TRANS_AMT>";
		cadena += "<XMT_DATE_TIME>";
		cadena += (_respuesta.getFechaOperacion().isEmpty())?"":_respuesta.getFechaOperacion();
		cadena += "</XMT_DATE_TIME>";
		cadena += "<STAN>";
		cadena += (_respuesta.getTrace().isEmpty())?"":_respuesta.getTrace();
		cadena += "</STAN>";
		cadena += "<LOC_TIME>";
		cadena += (_respuesta.getHoraLocal().isEmpty())?"":_respuesta.getHoraLocal();
		cadena += "</LOC_TIME>";
		cadena += "<LOC_DATE>";
		cadena += (_respuesta.getFechaLocal().isEmpty())?"":_respuesta.getFechaLocal();
		cadena += "</LOC_DATE>";
		cadena += "<RRN>";
		cadena += (_respuesta.getReferencia().isEmpty())?"":_respuesta.getReferencia();
		cadena += "</RRN>";
		cadena += "<AUTH_ID_RESP>";
		cadena += (_respuesta.getNumAutorizacion().isEmpty())?"":_respuesta.getNumAutorizacion();
		cadena += "</AUTH_ID_RESP>";
		cadena += "<RESP_CODE>";
		cadena += (_respuesta.getCodigoRespuesta().isEmpty())?"":_respuesta.getCodigoRespuesta();
		cadena += "</RESP_CODE>";
		cadena += "<CARD_ACCPT_TRMNL_ID>";
		cadena += (_respuesta.getTerminal().isEmpty())?"":_respuesta.getTerminal();
		cadena += "</CARD_ACCPT_TRMNL_ID>";
		cadena += "<CARD_ACCPT_ID>";
		cadena += (_respuesta.getAfiliacion().isEmpty())?"":_respuesta.getAfiliacion();
		cadena += "</CARD_ACCPT_ID>";
		cadena += "<BANK_LEN>";
		cadena += (_respuesta.getLongitudTramaBanco().isEmpty())?"":_respuesta.getLongitudTramaBanco();
		cadena += "</BANK_LEN>";
		cadena += "<BANK>";
		cadena += (_respuesta.getTramaBanco().isEmpty())?"":_respuesta.getTramaBanco();
		cadena += "</BANK>";
		cadena += "<LongToken>";
		cadena += (_respuesta.getLongitudToken().isEmpty())?"":_respuesta.getLongitudToken();
		cadena += "</LongToken>";
		cadena += "<TOKEN_ER_EX>";
		cadena += (_respuesta.getDescToken_ER_EX().isEmpty())?"":_respuesta.getDescToken_ER_EX();
		cadena += "</TOKEN_ER_EX>";
		
		cadena += "</CargaLlaveResponse></xml>";

		return cadena;
	}
	
	public void insertaMensajeOperacion(int _paisId, int _tiendaId,
			int _operacionId, long _referenciaId, String _mensaje)
			throws WSExcepcion {

		PeticionMensajesOperacionBean peticion = new PeticionMensajesOperacionBean();

		peticion.setPaisId(_paisId);
		peticion.setTiendaId(_tiendaId); // tienda
		peticion.setOperacionId(_operacionId); // Id de la operacion
		peticion.setReferenciaId(_referenciaId); // movimientoId de caja
		peticion.setMensaje(_mensaje); // logueo de la peticion o respuesta

		RespuestaMensajesOperacionBean respuesta = new RespuestaMensajesOperacionBean();

		VentaTarjetaDao dao = new VentaTarjetaDao(this, logeo.getIdLog());
		respuesta = dao.registraMensajeOperacion(peticion);

	}

}
