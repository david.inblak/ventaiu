package neto.sion.tarjeta.pagatodo.iso8583.nip.red.ctrl;

import Com.Elektra.Definition.Config.ConfigSION;
import neto.sion.Logs;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionRegistroPagoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBAZBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.PeticionVentaBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaRegistroPagoBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean.RespuestaVentaBean;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.dao.VentaTarjetaDao;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.mensajeria.GeneradorMensajePeticion;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.mensajeria.GeneradorRespuestaMensaje;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.socket.NoSocketPagatodo;
import neto.sion.tarjeta.pagatodo.iso8583.nip.red.util.Utilerias;
import neto.sion.venta.ventageneral.iso8583Red.bean.PeticionPagatodoNipBean;
import neto.sion.venta.ventageneral.iso8583Red.bean.RespuestaPagatodoNipBean;

public class ControladorVentaTarjeta {

	private Logs logeo;
	
	private VentaTarjetaDao dao;
	private GeneradorMensajePeticion generadorMensaje;
	private GeneradorRespuestaMensaje generadorRespuesta;
	private Utilerias utilerias;
	
	private PeticionVentaBAZBean peticionBAZ;
	
	private final int TIPO_PAGO_TARJETA = 2; //David tenia 3; //Antes 2
	
	private final int ESTATUS_SOLICITADO = 1;
	private final int ESTATUS_ACTUALIZACION_EXITOSO = 2;
	private final int ESTATUS_ACTUALIZACION_RECHAZADO = 3;
	private final int ESTATUS_ERROR_OPERACION = 4;
	private final int ESTATUS_ACTUALIZACION_CONLUIDO = 4;
	
	private final int PROCESO_EXC_INICIAL = 1;
	private final int PROCESO_EXC_ACTUALIZACION_PAGO = 2;
	
	private final int OP_VENTA = 41;
	private final int OP_VENTA_RESPONSE = 42;
	
	//private SocketBAZ socketBAZ = new SocketBAZ();
	
	public ControladorVentaTarjeta(Utilerias _utilerias, String idLog){
		
		this.utilerias = _utilerias;
		this.generadorMensaje = new GeneradorMensajePeticion(utilerias);
		this.generadorRespuesta = new GeneradorRespuestaMensaje(utilerias);
		this.dao = new VentaTarjetaDao(utilerias, idLog);
		logeo = new Logs(ConfigSION.obtenerParametro("WSLOGNOMBRE.VENTA.TARJETAPT"));
		logeo.setIdLog(idLog);
	}
	
	public enum EstatusRegistroPago {
		INICIAL , EXITOSO , RECHAZADO, ERROR, REVERSO_EXITOSO, REVERSO_RECHAZADO, REVERSO_ERROR 
	}
	
	/*public void cerrarConexionesSocket(){
		socketBAZ.cerrarConexiones();
	}*/
	
	
	
	/**
	 * M�todo encargado de generar y enviar a BAZ peticion de Venta (Msg 200)
	 * @param _peticionTDA
	 * @param _respuestaBD
	 * @return
	 */
	public RespuestaPagatodoNipBean enviarPeticionVenta(PeticionVentaBean _peticionTDA, RespuestaRegistroPagoBean _respuestaBD,
													 PeticionPagatodoNipBean _requestPT ){
		
		RespuestaPagatodoNipBean respuesta_TP = null;//new RespuestaPagatodoBean();
		//genera una peticion de venta que se enviara a switch de una peticion recibida desde TDA
		peticionBAZ = new PeticionVentaBAZBean();
		
		// loguea peticion en BD
		try {
			logeo.log("pago tarjeta :: paga todo :: inserta mensaje solicitud > "+_requestPT);
			utilerias.insertaMensajeOperacion(_peticionTDA.getPaisId(), _peticionTDA.getTiendaId(), 
											  OP_VENTA, _respuestaBD.getTrace(), _requestPT.toString() );
		} catch (Exception e) {
			logeo.logearExcepcion(e,"Ocurri� un error al insertar mensaje de operaci�n Petici�n de Venta con Uid: "+_peticionTDA.getuId());
		}
		
		//enviar peticion por cliente de PTdo
		boolean seRecibioMensaje = false;
		
		NoSocketPagatodo connPagatodo = new NoSocketPagatodo(logeo.getIdLog());		
		respuesta_TP = connPagatodo.enviarPago(_requestPT);
		logeo.log("Respuesta Solicitud Pago PTISO8583: "+respuesta_TP);
		
		if(respuesta_TP!=null){
			seRecibioMensaje = true;
		}
		
		
		if(seRecibioMensaje){
			//analizar mensaje del socket y generar una respuesta que se devuleva a tienda
			//respuestaVenta = respuesta_TP;//generadorRespuesta.generarRespuestaVenta(respuesta_TP, _peticionTDA);
			// loguea peticion en BD
			try {
				logeo.log("pago tarjeta :: paga todo :: inserta mensaje respuesta > "+respuesta_TP);
				utilerias.insertaMensajeOperacion(
						_peticionTDA.getPaisId(), 
						_peticionTDA.getTiendaId(), 
						OP_VENTA_RESPONSE, 
						_respuestaBD.getTrace(), 
						respuesta_TP.toString());
			} catch (Exception e) {
				e.printStackTrace();
				logeo.logearExcepcion(e,
						"Ocurri� un error al insertar mensaje de operaci�n Petici�n de Venta con Uid: "+_peticionTDA.getuId());
			}
			
		}else{
			logeo.log("pago tarjeta :: paga todo :: respuesta >>> NULA");
			//respuestaVenta = null;
		}
		
		return respuesta_TP;
	}
	
	/**
	 * M�todo encargado de registrar en BD el pago con tarjeta
	 * @param _peticion
	 * @return
	 */
	public RespuestaRegistroPagoBean registrarPago(PeticionVentaBean _peticionTDA, PeticionPagatodoNipBean _requestPT){
		
		logeo.log("peticionTDA: "+_peticionTDA);
		logeo.log("PAGA TODO Pago con tarjeta paso 1: "+_peticionTDA.getuId());
		
		RespuestaRegistroPagoBean respuesta = new RespuestaRegistroPagoBean();
		
		//primaro armar peticion de registro en BD a partir de la peticion recibida desde TDA
		PeticionRegistroPagoBean peticionRegistro = new PeticionRegistroPagoBean();
		
		peticionRegistro.setComision (_peticionTDA.getComision());
		peticionRegistro.setEstatusLecturaId(_peticionTDA.getEstatusLecturaId());  // 0 -> banda/fallback, 1 -> chip
		peticionRegistro.setFolioTrasaccionVenta(Long.parseLong(_requestPT.getNoTicket())); // 0 ya que no existe registro previo
		peticionRegistro.setImporte(_peticionTDA.getMonto());
		peticionRegistro.setNumeroAutorizacionBAZ("0"); //0 ya que hasta este punto aun no se ha enviado el pago a BAZ
		peticionRegistro.setNumeroTarjeta(_peticionTDA.getNumeroTarjeta());
		peticionRegistro.setPagoTarjetaId(0); //0 ya que aun no se cuenta con el
		peticionRegistro.setPaisId(_peticionTDA.getPaisId());
		peticionRegistro.setPaProcesoExc(PROCESO_EXC_INICIAL);
		peticionRegistro.setTiendaId(_peticionTDA.getTiendaId());
		peticionRegistro.setTipoPagoId(TIPO_PAGO_TARJETA);//_requestPT.getTipoTarjataId()
		peticionRegistro.setCodigoRespuestaId(""); //0 ya que aun no se cuenta con el
		peticionRegistro.setEstatusTarjetaId(ESTATUS_SOLICITADO); //ya que por default registra 1 cuando se trata de registrar
		
		//registrar en BD el pago
		
		respuesta = dao.registraPagoTarjeta(peticionRegistro);
		
		return respuesta;
		
	}
	
	/**
	 * M�todo encargado de actualizar en BD un pago registrado previamente y que ya fue enviado a BAZ
	 * @param _peticionTDA
	 * @param _respuestaRegistroBD
	 * @return
	 */
	public RespuestaRegistroPagoBean actualizarPago(PeticionVentaBean _peticionTDA, 
													RespuestaRegistroPagoBean _respuestaRegistroBD,
													RespuestaPagatodoNipBean _respuestaPTdo,
													PeticionPagatodoNipBean _solicitudPT,
													EstatusRegistroPago _estatus ){
		
		RespuestaRegistroPagoBean respuesta = new RespuestaRegistroPagoBean();
		
		//se arma la peticion de actualizacion usando los datos de respuesta del registro del pago en bd
		//mas la peticion original de tda
		
		PeticionRegistroPagoBean peticionRegistro = new PeticionRegistroPagoBean();
		
		switch(_estatus){
			case INICIAL:
				peticionRegistro.setPaProcesoExc(PROCESO_EXC_INICIAL); //ya que por default registra 1 cuando se trata de registrar
				//peticionRegistro.setNumeroAutorizacionBAZ(_respuestaPTdo.getrNoSecUnicoPT()); //numero de autorizacion devuelto por BAZ
				peticionRegistro.setEstatusTarjetaId(ESTATUS_SOLICITADO);
				//peticionRegistro.setCodigoRespuestaId(_respuestaPTdo.getrCodigoRespuesta());
			
				break;
			
			case EXITOSO:
								
				peticionRegistro.setPaProcesoExc(PROCESO_EXC_ACTUALIZACION_PAGO); //ya que por default registra 1 cuando se trata de registrar
				peticionRegistro.setNumeroAutorizacionBAZ(_respuestaPTdo.getrNoSecUnicoPT()); //numero de autorizacion devuelto por BAZ
				peticionRegistro.setEstatusTarjetaId(ESTATUS_ACTUALIZACION_EXITOSO);
				peticionRegistro.setCodigoRespuestaId(_respuestaPTdo.getrCodigoRespuesta());
				
				break;
			case RECHAZADO:
	         
				
				peticionRegistro.setPaProcesoExc(PROCESO_EXC_ACTUALIZACION_PAGO); //ya que por default registra 1 cuando se trata de registrar
				peticionRegistro.setNumeroAutorizacionBAZ("");
				peticionRegistro.setEstatusTarjetaId(ESTATUS_ACTUALIZACION_RECHAZADO);
				peticionRegistro.setCodigoRespuestaId(_respuestaPTdo.getrCodigoRespuesta());
				
				break;
			case ERROR:
				
				peticionRegistro.setPaProcesoExc(PROCESO_EXC_ACTUALIZACION_PAGO); //ya que por default registra 1 cuando se trata de registrar
				peticionRegistro.setNumeroAutorizacionBAZ("");
				peticionRegistro.setEstatusTarjetaId(ESTATUS_ERROR_OPERACION);
				peticionRegistro.setCodigoRespuestaId("");
				
				break;
		}
		
		
		peticionRegistro.setPagoTarjetaId(_respuestaRegistroBD.getTrace()); //trace obtenido en el registro del pago
		peticionRegistro.setComision(_peticionTDA.getComision());
		peticionRegistro.setFolioTrasaccionVenta(Long.parseLong(_respuestaPTdo.getrNoTicket())); // 0 ya que no existe registro previo
		peticionRegistro.setImporte(_peticionTDA.getMonto());
		peticionRegistro.setNumeroTarjeta(_peticionTDA.getNumeroTarjeta());
		peticionRegistro.setEstatusLecturaId(_peticionTDA.getEstatusLecturaId());
		peticionRegistro.setPaisId(_peticionTDA.getPaisId());
		peticionRegistro.setTiendaId(_peticionTDA.getTiendaId());
		peticionRegistro.setTipoPagoId(_solicitudPT.getTipoTarjataId());
		
		
		
		//registrar en BD el pago
		
		respuesta = dao.registraPagoTarjeta(peticionRegistro);
		
		return respuesta;
		
	}
	
	/**
	 * M�todo encargado de actualizar en BD un pago registrado previamente y que ya fue enviado a BAZ
	 * @param _peticionTDA
	 * @param _respuestaRegistroBD
	 * @return
	 */
	public RespuestaRegistroPagoBean confirmaPagoTarjeta(
				PeticionVentaBean _peticionTDA, 
				RespuestaRegistroPagoBean _respuestaRegistroBD,
				RespuestaPagatodoNipBean _respuestaPTdo,
				PeticionPagatodoNipBean _requestPT,
				Long transaccionId)
	{
		RespuestaRegistroPagoBean respuesta = new RespuestaRegistroPagoBean();
		PeticionRegistroPagoBean peticionRegistro = new PeticionRegistroPagoBean();
		
		try{		
			peticionRegistro.setPaProcesoExc			(Integer.valueOf(ConfigSION.obtenerParametro("venta.proceso.3.id"))); //ya que por default registra 1 cuando se trata de registrar
			peticionRegistro.setEstatusTarjetaId		(Integer.valueOf(ConfigSION.obtenerParametro("venta.proceso.3.estatus.tarjeta.7")));
			peticionRegistro.setNumeroAutorizacionBAZ	(_respuestaPTdo.getrNoConfirmacion()); //numero de autorizacion devuelto por BAZ
			peticionRegistro.setCodigoRespuestaId		(_respuestaPTdo.getrCodigoRespuesta());
			
			peticionRegistro.setPagoTarjetaId			(_respuestaRegistroBD.getTrace()); //trace obtenido en el registro del pago
			peticionRegistro.setComision				(_peticionTDA.getComision());
			peticionRegistro.setImporte					(_peticionTDA.getMonto());  //_respuestaPTdo.getrMontoAbonado();
			peticionRegistro.setNumeroTarjeta			(_peticionTDA.getNumeroTarjeta());
			peticionRegistro.setEstatusLecturaId		(_peticionTDA.getEstatusLecturaId());
			peticionRegistro.setPaisId					(_peticionTDA.getPaisId());
			peticionRegistro.setTiendaId				(_peticionTDA.getTiendaId());
			
			//peticionRegistro.setTipoPagoId				(_requestPT.getTipoTarjataId());//TIPO_PAGO_TARJETA
			peticionRegistro.setTipoPagoId				(TIPO_PAGO_TARJETA);
			peticionRegistro.setFolioTrasaccionVenta	(transaccionId); // 0 ya que no existe registro previo
			
			logeo.log("pago tarjeta :: solicitud :: 3 :: EXITOSO :: "+peticionRegistro);
			respuesta = dao.registraPagoTarjeta(peticionRegistro);
		}catch(Exception e){
			logeo.logearExcepcion(e, "");
		}
		
		return respuesta;
	}
	
	public RespuestaRegistroPagoBean cancelaPagoTarjeta(
			PeticionVentaBean _peticionTDA, 
			RespuestaRegistroPagoBean _respuestaRegistroBD,
			RespuestaPagatodoNipBean _respuestaPTdo,
			PeticionPagatodoNipBean _requestPT,
			long transaccionId)
	{
		RespuestaRegistroPagoBean respuesta = new RespuestaRegistroPagoBean();
		PeticionRegistroPagoBean peticionRegistro = new PeticionRegistroPagoBean();
				
		peticionRegistro.setPaProcesoExc			(Integer.valueOf(ConfigSION.obtenerParametro("venta.proceso.4.id")));
		peticionRegistro.setEstatusTarjetaId		(Integer.valueOf(ConfigSION.obtenerParametro("venta.proceso.4.estatus.tarjeta.cancelacionventa")));
		peticionRegistro.setNumeroAutorizacionBAZ	( _respuestaPTdo==null? "" : _respuestaPTdo.getrNoConfirmacion()); //numero de autorizacion devuelto por BAZ
		peticionRegistro.setCodigoRespuestaId		( _respuestaPTdo==null? "" : _respuestaPTdo.getrCodigoRespuesta());
		
		peticionRegistro.setPagoTarjetaId			(_respuestaRegistroBD.getTrace()); //trace obtenido en el registro del pago
		peticionRegistro.setComision				(_peticionTDA.getComision());
		peticionRegistro.setImporte					(_peticionTDA.getMonto());
		peticionRegistro.setNumeroTarjeta			(_peticionTDA.getNumeroTarjeta());
		peticionRegistro.setEstatusLecturaId		(_peticionTDA.getEstatusLecturaId());
		peticionRegistro.setPaisId					(_peticionTDA.getPaisId());
		peticionRegistro.setTiendaId				(_peticionTDA.getTiendaId());
		//peticionRegistro.setTipoPagoId				(_requestPT.getTipoTarjataId());
		peticionRegistro.setTipoPagoId				(TIPO_PAGO_TARJETA);
		peticionRegistro.setFolioTrasaccionVenta	(transaccionId); // 0 ya que no existe registro previo
		
		logeo.log("pago tarjeta :: solicitud :: 4 :: CANCELADO :: "+peticionRegistro);
		respuesta = dao.registraPagoTarjeta(peticionRegistro);
		
		return respuesta;
	}
	
	
	
	/**
	 * 
	 * @param _respuestaPTdo
	 * @return
	 */
	public RespuestaVentaBean generarRespuestaServicio(
			PeticionVentaBAZBean _peticionSwitch, final RespuestaPagatodoNipBean _respuestaPTdo, long _trace){
		RespuestaVentaBean respuesta = new RespuestaVentaBean();
		
		logeo.log("Generando respuesta que se enviar a tienda.");
		respuesta.setRespuestaPagaTodo(_respuestaPTdo);
		if(_respuestaPTdo == null){
			logeo.log("Respuesta de PAGA TODO nula, generando respuesta con error de WS.");
			respuesta.setCodigoError(utilerias.ERROR_COMUNICACION_BAZ);
			respuesta.setDescError("["+utilerias.ERROR_COMUNICACION_BAZ+"] Error al momento de aplicar el cobro con tarjeta PAGA TODO. Por favor vuelva a intentar.");
			respuesta.setRespuestaBAZ(null);
			respuesta.setRespuestaReversoBAZ(null);
			respuesta.setModoEntrada(_peticionSwitch.getModoEntrada());
			respuesta.setTrace(_trace);
		}else{
			
			if(_respuestaPTdo.getrCodigoRespuesta().equals("00")){
				logeo.log("Respuesta de PAGA TODO exitosa, generando respuesta exitosa.");
				respuesta.setCodigoError(0);
				respuesta.setDescError("El pago se realiz� correctamente");
				respuesta.setModoEntrada(_peticionSwitch.getModoEntrada());
				respuesta.setRespuestaBAZ(null);//_respuestaSwitch);
				respuesta.setRespuestaReversoBAZ(null);
				respuesta.setPeticionBAZ(_peticionSwitch);
				respuesta.setTrace(_trace);
			}else{
				logeo.log("Respuesta de PAGA TODO rechazada, generando respuesta con error por rechazo");
				//
				respuesta.setCodigoError(utilerias.ERROR_RECHAZO_BAZ);
				
				//respuesta.setDescError("["+utilerias.ERROR_RECHAZO_BAZ+"] "+dao.getMensajeRespuesta(_respuestaSwitch.getCodigoRespuesta()));
				respuesta.setDescError("["+_respuestaPTdo.getrCodigoRespuesta()+"] "+ _respuestaPTdo.getrDescRespuesta() );
				respuesta.setRespuestaBAZ(null);
				respuesta.setRespuestaReversoBAZ(null);
				respuesta.setModoEntrada(_peticionSwitch.getModoEntrada());
				respuesta.setPeticionBAZ(_peticionSwitch);
				respuesta.setTrace(_trace);
			}
			
		}
			
		
		return respuesta;
	}
	
	public PeticionVentaBAZBean getPeticionBAZ(){
		return peticionBAZ;
	}
	
}
