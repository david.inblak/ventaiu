package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

public class RespuestaRegistroPagoBean {

	private long trace; // pagoTarjetaId en base
	private int codigoError;
	private String descError;

	public long getTrace() {
		return trace;
	}

	public void setTrace(long trace) {
		this.trace = trace;
	}

	public int getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	@Override
	public String toString() {
		return "RespuestaRegistroPagoBean [codigoError=" + codigoError
				+ ", descError=" + descError + ", trace=" + trace + "]";
	}

}
