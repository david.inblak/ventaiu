package neto.sion.tarjeta.pagatodo.iso8583.nip.red.bean;

public class PeticionReversoBean {

	private int paisId;
	private int tiendaId;
	private long uId;
	private String montoOperacion;
	private String trace;
	private String horalocal;
	private String fechaLocal;
	private String modoEntrada;
	private String codigoRespuesta;
	private String terminal;
	private String afiliacion;
	private String longitudC63;
	private String C63_desc_token_ES_EZ;

	public String getMontoOperacion() {
		return montoOperacion;
	}

	public void setMontoOperacion(String montoOperacion) {
		this.montoOperacion = montoOperacion;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	public String getHoralocal() {
		return horalocal;
	}

	public void setHoralocal(String horalocal) {
		this.horalocal = horalocal;
	}

	public String getFechaLocal() {
		return fechaLocal;
	}

	public void setFechaLocal(String fechaLocal) {
		this.fechaLocal = fechaLocal;
	}

	public String getModoEntrada() {
		return modoEntrada;
	}

	public void setModoEntrada(String modoEntrada) {
		this.modoEntrada = modoEntrada;
	}

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public String getLongitudC63() {
		return longitudC63;
	}

	public void setLongitudC63(String longitudC63) {
		this.longitudC63 = longitudC63;
	}

	public String getC63_desc_token_ES_EZ() {
		return C63_desc_token_ES_EZ;
	}

	public void setC63_desc_token_ES_EZ(String c63DescTokenESEZ) {
		C63_desc_token_ES_EZ = c63DescTokenESEZ;
	}

	public int getPaisId() {
		return paisId;
	}

	public void setPaisId(int paisId) {
		this.paisId = paisId;
	}

	public int getTiendaId() {
		return tiendaId;
	}

	public void setTiendaId(int tiendaId) {
		this.tiendaId = tiendaId;
	}

	public long getuId() {
		return uId;
	}

	public void setuId(long uId) {
		this.uId = uId;
	}

	@Override
	public String toString() {
		return "PeticionReversoBean [C63_desc_token_ES_EZ="
				+ C63_desc_token_ES_EZ + ", afiliacion=" + afiliacion
				+ ", codigoRespuesta=" + codigoRespuesta + ", fechaLocal="
				+ fechaLocal + ", horalocal=" + horalocal + ", longitudC63="
				+ longitudC63 + ", modoEntrada=" + modoEntrada
				+ ", montoOperacion=" + montoOperacion + ", paisId=" + paisId
				+ ", terminal=" + terminal + ", tiendaId=" + tiendaId
				+ ", trace=" + trace + ", uId=" + uId + "]";
	}

}
