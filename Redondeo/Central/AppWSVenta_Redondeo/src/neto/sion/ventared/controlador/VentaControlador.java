package neto.sion.ventared.controlador;

import neto.sion.ventared.ventanormal.bean.PeticionVentaArticulosBean;
import neto.sion.ventared.ventanormal.bean.RespuestaVentaBean;
import neto.sion.ventared.ventanormal.servicio.VentaNormal;

import org.apache.axis2.AxisFault;

/***
 * Clase controlador del WebService de Venta. Sirve de enlace con las clases de
 * los flujos de VentaNormal y Venta de T.A.
 * 
 * @author Carlos V. Perez L.
 * 
 */
public class VentaControlador {

	private VentaNormal ventaNormal;

	public VentaControlador() {
		ventaNormal = new VentaNormal();
	}
	
	
	/***
	 * Realiza una llamada al metodo registrarVenta de la clase VentaNormal.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles del registro de la venta.
	 * @throws AxisFault
	 */
	public RespuestaVentaBean registrarVenta(PeticionVentaArticulosBean _peticion)
			throws AxisFault {
		return ventaNormal.registrarVenta(_peticion);
	}
}
