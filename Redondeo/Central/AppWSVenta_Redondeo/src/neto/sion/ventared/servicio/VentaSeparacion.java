package neto.sion.ventared.servicio;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import neto.sion.ventared.controlador.VentaControlador;
import neto.sion.ventared.ventanormal.bean.PeticionVentaArticulosBean;
import neto.sion.ventared.ventanormal.bean.RespuestaVentaBean;

import org.apache.axis2.AxisFault;

/***
 * Clase que representa al WebService de Venta
 * 
 * @author Carlos V. Perez L.
 * 
 */
@WebService
public class VentaSeparacion {
	private VentaControlador controlador;

	public VentaSeparacion() {
		controlador = new VentaControlador();
	}
	
	/***
	 * Realiza el registro de la venta de productos.
	 * 
	 * @param _peticion
	 * @return Un objeto con los detalles de la venta.
	 * @throws AxisFault
	 */
	@WebMethod
	public RespuestaVentaBean registrarVentaArticulos(
			@WebParam(name = "PeticionVentaArticulosBean") PeticionVentaArticulosBean _peticion)
			throws AxisFault {
		return controlador.registrarVenta(_peticion);
	}
	
}
