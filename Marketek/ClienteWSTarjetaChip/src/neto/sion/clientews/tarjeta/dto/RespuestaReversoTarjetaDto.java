package neto.sion.clientews.tarjeta.dto;

public class RespuestaReversoTarjetaDto extends RespuestaTarjeta
{
	private String afiliacion;
	private String autorizacion;
	private String bitMap;
	private String codRespuesta;
	private String codProceso;
	private String descripcion;
	private String fechaHora;
	private String fechaLocal;
	private String horaLocal;
	private String mensaje;
	private String monto;
	private String referencia;
	private String tarjetaId;
	private String terminal;
	private String tipoMensaje;
	private String trace;

	public RespuestaReversoTarjetaDto() {
		super();
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public String getAutorizacion() {
		return autorizacion;
	}
	
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	
	public String getBitMap() {
		return bitMap;
	}
	
	public void setBitMap(String bitMap) {
		this.bitMap = bitMap;
	}

	public String getCodRespuesta() {
		return codRespuesta;
	}

	public void setCodRespuesta(String codRespuesta) {
		this.codRespuesta = codRespuesta;
	}

	public String getCodProceso() {
		return codProceso;
	}

	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getFechaHora() {
		return fechaHora;
	}
	
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
	
	public String getFechaLocal() {
		return fechaLocal;
	}
	
	public void setFechaLocal(String fechaLocal) {
		this.fechaLocal = fechaLocal;
	}
	
	public String getHoraLocal() {
		return horaLocal;
	}
	
	public void setHoraLocal(String horaLocal) {
		this.horaLocal = horaLocal;
	}
	
	public String getMensaje() {
		return mensaje;
	}
	
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getReferencia() {
		return referencia;
	}
	
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	public String getTarjetaId() {
		return tarjetaId;
	}
	
	public void setTarjetaId(String tarjetaId) {
		this.tarjetaId = tarjetaId;
	}
	
	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getTipoMensaje() {
		return tipoMensaje;
	}
	
	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}
	
	public String getTrace() {
		return trace;
	}
	
	public void setTrace(String trace) {
		this.trace = trace;
	}
	

	@Override
	public String toString() {
		return "RespuestaReversoBean ["
				+ super.toString()
				+ ", afiliacion=" + afiliacion
				+ ", autorizacion=" + autorizacion
				+ ", bitMap=" + bitMap 
				+ ", codRespuesta=" + codRespuesta
				+ ", codProceso=" + codProceso
				+ ", descripcion=" + descripcion
				+ ", fechaHora=" + fechaHora
				+ ", fechaLocal=" + fechaLocal
				+ ", horaLocal=" + horaLocal
				+ ", mensaje=" + mensaje
				+ ", monto=" + monto
				+ ", referencia=" + referencia
				+ ", tarjetaId=" + tarjetaId
				+ ", terminal=" + terminal
				+ ", tipoMensaje=" + tipoMensaje
				+ ", trace=" + trace 
				+ "]";
	}	
}
