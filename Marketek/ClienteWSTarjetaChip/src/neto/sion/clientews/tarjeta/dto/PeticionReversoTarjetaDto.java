package neto.sion.clientews.tarjeta.dto;

import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;




@SuppressWarnings("serial")
public class PeticionReversoTarjetaDto extends PeticionBaseVentaDto
{
	private String afiliacion;
	private boolean chip;
	private double comision;
	private boolean falloLecturaChip;
	private String fechaLocal;
	private String horaLocal;
	private double monto;
	private String numTerminal;
	private long pagoTarjetaId;
	private String trace;
	private String track2;
	private String canalPago;

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public boolean isChip() {
		return chip;
	}

	public void setChip(boolean chip) {
		this.chip = chip;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public boolean isFalloLecturaChip() {
		return falloLecturaChip;
	}

	public void setFalloLecturaChip(boolean falloLecturaChip) {
		this.falloLecturaChip = falloLecturaChip;
	}

	public String getFechaLocal() {
		return fechaLocal;
	}

	public void setFechaLocal(String fechaLocal) {
		this.fechaLocal = fechaLocal;
	}

	public String getHoraLocal() {
		return horaLocal;
	}

	public void setHoraLocal(String horaLocal) {
		this.horaLocal = horaLocal;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getNumTerminal() {
		return numTerminal;
	}

	public void setNumTerminal(String numTerminal) {
		this.numTerminal = numTerminal;
	}

	public long getPagoTarjetaId() {
		return pagoTarjetaId;
	}

	public void setPagoTarjetaId(long pagoTarjetaId) {
		this.pagoTarjetaId = pagoTarjetaId;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	public String getTrack2() {
		return track2;
	}

	public void setTrack2(String track2) {
		this.track2 = track2;
	}

	public String getCanalPago() {
		return canalPago;
	}

	public void setCanalPago(String canalPago) {
		this.canalPago = canalPago;
	}

	@Override
	public String toString() {
		return "PeticionReversoBean ["
				+ super.toString()
				+ ", afiliacion=" + afiliacion
				+ ", chip=" + chip 
				+ ", comision=" + comision
				+ ", falloLecturaChip=" + falloLecturaChip
				+ ", fechaLocal=" + fechaLocal
				+ ", horaLocal=" + horaLocal
				+ ", monto=" + monto
				+ ", numTerminal=" + numTerminal
				+ ", pagoTarjetaId=" + pagoTarjetaId 
				+ ", trace=" + trace 
				+ ", track2=" + track2
				+ ", canalPago=" + canalPago
				+ "]";
	}
}
