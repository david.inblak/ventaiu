package neto.sion.tienda.dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import oracle.jdbc.OracleTypes;

public class TipoTarjetaDao {
 
    public int obtieneTipoTarjeta(String numeroTarjeta)
    {
        SION.log(Modulo.VENTA,"Obtener el tipo de tarjeta: " + numeroTarjeta, Level.FINE);
        String sql = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        int tipo=-1;
        try
        {
            sql = SION.obtenerParametro(Modulo.VENTA,"USRVELIT.PAVENTAS.SPCONSULTATIPOTARJETA");
            SION.log(Modulo.VENTA,"SQL a ejecutar... " + sql, Level.FINE);
            cs = Conexion.prepararLlamada(sql);	
            cs.setString(1, numeroTarjeta);	
            cs.registerOutParameter(2, OracleTypes.NUMBER);
            cs.execute();
            
            tipo = cs.getInt(2);
        }//try
        catch(Exception e)
        {
            SION.logearExcepcion(Modulo.VENTA, e, sql);
        }//catch
        finally
        {
            Conexion.cerrarResultSet(rs);
            Conexion.cerrarCallableStatement(cs);
        }//finally
        SION.log(Modulo.VENTA,"Tipo de tarjeta: " + tipo, Level.FINE);
        return tipo;
    }//calculaComision
    
    
    public boolean esTarjetaPagaTodo(String numeroTarjeta)
    {
        SION.log(Modulo.VENTA,"Obtener el tipo de tarjeta: " + numeroTarjeta, Level.FINE);
        String sql = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        int esPagaTodo=-1;
        try
        {
            sql = SION.obtenerParametro(Modulo.VENTA,"USRVELIT.PAVENTAS.SPCONSULTATIPOTARJETAPT");
            SION.log(Modulo.VENTA,"SQL a ejecutar... " + sql, Level.FINE);
            cs = Conexion.prepararLlamada(sql);	
            cs.setString(1, numeroTarjeta);	
            cs.registerOutParameter(2, OracleTypes.NUMBER);
            cs.execute();
            
            esPagaTodo = cs.getInt(2);
        }//try
        catch(Exception e)
        {
            SION.logearExcepcion(Modulo.VENTA, e, sql);
        }//catch
        finally
        {
            Conexion.cerrarResultSet(rs);
            Conexion.cerrarCallableStatement(cs);
        }//finally
        SION.log(Modulo.VENTA,"Tipo de tarjeta: " + esPagaTodo, Level.FINE);
        return esPagaTodo == 1;
    }//calculaComision

    public boolean esTarjetaDescuento(String numeroTarjeta)
    {
        SION.log(Modulo.VENTA,"Valida si es tarjeta de descuento: " + numeroTarjeta, Level.FINE);
        String sql = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        int esPagaTodo=-1;
        try
        {
            sql = SION.obtenerParametro(Modulo.VENTA,"USRVELIT.PAVENTAS.SPCONSULTATIPOTARJETACS");
            SION.log(Modulo.VENTA,"SQL a ejecutar... " + sql, Level.FINE);
            cs = Conexion.prepararLlamada(sql);	
            cs.setString(1, numeroTarjeta);	
            cs.registerOutParameter(2, OracleTypes.NUMBER);
            cs.execute();
            
            esPagaTodo = cs.getInt(2);
        }//try
        catch(Exception e)
        {
            SION.logearExcepcion(Modulo.VENTA, e, sql);
        }//catch
        finally
        {
            Conexion.cerrarResultSet(rs);
            Conexion.cerrarCallableStatement(cs);
        }//finally
        SION.log(Modulo.VENTA,"Tipo de tarjeta: " + esPagaTodo, Level.FINE);
        return esPagaTodo == 1;
    }
    
}//class
