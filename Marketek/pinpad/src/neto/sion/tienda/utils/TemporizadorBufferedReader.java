package neto.sion.tienda.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import neto.sion.tarjeta.chip.utilerias.Temporizador;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;

public class TemporizadorBufferedReader extends Temporizador
{
    private BufferedReader br;

    public TemporizadorBufferedReader(int length)
    {
        super(length);
        SION.log(Modulo.VENTA, "Valor de timeout asignado para cierre de stream de lectura: " + length, Level.FINE);
    }

    public void setStream(BufferedReader _br)
    {
        br = _br;
    }

    @Override
    public void timeout()
    {
        try
        {
            if(br != null)
            {
                SION.log(Modulo.VENTA, "Cerrando stream...", Level.FINE);
                br.close();
            }
        }
        catch (IOException e)
        {
            SION.logearExcepcion(Modulo.VENTA, e, e.getMessage());
            e.printStackTrace();
        }
        SION.log(Modulo.VENTA, "Terminando thread...\n", Level.FINE);
        Thread.currentThread().stop();
    }
}