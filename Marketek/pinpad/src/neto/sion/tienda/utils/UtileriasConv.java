/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.utils;

import java.util.logging.Level;
import neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto;
import neto.sion.clientews.tarjeta.dto.PeticionReversoTarjetaDto;
import neto.sion.clientews.tarjeta.dto.RespuestaPagoTarjetaDto;
import neto.sion.clientews.tarjeta.dto.RespuestaReversoTarjetaDto;
//import neto.sion.clientews.tarjeta.servicefacade.ConsumirTarjetaWSImpl;
import neto.sion.tarjeta.chip.utilerias.UtileriasCadenas;
import neto.sion.tarjeta.servicios.dto.PeticionPagoDto;
import neto.sion.tarjeta.servicios.dto.PeticionReversoDto;
import neto.sion.tarjeta.servicios.dto.RespuestaPagoDto;
import neto.sion.tarjeta.servicios.dto.RespuestaReversoDto;
//import neto.sion.tarjeta.servicios.serviceFacade.ConsumirTarjetaImp;
import neto.sion.tienda.bean.PeticionPagoTarjetaFrontBean;
import neto.sion.tienda.bean.PeticionReversoTarjetaFrontBean;
import neto.sion.tienda.bean.RespuestaPagoTarjetaFrontBean;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;

/**
 *
 * @author Administrador
 */
public class UtileriasConv {
    
    private UtileriasCadenas utilerias = new UtileriasCadenas();
    
    public PeticionPagoDto peticionPagoTarjetaDto2PeticionPagoDto (PeticionPagoTarjetaDto _peticion)
    {
        PeticionPagoDto peticion = new PeticionPagoDto();
        peticion.setPaPaisId(_peticion.getPaPaisId());
        peticion.setTiendaId(_peticion.getTiendaId());
        peticion.setCaja(_peticion.getNumTerminal());
        peticion.setComision(_peticion.getComision());
        peticion.setEsChip(false);
        peticion.setMontoVenta(String.valueOf(_peticion.getMonto()));
        peticion.setTrack1(_peticion.getTrack1());
        peticion.setTrack2(_peticion.getTrack2());
        return peticion;
    }

    public PeticionPagoTarjetaDto peticionPagoDto2PeticionPagoTarjetaDto (PeticionPagoDto _peticion)
    {
        PeticionPagoTarjetaDto peticion = new PeticionPagoTarjetaDto();
        peticion.setPaPaisId(_peticion.getPaPaisId());
        peticion.setTiendaId(_peticion.getTiendaId());
        peticion.setNumTerminal(_peticion.getCaja());
        peticion.setComision(_peticion.getComision());
        peticion.setChip(false);
        peticion.setMonto(Double.valueOf(_peticion.getMontoVenta()));
        peticion.setTrack1(_peticion.getTrack1());
        peticion.setTrack2(_peticion.getTrack2());
        return peticion;
    }

    public RespuestaPagoDto respuestaPagoTarjetaDto2RespuestaPagoDto (RespuestaPagoTarjetaDto _respuesta)
    {
        RespuestaPagoDto respuesta = new RespuestaPagoDto();
        respuesta.setAutorizacion(_respuesta.getNoAutorizacion());
        respuesta.setCodigo(_respuesta.getCodProceso());
        respuesta.setFechaLocal(_respuesta.getFechaLocal());
        respuesta.setHoraLocal(_respuesta.getHoraLocal());
        respuesta.setMensaje(_respuesta.getMensaje());
        respuesta.setTarjetaId(String.valueOf(_respuesta.getPagoTarjetaId()));
        respuesta.setTrace(_respuesta.getTrace());
        return respuesta;
    }
    
    public RespuestaPagoTarjetaDto respuestaPagoDto2RespuestaPagoTarjetaDto (RespuestaPagoDto _respuesta)
    {
        RespuestaPagoTarjetaDto respuesta = new RespuestaPagoTarjetaDto();
        boolean respValida = true;
        UtileriasCadenas utilerias = new UtileriasCadenas();
        try
        {
            if(utilerias.isCadenaVacia(_respuesta.getAutorizacion()))
                respValida = false;
            else
                respuesta.setNoAutorizacion(_respuesta.getAutorizacion());

            if(utilerias.isCadenaVacia(_respuesta.getCodigo()))
                respValida = false;
            else
                respuesta.setCodRespuesta(_respuesta.getCodigo());

            if(utilerias.isCadenaVacia(_respuesta.getFechaLocal()))
                respValida = false;
            else
                respuesta.setFechaLocal(_respuesta.getFechaLocal());

            if(utilerias.isCadenaVacia(_respuesta.getHoraLocal()))
                respValida = false;
            else
                respuesta.setHoraLocal(_respuesta.getHoraLocal());

            if(utilerias.isCadenaVacia(_respuesta.getMensaje()))
                respValida = false;
            else
                respuesta.setMensaje(_respuesta.getMensaje());

            if(utilerias.isCadenaVacia(_respuesta.getTarjetaId()) && !_respuesta.getTarjetaId().matches("\\d"))
                respValida = false;
            else
                respuesta.setPagoTarjetaId(Long.valueOf(_respuesta.getTarjetaId()));

            if(utilerias.isCadenaVacia(_respuesta.getTrace()))
                respValida = false;
            else
                respuesta.setTrace(_respuesta.getTrace());

            if(respValida)
            {
                respuesta.setCodError(0);
                respuesta.setDescError("Respuesta exitosa");
            }
            else
            {
                respuesta.setCodError(-1);
                respuesta.setDescError("Respuesta erronea");
            }
        }
        catch(Exception e)
        {
            SION.logearExcepcion(Modulo.VENTA, e, e.getMessage());
            respuesta.setCodError(-1);
            respuesta.setDescError("ERROR: No se pudo convertir la respuesta.\n" + e.getMessage());
        }
        return respuesta;
    }

    public PeticionReversoDto peticionReversoTarjetaDto2PeticionReversoDto (PeticionReversoTarjetaDto _peticion)
    {
        PeticionReversoDto peticion = new PeticionReversoDto();
        peticion.setAfiliacion(_peticion.getAfiliacion());
        peticion.setComision(_peticion.getComision());
        peticion.setMonto(String.valueOf(_peticion.getMonto()));
        peticion.setTarjetaId(String.valueOf(_peticion.getPagoTarjetaId()));
        peticion.setTerminal(_peticion.getNumTerminal());
        peticion.setTrack2(_peticion.getTrack2());
        peticion.setCanal(_peticion.getCanalPago());
        return peticion;
    }
    
    public PeticionReversoTarjetaDto peticionReversoDto2PeticionReversoTarjetaDto (PeticionReversoDto _peticion)
    {
        PeticionReversoTarjetaDto peticion = new PeticionReversoTarjetaDto();
        peticion.setAfiliacion(_peticion.getAfiliacion());
        peticion.setComision(_peticion.getComision());
        peticion.setMonto(Double.valueOf(_peticion.getMonto()));
        peticion.setPagoTarjetaId(Long.valueOf(_peticion.getTarjetaId()));
        peticion.setNumTerminal(_peticion.getTerminal());
        peticion.setTrack2(_peticion.getTrack2());
        peticion.setCanalPago(_peticion.getCanal());
        return peticion;
    }
    
    public RespuestaReversoDto respuestaReversoTarjetaDto2RespuestaReversoDto (RespuestaReversoTarjetaDto _respuesta)
    {
        RespuestaReversoDto respuesta = new RespuestaReversoDto();
        respuesta.setAutorizacion(_respuesta.getAutorizacion());
        respuesta.setCodigo(_respuesta.getCodProceso());
        respuesta.setFechaLocal(_respuesta.getFechaLocal());
        respuesta.setHoraLocal(_respuesta.getHoraLocal());
        respuesta.setMensaje(_respuesta.getMensaje());
        respuesta.setReferencia(_respuesta.getReferencia());
        respuesta.setTarjetaId(_respuesta.getTarjetaId());
        respuesta.setTrace(_respuesta.getTrace());
        return respuesta;
    }
    
    public RespuestaReversoTarjetaDto respuestaReversoDto2RespuestaReversoTarjetaDto (RespuestaReversoDto _respuesta, PeticionReversoDto _peticion)
    {
        RespuestaReversoTarjetaDto respuesta = new RespuestaReversoTarjetaDto();
        respuesta.setAutorizacion(_respuesta.getAutorizacion());
        respuesta.setCodProceso(_respuesta.getCodigo());
        respuesta.setFechaLocal(_respuesta.getFechaLocal());
        respuesta.setHoraLocal(_respuesta.getHoraLocal());
        respuesta.setMensaje(_respuesta.getMensaje());
        respuesta.setReferencia(_respuesta.getReferencia());
        respuesta.setTarjetaId(_respuesta.getTarjetaId());
        respuesta.setTrace(_respuesta.getTrace());
        respuesta.setAfiliacion(_peticion.getAfiliacion());
        return respuesta;
    }
    
    public PeticionReversoDto generaPeticionReversoDto (RespuestaPagoTarjetaFrontBean respuestaPago)
    {
        PeticionReversoDto peticionDto = new PeticionReversoDto();
        peticionDto.setAfiliacion(respuestaPago.getPeticionReverso().getAfiliacion());
        peticionDto.setCanal(respuestaPago.getPeticionReverso().getCanalPago());
        peticionDto.setComision(respuestaPago.getPeticionReverso().getComision());
        peticionDto.setMonto(String.valueOf(respuestaPago.getPeticionReverso().getMonto()));
        peticionDto.setPaConciliacionId(respuestaPago.getPeticionReverso().getPaConciliacionId());
        peticionDto.setPaPaisId(respuestaPago.getPeticionReverso().getPaPaisId());
        peticionDto.setTarjetaId(String.valueOf(respuestaPago.getPeticionReverso().getPagoTarjetaId()));
        peticionDto.setTerminal(respuestaPago.getPeticionReverso().getTerminal());
        peticionDto.setTiendaId(respuestaPago.getPeticionReverso().getTiendaId());
        peticionDto.setTrack2(respuestaPago.getPeticionReverso().getTrack2());
        return peticionDto;
    }
    
    public RespuestaReversoDto generaRespuestaReversoDto (RespuestaPagoTarjetaFrontBean respuestaPago)
    {
        RespuestaReversoDto respuestaDto = new RespuestaReversoDto();
        respuestaDto.setAutorizacion(respuestaPago.getNumAutorizacion());
        respuestaDto.setHoraLocal(respuestaPago.getPeticionReverso().getHoraLocal());
        respuestaDto.setFechaLocal(respuestaPago.getPeticionReverso().getFechaLocal());
        respuestaDto.setTrace(respuestaPago.getPeticionReverso().getTrace());
        return respuestaDto;
    }
    
    public RespuestaPagoTarjetaFrontBean generaRespuestaFront(String respuestaPinpad)
    {
        RespuestaPagoTarjetaFrontBean respuestaFrontBean = new RespuestaPagoTarjetaFrontBean();
        if(utilerias.obtieneValorBuscaPropiedad("codError", respuestaPinpad).equals("0")
        && utilerias.obtieneValorBuscaPropiedad("Accepted", respuestaPinpad).toLowerCase().equals("true"))
        {
            respuestaFrontBean.setCodError(0);
            respuestaFrontBean.setDescError("Aceptada por pinpad");
        }
        else
        {
            respuestaFrontBean.setCodError(-1);
            respuestaFrontBean.setDescError("Declinada por pinpad");
        }

        SION.log(Modulo.VENTA, "generaRespuestaFront:" + respuestaFrontBean.toString(), Level.INFO);
        return respuestaFrontBean;
    }//generaRespuestaFront

    public PeticionReversoTarjetaFrontBean generarPeticionReverso(PeticionPagoTarjetaDto peticionPago, RespuestaPagoTarjetaDto respuestaPago, PeticionPagoTarjetaFrontBean peticionPagoFront)
    {
        SION.log(Modulo.VENTA, "Generación de reverso:\n"
                + peticionPago.toString() + "\n"
                + respuestaPago.toString() + "\n"
                + peticionPagoFront.toString(), Level.SEVERE);

        PeticionReversoTarjetaFrontBean peticionReverso = new PeticionReversoTarjetaFrontBean();

        if(utilerias.isCadenaVacia(respuestaPago.getAfiliacion()))
            peticionReverso.setAfiliacion(peticionPagoFront.getAfiliacion());
        else
            peticionReverso.setAfiliacion(respuestaPago.getAfiliacion());

        peticionReverso.setChip(peticionPago.isChip());
        peticionReverso.setComision(peticionPago.getComision());
        peticionReverso.setFalloLecturaChip(peticionPago.isFalloLecturaChip());
        peticionReverso.setFechaLocal(respuestaPago.getFechaLocal());
        peticionReverso.setHoraLocal(respuestaPago.getHoraLocal());
        peticionReverso.setTerminal(peticionPago.getNumTerminal());
        peticionReverso.setMonto(peticionPago.getMonto());
        //peticionReverso.setPaConciliacionId(respuestaPago.getConciliacionId());
        peticionReverso.setPaPaisId(peticionPago.getPaPaisId());
        peticionReverso.setPagoTarjetaId(respuestaPago.getPagoTarjetaId());
        peticionReverso.setTiendaId(peticionPago.getTiendaId());
        peticionReverso.setTrace(respuestaPago.getTrace());
        peticionReverso.setTrack2(peticionPago.getTrack2());
        peticionReverso.setCanalPago(peticionPago.getCanalPago());

        SION.log(Modulo.VENTA, "Petición de reverso:\n" + peticionReverso.toString(), Level.SEVERE);

        return peticionReverso;
    }//generarPeticionReverso

    public PeticionReversoTarjetaDto peticionReversoTarjetaBean2PeticionReversoTarjetaDto (PeticionReversoTarjetaFrontBean peticionReversoBean)
    {
        PeticionReversoTarjetaDto peticionReversoDto = new PeticionReversoTarjetaDto();
        peticionReversoDto.setAfiliacion(peticionReversoBean.getAfiliacion());
        peticionReversoDto.setChip(peticionReversoBean.isChip());
        peticionReversoDto.setComision(peticionReversoBean.getComision());
        peticionReversoDto.setFalloLecturaChip(peticionReversoBean.isFalloLecturaChip());
        peticionReversoDto.setFechaLocal(peticionReversoBean.getFechaLocal());
        peticionReversoDto.setHoraLocal(peticionReversoBean.getHoraLocal());
        peticionReversoDto.setMonto(peticionReversoBean.getMonto());
        peticionReversoDto.setPaPaisId(peticionReversoBean.getPaPaisId());
        peticionReversoDto.setPagoTarjetaId(peticionReversoBean.getPagoTarjetaId());
        peticionReversoDto.setNumTerminal(peticionReversoBean.getTerminal());
        peticionReversoDto.setTiendaId(peticionReversoBean.getTiendaId());
        peticionReversoDto.setTrace(peticionReversoBean.getTrace());
        peticionReversoDto.setTrack2(peticionReversoBean.getTrack2());
        peticionReversoDto.setCanalPago(peticionReversoBean.getCanalPago());
        return peticionReversoDto;
    }
}
