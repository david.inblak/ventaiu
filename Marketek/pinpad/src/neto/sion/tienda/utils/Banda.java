package neto.sion.tienda.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;

public class Banda
{
  public static String leerBanda(int tEspera)
    throws Exception
  {
    StringBuffer respuesta = new StringBuffer();

    String strBinFile = SION.obtenerParametro(Modulo.VENTA, "pinpad.bin.banda") + " ";
    String comando = "cmd /c " + strBinFile + tEspera;
    String linea = null;

    SION.log(Modulo.VENTA, "Ejecutando comando " + comando, Level.FINE);

    Process p = Runtime.getRuntime().exec(comando);

    InputStream is = p.getInputStream();
    InputStream errorStream = p.getErrorStream();

    BufferedReader br = new BufferedReader(new InputStreamReader(is));
    BufferedReader bufferErrorReader = new BufferedReader(new InputStreamReader(errorStream));

    linea = bufferErrorReader.readLine();

    if (linea != null) {
      SION.log(Modulo.VENTA, "Ocurrio un error al ejecutar comando", Level.SEVERE);
      respuesta.append(linea);
      while (linea != null) {
        linea = bufferErrorReader.readLine();
        if (linea != null)
          respuesta.append("\n").append(linea);
      }
      throw new Exception(respuesta.toString());
    }

    SION.log(Modulo.VENTA, "OK, regresando resultado", Level.FINE);

    linea = br.readLine();
    respuesta.append(linea);
    while (linea != null) {
      linea = br.readLine();
      if (linea == null)
        continue;
      respuesta.append("\n").append(linea);
    }
    if (respuesta.toString().contains("Error"))
    {
      SION.log(Modulo.VENTA, "Ocurrio en error al leer el resultado: " + linea, Level.SEVERE);
      throw new Exception(linea);
    }
    String[] datos = respuesta.toString().split("\\n");
    for(String tmp : datos)
        System.out.println(tmp.substring( tmp.indexOf(":") < 0 ? 0 : tmp.indexOf(":")+1 ).trim() );
    return respuesta.toString();
  }
}