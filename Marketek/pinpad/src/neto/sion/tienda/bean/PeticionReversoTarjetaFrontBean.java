package neto.sion.tienda.bean;

import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;

public class PeticionReversoTarjetaFrontBean extends PeticionBaseVentaDto
{
    private double monto;
    private String canalPago;
    private String track2;
    private String terminal;
    private String afiliacion;
    private String trace;
    private String horaLocal;
    private String fechaLocal;
    private double comision;
    private boolean chip;
    private boolean falloLecturaChip;
    private long pagoTarjetaId;

    public PeticionReversoTarjetaFrontBean() {
        super();
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double _monto) {
        monto = _monto;
    }
    
    public String getCanalPago() {
        return canalPago;
    }

    public void setCanalPago(String canalPago) {
        this.canalPago = canalPago;
    }

    public String getTrack2() {
        return track2;
    }

    public void setTrack2(String _track2) {
        track2 = _track2;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String _terminal) {
        terminal = _terminal;
    }

    public String getAfiliacion() {
        return afiliacion;
    }

    public void setAfiliacion(String _afiliacion) {
        afiliacion = _afiliacion;
    }

    public String getTrace() {
        return trace;
    }

    public void setTrace(String _trace) {
        trace = _trace;
    }

    public String getHoraLocal() {
        return horaLocal;
    }

    public void setHoraLocal(String _horaLocal) {
        horaLocal = _horaLocal;
    }

    public String getFechaLocal() {
        return fechaLocal;
    }

    public void setFechaLocal(String _fechaLocal) {
        fechaLocal = _fechaLocal;
    }

    public double getComision() {
        return comision;
    }

    public void setComision(double _comision) {
        comision = _comision;
    }

    public boolean isChip() {
        return chip;
    }

    public void setChip(boolean _chip) {
        chip = _chip;
    }

    public boolean isFalloLecturaChip() {
        return falloLecturaChip;
    }

    public void setFalloLecturaChip(boolean _falloLecturaChip) {
        falloLecturaChip = _falloLecturaChip;
    }

    public long getPagoTarjetaId() {
        return pagoTarjetaId;
    }

    public void setPagoTarjetaId(long _pagoTarjetaId) {
        pagoTarjetaId = _pagoTarjetaId;
    }

    @Override
    public String toString() {
            return "PeticionReversoBean [" 
                    + super.toString()
                    + "monto=" + monto
                    + "canalPago=" + canalPago
                    + ", track2=" + track2
                    + ", terminal=" + terminal 
                    + ", afiliacion=" + afiliacion
                    + ", trace=" + trace 
                    + ", horaLocal=" + horaLocal
                    + ", fechaLocal=" + fechaLocal 
                    + ", comision=" + comision
                    + ", chip=" + chip 
                    + ", falloLecturaChip=" + falloLecturaChip
                    + "]";
    }
}
