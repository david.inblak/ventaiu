
package neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto;

import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.PeticionTransaccionCentralBean;

public class PeticionTransaccionCentralDto extends PeticionBaseVentaDto {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long paUsuarioId;
    private int paSistemaId;
    private int paModuloId;
    private int paSubModuloId;

    public long getPaUsuarioId() {
            return paUsuarioId;
    }

    public void setPaUsuarioId(long paUsuarioId) {
            this.paUsuarioId = paUsuarioId;
    }

    public int getPaSistemaId() {
            return paSistemaId;
    }

    public void setPaSistemaId(int paSistemaId) {
            this.paSistemaId = paSistemaId;
    }

    public int getPaModuloId() {
            return paModuloId;
    }

    public void setPaModuloId(int paModuloId) {
            this.paModuloId = paModuloId;
    }

    public int getPaSubModuloId() {
            return paSubModuloId;
    }

    public void setPaSubModuloId(int paSubModuloId) {
            this.paSubModuloId = paSubModuloId;
    }
    
    public PeticionTransaccionCentralBean toClienteSolicitud(){
        PeticionTransaccionCentralBean solicitud = new PeticionTransaccionCentralBean();
        
        solicitud.setUId		(this.getUId			());
	solicitud.setIpTerminal		(this.getIpTerminal		());
	solicitud.setTiendaId		(this.getTiendaId		());
	solicitud.setPaPaisId		(this.getPaPaisId		());
        solicitud.setPaConciliacionId	(this.getPaConciliacionId	());
	solicitud.setPaUsuarioId	(this.getPaUsuarioId		());
        solicitud.setPaSistemaId	(this.getPaSistemaId		());
        solicitud.setPaModuloId		(this.getPaModuloId		());
        solicitud.setPaSubModuloId	(this.getPaSubModuloId		());
        
        return solicitud;
    }
    

    @Override
    public String toString() {
            return "PeticionTransaccionCentralBean [paUsuarioId=" + paUsuarioId
                            + ", paSistemaId=" + paSistemaId + ", paModuloId=" + paModuloId
                            + ", paSubModuloId=" + paSubModuloId + super.toString() + "]";
    }

}
