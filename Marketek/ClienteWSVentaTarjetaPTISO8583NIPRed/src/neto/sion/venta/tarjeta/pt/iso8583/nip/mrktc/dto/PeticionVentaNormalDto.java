package neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto;

import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.ArticuloBean;
import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.PeticionVentaNormalBean;
import neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.TipoPagoBean;

public class PeticionVentaNormalDto {
    private long uId;
    private String ipTerminal;
    private long tiendaId;
    private int paPaisId;
    private long paConciliacionId;
    private String paTerminal;
    private long paUsuarioId;
    private long paUsuarioAutorizaId;
    private String paFechaOper;
    private int paTipoMovto;
    private double paMontoTotalVta;
    private ArticuloDto[] articulos;
    private TipoPagoDto[] tiposPago;

    private int tipoDevolucion;
    private long transaccionDev;

    private int fueraLinea;
    
    
    public String getIpTerminal() {
        return ipTerminal;
    }

    public void setIpTerminal(String ipTerminal) {
        this.ipTerminal = ipTerminal;
    }

    public long getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(long tiendaId) {
        this.tiendaId = tiendaId;
    }

    public long getuId() {
        return uId;
    }

    public void setuId(long uId) {
        this.uId = uId;
    }
    public long getPaConciliacionId() {
        return paConciliacionId;
    }

    public void setPaConciliacionId(long paConciliacionId) {
        this.paConciliacionId = paConciliacionId;
    }

    public int getPaPaisId() {
        return paPaisId;
    }

    public void setPaPaisId(int paPaisId) {
        this.paPaisId = paPaisId;
    }
    

    public String getPaTerminal() {
            return paTerminal;
    }

    public void setPaTerminal(String paTerminal) {
            this.paTerminal = paTerminal;
    }

    public long getPaUsuarioId() {
            return paUsuarioId;
    }

    public void setPaUsuarioId(long paUsuarioId) {
            this.paUsuarioId = paUsuarioId;
    }

    public String getPaFechaOper() {
            return paFechaOper;
    }

    public void setPaFechaOper(String paFechaOper) {
            this.paFechaOper = paFechaOper;
    }

    public int getPaTipoMovto() {
            return paTipoMovto;
    }

    public void setPaTipoMovto(int paTipoMovto) {
            this.paTipoMovto = paTipoMovto;
    }

    public double getPaMontoTotalVta() {
            return paMontoTotalVta;
    }

    public void setPaMontoTotalVta(double paMontoTotalVta) {
            this.paMontoTotalVta = paMontoTotalVta;
    }

    public ArticuloDto[] getArticulos() {
            return articulos;
    }

    public void setArticulos(ArticuloDto[] articulos) {
            this.articulos = articulos;
    }

    public TipoPagoDto[] getTiposPago() {
            return tiposPago;
    }

    public void setTiposPago(TipoPagoDto[] tiposPago) {
            this.tiposPago = tiposPago;
    }

    public int getFueraLinea() {
            return fueraLinea;
    }

    public void setFueraLinea(int fueraLinea) {
            this.fueraLinea = fueraLinea;
    }

    public long getPaUsuarioAutorizaId() {
            return paUsuarioAutorizaId;
    }

    public void setPaUsuarioAutorizaId(long paUsuarioAutorizaId) {
            this.paUsuarioAutorizaId = paUsuarioAutorizaId;
    }

    public int getTipoDevolucion() {
            return tipoDevolucion;
    }

    public void setTipoDevolucion(int tipoDevolucion) {
            this.tipoDevolucion = tipoDevolucion;
    }

    public long getTransaccionDev() {
            return transaccionDev;
    }

    public void setTransaccionDev(long transaccionDev) {
            this.transaccionDev = transaccionDev;
    }

    public PeticionVentaNormalBean toClienteSolicitud() {
        PeticionVentaNormalBean solicitud = new PeticionVentaNormalBean();
        
        ArticuloBean[] art = new ArticuloBean[articulos.length];
        TipoPagoBean[] tip = new TipoPagoBean[tiposPago.length];
        for (int i = 0; i < articulos.length; i++) {
            art[i] = (this.getArticulos()[i]).toClienteSolicitud();
        }
        for (int j = 0; j < tiposPago.length; j++) {
            tip[j] = (this.getTiposPago()[j]).toClienteSolicitud();
        }
                
        solicitud.setUId                        (this.getuId());
        solicitud.setTiendaId                   (this.getTiendaId());
        solicitud.setPaPaisId                   (this.getPaPaisId());
        solicitud.setPaConciliacionId           (this.getPaConciliacionId());
        solicitud.setIpTerminal                 (this.getIpTerminal());
         
        solicitud.setPaTerminal			(this.getPaTerminal		());
        solicitud.setPaUsuarioId		(this.getPaUsuarioId		());
        solicitud.setPaUsuarioAutorizaId	(this.getPaUsuarioAutorizaId	());
        solicitud.setPaFechaOper		(this.getPaFechaOper		());
        solicitud.setPaTipoMovto		(this.getPaTipoMovto		());
        solicitud.setPaMontoTotalVta		(this.getPaMontoTotalVta	());
        
        solicitud.setArticulos(art);
        solicitud.setTiposPago(tip);
    
        return solicitud;
    }

    @Override
    public String toString() {
        return "PeticionVentaNormalDto{" + "uId=" + uId + ", ipTerminal=" + ipTerminal + ", tiendaId=" + tiendaId + ", paPaisId=" + paPaisId + ", paConciliacionId=" + paConciliacionId + ", paTerminal=" + paTerminal + ", paUsuarioId=" + paUsuarioId + ", paUsuarioAutorizaId=" + paUsuarioAutorizaId + ", paFechaOper=" + paFechaOper + ", paTipoMovto=" + paTipoMovto + ", paMontoTotalVta=" + paMontoTotalVta + ", articulos=" + articulos + ", tiposPago=" + tiposPago + ", tipoDevolucion=" + tipoDevolucion + ", transaccionDev=" + transaccionDev + ", fueraLinea=" + fueraLinea + '}';
    }

    
    
}
