/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author dramirezr
 */
public class LineasDto {
    private int orden;
 private String texto;
 private int size;
 private String alineacion;
 private String inverso;
 private String estilo;

    public String getAlineacion() {
        return alineacion;
    }

    public void setAlineacion(String alineacion) {
        this.alineacion = alineacion;
    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public String getInverso() {
        return inverso;
    }

    public void setInverso(String inverso) {
        this.inverso = inverso;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    @Override
    public String toString() {
        return "LineasDto{" + "orden=" + orden + ", texto=" + texto + ", size=" + size + ", alineacion=" + alineacion + ", inverso=" + inverso + ", estilo=" + estilo + '}';
    }
 
 
}
