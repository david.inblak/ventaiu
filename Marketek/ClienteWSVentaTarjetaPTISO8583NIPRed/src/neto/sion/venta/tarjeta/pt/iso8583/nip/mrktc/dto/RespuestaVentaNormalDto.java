

package neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp.CuponesBean;
import neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp.LineasBean;


public class RespuestaVentaNormalDto {
        private long paTransaccionId;
	private int paCdgError;
	private String paDescError;
	private long paTaNumOperacion;
	private BloqueoDto[] paTypCursorBlqs;        
	private CuponesDto[] cupones;
	private String xmlResponse;

	public BloqueoDto[] getPaTypCursorBlqs() {
		return paTypCursorBlqs;
	}

	public void setPaTypCursorBlqs(BloqueoDto[] paTypCursorBlqs) {
		this.paTypCursorBlqs = paTypCursorBlqs;
	}

	public long getPaTransaccionId() {
		return paTransaccionId;
	}

	public void setPaTransaccionId(long paTransaccionId) {
		this.paTransaccionId = paTransaccionId;
	}

	public int getPaCdgError() {
		return paCdgError;
	}

	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}

	public String getPaDescError() {
		return paDescError;
	}

	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}

	public long getPaTaNumOperacion() {
		return paTaNumOperacion;
	}

	public void setPaTaNumOperacion(long paTaNumOperacion) {
		this.paTaNumOperacion = paTaNumOperacion;
	}

    public CuponesDto[] getCupones() {
        return cupones;
    }

    public void setCupones(CuponesDto[] cupones) {
        this.cupones = cupones;
    }

    public String getXmlResponse() {
        return xmlResponse;
    }

    public void setXmlResponse(String xmlResponse) {
        this.xmlResponse = xmlResponse;
    }

    @Override
    public String toString() {
        return "RespuestaVentaNormalDto{" + "paTransaccionId=" + paTransaccionId + ", paCdgError=" + paCdgError + ", paDescError=" + paDescError + ", paTaNumOperacion=" + paTaNumOperacion + ", paTypCursorBlqs=" + paTypCursorBlqs + ", cupones=" + cupones + ", xmlResponse=" + xmlResponse + '}';
    }

    
        
        



    public void crearRespuesta(neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp.RespuestaVentaBean  respuestaVentaNormal) {
        this.setPaCdgError(respuestaVentaNormal.getPaCdgError());
        this.setPaDescError(respuestaVentaNormal.getPaDescError());
        this.setPaTaNumOperacion(respuestaVentaNormal.getPaTaNumOperacion());
        this.setPaTransaccionId(respuestaVentaNormal.getPaTransaccionId());
        
        if( this.getPaTypCursorBlqs() != null ){
            BloqueoDto[] bloqueos = new BloqueoDto[respuestaVentaNormal.getPaTypCursorBlqs().length];
            for (int i = 0; i < respuestaVentaNormal.getPaTypCursorBlqs().length; i++) {
                BloqueoDto b= new BloqueoDto();
                b.setFiAvisosFalt( respuestaVentaNormal.getPaTypCursorBlqs()[i].getFiAvisosFalt() );
                b.setFiAvisosFalt( respuestaVentaNormal.getPaTypCursorBlqs()[i].getFiEstatusBloqueoId() );
                b.setFiAvisosFalt( respuestaVentaNormal.getPaTypCursorBlqs()[i].getFiNumAvisos() );
                b.setFiAvisosFalt( respuestaVentaNormal.getPaTypCursorBlqs()[i].getFiTipoPagoId() );
                bloqueos[i] = b;
            }

            this.setPaTypCursorBlqs(bloqueos);
        }else{
           this.setPaTypCursorBlqs(new BloqueoDto[0]); 
        }
        
        if( respuestaVentaNormal != null ){
            List<CuponesDto> cupons = new ArrayList<CuponesDto>();
            if( respuestaVentaNormal.getCupones() != null ){
                for (CuponesBean cupBean : respuestaVentaNormal.getCupones()) {
                    CuponesDto c = new CuponesDto();
                    c.setCodigo(cupBean.getCodigo());
                    c.setCodigoOferta(cupBean.getCodigoOferta());
                    c.setInformacionAdicional(cupBean.getInformacionAdicional());
                    c.setLogoCadena(cupBean.getLogoCadena());                
                    c.setLogoProducto(cupBean.getLogoProducto());                
                    c.setVecesAImprimir(cupBean.getVecesAImprimir());                

                    List<LineasDto> lineasDto = new ArrayList<LineasDto>();
                    for ( LineasBean lineaBean : cupBean.getLineas()) {
                        LineasDto l = new LineasDto();
                        l.setAlineacion(lineaBean.getAlineacion());
                        l.setEstilo(lineaBean.getEstilo());
                        l.setInverso(lineaBean.getInverso());
                        l.setOrden(lineaBean.getOrden());
                        l.setSize(lineaBean.getSize());
                        l.setTexto(lineaBean.getTexto());
                        lineasDto.add(l);
                    }

                    c.setLineas(lineasDto.toArray(new LineasDto[0]));

                    cupons.add(c);
                }
            }
            this.setCupones(cupons.toArray(new CuponesDto[0]));
        }
    }
    
    public neto.sion.venta.servicios.cliente.dto.RespuestaVentaDto crearRespuestaVentaNormalDtoActual(){
        neto.sion.venta.servicios.cliente.dto.RespuestaVentaDto res = 
                new neto.sion.venta.servicios.cliente.dto.RespuestaVentaDto();
        
        res.setPaCdgError(this.getPaCdgError());
        res.setPaDescError(this.getPaDescError());
        res.setPaTaNumOperacion(this.getPaTaNumOperacion());
        res.setPaTransaccionId(this.getPaTransaccionId());
        
        if( this.getPaTypCursorBlqs() != null ){
            neto.sion.venta.servicios.cliente.dto.BloqueoDto[] bloqueos = 
                    new neto.sion.venta.servicios.cliente.dto.BloqueoDto[this.getPaTypCursorBlqs().length];

            for (int i = 0; i < this.getPaTypCursorBlqs().length; i++) {
                neto.sion.venta.servicios.cliente.dto.BloqueoDto b= new neto.sion.venta.servicios.cliente.dto.BloqueoDto();
                b.setFiAvisosFalt       (this.getPaTypCursorBlqs()[i].getFiAvisosFalt());
                b.setFiEstatusBloqueoId (this.getPaTypCursorBlqs()[i].getFiEstatusBloqueoId());
                b.setFiNumAvisos        (this.getPaTypCursorBlqs()[i].getFiNumAvisos());
                b.setFiTipoPagoId       (this.getPaTypCursorBlqs()[i].getFiTipoPagoId());
                bloqueos[i] = b;
            }

            res.setPaTypCursorBlqs(bloqueos);
        }else{
            res.setPaTypCursorBlqs(new neto.sion.venta.servicios.cliente.dto.BloqueoDto[0]);
        }
        
        if( this.getCupones() != null ){
            List<neto.sion.venta.servicios.cliente.dto.CuponesBean> cupons = new ArrayList<neto.sion.venta.servicios.cliente.dto.CuponesBean>();
            for (CuponesDto cupBean : this.getCupones()) {
                neto.sion.venta.servicios.cliente.dto.CuponesBean c = new neto.sion.venta.servicios.cliente.dto.CuponesBean();
                c.setCodigo(cupBean.getCodigo());
                c.setCodigoOferta(cupBean.getCodigoOferta());
                c.setInformacionAdicional(cupBean.getInformacionAdicional());
                c.setLogoCadena(cupBean.getLogoCadena());                
                c.setLogoProducto(cupBean.getLogoProducto());                
                c.setVecesAImprimir(cupBean.getVecesAImprimir());                
                
                List<neto.sion.venta.servicios.cliente.dto.LineasBean> lineasDto = new ArrayList<neto.sion.venta.servicios.cliente.dto.LineasBean>();
                for ( LineasDto lineaBean : cupBean.getLineas()) {
                    neto.sion.venta.servicios.cliente.dto.LineasBean l = new neto.sion.venta.servicios.cliente.dto.LineasBean();
                    l.setAlineacion(lineaBean.getAlineacion());
                    l.setEstilo(lineaBean.getEstilo());
                    l.setInverso(lineaBean.getInverso());
                    l.setOrden(lineaBean.getOrden());
                    l.setSize(lineaBean.getSize());
                    l.setTexto(lineaBean.getTexto());
                    lineasDto.add(l);
                }

                c.setLineas(lineasDto);
                
                cupons.add(c);
            }
            
            res.setCupones(cupons);
        }
        
        return res;
    }
}
