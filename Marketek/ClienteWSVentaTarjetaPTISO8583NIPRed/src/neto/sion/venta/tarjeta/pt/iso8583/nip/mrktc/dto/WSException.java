package neto.sion.venta.tarjeta.pt.iso8583.nip.mrktc.dto;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public class WSException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String _detalles;
    public WSException(String motivo, String detalles){
            super(motivo);
            _detalles = detalles;
    }
    public WSException(String motivo, Throwable exc){
            super(motivo);
            _detalles = getStackTrace(exc);
    }
    public String getDetallesError(){
            return _detalles;
    }

    public static String getStackTrace(Throwable aThrowable) {
            Writer result = new StringWriter();
            PrintWriter printWriter = new PrintWriter(result);
            aThrowable.printStackTrace(printWriter);
            return result.toString();
    }
}
