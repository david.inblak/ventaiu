/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.redondeo;

import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesPresentador;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesRepositorio;
import neto.sion.tienda.venta.promociones.mvp.IPromocionesVista;
import neto.sion.tienda.venta.promociones.redondeo.modelo.ModeloArticulo;
import neto.sion.tienda.venta.promociones.redondeo.util.Util;
import neto.sion.tienda.venta.promociones.redondeo.vista.UIArticuloPromocion;

/**
 *
 * @author dramirezr
 */
public class VistaPromociones implements IPromocionesVista {
    
    private IPromocionesRepositorio repositorio;
    private IPromocionesPresentador presentador;
        
    private Text lblTitulo_NombreProducto = new Text();
    private Text lblTitulo_PrecioDescuento = new Text();
    private Text lblTitulo_PrecioRegular = new Text();
        
    private VBox vBoxPrincipal;
    private AnchorPane anchorPane = new AnchorPane();
    private HBox hboxScroll = new HBox(20);
    private Pane paneLoading = new Pane();
    private Pane paneError = new Pane();
    
    private Text txtMensajeError;
        
    private TextField textInputTotal= new TextField("");
    private TextField textInputCambio= new TextField(""); 
    private TextField textInputRecibido= new TextField(""); 
    private HBox cntGrey = new HBox();
    
    
    Label keyCodeF11 = new Label("F11 ");
    Label keyCodeF12 = new Label("F12 ");
    Label keyCodeF10 = new Label("F10 ");
    
    Button btnFinalizaVenta = new Button("Continuar Venta");    
    Button btnCancelar = new Button("No acepta promoción");
    
    //private final KeyCombination f8 = new KeyCodeCombination(KeyCode.F8);
    public static final KeyCombination f9 = new KeyCodeCombination(KeyCode.F9);
    public static final KeyCombination f10 = new KeyCodeCombination(KeyCode.F10);
    public static final KeyCombination f11 = new KeyCodeCombination(KeyCode.F11);
    public static final KeyCombination f12 = new KeyCodeCombination(KeyCode.F12);
    
    Locale local = new Locale("es", "MX");
            
    ListChangeListener listener = new ListChangeListener<Node>(){
        @Override
        public void onChanged(ListChangeListener.Change<? extends Node> nodo) {
            while(nodo.next()){
                if( nodo.wasAdded()){
                    UIArticuloPromocion nodoUIA = (UIArticuloPromocion)nodo.getAddedSubList().get(0);
                    nodoUIA.asignarIndice( hboxScroll.getChildren().size() );
                    vBoxPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, nodoUIA.getFiltroEventosF() );
                    vBoxPrincipal.addEventFilter(KeyEvent.KEY_RELEASED, nodoUIA.getFiltroEventosAltRelease());
                }
            }
            
            if( nodo.wasRemoved() ){
                UIArticuloPromocion nodoUIA = (UIArticuloPromocion)nodo.getRemoved().get(0);
                vBoxPrincipal.removeEventFilter(KeyEvent.KEY_PRESSED, nodoUIA.getFiltroEventosF() );
                vBoxPrincipal.removeEventFilter(KeyEvent.KEY_RELEASED, nodoUIA.getFiltroEventosAltRelease());
            }
        }

    };
    
    //Formato decimal + no ceros al inicio
    ChangeListener<String> listener_textImputRecibido_formato_decimal = new ChangeListener<String>(){
        @Override
        public void changed(ObservableValue<? extends String> arg0, String viejoValor, String nuevoVal) {
            if( nuevoVal.matches("^[0]{2,}$") ){
                textInputRecibido.setText("0");
            }else if( nuevoVal.matches("^[0]{1,}[0-9]$") ){
                textInputRecibido.setText(nuevoVal.replace("0", ""));
            }else if( !nuevoVal.matches("\\d{0,5}([\\.]\\d{0,2})?") ){
                textInputRecibido.setText(viejoValor);
            }
            calcularCambio(nuevoVal);
        }
    };
    
    EventHandler<KeyEvent> eventHandler_textInputRecibido_mas_igual_total = new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent arg0) {                    
                    if(arg0.getCharacter().equals("+")){
                        arg0.consume();
                        textInputRecibido.textProperty().set( textInputTotal.textProperty().get().replace("$", "").trim() );
                    }else if( !arg0.getCharacter().matches("[0-9]|\\.")){
                        arg0.consume();
                    }
                }                
            };
    
    @Override
    public void finalize() {
        try {
            super.finalize();
            hboxScroll.getChildren().removeListener(listener);
            textInputRecibido.textProperty().removeListener( listener_textImputRecibido_formato_decimal );
            textInputRecibido.removeEventFilter(KeyEvent.KEY_TYPED, eventHandler_textInputRecibido_mas_igual_total);
        } catch (Throwable ex) {
            Logger.getLogger(VistaPromociones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public VistaPromociones  (IPromocionesRepositorio _repositorio) {
        Label kc11 = new Label();
        kc11.textProperty().bind(keyCodeF10.textProperty());
        kc11.setStyle("-fx-text-fill: #e5e5e5;");
        Label kc12 = new Label();
        kc12.textProperty().bind(keyCodeF10.textProperty());
        kc12.setStyle("-fx-text-fill: #e5e5e5;");
        
        try{                       
            keyCodeF11.setStyle("-fx-font: 13pt System;");
            String txt = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.btnAceptaPromo");
            btnFinalizaVenta = new Button( (txt==null||"".contains(txt))? "Continuar Venta":txt.trim(), keyCodeF11);
        }catch(Exception e){
            btnFinalizaVenta = new Button("Continuar Venta", keyCodeF11);
        }
                
        try{           
            keyCodeF12.setStyle("-fx-font: 13pt System;");
            String txt = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.btnCancelaPromo");
            btnCancelar = new Button((txt==null||"".contains(txt))? "No acepta promoción":txt.trim(), keyCodeF12);
        }catch(Exception e){
            btnCancelar = new Button("No acepta promoción", keyCodeF12);
        }
        
        String verShortCut = "";
        try {  verShortCut = SION.obtenerParametro(Modulo.VENTA, "VENTA.REDONDEO.VERFS"); } catch (Exception ex) {}
        
            if( "0".equals( verShortCut) ){
                keyCodeF10.setText("");
                keyCodeF10.setVisible(false);
                keyCodeF11.setText("");
                keyCodeF11.setVisible(false);
                keyCodeF12.setText("");
                keyCodeF12.setVisible(false);
            }
        
                
        this.repositorio = _repositorio;
        
        textInputRecibido.setFocusTraversable(true);
        btnFinalizaVenta.setFocusTraversable(true);
        btnCancelar.setFocusTraversable(true);
        
        ///-------- Loading
        
        paneLoading.setPrefWidth(950);
        paneLoading.setPrefHeight(600);        
        paneLoading.setStyle("-fx-text-fill: white; -fx-font: 35pt System;");
        AnchorPane.setLeftAnchor(paneLoading, 0.0);
        AnchorPane.setRightAnchor(paneLoading, 0.0);
        AnchorPane.setBottomAnchor(paneLoading, 0.0);
        AnchorPane.setTopAnchor(paneLoading, 0.0);
        
        ImageView imgLoading = new ImageView(  );
        paneLoading.getChildren().add(imgLoading);
        paneLoading.setVisible(false);
        
        ///-------- Error
        
        paneError.setPrefWidth(750);
        paneError.setPrefHeight(600);
        paneError.setStyle("-fx-text-fill: white; -fx-font: 35pt System;");
        AnchorPane.setLeftAnchor(paneError, 0.0);
        AnchorPane.setRightAnchor(paneError, 0.0);
        AnchorPane.setBottomAnchor(paneError, 0.0);
        AnchorPane.setTopAnchor(paneError, 0.0);
        paneError.setVisible(false);
        
        //--- Contenedor Mensaje
        VBox vb2 = new VBox(20);
        vb2.setAlignment(Pos.CENTER);
        vb2.setFillWidth(true);
        vb2.setPrefHeight(120);
        vb2.setPrefWidth(500);
        vb2.setMinHeight(70);
        vb2.setStyle("-fx-background-color:#f93535;");
        
        txtMensajeError = new Text("");
        txtMensajeError.setFill(Color.WHITE);
        txtMensajeError.setTextAlignment(TextAlignment.CENTER);
        txtMensajeError.setWrappingWidth(450);
        txtMensajeError.setStyle("-fx-font: 17pt System;");
        VBox.setVgrow(txtMensajeError, Priority.ALWAYS);
        
        vb2.getChildren().add(txtMensajeError);
        
        Button btn1 = new Button("Aceptar");
        btn1.setStyle("-fx-background-color: #9c1421;-fx-border-color: #ffffff;-fx-text-color: #FFFFFF;-fx-font: 15pt System;");
                
        vb2.getChildren().add(btn1);
        
        ///------- Contenedor principal
        vBoxPrincipal = new VBox(10);
        vBoxPrincipal.alignmentProperty().setValue(Pos.TOP_CENTER);
        vBoxPrincipal.setPrefWidth(900);
        vBoxPrincipal.setPrefHeight(750);
        vBoxPrincipal.setStyle("-fx-background-color: #ffffff;-fx-border-color: #cccccc;-fx-background-radius: 10 10 10 10;");
        vBoxPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent evento) {                    
                    if( evento.getCode().equals(KeyCode.ESCAPE)){
                        //rechazarPromociones();
                        cerrarPromociones();
                        evento.consume();
                    }
                } 
            });
        
        vBoxPrincipal.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent ke) {
                if (f12.match(ke)) {
                    btnCancelar.fire();
                }else if (f11.match(ke)) {
                    btnFinalizaVenta.fire();
                }else if (f10.match(ke)) {
                    textInputRecibido.requestFocus();
                }else if (f9.match(ke)) {
                    
                }

            }
        });
        
        
        ///------- Titulo
        VBox hbTitulo= new VBox();
        hbTitulo.alignmentProperty().setValue(Pos.CENTER);
        hbTitulo.setPadding(new Insets(10));
        //hbTitulo.setPrefWidth(200);
        VBox.setVgrow(hbTitulo, Priority.ALWAYS);
        HBox.setHgrow(hbTitulo, Priority.ALWAYS);
        //hbTitulo.setPrefHeight(900);
        //hbTitulo.setMaxHeight(900);
        
        //String msj ="";
               
        
        
        Text lblTitulo1 = new Text();
        
        HBox.setHgrow(lblTitulo1, Priority.ALWAYS);        
        HBox.setHgrow(lblTitulo_NombreProducto, Priority.ALWAYS);
        
        Text lblTitulo3 = new Text();
        
        HBox.setHgrow(lblTitulo3, Priority.ALWAYS);       
        HBox.setHgrow(lblTitulo_PrecioDescuento, Priority.ALWAYS);
        
        Text lblTitulo5 = new Text();
        HBox.setHgrow(lblTitulo5, Priority.ALWAYS);       
        HBox.setHgrow(lblTitulo_PrecioRegular, Priority.ALWAYS);
        
         try{
            lblTitulo1.setText(SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.1"));
            lblTitulo3.setText(SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.3"));
            lblTitulo5.setText(SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.5"));
            int fontSize = Integer.parseInt(SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.fontSize"));
            
            lblTitulo1.setFont(Font.font(null, FontWeight.NORMAL, fontSize));
            lblTitulo_NombreProducto.setFont(Font.font(null, FontWeight.BOLD, fontSize));
            lblTitulo3.setFont(Font.font(null, FontWeight.NORMAL, fontSize));
            lblTitulo_PrecioDescuento.setFont(Font.font(null, FontWeight.BOLD, fontSize));
            lblTitulo5.setFont(Font.font(null, FontWeight.NORMAL, fontSize));
            lblTitulo_PrecioRegular.setFont(Font.font(null, FontWeight.NORMAL, fontSize));
                       
            hbTitulo.setStyle(SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.stylebackground"));
            
            String colorBase = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.styleTextColorBase");
            String colorResalta = SION.obtenerMensaje(Modulo.VENTA, "venta.redondeo.tituloventana.styleTextColorResaltado");
                        
            lblTitulo1.setFill(Color.web(colorBase));
            
            lblTitulo_NombreProducto.setFill(Color.web(colorBase));
            
            lblTitulo3.setFill(Color.web(colorBase));
            
            lblTitulo_PrecioDescuento.setFill(Color.web(colorResalta));
            
            lblTitulo5.setFill(Color.web(colorBase));
           
            lblTitulo_PrecioRegular.setFill(Color.web(colorBase));
            lblTitulo_PrecioRegular.setStyle("-fx-strikethrough: true;");
          
        }catch(Exception e){
            e.printStackTrace();
            lblTitulo1.setText("Por su compra puede llevarse el");
            lblTitulo3.setText("por");
            lblTitulo5.setText("en lugar de ");
                       
            hbTitulo.setStyle("-fx-background-color: white;");
        }finally{
             
         }
        
        VBox.setVgrow(hbTitulo, Priority.ALWAYS); 
        HBox ultLinea = new HBox(10);
        ultLinea.setAlignment(Pos.CENTER);
        
        hbTitulo.getChildren().addAll(
                lblTitulo1,lblTitulo_NombreProducto,
                ultLinea);
        ultLinea.getChildren().addAll(lblTitulo3,lblTitulo_PrecioDescuento,lblTitulo5,lblTitulo_PrecioRegular);
        
        vBoxPrincipal.getChildren().add(hbTitulo);
        
        //-------- Scroll articulos
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPrefHeight(350);
        scrollPane.setPrefWidth(650);
        scrollPane.setMinHeight(350);
        scrollPane.setMaxHeight(350);
        VBox.setMargin(scrollPane, new Insets(0, 10, 0, 10));
        
        
        anchorPane.setPrefHeight(330);
        anchorPane.setPrefWidth(750);
        
        
        hboxScroll.setPrefHeight(330);
        hboxScroll.setPrefWidth(850);
        hboxScroll.setAlignment(Pos.CENTER);
        
        vBoxPrincipal.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<javafx.scene.input.KeyEvent>() {
            @Override
            public void handle(javafx.scene.input.KeyEvent ke) {
                if( ke.getCode().equals(KeyCode.ALT) ){
                    if( keyCodeF10.isVisible() ){
                        keyCodeF10.setText("");
                        keyCodeF11.setText("");
                        keyCodeF12.setText("");
                        keyCodeF10.setVisible(false);
                        keyCodeF11.setVisible(false);
                        keyCodeF12.setVisible(false);
                    }else{
                        keyCodeF10.setText("F10 ");
                        keyCodeF11.setText("F11 ");
                        keyCodeF12.setText("F12 ");
                        keyCodeF10.setVisible(true);
                        keyCodeF11.setVisible(true);
                        keyCodeF12.setVisible(true);
                    }
                }
            }
        });
        
        anchorPane.getChildren().add(hboxScroll);
        
         
        hboxScroll.getChildren().addListener(listener);
        
        scrollPane.setContent(anchorPane);
        
        vBoxPrincipal.getChildren().add(scrollPane);

        ///------- Importes
        HBox contImportes = new HBox(15);
        contImportes.setAlignment(Pos.TOP_CENTER);
        contImportes.setPrefHeight(200);
        contImportes.setMaxHeight(200);
        VBox.setVgrow(contImportes, Priority.SOMETIMES);    
                
        VBox titImportes = new VBox(10);
        titImportes.setAlignment(Pos.TOP_RIGHT);
        titImportes.setPrefHeight(200);
        titImportes.setPrefWidth(100);
        
        HBox.setHgrow(titImportes, Priority.ALWAYS);
            // Total
            HBox hbxTotal = new HBox();
            hbxTotal.setPrefHeight(200);
            hbxTotal.setPrefWidth(100);
            hbxTotal.setAlignment(Pos.TOP_RIGHT);

            Label lblTotal = new Label("Total:");
            lblTotal.setStyle("-fx-font: 30pt System ;");

            hbxTotal.getChildren().add(lblTotal);

            titImportes.getChildren().add(hbxTotal);
            
            // Recibido
            HBox hbxRecibido = new HBox();
            hbxRecibido.setPrefHeight(200);
            hbxRecibido.setPrefWidth(100);
            hbxRecibido.setAlignment(Pos.TOP_RIGHT);

            Label lblRecibido = new Label("Importe recibido:");
            lblRecibido.setStyle("-fx-font: 25pt System ;");

            hbxRecibido.getChildren().add(lblRecibido);

            titImportes.getChildren().add(hbxRecibido);
            
            
            // Cambio
            HBox hbxCambio = new HBox();
            hbxCambio.setPrefHeight(200);
            hbxCambio.setPrefWidth(100);
            hbxCambio.setAlignment(Pos.TOP_RIGHT);

            Label lblCambio = new Label("Cambio:");
            lblCambio.setStyle("-fx-font: 25pt System ;");

            hbxCambio.getChildren().add(lblCambio);

            titImportes.getChildren().add(hbxCambio);
        
        contImportes.getChildren().add(titImportes);
        
        
        
        VBox inputImportes = new VBox(10);
        inputImportes.setAlignment(Pos.TOP_LEFT);
        inputImportes.setPrefHeight(200);
        inputImportes.setPrefWidth(100);
        
        HBox.setHgrow(inputImportes, Priority.ALWAYS);
            // Total
            HBox hbxInputTotal = new HBox();
            hbxInputTotal.setPrefHeight(200);
            hbxInputTotal.setPrefWidth(100);
            HBox.setHgrow(hbxInputTotal, Priority.SOMETIMES);

            textInputTotal.setFocusTraversable(false);
            textInputTotal.setPromptText("0.00");
            textInputTotal.setEditable(false);            
            textInputTotal.setPrefHeight(35);
            textInputTotal.setPrefWidth(100);
            textInputTotal.setStyle("-fx-background-color: #e5e5e5;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
            Label pesos = new Label(" $ ");
            pesos.setStyle("-fx-font: 25pt System;");
            
            HBox cntGrey1 = new HBox();
            cntGrey1.setStyle("-fx-background-color: #e5e5e5;-fx-background-radius: 5 5 5 5;");
            cntGrey1.getChildren().addAll(pesos,textInputTotal, kc11);

            hbxInputTotal.getChildren().addAll(cntGrey1);

            inputImportes.getChildren().add(hbxInputTotal);
            
            // Recibido           
            HBox hbxInputRecibido = new HBox();
            hbxInputRecibido.setPrefHeight(200);
            hbxInputRecibido.setPrefWidth(100);
            HBox.setHgrow(hbxInputRecibido, Priority.SOMETIMES);

            
            textInputRecibido.setPromptText("0.00");
            textInputRecibido.setEditable(true);
            textInputRecibido.setPrefHeight(35);
            textInputRecibido.setPrefWidth(100);
            textInputRecibido.setStyle("-fx-background-color: #e5e5e5;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
            
            //igual a total en mas
            textInputRecibido.addEventFilter(KeyEvent.KEY_TYPED, eventHandler_textInputRecibido_mas_igual_total);
            textInputRecibido.textProperty().addListener( listener_textImputRecibido_formato_decimal );
            /*textInputRecibido.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent arg0) {                    
                    if( arg0.getCode().equals(KeyCode.ENTER)){
                        aceptarPromociones();
                    }
                } 
            });*/
                        
            Label pesos1 = new Label(" $ ");
            pesos1.setStyle("-fx-font: 25pt System;");
            
            keyCodeF10.setStyle("-fx-font: 12pt System;");
                        
            cntGrey.setStyle("-fx-background-color: #e5e5e5;-fx-background-radius: 5 5 5 5;");            
            cntGrey.getChildren().addAll(pesos1,textInputRecibido, keyCodeF10);
            
            hbxInputRecibido.getChildren().addAll(cntGrey);
            
           
            
            //textInputRecibido.requestFocus();

            inputImportes.getChildren().add(hbxInputRecibido);
            
            // Cambio
            HBox hbxInputCambio = new HBox();
            hbxInputCambio.setPrefHeight(200);
            hbxInputCambio.setPrefWidth(100);
            
            HBox.setHgrow(hbxInputCambio, Priority.SOMETIMES);

            textInputCambio.setFocusTraversable(false);
            textInputCambio.setPromptText("0.00");
            textInputCambio.setEditable(false);
            textInputCambio.setPrefHeight(35);
            textInputCambio.setPrefWidth(100);
            textInputCambio.setStyle("-fx-background-color: #e5e5e5;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
            Label pesos2 = new Label(" $ ");
            pesos2.setStyle("-fx-font: 25pt System;");
            
            HBox cntGrey2 = new HBox();
            cntGrey2.setStyle("-fx-background-color: #e5e5e5;-fx-background-radius: 5 5 5 5;");
            cntGrey2.getChildren().addAll(pesos2,textInputCambio, kc12);
            
            hbxInputCambio.getChildren().addAll(cntGrey2);           

            inputImportes.getChildren().add(hbxInputCambio);
        
        contImportes.getChildren().add(inputImportes);
        
        vBoxPrincipal.getChildren().add(contImportes);
        
        ///------- Botones
        HBox hbxPie = new HBox(30);
        hbxPie.setAlignment(Pos.CENTER);
        hbxPie.setPrefHeight(100);
        hbxPie.setMinHeight(100);
        VBox.setVgrow(hbxPie, Priority.SOMETIMES);
        
        //btnFinalizaVenta.setDisable(true);
        btnFinalizaVenta.setStyle(
                  "-fx-background-color: #0f3b84; -fx-text-fill: white; -fx-font: 25pt System; "
                + "-fx-faint-focus-color: transparent; -fx-focus-color:#e08906;");
        btnFinalizaVenta.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                aceptarPromociones();
            }
        });
        
        
        btnCancelar.setStyle("-fx-background-color: #e08906; -fx-text-fill: white; -fx-font: 25pt System;");
        btnCancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                rechazarPromociones();
            }
        });
        
        hbxPie.getChildren().add(btnFinalizaVenta);
        hbxPie.getChildren().add(btnCancelar);
        
        vBoxPrincipal.getChildren().add(hbxPie);
        
        
    }
    
    @Override
    public void setPresentador(IPromocionesPresentador _presentador){
        this.presentador = _presentador;
    }
    
    
    private void speachPrimerArticulo(final Set<ModeloArticulo> articulosRedondeo) {
        Platform.runLater(new Runnable(){
                @Override
                public void run(){
                    try{
                        if( articulosRedondeo != null && articulosRedondeo.iterator().hasNext() ){
                            ModeloArticulo art = articulosRedondeo.iterator().next();
                            lblTitulo_NombreProducto.setText(art.getNombre());
                            lblTitulo_PrecioDescuento.setText( String.format(local,"$ %.2f", art.getImporte()));
                            lblTitulo_PrecioRegular.setText( String.format(local,"$ %.2f", art.getImporte() + art.getDescuento() ));
                        }
                    }catch(Exception e){
                        SION.logearExcepcion(Modulo.VENTA, e, "speachPrimerArticulo :: " + Util.getStackTrace(e));
                    }
                }
        });
    }
    public void generarArticuloPromocion(final ModeloArticulo articulo, final boolean bigger){
        
            Platform.runLater(new Runnable(){
                @Override
                public void run(){
                    try{ 
                        //SION.log(Modulo.VENTA, "Generando vista articulo : " + articulo.getNombre(), Level.INFO);
                        final UIArticuloPromocion art = new UIArticuloPromocion(articulo, bigger);
                        
                        //SION.log(Modulo.VENTA, "Constructor pasado ", Level.INFO);
                        art.setEventoBoton(new EventHandler<ActionEvent>(){
                            @Override
                            public void handle(ActionEvent arg0) {
                                    presentador.agregarPromocion( art.getModelo() );
                            }
                        });

                        if( bigger ){
                            hboxScroll.getChildren().add(1, art );
                            System.out.println("bigger > "+art);
                        }else{
                           hboxScroll.getChildren().add( art );
                           System.out.println("normal > "+art);
                        }

                        recalcularTamanoScroll(art.getPrefWidth());
                    }catch(Exception e){
                        SION.logearExcepcion(Modulo.VENTA, e, "generarArticuloPromocion :: " + Util.getStackTrace(e));
                    }
                }
            });
         
    }
    
    private void limpiarArticulosPromocion(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                hboxScroll.getChildren().removeAll(hboxScroll.getChildren());
            }
        });
        
    }
    
    private void recalcularTamanoScroll(Double widthPromocion){
        if( hboxScroll.getChildren().size() > 3 ){
            final Double incremento = ((widthPromocion+hboxScroll.getSpacing()) * (hboxScroll.getChildren().size() - 3) );
            Platform.runLater(new Runnable() {
                @Override
                public void run() {                   
                    anchorPane.setPrefWidth(850 + incremento.intValue() );
                    hboxScroll.setPrefWidth(850 + incremento.intValue() );
                }
            });
            
        } 
    }
   
    
    @Override
    public void cargarPromociones(){
        
        limpiarArticulosPromocion();
        
        Service servicioCargaArticulos = new Service() {
            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {
                        mostrarLoading();
                        Set<ModeloArticulo> articulosRedondeo = (Set<ModeloArticulo>)repositorio.getPromocionesDisponibles();
                        //SION.log(Modulo.VENTA, "Vistas de articulos por generar : " + articulosRedondeo.size(), Level.INFO);
                        
                        boolean big = false;
                        int indiceBig=0, indAct=0;
                        ModeloArticulo mayor = null;
                        for( ModeloArticulo art : articulosRedondeo){
                            if( indAct++==indiceBig ){
                                mayor = art;
                            }else{
                                generarArticuloPromocion( art, false);
                            }
                            //generarArticuloPromocion( art, false);
                        }
                        if( mayor != null )
                            generarArticuloPromocion( mayor, true);
                        
                        speachPrimerArticulo(articulosRedondeo);
                       // SION.log(Modulo.VENTA, "Se generaron las vistas de los articulos.", Level.INFO);
                       
                        return "OK";
                    }

                };
            }
        };
        
        servicioCargaArticulos.stateProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                if( arg2.equals(Worker.State.SUCCEEDED) || arg2.equals(Worker.State.CANCELLED) || arg2.equals(Worker.State.FAILED)){
                   ocultarLoading();                   
                }
                //SION.log(Modulo.VENTA, "El servicio cambio a estatus: "+arg2, Level.INFO);
                setFocusImporteRecibido();  
            }
        });
        servicioCargaArticulos.exceptionProperty().addListener(new ChangeListener<Exception>() {
            @Override
            public void changed(ObservableValue<? extends Exception> arg0, Exception arg1, Exception nuevoVal) {
                SION.logearExcepcion(Modulo.VENTA, arg1, "servicioCargaArticulos"); 
                SION.logearExcepcion(Modulo.VENTA, nuevoVal, "servicioCargaArticulos");
            }
            
        });
        servicioCargaArticulos.start();
        
    }
    
    
    @Override
    public void desactivarPromociones(Object promocionSeleccionada){
        ModeloArticulo modeloSeleccionado = (ModeloArticulo)promocionSeleccionada;
        
        for(Node nodo : hboxScroll.getChildren()){                    
            UIArticuloPromocion nodoUIA = (UIArticuloPromocion)nodo;
            if( nodoUIA.getModelo() == modeloSeleccionado )
                nodoUIA.cambiarEstatusSeleccionado();
            else
                nodoUIA.cambiarEstatusNoSeleccionado();
        }
        Platform.runLater(new Runnable() {
            @Override
            public void run() {    }
        });
    }
    
    @Override
    public void mostrarError(final String mensaje){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {                   
                paneError.setVisible(true);
                txtMensajeError.textProperty().set(mensaje);
            }
        });
    }
    
    @Override
    public void ocultarError(){        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                paneError.setVisible(true);
                txtMensajeError.textProperty().set("");
            }
        });  
    }
    
    

    @Override
    public void mostrarLoading() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                 paneLoading.setVisible(true);
            }
        });        
    }

    @Override
    public void ocultarLoading() {        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                 paneLoading.setVisible(false);
            }
        });
    }

    @Override
    public Object obtenerVista() {
        return vBoxPrincipal;
    }


    @Override
    public void mostrarCostoPromocionesSeleccionadas(Object promocionesSeleccionadas ){
        Set<ModeloArticulo> promos = (Set<ModeloArticulo>) promocionesSeleccionadas;
        ModeloArticulo promo = null;
        if( promos.iterator().hasNext() ){
           promo = promos.iterator().next();
           setImporteTotal( String.format(local,"%,.2f", promo.getTotal() ));
           setFocusImporteRecibido();           
        }
        
    }
    
    
    @Override
    public void aceptarPromociones(){
        if( !btnFinalizaVenta.isDisabled() ){
            presentador.aceptarPromociones();
        }
    }
    
    @Override
    public void rechazarPromociones(){
            presentador.rechazarPromociones();
    }
    
    @Override
    public void cerrarPromociones(){
            presentador.cerrarPromociones();
    }
     
     
    private void calcularCambio(String _recibido){
        try{
            Double total = Double.parseDouble( textInputTotal.textProperty().get().replace("$", "") );
            Double recibido = Double.parseDouble( "0"+_recibido );

            Double cambio = recibido - total ;
            if( cambio < 0.0 ){
                setImporteCambio( "" );
                deshabilitarFinalizarVenta();
            }else{
                setImporteCambio( String.format(local,"%.2f", cambio) );
                habilitarFinalizarVenta();
            }
            
        }catch(Exception e){
            System.out.println("Error al calcular el cambio  ");
        }
        
    }
    
    private void setImporteTotal(final String total){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                textInputTotal.textProperty().set(total);
                calcularCambio(textInputRecibido.textProperty().get());
            }
        });
        
    }
    
   /* private void setImporteRecibido(final String recibido){
       
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                 textInputTotal.textProperty().set(recibido);
            }
        });
    }*/
    
    private void setImporteCambio(final String importe){        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                 textInputCambio.textProperty().set(importe);
            }
            
        });
    }

    private void deshabilitarFinalizarVenta() {
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                textInputRecibido.setStyle("-fx-background-color: #dd9d9d;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
                cntGrey.setStyle("-fx-background-color: #dd9d9d;-fx-background-radius: 5 5 5 5;");
            }        
        });
    }

    private void habilitarFinalizarVenta() {
        Platform.runLater(new Runnable(){
            @Override
            public void run() {               
                textInputRecibido.setStyle("-fx-background-color: #e5e5e5;-fx-font: 25pt System;-fx-background-radius: 5 5 5 5;");
                cntGrey.setStyle("-fx-background-color: #e5e5e5;-fx-background-radius: 5 5 5 5;");
            }        
        });
    }
    
   

    private void setFocusImporteRecibido() {
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                textInputRecibido.selectAll();
               textInputRecibido.requestFocus();
            }        
        });        
    }

    @Override
    public void setImporteRecibido(final Double importe) {
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                textInputRecibido.textProperty().set(String.format(local,"%.2f", importe));
            }            
        });  
    }

    @Override
    public void setImporteTotal(final Double importe) {
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                textInputTotal.textProperty().set(String.format(local,"%.2f", importe));
            }            
        });        
    }

    @Override
    public Double getImporteRecibido() {
        return Double.parseDouble( "0"+textInputRecibido.textProperty().get() );
    }

    @Override
    public void resetVista() {
        limpiarArticulosPromocion();
    }
    
}
