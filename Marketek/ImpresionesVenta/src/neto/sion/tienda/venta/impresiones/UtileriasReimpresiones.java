package neto.sion.tienda.venta.impresiones;

import neto.sion.impresion.ventanormal.dto.ArticuloTicketDto;
import neto.sion.impresion.ventanormal.dto.TipoPagoTicketDto;
import neto.sion.impresion.ventanormal.util.UtileriaTicket;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.venta.servicios.cliente.dto.ArticuloDto;
import neto.sion.venta.servicios.cliente.dto.TipoPagoDto;

/**
 * Clase de utilieria utilizada en la reimpresion de tickets y vouchers.
 *
 * @author Carlos V. Perez L.
 */
public class UtileriasReimpresiones {

    private double totalLetras = 0.0;

    /**
     * *
     * Metodo utilizado para el acoplamiento de entre los articulos para
     * reimpresion.
     *
     * @param articulos
     * @return Un arreglo de objetos de tipo ArticuloTicketDto.
     */
    public ArticuloTicketDto[] reimpresionarticulosDtoaTicket(ArticuloDto[] articulos) {
        ArticuloTicketDto[] articulosA = new ArticuloTicketDto[articulos.length];
        int cont = 0;
        for (ArticuloDto articulo : articulos) {
            ArticuloTicketDto articuloA = new ArticuloTicketDto();
            articuloA.setCantidad(String.valueOf(articulo.getFnCantidad()));
            articuloA.setImporte(String.valueOf(articulo.getFnPrecio() * articulo.getFnCantidad()));
            articuloA.setIva(articulo.getFnIva());
            articuloA.setNombre(articulo.getFcNombreArticulo());
            articuloA.setPrecio(String.valueOf(articulo.getFnPrecio()));
            articulosA[cont] = articuloA;
            cont++;
        }
        return articulosA;
    }

    /**
     * *
     * Metodo utilizado para el acoplamiento de entre los tipos de pago para
     * reimpresion.
     *
     * @param tipos
     * @param totalVenta
     * @param montoRecibido
     * @param efectivoCambio
     * @return Un arreglo de objetos de tipo TipoPagoTicketDto.
     */
    public TipoPagoTicketDto[] reimpresiontipoPagoDtoaTicket(TipoPagoDto[] tipos, Double totalVenta, double montoRecibido, double efectivoCambio) {
        TipoPagoTicketDto[] tiposA;
        if (efectivoCambio >= 0.0) {
            tiposA = new TipoPagoTicketDto[tipos.length + 2];
        } else {
            tiposA = new TipoPagoTicketDto[tipos.length + 1];
        }
        TipoPagoTicketDto tipoTotal = new TipoPagoTicketDto();
        tipoTotal.setDescripcionTipoPago(SION.obtenerMensaje(Modulo.VENTA, "ventas,ticket.tipopago.total"));
        tipoTotal.setMontoPago(String.valueOf(totalVenta));
        tipoTotal.setTipoPago(6);
        tiposA[0] = tipoTotal;
        int cont = 1;
        for (int i = 0; i < tipos.length; i++) {
            TipoPagoTicketDto tipoA = new TipoPagoTicketDto();
            switch (tipos[i].getFiTipoPagoId()) {
                case 1:
                    tipoA.setDescripcionTipoPago(SION.obtenerMensaje(Modulo.VENTA, "ventas.ticket.tipopago1"));
                    tipoA.setMontoPago(String.valueOf(montoRecibido));
                    tipoA.setTipoPago(1);
                    tiposA[cont] = tipoA;
                    cont++;
                    break;
                case 2:
                    tipoA.setDescripcionTipoPago(SION.obtenerMensaje(Modulo.VENTA, "tipo.pago.dos"));
                    tipoA.setMontoPago(String.valueOf(tipos[i].getFnMontoPago()));
                    tipoA.setTipoPago(2);
                    tiposA[cont] = tipoA;
                    cont++;
                    break;
                case 3:
                    tipoA.setDescripcionTipoPago(SION.obtenerMensaje(Modulo.VENTA, "ventas.ticket.tipopago3"));
                    tipoA.setMontoPago(String.valueOf(tipos[i].getFnMontoPago()));
                    tipoA.setTipoPago(3);
                    tiposA[cont] = tipoA;
                    cont++;
                    break;
            }
        }
        if (efectivoCambio >= 0.0) {
            TipoPagoTicketDto cambio = new TipoPagoTicketDto();
            cambio.setDescripcionTipoPago(SION.obtenerMensaje(Modulo.VENTA, "ventas.ticket.tipopago.cambio"));
            cambio.setMontoPago(String.valueOf(efectivoCambio));
            cambio.setTipoPago(5);
            tiposA[cont] = cambio;
        }
        //Sumamos la comision total
        UtileriaTicket util = new UtileriaTicket();
        totalLetras = util.quitarFormatoCantidad(tiposA[0].getMontoPago(), 1);
        tiposA[0].setMontoPago(String.valueOf(totalLetras));
        return tiposA;
    }

    /**
     * *
     * Regresa la cantidad total antes de ser transformada a cadena
     *
     * @return Un double.
     */
    public double getTotalLetras() {
        return totalLetras;
    }
}
