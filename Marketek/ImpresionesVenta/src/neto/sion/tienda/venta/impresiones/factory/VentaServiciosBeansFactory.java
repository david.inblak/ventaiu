package neto.sion.tienda.venta.impresiones.factory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import neto.sion.impresion.ventanormal.dto.VentaServiciosTicketDto;

/**
 *
 * @author agomez
 */
public class VentaServiciosBeansFactory {

    private VentaServiciosTicketDto impresionVtaServicios;
    
    public Collection getCollection(){
        List<VentaServiciosTicketDto> list = new ArrayList<VentaServiciosTicketDto>();
        list.add(impresionVtaServicios);
        return list;
    }
    
    public void setVentaServiciosTicketDto(VentaServiciosTicketDto bean){
        impresionVtaServicios = bean;
    }
}
