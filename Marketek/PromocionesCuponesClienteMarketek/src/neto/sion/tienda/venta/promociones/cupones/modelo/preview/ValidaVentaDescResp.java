/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.preview;
import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "Mensaje",
    "cuponesCuponera",
    "codigoError",
    "registroMktcId"
})
public class ValidaVentaDescResp {
    
    @JsonProperty("Mensaje")
    private String mensaje;
    @JsonProperty("cuponesCuponera")
    private List<CuponCuponeraBeanResp> cuponesCuponera = null;
    @JsonProperty("codigoError")
    private long codigoError;
     @JsonProperty("registroMktcId")
    private long registroMktcId;

    @JsonProperty("Mensaje")
    public String getMensaje() {
        return mensaje;
    }

    @JsonProperty("Mensaje")
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @JsonProperty("cuponesCuponera")
    public List<CuponCuponeraBeanResp> getCuponesCuponera() {
        return cuponesCuponera;
    }

    @JsonProperty("cuponesCuponera")
    public void setCuponesCuponera(List<CuponCuponeraBeanResp> cuponesCuponera) {
        this.cuponesCuponera = cuponesCuponera;
    }

    @JsonProperty("codigoError")
    public long getCodigoError() {
        return codigoError;
    }

    @JsonProperty("codigoError")
    public void setCodigoError(long codigoError) {
        this.codigoError = codigoError;
    }
    
    @JsonProperty("registroMktcId")
    public long getRegistroMktcId() {
        return registroMktcId;
    }
    
    @JsonProperty("registroMktcId")
    public void setRegistroMktcId(long registroMktcId) {
        this.registroMktcId = registroMktcId;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("mensaje:").append(mensaje)
                .append(",cuponesCuponera:").append(cuponesCuponera)
                .append(",codigoError:").append( codigoError)
                .append(",registroMktcId:").append( registroMktcId)
                .toString();
    }
}
