/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.preview;

import org.codehaus.jackson.annotate.JsonProperty;


public class PluBeginVentaBean {

    @JsonProperty("codigo")
    private String codigo;
    @JsonProperty("estructura")
    private String estructura;
    @JsonProperty("cantidad")
    private double cantidad;
    @JsonProperty("monto")
    private double monto;
    @JsonProperty("montoTotal")
    private double montoTotal;

    @JsonProperty("codigo")
    public String getCodigo() {
        return codigo;
    }

    @JsonProperty("codigo")
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @JsonProperty("estructura")
    public String getEstructura() {
        return estructura;
    }

    @JsonProperty("estructura")
    public void setEstructura(String estructura) {
        this.estructura = estructura;
    }

    @JsonProperty("cantidad")
    public double getCantidad() {
        return cantidad;
    }

    @JsonProperty("cantidad")
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    @JsonProperty("monto")
    public double getMonto() {
        return monto;
    }

    @JsonProperty("monto")
    public void setMonto(double monto) {
        this.monto = monto;
    }

    @JsonProperty("montoTotal")
    public double getMontoTotal() {
        return montoTotal;
    }

    @JsonProperty("montoTotal")
    public void setMontoTotal(double montoTotal) {
        this.montoTotal = montoTotal;
    }

    @Override
    public String toString() {
        return new StringBuilder("")
                .append("codigo:").append( codigo)
                .append(",estructura:").append( estructura)
                .append(",cantidad:").append( cantidad)
                .append(",monto:").append(monto)
                .append(",montoTotal:").append( montoTotal).toString();
    }

}

