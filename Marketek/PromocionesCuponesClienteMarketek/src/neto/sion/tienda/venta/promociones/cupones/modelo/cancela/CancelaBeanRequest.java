/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.cancela;

import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.util.ConversionBeanes;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author dramirezr
 */
public class CancelaBeanRequest {
    @JsonProperty("pos") private long pos;
    @JsonProperty("ticket") private long ticket;
    @JsonProperty("sucursal") private long sucursal;
    @JsonProperty("registroMktcId") private long registroMktcId;
    @JsonProperty("ticketCompleto") private String ticketCompleto;

    public long getPos() {
        return pos;
    }

    public void setPos(long pos) {
        this.pos = pos;
    }

    public long getRegistroMktcId() {
        return registroMktcId;
    }

    public void setRegistroMktcId(long registroMktcId) {
        this.registroMktcId = registroMktcId;
    }

    public long getSucursal() {
        return sucursal;
    }

    public void setSucursal(long sucursal) {
        this.sucursal = sucursal;
    }

    public long getTicket() {
        return ticket;
    }

    public void setTicket(long ticket) {
        this.ticket = ticket;
    }

    public String getTicketCompleto() {
        return ticketCompleto;
    }

    public void setTicketCompleto(String ticketCompleto) {
        this.ticketCompleto = ticketCompleto;
    }
    
    @Override
    public String toString() {
        try {
            return ConversionBeanes.convertir(this, String.class).toString();
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "toString");
        }
        return null;
    }
}
