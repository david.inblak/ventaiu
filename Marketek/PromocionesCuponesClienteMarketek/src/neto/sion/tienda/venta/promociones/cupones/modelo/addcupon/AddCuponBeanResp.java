
package neto.sion.tienda.venta.promociones.cupones.modelo.addcupon;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "Mensaje",
    "codigoError",
    "cupon"
})
public class AddCuponBeanResp {

	
    @JsonProperty("Mensaje")
    private String mensaje;
    @JsonProperty("codigoError")
    private long codigoError;
    @JsonProperty("cupon")
    private CuponBeanResp cupon;

    @JsonProperty("Mensaje")
    public String getMensaje() {
        return mensaje;
    }

    @JsonProperty("Mensaje")
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @JsonProperty("codigoError")
    public long getCodigoError() {
        return codigoError;
    }

    @JsonProperty("codigoError")
    public void setCodigoError(long codigoError) {
        this.codigoError = codigoError;
    }

    @JsonProperty("cupon")
    public CuponBeanResp getCupon() {
        return cupon;
    }

    @JsonProperty("cupon")
    public void setCupon(CuponBeanResp cupon) {
        this.cupon = cupon;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("mensaje:").append(mensaje)
                .append("codigoError:").append(codigoError)
                .append("cupon:").append(cupon).toString();
    }

}
