
package neto.sion.tienda.venta.promociones.cupones.modelo.addcupon;

import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
	"estado",
	"codigo",
	"estadoDesc",
    "linea1",
    "linea2",
    "descuentoTotal",
    "articulos"
})
public class CuponBeanResp {
	
    @JsonProperty("estado")
    private long estado;
    @JsonProperty("codigo")
    private String codigo;	
    @JsonProperty("estadoDesc")
    private String estadoDesc;
    @JsonProperty("linea1")
    private String linea1;
    @JsonProperty("linea2")
    private String linea2;
    @JsonProperty("descuentoTotal")
    private DescuentoTotalBean descuentoTotal;
    @JsonProperty("articulos")
    private List<ArticuloConDescuentoBean> articulos = null;
    
    
    @JsonProperty("estado")
    public long getEstado() {
        return estado;
    }

    @JsonProperty("estado")
    public void setEstado(long estado) {
        this.estado = estado;
    }
	
    
    @JsonProperty("codigo")
    public String getCodigo() {
        return codigo;
    }

    @JsonProperty("codigo")
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    @JsonProperty("estadoDesc")
    public String getEstadoDesc() {
        return estadoDesc;
    }

    @JsonProperty("estadoDesc")
    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    @JsonProperty("linea1")
    public String getLinea1() {
        return linea1;
    }

    @JsonProperty("linea1")
    public void setLinea1(String linea1) {
        this.linea1 = linea1;
    }

    @JsonProperty("linea2")
    public String getLinea2() {
        return linea2;
    }

    @JsonProperty("linea2")
    public void setLinea2(String linea2) {
        this.linea2 = linea2;
    }
    
    @JsonProperty("descuentoTotal")
    public DescuentoTotalBean getDescuentoTotal() {
        return descuentoTotal;
    }

    @JsonProperty("descuentoTotal")
    public void setDescuentoTotal(DescuentoTotalBean descuentoTotal) {
        this.descuentoTotal = descuentoTotal;
    }

    
    @JsonProperty("articulos")
    public List<ArticuloConDescuentoBean> getArticulos() {
        return articulos;
    }

    @JsonProperty("articulos")
    public void setArticulos(List<ArticuloConDescuentoBean> articulos) {
        this.articulos = articulos;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CuponBeanResp other = (CuponBeanResp) obj;
        if ((this.codigo == null) ? (other.codigo != null) : !this.codigo.equals(other.codigo)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (this.codigo != null ? this.codigo.hashCode() : 0);
        return hash;
    }

    

    @Override
    public String toString() {
        return new StringBuilder().append("estado:").append(estado).
                            append("codigo:").append( codigo).
                            append("estadoDesc:").append( estadoDesc).
                            append("linea1:").append( linea1).
                            append("linea2:").append(linea2).
                            append("descuentoTotal:").append(descuentoTotal).
                            append("articulos:").append(articulos).toString();
    }

}
