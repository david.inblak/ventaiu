package neto.sion.tienda.venta.promociones.cupones.modelo.pt.resp;

public class FinalizaVentaCuponesPTBeanResp {
	private RespuestaVentaBean respuestaVentaNormal;
	private RespuestaVentaTarjetaBean respuestaTarjeta;
	
	public RespuestaVentaBean getRespuestaVentaNormal() {
		return respuestaVentaNormal;
	}
	public void setRespuestaVentaNormal(
			RespuestaVentaBean respuestaVentaNormal) {
		this.respuestaVentaNormal = respuestaVentaNormal;
	}
	public RespuestaVentaTarjetaBean getRespuestaTarjeta() {
		return respuestaTarjeta;
	}
	public void setRespuestaTarjeta(
			RespuestaVentaTarjetaBean respuestaTarjeta) {
		this.respuestaTarjeta = respuestaTarjeta;
	}
}
