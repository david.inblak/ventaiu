package neto.sion.tienda.venta.promociones.cupones.modelo.finventa;

import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/***
 * Clase que representa la respuesta de una venta.
 * 
 * @author Carlos V. Perez L.
 * 
 */

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "paTransaccionId",
    "paCdgError",
    "paDescError",
    "paTaNumOperacion",
    "paTypCursorBlqs",
    "cupones",
    "xmlResponse"
})
public class FinalizaVentaCuponesBeanResp {
	
	@JsonProperty("paTransaccionId") private long paTransaccionId;
	@JsonProperty("paCdgError") private int paCdgError;
	@JsonProperty("paDescError") private String paDescError;
	@JsonProperty("paTaNumOperacion") private long paTaNumOperacion;
	@JsonProperty("paTypCursorBlqs") private BloqueoBean[] paTypCursorBlqs;
	@JsonProperty("cupones") private List<CuponesBean> cupones;
	@JsonProperty("xmlResponse") private String xmlResponse;
	
	
	
	
	

	@JsonProperty("paTypCursorBlqs")
	public BloqueoBean[] getPaTypCursorBlqs() {
		return paTypCursorBlqs;
	}
	
	@JsonProperty("paTypCursorBlqs")
	public void setPaTypCursorBlqs(BloqueoBean[] paTypCursorBlqs) {
		this.paTypCursorBlqs = paTypCursorBlqs;
	}

	@JsonProperty("paTransaccionId")
	public long getPaTransaccionId() {
		return paTransaccionId;
	}
	
	@JsonProperty("paTransaccionId")
	public void setPaTransaccionId(long paTransaccionId) {
		this.paTransaccionId = paTransaccionId;
	}

	@JsonProperty("paCdgError")
	public int getPaCdgError() {
		return paCdgError;
	}

	@JsonProperty("paCdgError")
	public void setPaCdgError(int paCdgError) {
		this.paCdgError = paCdgError;
	}

	@JsonProperty("paDescError")
	public String getPaDescError() {
		return paDescError;
	}

	@JsonProperty("paDescError")
	public void setPaDescError(String paDescError) {
		this.paDescError = paDescError;
	}

	@JsonProperty("paTaNumOperacion")
	public long getPaTaNumOperacion() {
		return paTaNumOperacion;
	}

	@JsonProperty("paTaNumOperacion")
	public void setPaTaNumOperacion(long paTaNumOperacion) {
		this.paTaNumOperacion = paTaNumOperacion;
	}

	@JsonProperty("cupones")
	public List<CuponesBean> getCupones() {
		return cupones;
	}

	
	@JsonProperty("cupones")
	public void setCupones(List<CuponesBean> cupones) {
		this.cupones = cupones;
	}

	@JsonProperty("xmlResponse")
	public String getXmlResponse() {
		return xmlResponse;
	}

	@JsonProperty("xmlResponse")
	public void setXmlResponse(String xmlResponse) {
		this.xmlResponse = xmlResponse;
	}
	
	
	 @Override
	 public String toString() {
		 return new StringBuilder().
                        append("paTransaccionId:").append(paTransaccionId).
                        append(",paCdgError:").append(paCdgError).
                        append(",paDescError:").append(paDescError).
                        append(",paTaNumOperacion:").append(paTaNumOperacion).
                        append(",paTypCursorBlqs:").append(paTypCursorBlqs).
                        append(",cupones:").append(cupones).
                        append(",xmlResponse:").append(xmlResponse) .toString();
	}

}
