/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.finventa;

import java.util.Iterator;
import java.util.Set;
import neto.sion.tienda.venta.promociones.cupones.modelo.addcupon.CuponBeanResp;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author dramirezr
 */
public class FinalizaVentaCuponesBeanRequest {
    @JsonProperty("paPaisId")
    private int paPaisId;//--- Heredado
    @JsonProperty("paConciliacionId")
    private long paConciliacionId;//--- Heredado
    @JsonProperty("ipTerminal")
    private String ipTerminal;//--- Heredado
    @JsonProperty("uId")
    private long uId;//--- Heredado
    @JsonProperty("tiendaId")
    private long tiendaId;//--- Heredado
    @JsonProperty("paTerminal")
    private String paTerminal;
    @JsonProperty("paUsuarioId")
    private long paUsuarioId;
    @JsonProperty("paUsuarioAutorizaId")
    private long paUsuarioAutorizaId;
    @JsonProperty("paFechaOper")
    private String paFechaOper;
    @JsonProperty("paTipoMovto")
    private int paTipoMovto;
    @JsonProperty("paMontoTotalVta")
    private double paMontoTotalVta;
    @JsonProperty("fueraLinea")
    private int fueraLinea;
    @JsonProperty("articulos")
    private ArticuloPeticionVentaBean[] articulos = null;
    @JsonProperty("tiposPago")
    private TipoPagoBean[] tiposPagos = null;
    @JsonProperty("esVentaInicializadaMktc")
    private boolean esVentaInicializadaMktc;// Activar cuando no se agreguen cupones o no se entre a la ventana de agregar cupones
    @JsonProperty("esVentaSinMarketec")
    private boolean esVentaSinMarketec;//+++  Activar esta propiedad cuando BeginVenta o AddCupon marquen error
    @JsonProperty("motivoSinCupones")
    private String motivoSinCupones;//+++
    @JsonProperty("finTicketMktc")
    private FinTicketMktcBeanReq finTicketMktc ;//+++    
    @JsonProperty("cuponesValidados")   /**     Lista de cupones obtenidos de la respuesta de agregar cupones*/ 
    private Set<CuponBeanResp> cuponesValidados;//+++
    @JsonProperty("registroMktcId")
    private long registroMktcId;//+++    

    public ArticuloPeticionVentaBean[] getArticulos() {
        return articulos;
    }

    public void setArticulos(ArticuloPeticionVentaBean[] articulos) {
        this.articulos = articulos;
    }

    public Set<CuponBeanResp> getCuponesValidados() {
        return cuponesValidados;
    }

    public void setCuponesValidados(Set<CuponBeanResp> cuponesValidados) {
        this.cuponesValidados = cuponesValidados;
        for (Iterator<CuponBeanResp> it = cuponesValidados.iterator(); it.hasNext();) {
            CuponBeanResp cuponBeanResp = it.next();
            if( cuponBeanResp.getEstado() == -1 ){
                it.remove();
            }
        }
    }

    public boolean isEsVentaInicializadaMktc() {
        return esVentaInicializadaMktc;
    }

    public void setEsVentaInicializadaMktc(boolean esVentaInicializadaMktc) {
        this.esVentaInicializadaMktc = esVentaInicializadaMktc;
    }

    public boolean isEsVentaSinMarketec() {
        return esVentaSinMarketec;
    }

    public void setEsVentaSinMarketec(boolean esVentaSinMarketec) {
        this.esVentaSinMarketec = esVentaSinMarketec;
    }

    public FinTicketMktcBeanReq getFinTicketMktc() {
        return finTicketMktc;
    }

    public void setFinTicketMktc(FinTicketMktcBeanReq finTicketMktc) {
        if( finTicketMktc != null ){
            finTicketMktc.setPos        ( Integer.parseInt(this.getPaTerminal().substring(this.getPaTerminal().length()-1, this.getPaTerminal().length())) );
            finTicketMktc.setSucursal   ( (int)this.getTiendaId() );
            finTicketMktc.setTicketCompleto( String.valueOf(this.getPaConciliacionId()) );
        }
        this.finTicketMktc = finTicketMktc;
    
    }

    public int getFueraLinea() {
        return fueraLinea;
    }

    public void setFueraLinea(int fueraLinea) {
        this.fueraLinea = fueraLinea;
    }

    public String getIpTerminal() {
        return ipTerminal;
    }

    public void setIpTerminal(String ipTerminal) {
        this.ipTerminal = ipTerminal;
    }

    public String getMotivoSinCupones() {
        return motivoSinCupones;
    }

    public void setMotivoSinCupones(String motivoSinCupones) {
        this.motivoSinCupones = motivoSinCupones;
    }

    public long getPaConciliacionId() {
        return paConciliacionId;
    }

    public void setPaConciliacionId(long paConciliacionId) {
        this.paConciliacionId = paConciliacionId;
    }

    public String getPaFechaOper() {
        return paFechaOper;
    }

    public void setPaFechaOper(String paFechaOper) {
        this.paFechaOper = paFechaOper;
    }

    public double getPaMontoTotalVta() {
        return paMontoTotalVta;
    }

    public void setPaMontoTotalVta(double paMontoTotalVta) {
        this.paMontoTotalVta = paMontoTotalVta;
    }

    public int getPaPaisId() {
        return paPaisId;
    }

    public void setPaPaisId(int paPaisId) {
        this.paPaisId = paPaisId;
    }

    public String getPaTerminal() {
        return paTerminal;
    }

    public void setPaTerminal(String paTerminal) {
        this.paTerminal = paTerminal;
    }

    public int getPaTipoMovto() {
        return paTipoMovto;
    }

    public void setPaTipoMovto(int paTipoMovto) {
        this.paTipoMovto = paTipoMovto;
    }

    public long getPaUsuarioAutorizaId() {
        return paUsuarioAutorizaId;
    }

    public void setPaUsuarioAutorizaId(long paUsuarioAutorizaId) {
        this.paUsuarioAutorizaId = paUsuarioAutorizaId;
    }

    public long getPaUsuarioId() {
        return paUsuarioId;
    }

    public void setPaUsuarioId(long paUsuarioId) {
        this.paUsuarioId = paUsuarioId;
    }

    public long getRegistroMktcId() {
        return registroMktcId;
    }

    public void setRegistroMktcId(long registroMktcId) {
        this.registroMktcId = registroMktcId;
    }

    public long getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(long tiendaId) {
        this.tiendaId = tiendaId;
    }

    public TipoPagoBean[] getTiposPagos() {
        return tiposPagos;
    }

    public void setTiposPagos(TipoPagoBean[] tiposPagos) {
        this.tiposPagos = tiposPagos;
    }

    public long getuId() {
        return uId;
    }

    public void setuId(long uId) {
        this.uId = uId;
    }
    
    
}
