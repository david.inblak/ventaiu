/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.finventa;

import java.util.Arrays;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class CuponesBean {
	@JsonProperty("codigo") private String codigo;
	@JsonProperty("codigoOferta") private int codigoOferta;
	@JsonProperty("vecesAImprimir") private int vecesAImprimir;
	@JsonProperty("informacionAdicional") private String informacionAdicional;
	@JsonProperty("logoCadena") private String logoCadena;
	@JsonProperty("logoProducto") private String logoProducto;
	@JsonProperty("lineas") private List<LineasBean> lineas;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getVecesAImprimir() {
        return vecesAImprimir;
    }

    public void setVecesAImprimir(int vecesAImprimir) {
        this.vecesAImprimir = vecesAImprimir;
    }
	
        
        
	
	public int getCodigoOferta() {
		return codigoOferta;
	}
	public void setCodigoOferta(int codigoOferta) {
		this.codigoOferta = codigoOferta;
	}
	public String getInformacionAdicional() {
		return informacionAdicional;
	}
	public void setInformacionAdicional(String informacionAdicional) {
		this.informacionAdicional = informacionAdicional;
	}
	public String getLogoCadena() {
		return logoCadena;
	}
	public void setLogoCadena(String logoCadena) {
		this.logoCadena = logoCadena;
	}
	public String getLogoProducto() {
		return logoProducto;
	}
	public void setLogoProducto(String logoProducto) {
		this.logoProducto = logoProducto;
	}
	public List<LineasBean> getLineas() {
		return lineas;
	}
	public void setLineas(List<LineasBean> lineas) {
		this.lineas = lineas;
	}

    @Override
    public String toString() {
        return "CuponesBean{" + 
                //"codigoOferta=" + codigoOferta + 
                "codig=" + codigo + 
                ", vecesAImprimir=" + vecesAImprimir + 
                ", informacionAdicional=" + informacionAdicional + 
                ", logoCadena=" + logoCadena + 
                ", logoProducto=" + logoProducto + 
                ", lineas=" + lineas + "}";
    }
	
	
	
	
	

}

