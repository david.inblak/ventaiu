package neto.sion.tienda.venta.promociones.cupones.modelo.finventa;


import org.codehaus.jackson.annotate.JsonProperty;

public class TipoDePagoMktcBean {

	@JsonProperty("tipo") private int tipo;
	@JsonProperty("monto") private double monto;
	@JsonProperty("bine") private String bine;
	@JsonProperty("variedad") private String variedad;
	@JsonProperty("cuotas") private String cuotas;
	@JsonProperty("banco") private String banco;
	@JsonProperty("dni") private String dni;
	
	
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public String getBine() {
		return bine;
	}
	public void setBine(String bine) {
		this.bine = bine;
	}
	public String getVariedad() {
		return variedad;
	}
	public void setVariedad(String variedad) {
		this.variedad = variedad;
	}
	public String getCuotas() {
		return cuotas;
	}
	public void setCuotas(String cuotas) {
		this.cuotas = cuotas;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	
	
	@Override
	public String toString() {
		return "TipoDePagoBean :[tipo=" + tipo
							 + ", monto=" + monto 
							 + ", bine=" + bine 
							 + ", variedad=" + variedad 
							 + ", cuotas=" + cuotas 
							 + ", banco=" + banco 
							 + ", dni=" + dni + "]";
	}
	

}
