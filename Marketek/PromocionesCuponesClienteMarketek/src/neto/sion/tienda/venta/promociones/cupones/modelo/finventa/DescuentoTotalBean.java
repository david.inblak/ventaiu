/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.finventa;


import org.codehaus.jackson.annotate.JsonProperty;

public class DescuentoTotalBean {
        /**      Codigo de descuento - Id de cupon*/
	@JsonProperty("codigo") private String codigo;
	@JsonProperty("monto") private String monto;
	
	@JsonProperty("oferta") private String oferta;
        /**         Descripcion de la oferta*/
	@JsonProperty("descripcionOferta") private String descripcionOferta;
        /**         Dato que podria ir en el espacio de la descripcion del cupon*/
	@JsonProperty("prefijo") private String prefijo;
        /**         id del cupon*/

	@JsonProperty("articuloId") private long articuloId;
	@JsonProperty("codigoBarras") private String codigoBarras;
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getOferta() {
		return oferta;
	}
	public void setOferta(String oferta) {
		this.oferta = oferta;
	}
	public String getPrefijo() {
		return prefijo;
	}
	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}

	
	public long getArticuloId() {
		return articuloId;
	}
	public void setArticuloId(long articuloId) {
		this.articuloId = articuloId;
	}
	public String getCodigoBarras() {
		return codigoBarras;
	}
	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}
	
	public String getDescripcionOferta() {
		return descripcionOferta;
	}
	public void setDescripcionOferta(String descripcionOferta) {
		this.descripcionOferta = descripcionOferta;
	}
	
	@Override
	public String toString() {
		return "DescuentoTotalBean :[codigo=" + codigo
						+ ", monto=" + monto 
						+ ", oferta=" + oferta 
						+ ", prefijo=" + prefijo
						+ "]";
		
	}
}