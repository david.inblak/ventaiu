/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.modelo.preview;

import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "idCliente",
    "tipo",
    "email",
    "nroCuentaCanje",
    "saldoPuntosCanje",
    "saldoPuntosSuma",
    "nombre",
    "apellido",
    "DNI",
    "segmentos"
})
public class IdentidadBeginVentaBean {
    
    @JsonProperty("idCliente")
    private String idCliente;
    @JsonProperty("tipo")
    private long tipo;
    @JsonProperty("email")
    private String email;
    @JsonProperty("nroCuentaCanje")
    private String nroCuentaCanje;
    @JsonProperty("saldoPuntosCanje")
    private String saldoPuntosCanje;
    @JsonProperty("saldoPuntosSuma")
    private String saldoPuntosSuma;
    @JsonProperty("nombre")
    private String nombre;
    @JsonProperty("apellido")
    private String apellido;
    @JsonProperty("DNI")
    private String dNI;
    @JsonProperty("segmentos")
    private List<Long> segmentos = null;

    @JsonProperty("idCliente")
    public String getIdCliente() {
        return idCliente;
    }

    @JsonProperty("idCliente")
    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    @JsonProperty("tipo")
    public long getTipo() {
        return tipo;
    }

    @JsonProperty("tipo")
    public void setTipo(long tipo) {
        this.tipo = tipo;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("nroCuentaCanje")
    public String getNroCuentaCanje() {
        return nroCuentaCanje;
    }

    @JsonProperty("nroCuentaCanje")
    public void setNroCuentaCanje(String nroCuentaCanje) {
        this.nroCuentaCanje = nroCuentaCanje;
    }

    @JsonProperty("saldoPuntosCanje")
    public String getSaldoPuntosCanje() {
        return saldoPuntosCanje;
    }

    @JsonProperty("saldoPuntosCanje")
    public void setSaldoPuntosCanje(String saldoPuntosCanje) {
        this.saldoPuntosCanje = saldoPuntosCanje;
    }

    @JsonProperty("saldoPuntosSuma")
    public String getSaldoPuntosSuma() {
        return saldoPuntosSuma;
    }

    @JsonProperty("saldoPuntosSuma")
    public void setSaldoPuntosSuma(String saldoPuntosSuma) {
        this.saldoPuntosSuma = saldoPuntosSuma;
    }

    @JsonProperty("nombre")
    public String getNombre() {
        return nombre;
    }

    @JsonProperty("nombre")
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @JsonProperty("apellido")
    public String getApellido() {
        return apellido;
    }

    @JsonProperty("apellido")
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @JsonProperty("DNI")
    public String getDNI() {
        return dNI;
    }

    @JsonProperty("DNI")
    public void setDNI(String dNI) {
        this.dNI = dNI;
    }

    @JsonProperty("segmentos")
    public List<Long> getSegmentos() {
        return segmentos;
    }

    @JsonProperty("segmentos")
    public void setSegmentos(List<Long> segmentos) {
        this.segmentos = segmentos;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(",idCliente:").append(idCliente)
                .append(",tipo:").append(tipo)
                .append(",email:").append(email)
                .append(",nroCuentaCanje:").append( nroCuentaCanje)
                .append(",saldoPuntosCanje:").append( saldoPuntosCanje)
                .append(",saldoPuntosSuma:").append(saldoPuntosSuma)
                .append(",nombre:").append( nombre)
                .append(",apellido:").append( apellido)
                .append(",dNI:").append( dNI)
                .append(",segmentos:").append( segmentos).toString();
    }

}
