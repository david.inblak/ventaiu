
package neto.sion.tienda.venta.promociones.cupones.modelo.addcupon;


import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.util.ConversionBeanes;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "codigoCupon",
    "pedido",
    "pos",
    "sucursal",
    "ticket",
    "token",
    "registroMktcId"
})
public class AddCuponBeanReq {

    
    

	
    @JsonProperty("codigoCupon")
    private Long codigoCupon = null;
    @JsonProperty("pedido")
    private Integer pedido;
    @JsonProperty("pos")
    private Integer pos;
    @JsonProperty("sucursal")
    private Integer sucursal;
    @JsonProperty("ticket")
    private String ticket;
    @JsonProperty("ticketCompleto")  
    private String ticketCompleto;
    @JsonProperty("token")
    private String token;
    @JsonProperty("registroMktcId")
    private String registroMktcId;

    public AddCuponBeanReq() {
    }

    public AddCuponBeanReq(Integer pedido, Integer pos, Integer sucursal, String ticket, String ticketCompleto, String token, Long codigoCupon, String registroMktcId) {
        this.pedido = pedido;
        this.pos = pos;
        this.sucursal = sucursal;
        this.ticket = ticket;
        this.ticketCompleto = ticketCompleto;
        this.token = token;
        this.codigoCupon = codigoCupon;
        this.registroMktcId = registroMktcId;
    }
    
    public String getTicketCompleto() {
        return ticketCompleto;
    }

    public void setTicketCompleto(String ticketCompleto) {
        this.ticketCompleto = ticketCompleto;
    }

    
    @JsonProperty("codigoCupon")
    public long getCodigoCupon() {
        return codigoCupon;
    }

    @JsonProperty("codigoCupon")
    public void setCodigoCupon(long codigoCupon) {
        this.codigoCupon = codigoCupon;
    }

    @JsonProperty("pedido")
    public int getPedido() {
        return pedido;
    }

    @JsonProperty("pedido")
    public void setPedido(int pedido) {
        this.pedido = pedido;
    }

    @JsonProperty("pos")
    public int getPos() {
        return pos;
    }

    @JsonProperty("pos")
    public void setPos(int pos) {
        this.pos = pos;
    }

    @JsonProperty("sucursal")
    public int getSucursal() {
        return sucursal;
    }

    @JsonProperty("sucursal")
    public void setSucursal(int sucursal) {
        this.sucursal = sucursal;
    }

    @JsonProperty("ticket")
    public String getTicket() {
        return ticket;
    }

    @JsonProperty("ticket")
    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    @JsonProperty("token")
    public String getToken() {
        return token;
    }

    @JsonProperty("token")
    public void setToken(String token) {
        this.token = token;
    }

    @JsonProperty("registroMktcId")
    public String getRegistroMktcId() {
        return registroMktcId;
    }

    @JsonProperty("registroMktcId")
    public void setRegistroMktcId(String registroMktcId) {
        this.registroMktcId = registroMktcId;
    }
    
    

    @Override
    public String toString() {
        try {
            return ConversionBeanes.convertir(this, String.class).toString();
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "toString");
        }
        return null;
    }

}
