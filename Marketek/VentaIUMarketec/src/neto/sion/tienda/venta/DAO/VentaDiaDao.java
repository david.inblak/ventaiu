/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.modelo.ArticuloVendidoBean;
import neto.sion.tienda.venta.modelo.PeticionArticulosVendidosBean;
import neto.sion.tienda.venta.modelo.RespuestaArticulosVendidosBean;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.apache.commons.dbcp.DelegatingConnection;

/**
 *
 * @author fvega
 */
public class VentaDiaDao {

    private String sql;
    private OracleConnection oracleConnection;
    private CallableStatement callableStatement;
    //private ResultSet resultSet;
    private Connection connection;
    private ARRAY arregloArticulos;
    private Object[] objetoArregloArticulos;
    private StructDescriptor estructura;
    private ArrayDescriptor estructuraArreglo;
    private int contador;

    public VentaDiaDao() {
        this.sql = null;
        this.callableStatement = null;
        //this.resultSet = null;
        this.arregloArticulos = null;
        this.estructura = null;
        this.estructuraArreglo = null;
        this.contador = 0;
    }

    public RespuestaArticulosVendidosBean insertaArticulosVendidos(PeticionArticulosVendidosBean _peticion) {

        SION.log(Modulo.VENTA, "Petición de inserción de artículos vendidos: " + _peticion.toString(), Level.INFO);
        RespuestaArticulosVendidosBean respuesta = new RespuestaArticulosVendidosBean();
        contador = 0;

        try {
            connection = Conexion.obtenerConexion();
            oracleConnection = (OracleConnection) ((DelegatingConnection) connection).getInnermostDelegate();
            sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAVENTAS.SPREGISTRAREPORTEVENTA");
            SION.log(Modulo.VENTA, "SQL a ejecutar... " + sql, Level.FINE);
            callableStatement = connection.prepareCall(sql);

            objetoArregloArticulos = new Object[_peticion.getArticuloVendidoBeans().length];
            estructura = StructDescriptor.createDescriptor(
                    SION.obtenerParametro(Modulo.VENTA, "estructura.articuloVendido"), oracleConnection);
            estructuraArreglo = ArrayDescriptor.createDescriptor(
                    SION.obtenerParametro(Modulo.VENTA, "estructura.arreglo.articuloVendido"), oracleConnection);

            for (ArticuloVendidoBean articulo : _peticion.getArticuloVendidoBeans()) {
                Object[] objeto = new Object[]{articulo.getArticuloId(), articulo.getCodigoBarras(), articulo.getCantidad(),
                    articulo.getPrecio(), articulo.getCosto(), articulo.getDescuento(), articulo.getIva(), articulo.getIeps()};
                objetoArregloArticulos[contador] = new STRUCT(estructura, oracleConnection, objeto);
                contador++;
            }

            arregloArticulos = new ARRAY(estructuraArreglo, oracleConnection, objetoArregloArticulos);

            callableStatement.setArray(1, arregloArticulos);
            callableStatement.registerOutParameter(2, OracleTypes.NUMBER);
            callableStatement.registerOutParameter(3, OracleTypes.VARCHAR);
            callableStatement.execute();
            
            respuesta.setCodigoError(callableStatement.getInt(2));
            respuesta.setDescripcionError(callableStatement.getString(3));
            
            SION.log(Modulo.VENTA, "Respuesta a la petición: "+respuesta.toString(), Level.INFO);

        } catch (Exception e) {
            SION.log(Modulo.VENTA, "Se generó un error al consultar peticion", Level.SEVERE);
            SION.logearExcepcion(Modulo.VENTA, e, sql);
            respuesta = null;
        } finally {
            Conexion.cerrarCallableStatement(callableStatement);
            Conexion.cerrarConexion(connection);
        }

        return respuesta;
    }
}
