/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package neto.sion.tienda.venta.modelo;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import neto.sion.venta.bloqueo.bean.BloqueoVentaBean;

/**
 *
 * @author fvega
 */
public class DatosTiendaBean {
    
    public SimpleStringProperty paiptda;   
    public SimpleIntegerProperty finumerocaja;
    public SimpleIntegerProperty fipaisid;
    public SimpleLongProperty fitiendaid;
    public SimpleStringProperty fcnombretienda;
    public SimpleIntegerProperty fifrontal;
    public SimpleStringProperty fccanal;
    public SimpleStringProperty fcestado;
    public SimpleStringProperty fccalle;
    public SimpleStringProperty fccolonia;
    public SimpleStringProperty fcciudad;
    public SimpleStringProperty fcmunicipio;
    public SimpleStringProperty fccodigopostal;
    public SimpleStringProperty fctelefono;
    public SimpleStringProperty fcafiliacion;
    public BloqueoVentaBean bloqueoVentaSesion;   
       
 
	public DatosTiendaBean(String apaiptda, int afipaisid, long afitiendaid,  int afinumerocaja,  String afcnombretienda, 
                String afccanal, String afcestado, String afccolonia, String afccalle,
                String afcciudad, String afcmunicipio, String afccodigopostal, String afctelefono,
                String afcafiliacion, BloqueoVentaBean bloqueoVentaSesion) 
        {
            this.paiptda = new SimpleStringProperty(apaiptda);
            this.fipaisid = new SimpleIntegerProperty(afipaisid);
            this.fitiendaid = new SimpleLongProperty(afitiendaid);
            this.finumerocaja = new SimpleIntegerProperty(afinumerocaja);//terminal
            this.fcnombretienda = new SimpleStringProperty(afcnombretienda);
            this.fccanal = new SimpleStringProperty(afccanal);//canalterminal
            this.fcestado = new SimpleStringProperty(afcestado);
            this.fccolonia = new SimpleStringProperty(afccolonia);
            this.fccalle = new SimpleStringProperty(afccalle);
            this.fcciudad = new SimpleStringProperty(afcciudad);
            this.fcmunicipio = new SimpleStringProperty(afcmunicipio);
            this.fccodigopostal = new SimpleStringProperty(afccodigopostal);
            this.fctelefono = new SimpleStringProperty(afctelefono);
            this.fcafiliacion = new SimpleStringProperty(afcafiliacion);
            this.bloqueoVentaSesion = bloqueoVentaSesion;
            
            //this.fifrontal = new SimpleIntegerProperty(afifrontal);
            
            
        }
        
        public BloqueoVentaBean getBloqueoVentaSesion() {
        return bloqueoVentaSesion;
    }

    public void setBloqueoVentaSesion(BloqueoVentaBean bloqueoVentaSesion) {
        this.bloqueoVentaSesion = bloqueoVentaSesion;
    }  

    public String getFcafiliacion() {
        return fcafiliacion.get();
    }
    public void setFcafiliacion(String afcafiliacion) {
        fcafiliacion.set(afcafiliacion);
    }
    public SimpleStringProperty fcafiliacionProperty(){
        return fcafiliacion;
    }
    ///////
    public String getFccalle() {
        return fccalle.get();
    }
    public void setFccalle(String afccalle) {
        fccalle.set(afccalle);
    }
    public SimpleStringProperty fccalleProperty(){
        return fccalle;
    }
    //////
    public String getFccanal() {
        return fccanal.get();
    }
    public void setFccanal(String afccanal) {
        fccanal.set(afccanal);
    }
    public SimpleStringProperty fccanalProperty(){
        return fccanal;
    }
    //////
    public String getFcciudad() {
        return fcciudad.get();
    }
    public void setFcciudad(String afcciudad) {
        fcciudad.set(afcciudad);
    }
    SimpleStringProperty fcciudadProperty(){
        return fcciudad;
    }
    /////
    public String getFccodigopostal() {
        return fccodigopostal.get();
    }
    public void setFccodigopostal(String afccodigopostal) {
        fccodigopostal.set(afccodigopostal);
    }
    public SimpleStringProperty fccodigopostalProperty(){
        return fccodigopostal;
    }
    //////
    public String getFccolonia() {
        return fccolonia.get();
    }
    public void setFccolonia(String afccolonia) {
        fccolonia.set(afccolonia);
    }
    public SimpleStringProperty fccoloniaProperty(){
        return fccolonia;
    }
    /////
    public String getFcestado() {
        return fcestado.get();
    }
    public void setFcestado(String afcestado) {
        fcestado.set(afcestado);
    }
    public SimpleStringProperty fcestadoProperty(){
        return fcestado;
    }
    //////
    public String getFcmunicipio() {
        return fcmunicipio.get();
    }
    public void setFcmunicipio(String afcmunicipio) {
        fcmunicipio.set(afcmunicipio);
    }
    public SimpleStringProperty fcmunicipioProperty(){
        return fcmunicipio;
    }
    ///////
    public String getFctelefono() {
        return fctelefono.get();
    }
    public void setFctelefono(String afctelefono) {
        fctelefono.set(afctelefono);
    }
    public SimpleStringProperty fctelefonoProperty(){
        return fctelefono;
    } 
                
        public String getPaiptda() {
            return paiptda.get();
        }
        public void setPaiptda(String apaiptda) {
            paiptda.set(apaiptda);
        }
        public SimpleStringProperty paiptdaProperty() {
    		return paiptda;
        }
        /////
        public int getFipaisid() {
            return fipaisid.get();
        }
        public void setFipaisid(int afipaisid) {
            fipaisid.set(afipaisid);
        }
        public SimpleIntegerProperty fipaisidProperty() {
    		return fipaisid;
        }
        /////
        public long getFitiendaid() {
            return fitiendaid.get();
        }
        public void setFitiendaid(long afitiendaid) {
            fitiendaid.set(afitiendaid);
        }
        public SimpleLongProperty fitiendaidProperty() {
    		return fitiendaid;
        }
        /////
        public String getFcnombretienda(){
            return fcnombretienda.get();
        }
        public void setFcnombretienda(String afcnombretienda){
            fcnombretienda.set(afcnombretienda);
        }
        public SimpleStringProperty fcnombretiendaProperty(){
            return fcnombretienda;
        }
        /////
        public int getFinumerocaja(){
            return finumerocaja.get();
        }
        public void setFinumerocaja( int afinumerocaja){
            finumerocaja.set(afinumerocaja);
        }
        public SimpleIntegerProperty finumerocajaProperty(){
            return finumerocaja;
        }
    }

