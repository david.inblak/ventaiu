/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neto.sion.tienda.venta.bean;

/* @author Sammy Guergachi <sguergachi at gmail.com> */
public class PeticionActualizaConciliacionPendienteBean {
    int paPaisId;
    int paTiendaId;
    long paConciliacionId;

    public long getPaConciliacionId() {
        return paConciliacionId;
    }

    public void setPaConciliacionId(long paConciliacionId) {
        this.paConciliacionId = paConciliacionId;
    }

    public int getPaPaisId() {
        return paPaisId;
    }

    public void setPaPaisId(int paPaisId) {
        this.paPaisId = paPaisId;
    }

    public int getPaTiendaId() {
        return paTiendaId;
    }

    public void setPaTiendaId(int paTiendaId) {
        this.paTiendaId = paTiendaId;
    }
    
    
}
