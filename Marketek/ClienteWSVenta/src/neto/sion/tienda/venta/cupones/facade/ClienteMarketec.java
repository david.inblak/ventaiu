/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.cupones.facade;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
/*import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;*/
import java.util.logging.Level;
import javax.activation.DataHandler;

  /*  import javax.net.ssl.HostnameVerifier;
    import javax.net.ssl.HttpsURLConnection;
    import javax.net.ssl.SSLContext;
    import javax.net.ssl.SSLSession;
    import javax.net.ssl.TrustManager;
    import javax.net.ssl.X509TrustManager;*/


import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.proxy.WSVentaCuponesMarketecServiceStub;
//import neto.sion.venta.servicios.cliente.proxy.WSVentaCuponesMarketecServiceStub;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.ServiceClient;

/**
 *
 * @author dramirezr
 */
public class ClienteMarketec {
    private WSVentaCuponesMarketecServiceStub stub;
    private ServiceClient sc;
    public static int CODIGO_EXITO = 0;
    
    public ClienteMarketec() {
        try {           
                 /*       
             TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        // Trust always
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        // Trust always
                    }
                }
            };
             
              // Install the all-trusting trust manager
            SSLContext sc1 = SSLContext.getInstance("SSL");
            // Create empty HostnameVerifier
            HostnameVerifier hv = new HostnameVerifier() {
                        public boolean verify(String arg0, SSLSession arg1) {
                                return true;
                        }
            };
            
            sc1.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc1.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(hv);
            SSLContext.setDefault(sc1);*/
            long timeOut = Long.parseLong( SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE.TIMEOUT.CONNECT") ); 
            this.stub = new WSVentaCuponesMarketecServiceStub(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE"));
            sc = stub._getServiceClient();
             sc.getOptions().setTimeOutInMilliSeconds(timeOut); 
            //sc.engageModule("rampart");
        } catch (AxisFault ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "Error al intentar crear el cliente: ClienteMarketec");
        }/*catch (NoSuchAlgorithmException ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "Error al intentar crear el cliente: ClienteMarketec");
        }catch (java.security.KeyManagementException ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "Error al intentar crear el cliente: ClienteMarketec");
        }*/
    }
    
    
    public Object validaDesceuntosAutomaticos(Object solicitud) throws Exception{
        sc.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE.TIMEOUT.CONNECT.AUTOMATICO")));
        //sc.engageModule("rampart");
        //sc.engageModule("rampart");
               
        System.out.println("Solicitud > validaDesceuntosAutomaticos: " + solicitud.toString());
        Object resp =this.stub.validaVentaDescuento(solicitud.toString());
        System.out.println("Respuesta > validaDesceuntosAutomaticos: " + resp.toString());
        return resp;
    }
    
    public Object calculaDescuentoCupones(Object solicitud) throws Exception{
        sc.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE.TIMEOUT.CONNECT.ADDCUPONES")));
        //sc.engageModule("rampart");
        //sc.engageModule("rampart");
        
        System.out.println("Solicitud > calculaDescuentoCupones: " + solicitud.toString());
        Object resp = this.stub.addCupon(solicitud.toString());
        System.out.println("Respuesta > calculaDescuentoCupones: " + resp.toString());
        return resp;
    }
    
    public Object finalizaVenta(Object solicitud) throws Exception{
        sc.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE.TIMEOUT.CONNECT.FINALIZAVENTA")));
        //sc.engageModule("rampart");
        //sc.engageModule("rampart");
        
        System.out.println("Solicitud > finalizaVenta: " + solicitud.toString());
        Object resp = this.stub.finalizaVentaCupones(solicitud.toString());
        System.out.println("Respuesta > finalizaVenta: " + resp.toString());
        return resp;
    }
    
    public Object cancelarVenta(Object solicitud) throws Exception{
        sc.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE.TIMEOUT.CONNECT.CANCELARVENTA")));
        SION.log(Modulo.VENTA, "Solicitud > cancelarVenta: " + solicitud.toString(), Level.INFO);
        Object resp = this.stub.cancelaVentaMktc(solicitud.toString());
        SION.log(Modulo.VENTA, "Respuesta > cancelarVenta: " + resp.toString(), Level.INFO);
        return resp;
    }
    
    public byte[] desgargaImagen(String nombreImagen) throws Exception{
        WSVentaCuponesMarketecServiceStub.ObtenerImagenResp resp = null;
        byte[] imagenBytes = null;
        
        try
        {
            sc.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE.TIMEOUT.CONNECT.DESCARGAIMAGEN")));
            SION.log(Modulo.VENTA, "Solicitud > desgargaImagen: " + nombreImagen, Level.INFO);
            
            resp = this.stub.obtenerImagen(nombreImagen);
            if( resp.getCodigoError() == CODIGO_EXITO ){
                DataHandler handler = resp.getImagen();
                ByteArrayOutputStream output = new ByteArrayOutputStream();          
                handler.writeTo(output);          
                imagenBytes = output.toByteArray();
            }
            
            SION.log(Modulo.VENTA, "Respuesta > desgargaImagen: " + resp.getCodigoError() + " >> " + resp.getMsgError(), Level.INFO);
            
        } catch (IOException e) {
            SION.logearExcepcion(Modulo.VENTA, e, "Ocurrio un error al obtener la imagen");
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, "Ocurrio un error al obtener la imagen");
        } catch (Error e) {
            SION.logearExcepcion(Modulo.VENTA, new Exception(e), "Ocurrio un error al obtener la imagen");
        }
        
        return imagenBytes;
    }
}
