package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author agomez
 */
public class ServicioReimpresionDto {

    private double monto;
    private double impuesto;
    private double comision;
    private long numAutorizacion;
    private String referencia;
    private String mensaje;
    private String nomArticulo;

    public ServicioReimpresionDto() {
    }

    public ServicioReimpresionDto(double monto, double impuesto, double comision, long numAutorizacion, String referencia, String mensaje, String nomArticulo) {
        this.monto = monto;
        this.impuesto = impuesto;
        this.comision = comision;
        this.numAutorizacion = numAutorizacion;
        this.referencia = referencia;
        this.mensaje = mensaje;
        this.nomArticulo = nomArticulo;
    }

    public double getComision() {
        return comision;
    }

    public double getImpuesto() {
        return impuesto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public double getMonto() {
        return monto;
    }

    public String getNomArticulo() {
        return nomArticulo;
    }

    public long getNumAutorizacion() {
        return numAutorizacion;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setComision(double comision) {
        this.comision = comision;
    }

    public void setImpuesto(double impuesto) {
        this.impuesto = impuesto;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public void setNomArticulo(String nomArticulo) {
        this.nomArticulo = nomArticulo;
    }

    public void setNumAutorizacion(long numAutorizacion) {
        this.numAutorizacion = numAutorizacion;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @Override
    public String toString() {
        return "ServicioReimpresionDto{" + "monto=" + monto + ", impuesto=" + impuesto + ", comision=" + comision + ", numAutorizacion=" + numAutorizacion + ", referencia=" + referencia + ", mensaje=" + mensaje + ", nomArticulo=" + nomArticulo + '}';
    }
}
