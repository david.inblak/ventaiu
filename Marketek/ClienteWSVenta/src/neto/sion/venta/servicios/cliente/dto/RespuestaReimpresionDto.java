package neto.sion.venta.servicios.cliente.dto;

import java.util.Arrays;
import neto.sion.tienda.genericos.dto.Base;

/**
 * *
 * Clase que representa los datos de reimpresion de ticket y vouchers
 *
 * @author Carlos V. Perez L.
 *
 */
public class RespuestaReimpresionDto extends Base {

    private long paMoviminetoId;
    private long fiUsuarioId;
    private String paNombreUsuario;
    private String paFechaCreacion;
    private String paEstacionTrabajo;
    private ArticuloTAReimpresionDto[] articuloTAReimpresionDtos;
    private ArticuloReimpresionTicketDto[] articuloReimpresionDtos;
    private ServicioReimpresionDto[] serviciosReimpresionDtos;
    private ArticuloDevReimpresionDto[] arregloArticulosDevolucion;
    private TipoPagoReimpresionDto[] tipoPagoReimpresionDtos;
    private int paCdgError;
    private String paDescError;
    private TarjetaReimpresionDto[] tarjetaReimpresionDtos;
    private int paPermiteTicket;
    private int paTipoMovimiento;

    public ArticuloDevReimpresionDto[] getArregloArticulosDevolucion() {
        return arregloArticulosDevolucion;
    }

    public void setArregloArticulosDevolucion(ArticuloDevReimpresionDto[] arregloArticulosDevolucion) {
        this.arregloArticulosDevolucion = arregloArticulosDevolucion;
    }

    public ArticuloReimpresionTicketDto[] getArticuloReimpresionDtos() {
        return articuloReimpresionDtos;
    }

    public void setArticuloReimpresionDtos(ArticuloReimpresionTicketDto[] articuloReimpresionDtos) {
        this.articuloReimpresionDtos = articuloReimpresionDtos;
    }

    public ArticuloTAReimpresionDto[] getArticuloTAReimpresionDtos() {
        return articuloTAReimpresionDtos;
    }

    public void setArticuloTAReimpresionDtos(ArticuloTAReimpresionDto[] articuloTAReimpresionDtos) {
        this.articuloTAReimpresionDtos = articuloTAReimpresionDtos;
    }

    public long getFiUsuarioId() {
        return fiUsuarioId;
    }

    public void setFiUsuarioId(long fiUsuarioId) {
        this.fiUsuarioId = fiUsuarioId;
    }

    public int getPaCdgError() {
        return paCdgError;
    }

    public void setPaCdgError(int paCdgError) {
        this.paCdgError = paCdgError;
    }

    public String getPaDescError() {
        return paDescError;
    }

    public void setPaDescError(String paDescError) {
        this.paDescError = paDescError;
    }

    public String getPaEstacionTrabajo() {
        return paEstacionTrabajo;
    }

    public void setPaEstacionTrabajo(String paEstacionTrabajo) {
        this.paEstacionTrabajo = paEstacionTrabajo;
    }

    public String getPaFechaCreacion() {
        return paFechaCreacion;
    }

    public void setPaFechaCreacion(String paFechaCreacion) {
        this.paFechaCreacion = paFechaCreacion;
    }

    public long getPaMoviminetoId() {
        return paMoviminetoId;
    }

    public void setPaMoviminetoId(long paMoviminetoId) {
        this.paMoviminetoId = paMoviminetoId;
    }

    public String getPaNombreUsuario() {
        return paNombreUsuario;
    }

    public void setPaNombreUsuario(String paNombreUsuario) {
        this.paNombreUsuario = paNombreUsuario;
    }

    public int getPaPermiteTicket() {
        return paPermiteTicket;
    }

    public void setPaPermiteTicket(int paPermiteTicket) {
        this.paPermiteTicket = paPermiteTicket;
    }

    public int getPaTipoMovimiento() {
        return paTipoMovimiento;
    }

    public void setPaTipoMovimiento(int paTipoMovimiento) {
        this.paTipoMovimiento = paTipoMovimiento;
    }

    public ServicioReimpresionDto[] getServiciosReimpresionDtos() {
        return serviciosReimpresionDtos;
    }

    public void setServiciosReimpresionDtos(ServicioReimpresionDto[] serviciosReimpresionDtos) {
        this.serviciosReimpresionDtos = serviciosReimpresionDtos;
    }

    public TarjetaReimpresionDto[] getTarjetaReimpresionDtos() {
        return tarjetaReimpresionDtos;
    }

    public void setTarjetaReimpresionDtos(TarjetaReimpresionDto[] tarjetaReimpresionDtos) {
        this.tarjetaReimpresionDtos = tarjetaReimpresionDtos;
    }

    public TipoPagoReimpresionDto[] getTipoPagoReimpresionDtos() {
        return tipoPagoReimpresionDtos;
    }

    public void setTipoPagoReimpresionDtos(TipoPagoReimpresionDto[] tipoPagoReimpresionDtos) {
        this.tipoPagoReimpresionDtos = tipoPagoReimpresionDtos;
    }

    @Override
    public String toString() {
        return "RespuestaTicketReimpresionDto{" + 
                "paMoviminetoId=" + paMoviminetoId + 
                ", fiUsuarioId=" + fiUsuarioId + 
                ", paNombreUsuario=" + paNombreUsuario + 
                ", paFechaCreacion=" + paFechaCreacion + 
                ", paEstacionTrabajo=" + paEstacionTrabajo + 
                ", articuloTAReimpresionDtos=" + Arrays.toString(articuloTAReimpresionDtos) + 
                ", articuloReimpresionDtos=" + Arrays.toString(articuloReimpresionDtos) + 
                ", serviciosReimpresionDtos=" + Arrays.toString(serviciosReimpresionDtos) + 
                ", arregloArticulosDevolucion=" + Arrays.toString(arregloArticulosDevolucion) + 
                ", tipoPagoReimpresionDtos=" + Arrays.toString(tipoPagoReimpresionDtos) + 
                ", paCdgError=" + paCdgError + 
                ", paDescError=" + paDescError + 
                ", tarjetaReimpresionDtos=" + Arrays.toString(tarjetaReimpresionDtos)+ 
                ", paPermiteTicket=" + paPermiteTicket + 
                ", paTipoMovimiento=" + paTipoMovimiento + '}';
    }
}
