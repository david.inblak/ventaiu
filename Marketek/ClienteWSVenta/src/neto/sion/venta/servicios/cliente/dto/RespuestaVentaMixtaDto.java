package neto.sion.venta.servicios.cliente.dto;

/**
 * **
 * Clase que representa la respuesta de una venta mixta.
 *
 * @author Carlos V. Perez L.
 *
 */
public class RespuestaVentaMixtaDto {

    private long paTransaccionId;
    private int paCdgError;
    private String paDescError;
    private BloqueoDto[] paTypCursorBlqs;
    private RespuestaRecargaTADto[] respuestaRecargasTA;

    public int getPaCdgError() {
        return paCdgError;
    }

    public void setPaCdgError(int paCdgError) {
        this.paCdgError = paCdgError;
    }

    public String getPaDescError() {
        return paDescError;
    }

    public void setPaDescError(String paDescError) {
        this.paDescError = paDescError;
    }

    public RespuestaRecargaTADto[] getRespuestaRecargasTA() {
        return respuestaRecargasTA;
    }

    public void setRespuestaRecargasTA(RespuestaRecargaTADto[] respuestaRecargasTA) {
        this.respuestaRecargasTA = respuestaRecargasTA;
    }

    public long getPaTransaccionId() {
        return paTransaccionId;
    }

    public void setPaTransaccionId(long paTransaccionId) {
        this.paTransaccionId = paTransaccionId;
    }

    public BloqueoDto[] getPaTypCursorBlqs() {
        return paTypCursorBlqs;
    }

    public void setPaTypCursorBlqs(BloqueoDto[] paTypCursorBlqs) {
        this.paTypCursorBlqs = paTypCursorBlqs;
    }

    @Override
    public String toString() {
        return "RespuestaVentaMixtaDto{" + "paTransaccionId=" + paTransaccionId + ", paCdgError=" + paCdgError + ", paDescError=" + paDescError + ", paTypCursorBlqs=" + paTypCursorBlqs + ", respuestaRecargasTA=" + respuestaRecargasTA + '}';
    }
}
