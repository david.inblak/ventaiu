package neto.sion.venta.servicios.cliente.dto;

/**
 * *
 * Clase que representa la respuesta de una recarga de T.A
 *
 * @author Carlos V. Perez L.
 *
 */
public class RespuestaRecargaTADto {

    private int companiaId;
    private boolean recargaExitosa;
    private long numeroTelefonico;
    private long numeroOperacion;

    public int getCompaniaId() {
        return companiaId;
    }

    public void setCompaniaId(int companiaId) {
        this.companiaId = companiaId;
    }

    public boolean isRecargaExitosa() {
        return recargaExitosa;
    }

    public void setRecargaExitosa(boolean recargaExitosa) {
        this.recargaExitosa = recargaExitosa;
    }

    public long getNumeroTelefonico() {
        return numeroTelefonico;
    }

    public void setNumeroTelefonico(long numeroTelefonico) {
        this.numeroTelefonico = numeroTelefonico;
    }

    public long getNumeroOperacion() {
        return numeroOperacion;
    }

    public void setNumeroOperacion(long numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    @Override
    public String toString() {
        return "RespuestaRecargaTADto{" + "companiaId=" + companiaId + ", recargaExitosa=" + recargaExitosa + ", numeroTelefonico=" + numeroTelefonico + ", numeroOperacion=" + numeroOperacion + '}';
    }
}
