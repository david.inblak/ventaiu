package neto.sion.venta.servicios.cliente.dto;

import neto.sion.tienda.genericos.dto.Base;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * *
 * Clase que representa un tipo de pago.
 *
 * @author Carlos V. Perez L.
 *
 */
public class TipoPagoDto extends Base {

   @JsonProperty("fiTipoPagoId") private int fiTipoPagoId;
   @JsonProperty("fnMontoPago")  private double fnMontoPago;
   @JsonProperty("fnNumeroVales")  private int fnNumeroVales;
   @JsonProperty("paPagoTarjetaIdBus")  private long paPagoTarjetaIdBus;
   @JsonProperty("importeAdicional")  private double importeAdicional;
   @JsonProperty("fiMontoRecibido")  private double fiMontoRecibido;

    public TipoPagoDto() {
    }

    public TipoPagoDto(int fiTipoPagoId, double fnMontoPago, int fnNumeroVales, long paPagoTarjetaIdBus, double importeAdicional) {
        this.fiTipoPagoId = fiTipoPagoId;
        this.fnMontoPago = fnMontoPago;
        this.fnNumeroVales = fnNumeroVales;
        this.paPagoTarjetaIdBus = paPagoTarjetaIdBus;
        this.importeAdicional = importeAdicional;
    }

    public double getImporteAdicional() {
        return importeAdicional;
    }

    public void setImporteAdicional(double importeAdicional) {
        this.importeAdicional = importeAdicional;
    }

    public long getPaPagoTarjetaIdBus() {
        return paPagoTarjetaIdBus;
    }

    public void setPaPagoTarjetaIdBus(long paPagoTarjetaIdBus) {
        this.paPagoTarjetaIdBus = paPagoTarjetaIdBus;
    }

    public int getFiTipoPagoId() {
        return fiTipoPagoId;
    }

    public void setFiTipoPagoId(int fiTipoPagoId) {
        this.fiTipoPagoId = fiTipoPagoId;
    }

    public double getFnMontoPago() {
        return fnMontoPago;
    }

    public void setFnMontoPago(double fnMontoPago) {
        this.fnMontoPago = fnMontoPago;
    }

    public int getFnNumeroVales() {
        return fnNumeroVales;
    }

    public void setFnNumeroVales(int fnNumeroVales) {
        this.fnNumeroVales = fnNumeroVales;
    }

    public double getFiMontoRecibido() {
        return fiMontoRecibido;
    }

    public void setFiMontoRecibido(double fiMontoRecibido) {
        this.fiMontoRecibido = fiMontoRecibido;
    }

    @Override
    public String toString() {
        return "TipoPagoDto{" + "fiTipoPagoId=" + fiTipoPagoId + ", fnMontoPago=" + 
                fnMontoPago + ", fnNumeroVales=" + fnNumeroVales + ", paPagoTarjetaIdBus=" + 
                paPagoTarjetaIdBus + ", importeAdicional=" + importeAdicional + 
                ", fiMontoRecibido=" + fiMontoRecibido + '}';
    }
}
