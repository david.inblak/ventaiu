package neto.sion.venta.servicios.cliente.dto;

import java.util.Arrays;
import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;

public class PeticionDevolucionDto extends PeticionBaseVentaDto {

    private String paTerminal;
    private long paUsuarioId;
    private long paUsuarioAutorizaId;
    private String paFechaOper;
    private int paTipoMovto;
    private double paMontoTotalVta;
    private ArticuloDto[] articulos;
    private TipoPagoDto[] tiposPago;
    private int paTipoDevolucion;
    private long paTransaccionDev;
    private long paMovimientoIdDev;
    private int fueraLinea;

    public String getPaTerminal() {
        return paTerminal;
    }

    public void setPaTerminal(String paTerminal) {
        this.paTerminal = paTerminal;
    }

    public long getPaUsuarioId() {
        return paUsuarioId;
    }

    public void setPaUsuarioId(long paUsuarioId) {
        this.paUsuarioId = paUsuarioId;
    }

    public String getPaFechaOper() {
        return paFechaOper;
    }

    public void setPaFechaOper(String paFechaOper) {
        this.paFechaOper = paFechaOper;
    }

    public int getPaTipoMovto() {
        return paTipoMovto;
    }

    public void setPaTipoMovto(int paTipoMovto) {
        this.paTipoMovto = paTipoMovto;
    }

    public double getPaMontoTotalVta() {
        return paMontoTotalVta;
    }

    public void setPaMontoTotalVta(double paMontoTotalVta) {
        this.paMontoTotalVta = paMontoTotalVta;
    }

    public ArticuloDto[] getArticulos() {
        return articulos;
    }

    public void setArticulos(ArticuloDto[] articulos) {
        this.articulos = articulos;
    }

    public TipoPagoDto[] getTiposPago() {
        return tiposPago;
    }

    public void setTiposPago(TipoPagoDto[] tiposPago) {
        this.tiposPago = tiposPago;
    }

    public int getPaTipoDevolucion() {
        return paTipoDevolucion;
    }

    public void setPaTipoDevolucion(int paTipoDevolucion) {
        this.paTipoDevolucion = paTipoDevolucion;
    }

    public long getPaTransaccionDev() {
        return paTransaccionDev;
    }

    public void setPaTransaccionDev(long paTransaccionDev) {
        this.paTransaccionDev = paTransaccionDev;
    }

    public long getPaMovimientoIdDev() {
        return paMovimientoIdDev;
    }

    public void setPaMovimientoIdDev(long paMovimientoIdDev) {
        this.paMovimientoIdDev = paMovimientoIdDev;
    }

    public int getFueraLinea() {
        return fueraLinea;
    }

    public void setFueraLinea(int fueraLinea) {
        this.fueraLinea = fueraLinea;
    }

    public long getPaUsuarioAutorizaId() {
        return paUsuarioAutorizaId;
    }

    public void setPaUsuarioAutorizaId(long paUsuarioAutorizaId) {
        this.paUsuarioAutorizaId = paUsuarioAutorizaId;
    }
    
    @Override
    public String toString() {
        return "PeticionDevolucionDto{" + "paTerminal=" + paTerminal + ", paUsuarioId=" + 
                paUsuarioId + ", paUsuarioAutorizaId=" + paUsuarioAutorizaId + ", paFechaOper=" + paFechaOper + ", paTipoMovto=" + 
                paTipoMovto + ", paMontoTotalVta=" + paMontoTotalVta + ", articulos=" + 
                Arrays.toString(articulos) + ", tiposPago=" + Arrays.toString(tiposPago) + ", paTipoDevolucion=" + 
                paTipoDevolucion + ", paTransaccionDev=" + paTransaccionDev + ", "
                + "paMovimientoIdDev=" + paMovimientoIdDev + ", fueraLinea=" + fueraLinea + '}';
    }
}
