package neto.sion.venta.servicios.cliente.dto;

import neto.sion.tienda.genericos.dto.Base;

/**
 * *
 * Clase que representa la respuesta de una
 *
 * @author Carlos V. Perez L.
 */
public class RespuestaActReimpVoucherDto extends Base {

    private int paCdgError;
    private String paDescError;

    public int getPaCdgError() {
        return paCdgError;
    }

    public void setPaCdgError(int paCdgError) {
        this.paCdgError = paCdgError;
    }

    public String getPaDescError() {
        return paDescError;
    }

    public void setPaDescError(String paDescError) {
        this.paDescError = paDescError;
    }

    @Override
    public String toString() {
        return "RespuestaActReimpVoucherDto [paCdgErrorr=" + paCdgError
                + ", paDescError=" + paDescError + "]";
    }
}
