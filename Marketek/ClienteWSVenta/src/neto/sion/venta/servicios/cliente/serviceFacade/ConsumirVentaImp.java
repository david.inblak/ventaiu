package neto.sion.venta.servicios.cliente.serviceFacade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
/*import neto.sion.gen.http.conexion.ContextHttp;
import neto.sion.gen.http.conexion.HttpsConn;*/
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.exception.SionException;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.venta.servicios.cliente.dto.*;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.ArticuloBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.ArticuloPeticionVentaBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.ArticuloReimpresionTicketBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.ArticuloTAReimpresionBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.BloqueoBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.PeticionActReimpTicketBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.PeticionBloqueoBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.PeticionDetalleVentaBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.PeticionDevolucionArticulosBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.PeticionReimpresionBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.PeticionTransaccionCentralBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.PeticionUltimasTransaccionesVentaBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.PeticionVentaMixtaBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.PeticionVentaTABean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.ReimpresionBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.RespuestaActReimpTicketBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.RespuestaActReimpVoucherBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.RespuestaConsultaTransaccionTABean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.RespuestaConsultaVentaBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.RespuestaRecargaTABean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.RespuestaReimpresionBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.RespuestaUltimasTransaccionesVentaBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.RespuestaVentaBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.RespuestaVentaMixtaBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.ServicioReimpresionBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.TarjetaReimpresionBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.TipoPagoBean;
import neto.sion.venta.servicios.cliente.proxy.VentaServiceStub.TipoPagoReimpresionBean;
import neto.sion.venta.servicios.cliente.proxy.WSVentaCuponesMarketecServiceStub;
import neto.sion.venta.servicios.cliente.util.Utilerias;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.ServiceClient;

import neto.sion.ventared.servicios.cliente.proxy.VentaSeparacionServiceStub;

/**
 * Clase con la implementacion necesaria para consumir los metodos expuestos del
 * Servicio de Venta.
 *
 * @author Carlos V. Perez L.
 */
public class ConsumirVentaImp implements ConsumirVenta {

    static final int NOERRORCODE = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "noerror.codigo"));
    static final int TERMINAL_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "terminalerror.codigo"));
    static final int FECHAOPERACION_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "fechaoperacionerror.codigo"));
    static final int ARTICULOS_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "articuloserror.codigo"));
    static final int PAGOS_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "pagoserror.codigo"));
    static final int MONTOVENTA_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "montoventaerror.codigo"));
    static final int PAIS_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "paiserror.codigo"));
    static final int TIENDA_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.codigo"));
    static final int USUARIO_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "usuarioerror.codigo"));
    static final int TIPOMOV_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "tipomovimientoerror.codigo"));
    static final int NUMTRANSACCION_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "numtransaccionerror.codigo"));
    static final int MONTORECIBIDO_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "montorecibidoerror.codigo"));
    static final int COMPANIATEL_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "companiatelefonicaerror.codigo"));
    static final int NUMTEL_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "numtelefonicoerror.codigo"));
    static final int MAXTELID = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "companiatelefonica.maxid"));
    static final int MINTELID = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "companiatelefonica.minid"));
    static final int TIPOPROCESO_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "tipoprocesoerror.codigo"));
    static final int VOUCHER_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "arreglovouchererror.codigo"));
    static final int CONCILIACIONID_ERROR = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "conciliacionerror.codigo"));
    static final int VENTA_TA = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "arreglo.ventata.codigo"));
    static final int MOV_ID = Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "movimientoiderror.codigo"));
    private VentaServiceStub stub;
    private VentaSeparacionServiceStub stubV2;
    private WSVentaCuponesMarketecServiceStub stubV3;
    private ServiceClient sc;
    private ServiceClient sc2;
    private ServiceClient sc3;
    //private HttpsConn ventaRestCon;
    private long timedOut;
    private Utilerias utilerias;

    public ConsumirVentaImp() throws AxisFault, Exception {
        stub = new VentaServiceStub(SION.obtenerConfigurationContext(), SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice"));
        sc = stub._getServiceClient();
        sc.engageModule("rampart");        
        
        stubV2 = new VentaSeparacionServiceStub(SION.obtenerConfigurationContext(), SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice.redondeo"));
        sc2 = stubV2._getServiceClient();
        sc2.engageModule("rampart");
        
        
       /* ventaRestCon = new HttpsConn(
                SION.obtenerParametro(Modulo.VENTA, "endpoint.rest.venta"), 
                ContextHttp.getHttpsContext(),
                Modulo.VENTA);*/
        
        
        timedOut = 0;
        this.utilerias = new Utilerias();
    }

    public ConsumirVentaImp(long _timedOut) throws AxisFault, Exception {
        stub = new VentaServiceStub(SION.obtenerConfigurationContext(), SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice"));
        sc = stub._getServiceClient();
        sc.engageModule("rampart");
        
        stubV2 = new VentaSeparacionServiceStub(SION.obtenerConfigurationContext(), SION.obtenerParametro(Modulo.VENTA, "endpointseguro.ventaservice.redondeo"));
        sc2 = stubV2._getServiceClient();
        sc2.engageModule("rampart");
        
        stubV3 = new WSVentaCuponesMarketecServiceStub(SION.obtenerConfigurationContext(), SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE"));
        sc3 = stubV3._getServiceClient();
        sc3.engageModule("rampart");
                
        /*ventaRestCon = new HttpsConn(
                SION.obtenerParametro(Modulo.VENTA, "endpoint.rest.venta"), 
                ContextHttp.getHttpsContext(),
                Modulo.VENTA);*/
        
        timedOut = _timedOut;
        this.utilerias = new Utilerias();
        
        if (timedOut > 0) {
            sc3.getOptions().setTimeOutInMilliSeconds(timedOut);
        } else {
            sc3.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "ventanormal.axis2.timeout")));
        }
    }

    /**
     * *
     * Realiza la llamada al metodo registrarVenta expuesto por el WS de Venta
     *
     * @param _peticion
     * @return Un objeto con los detalles del registro de la venta.
     * @throws SionException
     */
    //@Override
    public RespuestaVentaDto registrarVenta_Anterior(PeticionVentaDto _peticion) throws SionException {
        RespuestaVentaDto respuesta = new RespuestaVentaDto();
        respuesta.setPaCdgError(NOERRORCODE);
        try {
            respuesta = validarPeticionVentaDto(_peticion);
            if (respuesta.getPaCdgError() != 0) {
                return respuesta;
            } else {
                VentaSeparacionServiceStub.PeticionVentaArticulosBean peticion = utilerias.peticionDtoaBean(_peticion);

                SION.log(Modulo.VENTA, "Enviando peticion de registro al WS de Venta, con UID:" + peticion.getUId(), Level.INFO);
                //SION.log(Modulo.VENTA, "Peticion client:" + peticion.toString(), Level.INFO);

                if (timedOut > 0) {
                    sc2.getOptions().setTimeOutInMilliSeconds(timedOut);
                } else {
                    sc2.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "ventanormal.axis2.timeout")));
                }
                SION.log(Modulo.VENTA, "Timed out con que se aplicara la venta: " + timedOut + " ms", Level.INFO);

                respuesta = respuestaBeanaDto(stubV2.registrarVentaArticulos(peticion));
                SION.log(Modulo.VENTA, "Recibiendo respuesta de WS de Venta", Level.INFO);
            }
        } catch (AxisFault ex) {
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.VENTA, ex, ex.getDetail().toString());
            } else {
                SION.logearExcepcion(Modulo.VENTA, ex);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw new SionException(ex);
        }
        return respuesta;
    }
    
    /**
     * *
     * Realiza la llamada al metodo registrarVenta expuesto por el WS de Venta
     *
     * @param _peticion
     * @return Un objeto con los detalles del registro de la venta.
     * @throws SionException
     */
    @Override
    public RespuestaVentaDto registrarVenta(PeticionVentaDto _peticion) throws SionException {
        RespuestaVentaDto respuesta = new RespuestaVentaDto();
        respuesta.setPaCdgError(NOERRORCODE);
        try {
            respuesta = validarPeticionVentaDto(_peticion);
            if (respuesta.getPaCdgError() != 0) {
                return respuesta;
            } else {
                /*respuesta = (RespuestaVentaDto) ventaRestCon
                        .enviarPost(
                                _peticion, 
                                RespuestaVentaDto.class, 
                                SION.obtenerParametro(Modulo.VENTA, "endpoint.rest.venta.path.venta_de_articulos"),
                                Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "endpoint.rest.venta.conntimeout")),    
                                Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "endpoint.rest.venta.readtimeout"))
                                );*/
                VentaSeparacionServiceStub.PeticionVentaArticulosBean peticion = utilerias.peticionDtoaBean(_peticion);

                SION.log(Modulo.VENTA, "Enviando peticion de registro al WS de Venta:" + _peticion, Level.INFO);                
                SION.log(Modulo.VENTA, "Timed out con que se aplicara la venta: " + timedOut + " ms", Level.INFO);

                respuesta = respuestaBeanaDto(stubV2.registrarVentaArticulos(peticion));
                SION.log(Modulo.VENTA, "Recibiendo respuesta de WS de Venta: " + respuesta, Level.INFO);
            }
        }  catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw new SionException(ex);
        }
        return respuesta;
    }
    
    @Override
    public String registrarVentaMarketec(String solicitud) throws SionException {        
        String respuesta = "";
        
        try {
                SION.log(Modulo.VENTA, "Enviando peticion de registro al WS de Venta:" + solicitud + 
                        "\nTimed out con que se aplicara la venta: " + timedOut + " ms", Level.INFO);
                              
                sc3.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE.TIMEOUT.CONNECT.FINALIZAVENTA")));
                               
                respuesta = stubV3.finalizaVentaCupones(solicitud);
                
                SION.log(Modulo.VENTA, "Recibiendo respuesta de WS de Venta: " + respuesta, Level.INFO);
        }  catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw new SionException(ex);
        }
        return respuesta;
    }

    /**
     * *
     * Obtiene el detalle de una venta, llamando al metodo obtenerDetalleVenta
     * expuesto por el WS de Venta.
     *
     * @param _peticion
     * @return Un objeto con los datos de la venta solicitada.
     * @throws SionException
     */
    @Override
    public RespuestaDetalleVentaDto obtenerDetalleVenta(PeticionDetalleVentaDto _peticion) throws SionException {
        RespuestaDetalleVentaDto respuesta = new RespuestaDetalleVentaDto();
        respuesta.setPaCdgError(NOERRORCODE);

        try {
            respuesta = validarPeticionDetalleVentaDto(_peticion);
            if (respuesta.getPaCdgError() != 0) {
                return respuesta;
            } else {
                PeticionDetalleVentaBean peticion = peticionDetalleVentaDtoaBean(_peticion);
                RespuestaConsultaVentaBean respuestaBean = stub.consultarDetalleVenta(peticion);
                SION.log(Modulo.VENTA, "Enviando peticion de detalle al WS de Venta con UID:" + peticion.getUId(), Level.INFO);
                respuesta = respuestaDetalleVentaBeanaDto(respuestaBean);
                SION.log(Modulo.VENTA, "Recibiendo respuesta de detalle del WS de Venta", Level.INFO);
            }
        } catch (AxisFault ex) {
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.VENTA, ex, ex.getDetail().toString());
            } else {
                SION.logearExcepcion(Modulo.VENTA, ex);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw new SionException(ex);
        }
        return respuesta;
    }

    /**
     * *
     * Realiza la devolucion de una venta, llamando al metodo
     * obtenerDevolucionVenta expuesto por el WS de venta.
     *
     * @param _peticion
     * @return Un objeto con los detalles de la devolucion.
     * @throws SionException
     */
    @Override
    public RespuestaVentaDto obtenerDevolucionVenta(PeticionDevolucionDto _peticion) throws SionException {
        RespuestaVentaDto respuesta = new RespuestaVentaDto();
        respuesta.setPaCdgError(NOERRORCODE);
        try {
            respuesta = validarPeticionDevolucionDto(_peticion);
            if (respuesta.getPaCdgError() != 0) {
                return respuesta;
            } else {
                PeticionDevolucionArticulosBean peticion = peticionDevolucionDtoaBean(_peticion);
                SION.log(Modulo.VENTA, "Enviando peticion de devolucion al WS de Venta con UID:" + peticion.getUId(), Level.INFO);
                RespuestaVentaBean respuestaBean = stub.registrarDevolucionArticulos(peticion);
                respuesta = respuestaBeanaDtoAnt(respuestaBean);
                SION.log(Modulo.VENTA, "Recibiendo respuesta de devolucion del WS de Venta", Level.INFO);
            }
        } catch (AxisFault ex) {
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.VENTA, ex, ex.getDetail().toString());
            } else {
                SION.logearExcepcion(Modulo.VENTA, ex);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw new SionException(ex);
        }

        return respuesta;
    }

    /**
     * *
     * Realiza una llamada al metodo registrarVentaTA expuesto por el WS de
     * Venta.
     *
     * @param _peticion
     * @return Un objeto con los detalles de la venta de TA.
     * @throws SionException
     */
    @Override
    public RespuestaVentaDto registrarVentaTA(PeticionVentaTADto _peticion) throws SionException {
        RespuestaVentaDto respuesta = new RespuestaVentaDto();
        respuesta.setPaCdgError(NOERRORCODE);
        try {
            respuesta = validarPeticionVentaTA(_peticion);
            if (respuesta.getPaCdgError() != 0) {
                return respuesta;
            } else {
                VentaServiceStub.PeticionVentaTABean peticion = peticionVentaTADTOaBean(_peticion);
                sc.getOptions().setTimeOutInMilliSeconds(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "tiempoaire.axis2.timeout")));
                SION.log(Modulo.VENTA, "Enviando peticion de registro de venta TA al WS de Venta con UID:" + peticion.getUId(), Level.INFO);
                RespuestaVentaBean respuestaBean = stub.registrarVentaTA(peticion);
                SION.log(Modulo.VENTA, "Recibiendo respuesta de registro de venta TA del WS de Venta", Level.INFO);
                respuesta = respuestaBeanaDtoAnt(respuestaBean);
            }
        } catch (AxisFault ex) {
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.VENTA, new Exception(ex.getCause()), ex.getDetail().toString());
            } else {
                SION.logearExcepcion(Modulo.VENTA, ex);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw new SionException(ex);
        }
        return respuesta;

    }

    /**
     * *
     * Metodo que regresa es estatus de un cajero.
     *
     * @param _cajeroId
     * @return Un objeto con el detalle del estatus.
     * @throws SionException
     */
    @Override
    public RespuestaBloqueoVentaDto obtenerEstatusCajero(PeticionBloqueoDto _peticion) throws SionException {
        RespuestaBloqueoVentaDto respuesta = new RespuestaBloqueoVentaDto();
        respuesta.setCdgError(NOERRORCODE);
        try {
            respuesta = validarPeticionBloqueo(_peticion);
            if (respuesta.getCdgError() != 0) {
                return respuesta;
            } else {
                PeticionBloqueoBean _peticionBean = peticionBloqueoDtoaBean(_peticion);
                SION.log(Modulo.VENTA, "Enviando peticion de estatus al WS de Venta con UID:" + _peticionBean.getUId(), Level.INFO);
                SION.log(Modulo.VENTA, "PETICION obtenerEstatusCajero: " + _peticion.toString(), Level.INFO);
                BloqueoBean[] respuestaBean = stub.consultaBloqueos(_peticionBean);
                SION.log(Modulo.VENTA, "Recibiendo respuesta de estatus del WS de Venta: " + Arrays.toString(respuestaBean), Level.INFO);
                respuesta = respuestaBloqueoVentaBeanaDtoAnt(respuestaBean);
                SION.log(Modulo.VENTA, "Respuesta de estatus del WS de Venta: " + respuesta.toString(), Level.INFO);
            }
        } catch (AxisFault ex) {
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.VENTA, ex, ex.getDetail().toString());
            } else {
                SION.logearExcepcion(Modulo.VENTA, ex);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw new SionException(ex);
        }

        return respuesta;
    }

    /**
     * *
     * Metodo que regresa todos los datos necesarios para la reimpresion de un
     * ticket.
     *
     * @param _peticion
     * @return Un objeto con los detalles necesarios para la reimpresion un
     * ticket.
     * @throws SionException
     */
    /*
     * @Override public RespuestaReimpresionDto
     * obtenerReimpresionTicket(PeticionReimpresionDto _peticion) throws
     * SionException { RespuestaReimpresionDto respuesta = new
     * RespuestaReimpresionDto(); ReimpresionTicketDto respuestaTicket = new
     * ReimpresionTicketDto(); ReimpresionVoucherDto respuestaVoucher = new
     * ReimpresionVoucherDto(); respuestaTicket.setPaCdgError(NOERRORCODE);
     * respuestaVoucher.setCdgError(NOERRORCODE); try { respuestaTicket =
     * validarPeticionReimpresionTicket(_peticion); if
     * (respuestaTicket.getPaCdgError() != 0) {
     * respuesta.setReimpresionTicketBean(respuestaTicket);
     * respuestaVoucher.setCdgError(respuestaTicket.getPaCdgError());
     * respuestaVoucher.setDescError(respuestaTicket.getPaDescError());
     * respuesta.setReimpresionVoucherBean(respuestaVoucher); return respuesta;
     * } else { PeticionReimpresionBean _peticionBean =
     * peticionReimpresionTicketDtoaBean(_peticion); SION.log(Modulo.VENTA,
     * "Enviando peticion de obtencion de datos para reimpresion de tickets con
     * UID:" + _peticionBean.getUId(), Level.INFO); RespuestaReimpresionBean
     * respuestaBean = stub.obtenerReimpresionTicket(_peticionBean);
     * if(respuestaBean.getRespuestaReimpresionTicketBean()==null)
     * System.out.println("nullllll"); SION.log(Modulo.VENTA, "Recibiendo
     * respuesta de datos para reimpresion de tickets:" + respuestaBean,
     * Level.INFO);
     * respuesta.setReimpresionTicketBean(respuestaReimpresionTicketBeanaDto(respuestaBean.getRespuestaReimpresionTicketBean()));
     * respuesta.setReimpresionVoucherBean(respuestaReimpresionVoucherBeanaDto(respuestaBean.getRespuestaReimpresionVoucherBean()));
     * } } catch (AxisFault ex) { if (ex.getDetail() != null) {
     * SION.logearExcepcion(Modulo.VENTA, ex, ex.getDetail().toString()); } else
     * { SION.logearExcepcion(Modulo.VENTA, ex); } throw new SionException(ex);
     * } catch (Exception ex) { SION.logearExcepcion(Modulo.VENTA, ex); throw
     * new SionException(ex); }
     *
     * return respuesta; }
     */
    /**
     * *
     * Metodo que regresa todos los datos necesarios para la reimpresion de
     * vouchers.
     *
     * @param _peticion
     * @return Un objeto con los detalles necesarios para la reimpresion un
     * ticket.
     * @throws SionException
     */
    @Override
    public RespuestaActReimpVoucherDto actualizarReimpresionVoucher(PeticionReimpresionDto _peticion) throws SionException {
        RespuestaActReimpVoucherDto respuesta = new RespuestaActReimpVoucherDto();
        respuesta.setPaCdgError(NOERRORCODE);
        try {
            respuesta = validarPeticionActualizacionReimpVoucher(_peticion);
            if (respuesta.getPaCdgError() != 0) {
                return respuesta;
            } else {
                PeticionReimpresionBean _peticionBean = peticionReimpresionTicketDtoaBean(_peticion);
                SION.log(Modulo.VENTA, "Enviando peticion de actualizacion de datos de reimpresion de vouchers con UID:" + _peticionBean.getUId(), Level.INFO);
                RespuestaActReimpVoucherBean respuestaBean = stub.actualizarReimpresionVoucher(_peticionBean);
                SION.log(Modulo.VENTA, "Recibiendo respuesta de actualizacion de datos de reimpresion de vouchers", Level.INFO);
                respuesta = respuestaActualizacionVoucherBeanaDto(respuestaBean);
            }
        } catch (AxisFault ex) {
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.VENTA, ex, ex.getDetail().toString());
            } else {
                SION.logearExcepcion(Modulo.VENTA, ex);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw new SionException(ex);
        }
        return respuesta;
    }

    /**
     * *
     * Metodo que consulta una transaccion en central.
     *
     * @param _peticion
     * @return Un objeto con los detalles de la consulta.
     * @throws SionException
     */
    @Override
    public RespuestaConsultaTransaccionCentralDto consultarTransaccionCentral(PeticionTransaccionCentralDto _peticion) throws SionException {
        RespuestaConsultaTransaccionCentralDto respuesta = new RespuestaConsultaTransaccionCentralDto();

        try {
            respuesta = validarConsultaTransaccionCentral(_peticion);
            if (respuesta.getPaCdgError() != 0) {
                return respuesta;
            } else {
                PeticionTransaccionCentralBean _peticionBean = peticionConsultaTransaccionDtoaBean(_peticion);
                SION.log(Modulo.VENTA, "Enviando peticion de consulta de transaccion local con UID:" + _peticionBean.getUId(), Level.INFO);
                long respuestaBean = stub.consultarTransaccionCentral(_peticionBean);
                SION.log(Modulo.VENTA, "Recibiendo respuesta de consulta de transaccion local", Level.INFO);
                respuesta.setPaTransaccionId(respuestaBean);
                if (respuestaBean != 0) {
                    respuesta.setPaCdgError(NOERRORCODE);
                    respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "conciliacion.sinerror.mensaje"));
                } else {
                    respuesta.setPaCdgError(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "conciliacion.error.codigo")));
                    respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "conciliacion.error.mensaje"));
                }
            }
        } catch (AxisFault ex) {
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.VENTA, ex, ex.getDetail().toString());
            } else {
                SION.logearExcepcion(Modulo.VENTA, ex);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw new SionException(ex);
        }

        return respuesta;
    }

    /**
     * *
     * Metodo que consulta una recarga en central.
     *
     * @param _peticion
     * @return Un objeto con los detalles de la consulta.
     * @throws SionException
     */
    @Override
    public RespuestaConsultaTransaccionTADto consultarTransaccionTACentral(PeticionTransaccionCentralDto _peticion)
            throws SionException {
        RespuestaConsultaTransaccionTADto respuesta = new RespuestaConsultaTransaccionTADto();
        try {
            VentaServiceStub.PeticionTransaccionCentralBean _peticionBean = peticionConsultaTransaccionDtoaBean(_peticion);
            SION.log(Modulo.VENTA, "Enviando peticion de consulta de transaccion local con UID:" + _peticionBean.getUId(), Level.INFO);
            VentaServiceStub.RespuestaConsultaTransaccionTABean respuestaBean = this.stub.consultarTransaccionTACentral(_peticionBean);
            SION.log(Modulo.VENTA, "Recibiendo respuesta de consulta de transaccion local", Level.INFO);
            respuesta = respuestaConsultaTransaccionTABeanaDto(respuestaBean);
        } catch (AxisFault ex) {
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.VENTA, ex, new String[]{ex.getDetail().toString()});
            } else {
                SION.logearExcepcion(Modulo.VENTA, ex, new String[0]);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, new String[0]);
            throw new SionException(ex);
        }

        return respuesta;
    }

    public RespuestaConsultaTransaccionTADto respuestaConsultaTransaccionTABeanaDto(RespuestaConsultaTransaccionTABean bean) {
        RespuestaConsultaTransaccionTADto respuesta = new RespuestaConsultaTransaccionTADto();
        respuesta.setCdgError(bean.getCdgError());
        respuesta.setDescError(bean.getDescError());
        respuesta.setNumAutorizacion(bean.getNumAutorizacion());
        respuesta.setNumTransaccion(bean.getNumTransaccion());
        return respuesta;
    }

    /**
     * *
     * Realiza la llamada al metodo registrarVentaMixta expuesto por el WS de
     * Venta
     *
     * @param _peticion
     * @return Un objeto con los detalles de venta mixta.
     * @throws SionException
     */
    @Override
    public RespuestaVentaMixtaDto registrarVentaMixta(PeticionVentaMixtaDto _peticion) throws SionException {
        RespuestaVentaMixtaDto respuesta = new RespuestaVentaMixtaDto();
        try {
            respuesta = validarPeticionVentaMixtaDto(_peticion);
            if (respuesta.getPaCdgError() != 0) {
                return respuesta;
            } else {
                PeticionVentaMixtaBean peticionBean = peticionVentaMixtaDtoaBean(_peticion);
                SION.log(Modulo.VENTA, "Enviando peticion de venta mixta con UID:" + peticionBean.getUId(), Level.INFO);
                RespuestaVentaMixtaBean repuestaBean = stub.registrarVentaMixta(peticionBean);
                SION.log(Modulo.VENTA, "Recibiendo respuesta de peticion de venta mixta:" + repuestaBean.toString(), Level.INFO);
                respuesta = respuestaVentaMixtaBeanaDto(repuestaBean);
            }
        } catch (AxisFault ex) {
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.VENTA, ex, ex.getDetail().toString());
            } else {
                SION.logearExcepcion(Modulo.VENTA, ex);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw new SionException(ex);
        }
        return respuesta;
    }

    /**
     * *
     * Realiza la llamada al metodo confirmarReimpresionTicket expuesto por el
     * WS de Venta
     *
     * @param _peticion
     * @return Un objeto con los de la confirmacion de la reimpresion..
     * @throws SionException
     */
    @Override
    public RespuestaActReimpTicketDto confirmarReimpresionTicket(PeticionActReimpTicketDto _peticion) throws SionException {
        RespuestaActReimpTicketDto respuesta = new RespuestaActReimpTicketDto();
        try {
            respuesta = validarPeticionConfirmacionReimpresionTicketDto(_peticion);
            if (respuesta.getPaCdgError() != 0) {
                return respuesta;
            } else {
                PeticionActReimpTicketBean peticionBean = peticionConfirmacionReimpDtoaBean(_peticion);
                SION.log(Modulo.VENTA, "Enviando peticion de confirmacion de reimpresin de ticket con UID:" + peticionBean.getUId(), Level.INFO);
                RespuestaActReimpTicketBean repuestaBean = stub.confirmarReimpresionTicket(peticionBean);
                SION.log(Modulo.VENTA, "Recibiendo respuesta de peticion de confirmacion de reimpresion de ticket:" + repuestaBean.toString(), Level.INFO);
                respuesta = respuestaConfirmacionReimpTicketBeanaDto(repuestaBean);
            }
        } catch (AxisFault ex) {
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.VENTA, ex, ex.getDetail().toString());
            } else {
                SION.logearExcepcion(Modulo.VENTA, ex);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex);
            throw new SionException(ex);
        }
        return respuesta;
    }

    /**
     * Realiza la validacion de los datos de una peticion confirmacion de
     * reimpresion de ticket..
     *
     * @param _peticion
     * @return Un objeto con el resultado de la validacion de la peticion.
     */
    private RespuestaActReimpTicketDto validarPeticionConfirmacionReimpresionTicketDto(PeticionActReimpTicketDto _peticion) {
        RespuestaActReimpTicketDto respuesta = new RespuestaActReimpTicketDto();

        if (!validarLongitud(_peticion.getTiendaId(), 10)) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tienda.longitud.error.mensaje"));
            return respuesta;
        }

        if (_peticion.getPaPaisId() == 0) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "paiserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiendaId() == 0) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaMovimientoId() == 0) {
            respuesta.setPaCdgError(MOV_ID);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "movimientoiderror.mensaje"));
            return respuesta;
        }
        return respuesta;
    }

    /**
     * Realiza la validacion de los datos de una peticion de Venta.
     *
     * @param _peticion
     * @return Un objeto con el resultado de la validacion de la peticion.
     */
    private RespuestaVentaDto validarPeticionVentaDto(PeticionVentaDto _peticion) {
        RespuestaVentaDto respuesta = new RespuestaVentaDto();
        double monto = 0.0;
        for (TipoPagoDto tipo : _peticion.getTiposPago()) {
            if (tipo != null) {
                monto = monto + tipo.getFnMontoPago();
            }
        }
        if (monto == 0.0) {
            respuesta.setPaCdgError(MONTOVENTA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "montoventaerror.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getTiendaId(), 10)) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tienda.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaUsuarioId(), 18)) {
            respuesta.setPaCdgError(USUARIO_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuario.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaPaisId(), 3)) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "pais.longitud.error.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaTerminal() == null || _peticion.getPaTerminal().trim().equals("")) {
            respuesta.setPaCdgError(TERMINAL_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "terminalformatoerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaFechaOper() == null || _peticion.getPaFechaOper().trim().equals("")) {
            respuesta.setPaCdgError(FECHAOPERACION_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "fechaoperacionerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getArticulos() == null || _peticion.getArticulos().length == 0) {
            respuesta.setPaCdgError(ARTICULOS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "articuloserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiposPago() == null || _peticion.getTiposPago().length == 0) {
            respuesta.setPaCdgError(PAGOS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "pagoserror.mensaje"));
            return respuesta;
        }

        if (_peticion.getPaPaisId() == 0) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "paiserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiendaId() == 0) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaUsuarioId() == 0) {
            respuesta.setPaCdgError(USUARIO_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuarioerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaTipoMovto() == 0 || _peticion.getPaTipoMovto() != 1) {
            respuesta.setPaCdgError(TIPOMOV_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tipomovimientoerror.mensaje"));
            return respuesta;
        }
        return respuesta;
    }
    
    public RespuestaVentaDto validarPeticionFinalizaVentaDto(PeticionVentaDto _peticion) {
        RespuestaVentaDto respuesta = new RespuestaVentaDto();
        double monto = 0.0;
        for (TipoPagoDto tipo : _peticion.getTiposPago()) {
            if (tipo != null) {
                monto = monto + tipo.getFnMontoPago();
            }
        }
        if (monto == 0.0) {
            respuesta.setPaCdgError(MONTOVENTA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "montoventaerror.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getTiendaId(), 10)) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tienda.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaUsuarioId(), 18)) {
            respuesta.setPaCdgError(USUARIO_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuario.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaPaisId(), 3)) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "pais.longitud.error.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaTerminal() == null || _peticion.getPaTerminal().trim().equals("")) {
            respuesta.setPaCdgError(TERMINAL_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "terminalformatoerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaFechaOper() == null || _peticion.getPaFechaOper().trim().equals("")) {
            respuesta.setPaCdgError(FECHAOPERACION_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "fechaoperacionerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getArticulos() == null || _peticion.getArticulos().length == 0) {
            respuesta.setPaCdgError(ARTICULOS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "articuloserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiposPago() == null || _peticion.getTiposPago().length == 0) {
            respuesta.setPaCdgError(PAGOS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "pagoserror.mensaje"));
            return respuesta;
        }

        if (_peticion.getPaPaisId() == 0) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "paiserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiendaId() == 0) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaUsuarioId() == 0) {
            respuesta.setPaCdgError(USUARIO_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuarioerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaTipoMovto() == 0 || _peticion.getPaTipoMovto() != 1) {
            respuesta.setPaCdgError(TIPOMOV_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tipomovimientoerror.mensaje"));
            return respuesta;
        }
        return respuesta;
    }

    /**
     * Realiza la validacion de los datos de una peticion de Venta.
     *
     * @param _peticion
     * @return Un objeto con el resultado de la validacion de la peticion.
     */
    private RespuestaVentaMixtaDto validarPeticionVentaMixtaDto(PeticionVentaMixtaDto _peticion) {
        RespuestaVentaMixtaDto respuesta = new RespuestaVentaMixtaDto();
        double monto = 0.0;
        for (TipoPagoDto tipo : _peticion.getTiposPago()) {
            if (tipo != null) {
                monto = monto + tipo.getFnMontoPago();
            }
        }
        if (monto == 0.0) {
            respuesta.setPaCdgError(MONTOVENTA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "montoventaerror.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getTiendaId(), 10)) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tienda.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaUsuarioId(), 18)) {
            respuesta.setPaCdgError(USUARIO_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuario.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaPaisId(), 3)) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "pais.longitud.error.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaTerminal() == null || _peticion.getPaTerminal().trim().equals("")) {
            respuesta.setPaCdgError(TERMINAL_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "terminalformatoerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaFechaOper() == null || _peticion.getPaFechaOper().trim().equals("")) {
            respuesta.setPaCdgError(FECHAOPERACION_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "fechaoperacionerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getArticulos() == null || _peticion.getArticulos().length == 0) {
            respuesta.setPaCdgError(ARTICULOS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "articuloserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiposPago() == null || _peticion.getTiposPago().length == 0) {
            respuesta.setPaCdgError(PAGOS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "pagoserror.mensaje"));
            return respuesta;
        }

        if (_peticion.getPaPaisId() == 0) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "paiserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiendaId() == 0) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaUsuarioId() == 0) {
            respuesta.setPaCdgError(USUARIO_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuarioerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaTipoMovto() == 0 || _peticion.getPaTipoMovto() != 1) {
            respuesta.setPaCdgError(TIPOMOV_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tipomovimientoerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getRecargasTA() != null && _peticion.getRecargasTA().length <= 0) {
            respuesta.setPaCdgError(TIPOMOV_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "arreglo.ventata.mensaje"));
            return respuesta;
        }
        return respuesta;
    }

    /**
     * *
     * Realiza la validacion de los datos de una peticion de detalle de venta.
     *
     * @param _peticion
     * @return Un objeto con el resultado de la validacion de los datos.
     */
    private RespuestaDetalleVentaDto validarPeticionDetalleVentaDto(PeticionDetalleVentaDto _peticion) {
        RespuestaDetalleVentaDto respuesta = new RespuestaDetalleVentaDto();
        if (!validarLongitud(_peticion.getTiendaId(), 10)) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tienda.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaNumTransaccion(), 18)) {
            respuesta.setPaCdgError(NUMTRANSACCION_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "numtransaccion.longitud.error.mensaje"));
        }
        if (_peticion.getPaNumTransaccion() == null || _peticion.getPaNumTransaccion().equals("")) {
            respuesta.setPaCdgError(NUMTRANSACCION_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "numtransaccionerror.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaPaisId(), 3)) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "pais.longitud.error.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaPaisId() == 0) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "paiserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiendaId() == 0) {
            respuesta.setPaCdgError(VENTA_TA);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.mensaje"));
            return respuesta;
        }
        return respuesta;
    }

    /**
     * *
     * Realiza la validacion de los datos de una peticion de devolucion.
     *
     * @param _peticion
     * @return Un objeto con el resultado de la validacion de los datos.
     */
    private RespuestaVentaDto validarPeticionDevolucionDto(PeticionDevolucionDto _peticion) {
        RespuestaVentaDto respuesta = new RespuestaVentaDto();
        if (!validarLongitud(_peticion.getTiendaId(), 10)) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tienda.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaPaisId(), 3)) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "pais.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaUsuarioId(), 18)) {
            respuesta.setPaCdgError(USUARIO_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuario.longitud.error.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaTerminal() == null || _peticion.getPaTerminal().trim().equals("")) {
            respuesta.setPaCdgError(TERMINAL_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "terminalformatoerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaFechaOper() == null || _peticion.getPaFechaOper().trim().equals("")) {
            respuesta.setPaCdgError(FECHAOPERACION_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "fechaoperacionerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getArticulos() == null || _peticion.getArticulos().length == 0) {
            respuesta.setPaCdgError(ARTICULOS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "articuloserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaMontoTotalVta() == 0.0) {
            respuesta.setPaCdgError(MONTOVENTA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "montoventaerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaPaisId() == 0) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "paiserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiendaId() == 0) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaUsuarioId() == 0) {
            respuesta.setPaCdgError(USUARIO_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuarioerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaTipoMovto() != 3 && _peticion.getPaTipoMovto() != 4) {
            respuesta.setPaCdgError(TIPOMOV_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tipomovimientoerror.mensaje"));
            return respuesta;
        }
        return respuesta;
    }

    /**
     * *
     * Validacion de una peticion de datos de bloqueo de venta.
     *
     * @param _peticion
     * @return Resultado de la validacion de la peticion.
     */
    private RespuestaBloqueoVentaDto validarPeticionBloqueo(PeticionBloqueoDto _peticion) {
        RespuestaBloqueoVentaDto respuesta = new RespuestaBloqueoVentaDto();
        if (!validarLongitud(_peticion.getTiendaId(), 10)) {
            respuesta.setCdgError(TIENDA_ERROR);
            respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "tienda.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaPaisId(), 3)) {
            respuesta.setCdgError(PAIS_ERROR);
            respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "pais.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaUsuarioId(), 18)) {
            respuesta.setCdgError(USUARIO_ERROR);
            respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "usuario.longitud.error.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaPaisId() == 0) {
            respuesta.setCdgError(PAIS_ERROR);
            respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "paiserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiendaId() == 0) {
            respuesta.setCdgError(TIENDA_ERROR);
            respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaUsuarioId() == 0) {
            respuesta.setCdgError(USUARIO_ERROR);
            respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA, "usuarioerror.mensaje"));
            return respuesta;
        }
        return respuesta;
    }

    /**
     * *
     * Validacion de una peticion de venta de T.A
     *
     * @param _peticion
     * @return El resultado de la validacion de la peticion.
     */
    private RespuestaVentaDto validarPeticionVentaTA(PeticionVentaTADto _peticion) {
        RespuestaVentaDto respuesta = new RespuestaVentaDto();
        if (!validarLongitud(_peticion.getTiendaId(), 10)) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tienda.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaPaisId(), 3)) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "pais.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaUsuarioId(), 18)) {
            respuesta.setPaCdgError(USUARIO_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuario.longitud.error.mensaje"));
            return respuesta;
        }
        if (_peticion.getCompaniaTelefonicaId() > MAXTELID || _peticion.getCompaniaTelefonicaId() < MINTELID) {
            respuesta.setPaCdgError(COMPANIATEL_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "companiatelefonicaerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaPaisId() == 0) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "paiserror.mensaje"));
            return respuesta;
        }
        if (!(_peticion.getPaTerminal().contains("|"))) {
            respuesta.setPaCdgError(TERMINAL_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "terminalformatoerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaTipoMovto() != 2) {
            respuesta.setPaCdgError(TIPOMOV_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tipomovimientoerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getArticulos() == null || _peticion.getArticulos().length == 0) {
            respuesta.setPaCdgError(ARTICULOS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "articuloserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTelefono() == 0) {
            respuesta.setPaCdgError(NUMTEL_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuarioerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaUsuarioId() == 0) {
            respuesta.setPaCdgError(USUARIO_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuarioerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiendaId() == 0) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.mensaje"));
            return respuesta;
        }
        return respuesta;
    }

    /**
     * *
     * Metodo que valida la peticion de reimpresion de un ticket
     *
     * @param _peticion
     * @return Un objeto con el resultado de la validacion.
     */
    private ReimpresionTicketDto validarPeticionReimpresionTicket(PeticionReimpresionDto _peticion) {
        ReimpresionTicketDto respuesta = new ReimpresionTicketDto();
        SION.log(Modulo.VENTA, "Logeo de peticion:" + _peticion, Level.INFO);
        if (!validarLongitud(_peticion.getTiendaId(), 10)) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tienda.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaPaisId(), 3)) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "pais.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaNumTransaccion(), 18)) {
            respuesta.setPaCdgError(NUMTRANSACCION_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "numtransaccion.longitud.error.mensaje"));
        }
        if (_peticion.getPaNumTransaccion() == null || _peticion.getPaNumTransaccion().equals("")) {
            respuesta.setPaCdgError(NUMTRANSACCION_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "numtransaccionerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaPaisId() == 0) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "paiserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiendaId() == 0) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.mensaje"));
            return respuesta;
        }
        return respuesta;
    }

    /**
     * *
     * Metodo que valida la peticion de actualizacion de reimpresion de vouchers
     *
     * @param _peticion
     * @return Un objeto con el resultado de la validacion.
     */
    private RespuestaActReimpVoucherDto validarPeticionActualizacionReimpVoucher(PeticionReimpresionDto _peticion) {
        RespuestaActReimpVoucherDto respuesta = new RespuestaActReimpVoucherDto();
        if (!validarLongitud(_peticion.getTiendaId(), 10)) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tienda.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaPaisId(), 3)) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "pais.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaNumTransaccion(), 18)) {
            respuesta.setPaCdgError(NUMTRANSACCION_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "numtransaccion.longitud.error.mensaje"));
        }
        if (_peticion.getPaNumTransaccion() == null || _peticion.getPaNumTransaccion().equals("")) {
            respuesta.setPaCdgError(NUMTRANSACCION_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "numtransaccionerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaPaisId() == 0) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "paiserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiendaId() == 0) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaArrReimpresiones() == null || _peticion.getPaArrReimpresiones().length == 0) {
            respuesta.setPaCdgError(VOUCHER_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "arreglovouchererror.mensaje"));
            return respuesta;
        }
        return respuesta;
    }

    /**
     * *
     * Metodo que valida la consulta de transacccion a central
     *
     * @param _peticion
     * @return Un objeto con el resultado de la validacion.
     */
    private RespuestaConsultaTransaccionCentralDto validarConsultaTransaccionCentral(PeticionTransaccionCentralDto _peticion) {
        RespuestaConsultaTransaccionCentralDto respuesta = new RespuestaConsultaTransaccionCentralDto();
        if (!validarLongitud(_peticion.getTiendaId(), 10)) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tienda.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaPaisId(), 3)) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "pais.longitud.error.mensaje"));
            return respuesta;
        }
        if (!validarLongitud(_peticion.getPaUsuarioId(), 18)) {
            respuesta.setPaCdgError(USUARIO_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuario.longitud.error.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaConciliacionId() == 0) {
            respuesta.setPaCdgError(CONCILIACIONID_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "conciliacionerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaPaisId() == 0) {
            respuesta.setPaCdgError(PAIS_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "paiserror.mensaje"));
            return respuesta;
        }
        if (_peticion.getTiendaId() == 0) {
            respuesta.setPaCdgError(TIENDA_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "tiendaerror.mensaje"));
            return respuesta;
        }
        if (_peticion.getPaUsuarioId() == 0) {
            respuesta.setPaCdgError(USUARIO_ERROR);
            respuesta.setPaDescError(SION.obtenerParametro(Modulo.VENTA, "usuarioerror.mensaje"));
            return respuesta;
        }
        return respuesta;
    }

    /**
     * *
     * Acoplamiento de una respuesta Bean entregada por el WS a una respuesta
     * Dto.
     *
     * @param _respuesta
     * @return Un objeto de respuesta del tipo esperado por el front.
     */
    private RespuestaVentaDto respuestaBeanaDto(VentaSeparacionServiceStub.RespuestaVentaBean _respuesta) {
        RespuestaVentaDto respuesta = new RespuestaVentaDto();
        respuesta.setPaCdgError(_respuesta.getPaCdgError());
        respuesta.setPaDescError(_respuesta.getPaDescError());
        respuesta.setPaTransaccionId(_respuesta.getPaTransaccionId());
        respuesta.setPaTaNumOperacion(_respuesta.getPaTaNumOperacion());
        respuesta.setPaTypCursorBlqs(bloqueoMensajeBeanaDto(_respuesta.getPaTypCursorBlqs()));
        return respuesta;
    }
    
    /**
     * *
     * Acoplamiento de una respuesta Bean entregada por el WS a una respuesta
     * Dto.
     *
     * @param _respuesta
     * @return Un objeto de respuesta del tipo esperado por el front.
     */
    private RespuestaVentaDto respuestaBeanaDtoAnt (VentaServiceStub.RespuestaVentaBean _respuesta) {
        RespuestaVentaDto respuesta = new RespuestaVentaDto();
        respuesta.setPaCdgError(_respuesta.getPaCdgError());
        respuesta.setPaDescError(_respuesta.getPaDescError());
        respuesta.setPaTransaccionId(_respuesta.getPaTransaccionId());
        respuesta.setPaTaNumOperacion(_respuesta.getPaTaNumOperacion());
        respuesta.setPaTypCursorBlqs(bloqueoMensajeBeanaDtoAnt(_respuesta.getPaTypCursorBlqs()));
        return respuesta;
    }

    /**
     * *
     * Acoplamiento entre articulos Dto y Bean.
     *
     * @param articulosDto
     * @return Arreglo de articulos de tipo Bean.
     */
    private ArticuloBean[] articulosDtoaBean(ArticuloDto[] articulosDto) {
        if (articulosDto != null) {
            ArticuloBean[] articulos = new ArticuloBean[articulosDto.length];
            int contador = 0;
            for (ArticuloDto dto : articulosDto) {
                if (dto != null) {
                    ArticuloBean articulo = new ArticuloBean();
                    articulo.setFiArticuloId(dto.getFiArticuloId());
                    articulo.setFcCdGbBarras(dto.getFcCdGbBarras());
                    articulo.setFnCantidad(dto.getFnCantidad());
                    articulo.setFnCosto(dto.getFnCosto());
                    articulo.setFnDescuento(dto.getFnDescuento());
                    articulo.setFnIva(dto.getFnIva());
                    articulo.setFnPrecio(dto.getFnPrecio());
                    articulos[contador] = articulo;
                    contador++;
                } else {
                    return null;
                }
            }
            return articulos;
        } else {
            return null;
        }
    }

    /**
     * *
     * Acomplamiento entre los tipo de Peticion de detalle , el generado por el
     * front hacia el esperado por el WS.
     *
     * @param _peticion
     * @return Un objeto de Peticion de detalle de venta tipo Bean.
     */
    private PeticionDetalleVentaBean peticionDetalleVentaDtoaBean(PeticionDetalleVentaDto _peticion) {
        PeticionDetalleVentaBean peticion = new PeticionDetalleVentaBean();
        peticion.setPaPaisId(_peticion.getPaPaisId());
        peticion.setPaNumTransaccion(_peticion.getPaNumTransaccion());
        peticion.setTiendaId(_peticion.getTiendaId());
        peticion.setPaParamTipoTrans(_peticion.getPaParamTipoTrans());
        peticion.setUId(_peticion.getUId());
        peticion.setIpTerminal(_peticion.getIpTerminal());
        return peticion;
    }

    /**
     * *
     * Acomplamiento entre la respuesta del detalle de una venta generado por el
     * WS a Dto.
     *
     * @param _respuesta
     * @return Un objeto con la respuesta de los detalles de una venta tipo Dto.
     */
    private RespuestaDetalleVentaDto respuestaDetalleVentaBeanaDto(RespuestaConsultaVentaBean _respuesta) {
        RespuestaDetalleVentaDto respuesta = new RespuestaDetalleVentaDto();
        respuesta.setPaMovimientoId(_respuesta.getPaMovimientoId());
        respuesta.setPaCdgError(_respuesta.getPaCdgError());
        respuesta.setPaCurDetalleVenta(articulosBeanaDto(_respuesta.getPaCurArticulosVenta()));
        respuesta.setPaDescError(_respuesta.getPaDescError());
        respuesta.setPaMontoDev(_respuesta.getPaMontoDev());
        return respuesta;
    }

    /**
     * *
     * Acoplamiento entre articulos Bean y Dto
     *
     * @param _articulos
     * @return Arreglo de articulos tipo Dto.
     */
    private ArticuloDto[] articulosBeanaDto(ArticuloPeticionVentaBean[] _articulos) {
        if (_articulos != null) {
            ArticuloDto[] articulos = new ArticuloDto[_articulos.length];
            int contador = 0;
            for (ArticuloPeticionVentaBean articuloBean : _articulos) {
                if (articuloBean != null) {
                    ArticuloDto articuloDto = new ArticuloDto(articuloBean.getFiArticuloId(),
                            articuloBean.getFcCdGbBarras(), articuloBean.getFnCantidad(),
                            articuloBean.getFnPrecio(), articuloBean.getFnCosto(),
                            articuloBean.getFnDescuento(), articuloBean.getFnIva(),
                            articuloBean.getFcNombreArticulo(), articuloBean.getFiAgranel(),
                            articuloBean.getFiIepsId(), 
                            0 //No es reelevante para el detalle 
                            );
                    articulos[contador] = articuloDto;
                    contador++;
                } else {
                    return null;
                }
            }
            return articulos;
        } else {
            return null;
        }
    }

    /**
     * *
     * Acoplamiento entre un arreglo de tipo BloqueoBean a Dto.
     *
     * @param _respuesta
     * @return Un arreglo de objetos tipo BloqueoDto
     */
    private RespuestaBloqueoVentaDto respuestaBloqueoVentaBeanaDto(VentaSeparacionServiceStub.BloqueoBean[] _respuesta) {
        RespuestaBloqueoVentaDto respuesta = new RespuestaBloqueoVentaDto();
        if (_respuesta != null) {
            BloqueoDto[] bloqueoDto = bloqueoMensajeBeanaDto(_respuesta);
            if (bloqueoDto != null) {
                respuesta.setMensajesBloqueo(bloqueoDto);
                respuesta.setCdgError(0);
                return respuesta;
            } else {
                respuesta.setCdgError(-1);
                return respuesta;
            }
        } else {
            respuesta.setCdgError(-1);
            return respuesta;
        }

    }
    
    /**
     * *
     * Acoplamiento entre un arreglo de tipo BloqueoBean a Dto.
     *
     * @param _respuesta
     * @return Un arreglo de objetos tipo BloqueoDto
     */
    private RespuestaBloqueoVentaDto respuestaBloqueoVentaBeanaDtoAnt(VentaServiceStub.BloqueoBean[] _respuesta) {
        RespuestaBloqueoVentaDto respuesta = new RespuestaBloqueoVentaDto();
        if (_respuesta != null) {
            BloqueoDto[] bloqueoDto = bloqueoMensajeBeanaDtoAnt(_respuesta);
            if (bloqueoDto != null) {
                respuesta.setMensajesBloqueo(bloqueoDto);
                respuesta.setCdgError(0);
                return respuesta;
            } else {
                respuesta.setCdgError(-1);
                return respuesta;
            }
        } else {
            respuesta.setCdgError(-1);
            return respuesta;
        }

    }

    /**
     * *
     * Acomplamiento entre objetos BloqueoMensajeDto a Bean
     *
     * @param _mensajes
     * @return Un arreglo de BloqueoMensajeDto de tipo Dto
     */
    private BloqueoDto[] bloqueoMensajeBeanaDto( VentaSeparacionServiceStub.BloqueoBean[] _mensajes) {
        if (_mensajes != null) {
            BloqueoDto[] respuesta = new BloqueoDto[_mensajes.length];

            int contador = 0;
            for (VentaSeparacionServiceStub.BloqueoBean mensajeBean : _mensajes) {
                if (mensajeBean != null) {
                    BloqueoDto mensajeDto = new BloqueoDto();
                    mensajeDto.setFiEstatusBloqueoId(mensajeBean.getFiEstatusBloqueoId());
                    mensajeDto.setFiNumAvisos(mensajeBean.getFiNumAvisos());
                    mensajeDto.setFiTipoPagoId(mensajeBean.getFiTipoPagoId());
                    mensajeDto.setFiAvisosFalt(mensajeBean.getFiAvisosFalt());
                    respuesta[contador] = mensajeDto;
                    contador++;
                } else {
                    return null;
                }

            }
            return respuesta;
        } else {
            return null;
        }
    }
    
    /**
     * *
     * Acomplamiento entre objetos BloqueoMensajeDto a Bean
     *
     * @param _mensajes
     * @return Un arreglo de BloqueoMensajeDto de tipo Dto
     */
    private BloqueoDto[] bloqueoMensajeBeanaDtoAnt( VentaServiceStub.BloqueoBean[] _mensajes) {
        if (_mensajes != null) {
            BloqueoDto[] respuesta = new BloqueoDto[_mensajes.length];

            int contador = 0;
            for (VentaServiceStub.BloqueoBean mensajeBean : _mensajes) {
                if (mensajeBean != null) {
                    BloqueoDto mensajeDto = new BloqueoDto();
                    mensajeDto.setFiEstatusBloqueoId(mensajeBean.getFiEstatusBloqueoId());
                    mensajeDto.setFiNumAvisos(mensajeBean.getFiNumAvisos());
                    mensajeDto.setFiTipoPagoId(mensajeBean.getFiTipoPagoId());
                    mensajeDto.setFiAvisosFalt(mensajeBean.getFiAvisosFalt());
                    respuesta[contador] = mensajeDto;
                    contador++;
                } else {
                    return null;
                }

            }
            return respuesta;
        } else {
            return null;
        }
    }

    /**
     * *
     * Acoplamiento entre objetos PeticionBloqueoDto y Bean
     *
     * @param _peticionDto
     * @return
     */
    private PeticionBloqueoBean peticionBloqueoDtoaBean(PeticionBloqueoDto _peticionDto) {
        PeticionBloqueoBean peticion = new PeticionBloqueoBean();
        peticion.setPaPaisId(_peticionDto.getPaPaisId());
        peticion.setTiendaId(_peticionDto.getTiendaId());
        peticion.setPaUsuarioId(_peticionDto.getPaUsuarioId());
        peticion.setUId(_peticionDto.getUId());
        peticion.setIpTerminal(_peticionDto.getIpTerminal());
        return peticion;
    }

    /**
     * *
     * Acomplamiento entre los tipos de objeto PeticionReimpresionDto y
     * PeticionReimpresionTicketBean.
     *
     * @param _peticion
     * @return Un objeto de tipo PeticionReimpresionTicketBean.
     */
    private PeticionReimpresionBean peticionReimpresionTicketDtoaBean(PeticionReimpresionDto _peticion) {
        PeticionReimpresionBean peticion = new PeticionReimpresionBean();
        peticion.setPaPaisId(_peticion.getPaPaisId());
        peticion.setTiendaId(_peticion.getTiendaId());
        peticion.setPaNumTransaccion(_peticion.getPaNumTransaccion());
        peticion.setPaArrReimpresiones(reimpresionDtoaBean(_peticion.getPaArrReimpresiones()));
        peticion.setPaMovimientoId(_peticion.getPaMovimientoId());
        peticion.setUId(_peticion.getUId());
        peticion.setIpTerminal(_peticion.getIpTerminal());
        return peticion;
    }

    /**
     * *
     * Acoplamineto entre los Arreglos de objetos de tipo ReimpresionDto y
     * ReimpresionBean
     *
     * @param arregloDto
     * @return Un arreglo de objetos de tip ReimpresionBean
     */
    private ReimpresionBean[] reimpresionDtoaBean(ReimpresionDto[] arregloDto) {
        if (arregloDto != null) {
            ReimpresionBean[] arregloBean = new ReimpresionBean[arregloDto.length];
            int cont = 0;
            for (ReimpresionDto dto : arregloDto) {
                if (dto != null) {
                    ReimpresionBean bean = new ReimpresionBean();
                    bean.setFiPagoTarjeta(dto.getFiPagoTarjeta());
                    arregloBean[cont] = bean;
                    cont++;
                } else {
                    return null;
                }
            }
            return arregloBean;
        } else {
            return null;
        }
    }

    /**
     * *
     * Acoplamiento entre RespuestaReimpresionTicketBean y ReimpresionTicketDto
     *
     * @param _respuesta
     * @return Un objeto de tipo ReimpresionTicketDto.
     */
    /*
     * private ReimpresionTicketDto
     * respuestaReimpresionTicketBeanaDto(RespuestaReimpresionTicketBean
     * _respuesta) { ReimpresionTicketDto respuesta = new
     * ReimpresionTicketDto();
     * respuesta.setPaCdgError(_respuesta.getPaCdgError());
     * respuesta.setPaDescError(_respuesta.getPaDescError());
     * respuesta.setUsuarioId(_respuesta.getUsuarioId());
     * respuesta.setNombreUsuario(_respuesta.getNombreUsuario());
     * respuesta.setPaFechaCreacion(_respuesta.getPaFechaCreacion());
     * respuesta.setPaEstacionTrabajo(_respuesta.getPaEstacionTrabajo());
     * respuesta.setPaMovimientoId(_respuesta.getPaMovimientoId()); if
     * (respuesta.getPaCdgError() == 0) {
     * respuesta.setPaArtReimpresiones(articulosBeanaDto(_respuesta.getPaArtReimpresiones()));
     * respuesta.setPaTPReimpresiones(tipoPagoBeanaDto(_respuesta.getPaTPReimpresiones()));
     * } return respuesta; }
     */
    /**
     * *
     * Acoplamiento entre el arreglo de objetos de tipo TipoPagoBean y
     * TipoPagoDto
     *
     * @param tiposBean
     * @return Un objeto de tipo TipoPagoDto.
     */
    private TipoPagoDto[] tipoPagoBeanaDto(TipoPagoBean[] tiposBean) {
        TipoPagoDto[] tiposDto = new TipoPagoDto[tiposBean.length];
        int cont = 0;
        for (TipoPagoBean bean : tiposBean) {
            TipoPagoDto dto = new TipoPagoDto();
            dto.setFiTipoPagoId(bean.getFiTipoPagoId());
            dto.setFnMontoPago(bean.getFnMontoPago());
            dto.setFiMontoRecibido(bean.getImporteAdicional());
            tiposDto[cont] = dto;
            cont++;
        }
        return tiposDto;
    }

    /**
     * *
     * Acomplamiento entre el arreglo de objetos de tipo ReimpresionTarjetaBean
     * y ReimpresionTarjetaDto
     *
     * @param _tarjetasBean
     * @return Un objeto de tipo ReimpresionTarjetaDto.
     */
    /*
     * private ReimpresionTarjetaDto[]
     * reimpresionTarjetaBeanaDto(ReimpresionTarjetaBean[] _tarjetasBean) {
     * ReimpresionTarjetaDto[] tarjetasDto = new
     * ReimpresionTarjetaDto[_tarjetasBean.length]; int cont = 0; for
     * (ReimpresionTarjetaBean bean : _tarjetasBean) { if (bean != null) {
     * ReimpresionTarjetaDto dto = new ReimpresionTarjetaDto();
     * dto.setFcNumTarjeta(bean.getFcNumTarjeta());
     * dto.setFcNumeroAutorizacion(bean.getFcNumeroAutorizacion());
     * dto.setFiAfiliacion(bean.getFiAfiliacion());
     * dto.setFiPagoTarjetasId(bean.getFiPagoTarjetasId());
     * dto.setImporteComision(bean.getImporteComision());
     * dto.setTipoTarjeta(bean.getTipoTarjeta()); tarjetasDto[cont] = dto;
     * cont++; } else { return null; }
     *
     * }
     * return tarjetasDto; }
     */
    /**
     * *
     * Acoplamiento entre RespuestaReimpresionVoucherBean y
     * ReimpresionVoucherDto
     *
     * @param _respuesta
     * @return Un objeto de tipo ReimpresionVoucherDto.
     */
    /*
     * private ReimpresionVoucherDto
     * respuestaReimpresionVoucherBeanaDto(RespuestaReimpresionVoucherBean
     * _respuesta) { ReimpresionVoucherDto respuesta = new
     * ReimpresionVoucherDto(); respuesta.setCdgError(_respuesta.getCdgError());
     * respuesta.setDescError(_respuesta.getDescError());
     * respuesta.setPaUsuarioId(_respuesta.getPaUsuarioId());
     * respuesta.setPaFechaCreacion(_respuesta.getPaFechaCreacion());
     * respuesta.setPaEstacionTrabajo(_respuesta.getPaEstacionTrabajo()); if
     * (_respuesta.getCdgError() == 0) { if (_respuesta.getPaReimpresionesTar()
     * != null && _respuesta.getPaReimpresionesTar().length > 0) {
     * ReimpresionTarjetaDto[] paReimpresionesTar =
     * reimpresionTarjetaBeanaDto(_respuesta.getPaReimpresionesTar()); if
     * (paReimpresionesTar != null) {
     * respuesta.setPaReimpresionesTar(paReimpresionesTar); } else {
     * respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA,
     * "voucher.limite.mensaje"));
     * respuesta.setCdgError(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA,
     * "voucher.limite.codigo"))); } } else {
     * respuesta.setDescError(SION.obtenerParametro(Modulo.VENTA,
     * "voucher.limite.mensaje"));
     * respuesta.setCdgError(Integer.valueOf(SION.obtenerParametro(Modulo.VENTA,
     * "voucher.limite.codigo"))); }
     *
     * }
     * return respuesta; }
     */
    /**
     * *
     * Acoplamiento entre RespuestaActReimpVoucherBean y
     * RespuestaActReimpVoucherDto
     *
     * @param _respuesta
     * @return Un objeto de tipo RespuestaActReimpVoucherDto.
     */
    private RespuestaActReimpVoucherDto respuestaActualizacionVoucherBeanaDto(RespuestaActReimpVoucherBean _respuesta) {
        RespuestaActReimpVoucherDto respuesta = new RespuestaActReimpVoucherDto();
        respuesta.setPaCdgError(_respuesta.getPaCdgError());
        respuesta.setPaDescError(_respuesta.getPaDescError());
        return respuesta;
    }

    /**
     * *
     * Acomplamiento entre los tipos de objeto PeticionReimpresionDto y
     * PeticionReimpresionTicketBean.
     *
     * @param _peticion
     * @return Un objeto de tipo PeticionReimpresionTicketBean.
     */
    private PeticionTransaccionCentralBean peticionConsultaTransaccionDtoaBean(PeticionTransaccionCentralDto _peticion) {
        PeticionTransaccionCentralBean peticion = new PeticionTransaccionCentralBean();
        peticion.setPaPaisId(_peticion.getPaPaisId());
        peticion.setTiendaId(_peticion.getTiendaId());
        peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
        peticion.setPaUsuarioId(_peticion.getPaUsuarioId());
        peticion.setPaSistemaId(_peticion.getPaSistemaId());
        peticion.setPaModuloId(_peticion.getPaModuloId());
        peticion.setPaSubModuloId(_peticion.getPaSubModuloId());
        peticion.setUId(_peticion.getUId());
        peticion.setIpTerminal(_peticion.getIpTerminal());
        return peticion;
    }

    /**
     * *
     * Acoplamiento entre los objetos PeticionDevolucionDto y
     * PeticionDevolucionBean.
     *
     * @param _peticion
     * @return Un objeto de tipo PeticionDevolucionBean.
     */
    private PeticionDevolucionArticulosBean peticionDevolucionDtoaBean(PeticionDevolucionDto _peticion) {
        PeticionDevolucionArticulosBean peticion = new PeticionDevolucionArticulosBean();
        peticion.setArticulos(utilerias.articulosDtoaBeanAnt(_peticion.getArticulos()));
        peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
        peticion.setPaFechaOper(_peticion.getPaFechaOper());
        peticion.setPaMontoTotalVta(_peticion.getPaMontoTotalVta());
        peticion.setPaMovimientoIdDev(_peticion.getPaMovimientoIdDev());
        peticion.setPaPaisId(_peticion.getPaPaisId());
        peticion.setPaTerminal(_peticion.getPaTerminal());
        peticion.setTiendaId(_peticion.getTiendaId());
        peticion.setPaTipoDevolucion(_peticion.getPaTipoDevolucion());
        peticion.setPaTipoMovto(_peticion.getPaTipoMovto());
        peticion.setPaTransaccionDev(_peticion.getPaTransaccionDev());
        peticion.setPaUsuarioId(_peticion.getPaUsuarioId());
        peticion.setPaUsuarioAutorizaId(_peticion.getPaUsuarioAutorizaId());
        peticion.setTiposPago(utilerias.tiposPagoDtoaBeanAnt(_peticion.getTiposPago()));
        peticion.setUId(_peticion.getUId());
        peticion.setIpTerminal(_peticion.getIpTerminal());
        return peticion;
    }

    /**
     * *
     * Acomplamiento entre los objetos de tipo PeticionVentaMixtaDto y
     * PeticionVentaMixtaBean.
     *
     * @param _peticion
     * @return Un objeto de tipo PeticionVentaMixtaBean.
     */
    private PeticionVentaMixtaBean peticionVentaMixtaDtoaBean(PeticionVentaMixtaDto _peticion) {
        PeticionVentaMixtaBean peticion = new VentaServiceStub.PeticionVentaMixtaBean();
        peticion.setArticulos(articulosDtoaBean(_peticion.getArticulos()));
        peticion.setFueraLinea(_peticion.getFueraLinea());
        peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
        peticion.setPaFechaOper(_peticion.getPaFechaOper());
        peticion.setPaMontoTotalVta(_peticion.getPaMontoTotalVta());
        peticion.setPaPaisId(_peticion.getPaPaisId());
        peticion.setPaTerminal(_peticion.getPaTerminal());
        peticion.setPaTipoMovto(_peticion.getPaTipoMovto());
        peticion.setPaUsuarioId(_peticion.getPaUsuarioId());
        peticion.setRecargasTA(peticionesRecargaTADtoaBean(_peticion.getRecargasTA()));
        peticion.setTiendaId(_peticion.getTiendaId());
        peticion.setTiposPago(utilerias.tiposPagoDtoaBeanAnt(_peticion.getTiposPago()));
        peticion.setUId(_peticion.getUId());
        peticion.setIpTerminal(_peticion.getIpTerminal());
        return peticion;
    }

    /**
     * *
     * Acoplamiento entre los objetos PeticionVentaTADto y PeticionVentaTABean.
     *
     * @param _peticion
     * @return Un objeto de tipo PeticionVentaTABean.
     */
    private PeticionVentaTABean peticionVentaTADTOaBean(PeticionVentaTADto _peticion) {
        PeticionVentaTABean peticion = new VentaServiceStub.PeticionVentaTABean();
        peticion.setArticulos(articulosDtoaBean(_peticion.getArticulos()));
        peticion.setCompaniaTelefonicaId(_peticion.getCompaniaTelefonicaId());
        peticion.setFueraLinea(_peticion.getFueraLinea());
        peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
        peticion.setPaFechaOper(_peticion.getPaFechaOper());
        peticion.setPaMontoTotalVta(_peticion.getPaMontoTotalVta());
        peticion.setPaPaisId(_peticion.getPaPaisId());
        peticion.setPaTerminal(_peticion.getPaTerminal());
        peticion.setTiendaId(_peticion.getTiendaId());
        peticion.setPaTipoMovto(_peticion.getPaTipoMovto());
        peticion.setPaTransaccionDev(_peticion.getPaTransaccionDev());
        peticion.setNumFolio(_peticion.getNumFolio());
        peticion.setPaUsuarioId(_peticion.getPaUsuarioId());
        peticion.setTelefono(_peticion.getTelefono());
        peticion.setTiposPago(utilerias.tiposPagoDtoaBeanAnt(_peticion.getTiposPago()));
        peticion.setUId(_peticion.getUId());
        peticion.setIpTerminal(_peticion.getIpTerminal());
        return peticion;
    }

    /**
     * **
     * Acoplamiento entre los objetos de tipo PeticionVentaTADto y
     * PeticionVentaTABean.
     *
     * @param recargasDto
     * @return PeticionVentaTABean.
     */
    private PeticionVentaTABean[] peticionesRecargaTADtoaBean(PeticionVentaTADto[] recargasDto) {
        PeticionVentaTABean[] recargasBean = new PeticionVentaTABean[recargasDto.length];
        int contador = 0;
        for (PeticionVentaTADto recargaDto : recargasDto) {
            PeticionVentaTABean recargaBean = peticionVentaTADTOaBean(recargaDto);
            recargasBean[contador] = recargaBean;
            contador++;
        }
        return recargasBean;
    }

    /**
     * *
     * Acomplamiento entre los objetos de tipo RespuestaVentaMixtaBean y
     * RespuestaVentaMixtaDto.
     *
     * @param _respuesta
     * @return Ub objeto de tipo RespuestaVentaMixtaBean.
     */
    private RespuestaVentaMixtaDto respuestaVentaMixtaBeanaDto(RespuestaVentaMixtaBean _respuesta) {
        RespuestaVentaMixtaDto respuesta = new RespuestaVentaMixtaDto();
        respuesta.setPaCdgError(_respuesta.getPaCdgError());
        respuesta.setPaDescError(_respuesta.getPaDescError());
        respuesta.setPaTransaccionId(_respuesta.getPaTransaccionId());
        respuesta.setPaTypCursorBlqs(bloqueoMensajeBeanaDtoAnt(_respuesta.getPaTypCursorBlqs()));
        respuesta.setRespuestaRecargasTA(respuestasRecargasBeanaDto(_respuesta.getRespuestaRecargasTA()));
        return respuesta;
    }

    /**
     * *
     * Acoplamiento entre los arreglos de objetos de tipo RespuestaRecargaTABean
     * y RespuestaRecargaTADto.
     *
     * @param _respuestasRecargas
     * @return Un arreglo de objetos de tipo RespuestaRecargaTADto.
     */
    private RespuestaRecargaTADto[] respuestasRecargasBeanaDto(RespuestaRecargaTABean[] _respuestasRecargas) {
        RespuestaRecargaTADto[] respuestasRecargas = new RespuestaRecargaTADto[_respuestasRecargas.length];
        int cont = 0;
        if (_respuestasRecargas != null) {
            for (RespuestaRecargaTABean respRecargaBean : _respuestasRecargas) {
                if (respRecargaBean != null) {
                    RespuestaRecargaTADto respRecargaDto = new RespuestaRecargaTADto();
                    respRecargaDto.setCompaniaId(respRecargaBean.getCompaniaId());
                    respRecargaDto.setNumeroOperacion(respRecargaBean.getNumeroOperacion());
                    respRecargaDto.setNumeroTelefonico(respRecargaBean.getNumeroTelefonico());
                    respRecargaDto.setRecargaExitosa(respRecargaBean.getRecargaExitosa());
                    respuestasRecargas[cont] = respRecargaDto;
                    cont++;
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
        return respuestasRecargas;

    }

    /**
     * **
     * Acoplamiento entre los objetos PeticionActReimpTicketDto y
     * PeticionActReimpTicketBean
     *
     * @param _peticion
     * @return Un objeto de tipo PeticionActReimpTicketBean.
     */
    private PeticionActReimpTicketBean peticionConfirmacionReimpDtoaBean(PeticionActReimpTicketDto _peticion) {
        PeticionActReimpTicketBean peticion = new PeticionActReimpTicketBean();
        peticion.setPaPaisId(_peticion.getPaPaisId());
        peticion.setTiendaId(_peticion.getTiendaId());
        peticion.setPaMovimientoId(_peticion.getPaMovimientoId());
        peticion.setIpTerminal(_peticion.getIpTerminal());
        peticion.setUId(_peticion.getUId());
        return peticion;
    }

    /**
     * **
     * Acoplamiento entre los objetos RespuestaActReimpTicketBean y
     * RespuestaActReimpTicketDto
     *
     * @param _respuesta
     * @return Un objeto de tipo RespuestaActReimpTicketDto
     */
    private RespuestaActReimpTicketDto respuestaConfirmacionReimpTicketBeanaDto(RespuestaActReimpTicketBean _respuesta) {
        RespuestaActReimpTicketDto respuesta = new RespuestaActReimpTicketDto();
        respuesta.setPaCdgError(_respuesta.getPaCdgError());
        respuesta.setPaDescError(_respuesta.getPaDescError());
        return respuesta;
    }

    /**
     * *
     * Metodo que valida la longitud maxima permitida en el campo
     *
     * @param valor
     * @param lontigud
     * @return true o false
     */
    private boolean validarLongitud(long valor, int lontigud) {
        if (String.valueOf(valor).length() > lontigud) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * *
     * Metodo que valida la longitud maxima permitida en el campo
     *
     * @param valor
     * @param lontigud
     * @return true o false
     */
    private boolean validarLongitud(String valor, int lontigud) {
        if (valor.length() > lontigud) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * *
     * Metodo que consulta una tarjeta en BD Central
     *
     * @param peticion de consulta
     * @return un objeto con el detalle de la tarjeta
     */
    @Override
    public RespuestaTarjetaDto consultarTarjeta(PeticionTarjetaDto _peticion) throws SionException {
        RespuestaTarjetaDto respuesta = new RespuestaTarjetaDto();

        try {
            VentaServiceStub.RespuestaTarjetaBean respuestaTarjetaBean = stub.consultarTarjeta(peticionTarjetaDtoaBean(_peticion));
            respuesta = respuestaTarjetaBeanaDto(respuestaTarjetaBean);
        } catch (AxisFault ex) {
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.SION, ex, new String[]{ex.getDetail().toString()});
            } else {
                SION.logearExcepcion(Modulo.SION, ex, new String[0]);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.SION, ex, new String[0]);
            throw new SionException(ex);
        }

        return respuesta;
    }

    private VentaServiceStub.PeticionTarjetaBean peticionTarjetaDtoaBean(PeticionTarjetaDto _peticionTarjetaDto) {
        VentaServiceStub.PeticionTarjetaBean peticionTarjetaBean = new VentaServiceStub.PeticionTarjetaBean();

        peticionTarjetaBean.setFiPaisId(_peticionTarjetaDto.getFiPaisId());
        peticionTarjetaBean.setPaFecha(_peticionTarjetaDto.getPaFecha());
        peticionTarjetaBean.setPaNumTarjeta(_peticionTarjetaDto.getPaNumTarjeta());
        peticionTarjetaBean.setPaTiendaId(_peticionTarjetaDto.getPaTiendaId());

        return peticionTarjetaBean;
    }

    private RespuestaTarjetaDto respuestaTarjetaBeanaDto(VentaServiceStub.RespuestaTarjetaBean _respuestaTarjetaBean) {
        RespuestaTarjetaDto respuestaTarjetaDto = new RespuestaTarjetaDto();

        if (_respuestaTarjetaBean != null) {

            PeticionTarjetaDto peticionTarjetaDto = new PeticionTarjetaDto();

            peticionTarjetaDto.setFiPaisId(_respuestaTarjetaBean.getPeticionTarjetaBean().getFiPaisId());
            peticionTarjetaDto.setPaFecha(_respuestaTarjetaBean.getPeticionTarjetaBean().getPaFecha());
            peticionTarjetaDto.setPaNumTarjeta(_respuestaTarjetaBean.getPeticionTarjetaBean().getPaNumTarjeta());
            peticionTarjetaDto.setPaTiendaId(_respuestaTarjetaBean.getPeticionTarjetaBean().getPaTiendaId());

            respuestaTarjetaDto.setCdgError(_respuestaTarjetaBean.getCdgError());
            respuestaTarjetaDto.setDescError(_respuestaTarjetaBean.getDescError());
            respuestaTarjetaDto.setTarjetaDtos(tarjetaBeanaDtos(_respuestaTarjetaBean.getTarjetaBeans()));

        }

        return respuestaTarjetaDto;
    }

    private TarjetaDto[] tarjetaBeanaDtos(VentaServiceStub.TarjetaBean[] _TarjetaBeans) {
        TarjetaDto[] tarjetaDtos = new TarjetaDto[_TarjetaBeans.length];

        if (_TarjetaBeans != null) {
            int cont = 0;
            TarjetaDto[] arregloTipoTarjetasDto = new TarjetaDto[_TarjetaBeans.length];

            for (VentaServiceStub.TarjetaBean bean : _TarjetaBeans) {

                if (bean != null) {

                    TarjetaDto dto = new TarjetaDto();

                    dto.setFiPagoTarjetasId(bean.getFiPagoTarjetasId());
                    dto.setFcEstatus(bean.getFcEstatus());
                    dto.setFcNumeroAutorizacion(bean.getFcNumeroAutorizacion());
                    dto.setFdFechaoperacion(bean.getFdFechaoperacion());
                    dto.setFiNumTransaccion(bean.getFiNumTransaccion());
                    dto.setFnMontoTarjeta(bean.getFnMontoTarjeta());
                    dto.setFiSubmoduloId(bean.getFiSubmoduloId());
                    dto.setFiEstatustarjetaId(bean.getFiEstatustarjetaId());

                    arregloTipoTarjetasDto[cont] = dto;
                    cont++;
                } else {
                    return null;
                }
            }
            return arregloTipoTarjetasDto;
        }
        return null;
    }

    /**
     * Método que consulta las últimas diez transacciones grabadas en central
     *
     * @return un objeto de respuesta con las transacciones
     * @throws SionException
     */
    @Override
    public RespuestaUltimastransaccionesVentaDto consultarUltimasTransacciones(PeticionUltimasTransaccionesDto _peticion) throws SionException {
        RespuestaUltimastransaccionesVentaDto respuesta = new RespuestaUltimastransaccionesVentaDto();

        try {
            VentaServiceStub.RespuestaUltimasTransaccionesVentaBean respuestaVentas = stub.consultarUltimasTransacciones(peticionUltimasTransaccionesDtoaBean(_peticion));
            respuesta = respuestaUltimasTransaccionesVentaBeanaDto(respuestaVentas);
        } catch (AxisFault ex) {
            respuesta = null;
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.SION, ex, new String[]{ex.getDetail().toString()});
            } else {
                SION.logearExcepcion(Modulo.SION, ex, new String[0]);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            respuesta = null;
            SION.logearExcepcion(Modulo.SION, ex, new String[0]);
            throw new SionException(ex);
        }

        return respuesta;
    }

    private PeticionUltimasTransaccionesVentaBean peticionUltimasTransaccionesDtoaBean(PeticionUltimasTransaccionesDto _peticion) {
        if (_peticion != null) {
            PeticionUltimasTransaccionesVentaBean peticionBean = new PeticionUltimasTransaccionesVentaBean();

            peticionBean.setPaPaisId(_peticion.getPaPaisId());
            peticionBean.setPaTiendaId(_peticion.getPaTiendaId());

            return peticionBean;
        } else {
            return null;
        }
    }

    private RespuestaUltimastransaccionesVentaDto respuestaUltimasTransaccionesVentaBeanaDto(
            RespuestaUltimasTransaccionesVentaBean _respuestaBean) {
        RespuestaUltimastransaccionesVentaDto respuestaDto = new RespuestaUltimastransaccionesVentaDto();
        UltimasTransaccionesVentaDto[] arregloUltimasVentasDto = new UltimasTransaccionesVentaDto[_respuestaBean.getTransaccionesVentaBeans().length];
        int contador = 0;

        for (VentaServiceStub.UltimasTransaccionesVentaBean bean : _respuestaBean.getTransaccionesVentaBeans()) {
            UltimasTransaccionesVentaDto dto = new UltimasTransaccionesVentaDto();
            dto.setFdFechaMovimiento(bean.getFdFechaMovimiento());
            dto.setFiNumTransaccion(bean.getFiNumTransaccion());
            dto.setFiSubModuloId(bean.getFiSubModuloId());
            dto.setFiTipoMovtoCajaId(bean.getFiTipoMovtoCajaId());
            arregloUltimasVentasDto[contador] = dto;
            contador++;
        }

        respuestaDto.setUltimasTransaccionesVentaDtos(arregloUltimasVentasDto);

        return respuestaDto;
    }

    private ArticuloTAReimpresionDto[] articuloTAReimpresionBeanaDto(ArticuloTAReimpresionBean[] _articulosTA) {
        ArticuloTAReimpresionDto[] articuloTAReimpresionDtos = new ArticuloTAReimpresionDto[_articulosTA.length];

        int contador = 0;

        for (ArticuloTAReimpresionBean beanTA : _articulosTA) {
            if (beanTA != null) {
                ArticuloTAReimpresionDto dtoTA = new ArticuloTAReimpresionDto();
                dtoTA.setCompania(beanTA.getCompania());
                dtoTA.setFcNombreArticulo(beanTA.getFcNombreArticulo());
                dtoTA.setFiEmpresaId(beanTA.getFiEmpresaId());
                dtoTA.setNumOperacion(beanTA.getNumOperacion());
                dtoTA.setNumReferencia(beanTA.getNumReferencia());
                dtoTA.setNumero(beanTA.getNumero());
                articuloTAReimpresionDtos[contador] = dtoTA;
                contador++;
            }
        }

        return articuloTAReimpresionDtos;
    }

    /*
     * private ArticuloReimpresionDto[]
     * articuloReimpresionBeanaDto(ArticuloReimpresionBean[] _articulos) {
     * ArticuloReimpresionDto[] articuloReimpresionDtos = new
     * ArticuloReimpresionDto[_articulos.length];
     *
     * int contador = 0;
     *
     * for (ArticuloReimpresionBean bean : _articulos) { ArticuloReimpresionDto
     * dto = new ArticuloReimpresionDto();
     * dto.setFcNombreArticulo(bean.getFcNombreArticulo());
     * dto.setFnCantidad(bean.getFnCantidad());
     * dto.setFnDescuento(bean.getFnDescuento()); dto.setFnIva(bean.getFnIva());
     * dto.setFnPrecio(bean.getFnPrecio()); articuloReimpresionDtos[contador] =
     * dto; contador++; }
     *
     * return articuloReimpresionDtos; }
     */
    private TipoPagoReimpresionDto[] tipoPagoReimpresionBeanaDto(TipoPagoReimpresionBean[] _tiposPago) {
        TipoPagoReimpresionDto[] tipoPagoReimpresionDtos = new TipoPagoReimpresionDto[_tiposPago.length];

        int contador = 0;

        for (TipoPagoReimpresionBean bean : _tiposPago) {
            TipoPagoReimpresionDto dto = new TipoPagoReimpresionDto();
            dto.setFiTipoPagoId(bean.getFiTipoPagoId());
            dto.setFnMontoRecibido(bean.getFnMontoRecibido());
            dto.setMontoPago(bean.getMontoPago());
            tipoPagoReimpresionDtos[contador] = dto;
            contador++;
        }

        return tipoPagoReimpresionDtos;
    }

    private TarjetaReimpresionDto[] tarjetaReimpresionBeanaDto(TarjetaReimpresionBean[] _tarjetas) {
        TarjetaReimpresionDto[] tarjetaReimpresionDtos = new TarjetaReimpresionDto[_tarjetas.length];

        int contador = 0;

        for (TarjetaReimpresionBean bean : _tarjetas) {
            TarjetaReimpresionDto dto = new TarjetaReimpresionDto();
            dto.setFcDescripcion(bean.getFcDescripcion());
            dto.setFcNumeroAutorizacion(bean.getFcNumeroAutorizacion());
            dto.setFcNumeroTarjeta(bean.getFcNumeroTarjeta());
            dto.setFiAfiliacion(bean.getFiAfiliacion());
            dto.setFiPagoTarjetasId(bean.getFiPagoTarjetasId());
            dto.setImporteComision(bean.getImporteComision());
            tarjetaReimpresionDtos[contador] = dto;
            contador++;
        }

        return tarjetaReimpresionDtos;
    }
    /**
     * ***reimpresion devoluciones 21 mayo 2014****
     */
    @Override
    public RespuestaTicketReimpresionDto consultarDetalleTicket(PeticionTicketReimpresionDto _peticion) throws SionException {
        if (_peticion != null) {
            RespuestaTicketReimpresionDto respuesta = new RespuestaTicketReimpresionDto();
            SION.log(Modulo.VENTA, "Peticion de reimpresion: "+_peticion.toString(), Level.INFO);
            try {
                respuesta = utilerias.respuestaTicketReimpresionBeanaDto(stub.consultarDetalleTicket(utilerias.peticionTicketReimpresionDtoaBean(_peticion)));
            } catch (AxisFault ex) {
                respuesta = null;
                if (ex.getDetail() != null) {
                    SION.logearExcepcion(Modulo.VENTA, ex, new String[]{ex.getDetail().toString()});
                } else {
                    SION.logearExcepcion(Modulo.VENTA, ex, new String[0]);
                }
                throw new SionException(ex);
            } catch (Exception ex) {
                SION.logearExcepcion(Modulo.VENTA, ex, new String[0]);
                throw new SionException(ex);
            }
            SION.log(Modulo.VENTA, "Respuesta recibida: "+respuesta.toString(), Level.INFO);
            return respuesta;
        } else {
            return null;
        }
    }
    
    /*Antad*/
    
    @Override
    public RespuestaReimpresionDto consultarReimpresion(PeticionTicketReimpresionDto _peticion) throws SionException {
        RespuestaReimpresionDto respuesta = new RespuestaReimpresionDto();

        SION.log(Modulo.VENTA, "Petición de detalle de ticket : " + _peticion, Level.INFO);

        try {
            RespuestaReimpresionBean respuestaBean = stub.consultarTicket(utilerias.peticionTicketReimpresionDtoaBean(_peticion));
            respuesta = respuestaReimpresionBeanaDto(respuestaBean);
            SION.log(Modulo.VENTA, "Respuesta recibida: " + respuesta, Level.INFO);
        } catch (AxisFault ex) {
            respuesta = null;
            if (ex.getDetail() != null) {
                SION.logearExcepcion(Modulo.VENTA, ex, new String[]{ex.getDetail().toString()});
            } else {
                SION.logearExcepcion(Modulo.VENTA, ex, new String[0]);
            }
            throw new SionException(ex);
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, new String[0]);
            throw new SionException(ex);
        }

        return respuesta;
    }

    public RespuestaReimpresionDto respuestaReimpresionBeanaDto(RespuestaReimpresionBean _respuesta) {
        RespuestaReimpresionDto respuesta = new RespuestaReimpresionDto();
        int contador = 0;

        respuesta.setFiUsuarioId(_respuesta.getFiUsuarioId());
        respuesta.setPaCdgError(_respuesta.getPaCdgError());
        respuesta.setPaDescError(_respuesta.getPaDescError());
        respuesta.setPaEstacionTrabajo(_respuesta.getPaEstacionTrabajo());
        respuesta.setPaFechaCreacion(_respuesta.getPaFechaCreacion());
        respuesta.setPaMoviminetoId(_respuesta.getPaMoviminetoId());
        respuesta.setPaNombreUsuario(_respuesta.getPaNombreUsuario());
        respuesta.setPaPermiteTicket(_respuesta.getPaPermiteTicket());
        respuesta.setPaTipoMovimiento(_respuesta.getPaTipoMovimiento());

        //valida codigo de error para llenar cursores
        if (respuesta.getPaCdgError() == 0) {

            TipoPagoReimpresionDto[] arregloTiposPago = new TipoPagoReimpresionDto[_respuesta.getTipoPagoReimpresionBeans().length];
            for (TipoPagoReimpresionBean tipoPago : _respuesta.getTipoPagoReimpresionBeans()) {
                TipoPagoReimpresionDto dto = new TipoPagoReimpresionDto();
                dto.setFiTipoPagoId(tipoPago.getFiTipoPagoId());
                dto.setFnMontoRecibido(tipoPago.getFnMontoRecibido());
                dto.setMontoPago(tipoPago.getMontoPago());
                arregloTiposPago[contador] = dto;
                contador++;
            }
            respuesta.setTipoPagoReimpresionDtos(arregloTiposPago);

            contador = 0;
            switch (respuesta.getPaTipoMovimiento()) {
                case 9://venta de articulos

                    ArticuloReimpresionTicketDto[] arregloArticulos = new ArticuloReimpresionTicketDto[_respuesta.getArticuloReimpresionBeans().length];
                    for (ArticuloReimpresionTicketBean articulo : _respuesta.getArticuloReimpresionBeans()) {
                        ArticuloReimpresionTicketDto dto = new ArticuloReimpresionTicketDto();
                        dto.setFcNombreArticulo(articulo.getFcNombreArticulo());
                        dto.setFiIepsId(articulo.getFiIepsId());
                        dto.setFnCantidad(articulo.getFnCantidad());
                        dto.setFnDescuento(articulo.getFnDescuento());
                        dto.setFnIva(articulo.getFnIva());
                        dto.setFnPrecio(articulo.getFnPrecio());
                        arregloArticulos[contador] = dto;
                        contador++;
                    }
                    respuesta.setArticuloReimpresionDtos(arregloArticulos);

                    contador = 0;
                    TarjetaReimpresionDto[] arregloPagosTarjeta = new TarjetaReimpresionDto[_respuesta.getTarjetaReimpresionBeans().length];
                    for (TarjetaReimpresionBean pagoTarjeta : _respuesta.getTarjetaReimpresionBeans()) {
                        TarjetaReimpresionDto dto = new TarjetaReimpresionDto();
                        if (pagoTarjeta != null) {
                            if (pagoTarjeta.getFcDescripcion() != null && pagoTarjeta.getFcNumeroAutorizacion() != null) {
                                dto.setFcDescripcion(pagoTarjeta.getFcDescripcion());
                                dto.setFcNumeroAutorizacion(pagoTarjeta.getFcNumeroAutorizacion());
                                dto.setFcNumeroTarjeta(pagoTarjeta.getFcNumeroTarjeta());
                                dto.setFiAfiliacion(pagoTarjeta.getFiAfiliacion());
                                dto.setFiPagoTarjetasId(pagoTarjeta.getFiPagoTarjetasId());
                                dto.setImporteComision(pagoTarjeta.getImporteComision());
                                arregloPagosTarjeta[contador] = dto;
                                contador++;
                            }
                        }
                    }
                    respuesta.setTarjetaReimpresionDtos(arregloPagosTarjeta);

                    respuesta.setArregloArticulosDevolucion(null);
                    respuesta.setArticuloTAReimpresionDtos(null);
                    respuesta.setServiciosReimpresionDtos(null);
                    break;
                case 10://recarga de tiempo aire

                    contador = 0;
                    ArticuloTAReimpresionDto[] arregloRecargas = new ArticuloTAReimpresionDto[_respuesta.getArticuloTAReimpresionBeans().length];
                    for (ArticuloTAReimpresionBean recarga : _respuesta.getArticuloTAReimpresionBeans()) {
                        ArticuloTAReimpresionDto dto = new ArticuloTAReimpresionDto();
                        dto.setCompania(recarga.getCompania());
                        dto.setFcNombreArticulo(recarga.getFcNombreArticulo());
                        dto.setFiEmpresaId(recarga.getFiEmpresaId());
                        dto.setNumOperacion(recarga.getNumOperacion());
                        dto.setNumReferencia(recarga.getNumReferencia());
                        dto.setNumero(recarga.getNumero());
                        arregloRecargas[contador] = dto;
                        contador++;
                    }
                    respuesta.setArticuloTAReimpresionDtos(arregloRecargas);

                    respuesta.setArregloArticulosDevolucion(null);
                    respuesta.setArticuloReimpresionDtos(null);
                    respuesta.setServiciosReimpresionDtos(null);
                    respuesta.setTarjetaReimpresionDtos(null);

                    break;
                case 19://pago de servicio

                    contador = 0;
                    ServicioReimpresionDto[] arregloServicios = new ServicioReimpresionDto[_respuesta.getServiciosReimpresionBeans().length];
                    for (ServicioReimpresionBean servicio : _respuesta.getServiciosReimpresionBeans()) {
                        ServicioReimpresionDto dto = new ServicioReimpresionDto();
                        dto.setComision(servicio.getComision());
                        dto.setImpuesto(servicio.getImpuesto());
                        dto.setMensaje(servicio.getMensaje());
                        dto.setMonto(servicio.getMonto());
                        dto.setNomArticulo(servicio.getNomArticulo());
                        dto.setNumAutorizacion(servicio.getNumAutorizacion());
                        dto.setReferencia(servicio.getReferencia());
                        arregloServicios[contador] = dto;
                        contador++;
                    }
                    respuesta.setServiciosReimpresionDtos(arregloServicios);

                    respuesta.setArregloArticulosDevolucion(null);
                    respuesta.setArticuloReimpresionDtos(null);
                    respuesta.setArticuloTAReimpresionDtos(null);
                    respuesta.setTarjetaReimpresionDtos(null);
                    break;
                case 58://devolucion

                    contador = 0;
                    ArticuloDevReimpresionDto[] arregloDevoluciones = new ArticuloDevReimpresionDto[_respuesta.getArregloArticulosDevolucion().length];
                    for (VentaServiceStub.ArticuloDevReimpresionBean articuloDev : _respuesta.getArregloArticulosDevolucion()) {
                        ArticuloDevReimpresionDto dto = new ArticuloDevReimpresionDto();
                        dto.setCantidad(articuloDev.getCantidad());
                        dto.setIepsId(articuloDev.getIepsId());
                        dto.setIva(articuloDev.getIva());
                        dto.setNombre(articuloDev.getNombre());
                        dto.setPrecio(articuloDev.getPrecio());
                        arregloDevoluciones[contador] = dto;
                        contador++;
                    }
                    respuesta.setArregloArticulosDevolucion(arregloDevoluciones);

                    respuesta.setArticuloReimpresionDtos(null);
                    respuesta.setArticuloTAReimpresionDtos(null);
                    respuesta.setServiciosReimpresionDtos(null);
                    respuesta.setTarjetaReimpresionDtos(null);
                    break;
            }

        } else {
            respuesta.setArticuloReimpresionDtos(null);
            respuesta.setArticuloTAReimpresionDtos(null);
            respuesta.setServiciosReimpresionDtos(null);
            respuesta.setTarjetaReimpresionDtos(null);
            respuesta.setArregloArticulosDevolucion(null);
        }


        return respuesta;
    }



    public static void main(String[] args) {
        
       
        
        //try {
        //ConsumirVentaImp cliente = new ConsumirVentaImp();
        /*
         * PeticionTicketReimpresionDto dto = new
         * PeticionTicketReimpresionDto(); dto.setPaPaisId(1);
         * dto.setPaParamTipoTrans(1); dto.setPaTiendaId(903);
         * dto.setPaNumTransaccion(Long.parseLong("2013012375018"));
         * System.out.println(cliente.consultarDetalleTicket(dto).toString());
         */
        /*
         * PeticionRecargaVentaMixtaDto peticion = new
         * PeticionRecargaVentaMixtaDto(); peticion.setArticuloId(430100);
         * peticion.setCodigoBarras("430100");
         * peticion.setCompaniaTelefonicaId(3);
         * peticion.setConsecutivoTiempoAire(16);
         * peticion.setFechaOperacion("17/05/2013 10:38:50");
         * peticion.setFolio(5697); peticion.setMonto(30); peticion.setPais(1);
         * peticion.setNumeroTelefono("5525616824");
         * peticion.setTerminal("10.51.218.110|CAJA1");
         * peticion.setTiendaId(903); peticion.setUsuarioId(741346);
         * System.out.println("peticion de recarga: " + peticion.toString());
         * RespuestaRecargaVentaMixtaDto respuesta =
         * cliente.aplicaRecargaVentamixta(peticion);
         * System.out.println("respuesta de descarga: " + respuesta.toString());
         */
        /*
         * PeticionTicketReimpresionDto peticion = new
         * PeticionTicketReimpresionDto();
         *
         * peticion.setPaPaisId(1); peticion.setPaParamTipoTrans(1);
         * peticion.setPaTiendaId(903);
         * peticion.setPaNumTransaccion(Long.valueOf("20131007269143"));
         *
         * RespuestaTicketReimpresionDto respuesta = new
         * RespuestaTicketReimpresionDto();
         *
         * System.out.println("peticion: "+peticion.toString());
         *
         * respuesta = cliente.consultarDetalleTicket(peticion);
         *
         * System.out.println("respuesta :"+respuesta.toString());
         *
         *
         *
         * } catch (Exception ex) { ex.printStackTrace(); }
         */
    }
}
