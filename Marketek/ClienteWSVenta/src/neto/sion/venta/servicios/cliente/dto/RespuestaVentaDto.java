package neto.sion.venta.servicios.cliente.dto;

import java.util.List;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.dto.Base;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.util.ConversionBeanes;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;


@JsonPropertyOrder({
    "paTransaccionId",
    "paCdgError",
    "paDescError",
    "paTaNumOperacion",
    "paTypCursorBlqs",
    "cupones",
    "xmlResponse"
})
public class RespuestaVentaDto extends Base {

   @JsonProperty("paTransaccionId") 
   private long paTransaccionId;
   @JsonProperty("paCdgError")  
   private int paCdgError;
   @JsonProperty("paDescError") 
   private String paDescError;
   @JsonProperty("paTaNumOperacion")  
   private long paTaNumOperacion;
   @JsonProperty("paTypCursorBlqs") 
   private BloqueoDto[] paTypCursorBlqs;
   @JsonProperty("cupones") 
   private List<CuponesBean> cupones;

    public BloqueoDto[] getPaTypCursorBlqs() {
        return paTypCursorBlqs;
    }

    public void setPaTypCursorBlqs(BloqueoDto[] paTypCursorBlqs) {
        this.paTypCursorBlqs = paTypCursorBlqs;
    }

    public long getPaTransaccionId() {
        return paTransaccionId;
    }

    public void setPaTransaccionId(long paTransaccionId) {
        this.paTransaccionId = paTransaccionId;
    }

    public int getPaCdgError() {
        return paCdgError;
    }

    public void setPaCdgError(int paCdgError) {
        this.paCdgError = paCdgError;
    }

    public String getPaDescError() {
        return paDescError;
    }

    public void setPaDescError(String paDescError) {
        this.paDescError = paDescError;
    }
    
    public long getPaTaNumOperacion() {
        return paTaNumOperacion;
    }

    public void setPaTaNumOperacion(long paTaNumOperacion) {
        this.paTaNumOperacion = paTaNumOperacion;
    }

    @JsonProperty("cupones")
    public List<CuponesBean> getCupones() {
	return cupones;
    }

	
    @JsonProperty("cupones")
    public void setCupones(List<CuponesBean> cupones) {
        this.cupones = cupones;
    }
	
	 
    @Override
    public String toString() {
        try {
            return ConversionBeanes.convertir(this, String.class).toString();
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.VENTA, ex, "toString");
        }
           
        return null;
    }
}
