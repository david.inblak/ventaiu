/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvega
 */
public class TarjetaDto {

    private long fiPagoTarjetasId;
    private String fdFechaoperacion;
    private long fiNumTransaccion;
    private String fcEstatus;
    private String fcNumeroAutorizacion;
    private double fnMontoTarjeta;
    private int fiSubmoduloId;
    private int fiEstatustarjetaId;

    public int getFiEstatustarjetaId() {
        return fiEstatustarjetaId;
    }

    public void setFiEstatustarjetaId(int fiEstatustarjetaId) {
        this.fiEstatustarjetaId = fiEstatustarjetaId;
    }

    public int getFiSubmoduloId() {
        return fiSubmoduloId;
    }

    public void setFiSubmoduloId(int fiSubmoduloId) {
        this.fiSubmoduloId = fiSubmoduloId;
    }

    public long getFiPagoTarjetasId() {
        return fiPagoTarjetasId;
    }

    public void setFiPagoTarjetasId(long fiPagoTarjetasId) {
        this.fiPagoTarjetasId = fiPagoTarjetasId;
    }

    public String getFdFechaoperacion() {
        return fdFechaoperacion;
    }

    public void setFdFechaoperacion(String fdFechaoperacion) {
        this.fdFechaoperacion = fdFechaoperacion;
    }

    public long getFiNumTransaccion() {
        return fiNumTransaccion;
    }

    public void setFiNumTransaccion(long fiNumTransaccion) {
        this.fiNumTransaccion = fiNumTransaccion;
    }

    public String getFcEstatus() {
        return fcEstatus;
    }

    public void setFcEstatus(String fcEstatus) {
        this.fcEstatus = fcEstatus;
    }

    public double getFnMontoTarjeta() {
        return fnMontoTarjeta;
    }

    public void setFnMontoTarjeta(double fnMontoTarjeta) {
        this.fnMontoTarjeta = fnMontoTarjeta;
    }

    public String getFcNumeroAutorizacion() {
        return fcNumeroAutorizacion;
    }

    public void setFcNumeroAutorizacion(String fcNumeroAutorizacion) {
        this.fcNumeroAutorizacion = fcNumeroAutorizacion;
    }

    @Override
    public String toString() {
        return "TarjetaDto{" + "fiPagoTarjetasId=" + fiPagoTarjetasId + ", fdFechaoperacion=" + fdFechaoperacion + ", fiNumTransaccion=" + fiNumTransaccion + ", fcEstatus=" + fcEstatus + ", fcNumeroAutorizacion=" + fcNumeroAutorizacion + ", fnMontoTarjeta=" + fnMontoTarjeta + ", fiSubmoduloId=" + fiSubmoduloId + ", fiEstatustarjetaId=" + fiEstatustarjetaId + '}';
    }
}
