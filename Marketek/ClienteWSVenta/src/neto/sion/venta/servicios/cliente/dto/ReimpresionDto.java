package neto.sion.venta.servicios.cliente.dto;

import neto.sion.tienda.genericos.dto.Base;

/**
 * *
 * Clase utilizada en la invocacion del store de reimpresiones para cual es el
 * ticket que actualizara su numero de reimpresiones.
 *
 * @author Carlos V. Perez L.
 *
 */
public class ReimpresionDto extends Base {

    private long fiPagoTarjeta;

    public long getFiPagoTarjeta() {
        return fiPagoTarjeta;
    }

    public void setFiPagoTarjeta(long fiPagoTarjeta) {
        this.fiPagoTarjeta = fiPagoTarjeta;
    }

    @Override
    public String toString() {
        return "ReimpresionesDto [fiPagoTarjeta=" + fiPagoTarjeta + "]";
    }
}
