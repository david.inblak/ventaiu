/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author fvega
 */
public class EmpresaBean {

    private int empresaId;
    private String codigoBarras;
    private String descrpcion;
    private int agrupacionId;  //se agrega en implementacion de Antad, indica si es un emisor de Antad
    private int categoriaId;  //se agrega en implementacion de Antad, indica si es un emisor en el cual se realizara consulta previa
    private String comision;
    private int tipoServicio;
    
    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public String getDescrpcion() {
        return descrpcion;
    }

    public void setDescrpcion(String descrpcion) {
        this.descrpcion = descrpcion;
    }

    public int getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(int empresaId) {
        this.empresaId = empresaId;
    }

    public int getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(int categoriaId) {
        this.categoriaId = categoriaId;
    }

    public int getAgrupacionId() {
        return agrupacionId;
    }

    public void setAgrupacionId(int agrupacionId) {
        this.agrupacionId = agrupacionId;
    }
    
    /**
     * @return the comision
     */
    public String getComision() {
        return comision;
    }

    /**
     * @param comision the comision to set
     */
    public void setComision(String comision) {
        this.comision = comision;
    }

    
    @Override
    public String toString() {
        return "EmpresaBean{" + "empresaId=" + empresaId + ", codigoBarras=" + codigoBarras + ", descrpcion=" + descrpcion + ", agrupacionId=" + agrupacionId + ", comision=" + comision +'}';
    }

    /**
     * @return the tipoServicio
     */
    public int getTipoServicio() {
        return tipoServicio;
    }

    /**
     * @param tipoServicio the tipoServicio to set
     */
    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    
}
