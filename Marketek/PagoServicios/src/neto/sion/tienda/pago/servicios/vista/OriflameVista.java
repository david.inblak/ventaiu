/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vista;

import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.binding.OriflameBinding;
import neto.sion.tienda.pago.servicios.modelo.PagoOriflameBean;
import neto.sion.tienda.pago.servicios.modelo.ServicioBean;
import neto.sion.tienda.pago.servicios.vistamodelo.PagoServiciosVistaModelo;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;

/**
 *
 * @author fvega
 */
public class OriflameVista {

    private UtileriasVenta utilerias;
    private OriflameBinding binding;
    private PagoServiciosVistaModelo pagosVistaModelo;
    //contenedores
    public Stage stage;
    private Group grupo;
    private Scene escena;
    private BorderPane panel;
    private VBox cajaPrincipal;
    //cajaPrincipal
    private VBox cajaBarraSuperior;
    private VBox cajaLogo;
    private HBox cajaDatos;
    //cajaBarraSuperior
    private Image imagenCerrar;
    private ImageView vistaImagenCerrar;
    //cajaLogo
    private Image imagenLogo;
    private ImageView vistaImagenLogo;
    //cajaDatos
    private VBox cajaTabla;
    private VBox cajaInformacion;
    //cajaTabla
    private HBox cajaCabeceroTabla;
    private HBox cajaCabeceroTabla_1;
    private HBox cajaCabeceroTabla_2;
    private HBox cajaCabeceroTabla_3;
    private HBox cajaCabeceroTabla_4;
    private Label etiquetaCabeceroTabla_1;
    private Label etiquetaCabeceroTabla_2;
    private Label etiquetaCabeceroTabla_3;
    private Label etiquetaCabeceroTabla_4;
    private TableView tablaPagos;
    private TableColumn columnaSeleccion;
    private TableColumn columnaNoPedido;
    private TableColumn columnaMonto;
    private TableColumn columnaFecha;
    //cajaInformacion
    private VBox cajaImagenProductos;
    private VBox cajaBotones;
    //cajaImagenProductos
    private Image imagenProductos;
    private ImageView vistaImagenProductos;
    //cajaBotones
    private Image imagenBotonAceptar;
    private Image imagenBotonAceptarHover;
    private ImageView vistaImagenBotonAceptar;
    private Button botonAceptar;
    private Image imagenBotonCancelar;
    private Image imagenBotonCancelarHover;
    private ImageView vistaImagenBotonCancelar;
    private Button botonCancelar;
    private Group grupoMontoTotal;
    //grupoMontoTotal
    private Image imagenMontoTotal;
    private ImageView vistaImagenMontoTotal;
    private Text textoMontoTotal;
    //
    private int tipoPago;
    private double comision;
    //
    private final KeyCombination escape = new KeyCodeCombination(KeyCode.ESCAPE);

    public OriflameVista(UtileriasVenta _utilerias, PagoServiciosVistaModelo _pagosVistaModelo, int _tipoPago) {

        this.utilerias = _utilerias;
        this.binding = new OriflameBinding(this, utilerias);
        this.pagosVistaModelo = _pagosVistaModelo;

        this.tipoPago = _tipoPago;

        //contenedores
        this.grupo = new Group();
        this.panel = new BorderPane();
        this.cajaPrincipal = utilerias.getVBox(0, 700, 415);

        //cajaPrincipal
        this.cajaBarraSuperior = utilerias.getVBox(0, 680, 30);
        this.cajaLogo = utilerias.getVBox(0, 680, 80);
        this.cajaDatos = utilerias.getHBox(5, 680, 290);

        //cajaBarraSuperior
        this.imagenCerrar = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/cerrar.png");
        this.vistaImagenCerrar = new ImageView(imagenCerrar);

        //cajaLogo
        this.imagenLogo = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/logo_oriflame.png");
        this.vistaImagenLogo = new ImageView(imagenLogo);

        //cajaDatos
        this.cajaTabla = utilerias.getVBox(0, 490, 280);
        this.cajaInformacion = utilerias.getVBox(0, 180, 280);

        //cajaTabla
        this.cajaCabeceroTabla = utilerias.getHBox(0, 490, 35);
        this.cajaCabeceroTabla_1 = utilerias.getHBox(0, 80, 35);
        this.cajaCabeceroTabla_2 = utilerias.getHBox(0, 155, 35);
        this.cajaCabeceroTabla_3 = utilerias.getHBox(0, 105, 35);
        this.cajaCabeceroTabla_4 = utilerias.getHBox(0, 110, 35);
        this.etiquetaCabeceroTabla_1 = utilerias.getLabel("", 80, 20);
        this.etiquetaCabeceroTabla_2 = utilerias.getLabel("", 155, 20);
        this.etiquetaCabeceroTabla_3 = utilerias.getLabel("", 105, 20);
        this.etiquetaCabeceroTabla_4 = utilerias.getLabel("", 110, 20);
        this.tablaPagos = new TableView();
        this.columnaSeleccion = utilerias.getColumna("Selecciona", 80);
        this.columnaNoPedido = utilerias.getColumna("No. pedido", 150);
        this.columnaMonto=  utilerias.getColumna("Monto", 100);
        this.columnaFecha = utilerias.getColumna("Fecha", 120);

        //cajaInformacion
        this.cajaImagenProductos = utilerias.getVBox(0, 180, 100);
        this.cajaBotones = utilerias.getVBox(5, 180, 170);

        //cajaImagenProductos
        this.imagenProductos = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/productos_oriflame.png");
        this.vistaImagenProductos = new ImageView(imagenProductos);

        //cajaBotones
        this.imagenBotonAceptar = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/boton_aceptar.png");
        this.imagenBotonAceptarHover = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/boton_aceptar_hover.png");
        this.vistaImagenBotonAceptar = new ImageView(imagenBotonAceptar);
        this.botonAceptar = new Button("", vistaImagenBotonAceptar);
        this.imagenBotonCancelar = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/boton_cancelar.png");
        this.imagenBotonCancelarHover = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/boton_cancelar_hover.png");
        this.vistaImagenBotonCancelar = new ImageView(imagenBotonCancelar);
        this.botonCancelar = new Button("", vistaImagenBotonCancelar);
        this.grupoMontoTotal = new Group();

        //grupoMontoTotal
        this.imagenMontoTotal = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/montototal.png");
        this.vistaImagenMontoTotal = new ImageView(imagenMontoTotal);
        this.textoMontoTotal = new Text("0.00");

    }

    private void creaEstilos() {

        cajaBarraSuperior.getStyleClass().add("CajaBarraSuperior");
        cajaLogo.getStyleClass().add("CajaLogo");

        cajaDatos.setStyle("-fx-background-color: transparent; -fx-alignment: center;");

        tablaPagos.getStyleClass().add("TablaOriflame");

        etiquetaCabeceroTabla_1.setStyle("-fx-alignment: center; -fx-font: 18pt Verdana; -fx-text-fill: white;");
        etiquetaCabeceroTabla_2.setStyle("-fx-alignment: center; -fx-font: 18pt Verdana; -fx-text-fill: white;");
        etiquetaCabeceroTabla_3.setStyle("-fx-alignment: center; -fx-font: 18pt Verdana; -fx-text-fill: white;");
        etiquetaCabeceroTabla_4.setStyle("-fx-alignment: center; -fx-font: 18pt Verdana; -fx-text-fill: white;");

        cajaCabeceroTabla_1.setStyle("-fx-background-color: transparent; -fx-alignment: center;");
        cajaCabeceroTabla_2.setStyle("-fx-background-color: transparent; -fx-alignment: center;");
        cajaCabeceroTabla_3.setStyle("-fx-background-color: transparent; -fx-alignment: center;");
        cajaCabeceroTabla_4.setStyle("-fx-background-color: transparent; -fx-alignment: center;");

        cajaTabla.getStyleClass().add("CajaTabla");
        cajaInformacion.getStyleClass().add("CajaInformacion");

        cajaImagenProductos.setStyle("-fx-background-color: transparent; -fx-alignment: center;");
        cajaBotones.setStyle("-fx-background-color: transparent; -fx-alignment: center;");
        textoMontoTotal.setStyle("-fx-alignment: center-right bottom-left; -fx-font: 20pt Verdana; -fx-text-fill: black;");

        cajaPrincipal.getStyleClass().add("CajaPrincipalOriflame");

        panel.setStyle("-fx-background-color: transparent;");
        //panel.setStyle(" -fx-background-color: white ; -fx-border-color: transparent, transparent, orange ; -fx-border-width: 3; -fx-border-radius: 1; fx-border-insets:-5");
    }

    private void ajustaComponentesPantalla() {

        //cajaBarraSuperior
        vistaImagenCerrar.setFitWidth(20);
        vistaImagenCerrar.setFitHeight(20);

        //cajaLogo
        vistaImagenLogo.setFitHeight(50);
        vistaImagenLogo.setFitWidth(280);

        //cajaDatos
        cajaTabla.getChildren().addAll(cajaCabeceroTabla, tablaPagos);
        cajaInformacion.getChildren().addAll(cajaImagenProductos, cajaBotones);

        //cajaTabla
        cajaCabeceroTabla_1.getChildren().addAll(etiquetaCabeceroTabla_1);
        cajaCabeceroTabla_2.getChildren().addAll(etiquetaCabeceroTabla_2);
        cajaCabeceroTabla_3.getChildren().addAll(etiquetaCabeceroTabla_3);
        cajaCabeceroTabla_4.getChildren().addAll(etiquetaCabeceroTabla_4);
        cajaCabeceroTabla.getChildren().addAll(cajaCabeceroTabla_1, cajaCabeceroTabla_2, cajaCabeceroTabla_3, cajaCabeceroTabla_4);
        tablaPagos.setMaxSize(470, 235);
        tablaPagos.setMinSize(470, 235);
        tablaPagos.setPrefSize(470, 235);
        tablaPagos.getColumns().addAll(columnaSeleccion, columnaNoPedido, columnaMonto, columnaFecha);

        //cajaInformacion
        vistaImagenProductos.setFitHeight(90);
        vistaImagenProductos.setFitWidth(105);
        cajaImagenProductos.getChildren().addAll(vistaImagenProductos);
        grupoMontoTotal.setBlendMode(BlendMode.MULTIPLY);
        grupoMontoTotal.getChildren().add(vistaImagenMontoTotal);
        grupoMontoTotal.getChildren().add(textoMontoTotal);
        textoMontoTotal.setX(50);
        textoMontoTotal.setY(70);
        vistaImagenBotonAceptar.setFitWidth(150);
        vistaImagenBotonAceptar.setFitHeight(30);
        botonAceptar.setMaxSize(150, 30);
        botonAceptar.setMinSize(150, 30);
        botonAceptar.setPrefSize(150, 30);
        vistaImagenBotonCancelar.setFitWidth(150);
        vistaImagenBotonCancelar.setFitHeight(30);
        botonCancelar.setMaxSize(150, 30);
        botonCancelar.setMinSize(150, 30);
        botonCancelar.setPrefSize(150, 30);
        vistaImagenMontoTotal.setFitWidth(150);
        vistaImagenMontoTotal.setFitHeight(100);
        cajaBotones.getChildren().addAll(botonAceptar, botonCancelar, grupoMontoTotal);

        cajaBarraSuperior.getChildren().addAll(vistaImagenCerrar);

        cajaLogo.getChildren().addAll(vistaImagenLogo);

        cajaDatos.getChildren().addAll(cajaTabla, cajaInformacion);

        cajaPrincipal.getChildren().addAll(cajaBarraSuperior, cajaLogo, cajaDatos);

        panel.setTop(cajaPrincipal);
        panel.setMinHeight(415);
        panel.setPrefHeight(415);
        panel.setMaxHeight(415);
        panel.setMinWidth(700);
        panel.setPrefWidth(700);
        panel.setMaxWidth(700);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                grupo.getChildren().addAll(panel);
            }
        });
    }

    private void setHover(final ImageView _vistaImagen, final Image _imagen, final Image _imagenHover) {

        _vistaImagen.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    _vistaImagen.setImage(_imagenHover);
                } else if (arg1 && !arg2) {
                    _vistaImagen.setImage(_imagen);
                }
            }
        });
    }

    private void creaHandlers() {

        vistaImagenCerrar.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {
                stage.close();
            }
        });

        panel.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if (stage.isShowing()) {
                    binding.limpiar();
                    limpiarVista();
                    stage.close();
                }
            }
        });

        botonCancelar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                SION.log(Modulo.PAGO_SERVICIOS, "Boton cancelar presionado", Level.INFO);
                if (stage.isShowing()) {
                    binding.limpiar();
                    limpiarVista();
                    stage.close();
                }
            }
        });

        botonAceptar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                SION.log(Modulo.PAGO_SERVICIOS, "Boton aceptar presionado", Level.INFO);

                //cargar seleccionados
                binding.cargarPagosSeleccionados();
                actualizaRegistrosOriflame();

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        binding.limpiar();
                        limpiarVista();
                        stage.close();
                    }
                });


            }
        });
    }

    private void setCursor(final Node _nodo) {

        _nodo.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {

                if (arg2 && !arg1) {
                    _nodo.setCursor(Cursor.HAND);

                } else if (arg1 && !arg2) {
                    _nodo.setCursor(Cursor.NONE);

                }


            }
        });

    }

    public void quitaCabeceraTabla(final TableView tabla) {
        tabla.widthProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
                Pane header = (Pane) tabla.lookup("TableHeaderRow");
                if (header.isVisible()) {
                    header.setMaxHeight(0);
                    header.setMinHeight(0);
                    header.setPrefHeight(0);
                    header.setVisible(false);
                }
            }
        });
    }

    private void binding() {

        binding.asignaPropiedadBean(columnaNoPedido, String.class, "subReferencia");
        binding.asignaPropiedadBean(columnaMonto, Double.class, "monto");
        binding.asignaPropiedadBean(columnaFecha, String.class, "fecha");

        binding.setCallbackCheckBox(columnaSeleccion);

        binding.enlazarTablapagos();

    }

    private void cargaPagosOriflame(String _cadena, ServicioBean[] _servicios) {

        SION.log(Modulo.PAGO_SERVICIOS, "Cadena Oriflame recibida: " + _cadena, Level.INFO);

        //evaluar cabecero
        binding.evaluaCabecero(_cadena);

        //evaluar datos grales para multiseleccion
        binding.evaluaDatosGenerales(_cadena);

        //evaluar y cargar pagoa a la lista
        binding.evaluarPagos(_cadena, _servicios);
    }

    public Stage getStageOriflame() {
        return stage;
    }

    public void setCabeceroTabla1(String _cadena) {
        etiquetaCabeceroTabla_1.setText(_cadena);
    }

    public void setCabeceroTabla2(String _cadena) {
        etiquetaCabeceroTabla_2.setText(_cadena);
    }

    public void setCabeceroTabla3(String _cadena) {
        etiquetaCabeceroTabla_3.setText(_cadena);
    }

    public void setCabeceroTabla4(String _cadena) {
        etiquetaCabeceroTabla_4.setText(_cadena);
    }

    public TableView getTablaPagos() {
        return tablaPagos;
    }

    public Text getTextoMontoTotal() {
        return textoMontoTotal;
    }

    public void limpiarVista() {
        etiquetaCabeceroTabla_1.setText("");
        etiquetaCabeceroTabla_2.setText("");
        etiquetaCabeceroTabla_3.setText("");
        etiquetaCabeceroTabla_4.setText("");

        textoMontoTotal.setText("0.00");
    }

    public ObservableList<PagoOriflameBean> getListaPagosSeleccionados() {
        return binding.getListaPagosSeleccionados();
    }

    public void actualizaRegistrosOriflame() {

        double comisionCliente = 0;
        int contadorComisiones = 0;

        //se valida que solo se cobre una sola vez la comision no importa la cantidad de pedidos que se hagan
                //se carga solamente al primer registro
        for (PagoOriflameBean pago : binding.getListaPagosSeleccionados()) {

            if (contadorComisiones == 0) {
                comisionCliente = comision;
            } else {
                comisionCliente = 0;
            }

            pagosVistaModelo.agregaReferenciaOriflame(tipoPago, pago, comisionCliente);
            
            contadorComisiones++;
        }

    }

    public void abreStageOriflame(String _cadena, double _comision, ServicioBean[] _servicios) {

        SION.log(Modulo.PAGO_SERVICIOS, "Inicializando Stage de pagos de servicio Oriflame", Level.INFO);

        if (escena == null) {

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    escena = new Scene(grupo, 700, 415, Color.WHITE);
                    stage = new Stage(StageStyle.UNDECORATED);
                    stage.initModality(Modality.APPLICATION_MODAL);

                    creaEstilos();
                    ajustaComponentesPantalla();
                    creaHandlers();
                    binding();

                    setCursor(vistaImagenCerrar);
                    setCursor(botonAceptar);
                    setCursor(botonCancelar);

                    setHover(vistaImagenBotonAceptar, imagenBotonAceptar, imagenBotonAceptarHover);
                    setHover(vistaImagenBotonCancelar, imagenBotonCancelar, imagenBotonCancelarHover);

                    quitaCabeceraTabla(tablaPagos);

                    escena.getStylesheets().addAll(PagoServiciosVista.class.getResource("/neto/sion/tienda/pago/servicios/vista/EstilosPagoServicios.css").toExternalForm());

                    stage.setTitle("Pagos Oriflame");
                    stage.setScene(escena);
                    stage.centerOnScreen();
                    stage.show();
                }
            });

        } else {

            //escena.getStylesheets().addAll(PagoServiciosVista.class.getResource("/neto/sion/tienda/pago/servicios/vista/EstilosPagoServicios.css").toExternalForm());

            stage.setTitle("Pagos Oriflame");
            stage.setScene(escena);
            stage.centerOnScreen();
            stage.show();

        }

        comision = _comision;
        limpiarVista();
        cargaPagosOriflame(_cadena,_servicios);

    }
}
