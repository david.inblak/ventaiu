/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author fvega
 */
public class RespuestaPagoBean {

    private SimpleStringProperty nombreEmisor;
    private SimpleStringProperty mensajeSwitch;
    private SimpleDoubleProperty importe;
    private SimpleBooleanProperty fueAutorizado;

    public RespuestaPagoBean() {
        
        nombreEmisor = new SimpleStringProperty();
        mensajeSwitch = new SimpleStringProperty();
        importe = new SimpleDoubleProperty();
        fueAutorizado = new SimpleBooleanProperty();
        
    }
    
    public double getImporte() {
        return importe.get();
    }

    public void setImporte(double importe) {
        this.importe.set(importe);
    }

    public String getMensajeSwitch() {
        return mensajeSwitch.get();
    }

    public void setMensajeSwitch(String mensajeSwitch) {
        this.mensajeSwitch.set(mensajeSwitch);
    }

    public String getNombreEmisor() {
        return nombreEmisor.get();
    }

    public void setNombreEmisor(String nombreEmisor) {
        this.nombreEmisor.set(nombreEmisor);
    }

    public boolean getFueAutorizado() {
        return fueAutorizado.get();
    }

    public void setFueAutorizado(boolean fueAutorizado) {
        this.fueAutorizado.set(fueAutorizado);
    }

    @Override
    public String toString() {
        return "RespuestaPagoBean{" + "nombreEmisor=" + nombreEmisor + ", mensajeSwitch=" + mensajeSwitch + ", importe=" + importe + ", fueAutorizado=" + fueAutorizado + '}';
    }
}
