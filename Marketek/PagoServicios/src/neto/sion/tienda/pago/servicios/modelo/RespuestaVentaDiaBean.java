/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author fvega
 */
public class RespuestaVentaDiaBean {

    private int codigoError;
    private String descripcionError;

    public int getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }

    public String getDescripcionError() {
        return descripcionError;
    }

    public void setDescripcionError(String descripcionError) {
        this.descripcionError = descripcionError;
    }

    @Override
    public String toString() {
        return "RespuestaVentaDiaBean{" + "codigoError=" + codigoError + ", descripcionError=" + descripcionError + '}';
    }
}
