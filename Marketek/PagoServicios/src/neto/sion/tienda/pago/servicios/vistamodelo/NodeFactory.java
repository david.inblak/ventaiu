/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vistamodelo;

/**
 *
 * @author fvega
 */
import javafx.scene.Node;

public interface NodeFactory<DataType>
{
    Node createNode(DataType data);
}