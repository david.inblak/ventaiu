/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vistamodelo;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import neto.sion.base.bean.BloqueoBean;
import neto.sion.impresion.ventanormal.dto.ServicioTicketDto;
import neto.sion.impresion.ventanormal.dto.VentaServiciosTicketDto;
import neto.sion.pago.servicios.cliente.dto.*;
import neto.sion.pago.servicios.cliente.serviceFacade.ConsumirWSPagoServiciosImpl;
import neto.sion.tienda.barraUsuario.vista.VistaBarraUsuario;
import neto.sion.tienda.caja.bean.ObjetoBloqueo;
import neto.sion.tienda.caja.controlador.BloqueosOperadorLocal;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.exception.SionException;
import neto.sion.tienda.genericos.utilidades.Constantes;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.binding.PagoServiciosBinding;
import neto.sion.tienda.pago.servicios.dao.PagoServiciosDao;
import neto.sion.tienda.pago.servicios.modelo.*;
import neto.sion.tienda.pago.servicios.vista.ConfirmacionReferenciaVista;
import neto.sion.tienda.pago.servicios.vista.MAZVista;
import neto.sion.tienda.pago.servicios.vista.OriflameVista;
import neto.sion.tienda.pago.servicios.vista.PagoServiciosVista;
import neto.sion.tienda.venta.bloqueo.bean.MensajeBloqueoBean;
import neto.sion.tienda.venta.bloqueo.fachada.BloqueoVenta;
import neto.sion.tienda.venta.conciliacion.bean.PeticionActTransaccionLocalBean;
import neto.sion.tienda.venta.conciliacion.bean.PeticionConciliacionLocalBean;
import neto.sion.tienda.venta.conciliacion.bean.RespuestaActualizacionLocalBean;
import neto.sion.tienda.venta.conciliacion.bean.RespuestaConciliacionLocalBean;
import neto.sion.tienda.venta.conciliacion.controlador.ConciliacionLocalControlador;
import neto.sion.tienda.venta.fueralinea.bean.PagoServicioBean;
import neto.sion.tienda.venta.fueralinea.bean.PeticionPagoServicioLocalBean;
import neto.sion.tienda.venta.fueralinea.bean.RespuestaPagoServicioLocalBean;
import neto.sion.tienda.venta.fueralinea.bean.TipoPagoBean;
import neto.sion.tienda.venta.fueralinea.controlador.VentaFueraLineaControlador;
import neto.sion.tienda.venta.impresiones.ImpresionVenta;
import neto.sion.tienda.venta.utilerias.bean.RespuestaParametroBean;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;
import neto.sion.venta.bloqueo.bean.BloqueoVentaBean;
import neto.sion.venta.servicios.cliente.dto.PeticionBloqueoDto;
import neto.sion.venta.servicios.cliente.dto.RespuestaConsultaTransaccionCentralDto;
import neto.sion.venta.servicios.cliente.dto.TipoPagoDto;
import neto.sion.venta.servicios.cliente.serviceFacade.ConsumirVentaImp;
import org.apache.axis2.AxisFault;

/**
 *
 * @author fvega
 */
public class PagoServiciosVistaModelo {

    private PagoServiciosVista vista;
    private UtileriasVenta utilerias;
    private RespuestaParametroBean parametro;
    private PagoServiciosDao dao;
    private OriflameVista oriflame;
    private MAZVista maz;
    private ConfirmacionReferenciaVista ventanaConfirmacion;
    private PagoServiciosBinding binding;
    //cliente
    private ConsumirWSPagoServiciosImpl cliente;
    private PeticionReferenciaDto peticionValidaReferenciaDto;
    private RespuestaReferenciaDto respuestaReferenciaDto;
    private ArrayList<ReferenciaValidaBean> listaReferenciasValidadas;
    private ArrayList<PeticionPagoServicioDto> listaPeticionesPago;
    private ArrayList<TipoPagoServicioDto> listaTiposPago;
    private PeticionPagoServicioDto[] arregloPeticionesPagoDto;
    private RespuestaPagoServicioDto[] arregloRespuestasPagoDto;
    private PeticionVentaServiciosDto peticionVentaServiciosDto;
    private RespuestaVentaServiciosDto respuestaVentaServiciosDto;
    private TipoPagoServicioDto[] arregloTiposPago;
    //fin cliente
    ///impresion
    private VentaServiciosTicketDto ticket;
    private ImpresionVenta impresionVenta;
    private String cadenaDireccion;
    private String cadenaDelegacion;
    private boolean seImprimioTicketExitoso;
    ///fin impresion
    //conciliacion
    private PeticionConciliacionLocalBean peticionConciliacion;
    private RespuestaConciliacionLocalBean respuestaConciliacionLocalBean;
    private PeticionActTransaccionLocalBean peticionActTransaccionLocalBean;
    private RespuestaActualizacionLocalBean respuestaActualizacionLocalBean;
    private ConciliacionLocalControlador conciliacionLocalControlador;
    private final int ACTUALIZACION_LOCAL = 0;
    //
    private PeticionEmpresasBean peticionEmpresasBean;
    private RespuestaEmpresasBean respuestaEmpresasBean;
    
    private ObservableList<EmpresaBean> listaEmpresas = FXCollections.observableArrayList();//parametros de la operacion
    private ObservableList<String> listaDescEmpresas =  FXCollections.observableArrayList();//parametros de la operacion
    
    private ObservableList<EmpresaBean> listaPines = FXCollections.observableArrayList();//parametros de la operacion
    private ObservableList<String> listaDescPines =  FXCollections.observableArrayList();//parametros de la operacion
    
    
    private ObservableList<ServicioBean> listaServiciosAcumulados =
            FXCollections.observableArrayList();
    private ObservableList<ServicioBean> listapagosSeleccionados =
            FXCollections.observableArrayList();
    private ObservableList<ServicioFrecuenteBean> listaServiciosFrecuentes =
            FXCollections.observableArrayList();
    //esta lista es de los emisores activos y que se cuenta con imagen para mostrar en parte superior
    private ObservableList<EmpresaBean> listaEmpresasCabecera =
            FXCollections.observableArrayList();
    //usar en pagos parciales
    private ObservableList<RespuestaPagoBean> listaPagosParciales =
            FXCollections.observableArrayList();
    //
    //bloqueos
    private boolean existeAvisoBloqueo;
    private String mensajeAvisoBloqueo;
    private boolean existeBloqueo;
    private String mensajeBloqueo;
    private boolean existe2oAvisoBloqueo;
    private String mensaje2oAvisoBloqueo;
    private neto.sion.venta.bloqueo.bean.BloqueoVentaBean datosSesion;
    private MensajeBloqueoBean mensajeBloqueoAviso;
    private boolean existenDatosSesion;
    private PeticionBloqueoDto peticionBloqueoDto;
    private BloqueoBean[] datosBloqueo;
    private BloqueoVenta bloqueoVenta;
    //
    private PeticionServiciosFrecuentesBean peticionServiciosFrecuentesBean;
    private RespuestaServiciosFrecuentesBean respuestaServiciosFrecuentesBean;
    ///
    private final int EFECTIVO = 1;
    private final int TARJETA = 2;
    //venta del dia
    private VentaDiaBean[] pagosDia;
    private PeticionVentaDiaBean peticionVentaDiaBean;
    private RespuestaVentaDiaBean respuestaVentaDiaBean;
    ///fuera de linea
    private VentaFueraLineaControlador pagoFueraLinea;
    private PeticionPagoServicioLocalBean peticionPagoLocal;
    private RespuestaPagoServicioLocalBean respuestaPagoLocal;
    private PeticionConsultaReferenciaBean peticionConsultaReferencia;
    private RespuestaConsultaReferenciaBean respuestaConsultaReferencia;
    private PeticionConsultaOriflameBean peticionConsultaPedidoOriflame;
    private RespuestaConsultaOriflameBean respuestaConsultaPedidoOriflame;
    boolean esPagoLocalExitoso = false;
    //bloqueos fuera de linea
    private BloqueosOperadorLocal bloqueoLocal;
    private List<ObjetoBloqueo> arregloBloqueosLocal;
    private boolean existeBloqueoLocal = false;
    private boolean existeAvisoLocal = false;
    private int tipoBloqueo = 0;
    private int avisosRestantesBloqueoLocal = 0;
    //
    private final int EMISOR_ORIFLAME = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.ORIFLAME").trim());
    private final int EMISOR_MAZ = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.MAZ").trim());
    private final int EMISOR_SKY = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.SKY").trim());
    private final int EMISOR_GOBIERNODF = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.GOBIERNODF").trim());
    private final int EMISOR_IUSACELL = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.IUSACELL").trim());
    private final int EMISOR_TOTALPLAY = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.TOTALPLAY").trim());
    private final int EMISOR_IUSACELL_SECSA = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.IUSACELL_SECSA").trim());
    private final int EMISOR_IZZI = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.IZZI").trim());
    private final int EMISOR_DINEROEXP = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.DINEROEXP").trim());
    //ieps
    private int iepsId;
    
    private String errorAxis;
    private ConsumirVentaImp clienteVenta;
  
    public PagoServiciosVistaModelo(UtileriasVenta _utileriasVenta, PagoServiciosBinding _binding) {
        this.peticionServiciosFrecuentesBean = null;
        this.respuestaServiciosFrecuentesBean = null;
        this.utilerias = _utileriasVenta;
        this.maz = null;
        this.ventanaConfirmacion = null;
        this.oriflame = null;
        this.binding = _binding;
        this.parametro = new RespuestaParametroBean();
        this.dao = new PagoServiciosDao();
        this.peticionEmpresasBean = null;
        this.respuestaEmpresasBean = null;
        //cliente
        this.cliente = new ConsumirWSPagoServiciosImpl(Modulo.PAGO_SERVICIOS);
        try {
            clienteVenta = new ConsumirVentaImp();
        } catch (AxisFault ex) {
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex, "Ocurrio un error al generar cliente de venta");
        } catch (Exception ex) {
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex, "Ocurrio un error al generar cliente de venta");
        }
        this.peticionValidaReferenciaDto = null;
        this.respuestaReferenciaDto = null;
        this.listaReferenciasValidadas = new ArrayList<ReferenciaValidaBean>();
        this.listaPeticionesPago = new ArrayList<PeticionPagoServicioDto>();
        this.listaTiposPago = new ArrayList<TipoPagoServicioDto>();
        this.arregloPeticionesPagoDto = null;
        this.arregloRespuestasPagoDto = null;
        this.arregloTiposPago = null;
        this.peticionVentaServiciosDto = null;
        this.respuestaVentaServiciosDto = null;
        //fin cliente

        //conciliacion
        this.peticionConciliacion = new PeticionConciliacionLocalBean();
        this.conciliacionLocalControlador = new ConciliacionLocalControlador();
        this.peticionActTransaccionLocalBean = new PeticionActTransaccionLocalBean();
        this.respuestaActualizacionLocalBean = new RespuestaActualizacionLocalBean();

        //impresion
        this.ticket = new VentaServiciosTicketDto();
        this.impresionVenta = new ImpresionVenta();
        this.cadenaDireccion = "";
        this.cadenaDelegacion = "";
        //fin impresion

        //bloqueos
        this.bloqueoVenta = new BloqueoVenta();

        ///fuera de linea
        this.pagoFueraLinea = new VentaFueraLineaControlador();
        this.peticionPagoLocal = new PeticionPagoServicioLocalBean();
        this.respuestaPagoLocal = new RespuestaPagoServicioLocalBean();

        ///bloqueos fuera de linea
        this.bloqueoLocal = new BloqueosOperadorLocal(Modulo.PAGO_SERVICIOS);
        this.arregloBloqueosLocal = null;

        //ieps
        try {
            this.iepsId = Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "servicio.iepsId").trim());
        } catch (NumberFormatException e) {
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e, "Ocurrio un error al consultar iepsId desde archivo de propiedades, se asiga 69 default");
            iepsId = 69;
        }
        
    }
    
    public enum ConfirmacionPago{
        
        TOTALPLAY, IUSACELL, IUSACELL_SECSA
        
    }
    
    public void quitaOverlaySeleccionado(){
        this.vista.getCajaOverlay().setVisible(false);
    }
    
    //getters

    public PeticionConciliacionLocalBean getPeticionConciliacion() {
        return peticionConciliacion;
    }

    public RespuestaConciliacionLocalBean getRespuestaConciliacionLocalBean() {
        return respuestaConciliacionLocalBean;
    }

    public ArrayList<ReferenciaValidaBean> getListaReferenciasValidadas() {
        return listaReferenciasValidadas;
    }

    public RespuestaEmpresasBean getRespuestaEmpresasBean() {
        return respuestaEmpresasBean;
    }
    
    //venta del dia
    public void guardarVentaDia(boolean _esFueraLinea) {
        pagosDia = null;
        peticionVentaDiaBean = null;
        respuestaVentaDiaBean = null;

        pagosDia = null;
        peticionVentaDiaBean = new PeticionVentaDiaBean();
        respuestaVentaDiaBean = new RespuestaVentaDiaBean();

        if (_esFueraLinea) {
            pagosDia = new VentaDiaBean[peticionPagoLocal.getArrayServicios().length];
            cargarPagosDiaLocal(peticionPagoLocal.getArrayServicios());
        } else {
            pagosDia = new VentaDiaBean[respuestaVentaServiciosDto.getPaArrayServicios().length];
            cargarPagosDia(respuestaVentaServiciosDto.getPaArrayServicios());
        }

        if (pagosDia != null) {

            peticionVentaDiaBean.setVentaDiaBeans(pagosDia);

            respuestaVentaDiaBean = dao.guardarVentaDia(peticionVentaDiaBean);

            if (respuestaVentaDiaBean != null) {
                if (respuestaVentaDiaBean.getCodigoError() == 0) {
                    SION.log(Modulo.PAGO_SERVICIOS, "La venta se guardo exitosamente", Level.INFO);
                } else {
                    SION.log(Modulo.PAGO_SERVICIOS, respuestaVentaDiaBean.getDescripcionError(), Level.SEVERE);
                }
            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de inserción de venta del día nula", Level.SEVERE);
            }
        } else {
            SION.log(Modulo.PAGO_SERVICIOS, "No hay pagos que registrar como venta del día en esta operación", Level.WARNING);
        }
    }

    private void cargarPagosDia(PagoServicioVentaDto[] _arregloPagos) {
        int contador = 0;
        boolean hayRegistrosExitosos = false;
        for (PagoServicioVentaDto pago : _arregloPagos) {
            if (pago != null) {
                //if (pago.getMovimientoId() > 0) {
                VentaDiaBean pagoDia = new VentaDiaBean();
                pagoDia.setArticuloId(pago.getFiArticuloId());
                pagoDia.setCantidad(1);
                pagoDia.setCodigoBarras(pago.getFcCdgBarras());
                pagoDia.setCosto(buscaPrecioFromPago(pago));
                pagoDia.setDescuento(0);
                pagoDia.setIva(0);
                pagoDia.setPrecio(0);
                pagoDia.setIepsId(iepsId);
                pagosDia[contador] = pagoDia;

                //}

            }
            contador++;
        }

        /*
         * if (!hayRegistrosExitosos) { pagosDia = null; }
         */
    }

    private void cargarPagosDiaLocal(PagoServicioBean[] _arregloPagos) {
        int contador = 0;
        boolean hayRegistrosExitosos = false;
        for (PagoServicioBean pago : _arregloPagos) {
            if (pago != null) {

                VentaDiaBean pagoDia = new VentaDiaBean();
                pagoDia.setArticuloId(pago.getArticuloId());
                pagoDia.setCantidad(1);
                pagoDia.setCodigoBarras(pago.getCodigoBarras());
                pagoDia.setCosto(pago.getMonto());
                pagoDia.setDescuento(0);
                pagoDia.setIva(0);
                pagoDia.setPrecio(0);
                pagoDia.setIepsId(iepsId);
                pagosDia[contador] = pagoDia;
                hayRegistrosExitosos = true;

            }
            contador++;
        }

        if (!hayRegistrosExitosos) {
            pagosDia = null;
        }
    }

    private double buscaPrecioFromPago(PagoServicioVentaDto _pago) {
        double precio = 0;
        for (PeticionPagoServicioDto peticion : peticionVentaServiciosDto.getArrayPagos()) {
            if (peticion.getModuloId() == _pago.getFiArticuloId()
                    && peticion.getReferencia().equals(_pago.getFcreferencia())) {
                precio = peticion.getMonto();
            }
        }
        return precio;
    }

    //
    //pagos parciales
    public ObservableList<RespuestaPagoBean> getListaPagosParciales() {
        return listaPagosParciales;
    }

    public int getCantidadPagosAutorizados() {
        int cantidad = 0;
        for (RespuestaPagoBean bean : listaPagosParciales) {
            if (bean != null) {
                if (bean.getFueAutorizado()) {
                    cantidad++;
                }
            }
        }

        return cantidad;
    }

    public void cargarlistaParciales(PagoServicioVentaDto[] _arreglo) {

        for (PagoServicioVentaDto pago : _arreglo) {
            if (pago != null) {
                RespuestaPagoBean parcial = new RespuestaPagoBean();
                parcial.setImporte(getPeticionPagoFromPeticionVenta(pago).getMonto());
                parcial.setNombreEmisor(buscaNombreEmisorXid(getPeticionPagoFromPeticionVenta(pago).getModuloId()));

                parcial.setFueAutorizado(true);
                if (pago.getMovimientoId() == 0) {
                    parcial.setMensajeSwitch(pago.getFcMensaje());
                } else {
                    parcial.setMensajeSwitch(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.mensaje.autorizacion"));
                }

                listaPagosParciales.add(parcial);
            }
        }

    }

    public double getImporteFinalaCobrar(PagoServicioVentaDto[] _arreglo) {
        double importe = 0;
        for (PagoServicioVentaDto pago : _arreglo) {
            if (pago != null) {
                if (pago.getMovimientoId() > 0) {
                    importe += getPeticionPagoFromPeticionVenta(pago).getMonto();
                }
            }
        }
        return importe;
    }
    //fin pagos parciales

    //bloqueos
    public void inicializarVariablesBloqueo() {
        existeBloqueo = false;
        existeAvisoBloqueo = false;
        existe2oAvisoBloqueo = false;
    }

    public boolean existeAvisoBloqueo() {
        return existeAvisoBloqueo;
    }

    public String getMensajeAvisoBloqueo() {
        return mensajeAvisoBloqueo;
    }

    public boolean existe2oAvisoBloqueo() {
        return existe2oAvisoBloqueo;
    }

    public String get2oMensajeAvisoBloqueo() {
        return mensaje2oAvisoBloqueo;
    }

    public boolean existeBloqueo() {
        return existeBloqueo;
    }

    public String getMensajeBloqueo() {
        return mensajeBloqueo;
    }

    public void evaluarBloqueoVenta(Button boton) {

        SION.log(Modulo.PAGO_SERVICIOS, "Evaluando bloqueo de la venta.", Level.INFO);
        existeAvisoBloqueo = false;
        mensajeAvisoBloqueo = "";

        datosSesion = VistaBarraUsuario.getSesion().getBloqueosVenta();
        existenDatosSesion = false;

        if (datosSesion == null) {
            SION.log(Modulo.PAGO_SERVICIOS, "No se encontraron Datos en sesión", Level.INFO);
            existenDatosSesion = bloqueoVenta.existenDatosSesion(null);
        } else {
            SION.log(Modulo.PAGO_SERVICIOS, "Se encontraron Datos en sesión.", Level.INFO);
            existenDatosSesion = bloqueoVenta.existenDatosSesion((BloqueoVentaBean) datosSesion);
        }

        if (existenDatosSesion && ((BloqueoVentaBean) datosSesion).getCdgError() != -1) {
            //Si hay datos en sesion
            //Evaluar datos bloqueo
            //BloqueoVentaBean datosSesion = (BloqueoVentaBean) VistaBarraUsuario.getSesion().getBloqueosVenta();
            mensajeBloqueoAviso = bloqueoVenta.buscarBloqueo(datosSesion.getMensajeBloqueo(), VistaBarraUsuario.getSesion().getNombreUsuario());
            if (mensajeBloqueoAviso.estaBloqueado()) {
                SION.log(Modulo.PAGO_SERVICIOS, "Obteniendo Datos de bloqueo", Level.INFO);
                obtenerDatosBloqueos(bloqueoVenta, boton);
            } else {
                if (mensajeBloqueoAviso.existeAviso()) {
                    SION.log(Modulo.PAGO_SERVICIOS, "Se encontró mensaje de aviso de bloqueo " + mensajeBloqueoAviso.getMensajeAviso(), Level.INFO);

                    existeAvisoBloqueo = true;
                    mensajeAvisoBloqueo = mensajeBloqueoAviso.getMensajeAviso();

                }
            }

        } else if (existenDatosSesion && ((BloqueoVentaBean) datosSesion).getCdgError() == -1) {

            SION.log(Modulo.PAGO_SERVICIOS, "No existen Datos de Bloqueo", Level.INFO);
        } else {
            SION.log(Modulo.PAGO_SERVICIOS, "Se encontraron Datos de bloqueo", Level.INFO);
            obtenerDatosBloqueos(bloqueoVenta, boton);
        }

    }

    public void obtenerDatosBloqueos(BloqueoVenta bloqueoVenta, final Button boton) {
        SION.log(Modulo.PAGO_SERVICIOS, "Obteniendo datos de bloqueo", Level.INFO);

        peticionBloqueoDto = null;
        peticionBloqueoDto = new PeticionBloqueoDto();
        datosSesion = null;
        datosSesion = new BloqueoVentaBean();

        mensajeBloqueo = "";
        existeBloqueo = false;

        peticionBloqueoDto.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
        peticionBloqueoDto.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
        peticionBloqueoDto.setPaUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());

        datosSesion = null;
        datosBloqueo = null;

        try {
            datosSesion = bloqueoVenta.existeComunicacionCentral(peticionBloqueoDto);
        } catch (Exception e) {
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió un error al intentar comunicarse con central: " + e.toString(), Level.FINEST);
        }

        if (datosSesion != null) {
            boolean comunicacion = datosSesion.existeComunicacion();
            if (comunicacion) {
                //Subimos datos a sesion
                SION.log(Modulo.PAGO_SERVICIOS, "Estableciendo datos de sesion: " + datosSesion.toString(), Level.INFO);

                VistaBarraUsuario.getSesion().setBloqueosVenta(datosSesion);
                datosBloqueo = datosSesion.getMensajeBloqueo();
                if (datosBloqueo == null) {
                    SION.log(Modulo.PAGO_SERVICIOS, "Los datos de bloqueo vienen nulos", Level.INFO);

                    datosSesion.setCdgError(-1);
                    VistaBarraUsuario.getSesion().setBloqueosVenta(datosSesion);

                    return;
                }
                SION.log(Modulo.PAGO_SERVICIOS, "Verificando si el usuario se encuentra bloqueado", Level.INFO);
                mensajeBloqueoAviso = bloqueoVenta.buscarBloqueo(datosBloqueo, VistaBarraUsuario.getSesion().getNombreUsuario());
                if (mensajeBloqueoAviso.estaBloqueado()) {

                    existeBloqueo = true;
                    SION.log(Modulo.PAGO_SERVICIOS, "Usuario bloqueado. Lanzando aplicación de traspasos", Level.INFO);
                    mensajeBloqueo = mensajeBloqueoAviso.getMensajeBloqueo();

                } else {
                    if (mensajeBloqueoAviso.existeAviso()) {

                        existe2oAvisoBloqueo = true;
                        mensaje2oAvisoBloqueo = mensajeBloqueoAviso.getMensajeAviso();
                        SION.log(Modulo.PAGO_SERVICIOS, "El usuario no se encuentra bloqueado pero se encontraron avisos. "
                                + "Mostrando aviso de bloqueo", Level.INFO);

                    } else {
                        SION.log(Modulo.PAGO_SERVICIOS, "No se encontraron bloqueos para el usuario", Level.INFO);
                    }
                }


            }
        }

    }

    public void agregarObjetoSesion(BloqueoDto[] _bloqueos, int _codigoRespuesta) {

        SION.log(Modulo.PAGO_SERVICIOS, "Agregando objetos a sesión.", Level.INFO);

        if (_bloqueos != null) {

            BloqueoVentaBean nuevosDatos = (BloqueoVentaBean) VistaBarraUsuario.getSesion().getBloqueosVenta();
            nuevosDatos.setMensajeBloqueo(bloqueoVenta.bloqueDtoWSaSesion(bloqueosVenta2PagoServicios(_bloqueos)));
            nuevosDatos.setCdgError(0);
            VistaBarraUsuario.getSesion().setBloqueosVenta(nuevosDatos);

        } else if (_codigoRespuesta == 0) {
            BloqueoVentaBean nuevosDatos = new BloqueoVentaBean();
            nuevosDatos.setCdgError(-1);
            VistaBarraUsuario.getSesion().setBloqueosVenta(nuevosDatos);
        } else {
            VistaBarraUsuario.getSesion().setBloqueosVenta(null);
        }

    }

    public neto.sion.venta.servicios.cliente.dto.BloqueoDto[] bloqueosVenta2PagoServicios(BloqueoDto[] _bloqueos) {
        neto.sion.venta.servicios.cliente.dto.BloqueoDto[] bloqueos = new neto.sion.venta.servicios.cliente.dto.BloqueoDto[_bloqueos.length];

        int contador = 0;

        for (BloqueoDto dto : _bloqueos) {
            if (dto != null) {
                neto.sion.venta.servicios.cliente.dto.BloqueoDto bloqueo = new neto.sion.venta.servicios.cliente.dto.BloqueoDto();
                bloqueo.setFiAvisosFalt(dto.getFiAvisosFalt());
                bloqueo.setFiEstatusBloqueoId(dto.getFiEstatusBloqueoId());
                bloqueo.setFiNumAvisos(dto.getFiNumAvisos());
                bloqueo.setFiTipoPagoId(dto.getFiTipoPagoId());
                bloqueos[contador] = bloqueo;
            }

            contador++;
        }

        return bloqueos;
    }

    //fin bloqueos
    public ObservableList<String> getListaDescEmpresas() {
        return listaDescEmpresas;
    }

    public ObservableList<EmpresaBean> getListaEmpresas() {
        return listaEmpresas;
    }
    
    public ObservableList<EmpresaBean> getListaPines() {
        return listaPines;
    }

    public ObservableList<EmpresaBean> getListaEmpresasCabecera() {
        return listaEmpresasCabecera;
    }

    public ObservableList<ServicioBean> getListaPagosAcumulados() {
        return listaServiciosAcumulados;
    }

    public ObservableList<ServicioBean> getListaPagosSeleccionados() {
        return listapagosSeleccionados;
    }

    public ObservableList<ServicioFrecuenteBean> getListaServiciosFrecuentes() {
        return listaServiciosFrecuentes;
    }

    public ArrayList<PeticionPagoServicioDto> getListaPeticionesPago() {
        return listaPeticionesPago;
    }

    public ArrayList<ReferenciaValidaBean> getListaReferenciasValidas() {
        return listaReferenciasValidadas;
    }

    public void cargarListaEmpresas() {

        peticionEmpresasBean = null;
        peticionEmpresasBean = new PeticionEmpresasBean();
        peticionEmpresasBean.setPaisId(VistaBarraUsuario.getSesion().getPaisId());
        peticionEmpresasBean.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());

        SION.log(Modulo.PAGO_SERVICIOS, "Peticion de consulta de empresas a bd local: " + peticionEmpresasBean.toString(), Level.INFO);

        respuestaEmpresasBean = null;
        respuestaEmpresasBean = dao.consultarEmpresas(peticionEmpresasBean);

        if (respuestaEmpresasBean != null) {
            if (respuestaEmpresasBean.getCodigoError() == 0) {
                
                
                for (EmpresaBean bean : respuestaEmpresasBean.getEmpresaBeans()) {
                    if (bean != null) {
                        listaEmpresas.add(bean);
                        listaDescEmpresas.add(bean.getDescrpcion());
                    }
                }
                
                
                /*for (EmpresaBean bean : respuestaEmpresasBean.getEmpresaPines()) {
                    if (bean != null) {
                        listaPines.add(bean);
                        listaDescPines.add(bean.getDescrpcion());
                    }
                }*/
                
            }
        }
        
        SION.log(Modulo.PAGO_SERVICIOS, "Count Lista de empresas: " + listaDescEmpresas.size(), Level.INFO);

        String cadenaEmpresasCabecera = "";
        cadenaEmpresasCabecera = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "emisores.activos.cabecera");

        for (EmpresaBean empresa : listaEmpresas) {
            if (cadenaEmpresasCabecera.contains(",".concat(String.valueOf(empresa.getEmpresaId())).concat(","))) {
                listaEmpresasCabecera.add(empresa);
            }
        }
        
        

        SION.log(Modulo.PAGO_SERVICIOS, "Lista de emisores encontrados a mostrar en frecuentes: " + listaEmpresasCabecera.toString(), Level.INFO);

        /*
         * EmpresaBean empresaJafra = new EmpresaBean();
         * empresaJafra.setDescrpcion("JAFRA"); empresaJafra.setEmpresaId(107);
         * listaEmpresas.add(empresaJafra);
         * listaDescEmpresas.add(empresaJafra.getDescrpcion());
         */
    }

    public boolean esEmisorActivo(long _emisor) {
        boolean esActivo = false;
        for (EmpresaBean empresa : respuestaEmpresasBean.getEmpresaBeans()) {
            if (empresa != null) {
                if (_emisor == empresa.getEmpresaId()) {
                    esActivo = true;
                    break;
                }
            }
        }
        return esActivo;
    }
    
    

    public double getImporteTotalPago() {
        double importe = 0;
        for (ServicioBean bean : listaServiciosAcumulados) {
            if (bean.getCheckId()) {
                importe += bean.getImporte() + bean.getComision() /*
                         * + bean.getImpuesto()
                         */;
            }
        }
        return importe;
    }

    public PeticionReferenciaDto getPeticionValidaReferencia() {
        return peticionValidaReferenciaDto;
    }

    public RespuestaReferenciaDto getRespuestaValidaReferencia() {
        return respuestaReferenciaDto;
    }

    public boolean esReferenciaValidaSwitch(String _referencia, double _importe, int _emisorId, int _tipoPago) {
        boolean esValida = false;
        peticionValidaReferenciaDto = null;
        respuestaReferenciaDto = null;
        peticionValidaReferenciaDto = new PeticionReferenciaDto();
        respuestaReferenciaDto = new RespuestaReferenciaDto();

        peticionValidaReferenciaDto.setPaisId(VistaBarraUsuario.getSesion().getPaisId());
        peticionValidaReferenciaDto.setAgenteId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.AGENTEID")));
        peticionValidaReferenciaDto.setSubsidiariaId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SUBSIDIARIAID")));
        peticionValidaReferenciaDto.setSucursalId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SUCURSALID")));
        peticionValidaReferenciaDto.setEmisorId(_emisorId);
        peticionValidaReferenciaDto.setModuloId(_emisorId);
        peticionValidaReferenciaDto.setNegocioId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.NEGOCIOID")));
        peticionValidaReferenciaDto.setEstacion(VistaBarraUsuario.getSesion().getIpEstacion());
        peticionValidaReferenciaDto.setReferencia(_referencia);
        peticionValidaReferenciaDto.setImporte(_importe);
        peticionValidaReferenciaDto.setUsuarioId(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.USUARIOID"));
        peticionValidaReferenciaDto.setAdicionales("");
        peticionValidaReferenciaDto.setParametros("");
        peticionValidaReferenciaDto.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
        SION.log(Modulo.PAGO_SERVICIOS, "peticion de validacion de referencia: " + peticionValidaReferenciaDto.toString(), Level.INFO);

        try {
            respuestaReferenciaDto = cliente.validarReferencia(peticionValidaReferenciaDto);
        } catch (Exception e) {
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió un error al llamar metodo de validacion de referencia", Level.SEVERE);
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
            respuestaReferenciaDto = null;
        }
        /*respuestaReferenciaDto.setAdicionales("<Impuesto></Impuesto><Cliente>ERICK HERNANDEZ FRANCISCO</Cliente><Telefono></Telefono><Contrato></Contrato><Referencia>19510955</Referencia><Saldo></Saldo><Cuenta></Cuenta><IVAEmisor>1.12</IVAEmisor><Sufijo></Sufijo><idEmisor>283</idEmisor>");
        respuestaReferenciaDto.setComisionCliente(0);
        respuestaReferenciaDto.setComisionEmisor(0);
        respuestaReferenciaDto.setDescResp("Operacion exitosa");
        respuestaReferenciaDto.setIva(0);
        respuestaReferenciaDto.setNoCuenta("01720157410788");
        respuestaReferenciaDto.setNombreGrupo("");
        respuestaReferenciaDto.setParametros("|switchCon:1~retencion:2~cuentaConcentra:01720157410788~cobraComision:0~sucursalid:1~canalid:2018~cekt:7.0~importe:550.0~idemisor:283~cliente:ERICK HERNANDEZ FRANCISCO~referencia:19510955~ivaemisor:1.12");
        respuestaReferenciaDto.setRespuestaId(0);
        respuestaReferenciaDto.setUid("30269867226");*/

        if (respuestaReferenciaDto != null) {
            SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de validacion de referencia: " + respuestaReferenciaDto.toString(), Level.INFO);
             
            
            if (respuestaReferenciaDto.getRespuestaId() == 0) {
                esValida = true;
                if (peticionValidaReferenciaDto.getEmisorId() == EMISOR_ORIFLAME) {
                    //emisor oriflame
                    if (respuestaReferenciaDto.getAdicionales().contains("<PagosCliente>")
                            && respuestaReferenciaDto.getAdicionales().contains("</PagosCliente>")) {

                        oriflame = null;
                        oriflame = new OriflameVista(utilerias, this, _tipoPago);
                        oriflame.abreStageOriflame(respuestaReferenciaDto.getAdicionales().substring(
                                respuestaReferenciaDto.getAdicionales().indexOf("<PagosCliente>"),
                                respuestaReferenciaDto.getAdicionales().indexOf("</PagosCliente>")),
                                getRespuestaValidaReferencia().getComisionCliente()
                                + getRespuestaValidaReferencia().getIva(), listaServiciosAcumulados.toArray(new ServicioBean[0]));
                    }
                } else if (peticionValidaReferenciaDto.getEmisorId() == EMISOR_MAZ) {

                    SION.log(Modulo.PAGO_SERVICIOS, "Validando respuesta de validacion de referencia Micro Negocio", Level.INFO);
                    ///se agrega validacion de nombre de grupo
                    if (respuestaReferenciaDto.getNombreGrupo() != null
                            && !respuestaReferenciaDto.getNombreGrupo().isEmpty()) {
                        maz = null;
                        maz = new MAZVista(utilerias, _tipoPago, this);
                        maz.abreStageMAZ(respuestaReferenciaDto, getRespuestaValidaReferencia().getComisionCliente());
                    } else {
                        SION.log(Modulo.PAGO_SERVICIOS, "Se recibe nombre de grupo en abono de Maz vacío, se cancela el pago", Level.SEVERE);
                        respuestaReferenciaDto = null;
                        esValida = false;
                    }

                } else if(peticionValidaReferenciaDto.getEmisorId() == EMISOR_TOTALPLAY){
                    
                    SION.log(Modulo.PAGO_SERVICIOS, "Validando respuesta de validacion de referencia TotalPlay", Level.INFO);
                    if(respuestaReferenciaDto.getAdicionales()!=null
                            && !respuestaReferenciaDto.getAdicionales().isEmpty()){
                        ventanaConfirmacion = null;
                        ventanaConfirmacion =  new ConfirmacionReferenciaVista(utilerias, _tipoPago, this, ConfirmacionPago.TOTALPLAY);
                        ventanaConfirmacion.abreStageConfirmacion(respuestaReferenciaDto, getRespuestaValidaReferencia().getComisionCliente());
                        
                    }else{
                        
                        SION.log(Modulo.PAGO_SERVICIOS, "Se recibe titular del pago Totalplay vacío, se cancela el pago", Level.SEVERE);
                        respuestaReferenciaDto = null;
                        esValida = false;
                        
                    }
                    
                } else if(peticionValidaReferenciaDto.getEmisorId() == EMISOR_IUSACELL_SECSA){
                    
                    SION.log(Modulo.PAGO_SERVICIOS, "Validando respuesta de validacion de referencia Iusacell Secsa", Level.INFO);
                    if(respuestaReferenciaDto.getAdicionales()!=null
                            && !respuestaReferenciaDto.getAdicionales().isEmpty()){
                        ventanaConfirmacion = null;
                        ventanaConfirmacion =  new ConfirmacionReferenciaVista(utilerias, _tipoPago, this, ConfirmacionPago.IUSACELL_SECSA);
                        ventanaConfirmacion.abreStageConfirmacion(respuestaReferenciaDto, getRespuestaValidaReferencia().getComisionCliente());
                        
                    }else{
                        
                        SION.log(Modulo.PAGO_SERVICIOS, "Se recibe titular del pago Iusacell Secsavacío, se cancela el pago", Level.SEVERE);
                        respuestaReferenciaDto = null;
                        esValida = false;
                        
                    }
                    
                } else if(peticionValidaReferenciaDto.getEmisorId() == EMISOR_IUSACELL){
                    
                    SION.log(Modulo.PAGO_SERVICIOS, "Validando respuesta de validacion de referencia Iusacell", Level.INFO);
                    if(respuestaReferenciaDto.getAdicionales()!=null
                            && !respuestaReferenciaDto.getAdicionales().isEmpty()){
                        ventanaConfirmacion = null;
                        ventanaConfirmacion =  new ConfirmacionReferenciaVista(utilerias, _tipoPago, this, ConfirmacionPago.IUSACELL);
                        ventanaConfirmacion.abreStageConfirmacion(respuestaReferenciaDto, getRespuestaValidaReferencia().getComisionCliente());
                        
                    }else{
                        
                        SION.log(Modulo.PAGO_SERVICIOS, "Se recibe titular del pago Iusacell Secsavacío, se cancela el pago", Level.SEVERE);
                        respuestaReferenciaDto = null;
                        esValida = false;
                        
                    }
                    
                } else if (peticionValidaReferenciaDto.getEmisorId() == EMISOR_IZZI){
                    
                     SION.log(Modulo.PAGO_SERVICIOS, "Validando respuesta de validacion de referencia Izzi", Level.INFO);
                    if(respuestaReferenciaDto.getAdicionales()!=null
                            && !respuestaReferenciaDto.getAdicionales().isEmpty()){
                     
                        
                        
                        SION.log(Modulo.PAGO_SERVICIOS, "Validando emisor en la respuesta de referencia de emisor IZZI", Level.INFO);
                        
                        String emisor = respuestaReferenciaDto.getAdicionales().substring(respuestaReferenciaDto.getAdicionales().indexOf("<idEmisor>")+10, respuestaReferenciaDto.getAdicionales().indexOf("</idEmisor>"));
                        SION.log(Modulo.PAGO_SERVICIOS, "Emisor encontrado en la cadena adicionales: "+emisor, Level.INFO);
                        
                        
                        SION.log(Modulo.PAGO_SERVICIOS, "Cambiando emisor en la respuesta", Level.INFO);
                        peticionValidaReferenciaDto.setEmisorId(Integer.parseInt(emisor));
                        peticionValidaReferenciaDto.setModuloId(Integer.parseInt(emisor));
                        SION.log(Modulo.PAGO_SERVICIOS, "Nueva peticion de referencia: "+peticionValidaReferenciaDto.toString(), Level.INFO);
                        
                        agregarReferencia(_referencia, Integer.parseInt(emisor));
                        
                    }
                    
                }else if (peticionValidaReferenciaDto.getEmisorId() == EMISOR_DINEROEXP){
                    
                     SION.log(Modulo.PAGO_SERVICIOS, "Validando respuesta de validacion de referencia Dinero Express", Level.INFO);
                    if(respuestaReferenciaDto.getAdicionales()!=null
                            && !respuestaReferenciaDto.getAdicionales().isEmpty()){
                     
                        
                        
                        SION.log(Modulo.PAGO_SERVICIOS, "Agregando el importe congtenido en datos adicionales", Level.INFO);
                        
                        
                        double importeAdicional = Double.valueOf(respuestaReferenciaDto.getAdicionales().substring(
                                respuestaReferenciaDto.getAdicionales().indexOf("<Saldo>")+7,
                                respuestaReferenciaDto.getAdicionales().indexOf("</Saldo>")));
                        
                        peticionValidaReferenciaDto.setImporte(importeAdicional);
                        
                        
                        
                        agregarReferencia(_referencia, _emisorId);
                        
                    }
                    
                }else {
                    //flujo defalut para emisores que no necesitan casos especiales
                    agregarReferencia(_referencia, _emisorId);
                }
            }
        } else {
            SION.log(Modulo.PAGO_SERVICIOS, "Se recibe respuesta de validacion nula", Level.SEVERE);
            respuestaReferenciaDto = null;
            esValida = false;
        }

        return esValida;
    }

    public OriflameVista getOriflame() {
        return oriflame;
    }
    
    public void agregaReferenciaIusacell(int _tipoPago, double _comision){
        
        SION.log(Modulo.PAGO_SERVICIOS, "Agregando referencia Iusacell", Level.INFO);
        
        if (respuestaReferenciaDto.getUid() == null || respuestaReferenciaDto.getUid().isEmpty()) {
            respuestaReferenciaDto.setUid(String.valueOf(peticionValidaReferenciaDto.getUId()));
        }
        
        listaReferenciasValidadas.add(new ReferenciaValidaBean(peticionValidaReferenciaDto.getReferencia(),
                "", EMISOR_IUSACELL, respuestaReferenciaDto));
        
        binding.agregaPagoIusacell(_tipoPago, _comision);
        
    }
    
    public void agregaReferenciaIusacell_Secsa(int _tipoPago, double _comision){
        
        SION.log(Modulo.PAGO_SERVICIOS, "Agregando referencia Iusacell Secsa", Level.INFO);
        
        if (respuestaReferenciaDto.getUid() == null || respuestaReferenciaDto.getUid().isEmpty()) {
            respuestaReferenciaDto.setUid(String.valueOf(peticionValidaReferenciaDto.getUId()));
        }
        
        listaReferenciasValidadas.add(new ReferenciaValidaBean(peticionValidaReferenciaDto.getReferencia(),
                "", EMISOR_IUSACELL_SECSA, respuestaReferenciaDto));
        
        binding.agregaPagoIusacell_Secsa(_tipoPago, _comision);
        
    }
    
    public void agregaReferenciaTotalplay(int _tipoPago, double _comision){
        
        SION.log(Modulo.PAGO_SERVICIOS, "Agregando referencia TotalPlay", Level.INFO);
        
        if (respuestaReferenciaDto.getUid() == null || respuestaReferenciaDto.getUid().isEmpty()) {
            respuestaReferenciaDto.setUid(String.valueOf(peticionValidaReferenciaDto.getUId()));
        }
        
        listaReferenciasValidadas.add(new ReferenciaValidaBean(peticionValidaReferenciaDto.getReferencia(),
                "", EMISOR_TOTALPLAY, respuestaReferenciaDto));
        
        binding.agregaPagoTotalplay(_tipoPago, _comision);
        
    }

    public void agregaReferenciaMAZ(int _tipoPago, double _comision) {
        
        SION.log(Modulo.PAGO_SERVICIOS, "Agregando referencia MAZ", Level.INFO);

        if (respuestaReferenciaDto.getUid() == null || respuestaReferenciaDto.getUid().isEmpty()) {
            respuestaReferenciaDto.setUid(String.valueOf(peticionValidaReferenciaDto.getUId()));
        }

        listaReferenciasValidadas.add(new ReferenciaValidaBean(peticionValidaReferenciaDto.getReferencia(),
                "", EMISOR_MAZ, respuestaReferenciaDto));

        binding.agregaPagoMAZ(_tipoPago, _comision);
    }

    public void agregaReferenciaOriflame(int _tipoPago, PagoOriflameBean pago, double comision) {
        
        SION.log(Modulo.PAGO_SERVICIOS, "Agregando referencia Oriflame", Level.INFO);
        

        listaReferenciasValidadas.add(new ReferenciaValidaBean(peticionValidaReferenciaDto.getReferencia(),
                pago.getSubReferencia(), EMISOR_ORIFLAME, respuestaReferenciaDto));
        //solo se usa en pruebas
        //listaReferenciasValidadas.add(new ReferenciaValidaBean("15974467", pago.getSubReferencia(),
        //       EMISOR_ORIFLAME, new RespuestaValidaReferenciaDto("123", "ok", "addd", "param", 0, 0, 0, 0, "1234567890")));
        binding.agregaPagoOriflame(_tipoPago, pago, comision);
    }

    public void agregarReferencia(String _referencia, int _emisorId) {
        
        SION.log(Modulo.PAGO_SERVICIOS, "Agregando referencia.", Level.INFO);

        ////se valida que si el switch agrega o cambia la referencia en el campo adicionales esta se actualize
        String adicionales = respuestaReferenciaDto.getAdicionales();
        String nuevaReferencia = null;

        if (adicionales.contains("<Referencia>") && adicionales.contains("</Referencia>")) {
            nuevaReferencia = adicionales.substring(adicionales.indexOf("<Referencia>") + 12, adicionales.indexOf("</Referencia>"));
            if (nuevaReferencia.trim().equals(_referencia.trim())) {
                nuevaReferencia = null;
            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "Se detecta que referencia obtenida por el switch es diferente a la enviada en la peticion.", Level.INFO);
                SION.log(Modulo.PAGO_SERVICIOS, "Enviada: " + _referencia + ", Recibida: " + nuevaReferencia + ", se procede a actualizar", Level.INFO);
            }
        }

        if (nuevaReferencia != null) {
            listaReferenciasValidadas.add(new ReferenciaValidaBean(nuevaReferencia, "", _emisorId, respuestaReferenciaDto));
        } else {
            listaReferenciasValidadas.add(new ReferenciaValidaBean(_referencia, "", _emisorId, respuestaReferenciaDto));
        }
    }

    public String getMensajeReferencia() {
        if (respuestaReferenciaDto != null) {
            return respuestaReferenciaDto.getDescResp();
        } else {
            return "";
        }
    }

    private double getIVAReferencia(ServicioBean _bean) {
        double iva = 0;

        for (ReferenciaValidaBean referencia : listaReferenciasValidadas) {
            if (referencia.getEmisorId() == _bean.getEmisorId()
                    && referencia.getReferencia().trim().equals(_bean.getReferencia().trim())) {
                iva = referencia.getRespuestaReferencia().getIva();
                break;
            }
        }

        return iva;
    }

    public RespuestaServiciosFrecuentesBean consultarServiciosFrecuentes(int _paisId, long _tiendaId) {
        peticionServiciosFrecuentesBean = null;
        respuestaServiciosFrecuentesBean = null;

        peticionServiciosFrecuentesBean = new PeticionServiciosFrecuentesBean();
        peticionServiciosFrecuentesBean.setPaisId(_paisId);
        peticionServiciosFrecuentesBean.setTiendaId(_tiendaId);

        //cargar en duro
        //respuestaServiciosFrecuentesBean = dao.consultarServiciosFrecuentes(peticionServiciosFrecuentesBean);

        //cargar de loq ue se encontro en properties
        ArrayList<ServicioFrecuenteBean> servicios = new ArrayList<ServicioFrecuenteBean>();
        for (EmpresaBean empresa : listaEmpresasCabecera) {
            ServicioFrecuenteBean frecuente = new ServicioFrecuenteBean();
            frecuente.setDescripcion(empresa.getDescrpcion());
            frecuente.setModuloId(empresa.getEmpresaId());
            servicios.add(frecuente);
        }

        if (!servicios.isEmpty()) {
            respuestaServiciosFrecuentesBean = new RespuestaServiciosFrecuentesBean();
            respuestaServiciosFrecuentesBean.setCodigoError(0);
            respuestaServiciosFrecuentesBean.setDescError("OK");
            respuestaServiciosFrecuentesBean.setServicioFrecuenteBeans(servicios.toArray(new ServicioFrecuenteBean[0]));
        }

        return respuestaServiciosFrecuentesBean;
    }

    public void agregarTipoPago(double _montoPago, double _montorecibido, int _tipoPago) {
        TipoPagoServicioDto pagoDto = new TipoPagoServicioDto();
        pagoDto.setFiMontoRecibido(_montorecibido);
        pagoDto.setFiTipoPagoId(_tipoPago);
        if (_tipoPago == EFECTIVO) {
            pagoDto.setImporteAdicional(_montorecibido);
        } else {
            pagoDto.setImporteAdicional(calcularComision());
        }
        pagoDto.setFnNumeroVales(0);
        pagoDto.setFnMontoPago(_montoPago);
        pagoDto.setPaPagoTarjetaIdBus(0);
        pagoDto.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
        listaTiposPago.add(pagoDto);
    }

    private double calcularComision() {
        return 0;
    }

    public void agregarPeticionPago(ServicioBean _bean) {

        ///verificar si se envia id de sucursal real o generico
        boolean existeParametroSucursal = false;

        parametro = null;
        parametro = utilerias.consultaParametro(UtileriasVenta.Accion.SUCURSAL_PAGO_SERVICIOS, Modulo.PAGO_SERVICIOS, Modulo.PAGO_SERVICIOS);
        if (parametro != null) {
            if (parametro.getCodigoError() == 0) {
                if (parametro.getValorConfiguracion() == 1) {
                    existeParametroSucursal = true;
                }
            }
        }

        PeticionPagoServicioDto peticion = new PeticionPagoServicioDto();

        if (_bean.getEmisorId() == EMISOR_ORIFLAME) {
            peticion.setAdicionales(generarAdicionalesOriflame(_bean));
        } else {
            peticion.setAdicionales(generarAdicionalesPago(_bean));
        }

        peticion.setAgenteId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "AGENTE.ID")));
        peticion.setComision(Double.parseDouble(utilerias.getNumeroFormateado(_bean.getComision() - getIVAReferencia(_bean))));
        //peticion.setComision(Double.parseDouble(utilerias.getNumeroFormateado(_bean.getComision() - getIVA())));
        peticion.setEstacion(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "ip.servidor.neto"));
        //peticion.setImpuesto(calcularImpuesto(_bean.getComision()));
        peticion.setImpuesto(_bean.getImpuesto());
        peticion.setModuloId(_bean.getEmisorId());
        peticion.setMonedaId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "MONEDA.ID")));
        peticion.setMonto(_bean.getImporte());
        peticion.setMovimientoId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "MOVIMIENTO.ID")));
        peticion.setNegocioId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "NEGOCIO.ID")));
        peticion.setPaisId(VistaBarraUsuario.getSesion().getPaisId());
        peticion.setParametros(getParametrosValidacionReferencia(_bean));
        peticion.setProductoId(Long.parseLong(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PRODUCTO.ID")));
        peticion.setReferencia(_bean.getReferencia());
        peticion.setSubsidiariaId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "SUBSIDIARIA.ID")));
        if (existeParametroSucursal) {
            peticion.setSucursalId((int) VistaBarraUsuario.getSesion().getTiendaId());
        } else {
            peticion.setSucursalId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "SUCURSAL.ID")));
        }

        peticion.setUid(getUidValidacionReferencia(_bean));
        
        //validar el codigo de barras es enviar en caso de ser IZZI setear en duro para los ID 283 y 306
        if(_bean.getEmisorId()==283){
            peticion.setCodigoBarras(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "IZZI.SKU.RED"));
        }else if(_bean.getEmisorId()==306){
            peticion.setCodigoBarras(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "IZZI.SKU.MAS"));
        }else{
            peticion.setCodigoBarras(buscaCodigoxId(_bean));
        }
        peticion.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
        peticion.setUsuarioId(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.USUARIOID").trim());
        //peticion.setUsuarioId("H2HNETOUSR");

        SION.log(Modulo.PAGO_SERVICIOS, "Peticion de pago agregada: " + peticion.toString(), Level.INFO);
        listaPeticionesPago.add(peticion);
    }

    private String buscaCodigoxId(ServicioBean _bean) {
        String codigoBarras = "";
        for (EmpresaBean bean : listaEmpresas) {
            if (bean.getEmpresaId() == _bean.getEmisorId()) {
                codigoBarras = bean.getCodigoBarras();
            }
        }
        return codigoBarras;
    }

    private double calcularImpuesto(double _comision) {
        return _comision * .16;
    }

    private String generarAdicionalesPago(ServicioBean _bean) {
        String adicionales = "";

        adicionales += SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "SUBSIDIARIA.ID").concat("|");
        adicionales += SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "SUBSIDIARIA.DESC").concat("|");
        adicionales += String.valueOf(VistaBarraUsuario.getSesion().getTiendaId()).concat("|");
        adicionales += VistaBarraUsuario.getSesion().getNombreTienda().concat("|");
        adicionales += VistaBarraUsuario.getSesion().getDatosTienda().getCodigoPostal();

        //adicionales = "001|Subsidiaria 001|001|Sucural 001|59040";

        return adicionales;
    }

    private String generarAdicionalesOriflame(ServicioBean _bean) {
        String adicionales = "";

        adicionales += _bean.getDeposito();
        adicionales += "|";
        adicionales += String.valueOf(_bean.getImporte());

        return adicionales;
    }

    private String getParametrosValidacionReferencia(ServicioBean _bean) {
        String parametros = "";

        for (ReferenciaValidaBean bean : listaReferenciasValidadas) {
            if (bean != null) {
                if (bean.getEmisorId() == _bean.getEmisorId()
                        && (bean.getReferencia().equals(_bean.getReferencia())
                        || _bean.getReferencia().trim().contains(bean.getReferencia().trim()))) {
                    parametros = bean.getRespuestaReferencia().getParametros();
                }
            }
        }

        return parametros;
    }

    private String getUidValidacionReferencia(ServicioBean _bean) {
        String parametros = "";

        for (ReferenciaValidaBean bean : listaReferenciasValidadas) {
            if (bean != null) {
                if (bean.getEmisorId() == _bean.getEmisorId()
                        && (bean.getReferencia().equals(_bean.getReferencia())
                        || _bean.getReferencia().trim().contains(bean.getReferencia().trim()))) {
                    parametros = bean.getRespuestaReferencia().getUid();
                }
            }
        }

        return parametros;
    }

    public RespuestaVentaServiciosDto getRespuestaPagoServicios() {
        return respuestaVentaServiciosDto;
    }

    public PeticionVentaServiciosDto getPeticionPagoServicios() {
        return peticionVentaServiciosDto;
    }

    public void pagarServicios(double _montoVenta, int _tipoMovto) {

        SION.log(Modulo.PAGO_SERVICIOS, "Lista de servicios en la peticion: " + listaPeticionesPago.toString(), Level.INFO);
        //limpiar arreglos
        arregloPeticionesPagoDto = null;
        arregloRespuestasPagoDto = null;
        arregloTiposPago = null;
        //limpiar objetos de peticion y rspuesta
        peticionVentaServiciosDto = null;
        respuestaVentaServiciosDto = null;
        //inicializar
        arregloPeticionesPagoDto = new PeticionPagoServicioDto[listaPeticionesPago.size()];
        arregloRespuestasPagoDto = new RespuestaPagoServicioDto[listaPeticionesPago.size()];
        arregloTiposPago = new TipoPagoServicioDto[listaTiposPago.size()];
        peticionVentaServiciosDto = new PeticionVentaServiciosDto();
        respuestaVentaServiciosDto = new RespuestaVentaServiciosDto();
        
        //se genera numero de conciliacion
        generarConciliacion(
                Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SISTEMA")),//1
                Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.MODULO")),//2
                Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SUBMODULO")));//9

        if (respuestaConciliacionLocalBean != null) {
            SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de conciliacion: " + respuestaConciliacionLocalBean.toString(), Level.INFO);
            if (respuestaConciliacionLocalBean.getPaCdgError() == 0) {

                String t = VistaBarraUsuario.getSesion().getIpEstacion();
                t = t.concat("|CAJA");
                t = t.concat(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));
                //se llena arreglo de peticiones de servicios
                llenarArregloServicios();
                //llenar arreglo de tipos de pago
                llenarArregloFormasPago();
                //se llena peticion general
                peticionVentaServiciosDto = new PeticionVentaServiciosDto();
                peticionVentaServiciosDto.setPaFechaOper(obtenerFecha());

                peticionVentaServiciosDto.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
                peticionVentaServiciosDto.setPaTipoMovto(7);
                peticionVentaServiciosDto.setPaUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());
                peticionVentaServiciosDto.setArrayTiposPago(arregloTiposPago);
                peticionVentaServiciosDto.setArrayPagos(arregloPeticionesPagoDto);
                peticionVentaServiciosDto.setPaMontoTotalVta(_montoVenta);
                peticionVentaServiciosDto.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
                peticionVentaServiciosDto.setPaConciliacionId(respuestaConciliacionLocalBean.getPaConciliacionId());
                peticionVentaServiciosDto.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
                peticionVentaServiciosDto.setPaTerminal(t);
                peticionVentaServiciosDto.setPaFueraLinea(0);

                SION.log(Modulo.PAGO_SERVICIOS, "Petición de pago de servicios: " + peticionVentaServiciosDto.toString(), Level.INFO);
                
                try {
                    errorAxis = "";
                    respuestaVentaServiciosDto = cliente.registrarVentaServicios(peticionVentaServiciosDto);
                    SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de pago de servicios: " + respuestaVentaServiciosDto.toString(), Level.INFO);
                } catch (SionException ex) {
                    respuestaVentaServiciosDto = null;
                    if (ex.getCodigoAccion() == Constantes.ACCION.COMPROBAR_TRANSACCION) {
                        errorAxis = "Error";
                    } else{
                        errorAxis = "ErrorOtro";
                    }
                    SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un problema al registrar los servicios time out ", Level.SEVERE);
                    SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex);
                } catch (Exception ex) {
                    errorAxis = "ErrorOtro";
                    respuestaVentaServiciosDto = null;
                    SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un problema al registrar los servicios", Level.SEVERE);
                    SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex);
                }
                
                RespuestaConsultaTransaccionCentralDto respuestaConsulta;
                int banderaContador = 0;
                if(errorAxis.equals("Error")){
                    SION.log(Modulo.PAGO_SERVICIOS, "Entro a error", Level.SEVERE);
                    neto.sion.venta.servicios.cliente.dto.PeticionTransaccionCentralDto peticionConsulta= new neto.sion.venta.servicios.cliente.dto.PeticionTransaccionCentralDto();
                    peticionConsulta.setPaConciliacionId(respuestaConciliacionLocalBean.getPaConciliacionId());
                    peticionConsulta.setPaModuloId(2);
                    peticionConsulta.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
                    peticionConsulta.setPaSistemaId(1);
                    peticionConsulta.setPaSubModuloId(9);
                    peticionConsulta.setPaUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());
                    peticionConsulta.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
                    respuestaConsulta = new RespuestaConsultaTransaccionCentralDto();
                    for (int i = 0; i < Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "ps.noReintentosTimeOut")); i++) {    
                        try {
                            SION.log(Modulo.PAGO_SERVICIOS, "Antes de peticion de consulta", Level.SEVERE);
                            respuestaConsulta = clienteVenta.consultarTransaccionCentral(peticionConsulta);
                            //MODIFICAR EL DE CONSULTA
                            SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de pago de servicios: " + respuestaConsulta.toString(), Level.INFO);
                        } catch (SionException ex) {
                            if (ex.getCodigoAccion() == Constantes.ACCION.COMPROBAR_TRANSACCION) {
                                respuestaConsulta = null;
                                SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un problema al registrar los servicios time out", Level.SEVERE);
                            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex);
                                try {
                                    Thread.sleep(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "ps.tiempoEsperaReintentos")));
                                } catch (InterruptedException ex1) {
                                    SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex1, new String[0]);
                                }
                            }
                        } catch (Exception ex) {
                            respuestaConsulta = null;
                            StringWriter sw = new StringWriter();
                            ex.printStackTrace(new PrintWriter(sw));
                            String exceptionAsString = sw.toString();
                            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un problema al registrar los servicios "+exceptionAsString, Level.SEVERE);
                            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex);
                        }
                        if (respuestaConsulta != null && respuestaConsulta.getPaCdgError()==0 && respuestaConsulta.getPaTransaccionId()!=-1) {
                            SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de consulta de pago de servicios codigo 0: " + respuestaConsulta.toString(), Level.SEVERE);
                            break;
                        }else{
                            if(respuestaConsulta != null) {
                                SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de consulta de pago de servicios: " + respuestaConsulta.toString(), Level.SEVERE);
                            }
                            respuestaConsulta = null;
                            if(banderaContador==0){
                                i = 0;
                                banderaContador = 1;
                            }
                            SION.log(Modulo.PAGO_SERVICIOS, "Antes del sleep", Level.SEVERE);
                            try {
                                Thread.sleep(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "ps.tiempoEsperaReintentos")));
                            } catch (InterruptedException ex1) {
                                SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex1, new String[0]);
                            }
                            SION.log(Modulo.PAGO_SERVICIOS, "Despues del sleep", Level.SEVERE);
                        }
                    }
                    if (respuestaConsulta != null) {
                        SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de consulta de pago de servicios final+++: " + respuestaConsulta.toString(), Level.SEVERE);
                    }
                    if(respuestaConsulta!=null && respuestaConsulta.getPaCdgError()==0 && respuestaConsulta.getPaTransaccionId()!=0){
                        respuestaVentaServiciosDto = new RespuestaVentaServiciosDto();
                        PagoServicioVentaDto [] arreglo = new PagoServicioVentaDto[1];
                        PagoServicioVentaDto peticion = new PagoServicioVentaDto();                        
                        peticion.setFcCdgBarras(peticionVentaServiciosDto.getArrayPagos()[0].getCodigoBarras());
                        peticion.setFcMensaje(peticionVentaServiciosDto.getArrayPagos()[0].getParametros());
                        peticion.setFcreferencia(peticionVentaServiciosDto.getArrayPagos()[0].getReferencia());
                        peticion.setFiArticuloId(peticionVentaServiciosDto.getArrayPagos()[0].getModuloId());
                        peticion.setFiPagoServiciosId(peticionVentaServiciosDto.getArrayPagos()[0].getIdReferencia());
                        peticion.setMovimientoId(peticionVentaServiciosDto.getArrayPagos()[0].getMovimientoId());
                        arreglo[0] = peticion;
                        respuestaVentaServiciosDto.setPaArrayBloqueos(null);
                        respuestaVentaServiciosDto.setPaArrayServicios(arreglo);
                        respuestaVentaServiciosDto.setPaCdgError(0);
                        respuestaVentaServiciosDto.setPaDescError("");
                        respuestaVentaServiciosDto.setPaTransaccionId(respuestaConsulta.getPaTransaccionId());
                        SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de consulta "+respuestaConsulta.toString(), Level.SEVERE);
                    }else{
                        respuestaVentaServiciosDto = new RespuestaVentaServiciosDto();
                        respuestaVentaServiciosDto.setPaCdgError(-1);
                    }
                }
                if(errorAxis.equals("ErrorOtro")){
                    respuestaVentaServiciosDto = new RespuestaVentaServiciosDto();
                    respuestaVentaServiciosDto.setPaCdgError(-1);
                }
            } else {
                respuestaVentaServiciosDto.setPaArrayBloqueos(null);
                respuestaVentaServiciosDto.setPaArrayServicios(null);
                respuestaVentaServiciosDto.setPaCdgError(respuestaConciliacionLocalBean.getPaCdgError());
                respuestaVentaServiciosDto.setPaDescError(respuestaConciliacionLocalBean.getPaDescError());
                respuestaVentaServiciosDto.setPaTransaccionId(0);
                SION.log(Modulo.PAGO_SERVICIOS, respuestaConciliacionLocalBean.getPaDescError(), Level.SEVERE);
            }

        } else {
            //respuestaVentaServiciosDto = null;
            respuestaVentaServiciosDto = new RespuestaVentaServiciosDto();
            respuestaVentaServiciosDto.setPaCdgError(-1);
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió un error al generar número de conicliación", Level.SEVERE);
        }

    }

    private void llenarArregloFormasPago() {
        int contador = 0;
        for (TipoPagoServicioDto dto : listaTiposPago) {
            if (dto != null) {
                arregloTiposPago[contador] = dto;
                contador++;
            }
        }
    }

    private void llenarArregloServicios() {
        int contador = 0;
        for (PeticionPagoServicioDto dto : listaPeticionesPago) {
            if (dto != null) {
                arregloPeticionesPagoDto[contador] = dto;
                contador++;
            }
        }
    }

    public double getComisionPorServicios() {
        double comision = 0;
        for (PeticionPagoServicioDto dto : arregloPeticionesPagoDto) {
            if (dto != null) {
                comision += dto.getComision();
            }
        }
        return comision;
    }

    ///tickets
    public void armarTicket(double _montoRecibido, double _montoCambio, boolean esFueraLinea) {

        SION.log(Modulo.PAGO_SERVICIOS, "Construyendo ticket de pago de servicios", Level.INFO);

        ticket = null;
        ticket = new VentaServiciosTicketDto();
        seImprimioTicketExitoso = false;

        cadenaDireccion = "";
        cadenaDelegacion = "";

        boolean esSuperPrecio = false;
        parametro = null;
        parametro = new RespuestaParametroBean();
        parametro = utilerias.consultaParametro(UtileriasVenta.Accion.ENCABEZADO_TICKET, Modulo.VENTA, Modulo.PAGO_SERVICIOS);
        if (parametro != null) {
            if (parametro.getCodigoError() == 0 && parametro.getValorConfiguracion() != 1) {
                esSuperPrecio = true;
            }
        }

        cadenaDireccion = cadenaDireccion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getCalle()));
        cadenaDireccion = cadenaDireccion.concat(" ");
        cadenaDireccion = cadenaDireccion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroExterior()));
        if (!"".equals(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior())
                && !"0".equals(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior())
                && VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior() != null) {
            cadenaDireccion = cadenaDireccion.concat("-");
            cadenaDireccion = cadenaDireccion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getNumeroInterior()));
        }

        cadenaDelegacion = cadenaDelegacion.concat(String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getMunicipio()).concat(" ").concat(
                String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getEstado())).concat(" ").concat(
                String.valueOf(VistaBarraUsuario.getSesion().getDatosTienda().getCodigoPostal())));

        if (esFueraLinea) {
            ticket.setCaja(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));
            ticket.setCajero(String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()));
            ticket.setCalleNumeroTienda(cadenaDireccion);
            ticket.setColoniaTienda(VistaBarraUsuario.getSesion().getDatosTienda().getColonia());
            ticket.setDelegacionCiudadEstadoCP(cadenaDelegacion);
            if (esSuperPrecio) {
                ticket.setEncabezado(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ENCABEZADO.TICKET.SUPR"));
            } else {
                ticket.setEncabezado(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ENCABEZADO.TICKET"));
            }
            ticket.setFecha(obtenerFecha().substring(0, obtenerFecha().indexOf(" ")));
            ticket.setHora(obtenerFecha().substring(10));
            ticket.setNombreTienda(VistaBarraUsuario.getSesion().getNombreTienda());
            ticket.setFolio(String.valueOf(String.valueOf(respuestaPagoLocal.getConciliacionId()).concat("O")));
            ticket.setServicios(arrayPagoServicioBean2ServicioTicketDto(peticionPagoLocal.getArrayServicios()));
            ticket.setTransaccion(String.valueOf(String.valueOf(respuestaPagoLocal.getConciliacionId())).concat("O"));
            ticket.setTiendaId(String.valueOf(VistaBarraUsuario.getSesion().getTiendaId()));
            ticket.setTotalComision(String.valueOf(calcularComisionPagos(true)));
            ticket.setTiposPago(impresionVenta.tipoPagoDtoaTicket(arrayTiposPagoServicio2Venta(listaTiposPago), peticionPagoLocal.getTotalVenta(), _montoRecibido, _montoCambio));
            ticket.setTotalServicios();
            ticket.setTotalLetras(String.valueOf(impresionVenta.getTotalLetras()));
        } else {
            ticket.setCaja(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));
            ticket.setCajero(String.valueOf(VistaBarraUsuario.getSesion().getUsuarioId()));
            ticket.setCalleNumeroTienda(cadenaDireccion);
            ticket.setColoniaTienda(VistaBarraUsuario.getSesion().getDatosTienda().getColonia());
            ticket.setDelegacionCiudadEstadoCP(cadenaDelegacion);
            if (esSuperPrecio) {
                ticket.setEncabezado(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ENCABEZADO.TICKET.SUPR"));
            } else {
                ticket.setEncabezado(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ENCABEZADO.TICKET"));
            }
            ticket.setFecha(obtenerFecha().substring(0, obtenerFecha().indexOf(" ")));
            ticket.setHora(obtenerFecha().substring(10));
            ticket.setNombreTienda(VistaBarraUsuario.getSesion().getNombreTienda());
            ticket.setFolio(String.valueOf(respuestaVentaServiciosDto.getPaTransaccionId()).concat("I"));
            ticket.setServicios(arrayPagoServicioVenta2ServicioTicketDto(respuestaVentaServiciosDto.getPaArrayServicios()));
            ticket.setTransaccion(String.valueOf(respuestaVentaServiciosDto.getPaTransaccionId()));
            ticket.setTiendaId(String.valueOf(VistaBarraUsuario.getSesion().getTiendaId()));
            ticket.setTotalComision(String.valueOf(calcularComisionPagos(false)));
            ticket.setTiposPago(impresionVenta.tipoPagoDtoaTicket(arrayTiposPagoServicio2Venta(listaTiposPago), peticionVentaServiciosDto.getPaMontoTotalVta(), _montoRecibido, _montoCambio));
            ticket.setTotalServicios();
            ticket.setTotalLetras(String.valueOf(impresionVenta.getTotalLetras()));

        }

        SION.log(Modulo.PAGO_SERVICIOS, "Ticket a imprimir: " + ticket.toString(), Level.INFO);

        //se valida que solo se imprima ticket si hubo algun pago exitoso
        if (ticket.getServicios().length > 0) {

            try {
                ticket.setIvaTotal();
                impresionVenta.imprimirTicketServicios(ticket);
                seImprimioTicketExitoso = true;
            } catch (Exception ex) {
                SION.logearExcepcion(Modulo.PAGO_SERVICIOS, ex);
                SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió una excepcion al imprimir ticket de pago", Level.SEVERE);

            } catch (Error err) {
                SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un error al imprimir el ticket de pago", Level.SEVERE);

            }
        } else {
            SION.log(Modulo.PAGO_SERVICIOS, "No se encontraron servicios exitosos, no se imprime ticket", Level.WARNING);
            seImprimioTicketExitoso = true;
        }
    }

    public boolean seImprimioTicket() {
        return seImprimioTicketExitoso;
    }

    private ArrayList<TipoPagoDto> arrayTiposPagoServicio2Venta(ArrayList<TipoPagoServicioDto> _pagos) {
        ArrayList<TipoPagoDto> pagos = new ArrayList<TipoPagoDto>();

        for (TipoPagoServicioDto dto : _pagos) {
            if (dto != null) {
                TipoPagoDto pago = new TipoPagoDto();
                pago.setFiMontoRecibido(dto.getFiMontoRecibido());
                pago.setFiTipoPagoId(dto.getFiTipoPagoId());
                pago.setFnMontoPago(dto.getFnMontoPago());
                pago.setFnNumeroVales(dto.getFnNumeroVales());
                pago.setImporteAdicional(dto.getImporteAdicional());
                pago.setPaPagoTarjetaIdBus(dto.getPaPagoTarjetaIdBus());
                pagos.add(pago);
            }
        }

        return pagos;
    }

    private ServicioTicketDto[] arrayPagoServicioVenta2ServicioTicketDto(PagoServicioVentaDto[] _pagos) {
        ArrayList<ServicioTicketDto> listaServicios = new ArrayList<ServicioTicketDto>();
        ServicioTicketDto[] serviciosTicket = null;

        for (PagoServicioVentaDto pago : _pagos) {
            if (pago != null) {
                //valida que solo se impriman pagos exitosos

                ServicioTicketDto dto = new ServicioTicketDto();
                dto.setComision(utilerias.getNumeroFormateado(getPeticionPagoFromPeticionVenta(pago).getComision() + getPeticionPagoFromPeticionVenta(pago).getImpuesto()));
                dto.setEmisora(buscaNombreEmisorXid(pago.getFiArticuloId()));
                dto.setImporte(String.valueOf(getPeticionPagoFromPeticionVenta(pago).getMonto()));
                /*if (pago.getFiArticuloId() == Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.GOBIERNODF").trim())
                        || pago.getFiArticuloId() == Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.EDOMEX").trim())) {
                    dto.setConceptoImpuesto(getImpuestoFromReferencia(getPeticionPagoFromPeticionVenta(pago), false, null));
                    dto.setEsConceptoImpuesto(true);
                    if (pago.getFiArticuloId() == Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.GOBIERNODF").trim())) {
                        String leyenda = SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.tesoreria.df").concat(": ").concat(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.default.mensaje"));
                        dto.setLeyenda(leyenda);
                    } else if (pago.getFiArticuloId() == EMISOR_SKY) {
                        dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.sky.mensaje"));
                    } else if (pago.getFiArticuloId() == EMISOR_MAZ) {
                        dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.maz.mensaje"));
                    } else {
                        dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.default.mensaje"));
                    }
                } else {*/
                    dto.setEsConceptoImpuesto(false);
                    //if (pago.getMovimientoId() > 0) {
                    //dto.setLeyenda(pago.getFcMensaje());
                    if (pago.getFiArticuloId() == Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.IUSACELL"))) {
                        String leyenda = " Datos del Cliente: ";
                        if (!getUsuarioIusacellReferencia().isEmpty()) {
                            leyenda += getUsuarioIusacellReferencia().concat(". ");
                        }
                        if (!getContratoIusacellReferencia().isEmpty()) {
                            leyenda += getContratoIusacellReferencia().concat(" ");
                        }
                        if (!getContratoIusacellReferencia().isEmpty()) {
                            leyenda += "Contrato: ".concat(getContratoIusacellReferencia());
                        }

                        dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.default.mensaje").concat(leyenda));
                    } /*else if (pago.getFiArticuloId() == EMISOR_SKY) {
                        dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.sky.mensaje"));
                    }*/ else if (pago.getFiArticuloId() == EMISOR_MAZ) {
                        dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.maz.mensaje"));
                    } else {
                        dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.default.mensaje"));
                    }
                    //}else{
                    //    dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.estatus4.mensaje"));
                    //}
                //}
                dto.setIva(0);
                if (pago.getMovimientoId() > 0) {
                    dto.setNoAutorizacion(String.valueOf(pago.getMovimientoId()));
                } else {
                    dto.setNoAutorizacion("0");
                }

                dto.setReferencia(pago.getFcreferencia());
                listaServicios.add(dto);


            }
        }
        serviciosTicket = new ServicioTicketDto[listaServicios.size()];
        return listaServicios.toArray(serviciosTicket);
    }

    /*
     * private String formatearComision(double monto) { DecimalFormat df = new
     * DecimalFormat("#.##"); return String.valueOf(df.format(monto)); }
     */
    private ServicioTicketDto[] arrayPagoServicioBean2ServicioTicketDto(PagoServicioBean[] _pagos) {
        ArrayList<ServicioTicketDto> listaServicios = new ArrayList<ServicioTicketDto>();
        ServicioTicketDto[] serviciosTicket = null;

        for (PagoServicioBean pago : _pagos) {
            if (pago != null) {

                ServicioTicketDto dto = new ServicioTicketDto();
                dto.setComision(utilerias.getNumeroFormateado(pago.getComision() + pago.getImpuesto()));
                dto.setEmisora(buscaNombreEmisorXid(pago.getArticuloId()));
                dto.setImporte(String.valueOf(pago.getMonto()));
                if (pago.getArticuloId() == Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.GOBIERNODF").trim())
                        || pago.getArticuloId() == Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.EDOMEX").trim())) {
                    dto.setConceptoImpuesto(getImpuestoFromReferencia(null, true, pago));
                    dto.setEsConceptoImpuesto(true);
                    if (pago.getArticuloId() == Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.GOBIERNODF").trim())) {
                        String leyenda = SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.tesoreria.df").concat(". ").concat(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.default.mensaje"));
                        dto.setLeyenda(leyenda);
                    } else {
                        dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.default.mensaje"));
                    }
                } else {
                    dto.setEsConceptoImpuesto(false);
                    if (pago.getArticuloId() == EMISOR_SKY) {
                        /*dto.setLeyenda("TRANSACCION APROBADA | Transaccion Aprobada."
                        +"Servicio operado por Plataforma Electronica XCD. "
                        +"El pago quedara aplicado en 24 hrs. Si se supende su servicio, envie desde su celular reactiva espacio y su numero de cliente al 30200."
                        +"Para aclaraciones marque 01(81)-8988-0200. Favor de guardar este comprobante de pago para posibles aclaraciones.");
                        */dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.sky.mensaje"));
                    } else if (pago.getArticuloId() == EMISOR_MAZ) {
                        /*dto.setLeyenda("TRANSACCION APROBADA | Transaccion Aprobada."
                        +"Servicio operado por Plataforma Electronica XCD. "
                        +"El pago quedara aplicado en 24 hrs. Si se supende su servicio, envie desde su celular reactiva espacio y su numero de cliente al 30200."
                        +"Para aclaraciones marque 01(81)-8988-0200. Favor de guardar este comprobante de pago para posibles aclaraciones.");
                        */dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.maz.mensaje"));
                    } else if (pago.getArticuloId() == EMISOR_IUSACELL) {
                        String leyenda = " Datos del Cliente: ";
                        if (!getUsuarioIusacellReferencia().isEmpty()) {
                            leyenda += getUsuarioIusacellReferencia().concat(". ");
                        }
                        if (!getNumeroIusacellReferencia().isEmpty()) {
                            leyenda += getNumeroIusacellReferencia().concat(" ");
                        }
                        if (!getContratoIusacellReferencia().isEmpty()) {
                            leyenda += "Contrato: ".concat(getContratoIusacellReferencia());
                        }

                        /*dto.setLeyenda("TRANSACCION APROBADA | Transaccion Aprobada."
                        +"Servicio operado por Plataforma Electronica XCD. "
                        +"El pago quedara aplicado en 24 hrs. Si se supende su servicio, envie desde su celular reactiva espacio y su numero de cliente al 30200."
                        +"Para aclaraciones marque 01(81)-8988-0200. Favor de guardar este comprobante de pago para posibles aclaraciones.");
                        */dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.default.mensaje").concat(leyenda));
                    } else {
                        /*dto.setLeyenda("TRANSACCION APROBADA | Transaccion Aprobada."
                        +"Servicio operado por Plataforma Electronica XCD. "
                        +"El pago quedara aplicado en 24 hrs. Si se supende su servicio, envie desde su celular reactiva espacio y su numero de cliente al 30200."
                        +"Para aclaraciones marque 01(81)-8988-0200. Favor de guardar este comprobante de pago para posibles aclaraciones.");
                        */dto.setLeyenda(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.default.mensaje"));
                    }

                }
                dto.setIva(0);

                dto.setNoAutorizacion("0");
                dto.setReferencia(pago.getReferencia());
                listaServicios.add(dto);


            }
        }
        serviciosTicket = new ServicioTicketDto[listaServicios.size()];
        return listaServicios.toArray(serviciosTicket);
    }

    private String getUsuarioIusacellReferencia() {
        String cadena = "";

        if (respuestaReferenciaDto != null) {
            if (respuestaReferenciaDto.getAdicionales().contains("<Cliente>")) {
                cadena = respuestaReferenciaDto.getAdicionales().substring(respuestaReferenciaDto.getAdicionales().indexOf("<Cliente>") + 9, respuestaReferenciaDto.getAdicionales().indexOf("</Cliente>"));
            }
        }

        return cadena;
    }

    private String getNumeroIusacellReferencia() {
        String cadena = "";

        if (respuestaReferenciaDto != null) {
            if (respuestaReferenciaDto.getAdicionales().contains("<Telefono>")) {
                cadena = respuestaReferenciaDto.getAdicionales().substring(respuestaReferenciaDto.getAdicionales().indexOf("<Telefono>") + 10, respuestaReferenciaDto.getAdicionales().indexOf("</Telefono>"));
            }
        }

        return cadena;
    }

    private String getContratoIusacellReferencia() {
        String cadena = "";

        if (respuestaReferenciaDto != null) {
            if (respuestaReferenciaDto.getAdicionales().contains("<Contrato>")) {
                cadena = respuestaReferenciaDto.getAdicionales().substring(respuestaReferenciaDto.getAdicionales().indexOf("<Contrato>") + 10, respuestaReferenciaDto.getAdicionales().indexOf("</Contrato>"));
            }
        }

        return cadena;
    }

    private PeticionPagoServicioDto getPeticionPagoFromPeticionVenta(PagoServicioVentaDto _pago) {
        PeticionPagoServicioDto peticion = null;
        for (PeticionPagoServicioDto dto : peticionVentaServiciosDto.getArrayPagos()) {
            if (_pago.getFiArticuloId() == dto.getModuloId() && _pago.getFcreferencia().equals(dto.getReferencia())) {
                peticion = dto;
                break;
            }
        }
        return peticion;
    }

    private String getImpuestoFromReferencia(PeticionPagoServicioDto _peticion, boolean _esFueraLinea,
            PagoServicioBean _pagoFueraLinea) {
        String concepto = "";

        if (_esFueraLinea) {

            for (ReferenciaValidaBean bean : listaReferenciasValidadas) {
                if (bean != null) {
                    if (bean.getEmisorId() == _pagoFueraLinea.getArticuloId() && bean.getReferencia().equals(_pagoFueraLinea.getReferencia())) {
                        if (bean.getRespuestaReferencia().getAdicionales().contains("<Impuesto>") && bean.getRespuestaReferencia().getAdicionales().contains("</Impuesto>")) {
                            concepto = bean.getRespuestaReferencia().getAdicionales().substring(bean.getRespuestaReferencia().getAdicionales().indexOf("<Impuesto>") + 10, bean.getRespuestaReferencia().getAdicionales().indexOf("</Impuesto>"));
                        }
                    }
                }
            }

        } else {

            for (ReferenciaValidaBean bean : listaReferenciasValidadas) {
                if (bean != null) {
                    if (bean.getEmisorId() == _peticion.getModuloId() && bean.getReferencia().equals(_peticion.getReferencia())) {
                        if (bean.getRespuestaReferencia().getAdicionales().contains("<Impuesto>") && bean.getRespuestaReferencia().getAdicionales().contains("</Impuesto>")) {
                            concepto = bean.getRespuestaReferencia().getAdicionales().substring(bean.getRespuestaReferencia().getAdicionales().indexOf("<Impuesto>") + 10, bean.getRespuestaReferencia().getAdicionales().indexOf("</Impuesto>"));
                        }
                    }
                }
            }
        }

        return concepto;
    }

    public double getImporteCobrado() {
        double importe = 0;

        for (PagoServicioVentaDto respuesta : respuestaVentaServiciosDto.getPaArrayServicios()) {
            for (PeticionPagoServicioDto peticion : peticionVentaServiciosDto.getArrayPagos()) {
                if (peticion.getModuloId() == respuesta.getFiArticuloId()
                        && peticion.getReferencia().equals(respuesta.getFcreferencia())) {
                    if (respuesta.getMovimientoId() > 0) {
                        importe += peticion.getMonto();
                    }
                }
            }
        }

        return importe;
    }
    //termina ticket

    public String buscaNombreEmisorXid(long _id) {
        String nombre = "";
        for (EmpresaBean empresa : listaEmpresas) {
            if (empresa.getEmpresaId() == _id) {
                nombre = empresa.getDescrpcion();
                break;
            }
        }
        return nombre;
    }

    private double calcularComisionPagos(boolean _esFueraLinea) {
        double comision = 0;

        if (_esFueraLinea) {

            for (PagoServicioBean bean : peticionPagoLocal.getArrayServicios()) {
                comision += bean.getComision() + bean.getImpuesto();
            }

        } else {

            for (PagoServicioVentaDto respuesta : respuestaVentaServiciosDto.getPaArrayServicios()) {
                if (respuesta != null) {
                    for (PeticionPagoServicioDto peticion : peticionVentaServiciosDto.getArrayPagos()) {
                        if (respuesta.getFiArticuloId() == peticion.getModuloId() && respuesta.getFcreferencia().equals(peticion.getReferencia())) {
                            comision += peticion.getComision() + peticion.getImpuesto();
                        }
                    }
                }
            }

        }

        return comision;
    }

    public String obtenerFecha() {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar Cal = Calendar.getInstance();
        return dateFormat.format(new Date());
    }

    public boolean existenPagosRechazados() {
        boolean existenCancelados = false;
        for (PagoServicioVentaDto pago : respuestaVentaServiciosDto.getPaArrayServicios()) {
            if (pago != null) {
                if (pago.getMovimientoId() == 0) {
                    existenCancelados = true;
                }
            } else {
                existenCancelados = true;
            }
        }
        return existenCancelados;
    }

    public boolean existenPagosTimedOut() {
        boolean existenTimedOut = false;
        for (PagoServicioVentaDto pago : respuestaVentaServiciosDto.getPaArrayServicios()) {
        }
        return existenTimedOut;
    }

    /////conciliacion
    public void generarConciliacion(int sistema, int modulo, int submodulo) {

        peticionConciliacion = null;
        respuestaConciliacionLocalBean = null;

        peticionConciliacion = new PeticionConciliacionLocalBean();
        respuestaConciliacionLocalBean = new RespuestaConciliacionLocalBean();

        peticionConciliacion.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
        peticionConciliacion.setPaTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
        peticionConciliacion.setPaUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());
        peticionConciliacion.setPaSistemaId(sistema);
        peticionConciliacion.setPaModulo(modulo);
        peticionConciliacion.setPaSubmoduloId(submodulo);

        SION.log(Modulo.PAGO_SERVICIOS, "Petición de conciliación: " + peticionConciliacion.toString(), Level.INFO);

        respuestaConciliacionLocalBean = conciliacionLocalControlador.conciliacionVentaLocal(peticionConciliacion);

        SION.log(Modulo.PAGO_SERVICIOS, "Respuesta obtenida: " + respuestaConciliacionLocalBean.toString(), Level.INFO);

    }

    public void actualizarTransacciónLocal(int sistema, int modulo, int submodulo) {

        SION.log(Modulo.PAGO_SERVICIOS, "Actualizando conciliacion en BD local", Level.INFO);

        peticionActTransaccionLocalBean = null;
        peticionActTransaccionLocalBean = new PeticionActTransaccionLocalBean();
        respuestaActualizacionLocalBean = null;
        respuestaActualizacionLocalBean = new RespuestaActualizacionLocalBean();
        
       
        peticionActTransaccionLocalBean.setPaConciliacionId(peticionVentaServiciosDto.getPaConciliacionId());
        peticionActTransaccionLocalBean.setPaTransaccionVenta(respuestaVentaServiciosDto.getPaTransaccionId());
        
        SION.log(Modulo.PAGO_SERVICIOS, "Petición de actualización de transacción local : " + peticionActTransaccionLocalBean.toString(), Level.INFO);

        respuestaActualizacionLocalBean = conciliacionLocalControlador.actualizarTransaccionLocal(peticionActTransaccionLocalBean);

        int contador = 0;

        while (true && contador < 3) {
            if (respuestaActualizacionLocalBean.getPaCdgError() == 0) {
                break;
            } else {
                respuestaActualizacionLocalBean = conciliacionLocalControlador.actualizarTransaccionLocal(peticionActTransaccionLocalBean);
            }

            contador++;
        }

        SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de la petición de actualización de la transacción local : " + respuestaActualizacionLocalBean.toString(), Level.INFO);

    }

    public void cancelarConciliacionVenta(boolean _esFueraLinea) {

        SION.log(Modulo.PAGO_SERVICIOS, "Cancelando conciliacion de venta en BD local", Level.INFO);

        peticionActTransaccionLocalBean = null;
        peticionActTransaccionLocalBean = new PeticionActTransaccionLocalBean();

        if (_esFueraLinea) {

            peticionActTransaccionLocalBean.setPaConciliacionId(peticionPagoLocal.getConciliacionId());
            peticionActTransaccionLocalBean.setPaTransaccionVenta(peticionPagoLocal.getConciliacionId());

            conciliacionLocalControlador.updCancelacionConciliacion(peticionActTransaccionLocalBean);

        } else {
            peticionActTransaccionLocalBean.setPaConciliacionId(peticionVentaServiciosDto.getPaConciliacionId());
            peticionActTransaccionLocalBean.setPaTransaccionVenta(respuestaVentaServiciosDto.getPaTransaccionId());

            conciliacionLocalControlador.updCancelacionConciliacion(peticionActTransaccionLocalBean);
        }

    }

    ////fin conciliacion
    ///fuera de linea
    public void llenarPeticionPagoFueraLinea(boolean existeParametroFL, double _montoVenta) {
        peticionPagoLocal = null;
        peticionPagoLocal = new PeticionPagoServicioLocalBean();

        SION.log(Modulo.PAGO_SERVICIOS, "Llenando peticion de pago local", Level.INFO);

        if (existeParametroFL) {

            //limpiar arreglos
            arregloPeticionesPagoDto = null;
            arregloTiposPago = null;

            //inicializar
            arregloPeticionesPagoDto = new PeticionPagoServicioDto[listaPeticionesPago.size()];
            arregloTiposPago = new TipoPagoServicioDto[listaTiposPago.size()];

            String t = VistaBarraUsuario.getSesion().getIpEstacion();
            t = t.concat("|CAJA");
            t = t.concat(String.valueOf(VistaBarraUsuario.getSesion().getNumeroEstacion()));

            //se llena arreglo de peticiones de servicios
            llenarArregloServicios();
            //llenar arreglo de tipos de pago
            llenarArregloFormasPago();

            peticionPagoLocal.setArrayPagos(tipoPagoDto2TipoPagoBean(arregloTiposPago));
            peticionPagoLocal.setArrayServicios(peticionPagoServicioDto2pagoServicioBean(arregloPeticionesPagoDto));
            peticionPagoLocal.setConciliacionId(0);
            peticionPagoLocal.setIndicador(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "pago.local.indicador")));//7
            peticionPagoLocal.setModuloId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACIONFL.MODULO")));//2
            peticionPagoLocal.setPais(VistaBarraUsuario.getSesion().getPaisId());
            peticionPagoLocal.setSistemaId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACIONFL.SISTEMA")));//1
            peticionPagoLocal.setSubModuloId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACIONFL.SUBMODULO")));//10
            peticionPagoLocal.setTerminal(t);
            peticionPagoLocal.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());
            peticionPagoLocal.setTotalVenta(_montoVenta);
            peticionPagoLocal.setUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());

        } else {
            peticionPagoLocal.setArrayPagos(tipoPagoDto2TipoPagoBean(peticionVentaServiciosDto.getArrayTiposPago()));
            peticionPagoLocal.setArrayServicios(peticionPagoServicioDto2pagoServicioBean(peticionVentaServiciosDto.getArrayPagos()));
            peticionPagoLocal.setConciliacionId(peticionVentaServiciosDto.getPaConciliacionId());
            peticionPagoLocal.setIndicador(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "pago.local.indicador")));//7
            peticionPagoLocal.setModuloId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.MODULO")));//2
            peticionPagoLocal.setPais(peticionVentaServiciosDto.getPaPaisId());
            peticionPagoLocal.setSistemaId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SISTEMA")));//1
            peticionPagoLocal.setSubModuloId(Integer.parseInt(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "CONCILIACION.SUBMODULO")));//9
            peticionPagoLocal.setTerminal(peticionVentaServiciosDto.getPaTerminal());
            peticionPagoLocal.setTiendaId(peticionVentaServiciosDto.getTiendaId());
            peticionPagoLocal.setTotalVenta(peticionVentaServiciosDto.getPaMontoTotalVta());
            peticionPagoLocal.setUsuarioId(peticionVentaServiciosDto.getPaUsuarioId());
        }
    }

    private TipoPagoBean[] tipoPagoDto2TipoPagoBean(TipoPagoServicioDto[] _pagos) {
        TipoPagoBean[] pagos = new TipoPagoBean[_pagos.length];

        int contador = 0;

        for (TipoPagoServicioDto dto : _pagos) {
            TipoPagoBean bean = new TipoPagoBean();

            bean.setFcReferenciaPagoTarjetas(String.valueOf(dto.getPaPagoTarjetaIdBus()));
            bean.setFiTipoPagoId(dto.getFiTipoPagoId());
            bean.setFnImporteAdicional(dto.getImporteAdicional());
            bean.setFnMontoPago(dto.getFnMontoPago());
            bean.setFnNumeroVales(dto.getFnNumeroVales());

            pagos[contador] = bean;
            contador++;
        }

        return pagos;
    }

    private TipoPagoServicioDto[] tipoPagoBean2TipoPagoDto(TipoPagoBean[] _pagos) {
        TipoPagoServicioDto[] pagos = new TipoPagoServicioDto[_pagos.length];

        int contador = 0;

        for (TipoPagoBean bean : _pagos) {
            TipoPagoServicioDto dto = new TipoPagoServicioDto();

            dto.setPaPagoTarjetaIdBus(Long.parseLong(bean.getFcReferenciaPagoTarjetas()));
            dto.setFiTipoPagoId(bean.getFiTipoPagoId());
            dto.setImporteAdicional(bean.getFnImporteAdicional());
            dto.setFnMontoPago(bean.getFnMontoPago());
            dto.setFnNumeroVales(bean.getFnNumeroVales());

            pagos[contador] = dto;
            contador++;
        }

        return pagos;
    }

    private PagoServicioBean[] peticionPagoServicioDto2pagoServicioBean(PeticionPagoServicioDto[] _pagos) {
        PagoServicioBean[] pagos = new PagoServicioBean[_pagos.length];

        int contador = 0;

        for (PeticionPagoServicioDto dto : _pagos) {
            PagoServicioBean bean = new PagoServicioBean();
            bean.setAdicionales(dto.getAdicionales());
            bean.setArticuloId(dto.getModuloId());
            bean.setCodigoBarras(dto.getCodigoBarras());
            bean.setComision(dto.getComision());
            bean.setImpuesto(dto.getImpuesto());
            bean.setMonto(dto.getMonto());
            bean.setParametros(dto.getParametros());
            bean.setReferencia(dto.getReferencia());
            bean.setUid(dto.getUid());
            pagos[contador] = bean;
            contador++;
        }

        return pagos;
    }
    
    public RespuestaConsultaReferenciaBean consultaReferencia(PeticionConsultaReferenciaBean _peticionConsulta){
        
        respuestaConsultaReferencia = null;
        respuestaConsultaReferencia = new RespuestaConsultaReferenciaBean();
        
        respuestaConsultaReferencia = dao.consultaReferencia(_peticionConsulta);
                
        return respuestaConsultaReferencia;
    }
    
    public RespuestaConsultaOriflameBean consultaPedidoOriflame(PeticionConsultaOriflameBean _peticionConsulta){
        respuestaConsultaPedidoOriflame = null;
        respuestaConsultaPedidoOriflame = new RespuestaConsultaOriflameBean();
        
        respuestaConsultaPedidoOriflame = dao.consultarPedidosOriflame(_peticionConsulta);
        
        return respuestaConsultaPedidoOriflame;
    }

    public void pagarFueraDeLinea() {

        respuestaPagoLocal = null;
        respuestaPagoLocal = new RespuestaPagoServicioLocalBean();
        respuestaConsultaReferencia = new RespuestaConsultaReferenciaBean();
        peticionConsultaReferencia = new PeticionConsultaReferenciaBean();

        int cantidadIntentos = 5;
        int intento = 1;
        esPagoLocalExitoso = false;
        boolean existeReferenciaDuplicada = false;

        if (peticionPagoLocal != null) {
            for (PagoServicioBean pago : peticionPagoLocal.getArrayServicios()) {
                SION.log(Modulo.PAGO_SERVICIOS, "Consultando en busca de referencias duplicadas en BD local", Level.INFO);

                peticionConsultaReferencia.setPaEmisorId(pago.getArticuloId());
                peticionConsultaReferencia.setPaMontoPago(pago.getMonto());
                peticionConsultaReferencia.setPaReferencia(pago.getReferencia());
                peticionConsultaReferencia.setPaFechaConc(obtenerFecha());
                peticionConsultaReferencia.setPaPaisId(peticionPagoLocal.getPais());
                peticionConsultaReferencia.setPaTiendaId((int) peticionPagoLocal.getTiendaId());
                respuestaConsultaReferencia = dao.consultaReferencia(peticionConsultaReferencia);

                if (respuestaConsultaReferencia != null) {
                    SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de consulta de referencia: " + respuestaConsultaReferencia.toString(), Level.INFO);
                    if (respuestaConsultaReferencia.getPaCdgError() != 0) {
                        existeReferenciaDuplicada = true;
                        break;
                    }
                }
            }
            if (existeReferenciaDuplicada) {
                respuestaPagoLocal.setCodigoError(respuestaConsultaReferencia.getPaCdgError());
                respuestaPagoLocal.setDescError(respuestaConsultaReferencia.getPaDescError());
                SION.log(Modulo.PAGO_SERVICIOS, "Se detecta que ya existe el pago", Level.INFO);
                //AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(
                //Modulo.PAGO_SERVICIOS, "servicio.error.existencia"), TipoMensaje.ERROR, vista.getTablaServicios());
            } else {
                do {
                    SION.log(Modulo.PAGO_SERVICIOS, "intento de pago en bd local #" + intento, Level.INFO);
                    //SION.log(Modulo.PAGO_SERVICIOS, "Peticion de pago local: " + peticionPagoLocal.toString(), Level.INFO);
                    respuestaPagoLocal = pagoFueraLinea.spPagoServicioLocal(peticionPagoLocal);
                    //SION.log(Modulo.PAGO_SERVICIOS, "Respuesta recibida: "+respuestaPagoLocal.toString(), Level.INFO);

                    if (respuestaPagoLocal.getCodigoError() == 0 && respuestaPagoLocal.getConciliacionId() != 0) {
                        esPagoLocalExitoso = true;
                    }

                    intento++;

                } while (!esPagoLocalExitoso && intento <= cantidadIntentos);
            }
        }
    }

    public boolean esPagoLocalExitoso() {
        return esPagoLocalExitoso;
    }

    public RespuestaPagoServicioLocalBean getRespuestaPagoLocal() {
        return respuestaPagoLocal;
    }

    public PeticionPagoServicioLocalBean getPeticionpagoLocal() {
        return peticionPagoLocal;
    }

    ///bloqueos fuera de linea
    public List<ObjetoBloqueo> getArregloBloqueosLocal() {
        return arregloBloqueosLocal;
    }

    public List<ObjetoBloqueo> consultaBloqueosLocal(long _conciliacion) {
        ///verificar bloqueos fuera de linea
        SION.log(Modulo.PAGO_SERVICIOS, "Consultando bloqueos bd local", Level.INFO);

        existeBloqueoLocal = false;
        existeAvisoLocal = false;
        tipoBloqueo = 0;

        arregloBloqueosLocal = bloqueoLocal.consultaStatusBloqueo(VistaBarraUsuario.getSesion().getUsuarioId());

        SION.log(Modulo.PAGO_SERVICIOS, "Arreglo de bloqueos recibido: " + arregloBloqueosLocal.toString(), Level.INFO);

        SION.log(Modulo.PAGO_SERVICIOS, "Estableciendo datos de bloqueo en sesion", Level.INFO);

        BloqueoBean[] arregloBloqueosSesion = new BloqueoBean[arregloBloqueosLocal.size()];
        int contadorBloqueo = 0;

        for (ObjetoBloqueo bloqueo : arregloBloqueosLocal) {
            BloqueoBean bean = new BloqueoBean();
            bean.setAvisosRestantes(bloqueo.getNumAvisosRestantes());
            bean.setFiEstatusBloqueoId(bloqueo.getStatusBloqueo());
            bean.setFiNumAvisos(bloqueo.getNumAvisos());
            bean.setFiTipoPagoId(bloqueo.getTipoPagoId());
            arregloBloqueosSesion[contadorBloqueo] = bean;
        }

        PeticionBloqueoDto peticionBloqueoLocal = new PeticionBloqueoDto();
        peticionBloqueoLocal.setPaConciliacionId(_conciliacion);
        peticionBloqueoLocal.setPaPaisId(VistaBarraUsuario.getSesion().getPaisId());
        peticionBloqueoLocal.setPaUsuarioId(VistaBarraUsuario.getSesion().getUsuarioId());
        peticionBloqueoLocal.setTiendaId(VistaBarraUsuario.getSesion().getTiendaId());

        try {
            SION.log(Modulo.PAGO_SERVICIOS, "Cargando datos de bloqueos de venta", Level.INFO);
            VistaBarraUsuario.getSesion().setBloqueosVenta(bloqueoVenta.existeComunicacionCentral(peticionBloqueoLocal));
            SION.log(Modulo.PAGO_SERVICIOS, "Datos de sesion cargados", Level.INFO);
        } catch (Exception e) {
            SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió un error al establecer datos de sesion " + e.toString(), Level.SEVERE);
        }

        for (ObjetoBloqueo bloqueo : arregloBloqueosLocal) {
            if (bloqueo.getStatusBloqueo() == 1) {
                tipoBloqueo = bloqueo.getTipoPagoId();
                existeBloqueoLocal = true;
                break;
            } else if (bloqueo.getNumAvisosRestantes() > 0 && bloqueo.getNumAvisosRestantes() <= 3) {
                tipoBloqueo = bloqueo.getTipoPagoId();
                existeAvisoLocal = true;
                avisosRestantesBloqueoLocal = bloqueo.getNumAvisosRestantes();
                break;
            }
        }
        return arregloBloqueosLocal;
    }

    public boolean existeBloqueoLocal() {
        return existeBloqueoLocal;
    }

    public boolean existeAvisoBloqueoLocal() {
        return existeAvisoLocal;
    }

    public int getTipoBloqueolocal() {
        return tipoBloqueo;
    }

    public int getAvisosRestantesLocal() {
        return avisosRestantesBloqueoLocal;
    }
    ///fin bloqueos fuera de linea
    
    
    ///termina fuera de linea
    public void limpiar() {
        SION.log(Modulo.PAGO_SERVICIOS, "Limpiando variables de pago de servicios", Level.INFO);
        listaTiposPago.clear();
        listaPeticionesPago.clear();
        listaReferenciasValidadas.clear();
        listaServiciosAcumulados.clear();
        listapagosSeleccionados.clear();
        listaPagosParciales.clear();
        arregloPeticionesPagoDto = null;
        arregloRespuestasPagoDto = null;
        arregloTiposPago = null;
        peticionVentaServiciosDto = null;
        respuestaVentaServiciosDto = null;
        peticionActTransaccionLocalBean = null;
        respuestaActualizacionLocalBean = null;

        oriflame = null;
    }

    public void limpiaParcial() {
        SION.log(Modulo.PAGO_SERVICIOS, "Limpiando variables de pago de servicios", Level.INFO);
        listaTiposPago.clear();
        listaPeticionesPago.clear();
        listaPagosParciales.clear();
        arregloPeticionesPagoDto = null;
        arregloRespuestasPagoDto = null;
        arregloTiposPago = null;
        peticionVentaServiciosDto = null;
        respuestaVentaServiciosDto = null;
        peticionActTransaccionLocalBean = null;
        respuestaActualizacionLocalBean = null;

        oriflame = null;
    }
    
    
    
    
    
    
    
    
}