/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

import java.util.Arrays;

/**
 *
 * @author fvega
 */
public class RespuestaEmpresasBean {

    private EmpresaBean[] empresaBeans;
    private EmpresaBean[] empresaPines;
    private int codigoError;
    private String descError;

    public int getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    public EmpresaBean[] getEmpresaBeans() {
        return empresaBeans;
    }

    public void setEmpresaBeans(EmpresaBean[] empresaBeans) {
        this.empresaBeans = empresaBeans;
    }

    @Override
    public String toString() {
        return "RespuestaEmpresasBean{" + "empresaBeans=" + Arrays.toString(empresaBeans) + 
                                        ", empresaPines=" + Arrays.toString(empresaPines) + 
                                        ", codigoError=" + codigoError + 
                                        ", descError=" + descError + '}';
    }

    /**
     * @return the empresaPines
     */
    public EmpresaBean[] getEmpresaPines() {
        return empresaPines;
    }

    /**
     * @param empresaPines the empresaPines to set
     */
    public void setEmpresaPines(EmpresaBean[] empresaPines) {
        this.empresaPines = empresaPines;
    }
}
