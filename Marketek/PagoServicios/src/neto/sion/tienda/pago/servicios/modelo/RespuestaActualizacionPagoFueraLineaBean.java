/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author fvegap
 */
public class RespuestaActualizacionPagoFueraLineaBean {

    private int codigoError;
    private String descError;

    public int getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    @Override
    public String toString() {
        return "RespuestaActualizacionPagoFueraLineaBean{" + "codigoError=" + codigoError + ", descError=" + descError + '}';
    }
}
