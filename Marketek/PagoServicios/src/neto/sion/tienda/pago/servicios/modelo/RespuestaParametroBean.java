/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author esantiago
 */
public class RespuestaParametroBean {
    
    private int codigoError;
  private String descripcionError;
  private double valorConfiguracion;
  
  public int getCodigoError()
  {
    return codigoError;
  }
  
  public void setCodigoError(int codigoError)
  {
    this.codigoError = codigoError;
  }
  
  public String getDescripcionError()
  {
    return descripcionError;
  }
  
  public void setDescripcionError(String descripcionError)
  {
    this.descripcionError = descripcionError;
  }
  
  public double getValorConfiguracion()
  {
    return valorConfiguracion;
  }
  
  public void setValorConfiguracion(double valorConfiguracion)
  {
    this.valorConfiguracion = valorConfiguracion;
  }
  
  @Override
  public String toString()
  {
    return "RespuestaParametroBean{codigoError=" + codigoError + ", descripcionError=" + descripcionError + ", valorConfiguracion=" + valorConfiguracion + '}';
  }
    
}
