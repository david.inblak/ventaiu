/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vista;

import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.modelo.RespuestaPagoBean;
import neto.sion.tienda.pago.servicios.vistamodelo.PagoServiciosVistaModelo;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;


/**
 *
 * @author fvega
 */
/*public class PagosParcialesVista {

    private UtileriasVenta utilerias;
    private PagoServiciosVista pagoServiciosVista;
    private PagoServiciosVistaModelo vistaModelo;
    //
    private Stage stage;
    private Group grupo;
    private Scene escena;
    private BorderPane panel;
    //panel
    private VBox cajaCabecera;
    private VBox cajaPagos;
    ///////cajaCabecera
    private Group grupoCabecera;
    //grupoCabecera
    private VBox cajaRelleno;
    private Label etiquetaResumenPagos;
    private Image imagenCerrar;
    private Image imagenCerrarHover;
    private ImageView vistaImagenCerrar;
    ///////cajaPagos
    private HBox cajaMontoPagos;
    private VBox cajaTablaPagos;
    private VBox cajaCambioPagos;
    //cajaMontoPagos
    private Label etiquetaMontoPago;
    private TextField campoMontoPago;
    //cajaTablaPagos
    private TableView tablaPagos;
    private TableColumn columnaEmisor;
    private TableColumn columnaImagen;
    private TableColumn columnaMensaje;
    private TableColumn columnaImporte;
    //cajaCambioPagos
    private HBox cajaMontoRecibido;
    private HBox cajaCambio;
    //cajaMontoRecibido
    private Label etiquetaMontoRecibido;
    private TextField campoMontoRecibido;
    //cajaCambio
    private Label etiquetaMontoCambio;
    private TextField campoMontoCambio;
    
    private final KeyCombination escape = new KeyCodeCombination(KeyCode.ESCAPE);

    public PagosParcialesVista(final PagoServiciosVista _pagoServiciosVista, final UtileriasVenta _utilerias,
            final PagoServiciosVistaModelo _vistaModelo) {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {

                //
                pagoServiciosVista = _pagoServiciosVista;
                utilerias = _utilerias;
                vistaModelo = _vistaModelo;
                //
                panel = new BorderPane();
                grupo = new Group();
                escena = new Scene(grupo, 900, 520, Color.WHITE);
                stage = new Stage(StageStyle.UNDECORATED);
                stage.initModality(Modality.APPLICATION_MODAL);

                //panel
                cajaCabecera = utilerias.getVBox(0, 900, 50);
                cajaPagos = utilerias.getVBox(0, 900, 460);

                ///////cajaCabecera
                grupoCabecera = new Group();
                //grupoCabecera
                cajaRelleno = utilerias.getVBox(0, 900, 50);
                etiquetaResumenPagos = utilerias.getLabel("Resumen de pagos", 300, 40);
                imagenCerrar = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/btnCerrar.png");
                imagenCerrarHover = utilerias.getImagen("/neto/sion/tienda/venta/imagenes/inicio/btn_Cerrar_2.png");
                vistaImagenCerrar = new ImageView(imagenCerrar);

                ///////cajaPagos
                cajaMontoPagos = utilerias.getHBox(10, 870, 50);
                cajaTablaPagos = utilerias.getVBox(0, 900, 300);
                cajaCambioPagos = utilerias.getVBox(5, 870, 90);
                //cajaMontoPagos
                etiquetaMontoPago = utilerias.getLabel("Monto cobrado:", 250, 40);
                campoMontoPago = utilerias.getTextField("", 150, 40);
                //cajaTablaPagos
                tablaPagos = new TableView();
                columnaEmisor = utilerias.getColumna("Emisor", 260);
                columnaImagen = utilerias.getColumna("", 50);
                columnaMensaje = utilerias.getColumna("Mensaje", 420);
                columnaImporte = utilerias.getColumna("Importe", 100);
                //cajaCambioPagos
                cajaMontoRecibido = utilerias.getHBox(15, 870, 45);
                cajaCambio = utilerias.getHBox(15, 870, 45);
                //cajaMontoRecibido
                etiquetaMontoRecibido = utilerias.getLabel("Importe Recibido", 200, 40);
                campoMontoRecibido = utilerias.getTextField("", 100, 40);
                //cajaCambio
                etiquetaMontoCambio = utilerias.getLabel("Cambio Final", 200, 40);
                campoMontoCambio = utilerias.getTextField("", 100, 40);


                aplicarEstilos();
                ajustarTamanios();
                creaListeners();
                setCursor(vistaImagenCerrar);
            }
        });

    }

    private void aplicarEstilos() {
        cajaRelleno.getStyleClass().add("CajaCabeceraParciales");
        cajaPagos.setStyle("-fx-alignment: center;");
        cajaTablaPagos.setStyle("-fx-alignment: center; -fx-border-color: orange; -fx-border-width: 0 2 0 2;");
        cajaMontoPagos.setStyle("-fx-alignment: center-right bottom-left; "
                + "");
        cajaMontoRecibido.setStyle("-fx-alignment: center-right bottom-left; "
                + "");
        cajaCambio.setStyle("-fx-alignment: center-right bottom-left;"
                + "");


        etiquetaResumenPagos.setStyle("-fx-alignment: center; -fx-font: 22pt Verdana; -fx-text-fill: white");
        etiquetaMontoPago.setStyle("-fx-alignment: center; -fx-font: 22pt Verdana; -fx-text-fill: #084B8A");
        etiquetaMontoRecibido.setStyle("-fx-alignment: center; -fx-font: 20pt Verdana; -fx-text-fill: #084B8A");
        etiquetaMontoCambio.setStyle("-fx-alignment: center; -fx-font: 20pt Verdana; -fx-text-fill: #084B8A");

        panel.setStyle("-fx-border-color: orange; -fx-border-width: 2;");

        campoMontoCambio.getStyleClass().add("TextField");
        campoMontoPago.getStyleClass().add("TextField");
        campoMontoRecibido.getStyleClass().add("TextField");
    }

    private void ajustarTamanios() {

        //cajaCabecera
        cajaCabecera.getChildren().addAll(grupoCabecera);
        grupoCabecera.setBlendMode(BlendMode.MULTIPLY);
        grupoCabecera.getChildren().add(cajaRelleno);
        grupoCabecera.getChildren().add(etiquetaResumenPagos);
        grupoCabecera.getChildren().add(vistaImagenCerrar);
        etiquetaResumenPagos.setLayoutX(20);
        etiquetaResumenPagos.setLayoutY(10);
        vistaImagenCerrar.setFitHeight(25);
        vistaImagenCerrar.setFitWidth(25);
        vistaImagenCerrar.setX(850);
        vistaImagenCerrar.setY(10);

        //cajapagos
        cajaPagos.getChildren().addAll(cajaMontoPagos, cajaTablaPagos, cajaCambioPagos);
        cajaMontoPagos.getChildren().addAll(etiquetaMontoPago, campoMontoPago);
        cajaTablaPagos.getChildren().addAll(tablaPagos);
        tablaPagos.getColumns().addAll(columnaEmisor, columnaImagen, columnaMensaje, columnaImporte);
        tablaPagos.setMaxSize(870, 290);
        tablaPagos.setMinSize(870, 290);
        tablaPagos.setPrefSize(870, 290);
        asignaPropiedadBean(columnaEmisor, String.class, "nombreEmisor");
        asignaPropiedadBean(columnaMensaje, String.class, "mensajeSwitch");
        asignaPropiedadBean(columnaImporte, Double.class, "importe");
        setCellFactory(tablaPagos, columnaEmisor, String.class, Pos.TOP_RIGHT, false);
        //setCellFactory(tablaPagos, columnaMensaje, String.class, Pos.TOP_RIGHT, false);
        setCallbackLabel(columnaMensaje);
        setCellFactory(tablaPagos, columnaImporte, Double.class, Pos.TOP_LEFT, false);
        setCallbackImagen(columnaImagen);
        tablaPagos.setItems(vistaModelo.getListaPagosParciales());
        tablaPagos.setEditable(false);

        cajaCambioPagos.getChildren().addAll(cajaMontoRecibido, cajaCambio);
        cajaMontoRecibido.getChildren().addAll(etiquetaMontoRecibido, campoMontoRecibido);
        cajaCambio.getChildren().addAll(etiquetaMontoCambio, campoMontoCambio);

        campoMontoPago.setEditable(false);
        campoMontoRecibido.setEditable(false);
        campoMontoCambio.setEditable(false);

        panel.setTop(cajaCabecera);
        panel.setCenter(cajaPagos);
        panel.setMaxSize(900, 520);
        panel.setMinSize(900, 520);
        panel.setPrefSize(900, 520);
        grupo.getChildren().addAll(panel);

    }

    private <E> void asignaPropiedadBean(TableColumn columna, E e, final String id) {
        columna.setCellValueFactory(new PropertyValueFactory<RespuestaPagoBean, E>(id));
    }

    public <E> void setCellFactory(final TableView tabla, final TableColumn columna, final E e, final Pos posicion,
            final boolean seValidaFormato) {

        columna.setCellFactory(new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn param) {
                return new TableCell<RespuestaPagoBean, E>() {

                    @Override
                    public void updateItem(E item, boolean empty) {
                        super.updateItem(item, empty);

                        if (!this.isEmpty()) {
                            ////reavisa si E es Intger
                            if (e.toString().contains(Integer.class.getCanonicalName())) {
                            } else if (e.toString().contains(Double.class.getCanonicalName())) {
                                if (seValidaFormato) {

                                    String cadenaDouble = "";
                                    int indicePunto = 0;
                                    String cadenaPuntoAlaDerecha = "";

                                    cadenaDouble = String.valueOf(item);
                                    indicePunto = cadenaDouble.indexOf(".");
                                    cadenaPuntoAlaDerecha = cadenaDouble.substring(indicePunto + 1);

                                    if (Double.parseDouble(cadenaPuntoAlaDerecha) > 0) {
                                        this.setText(cadenaDouble);
                                    } else {
                                        this.setText(cadenaDouble.substring(0, indicePunto));
                                    }

                                } else {

                                    this.setText(utilerias.getNumeroFormateado(Double.parseDouble(item.toString())));
                                }
                            } else {///es String
                                this.setText(String.valueOf(item));

                            }

                            this.setWrapText(true);
                            this.alignmentProperty().set(posicion);
                            this.setStyle("-fx-font: 20pt Verdana;");
                        }
                    }
                };
            }
        });
    }

    ;
    
    public void setCallbackImagen(final TableColumn _columna) {
        _columna.setCellValueFactory(new Callback<CellDataFeatures<RespuestaPagoBean, ImageView>, ObservableValue<ImageView>>() {

            @Override
            public ObservableValue<ImageView> call(CellDataFeatures<RespuestaPagoBean, ImageView> arg0) {
                final RespuestaPagoBean usuario = arg0.getValue();
                Image imagen = null;
                ImageView vistaImagen = new ImageView();
                boolean seCargoImagen = false;

                if (usuario.getFueAutorizado()) {
                    try {
                        imagen = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/autorizado.png");
                        seCargoImagen = true;
                    } catch (Exception e) {
                        SION.log(Modulo.PAGO_SERVICIOS, "ocurrio un error al cargar imagen", Level.SEVERE);
                        SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
                    }
                } else {
                    try {
                        imagen = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/rechazado.png");
                        seCargoImagen = true;
                    } catch (Exception e) {
                        SION.log(Modulo.PAGO_SERVICIOS, "ocurrio un error al cargar imagen", Level.SEVERE);
                        SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
                    }
                }

                if (seCargoImagen) {
                    vistaImagen.setImage(imagen);
                    vistaImagen.setFitWidth(50);
                    vistaImagen.setFitHeight(50);
                }



                return new SimpleObjectProperty<ImageView>(vistaImagen);
            }
        });
    }

    public void setCallbackLabel(final TableColumn _columna) {
        _columna.setCellValueFactory(new Callback<CellDataFeatures<RespuestaPagoBean, BorderPane>, ObservableValue<BorderPane>>() {

            @Override
            public ObservableValue<BorderPane> call(CellDataFeatures<RespuestaPagoBean, BorderPane> arg0) {
                final BorderPane panel = new BorderPane();
                final RespuestaPagoBean usuario = arg0.getValue();
                final Label etiqueta = new Label(usuario.getMensajeSwitch());

                if (usuario.getFueAutorizado()) {

                    etiqueta.setText("Autorizado");

                } else {

                    etiqueta.setMaxWidth(370);
                    etiqueta.setMinWidth(370);
                    etiqueta.setPrefWidth(370);

                    etiqueta.setMaxHeight(120);
                    etiqueta.setMinHeight(120);
                    etiqueta.setPrefHeight(120);

                    panel.setMaxSize(370, 120);
                    panel.setMinSize(370, 120);
                    panel.setPrefSize(370, 120);
                }

                etiqueta.setStyle("-fx-font: 20pt Verdana;");

                etiqueta.setWrapText(true);
                panel.setTop(etiqueta);
                return new SimpleObjectProperty<BorderPane>(panel);
            };
        });
    }

    private void creaListeners() {
        
        panel.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if(escape.match(arg0)){
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            cerrarStage();
                        }
                    });
                }
            }
        });

        vistaImagenCerrar.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent arg0) {
                cerrarStage();
            }
        });

        vistaImagenCerrar.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    vistaImagenCerrar.setImage(imagenCerrarHover);
                } else if (arg1 && !arg2) {
                    vistaImagenCerrar.setImage(imagenCerrar);
                }
            }
        });

        tablaPagos.focusedProperty().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable arg0) {
                if (tablaPagos.isFocused()) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            panel.requestFocus();
                        }
                    });
                }
            }
        });
    }

    private void setCursor(final Node _nodo) {

        _nodo.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    _nodo.setCursor(Cursor.HAND);
                } else if (arg1 && !arg2) {
                    _nodo.setCursor(Cursor.NONE);
                }
            }
        });

    }

    public void setMontoaCobrar(String _monto) {
        campoMontoPago.setText(_monto);
    }

    public void setMontoRecibido(String _monto) {
        campoMontoRecibido.setText(_monto);
    }

    public void setMontoCambio(String _monto) {
        campoMontoCambio.setText(_monto);
    }

    public void mostrarStage() {
        SION.log(Modulo.PAGO_SERVICIOS, "Mostrando stage pagos parciales", Level.INFO);
        escena.getStylesheets().addAll(PagoServiciosVista.class.getResource("/neto/sion/tienda/pago/servicios/vista/EstilosPagoServicios.css").toExternalForm());
        stage.setScene(escena);
        stage.show();
        stage.centerOnScreen();
    }

    public void cerrarStage() {
        SION.log(Modulo.PAGO_SERVICIOS, "Cerrando stage de pagos parciales", Level.INFO);
        vistaModelo.limpiar();
        pagoServiciosVista.limpiar();
        stage.close();
    }
}*/
