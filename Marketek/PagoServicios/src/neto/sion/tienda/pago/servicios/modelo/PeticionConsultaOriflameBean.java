/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author fvegap
 */
public class PeticionConsultaOriflameBean {

    private String referencia;
    private int paisId;
    private long tiendaId;

    public int getPaisId() {
        return paisId;
    }

    public void setPaisId(int paisId) {
        this.paisId = paisId;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public long getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(long tiendaId) {
        this.tiendaId = tiendaId;
    }

    @Override
    public String toString() {
        return "PeticionConsultaOriflameBean{" + "referencia=" + referencia + ", paisId=" + paisId + ", tiendaId=" + tiendaId + '}';
    }
}
