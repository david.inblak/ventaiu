/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vista;

import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import neto.sion.pago.servicios.cliente.dto.RespuestaReferenciaDto;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.vistamodelo.PagoServiciosVistaModelo;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;

/**
 *
 * @author fvega
 */
public class MAZVista {
    
    private UtileriasVenta utilerias;
    private PagoServiciosVistaModelo pagosVistaModelo;

    //contenedores
    public Stage stage;
    private Group grupo;
    private Scene escena;
    private BorderPane panel;
    private VBox cajaPrincipal;
    
    //cajaPrincipal
    private VBox cajaBarraSuperior;
    private VBox cajaLogo;
    private VBox cajaDatos;
    
    //cajaBarraSuperior
    private Image imagenCerrar;
    private ImageView vistaImagenCerrar;
    
    //cajaLogo
    private Image imagenLogo;
    private ImageView vistaImagenLogo;
    
    //cajaDatos
    private VBox cajaEtiquetas;
    private HBox cajaBotones;
    
    //cajaEtiquetas
    private VBox cajaTexto;
    private HBox cajaNombreGrupo;
    
    //cajaTexto
    private Label etiqueta1;
    
    //cajaNombreGrupo
    private Label etiqueta2;
    private Label etiquetaNombre;
    
    //cajaBotones
    private Image imagenBotonCorrecto;
    private Image imagenBotonCorrectoHover;
    private ImageView vistaImagenCorrecto;
    private Button botonCorrecto;
    private Image imagenBotonIncorrecto;
    private Image imagenBotonIncorrectoHover;
    private ImageView vistaImagenIncorrecto;
    private Button botonIncorrecto;
    
    //
    private int tipoPago;
    private double comision;
    
    //
    private SimpleBooleanProperty seAcepto;

    public MAZVista(UtileriasVenta _utilerias, int _tipoPago, PagoServiciosVistaModelo _pagosVistaModelo) {
        
        this.utilerias = _utilerias;
        this.pagosVistaModelo = _pagosVistaModelo;
        
        this.tipoPago = _tipoPago;
        this.comision = 0;
        
        //contenedores
        this.grupo = new Group();
        this.panel = new BorderPane();
        this.cajaPrincipal = utilerias.getVBox(0, 700, 415);
        
        //cajaPrincipal
        this.cajaBarraSuperior = utilerias.getVBox(0, 680, 30);
        this.cajaLogo = utilerias.getVBox(0, 680, 120);
        this.cajaDatos = utilerias.getVBox(5, 680, 250);
        
        //cajaBarraSuperior
        this.imagenCerrar = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/cerrar.png");
        this.vistaImagenCerrar = new ImageView(imagenCerrar);
        
        //cajaLogo
        this.imagenLogo = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/MAZlogo.jpg");
        this.vistaImagenLogo = new ImageView(imagenLogo);
        
        //cajaDatos
        this.cajaEtiquetas = utilerias.getVBox(0, 650, 180);
        this.cajaBotones = utilerias.getHBox(15, 680, 60);
        
        //cajaEtiquetas
        this.cajaTexto = utilerias.getVBox(5, 530, 100);
        this.cajaNombreGrupo = utilerias.getHBox(20, 650, 80);
        
        //cajaTexto
        this.etiqueta1 = new Label("Favor de validar el nombre del grupo al que desea realizar el pago");
    
        //cajaNombreGrupo
        this.etiqueta2 = new Label("Nombre del grupo");
        this.etiquetaNombre = utilerias.getLabel("", 420, 50);
    
        //cajaBotones
        this.imagenBotonCorrecto = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/MAZcorrecto.png");
        this.imagenBotonCorrectoHover = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/MAZcorrecto.png");
        this.vistaImagenCorrecto = new ImageView(imagenBotonCorrecto);
        this.botonCorrecto = new Button("", vistaImagenCorrecto);
        this.imagenBotonIncorrecto = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/MAZincorrecto.png");
        this.imagenBotonIncorrectoHover = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/MAZincorrecto.png");
        this.vistaImagenIncorrecto = new ImageView(imagenBotonIncorrecto);
        this.botonIncorrecto = new Button("", vistaImagenIncorrecto);
        
        //
        this.seAcepto = new SimpleBooleanProperty(false);
        
    }

    private void agregarEstilos(){
        
        cajaBarraSuperior.getStyleClass().add("CajaBarraSuperior");
        cajaLogo.getStyleClass().add("CajaLogoMAZ");
        
        etiqueta1.setStyle("-fx-alignment: center-right bottom-left; -fx-font: 23pt Verdana; -fx-text-fill: #2F4F4F;");
        etiqueta2.setStyle("-fx-alignment: center-right bottom-left; -fx-font: 23pt Verdana; -fx-text-fill: #2F4F4F;");
     
        etiquetaNombre.getStyleClass().add("TextFieldMAZ");
        
        cajaTexto.setStyle("-fx-alignment: center;");
        cajaNombreGrupo.setStyle("-fx-alignment: center;");
        cajaEtiquetas.setStyle("-fx-alignment: center;");
        cajaDatos.setStyle("-fx-alignment: center;");
        
        cajaBotones.setStyle("-fx-alignment: center;");
        
        cajaPrincipal.getStyleClass().add("CajaPrincipalMAZ");
        panel.setStyle("-fx-background-color: transparent;");
    }
    
    private void ajustaComponentes(){
        
        //cajaBarraSuperior
        vistaImagenCerrar.setFitWidth(20);
        vistaImagenCerrar.setFitHeight(20);

        //cajaLogo
        vistaImagenLogo.setFitHeight(110);
        vistaImagenLogo.setFitWidth(340);
        
        //cajaDatos
        etiqueta1.setMaxSize(500, 120);
        etiqueta1.setMinSize(500, 120);
        etiqueta1.setPrefSize(500, 120);
        etiqueta2.setMaxSize(230, 120);
        etiqueta2.setMinSize(230, 120);
        etiqueta2.setPrefSize(230, 120);
        etiqueta1.setWrapText(true);
        etiqueta1.setTextAlignment(TextAlignment.CENTER);
        cajaTexto.getChildren().addAll(etiqueta1);
        cajaEtiquetas.getChildren().addAll(cajaTexto, cajaNombreGrupo);
        
        //etiquetaNombre.setText("LOS SUPERSONICOS");
        etiquetaNombre.setAlignment(Pos.CENTER);
        cajaNombreGrupo.getChildren().addAll(etiqueta2, etiquetaNombre);
        
        vistaImagenCorrecto.setFitWidth(200);
        vistaImagenCorrecto.setFitHeight(40);
        vistaImagenIncorrecto.setFitWidth(200);
        vistaImagenIncorrecto.setFitHeight(40);
        botonCorrecto.setMaxSize(200, 40);
        botonCorrecto.setMinSize(200, 40);
        botonCorrecto.setPrefSize(200, 40);
        botonIncorrecto.setMaxSize(200, 40);
        botonIncorrecto.setMinSize(200, 40);
        botonIncorrecto.setPrefSize(200, 40);
        cajaBotones.getChildren().addAll(botonCorrecto,botonIncorrecto);
        
        cajaBarraSuperior.getChildren().addAll(vistaImagenCerrar);

        cajaLogo.getChildren().addAll(vistaImagenLogo);

        cajaDatos.getChildren().addAll(cajaEtiquetas, cajaBotones);

        cajaPrincipal.getChildren().addAll(cajaBarraSuperior, cajaLogo, cajaDatos);
        
        panel.setTop(cajaPrincipal);
        panel.setMinHeight(415);
        panel.setPrefHeight(415);
        panel.setMaxHeight(415);
        panel.setMinWidth(700);
        panel.setPrefWidth(700);
        panel.setMaxWidth(700);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                grupo.getChildren().addAll(panel);
            }
        });
    }
    
    private void setHover(final ImageView _vistaImagen, final Image _imagen, final Image _imagenHover) {

        _vistaImagen.hoverProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    _vistaImagen.setImage(_imagenHover);
                } else if (arg1 && !arg2) {
                    _vistaImagen.setImage(_imagen);
                }
            }
        });
    }
    
    private void crearHandlers(){
        
        botonCorrecto.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                SION.log(Modulo.PAGO_SERVICIOS, "Boton correcto precionado", Level.INFO);
                pagosVistaModelo.agregaReferenciaMAZ(tipoPago, comision);
                stage.close();
            }
        });
        
        botonIncorrecto.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                SION.log(Modulo.PAGO_SERVICIOS, "Boton incorrecto presionado", Level.INFO);
                pagosVistaModelo.limpiaParcial();
                stage.close();
            }
        });
        
        vistaImagenCerrar.setOnMouseClicked(new EventHandler<MouseEvent>(){

            @Override
            public void handle(MouseEvent arg0) {
                SION.log(Modulo.PAGO_SERVICIOS, "Boton cerrar presionado", Level.INFO);
                pagosVistaModelo.limpiaParcial();
                stage.close();
            }
        });

    }
    
    private void setCursor(final Node _nodo) {
        _nodo.hoverProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                if (arg2 && !arg1) {
                    _nodo.setCursor(Cursor.HAND);
                } else if (arg1 && !arg2) {
                    _nodo.setCursor(Cursor.NONE);
                }
            }
        });
    }

    public boolean getSeAcepto() {
        return seAcepto.get();
    }
    
    public Stage getStageMAZ() {
        return stage;
    }
    
    public void abreStageMAZ(RespuestaReferenciaDto _respuestaReferencia, double _comision) {

        SION.log(Modulo.PAGO_SERVICIOS, "Abriendo Popup de confirmacion de Micronegocios Azteca", Level.INFO);

        if (escena == null) {

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    escena = new Scene(grupo, 700, 415, Color.TRANSPARENT);
                    stage = new Stage(StageStyle.UNDECORATED);
                    stage.initModality(Modality.APPLICATION_MODAL);

                    agregarEstilos();
                    ajustaComponentes();
                    crearHandlers();
                    
                    setCursor(vistaImagenCerrar);
                    setCursor(botonCorrecto);
                    setCursor(botonIncorrecto);

                    escena.getStylesheets().addAll(PagoServiciosVista.class.getResource("/neto/sion/tienda/pago/servicios/vista/EstilosPagoServicios.css").toExternalForm());

                    stage.setTitle("Pagos Oriflame");
                    stage.setScene(escena);
                    stage.centerOnScreen();
                    stage.show();
                }
            });

        } else {

            stage.setTitle("Pagos Oriflame");
            stage.setScene(escena);
            stage.centerOnScreen();
            stage.show();

        }
        
        comision = _comision;
        
        etiquetaNombre.setText(_respuestaReferencia.getNombreGrupo());
    }
}
