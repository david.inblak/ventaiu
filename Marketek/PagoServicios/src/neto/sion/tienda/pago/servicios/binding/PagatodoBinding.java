/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.binding;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import neto.sion.tienda.caja.principal.VerificaVentana;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.vista.PagoServiciosVista;
import neto.sion.tienda.pago.servicios.vistamodelo.PagatodoVistaModelo;
import neto.sion.tienda.pago.servicios.vistamodelo.PagoServiciosVistaModelo;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;
import neto.sion.venta.pagatodo.ps.dto.ConsultaComisionDtoResp;

/**
 *
 * @author esantiago
 */
public class PagatodoBinding {
    
    
     private PagoServiciosVista vista;
    private PagoServiciosBinding binding;
    private PagatodoVistaModelo pagatodoVistaModelo;
    private VerificaVentana verificaVentana;
    private Image aceptarBloqueos;
    private ImageView vistaImagenAceptarBloqueos;
    private Button botonBloqueos;



    /**
     * Clase constructor
     * @param _binding 
     */
    public PagatodoBinding(PagoServiciosVista _vista, PagoServiciosBinding _binding, PagoServiciosVistaModelo _vistModelo, 
            UtileriasVenta _utilerias, VerificaVentana _verificaVentana) {
        this.vista = _vista;
        this.binding = _binding;
        this.pagatodoVistaModelo = new PagatodoVistaModelo(_vistModelo, _utilerias,this);
        this.verificaVentana = _verificaVentana;
        
        this.aceptarBloqueos = new Image(getClass().getResourceAsStream("/neto/sion/tienda/pago/servicios/vista/btn_Aceptar.png"));
        this.vistaImagenAceptarBloqueos = new ImageView(aceptarBloqueos);
        this.botonBloqueos = new Button("", vistaImagenAceptarBloqueos);
        
        creaListenerBotonBloqueos();
    }
    
    //bloqueos
    
    public void creaListenerBotonBloqueos() {

        botonBloqueos.setMaxSize(vistaImagenAceptarBloqueos.getFitWidth(), vistaImagenAceptarBloqueos.getFitHeight());
        botonBloqueos.setMinSize(vistaImagenAceptarBloqueos.getFitWidth(), vistaImagenAceptarBloqueos.getFitHeight());
        botonBloqueos.setPrefSize(vistaImagenAceptarBloqueos.getFitWidth(), vistaImagenAceptarBloqueos.getFitHeight());

        botonBloqueos.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        evaluarBloqueosVenta();
                    }
                });
            }
        });

    }
    
    
    ///validaciones

    
    public boolean existePagoPagatodo(){
        return pagatodoVistaModelo.existePagoPagatodo();
    }
    

    public boolean esEmisoPagatodo (long _emisor){
        return pagatodoVistaModelo.esEmisorPagatodo(_emisor);
    }
    

    
    /**
     * 
     * @param _emisor
     * @param _importe
     * @param _referencia 
     */
    public void consultarReferencia(final long _emisor, final double _importe, final String _referencia, final double _comision, final int tipoServicio, final ConsultaComisionDtoResp detallePines){
                
        vista.cargarLoading();
        
        final Task tarea = new Task() {

            @Override
            protected Object call() throws Exception {
                
              
                SION.log(Modulo.PAGO_SERVICIOS, "Comienza tarea de envío de petición a método consultaPagoPagatodo", Level.INFO);
                pagatodoVistaModelo.consultaPagoPagatodo(false, _emisor, _referencia, _importe, _comision, tipoServicio, detallePines );
                
                return "ok";
            }
        };
        
        final Service servicio = new Service() {

            @Override
            protected Task createTask() {
                return tarea;
            }
        };
        
        servicio.stateProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                if (arg2.equals(Worker.State.SUCCEEDED)) {
                    boolean esConsultaValida = (pagatodoVistaModelo.getAutorizacionPagaTodoResp().getCodigoError()==0)?true:false;
                    
                    /**
                     * Errores por validar:
                     * -1   No se genero conciliacion
                     * -2   Excepcion en comunicacion - se muestra mensaje generico
                     */
                    
                    vista.removerImagenLoading();
                    SION.log(Modulo.PAGO_SERVICIOS, "Tarea de consulta de pago termina correctamente, la consulta ha sido validada: " + esConsultaValida, Level.INFO);
                    
                    //muestra popup de error en consulta
                    if(!esConsultaValida){
                        
                        
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                
                            AdministraVentanas.mostrarAlertaRetornaFoco(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getDescError(), TipoMensaje.ERROR, vista.getTablaPagosAcumulados());
                            limpiarCamposReferencia();
                            pagatodoVistaModelo.limpiar();
                            pagatodoVistaModelo.limpiarAntad();
                                
                            //limpia campos y forza a regresar el foco a la tabla
                            vista.getCampoEmisor().setText("");
                            vista.getCampoReferencia().setText("");
                            vista.getCampoPago().setText("0.00");
                            vista.getTablaPagosAcumulados().requestFocus();
                            }
                            
                        });
                        
                    }else{
                        vista.getCajaOverlay().setVisible(true);
                    }
                    
                    Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                            //forza a regresar el foco a la tabla
                            vista.getTablaPagosAcumulados().requestFocus();
                            }
                            
                     });
                    
                    
                    
                }else if (arg2.equals(Worker.State.FAILED)) {
                    vista.removerImagenLoading();
                    SION.log(Modulo.PAGO_SERVICIOS, "Tarea de consulta de pago termina con la siguiente excepción: "+servicio.getException().getLocalizedMessage(), Level.INFO);
                    
                    StringWriter sw = new StringWriter();
                    servicio.getException().printStackTrace(new PrintWriter(sw));
                    String exceptionAsString = sw.toString();
                    SION.log(Modulo.PAGO_SERVICIOS, "Exception - Cliente AntadTda Método Consulta : "+exceptionAsString, Level.SEVERE);
                    servicio.getException().printStackTrace();
                    
                     Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            vista.getTablaPagosAcumulados().requestFocus();
                        }
                    });
                }
            }
        });
        
        servicio.start();
        
    }
    
    /**
     * 
     * @param _montoRecibido 
     */
    public void aplicarAutorizacion(final double _montoRecibido,final double _comision ,final String _referencia){
        
        vista.cargarLoading();
        
        final Task tarea = new Task() {

            @Override
            protected Object call() throws Exception {
                
              
                SION.log(Modulo.PAGO_SERVICIOS, "Comienza tarea de envío de peticion a metodo Autorizacion Pagatodo", Level.INFO);
                
                pagatodoVistaModelo.autorizaPagoPagatodo(_montoRecibido, _comision,_referencia);
                vista.getCajaOverlay().setVisible(false);
                return "ok";
            }
        };
        
        final Service servicio = new Service() {

            @Override
            protected Task createTask() {
                return tarea;
            }
        };
        
        servicio.stateProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                if (arg2.equals(Worker.State.SUCCEEDED)) {
                    //try
                    boolean seRecibioRespuestaAutorizacionExitosa = false;
                    
                    //termina la tarea y se verifica si la autorizacion fue exitosa
                    
                    if(pagatodoVistaModelo.getAutorizacionPagaTodoResp()!=null && pagatodoVistaModelo.getAutorizacionPagaTodoResp().getCodigoError()==0){
                        
                        //caso exitoso
                        SION.log(Modulo.PAGO_SERVICIOS, "Tarea de autorización de pago termina correctamente, el pago ha sido aplicado en linea exitosamente", Level.INFO);
                        seRecibioRespuestaAutorizacionExitosa = true;
                    
                    }else if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getCodigoError()==-3){
                        
                         SION.log(Modulo.PAGO_SERVICIOS, "El pago no fue realizado, respuesta de Pagatodo erronea", Level.INFO);
                        seRecibioRespuestaAutorizacionExitosa = false;
                    }
                       
                    //quitar el loading
                    vista.removerImagenLoading();
                    
                    if(seRecibioRespuestaAutorizacionExitosa){
                        //si la respuesta es exitosa se verifica el estado de la impresion para mostrar popup de error si procede
                        validaEstadoImpresion();
                    }else{
                        SION.log(Modulo.PAGO_SERVICIOS, "Validando codigo de error recibido", Level.INFO);
                        
                        //si se obtuvo un codigo de error se muestra popup con error ya sea haya sido originado en central o bd local
                        if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getCodigoError() == -8){
                            
                            //rechazo encontrado en la consulta
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: Erro en base de datos central", Level.INFO);
                            

                            if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getDescError().contains("|")){   
                                final String[] msj = pagatodoVistaModelo.getAutorizacionPagaTodoResp().getDescError().substring(0).split("\\|");
                                
                                        AdministraVentanas.mostrarAlertaRetornaFoco(msj[0], TipoMensaje.ERROR, vista.getCampoReferencia());

                            }else{
                                
                                        AdministraVentanas.mostrarAlertaRetornaFoco("Ocurrió un error al realizar el pago, favor de intentarlo de nuevo o contactar a soporte tecnico", TipoMensaje.ERROR, vista.getCampoReferencia());
                          
                           
                                
                            }
                                    
                            
                        }else if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getCodigoError() == -7){
                            
                            //rechazo encontrado en la consulta
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: Time out al realizar el pago, se encuentra pago no exitoso", Level.INFO);
                            
                            
                                        AdministraVentanas.mostrarAlertaRetornaFoco(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getDescError(), 
                                        TipoMensaje.ERROR, vista.getCampoReferencia());
                                    
                            
                        }else if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getCodigoError() == -6){
                            
                            //rechazo encontrado en la consulta
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: Time out al realizar el pago, se encuentra pago no exitoso", Level.INFO);
                            
                            Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.error.todos"), 
                                        TipoMensaje.ERROR, vista.getCampoReferencia());
                                    }
                                });
                            
                        }else if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getCodigoError() == -5){
                            
                            //no se encontro pago en la consukta
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: Time out al realizar el pago, no se encontro en BD Central al realizar la consulta", Level.INFO);
                            
                            /*Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {*/
                                        
                                        String mensajeConcilia = "Su pago no pudo ser confirmado favor de llamar a soporte con el numero de conciliacion : " + pagatodoVistaModelo.getaAutorizacionPagaTodoReq().getPaConciliacionId();
                                        AdministraVentanas.mostrarAlertaRetornaFoco( mensajeConcilia, 
                                        TipoMensaje.ERROR, vista.getCampoReferencia());
                                    /*}
                                });*/
                            
                        }else if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getCodigoError() == -3){
                            
                            //rechazo encontrado en la consulta
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: Time out al realizar el pago,rechazo en la consulta de pago", Level.INFO);
                            
                           
                                        if (pagatodoVistaModelo.getRespuestaConsultaBDCentral()!=null && pagatodoVistaModelo.getRespuestaConsultaBDCentral().getMensajePagatodo()!=null){
                                            AdministraVentanas.mostrarAlertaRetornaFoco(pagatodoVistaModelo.getRespuestaConsultaBDCentral().getMensajePagatodo(), 
                                        TipoMensaje.ERROR, vista.getCampoReferencia());
                                        }else{
                                            AdministraVentanas.mostrarAlertaRetornaFoco("No se pudo obtener la confirmacion de pago, favor de comunicarse a soporte tectnico con el"
                                                    + " Numero de conciliacion : " + pagatodoVistaModelo.getaAutorizacionPagaTodoReq().getPaConciliacionId(), 
                                        TipoMensaje.ERROR, vista.getCampoReferencia());
                                        }
                                        
                            
                        }else if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getCodigoError() == -2){
                            
                            //error al marcar en bd local
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: BD Local", Level.INFO);
                            if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getDescError().contains("|")){   
                                //error originado en  base de datos local
                                final String[] msj = pagatodoVistaModelo.getRespuestaPagoLocal().getDescError().substring(1).split("\\|");
                                
                                        AdministraVentanas.mostrarAlertaRetornaFoco(msj[0].concat(" Favor de llamar a Soporte Técnico."), TipoMensaje.ERROR, vista.getCampoReferencia());
                          
                                

                            }else{
                                //excepcion al conectar con bd local
                               
                                       final String[] msj = pagatodoVistaModelo.getRespuestaPagoLocal().getDescError().substring(1).split("\\|");
                                
                                        AdministraVentanas.mostrarAlertaRetornaFoco(msj[0].concat(" Favor de llamar a Soporte Técnico."), TipoMensaje.ERROR, vista.getCampoReferencia());
                          
                           
                                
                            }
                        
                        }else if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getCodigoError() == -1){
                        
                            //la venta fl se realizo ok, error al conectar con plataforma antad
                            SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: WS NETO", Level.INFO);
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.error.todos"),
                                    TipoMensaje.ERROR, vista.getCampoReferencia());
                                }
                            });
                            
                            
                            
                        }else{
                            
                            //error de antad
                            if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getRespuestaPagatodoDto()!=null){
                                
                                //muestra mensaje de error obtenido en repuesta de autorizacion
                                SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: WS PAGATODO", Level.INFO);
                                String mensaje = "";
                                if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getDescError()!=null){
                                    
                                    if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getDescError().contains("|")){   
                                        String[] msj = pagatodoVistaModelo.getAutorizacionPagaTodoResp().getDescError().substring(0).split("\\|");
                                         mensaje = mensaje.concat(msj[0]).concat("\n ");
                                    }else{
                                         mensaje = mensaje.concat(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getDescError()).concat("\n ");
                                    }
                                        
                                    
                                    
                                    
                                   
                                }

                                if(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getRespuestaPagatodoDto().getrDescRespuesta()!=null){
                                    mensaje = mensaje.concat(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getRespuestaPagatodoDto().getrDescRespuesta());
                                }

                                ///valida que exista mensaje de antad, sino muestra msj especifico para tal caso
                                if(mensaje.isEmpty()){
                                    mensaje = "Ocurrió un error al recibir respuesta de pago. Por favor contacte a soporte técnico.";
                                }

                                final String msj = mensaje;
                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco(msj,TipoMensaje.ERROR, vista.getCampoReferencia());
                                    }
                                });
                                

                            }else{
                                
                                //la venta fl se realizo ok, error al conectar con plataforma antad
                                SION.log(Modulo.PAGO_SERVICIOS, "Tipo de error detectado: WS NETO", Level.INFO);
                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco("Ocurrió un error al registrar su pago. Por favor intente más tarde.",
                                        TipoMensaje.ERROR, vista.getCampoReferencia());
                                    }
                                });
                                

                            }
                        }

                    }
                    
                    //se regresa focus a tabla 
                    /*Platform.runLater(new Runnable() {

                        @Override
                        public void run() {*/
                            pagatodoVistaModelo.limpiarAntad();
                            pagatodoVistaModelo.limpiar();
                            binding.limpiar();
                            binding.limpiarVista();
                            binding.regresarFoco();
                        /*}
                    });*/
                    /*}catch(Exception e){
                        StringWriter sw = new StringWriter();
                        e.printStackTrace(new PrintWriter(sw));
                        String exceptionAsString = sw.toString();
                        SION.log(Modulo.PAGO_SERVICIOS, "Exception - Cliente AntadTda Método Autorización : "+exceptionAsString, Level.SEVERE);
                    }*/
                    
                }else if (arg2.equals(Worker.State.FAILED)) {
                    vista.removerImagenLoading();
                    SION.log(Modulo.PAGO_SERVICIOS, "Tarea de autorización de pago termina con la siguiente excepción: "+servicio.getException().getLocalizedMessage(), Level.INFO);
                    
                    StringWriter sw = new StringWriter();
                    servicio.getException().printStackTrace(new PrintWriter(sw));
                    String exceptionAsString = sw.toString();
                    SION.log(Modulo.PAGO_SERVICIOS, "Exception - Cliente Pagatodo Método Autorización : "+exceptionAsString, Level.SEVERE);
                    
                     Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            pagatodoVistaModelo.limpiarAntad();
                            pagatodoVistaModelo.limpiar();
                            binding.limpiar();
                            binding.limpiarVista();
                            binding.regresarFoco();
                            //muestra msj de error generico
                            AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "servicio.error.todos"),
                                    TipoMensaje.ERROR, vista.getCampoReferencia());
                        }
                    });
                }
            }
        });
        
        servicio.start();
        
    }
    
    /**
     * Se utiliza cuando se cancela el pago de servicio
     */
    public void cancelarConciliacionAntad(){
        
        pagatodoVistaModelo.cancelarConciliacionVenta(true);
        
    }
    
    public void validaEstadoImpresion(){
        
        if (!pagatodoVistaModelo.seImprimioTicket()) {

            int parametroVentana = 0;
            boolean estaVentanaAbierta = false;

            try {
                parametroVentana = Integer.parseInt(SION.obtenerParametro(Modulo.SION, "SEGURIDAD.VENTANADESBLOQUEO.EGRESOVALORES"));
            } catch (NumberFormatException e) {
                parametroVentana = 1;
            }

            if (verificaVentana.consultaEstatusVentana(parametroVentana)) {
                SION.log(Modulo.PAGO_SERVICIOS, "estatus de ventana de bloqueo = true, mostrando alerta de "
                        + "error de impresora y se continúa con la venta", Level.INFO);

                
                        AdministraVentanas.mostrarAlertaRetornaFoco(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ventas.noimpresora").replace(
                        "%1", String.valueOf(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getTransaccionId()).concat("I")), TipoMensaje.INFO, vista.getTablaServicios());
                   
                

            } else {
                SION.log(Modulo.PAGO_SERVICIOS, "estatus de ventana de bloquoe = false, mostrando alerta de "
                        + "error de impresora en impresión de ticket de venta", Level.INFO);

               
                        AdministraVentanas.mostrarAlerta(SION.obtenerMensaje(Modulo.PAGO_SERVICIOS, "ventas.noimpresora").replace(
                        "%1", String.valueOf(pagatodoVistaModelo.getAutorizacionPagaTodoResp().getTransaccionId()).concat("I")), TipoMensaje.INFO, botonBloqueos);
                    
                

            }

        }
    }
    
    public void limpiarCamposReferencia(){

        
                vista.getCampoReferencia().clear();
                vista.getCampoPago().clear();
                vista.getCampoReferencia().requestFocus();
            
        
    }
    
    public void evaluarBloqueosVenta(){
        binding.evaluarBloqueosVenta();
    }
    
    
    public ConsultaComisionDtoResp consultaComisionXidEmisor(long emisorId){
        
        ConsultaComisionDtoResp  comisionDto = pagatodoVistaModelo.consultaComisionXEmisorId(emisorId);
        
        return comisionDto;
    }
    
}
