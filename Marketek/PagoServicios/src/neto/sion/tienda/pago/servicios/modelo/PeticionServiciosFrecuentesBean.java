/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

/**
 *
 * @author fvega
 */
public class PeticionServiciosFrecuentesBean {

    private int paisId;
    private long tiendaId;

    public int getPaisId() {
        return paisId;
    }

    public void setPaisId(int paisId) {
        this.paisId = paisId;
    }

    public long getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(long tiendaId) {
        this.tiendaId = tiendaId;
    }

    @Override
    public String toString() {
        return "PeticionServiciosFrecuentesBean{" + "paisId=" + paisId + ", tiendaId=" + tiendaId + '}';
    }
}
