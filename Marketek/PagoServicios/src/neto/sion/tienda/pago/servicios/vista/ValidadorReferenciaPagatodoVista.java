/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.vista;

import com.sion.tienda.genericos.popup.TipoMensaje;
import com.sion.tienda.genericos.ventanas.AdministraVentanas;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.binding.PagatodoBinding;
import neto.sion.tienda.pago.servicios.binding.PagoServiciosBinding;
import neto.sion.tienda.pago.servicios.vistamodelo.PagatodoVistaModelo;
import neto.sion.tienda.venta.utilerias.util.UtileriasVenta;
import neto.sion.venta.pagatodo.ps.dto.ConsultaComisionDtoResp;
import neto.sion.venta.pagatodo.ps.dto.DetalleMontoArticuloDto;
import neto.sion.tienda.pago.servicios.modelo.RespuestaParametroBean;

/**
 *
 * @author esantiago
 */
public class ValidadorReferenciaPagatodoVista {
    
    private PagatodoBinding pagatodoBinding;
    
    private UtileriasVenta utilerias;
    private PagoServiciosBinding binding;
    
    //contenedores
    public Stage stage;
    private Group grupo;
    private Scene escena;
    private BorderPane panel;
    private VBox cajaPrincipal;
    
    //cajaPrincipal
    private VBox cajaLeyendaConfirmacion;
    private HBox cajaCampoConfirmacion;
    private HBox cajaReferencia;
    private HBox cajaImporte;
    private HBox cajaBotonValida;
    
    //cajaLeyendaConfirmacion
    private HBox cajaLeyendaConfirmacion1;
    private HBox cajaLeyendaConfirmacion2;
    
    //cajaLeyendaConfirmacion1
    private Label etiquetaConfirmacion1;
    
    //cajaLeyendaConfirmacion2
    private Label etiquetaConfirmacion2;
    
    //Etiquetas para validacion
    private Label etiquetaReferencia;
    private Label etiquetaImporte;
    
    //cajaCampoConfirmacion
    private TextField campoTextoConfirmacion;
    private TextField campoTextoImporte;
    private Image imagenValidarPago;
    private Label etiquetaConfirmaRefImporte;
    private ImageView vistaImagenValidarPago;
    private Button botonConfirmacion;
    
    //popuop rechazo
    private Image imagenCancelarPago;
    private ImageView vistaImagenCancelarPago;
    private Button botonCancelarValidacion;
    
    private final KeyCombination ENTER = new KeyCodeCombination(KeyCode.ENTER);
    
    //referencia ingresada en primera instancia
    private String referencia;
    private String segundaReferencia;
    private double montoPago;
    private String segundoMontoPago;
    private long emisorId;
    private double comision;
    private int tipoServicio;
    private String mensajeComision;
    
    ConsultaComisionDtoResp comisionDto;
    
    private double heightP;
    
    private double importeLimite;


        public ValidadorReferenciaPagatodoVista( PagatodoBinding _pagatodoBinding, UtileriasVenta _utilerias, PagoServiciosBinding _binding) {
       
        this.pagatodoBinding = _pagatodoBinding;
        this.utilerias = _utilerias;
        this.binding = _binding;
        
        //contenedores
        this.grupo = new Group();
        this.panel = new BorderPane();
        this.cajaPrincipal = utilerias.getVBox(0, 700, 220);
        
        //cajaPrincipal
        this.cajaLeyendaConfirmacion = utilerias.getVBox(0, 700, 120);
        this.cajaCampoConfirmacion = utilerias.getHBox(15, 700, 100);
        this.cajaReferencia = utilerias.getHBox(15, 700, 100);
        this.cajaImporte = utilerias.getHBox(15, 700, 100);
        this.cajaBotonValida = utilerias.getHBox(15, 700, 100);
        
        //cajaLeyendaConfirmacion
        this.cajaLeyendaConfirmacion1 = utilerias.getHBox(0, 650, 75);
        this.cajaLeyendaConfirmacion2 = utilerias.getHBox(0, 650, 45);
        
        //cajaLeyendaConfirmacion1
        this.etiquetaConfirmacion1 = utilerias.getLabel("Es necesario", 550, 30);
        
        //cajaLeyendaConfirmacion2
        this.etiquetaConfirmacion2 = utilerias.getLabel("que ingreses nuevamente la referencia.", 630, 30);
        
        //CajaLeyendaContirmacion 3
        this.etiquetaConfirmaRefImporte = utilerias.getLabel("que ingreses nuevamente la referencia e importe.", 650, 30);
        
        // etiquetas validacion referencia e importe
        this.etiquetaReferencia = utilerias.getLabel("Referencia: ", 150, 30);
        this.etiquetaImporte = utilerias.getLabel("Importe: ", 150, 30);
        
        //cajaCampoConfirmacion
        this.campoTextoConfirmacion = utilerias.getTextField("", 350, 50);
        this.campoTextoImporte = utilerias.getTextField("", 350, 50);
        //this.imagenValidarPago = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/btn_ValidarPago.png");
        this.imagenValidarPago = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/imgs/ico_valida_pago.png");
        this.vistaImagenValidarPago = new ImageView(imagenValidarPago);
        //this.botonConfirmacion = new Button("", vistaImagenValidarPago);
        
        this.botonConfirmacion = new Button();
        this.botonConfirmacion.getStyleClass().add("buttonValidarPagoPop");
        this.botonConfirmacion.setText("Validar Pago");
        this.botonConfirmacion.setGraphic(this.vistaImagenValidarPago);
        this.botonConfirmacion.setContentDisplay(ContentDisplay.LEFT);
        this.botonConfirmacion.setWrapText(true);
        
        //popup rechazo
        //this.imagenCancelarPago = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/btn_Cancelar.png");
        this.imagenCancelarPago = utilerias.getImagen("/neto/sion/tienda/pago/servicios/vista/imgs/ico_cancel.png");
        this.vistaImagenCancelarPago = new ImageView(imagenCancelarPago);
        //this.botonCancelarValidacion = new Button("", vistaImagenCancelarPago);
        this.botonCancelarValidacion = new Button();
        this.botonCancelarValidacion.getStyleClass().add("buttonCancelar");
        this.botonCancelarValidacion.setText("Cancelar");
        this.botonCancelarValidacion.setGraphic(this.vistaImagenCancelarPago);
        this.botonCancelarValidacion.setContentDisplay(ContentDisplay.RIGHT);
        this.botonCancelarValidacion.setWrapText(true);
        
        
    }
    
    private void agregarEstilos (){
        
        cajaPrincipal.getStyleClass().add("CajaPrincipalOriflame");
        
        cajaLeyendaConfirmacion.setStyle("-fx-alignment: center;");
        
        etiquetaConfirmacion1.setWrapText(false);
        etiquetaConfirmacion1.setStyle("-fx-alignment: center; -fx-font: 22pt Verdana; -fx-text-fill: #084B8A;");
        cajaLeyendaConfirmacion1.setStyle("-fx-alignment: bottom-center;");
        
        etiquetaConfirmacion2.setWrapText(false);
        etiquetaConfirmacion2.setStyle("-fx-alignment: center; -fx-font: 22pt Verdana; -fx-text-fill: #084B8A;");
        cajaLeyendaConfirmacion2.setStyle("-fx-alignment: center;");
        
        etiquetaConfirmaRefImporte.setStyle("-fx-alignment: center; -fx-font: 22pt Verdana; -fx-text-fill: #084B8A;");
        
        etiquetaReferencia.setWrapText(false);
        etiquetaReferencia.setStyle("-fx-alignment: center; -fx-font: 22pt Verdana; -fx-text-fill: #084B8A;");
        etiquetaImporte.setWrapText(false);
        etiquetaImporte.setStyle("-fx-alignment: center; -fx-font: 22pt Verdana; -fx-text-fill: #084B8A;");
        
        campoTextoConfirmacion.getStyleClass().add("TextField");
        campoTextoImporte.getStyleClass().add("TextFieldImporte");
        
        cajaCampoConfirmacion.setStyle("-fx-alignment: center;");
        
        cajaReferencia.setStyle("-fx-alignment: center;");
        cajaReferencia = utilerias.getHBox(15, 700, 100);
        
        cajaImporte.setStyle("-fx-alignment: center;");
        cajaImporte = utilerias.getHBox(15, 700, 100);
        
        cajaBotonValida.setStyle("-fx-alignment: center;");
        cajaBotonValida = utilerias.getHBox(15, 700, 100);
        
      
        
    }
    
     private void resetStyles(){
        
        cajaPrincipal.getStyleClass().clear();
        cajaLeyendaConfirmacion.setStyle(null);

        etiquetaConfirmacion1.setStyle(null);
        cajaLeyendaConfirmacion1.setStyle(null);
        
        etiquetaConfirmacion2.setStyle(null);
        cajaLeyendaConfirmacion2.setStyle(null);
        
        etiquetaConfirmaRefImporte.setStyle(null);
        
        
        etiquetaReferencia.setStyle(null);
        etiquetaImporte.setStyle(null);
        
        campoTextoConfirmacion.getStyleClass().clear();
        campoTextoImporte.getStyleClass().clear();
        
        cajaCampoConfirmacion.setStyle(null);
        
        cajaReferencia.setStyle(null);
        cajaImporte.setStyle(null);
        
        cajaBotonValida.setStyle(null);
        
        
    }
    
    private void ajustaComponentesRef(){
        
        cajaLeyendaConfirmacion1.getChildren().addAll(etiquetaConfirmacion1);
        cajaLeyendaConfirmacion2.getChildren().addAll(etiquetaConfirmacion2);
        
        cajaLeyendaConfirmacion.getChildren().addAll(cajaLeyendaConfirmacion1,cajaLeyendaConfirmacion2);
        
        
        vistaImagenValidarPago.setFitHeight(25);
        vistaImagenValidarPago.setFitWidth(25);
        
        vistaImagenCancelarPago.setFitHeight(25);
        vistaImagenCancelarPago.setFitWidth(25);
        
        botonConfirmacion.setPrefSize(200, 50);
        botonConfirmacion.setMaxSize(200, 50);
        botonConfirmacion.setMinSize(200, 50);
        
        
        cajaCampoConfirmacion.setAlignment(Pos.CENTER);
        
        cajaCampoConfirmacion.getChildren().addAll(campoTextoConfirmacion,botonConfirmacion);
        
        cajaPrincipal.getChildren().addAll(cajaLeyendaConfirmacion,cajaCampoConfirmacion);
        
        heightP = 220;
        
        this.cajaPrincipal.setMinHeight(heightP);
        this.cajaPrincipal.setPrefHeight(heightP);
        this.cajaPrincipal.setMaxHeight(heightP);
        
        
        
        panel.setTop(cajaPrincipal);
        panel.setMinHeight(heightP);
        panel.setPrefHeight(heightP);
        panel.setMaxHeight(heightP);
        panel.setMinWidth(700);
        panel.setPrefWidth(700);
        panel.setMaxWidth(700);
        
        
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                grupo.getChildren().addAll(panel);
                campoTextoConfirmacion.requestFocus();
            }
        }); 
        
        
    }
    
    
    
    private void ajustaComponentesRefImporte(){
        
        
        //agrega mensaje de validacion
        cajaLeyendaConfirmacion1.getChildren().addAll(etiquetaConfirmacion1);
        cajaLeyendaConfirmacion2.getChildren().addAll(etiquetaConfirmaRefImporte);
        cajaLeyendaConfirmacion.getChildren().addAll(cajaLeyendaConfirmacion1,cajaLeyendaConfirmacion2);
        
        
        //agrega caja de contenido para escribir referencia
        
        cajaReferencia.setAlignment(Pos.CENTER);
        cajaReferencia.getChildren().addAll(etiquetaReferencia,campoTextoConfirmacion);
        
        cajaImporte.setAlignment(Pos.CENTER);
        cajaImporte.getChildren().addAll(etiquetaImporte,campoTextoImporte);
        
        cajaBotonValida.setAlignment(Pos.CENTER);
        cajaBotonValida.getChildren().addAll(botonConfirmacion);
        
        
        cajaPrincipal.getChildren().addAll(cajaLeyendaConfirmacion,cajaReferencia,cajaImporte,cajaBotonValida);
        heightP = 520;
        
        
        this.cajaPrincipal.setMinHeight(heightP);
        this.cajaPrincipal.setPrefHeight(heightP);
        this.cajaPrincipal.setMaxHeight(heightP);
        
        
        panel.setTop(cajaPrincipal);
        panel.setMinHeight(heightP);
        panel.setPrefHeight(heightP);
        panel.setMaxHeight(heightP);
        panel.setMinWidth(700);
        panel.setPrefWidth(700);
        panel.setMaxWidth(700);
        
        
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                grupo.getChildren().addAll(panel);
                campoTextoConfirmacion.requestFocus();
            }
        }); 
        
    }
    
    
    private void resetAjustes(){
        
        cajaLeyendaConfirmacion1.getChildren().remove(etiquetaConfirmacion1);
        
        cajaLeyendaConfirmacion2.getChildren().remove(etiquetaConfirmaRefImporte);
        cajaLeyendaConfirmacion2.getChildren().remove(etiquetaConfirmacion2);
        
        
        cajaLeyendaConfirmacion.getChildren().remove(cajaLeyendaConfirmacion1);
        cajaLeyendaConfirmacion.getChildren().remove(cajaLeyendaConfirmacion2);
        
        cajaCampoConfirmacion.getChildren().remove(campoTextoConfirmacion);
        cajaCampoConfirmacion.getChildren().remove(botonConfirmacion);
        
        cajaReferencia.getChildren().remove(etiquetaReferencia);
        cajaReferencia.getChildren().remove(campoTextoConfirmacion);
        
        cajaImporte.getChildren().remove(etiquetaImporte);
        cajaImporte.getChildren().remove(campoTextoImporte);
        
        cajaBotonValida.getChildren().remove(botonConfirmacion);
        
        cajaPrincipal.getChildren().remove(cajaLeyendaConfirmacion);
        cajaPrincipal.getChildren().remove(cajaReferencia);
        cajaPrincipal.getChildren().remove(cajaImporte);
        cajaPrincipal.getChildren().remove(cajaBotonValida);
        cajaPrincipal.getChildren().remove(cajaCampoConfirmacion);
        
        this.cajaPrincipal.setMinHeight(0);
        this.cajaPrincipal.setPrefHeight(0);
        this.cajaPrincipal.setMaxHeight(0);
        
        this.campoTextoConfirmacion = utilerias.getTextField("", 350, 50);
        this.campoTextoImporte = utilerias.getTextField("", 350, 50);
        
        
        panel.setTop(null);
        panel.setMinHeight(0);
        panel.setPrefHeight(0);
        panel.setMaxHeight(0);
        
        
        
        
        this.panel = new BorderPane();
        this.cajaPrincipal = utilerias.getVBox(0, 700, 220);
        
        grupo.getChildren().remove(panel);
        
        this.grupo = new Group();
        
        
    }
    
    
    private void ajustaComponentes (){
        
        cajaLeyendaConfirmacion1.getChildren().addAll(etiquetaConfirmacion1);
        cajaLeyendaConfirmacion2.getChildren().addAll(etiquetaConfirmacion2);
        
        cajaLeyendaConfirmacion.getChildren().addAll(cajaLeyendaConfirmacion1,cajaLeyendaConfirmacion2);
        
        vistaImagenValidarPago.setFitHeight(25);
        vistaImagenValidarPago.setFitWidth(25);
        
        vistaImagenCancelarPago.setFitHeight(25);
        vistaImagenCancelarPago.setFitWidth(25);
        
        botonConfirmacion.setPrefSize(200, 50);
        botonConfirmacion.setMaxSize(200, 50);
        botonConfirmacion.setMinSize(200, 50);
        
        cajaCampoConfirmacion.getChildren().addAll(campoTextoConfirmacion,botonConfirmacion);
        
        cajaPrincipal.getChildren().addAll(cajaLeyendaConfirmacion,cajaCampoConfirmacion);
        
        panel.setTop(cajaPrincipal);
        panel.setMinHeight(220);
        panel.setPrefHeight(220);
        panel.setMaxHeight(220);
        panel.setMinWidth(700);
        panel.setPrefWidth(700);
        panel.setMaxWidth(700);
        
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                grupo.getChildren().addAll(panel);
                campoTextoConfirmacion.requestFocus();
            }
        });
        
    }
    
    private void crearHandlers (){
        
        botonConfirmacion.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                segundaReferencia = campoTextoConfirmacion.getText().trim();
                segundoMontoPago = campoTextoImporte.getText().trim();
                validarReferenciasIngresadas();
            }
        });
        
        campoTextoConfirmacion.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if(ENTER.match(arg0)){
                    
                    if (montoPago>=importeLimite){
                        campoTextoImporte.requestFocus();
                    }else{
                        botonConfirmacion.requestFocus();
                    }
                }
            }
        });
        
        campoTextoImporte.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                if(ENTER.match(arg0)){
                        botonConfirmacion.requestFocus();
                }
            }
        });
        
        botonCancelarValidacion.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        stage.close();
                        binding.regresarFoco();
                        
                    }
                });
            }
        });
        
    }
    
    private void validarReferenciasIngresadas(){
        
       if (montoPago >= importeLimite){
           if (!segundoMontoPago.isEmpty()){
                
                try {
                
                    double segMonto = Double.parseDouble(segundoMontoPago);
                    
                    if(referencia.equals(segundaReferencia) && montoPago == segMonto){
                        
                        
                        /**************************REFERENCIA Y MONTO OK SE PROCEDE A ENVIO**********/
                        
                        
                        //se continua el proceso una vez que se han validado las referencias ingresadas
                        SION.log(Modulo.PAGO_SERVICIOS, "Referencias coinciden, se continua con el proceso", Level.INFO);
                        if (String.valueOf(emisorId).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.TELMEX.PAGATODO"))){
                            if (referencia.length()==10 || referencia.length()==20){
                                /*******************************************************************************/
                                comisionDto = pagatodoBinding.consultaComisionXidEmisor(emisorId);
                                if (comisionDto.getCodigoError()==0){
                                    comision = Double.valueOf(comisionDto.getArrayDetalleArticulos()[0].getFcComision());
                                    pagatodoBinding.consultarReferencia(emisorId, montoPago, referencia, comision , tipoServicio, comisionDto);
                                    binding.setComision(comision);
                                    campoTextoConfirmacion.clear();
                                    campoTextoImporte.clear();
                                    stage.close();
                                }else{
                                    mensajeComision = comisionDto.getDescError();
                                    //se muestra mensaje de error y se borran referencias para iniciar nuevamente con el proceso
                                    SION.log(Modulo.PAGO_SERVICIOS, "No se pudo obtener la comison del emisor: (" +mensajeComision+" )", Level.INFO);
                                    campoTextoConfirmacion.clear();
                                    campoTextoImporte.clear();
                                    pagatodoBinding.limpiarCamposReferencia();
                                    stage.close();
                                    Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            AdministraVentanas.mostrarAlertaRetornaFoco("El pago no puede ser procesado " + mensajeComision, TipoMensaje.ADVERTENCIA, botonCancelarValidacion);
                                        }
                                    });
                                }
                                /**************************************************************************************/
                            }else{
                                mensajeComision = "La referencia es invalida, debe de ser de 10 o 20";
                                //se muestra mensaje de error y se borran referencias para iniciar nuevamente con el proceso
                                SION.log(Modulo.PAGO_SERVICIOS, "No se pudo obtener la comison del emisor: (" +mensajeComision+" )", Level.INFO);
                                campoTextoConfirmacion.clear();
                                campoTextoImporte.clear();
                                pagatodoBinding.limpiarCamposReferencia();
                                stage.close();
                                
                                Platform.runLater(new Runnable() {
                                    
                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco("El pago no puede ser procesado " + mensajeComision, TipoMensaje.ADVERTENCIA, botonCancelarValidacion);
                                    }
                                });
                            }
                        }else{
                            /********************************************************************/
                            comisionDto = pagatodoBinding.consultaComisionXidEmisor(emisorId);
                            if (comisionDto.getCodigoError()==0){
                                comision = Double.valueOf(comisionDto.getArrayDetalleArticulos()[0].getFcComision());
                                pagatodoBinding.consultarReferencia(emisorId, montoPago, referencia, comision, tipoServicio, comisionDto);
                                binding.setComision(comision);
                                campoTextoConfirmacion.clear();
                                campoTextoImporte.clear();
                                stage.close();
                            }else{
                                mensajeComision = comisionDto.getDescError();
                                //se muestra mensaje de error y se borran referencias para iniciar nuevamente con el proceso
                                SION.log(Modulo.PAGO_SERVICIOS, "No se pudo obtener la comison del emisor: (" +mensajeComision+" )", Level.INFO);
                                campoTextoConfirmacion.clear();
                                campoTextoConfirmacion.clear();
                                pagatodoBinding.limpiarCamposReferencia();
                                stage.close();
                                
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        AdministraVentanas.mostrarAlertaRetornaFoco("El pago no puede ser procesado " + mensajeComision, TipoMensaje.ADVERTENCIA, botonCancelarValidacion);
                                    }
                                });
                            }
                            /*********************************************************************/
                        }

                        
                        /*******************************TERMINA BLOQUE DE REFERENCIAS VALIDAS ********************/
                        
                        
                    }else{
                        
                         //se muestra mensaje de error y se borran referencias para iniciar nuevamente con el proceso
                        SION.log(Modulo.PAGO_SERVICIOS, "Las referencias ingresadas o importe no coinciden no coinciden    Referencia: (" +referencia+" - "+segundaReferencia+" )   Monto: (" +montoPago+" - "+segundoMontoPago+")", Level.INFO);
                        campoTextoConfirmacion.clear();
                        pagatodoBinding.limpiarCamposReferencia();
                        stage.close();
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                AdministraVentanas.mostrarAlertaRetornaFoco("Las referencias o importe  no coinciden, por favor verifica.", TipoMensaje.ADVERTENCIA, botonCancelarValidacion);
                            }
                        });
                        
                    }
                
                }catch (Exception e) {
                    //se muestra mensaje de error y se borran referencias para iniciar nuevamente con el proceso
                    SION.log(Modulo.PAGO_SERVICIOS, "Ocurrio un error al validar la referencia del segundo monto : msg" + e.getMessage(), Level.INFO);
                    campoTextoConfirmacion.clear();
                    pagatodoBinding.limpiarCamposReferencia();
                    stage.close();
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            AdministraVentanas.mostrarAlertaRetornaFoco("Las referencias no coinciden, por favor verifica.", TipoMensaje.ADVERTENCIA, botonCancelarValidacion);
                        }
                    });
                
                }finally{
                    
                }
            }
       
       }else{
           
           if(referencia.equals(segundaReferencia)){
               //se continua el proceso una vez que se han validado las referencias ingresadas
               SION.log(Modulo.PAGO_SERVICIOS, "Referencias coinciden, se continua con el proceso", Level.INFO);
               
               if (String.valueOf(emisorId).equals(SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "EMISOR.TELMEX.PAGATODO"))){
                   
                   if (referencia.length()==10 || referencia.length()==20){
                       
                       /*******************************************************************************/
                       comisionDto = pagatodoBinding.consultaComisionXidEmisor(emisorId);
                       
                       if (comisionDto.getCodigoError()==0){
                           
                           comision = Double.valueOf(comisionDto.getArrayDetalleArticulos()[0].getFcComision());
                           pagatodoBinding.consultarReferencia(emisorId, montoPago, referencia, comision , tipoServicio, comisionDto);
                           binding.setComision(comision);
                           campoTextoConfirmacion.clear();
                           stage.close();
                       }else{
                           mensajeComision = comisionDto.getDescError();
                           //se muestra mensaje de error y se borran referencias para iniciar nuevamente con el proceso
                           SION.log(Modulo.PAGO_SERVICIOS, "No se pudo obtener la comison del emisor: (" +mensajeComision+" )", Level.INFO);
                           campoTextoConfirmacion.clear();
                           pagatodoBinding.limpiarCamposReferencia();
                           stage.close();
                           
                           Platform.runLater(new Runnable() {
                               @Override
                               public void run() {
                                   AdministraVentanas.mostrarAlertaRetornaFoco("El pago no puede ser procesado " + mensajeComision, TipoMensaje.ADVERTENCIA, botonCancelarValidacion);
                               }
                           });
                       }
                       /**************************************************************************************/
                   }else{
                       mensajeComision = "La referencia es invalida, debe de ser de 10 o 20";
                       //se muestra mensaje de error y se borran referencias para iniciar nuevamente con el proceso
                       SION.log(Modulo.PAGO_SERVICIOS, "No se pudo obtener la comison del emisor: (" +mensajeComision+" )", Level.INFO);
                       campoTextoConfirmacion.clear();
                       pagatodoBinding.limpiarCamposReferencia();
                       stage.close();
                       
                       Platform.runLater(new Runnable() {
                           @Override
                           public void run() {
                               AdministraVentanas.mostrarAlertaRetornaFoco("El pago no puede ser procesado " + mensajeComision, TipoMensaje.ADVERTENCIA, botonCancelarValidacion);
                           }
                       });
                   }
               }else{
                   /********************************************************************/
                   comisionDto = pagatodoBinding.consultaComisionXidEmisor(emisorId);
                   if (comisionDto.getCodigoError()==0){
                       comision = Double.valueOf(comisionDto.getArrayDetalleArticulos()[0].getFcComision());
                       pagatodoBinding.consultarReferencia(emisorId, montoPago, referencia, comision, tipoServicio, comisionDto);
                       binding.setComision(comision);
                       campoTextoConfirmacion.clear();
                       stage.close();
                   }else{
                       mensajeComision = comisionDto.getDescError();
                       //se muestra mensaje de error y se borran referencias para iniciar nuevamente con el proceso
                       SION.log(Modulo.PAGO_SERVICIOS, "No se pudo obtener la comison del emisor: (" +mensajeComision+" )", Level.INFO);
                       campoTextoConfirmacion.clear();
                       pagatodoBinding.limpiarCamposReferencia();
                       stage.close();
                       Platform.runLater(new Runnable() {
                           
                           @Override
                           public void run() {
                               AdministraVentanas.mostrarAlertaRetornaFoco("El pago no puede ser procesado " + mensajeComision, TipoMensaje.ADVERTENCIA, botonCancelarValidacion);
                           }
                       });
                   }
                   /*********************************************************************/
               }
           }else{
               //se muestra mensaje de error y se borran referencias para iniciar nuevamente con el proceso
               SION.log(Modulo.PAGO_SERVICIOS, "Las referencias ingresadas no coinciden: (" +referencia+" - "+segundaReferencia+" )", Level.INFO);
               campoTextoConfirmacion.clear();
               pagatodoBinding.limpiarCamposReferencia();
               stage.close();
               Platform.runLater(new Runnable() {
                   @Override
                   public void run() {
                       AdministraVentanas.mostrarAlertaRetornaFoco("Las referencias no coinciden, por favor verifica.", TipoMensaje.ADVERTENCIA, botonCancelarValidacion);
                   }
               });
           }
       
       }
        
        
        
    }
    
    public void abreStageConfirmacionReferenciaPagatodo(final long _emisorId, final double _monto, final String _referencia, final int _tipoServicio){
        
        SION.log(Modulo.PAGO_SERVICIOS, "Abriendo Popup de confirmacion de referencia de pago de PagaTodo", Level.INFO);
        
        
        RespuestaParametroBean respParametro = binding.getImporteLimValida();
        
        if (respParametro.getCodigoError()==0){
            importeLimite = respParametro.getValorConfiguracion();
        }else{
            importeLimite = 5000;
        }
        
        importeLimite = 5000;
        

        //si no existe escena la crea, esto cuando se abre por primera vez popup de confirmacion pagatodo
        if (escena == null) {

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    
                    agregarEstilos();
                    
                    if (_monto>=importeLimite){
                        
                        SION.log(Modulo.PAGO_SERVICIOS, "Monto mayor a "+importeLimite, Level.INFO);
                        escena = new Scene(grupo, 700, 520, Color.TRANSPARENT);
                        ajustaComponentes();
                        ajustaComponentesRefImporte();
                    }else{
                        
                        SION.log(Modulo.PAGO_SERVICIOS, "Monto mayor a "+importeLimite, Level.INFO);
                        escena = new Scene(grupo, 700, 220, Color.TRANSPARENT);
                        
                        ajustaComponentesRef();
                    
                    }
                    
                    
                    stage = new Stage(StageStyle.UNDECORATED);
                    stage.initModality(Modality.APPLICATION_MODAL);
                    
                    //inicializa referencias
                    referencia="";
                    segundaReferencia = "";
                    montoPago = 0;
                    emisorId = 0;
                    comision = 0;
                    
                    referencia =_referencia;
                    montoPago = _monto;
                    emisorId = _emisorId;
                    tipoServicio = _tipoServicio;
                    
                    
                    SION.log(Modulo.PAGO_SERVICIOS, "Inicilizando referencias: ("+referencia+") - ("+segundaReferencia+")", Level.INFO);

                    
                    
                    crearHandlers();
                    
                    escena.getStylesheets().addAll(PagoServiciosVista.class.getResource("/neto/sion/tienda/pago/servicios/vista/EstilosPagoServicios.css").toExternalForm());

                    stage.setTitle("Confirmacion de referencia de pago de servicio PagaTodo");
                    stage.setScene(escena);
                    stage.centerOnScreen();
                    stage.show();
                }
            });

        } else {
            
            
            escena.getStylesheets().clear();
            escena.setRoot(null);
            resetStyles();
            resetAjustes();
            agregarEstilos(); 
            
            
            
            if (_monto>importeLimite){
                SION.log(Modulo.PAGO_SERVICIOS, "Monto mayor a " + importeLimite, Level.INFO);
                escena = new Scene(grupo, 700, 520, Color.TRANSPARENT);
                ajustaComponentesRefImporte();
                
            }else{
                SION.log(Modulo.PAGO_SERVICIOS, "Monto menor a "+ importeLimite, Level.INFO);
                escena = new Scene(grupo, 700, 220, Color.TRANSPARENT);
                ajustaComponentesRef();
            
            }
            
            
            escena.getStylesheets().addAll(PagoServiciosVista.class.getResource("/neto/sion/tienda/pago/servicios/vista/EstilosPagoServicios.css").toExternalForm());
            
            
            crearHandlers();
            
            stage = new Stage(StageStyle.UNDECORATED);
            stage.initModality(Modality.APPLICATION_MODAL);
            
            
            
            //cuando ya se ha abierto antes el popup
            
            //inicializa referencias
            referencia="";
            segundaReferencia = "";
            emisorId = 0;
            montoPago = 0;
            emisorId = _emisorId;
            montoPago = _monto;
            tipoServicio = _tipoServicio;
            
            referencia =_referencia;

            stage.setTitle("Confirmacion de referencia de pago de servicio PagaTodo");
            stage.setScene(escena);
            stage.centerOnScreen();
            stage.show();

        }
        
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                
                campoTextoConfirmacion.requestFocus();
            }
        });
        
    }
    
}
