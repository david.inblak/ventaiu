/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.pago.servicios.modelo.*;
import neto.sion.venta.pagatodo.ps.facade.ConsumirPagaTodoPSImp;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.internal.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.apache.commons.dbcp.DelegatingConnection;

/**
 *
 * @author fvega
 */
public class PagoServiciosDao {

    private String sql;
    private CallableStatement cs;
    private ResultSet rs;
    private Connection connection;
    private OracleConnection oracleConnection;
    private Object[] objetoPagos;
    private StructDescriptor estrcuturaPago;
    private ArrayDescriptor estructuraArreglo;
    private ARRAY arregloPagos;
    
    //antad
    private final int PAGO_EXITOSO_CENTRAL = 3;

    public PagoServiciosDao() {
        this.sql = null;
        this.cs = null;
        this.rs = null;
    }

    public RespuestaEmpresasBean consultarEmpresas(PeticionEmpresasBean _peticion) {
        if (_peticion != null) {
            SION.log(Modulo.PAGO_SERVICIOS, "Peticion de consulta de empresas: " + _peticion.toString(), Level.INFO);
            RespuestaEmpresasBean respuesta = new RespuestaEmpresasBean();

            try {

                sql = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.FNCONSULTASERVICIOS");
                cs = Conexion.prepararLlamada(sql);

                cs.registerOutParameter(1, OracleTypes.CURSOR);
                cs.execute();

                rs = (ResultSet) cs.getObject(1);

                ArrayList<EmpresaBean> arregloEmpresas = new ArrayList<EmpresaBean>();

                EmpresaBean bean;
                        
                while (rs.next()) {
                    
                    
                        bean = new EmpresaBean();
                        bean.setEmpresaId(rs.getInt(1));
                        bean.setCodigoBarras(rs.getString(2));
                        bean.setDescrpcion(rs.getString(3));
                        bean.setAgrupacionId(rs.getInt(5));
                        bean.setCategoriaId(rs.getInt(6));
                        bean.setTipoServicio(rs.getInt(7));
                        arregloEmpresas.add(bean);
            
                    
                }

                EmpresaBean[] beans = new EmpresaBean[arregloEmpresas.size()];
                int contador = 0;
                for (EmpresaBean eb : arregloEmpresas) {
                    if (eb != null) {
                        beans[contador] = eb;
                        contador++;
                    }
                }
                

                respuesta.setEmpresaBeans(beans);
                
                if (beans.length > 0) {
                    respuesta.setCodigoError(0);
                    respuesta.setDescError("OK");
                } else {
                    respuesta.setCodigoError(1);
                    respuesta.setDescError("SIN DATOS");
                }
                
                
            } catch (Exception e) {
                respuesta = null;
                SION.log(Modulo.PAGO_SERVICIOS, "ocurrio una excepcion al consultar empresas", Level.SEVERE);
                SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
            } finally {
                Conexion.cerrarResultSet(rs);
                Conexion.cerrarCallableStatement(cs);
            }

            SION.log(Modulo.PAGO_SERVICIOS, "Respuesta recibida: Codigo Error = " + respuesta.getCodigoError()+"  DescRespuesta = " + respuesta.getDescError() , Level.INFO);

            return respuesta;
        } else {
            return null;
        }
    }

    public RespuestaVentaDiaBean guardarVentaDia(PeticionVentaDiaBean _peticion) {
        if (_peticion != null) {

            SION.log(Modulo.PAGO_SERVICIOS, "Petición de inserción de venta del día: " + _peticion.toString(), Level.INFO);
            RespuestaVentaDiaBean respuesta = new RespuestaVentaDiaBean();
            int contador = 0;

            try {

                connection = Conexion.obtenerConexion();
                oracleConnection = (OracleConnection) ((DelegatingConnection) connection).getInnermostDelegate();
                sql = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.SPREGISTRAREPORTEVENTA");
                SION.log(Modulo.PAGO_SERVICIOS, "SQL a ejecutar: " + sql, Level.INFO);
                cs = connection.prepareCall(sql);

                objetoPagos = new Object[_peticion.getVentaDiaBeans().length];

                estrcuturaPago = StructDescriptor.createDescriptor(SION.obtenerParametro(Modulo.PAGO_SERVICIOS,
                        "estructura.pago"), oracleConnection);

                estructuraArreglo = ArrayDescriptor.createDescriptor(SION.obtenerParametro(Modulo.PAGO_SERVICIOS,
                        "estructura.arreglo.pago"), oracleConnection);

                for (VentaDiaBean bean : _peticion.getVentaDiaBeans()) {
                    Object[] objeto = new Object[]{bean.getArticuloId(), bean.getCodigoBarras(), bean.getCantidad(),
                        bean.getPrecio(), bean.getCosto(), bean.getDescuento(), bean.getIva(), bean.getIepsId()};
                    objetoPagos[contador] = new STRUCT(estrcuturaPago, oracleConnection, objeto);
                    contador++;
                }

                arregloPagos = new ARRAY(estructuraArreglo, oracleConnection, objetoPagos);

                cs.setArray(1, arregloPagos);
                cs.registerOutParameter(2, OracleTypes.NUMBER);
                cs.registerOutParameter(3, OracleTypes.VARCHAR);
                cs.execute();

                respuesta.setCodigoError(cs.getInt(2));
                respuesta.setDescripcionError(cs.getString(3));

                SION.log(Modulo.PAGO_SERVICIOS, "Respuesta de insercion: " + respuesta.toString(), Level.INFO);

            } catch (Exception e) {
                SION.log(Modulo.PAGO_SERVICIOS, "Ocurrió un error al realizar la insercion", Level.SEVERE);
                SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
                respuesta = null;
            } finally {
                Conexion.cerrarConexion(connection);
                Conexion.cerrarCallableStatement(cs);
            }

            return respuesta;
        } else {
            return null;
        }
    }

    public RespuestaConsultaReferenciaBean consultaReferencia(PeticionConsultaReferenciaBean _peticion) {

        SION.log(Modulo.PAGO_SERVICIOS, "Petición de consulta de pago: " + _peticion.toString(), Level.INFO);
        String sql = null;
        CallableStatement cs = null;

        RespuestaConsultaReferenciaBean respuesta = new RespuestaConsultaReferenciaBean();

        try {

            sql = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.SPCONSULTAREFERENCIA");
            SION.log(Modulo.PAGO_SERVICIOS, "SQL a ejecutar... " + sql, Level.INFO);
            cs = Conexion.prepararLlamada(sql);
            cs.setString(1, _peticion.getPaReferencia());
            cs.setDouble(2, _peticion.getPaMontoPago());
            cs.setLong(3, _peticion.getPaEmisorId());
            cs.setString(4, _peticion.getPaFechaConc());
            cs.setInt(5, _peticion.getPaPaisId());
            cs.setInt(6, _peticion.getPaTiendaId());
            cs.registerOutParameter(7, OracleTypes.NUMBER);
            cs.registerOutParameter(8, OracleTypes.VARCHAR);
            cs.execute();

            respuesta.setPaCdgError(cs.getInt(7));
            respuesta.setPaDescError(cs.getString(8));

            SION.log(Modulo.PAGO_SERVICIOS, "Respuesta recibida: " + respuesta.toString(), Level.INFO);

        } catch (Exception e) {
            respuesta = null;
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e, sql);
        } finally {
            Conexion.cerrarCallableStatement(cs);
        }
        return respuesta;
    }

    public RespuestaConsultaOriflameBean consultarPedidosOriflame(PeticionConsultaOriflameBean _peticion) {
        if (_peticion != null) {
            SION.log(Modulo.PAGO_SERVICIOS, "Peticion de consulta de pedidos Oriflame: " + _peticion.toString(), Level.INFO);
            RespuestaConsultaOriflameBean respuesta = new RespuestaConsultaOriflameBean();

            try {

                sql = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.FNCONSULTAPEDIDOORIFLAME");
                cs = Conexion.prepararLlamada(sql);

                cs.registerOutParameter(1, OracleTypes.CURSOR);
                cs.setString(2, _peticion.getReferencia());
                cs.setInt(3, _peticion.getPaisId());
                cs.setLong(4, _peticion.getTiendaId());
                cs.execute();

                rs = (ResultSet) cs.getObject(1);

                ArrayList<String> arregloPedidos = new ArrayList<String>();
                int contador = 0;

                while (rs.next()) {
                    String pedido = rs.getString(1);
                    arregloPedidos.add(pedido);
                }

                respuesta.setPedidos(arregloPedidos.toArray(new String[0]));
                SION.log(Modulo.PAGO_SERVICIOS, "Respuesta recibida: " + respuesta.toString(), Level.INFO);

                return respuesta;

            } catch (Exception e) {
                respuesta = null;
                SION.log(Modulo.PAGO_SERVICIOS, "ocurrio una excepcion al consultar empresas", Level.SEVERE);
                SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e);
            } finally {
                Conexion.cerrarResultSet(rs);
                Conexion.cerrarCallableStatement(cs);
            }

            SION.log(Modulo.PAGO_SERVICIOS, "Respuesta recibida: " + respuesta.toString(), Level.INFO);

            return respuesta;
        } else {
            return null;
        }
    }
    
    public RespuestaActualizacionPagoFueraLineaBean actualizarPagoFL (PeticionActualizacionPagoFueraLineaBean _peticion, int _estatus){
        RespuestaActualizacionPagoFueraLineaBean respuesta = new RespuestaActualizacionPagoFueraLineaBean();
        
        SION.log(Modulo.PAGO_SERVICIOS, "Petición de actulizacion de pago: " + _peticion.toString(), Level.INFO);
        String sql = null;
        CallableStatement cs = null;

        try {

            sql = SION.obtenerParametro(Modulo.PAGO_SERVICIOS, "PAGO.SERVICIOS.SPUPDESTATUSPSANTAD");
            SION.log(Modulo.PAGO_SERVICIOS, "SQL a ejecutar... " + sql, Level.INFO);
            cs = Conexion.prepararLlamada(sql);
            cs.setLong(1, _peticion.getConciliacionId());
            //valida que el pago sea exitoso para actulizar no. de transaccion, de lo contrario no sera tomado en cuenta por demonio
            if(_estatus == PAGO_EXITOSO_CENTRAL){
                cs.setLong(2, _peticion.getTransaccionId());
            }else{
                cs.setObject(2, null);
            }
            cs.setInt(3, _peticion.getEstatusId());
            cs.setString(4, _peticion.getRespuestaAntad());
            cs.registerOutParameter(5, OracleTypes.NUMBER);
            cs.registerOutParameter(6, OracleTypes.VARCHAR);
            cs.execute();

            respuesta.setCodigoError(cs.getInt(5));
            respuesta.setDescError(cs.getString(6));

            SION.log(Modulo.PAGO_SERVICIOS, "Respuesta recibida: " + respuesta.toString(), Level.INFO);

        } catch (Exception e) {
            respuesta = null;
            SION.logearExcepcion(Modulo.PAGO_SERVICIOS, e, sql);
        } finally {
            Conexion.cerrarCallableStatement(cs);
        }
        
        return respuesta;
    }
    
    
     public RespuestaParametroBean consultaParametroOperacion(PeticionParametroBean _peticion) {

        SION.log(Modulo.VENTA, "Petición de parámetro de operación: " + _peticion.toString(), Level.INFO);

        RespuestaParametroBean respuesta = new RespuestaParametroBean();

        try {

            sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS");
            SION.log(Modulo.VENTA, "SQL a ejecutar... " + sql, Level.INFO);
            cs = Conexion.prepararLlamada(sql);
            cs.setInt(1, _peticion.getPaisId());
            cs.setInt(2, _peticion.getSistemaId());
            cs.setInt(3, _peticion.getModuloId());
            cs.setInt(4, _peticion.getSubmoduloId());
            cs.setInt(5, _peticion.getConfiguracionId());
            cs.registerOutParameter(6, OracleTypes.NUMBER);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.NUMBER);
            cs.execute();

            respuesta.setCodigoError(cs.getInt(6));
            respuesta.setDescripcionError(cs.getString(7));
            respuesta.setValorConfiguracion(cs.getDouble(8));

            SION.log(Modulo.VENTA, "Respuesta recibida: " + respuesta.toString(), Level.INFO);

        } catch (Exception e) {
            respuesta = null;
            SION.logearExcepcion(Modulo.VENTA, e, sql);
        } finally {
            Conexion.cerrarCallableStatement(cs);
        }

        return respuesta;

    }
   
   
      
}