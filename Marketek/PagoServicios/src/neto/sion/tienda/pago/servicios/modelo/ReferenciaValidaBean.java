/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

import neto.sion.pago.servicios.cliente.dto.RespuestaReferenciaDto;
import neto.sion.tienda.venta.cliente.servicios.antad.dto.RespuestaConsultaDto;

/**
 *
 * @author fvega
 */
public class ReferenciaValidaBean {

    private String referencia;
    private String subReferencia;
    private int emisorId;
    private RespuestaReferenciaDto respuestaReferencia;
    private RespuestaConsultaDto respuestaConsultaAntad;

    public ReferenciaValidaBean(String _referencia, String _subReferencia, int _emisorId, RespuestaReferenciaDto _respuestaReferencia) {
        this.referencia = _referencia;
        this.subReferencia = _subReferencia;
        this.emisorId = _emisorId;
        this.respuestaReferencia = _respuestaReferencia;
        this.respuestaConsultaAntad = null;
    }

    public ReferenciaValidaBean(String _referencia, String _subReferencia, int _emisorId, RespuestaConsultaDto _respuestaConsulta) {
        this.referencia = _referencia;
        this.subReferencia = _subReferencia;
        this.emisorId = _emisorId;
        this.respuestaConsultaAntad = _respuestaConsulta;
        this.respuestaReferencia = null;
    }

    public ReferenciaValidaBean() {
    }

    public int getEmisorId() {
        return emisorId;
    }

    public void setEmisorId(int emisorId) {
        this.emisorId = emisorId;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public RespuestaReferenciaDto getRespuestaReferencia() {
        return respuestaReferencia;
    }

    public void setRespuestaReferencia(RespuestaReferenciaDto respuestaReferencia) {
        this.respuestaReferencia = respuestaReferencia;
    }

    public String getSubReferencia() {
        return subReferencia;
    }

    public void setSubReferencia(String subReferencia) {
        this.subReferencia = subReferencia;
    }

    public RespuestaConsultaDto getRespuestaConsultaAntad() {
        return respuestaConsultaAntad;
    }

    public void setRespuestaConsultaAntad(RespuestaConsultaDto respuestaConsultaAntad) {
        this.respuestaConsultaAntad = respuestaConsultaAntad;
    }

    @Override
    public String toString() {
        return "ReferenciaValidaBean{" + "referencia=" + referencia + ", subReferencia=" + subReferencia + ", emisorId=" + emisorId + ", respuestaReferencia=" + respuestaReferencia + ", respuestaConsultaAntad=" + respuestaConsultaAntad + '}';
    }
}
