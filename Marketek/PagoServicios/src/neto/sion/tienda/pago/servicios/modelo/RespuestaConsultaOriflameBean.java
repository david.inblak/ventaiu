/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.pago.servicios.modelo;

import java.util.Arrays;

/**
 *
 * @author fvegap
 */
public class RespuestaConsultaOriflameBean {

    private String[] pedidos;

    public String[] getPedidos() {
        return pedidos;
    }

    public void setPedidos(String[] pedidos) {
        this.pedidos = pedidos;
    }

    @Override
    public String toString() {
        return "PagoConsultaOriflameBean{" + "pedidos=" + Arrays.toString(pedidos)
                + '}';
    }
}
