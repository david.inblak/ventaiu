package promocionescupone.test;


import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.util.ConversionBeanes;
import neto.sion.tienda.venta.promociones.cupones.modelo.addcupon.AddCuponBeanReq;
import neto.sion.tienda.venta.promociones.cupones.modelo.addcupon.AddCuponBeanResp;
import neto.sion.tienda.venta.promociones.cupones.mvp.concreto.MarketecRepositorioCupones;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dramirezr
 */
public class TestAddCupon {
    public static void main(String[] args) {
        AddCuponBeanReq sol = new AddCuponBeanReq();
        sol.setCodigoCupon(201910301835L);
        sol.setPedido(123456789);
        sol.setPos(3);
        sol.setSucursal(3725);
        sol.setTicket("123456789");
        sol.setToken(null);
        
        try {
           

            String req = (String)ConversionBeanes.convertir(sol, String.class);
            System.out.println(">" + req);
            
            MarketecRepositorioCupones repositorio = new MarketecRepositorioCupones();
            Object desctosStr = repositorio.calculaDescuentosCupon(req);
            
            AddCuponBeanResp descBean = (AddCuponBeanResp) ConversionBeanes.convertir(desctosStr, AddCuponBeanResp.class);
            System.out.println(">" + descBean);
        } catch (Exception ex) {
            ex.printStackTrace();
            SION.logearExcepcion(Modulo.VENTA, ex, "TestAddCupon");
        }
    }
}
