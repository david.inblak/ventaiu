/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package promocionescupone.test;

import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import neto.sion.tienda.venta.promociones.cupones.modelo.ArticuloCupon;
import neto.sion.tienda.venta.promociones.cupones.modelo.CuponBean;
import neto.sion.tienda.venta.promociones.cupones.modelo.finventa.FinTicketMktcBeanReq;
import neto.sion.tienda.venta.promociones.cupones.modelo.DatosCuponesVentaDto;
import neto.sion.tienda.venta.promociones.cupones.mvp.IPresentadorCupones;
import neto.sion.tienda.venta.promociones.cupones.mvp.IRepositorioCupones;
import neto.sion.tienda.venta.promociones.cupones.mvp.concreto.MarketecPresentadorCupones;
import neto.sion.tienda.venta.promociones.cupones.mvp.concreto.MarketecVistaCupones;

/**
 *
 * @author dramirezr
 */
public class TestPromocionesCupones extends Application {

    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Hello World!");
        VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
                
        StackPane root = new StackPane();
        root.getChildren().addAll(vbox);
        
        int alto=700, largo=1120;
        
        //IRepositorioCupones repositorioCupones = new MarketecRepositorioCupones();
        IRepositorioCupones repositorioCupones = new FakeRepositorioCupones();
        MarketecVistaCupones vistaGral = new MarketecVistaCupones();
        IPresentadorCupones presenter = new MarketecPresentadorCupones(vistaGral, repositorioCupones);
        vistaGral.setPresenter(presenter);
        
        vbox.getChildren().addAll(
               vistaGral.getVista()
                );
        
        
        List<ArticuloCupon> articulos = new ArrayList<ArticuloCupon>();
        
        articulos.add(new ArticuloCupon(
                /* Long id, Double cantidad, String nombre, Double precio, */
                3030000547L, 1.0, "REFRESCO COCA COLA COLA SIN AZUCAR 2.5 LT.", 22.5,
                /* Double costo, Double descuento, Double iva, Double agranel, */
                18.39, 0.0, 0.16, 0.0,
                /* Integer idFamilia, Integer idIEPS, Integer unidad, Integer normaempaque, */
                6, 69, 1, 1,
                /* CuponBean cupon, String codBarras, String codBarraspadre, Integer tipoDescuento*/
                new CuponBean(), "75010553216811", "0", 0, 0.0));
        
        articulos.add(new ArticuloCupon(
                /* Long id, Double cantidad, String nombre, Double precio, */
                1010002281L, 1.0, "FRIJOL BEST CHOICE FLOR DE MAYO 900 GR.", 17.9,
                /* Double costo, Double descuento, Double iva, Double agranel, */
                12.4, 0.0, 0.0, 0.0,
                /* Integer idFamilia, Integer idIEPS, Integer unidad, Integer normaempaque, */
                6, 69, 1, 1,
                /* CuponBean cupon, String codBarras, String codBarraspadre, Integer tipoDescuento*/
                new CuponBean(), "7503008935945", "0", 0, 0.0));
        
        DatosCuponesVentaDto identidad = new DatosCuponesVentaDto();
        identidad.setFinTicketMktc(new FinTicketMktcBeanReq());
        identidad.getFinTicketMktc().setCajeroId(152886);
        identidad.getFinTicketMktc().setPos(1);
        identidad.getFinTicketMktc().setSucursal(3725);
        
        identidad.getArticulosCupon().addAll(articulos);
        //identidad.getDatos().put("articulos", articulos);
        
        presenter.init(identidad);

        primaryStage.setScene(new Scene(root, largo, alto));
        primaryStage.show();
    }
}
