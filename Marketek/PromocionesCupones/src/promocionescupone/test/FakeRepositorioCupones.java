/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package promocionescupone.test;

import java.util.ArrayList;
import java.util.List;
import neto.sion.tienda.venta.promociones.cupones.mvp.IRepositorioCupones;

/**
 *
 * @author dramirezr
 */
public class FakeRepositorioCupones implements IRepositorioCupones{
    List<String> respAddCupon = new ArrayList<String>();
    int indice =0;
    
    public FakeRepositorioCupones(){
        respAddCupon.add("{\"Mensaje\":\"OK\","
                + "\"codigoError\":0,"
                + "\"cupon\":"
                    + "{\"estado\":0,"
                    + "\"codigo\":\"4600060006347\","
                    + "\"estadoDesc\":\"Aplicado\","
                    + "\"linea1\":\"Aplicado\","
                    + "\"linea2\":\" \","
                    + "\"descuentoTotal\":{"
                        + "\"codigo\":\"4600060006347\","
                        + "\"monto\":\"25.00\","
                        + "\"oferta\":300434,"
                        + "\"descripcionOferta\":\"Descuento directo a la venta\","
                        + "\"prefijo\":\"\","
                        + "\"articuloId\":1000001,"
                        + "\"codigoBarras\":\"1000001\"}}}");
        respAddCupon.add("{\"Mensaje\":\"OK\","
                + "\"codigoError\":0,"
                + "\"cupon\":"
                    + "{\"estado\":0,"
                    + "\"codigo\":\"4600060006349\","
                    + "\"estadoDesc\":\"Aplicado\","
                    + "\"linea1\":\"Aplicado\","
                    + "\"linea2\":\" \","
                    + "\"descuentoTotal\":{"
                        + "\"codigo\":\"4600060006349\","
                        + "\"monto\":\"25.00\","
                        + "\"oferta\":300434,"
                        + "\"descripcionOferta\":\"Descuento directo a la venta\","
                        + "\"prefijo\":\"\","
                        + "\"articuloId\":1000001,"
                        + "\"codigoBarras\":\"1000001\"}}}");
        
        respAddCupon.add(" {\"Mensaje\":\"OK\","
                + "\"codigoError\":0,"
                + "\"cupon\":"
                + "{\"estado\":0,"
                + "\"codigo\":\"4600060003094\","
                + "\"estadoDesc\":\"Aplicado\","
                + "\"linea1\":\"Aplicado\","
                + "\"linea2\":\" \","
                + "\"articulos\":["
                    + "{\"cantidad\":2.0,"
                    + "\"articuloId\":6070005204,"
                    + "\"cupon\":4600060003094,"
                    + "\"descripcionOferta\":\"Descuento en cocas\","
                    + "\"montoDesc\":5.0,"
                    + "\"puntosCanje\":\"0\"}"
                    + "]}}");
    }     
    
    @Override
    public Object debeIntentarDescuentoCupones(Object Solicitud) throws Exception {
        return true;
    }

    @Override
    public Object calculaDescuentosAutomaticos(Object solicitud) throws Exception {
        Thread.sleep(500);
        //return cliente.validaDesceuntosAutomaticos(solicitud);
        //return "{\"Mensaje\":\"OK\",\"cuponesCuponera\":[{\"arrayArticulosCuponera\":[{\"cantidad\":1,\"codigoBarras\":\"7503008935945\",\"descripcionOferta\":\"Descuento Frijol Dummy\",\"monto\":1.0}],\"codigoCupon\":\"201910281640\"}],\"codigoError\":0,\"registroMktcId\":0}";
        return "{\"Mensaje\":\"OK\",\"codigoError\":0,\"registroMktcId\":200116000000466}";
    }

    @Override
    public Object calculaDescuentosCupon(Object solicitud) throws Exception {
        Thread.sleep(500);
        if( indice == 2 ) indice=0;        //return cliente.calculaDescuentoCupones(solicitud);
        //return "{\"Mensaje\":\"OK\",\"codigoError\":0,\"cupon\":{\"estado\":0,\"codigo\":\"201910281640\",\"estadoDesc\":\"Aplicado\",\"linea1\":\"Aplicado\",\"linea2\":\"\",\"articulos\":[{\"cantidad\":1.0,\"codigoBarras\":\"75010553216811\",\"cupon\":9808880000636,\"descripcionOferta\":\"Oferta Cupon Dummy\",\"montoDesc\":1.0}]}}";
        //return " {\"Mensaje\":\"OK\",\"codigoError\":0,\"cupon\":{\"estado\":0,\"codigo\":\"4600060003094\",\"estadoDesc\":\"Aplicado\",\"linea1\":\"Aplicado\",\"linea2\":\" \",\"articulos\":[{\"cantidad\":2.0,\"articuloId\":7100005566,\"cupon\":4600060003094,\"descripcionOferta\":\"Oferta 300408\",\"montoDesc\":25.0,\"puntosCanje\":\"0\"}]}}";
        
        /*return " {\"Mensaje\":\"OK\","
                + "\"codigoError\":0,"
                + "\"cupon\":"
                + "{\"estado\":0,"
                + "\"codigo\":\"4600060003094\","
                + "\"estadoDesc\":\"Aplicado\","
                + "\"linea1\":\"Aplicado\","
                + "\"linea2\":\" \","
                + "\"articulos\":["
                    + "{\"cantidad\":2.0,"
                + "\"articuloId\":6070005204,"
                + "\"cupon\":4600060003094,"
                + "\"descripcionOferta\":\"Descuento en cocas\","
                + "\"montoDesc\":5.0,"
                + "\"puntosCanje\":\"0\"}"
                + "]}}";*/
        
        return respAddCupon.get(indice++);
    }

    @Override
    public Object hayComunicacion(Object solicitud) throws Exception {
        return true;
    }

    @Override
    public Object obtenerMaximoNumeroIntentosXApagar(Object solicitud) throws Exception {
        return 10;
    }

    @Override
    public Object cancelaVentaCupones(Object solicitud) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
