package neto.sion.tienda.venta.promociones.cupones.vista;


import java.awt.Transparency;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import neto.sion.tienda.venta.promociones.cupones.modelo.ArticuloCupon;
import neto.sion.tienda.venta.promociones.cupones.modelo.CuponBean;



/**
 *
 * @author dramirezr
 */
public class CuponUI {
    
    public static int altoCupon = 35, anchoCUpon = 200;
    static{
        //styleBackground = SION.obtenerParametro(Modulo.VENTA, "venta.cupones.estilos.background");
    }
    
    interface ICupon {
        Object getVista();
        Object eliminaCupon();
    }
    
    public static class Cupon implements ICupon{
        CuponBean modelo;
        ArticuloCupon articulo;
        Label idCupon;

        public Cupon(CuponBean _cupon, ArticuloCupon _articulo) {
            this.modelo = _cupon;
            this.articulo = _articulo;
        }
 
        @Override
        public Node getVista() {
            int espLateral = 10;
            HBox hbPrincipal = new HBox();
            hbPrincipal.visibleProperty().bind(modelo.activo);
            hbPrincipal.setPrefSize((anchoCUpon+espLateral*2), altoCupon);
            hbPrincipal.setMaxSize((anchoCUpon+espLateral*2), altoCupon);
            HBox.setHgrow(hbPrincipal, Priority.NEVER);
            VBox.setVgrow(hbPrincipal, Priority.NEVER);
            hbPrincipal.setAlignment(Pos.CENTER);
            HBox.setHgrow(hbPrincipal, Priority.ALWAYS);
            hbPrincipal.setPadding(new Insets(1,espLateral,1,espLateral));
            //hbPrincipal.setStyle("-fx-background-color: #cccccc;");
                        
                Pane panePrincipal = new Pane();
                panePrincipal.setPrefSize(anchoCUpon, altoCupon);
                panePrincipal.setStyle("-fx-background-color: #ffffff;");
                hbPrincipal.getChildren().add(panePrincipal);
                
                    Rectangle r = new Rectangle(anchoCUpon, altoCupon);
                    r.setArcHeight(5);
                    r.setArcWidth(5);
                    r.setFill(Color.web("#bcb8f5"));
                    r.setStyle("-fx-border-radius: 50px;");
                    r.strokeProperty().setValue(Color.TRANSPARENT);
                    panePrincipal.getChildren().add(r);
                            
                    HBox hb = new HBox(5);
                    HBox.setHgrow(hb, Priority.ALWAYS);
                    hb.setPrefSize(anchoCUpon, altoCupon);
                    hb.setAlignment(Pos.CENTER_RIGHT); //hb.setStyle("-fx-background-color: #ccc;");
                    hbPrincipal.getChildren().add(hb);
                        idCupon = new Label();
                        idCupon.textProperty().bind( Bindings.when(modelo.automatico).then( "AUTOMATICO" ).otherwise( modelo.idCupon ) );
                        idCupon.setFont(new Font(null, 20));
                        idCupon.setTextAlignment(TextAlignment.CENTER);
                        idCupon.setMinSize(anchoCUpon - altoCupon - 10, altoCupon);
                        hb.getChildren().addAll(idCupon);
                        
                        /*Button btnEliminar = new Button("*");
                        btnEliminar.setStyle("-fx-background-color: transparent;");
                        //btnEliminar.setStyle("-fx-background-color: yellow;");
                        btnEliminar.setMinSize(altoCupon, altoCupon);
                        btnEliminar.setPrefSize(altoCupon, altoCupon);
                        btnEliminar.setMaxSize(altoCupon, altoCupon);
                        this.modelo.automatico.addListener(new ChangeListener<Boolean>(){
                            public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
                                System.out.println("nuevo valor: " + arg2);
                            }
                        });
                        btnEliminar.visibleProperty().bind(Bindings.not(this.modelo.automatico));
                        btnEliminar.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent arg0) {
                                articulo.cupon.get().setActivo(false);
                                articulo.descuento.set(0.0);
                                articulo.tipoDescuento.set(ArticuloCupon.ID_SIN_DESCUENTO);
                            }
                        });
                        
                        hb.getChildren().addAll(btnEliminar);
                        
                            Pane pane = new Pane();
                            pane.setPrefSize(altoCupon, altoCupon);
                            pane.setMaxSize(altoCupon, altoCupon);
                            btnEliminar.setGraphic(pane);
                            
                                Circle c = new Circle(altoCupon/2.3);
                                c.setFill(Color.WHITE);                                
                                c.setTranslateX((int)altoCupon*0.17);
                                c.setTranslateY((int)altoCupon/2.20);
                                c.setStroke(Color.TRANSPARENT);
                                c.setFill(Color.web("#c30000"));
                                pane.getChildren().add(c);
                                
                                Label t = new Label("X");
                                t.setStyle(" -fx-text-fill: white;");
                                t.setFont(new Font(altoCupon/2));
                                t.setLayoutY((int)altoCupon*0.13);
                                pane.getChildren().add(t);*/
            
            return hbPrincipal;
        }
        
        @Override
        public Object eliminaCupon(){
            return true;
        }

    }
    
    public static Cupon crearCupon(CuponBean cupon, ArticuloCupon articulo){
        Cupon c = new Cupon(cupon, articulo);
        return c;
    }
}
