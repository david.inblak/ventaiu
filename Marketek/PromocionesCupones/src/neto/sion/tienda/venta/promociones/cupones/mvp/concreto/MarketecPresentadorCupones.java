/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mvp.concreto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import javafx.application.Platform;
//import javafx.application.Platform;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.venta.cupones.util.ConversionBeanes;
import neto.sion.tienda.venta.cupones.util.Excepciones;
import neto.sion.tienda.venta.promociones.cupones.excepciones.ContinuaSinCuponesException;
import neto.sion.tienda.venta.promociones.cupones.modelo.ArticuloCupon;
import neto.sion.tienda.venta.promociones.cupones.modelo.CuponBean;
//import neto.sion.tienda.venta.promociones.cupones.modelo.IdentidadCaja;
import neto.sion.tienda.venta.promociones.cupones.modelo.addcupon.*;
import neto.sion.tienda.venta.promociones.cupones.modelo.DatosCuponesVentaDto;
import neto.sion.tienda.venta.promociones.cupones.modelo.cancela.CancelaBeanRequest;
import neto.sion.tienda.venta.promociones.cupones.modelo.preview.*;
import neto.sion.tienda.venta.promociones.cupones.mvp.IComandoCupon;
import neto.sion.tienda.venta.promociones.cupones.mvp.IPresentadorCupones;
import neto.sion.tienda.venta.promociones.cupones.mvp.IRepositorioCupones;
import neto.sion.tienda.venta.promociones.cupones.mvp.IVistaCupones;

/**
 *
 * @author dramirezr
 */
public class MarketecPresentadorCupones implements IPresentadorCupones {
    IVistaCupones vista;
    IRepositorioCupones repo;
    
    IComandoCupon comandoCerrar;
    IComandoCupon comandoAplicar;
    IComandoCupon comandoContinuar;    
    IComandoCupon comandoControlError;
    
    private DatosCuponesVentaDto ide;
    //private DatosCuponesVentaDto datos;
    
    static final int MISMA_FECHA = 0;
    
    static Date fechaInicio;
    static int maxIntentosDeVentaConCupones;
    static int currIntentosDeVentaConCupones;
        
    ExecutorService threadPool = Executors.newFixedThreadPool(1);

    public MarketecPresentadorCupones(IVistaCupones vista, IRepositorioCupones repo) {
        this.vista = vista;
        this.repo = repo;
        
        fechaInicio = new Date( sdf.format(new Date()) );
    }
    

    @Override
    public void init(Object solicitud) throws Exception{
                                          
        //maxIntentosDeVentaConCupones = (Integer)repo.obtenerMaximoNumeroIntentosXApagar(null);
        
        if( debeIntentarDescuentoCupones()){
            ide = ((DatosCuponesVentaDto)solicitud).clonar();
            ide.setEsVentaInicializadaMktc(true);
            
            this.eliminarDescuentosArticulos();

            vista.mostrarArticulosDeVenta(ide.getArticulosCupon());
            calculaDescuentosAutomaticos(solicitud);
        }
    }
        
    @Override
    public Object mostrarArticulosDeVenta(Object solicitud)  throws Exception{
        return this.vista.mostrarArticulosDeVenta(solicitud);
    }

    @Override
    public boolean debeIntentarDescuentoCupones() throws Exception {

        //maxIntentosDeVentaConCupones = (Integer)repo.obtenerMaximoNumeroIntentosXApagar(null);
        /*if( currIntentosDeVentaConCupones > maxIntentosDeVentaConCupones ){
            SION.log(Modulo.VENTA, "Se llegó al límite de intentos para venta con cupones en el periódo de tiempo.", Level.WARNING);                
            throw new ContinuaSinCuponesException("Se llegó al límite de intentos para venta con cupones en el periódo de tiempo.");                
        } */
        if( !(Boolean) repo.debeIntentarDescuentoCupones(null) ){
            SION.log(Modulo.VENTA, "Parametro de cupones desactivado.", Level.WARNING);
            throw new ContinuaSinCuponesException("Parametro de funcionamiento desactivado.");                

        }  
        if(!(Boolean) repo.hayComunicacion(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.SERVICE"))){
            SION.log(Modulo.VENTA, "Parametro de cupones desactivado.", Level.WARNING);
            throw new ContinuaSinCuponesException("Parametro de funcionamiento desactivado.");           
        }         
        
        return true;
    }

    @Override
    public Object calculaDescuentosAutomaticos(final Object solicitud) throws Exception{         
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    vista.mostrarCargando("Conectando con el sistema de cupones...");
                    Object respDescuentosAutomaticos = repo.calculaDescuentosAutomaticos(generaSolicitudPreview());
                    vista.actualizarDescuentosArticulosDeVenta(generaRespuestaPreview(respDescuentosAutomaticos));
                    vista.calculaPrecioTotalConDescuentos();
                } catch (Exception ex) {
                    SION.logearExcepcion(Modulo.VENTA, ex, "calculaDescuentosAutomaticos :: threadPool >> "+ Excepciones.getStackTrace(ex));
                    terminarCuponesXError("Se produjo un error durante la llamada a Begin cupones.", false);//Cualquier error lanza el comando controlar cupones
                }finally{
                    vista.ocultarCargando(null);
                }
            }

        });
        
        return null;
    }
    
    @Override
    public void eliminarDescuentosArticulos() throws Exception {
        for( CuponBeanResp cupon : ide.getCuponesValidados() ){
            cupon.setEstado(CUPON_INACTIVO);
        }
         vista.eliminarDescuentosArticulos(null);
    }

    private long CUPON_ACTIVO = 0, CUPON_INACTIVO =-1;
    int pedido=0;
    private String token = null;
    @Override
    public Object calculaDescuentosXCupon(final Object solicitud) throws Exception {
        if( "".equals(solicitud.toString()) )
            return null;
        
         vista.mostrarCargando(null);

        //threadPool.execute(new Runnable() {
         Platform.runLater(new Runnable() {
            @Override
            public void run() {
                 try {       
                   

                    if( !validaExistenciaCupon( solicitud.toString() ) )
                    {                        
                                
                        AddCuponBeanReq sol = new AddCuponBeanReq(
                            pedido,
                            (int)ide.getFinTicketMktc().getPos(), 
                            (int)ide.getFinTicketMktc().getSucursal(), 
                            ide.getFinTicketMktc().getTicket(),
                            ide.getFinTicketMktc().getTicketCompleto(),
                            token,
                            Long.parseLong(solicitud.toString()), 
                            String.valueOf(ide.getRegistroMktcId()));

                        Object respCuponDescuento = repo.calculaDescuentosCupon(sol);
                        Object respAdd = generaRespuestaAddCupon(respCuponDescuento);

                        vista.actualizarDescuentosArticulosDeVenta(respAdd);
                        vista.calculaPrecioTotalConDescuentos();
                   }
                    //Verificar existencia en vista
                    
                } catch (Exception ex) {
                    SION.logearExcepcion(Modulo.VENTA, ex, "calculaDescuentosXCupon :: "+Excepciones.getStackTrace(ex));
                    terminarCuponesXError("Se produjo un error durante la llamada a AddCupon.", false);
                }finally{
                    vista.ocultarCargando(null);
                }
            }
        });
       
        return null;
    }
    
    
    @Override
    public void eliminaDescuentoXCupon(Object solicitud) throws Exception {
        vista.eliminarCuponDescuento(solicitud);
         for (Iterator it = ide.getCuponesValidados().iterator(); it.hasNext(); ) {
            CuponBeanResp cu = (CuponBeanResp) it.next();
            if( cu.getCodigo().equals(solicitud) ){
                cu.setEstado(CUPON_INACTIVO);
                vista.eliminarCuponDescuento(solicitud);
            } 
        }
    }
    
    
    @Override
    public Object cancelaVenta(Object solicitud) {
        DatosCuponesVentaDto req = (DatosCuponesVentaDto)solicitud;
        
        if( req.getRegistroMktcId() > 0 ){
            try{
                CancelaBeanRequest cancelaReq = new CancelaBeanRequest();
                cancelaReq.setRegistroMktcId( req.getRegistroMktcId() );
                cancelaReq.setPos( req.getFinTicketMktc().getPos() );
                cancelaReq.setSucursal( req.getFinTicketMktc().getSucursal());
                cancelaReq.setTicket( Long.parseLong( req.getFinTicketMktc().getTicket() ));
                cancelaReq.setTicketCompleto( req.getFinTicketMktc().getTicketCompleto());
                SION.log(Modulo.VENTA, "Se envía cancenlación a marketec de venta en progreso: " + req.getRegistroMktcId(), Level.INFO);
                repo.cancelaVentaCupones(cancelaReq);
            }catch(Exception e){
                SION.logearExcepcion(Modulo.VENTA, e, "Error al cancelar la venta Marketec.");
            }
        }
        
        return null;
    }
    


    @Override
    public void continuar(Object solicitud) {
        ide.setArticulosCupon((List<ArticuloCupon>)solicitud);
                
        comandoAplicar.ejecutar( ide );
        comandoContinuar.ejecutar();
    }

    
    @Override
    public void calculaPrecioTotalConDescuentos() {
        vista.calculaPrecioTotalConDescuentos();
    }
    
    public void terminarCuponesXError(String solicitud, boolean xCodigoError){
        SION.log(Modulo.VENTA, "terminarCuponesXError ::: "+solicitud, Level.INFO);
        //incrementaIntentoFallidoVentaCupones(xCodigoError);
        
        ide.getFinTicketMktc().setTicketAnterior(ide.getFinTicketMktc().getTicket());
        ide.setMotivoSinCupones(solicitud);
        ide.setEsVentaSinMarketec(true);
        
        vista.eliminarDescuentosArticulos(null);
        comandoCerrar.ejecutar();
        comandoControlError.ejecutar(ide);
    }
    
    public DatosCuponesVentaDto getIdentidad(){
        return ide;
    }
    
    static final SimpleDateFormat sdfNumFecha = new SimpleDateFormat("yyyyMMdd");
    static final SimpleDateFormat sdfNumHora = new SimpleDateFormat("HHmmss");
    static final SimpleDateFormat sdfNum = new SimpleDateFormat("yyyyMMddHHmmss");
    static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    
    private Object generaSolicitudPreview(){
        Date ahora = new Date();
         
        ValidaVentaDescReq req = new ValidaVentaDescReq();
        
        req.setCajero(ide.getFinTicketMktc().getCajeroId());
        req.setCantidadArticulos(ide.getArticulosCupon().size());
        req.setFecha(sdfNumFecha.format(ahora));        
        req.setHora(sdfNumHora.format(ahora));
                
        req.setTicketAnterior(ide.getFinTicketMktc().getTicket());
        Double sumaCostoTotal = 0.0;
        for ( ArticuloCupon art : ide.getArticulosCupon())
            sumaCostoTotal += art.getImporte();
        
        req.setMontototal( sumaCostoTotal  );
        long idTicketConsecutivo = generaConsecutivoMasIntento(ide);
        long idConsecutivoVenta = generaConsecutivo(ide);
        
        ide.getFinTicketMktc().setTicket(idTicketConsecutivo+"");
        ide.getFinTicketMktc().setTicketOriginal(idConsecutivoVenta+"");
        
        req.setTicket(ide.getFinTicketMktc().getTicket());
        req.setTicketOriginal(ide.getFinTicketMktc().getTicketOriginal());
        req.setTicketCompleto(ide.getFinTicketMktc().getTicketCompleto());
        
        List<PluBeginVentaBean> plus = new ArrayList<PluBeginVentaBean>();
        for (ArticuloCupon articuloCupon : ide.getArticulosCupon()) {
            PluBeginVentaBean pluBeginVenta = new PluBeginVentaBean();
            
            pluBeginVenta.setCantidad( articuloCupon.getCantidad() );
            //pluBeginVenta.setCodigo( articuloCupon.getCodigoBarras());
            pluBeginVenta.setCodigo( String.valueOf(articuloCupon.getId()) );
            pluBeginVenta.setEstructura( String.valueOf(articuloCupon.getIdFamilia()) );
            pluBeginVenta.setMonto( articuloCupon.getPrecio() );
            pluBeginVenta.setMontoTotal( articuloCupon.getImporte() );
            
            plus.add(pluBeginVenta);
        }
        
        req.setPlu(plus);
        req.setPos(ide.getFinTicketMktc().getPos());
        req.setSucursal(ide.getFinTicketMktc().getSucursal());
        
        return req;
    }
    
    private final static long PROCESADO_EXITOSO = 0;
    private final static long ERROR_MARKETEC = 100;
    private final static String CUPON_ESTATUS_APLICADO = "Aplicado";
    private Object generaRespuestaPreview(Object respDescuentosAutomaticos) throws Exception {
        List<ArticuloCupon> desctos = new ArrayList<ArticuloCupon>();
        
        ValidaVentaDescResp resp = (ValidaVentaDescResp)ConversionBeanes.convertir(respDescuentosAutomaticos, ValidaVentaDescResp.class);
        
        ide.setRegistroMktcId( resp.getRegistroMktcId() );
        
        if( PROCESADO_EXITOSO == resp.getCodigoError() ){
           List<CuponCuponeraBeanResp> cupones = resp.getCuponesCuponera();
           if( cupones != null ){
                for(CuponCuponeraBeanResp item : resp.getCuponesCuponera()){
                    String cuponId = item.getCodigoCupon();
                    List<ArticuloCuponeraBeanResp> articulos = item.getArticulosCuponera();
                    if( articulos != null ){
                        for (ArticuloCuponeraBeanResp articuloCuponeraBeanResp : articulos) {
                            CuponBean cupon = new CuponBean(cuponId, true, articuloCuponeraBeanResp.getDescripcionOferta(), true, null);
                            
                            desctos.add(new ArticuloCupon( 0,
                                    articuloCuponeraBeanResp.getCantidad(), 
                                    articuloCuponeraBeanResp.getMonto(), 
                                    cupon, 
                                    articuloCuponeraBeanResp.getCodigoBarras(), 
                                    ArticuloCupon.ID_TIPODESCUENTO_CUPONES,
                                    articuloCuponeraBeanResp.getDescripcionOferta()));
                        }
                    }
                }
           }
        }else {
            terminarCuponesXError("Respuesta de central :: "+resp.getMensaje(), true);
        }
        
        return desctos;
    }

    
    public long generaConsecutivoMasIntento(DatosCuponesVentaDto identidad){                
        long dato = Long.parseLong(
                identidad.getFinTicketMktc().getTicketCompleto().replace(sdfNumFecha.format(new Date()), "")+
                identidad.getIntentos() );
        return dato;
    }
    
    public long generaConsecutivo(DatosCuponesVentaDto identidad){                
        long dato = Long.parseLong(
                identidad.getFinTicketMktc().getTicketCompleto().replace(sdfNumFecha.format(new Date()), "") );
        return dato;
    }
    
    public String generaConsecutivoIntentoAnterior(DatosCuponesVentaDto identidad){
        if( identidad.getIntentos() == 1){
            return  identidad.getFinTicketMktc().getTicketCompleto().replace(sdfNumFecha.format(new Date()), "")+
                    (identidad.getIntentos() - 1)+"";
        }
        
        return null;
    }
    
    /*private void incrementaIntentoFallidoVentaCupones(){ incrementaIntentoFallidoVentaCupones(false); }
    private void incrementaIntentoFallidoVentaCupones( boolean desactivaHastaReiniciar ){
        if( desactivaHastaReiniciar )
            currIntentosDeVentaConCupones =maxIntentosDeVentaConCupones;
        else{
            Date currDate = new Date( sdf.format(new Date()) );
            if( fechaInicio.compareTo(currDate) == MISMA_FECHA){
                currIntentosDeVentaConCupones++;
            }else{
                fechaInicio = currDate;
            }
        }
    }*/

    private boolean validaExistenciaCupon(String solicitud){
           
        boolean yaExiste = false;
        
            for (Iterator it = ide.getCuponesValidados().iterator(); it.hasNext();) {
                final CuponBeanResp cup = (CuponBeanResp) it.next();
                yaExiste = cup.getCodigo().equals(solicitud);
                if( yaExiste ){
                    if( cup.getEstado() == CUPON_ACTIVO ){
                        vista.mostrarMensaje("El cupón ya fue agregado previamente.");
                        return yaExiste;
                    }else{//Existe inactivo se remueve anterior por si el imorte descuento es diferente
                        cup.setCodigo(cup.getCodigo());
                        it.remove();
                        //ide.getCuponesValidados().remove(cup);
                        yaExiste = false;
                    }                              
                }
            }
            return yaExiste;
    }
    
    private Object generaRespuestaAddCupon(Object respCuponDescuento) throws Exception {                
        List<ArticuloCupon> desctos = new ArrayList<ArticuloCupon>();
        
        AddCuponBeanResp resp = (AddCuponBeanResp)ConversionBeanes.convertir(respCuponDescuento, AddCuponBeanResp.class);
        
        if( PROCESADO_EXITOSO == resp.getCodigoError() )
        {//Control de respuesta central
            
            String cuponId = resp.getCupon().getCodigo()+"";
                       
            //Controlar el estado del cupon
            if( resp.getCupon().getEstado() == PROCESADO_EXITOSO && 
                CUPON_ESTATUS_APLICADO.equals( resp.getCupon().getEstadoDesc() ) ){
                ide.getCuponesValidados().add(resp.getCupon());
                if( resp.getCupon().getDescuentoTotal() == null ){
                    List<ArticuloConDescuentoBean> articulos = resp.getCupon().getArticulos();
                    if( articulos != null )
                    {
                        for (ArticuloConDescuentoBean articuloCuponeraBeanResp : articulos)
                        {
                            CuponBean cupon = new CuponBean(cuponId, true, articuloCuponeraBeanResp.getDescripcionOferta(), false, resp.getCupon());
                            
                            desctos.add(new ArticuloCupon(
                                    Long.parseLong(articuloCuponeraBeanResp.getArticuloId()+""),
                                    1.0,//articuloCuponeraBeanResp.getCantidad(), 
                                    articuloCuponeraBeanResp.getMontoDesc(), 
                                    cupon, 
                                    articuloCuponeraBeanResp.getCodigoBarras(), 
                                    ArticuloCupon.ID_TIPODESCUENTO_CUPONES,
                                    articuloCuponeraBeanResp.getDescripcionOferta()));
                        }
                    }
                    
                }else{
                    DescuentoTotalBean descTotalVenta = resp.getCupon().getDescuentoTotal();
                    CuponBean cupon = new CuponBean(descTotalVenta.getCodigo(), true, descTotalVenta.getDescripcionOferta(), false, resp.getCupon());
                    
                    desctos.add(
                        new ArticuloCupon(
                            /* Long id, Double cantidad, String nombre, Double precio, */
                            ArticuloCupon.ID_ARTICULO_DESCTO_TOTAL, 1.0,  descTotalVenta.getDescripcionOferta(), 0.01,
                            /* Double costo, Double descuento, Double iva, Double agranel, */
                            0.01, Double.parseDouble("0"+descTotalVenta.getMonto()) + 0.01, 0.0, 0.0,
                            /* Integer idFamilia, Integer idIEPS, Integer unidad, Integer normaempaque, */
                            0, ArticuloCupon.ID_SIN_IEPS, 1, 1,
                            /* CuponBean cupon, String codBarras, String codBarraspadre, Integer tipoDescuento*/
                            cupon, ArticuloCupon.ID_ARTICULO_DESCTO_TOTAL+"", "0", ArticuloCupon.ID_TIPODESCUENTO_CUPONES,
                            0.0)
                            );
                    
                }   
                
            }else{
                //vista.mostrarMensaje(resp.getCupon().getLinea1() + resp.getCupon().getLinea2() + " (Estado cupon)");
                vista.mostrarMensaje(//"El cupón no tiene un estado válido.\n"+
                       ""+ resp.getCupon().getEstadoDesc()+"\n"+
                       ""+ resp.getCupon().getLinea1() +"\n"+
                       ""+ resp.getCupon().getLinea2());
            }
         }else if( ERROR_MARKETEC == resp.getCodigoError() ){
             terminarCuponesXError(resp.getMensaje() + " (central)", true);
        }else{
             terminarCuponesXError("Respuesta de central :: "+resp.getMensaje() + " (central)", false);
        }
        return desctos;
    }
    
    

    @Override
    public void setComandoAplicarDescuentos(IComandoCupon iComando) {
        comandoAplicar = iComando;
    }

    @Override
    public void setComandoCerrarCupones(IComandoCupon iComando) {
        comandoCerrar = iComando;
    }

    @Override
    public void setComandoContinuar(IComandoCupon iComando) {
        comandoContinuar = iComando;
    }

    @Override
    public void cerrarVentana() {
        if( comandoCerrar != null)
            comandoCerrar.ejecutar();
    }

    @Override
    public void setComandoOnError(IComandoCupon iComando) {
        comandoControlError = iComando;
    }


}
