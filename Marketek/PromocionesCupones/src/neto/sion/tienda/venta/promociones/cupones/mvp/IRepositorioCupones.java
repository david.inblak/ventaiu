/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mvp;

/**
 *
 * @author dramirezr
 */
public interface IRepositorioCupones {
    Object debeIntentarDescuentoCupones(Object Solicitud) throws Exception;
    Object calculaDescuentosAutomaticos(Object solicitud) throws Exception;
    Object calculaDescuentosCupon(Object solicitud ) throws Exception;
    Object cancelaVentaCupones(Object solicitud ) throws Exception;
    Object hayComunicacion(Object solicitud ) throws Exception;

    Object obtenerMaximoNumeroIntentosXApagar(Object solicitud) throws Exception;
}
