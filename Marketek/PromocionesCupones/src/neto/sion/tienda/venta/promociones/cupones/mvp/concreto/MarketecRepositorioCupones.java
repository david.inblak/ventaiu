/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.tienda.venta.promociones.cupones.mvp.concreto;


import java.sql.CallableStatement;
import java.sql.Connection;
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.sql.Conexion;
import neto.sion.tienda.genericos.utilidades.Modulo;
import neto.sion.tienda.genericos.utilidades.Utilerias;
import neto.sion.tienda.venta.cupones.facade.ClienteMarketec;
import neto.sion.tienda.venta.promociones.cupones.mvp.IRepositorioCupones;
import oracle.jdbc.internal.OracleTypes;

/**
 *
 * @author dramirezr
 */
public class MarketecRepositorioCupones implements IRepositorioCupones{
    private static final int CODIGO_EXITO = 1, NUMERO_INTENTOS = 1;
    private ClienteMarketec cliente;
        
    public MarketecRepositorioCupones() {
        cliente = new ClienteMarketec();
    }
    
    @Override
    public Object calculaDescuentosAutomaticos(Object solicitud) throws Exception {
        return cliente.validaDesceuntosAutomaticos(solicitud);
    }

    @Override
    public Object calculaDescuentosCupon(Object solicitud) throws Exception {
         return cliente.calculaDescuentoCupones(solicitud);
    }
    
    @Override
    public Object cancelaVentaCupones(Object solicitud) throws Exception {
        return cliente.cancelarVenta(solicitud);
    }

    @Override
    public Boolean debeIntentarDescuentoCupones(Object Solicitud) throws Exception{
        
        CallableStatement cs=null;
        Connection conn = null;        
        String sql = null;
        try{
            conn = Conexion.obtenerConexion();
            
            sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS");
                   
            cs = conn.prepareCall(sql);
            cs.setInt(1, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.PAIS")));
            cs.setInt(2, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.SISTEMA")));
            cs.setInt(3, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.MODULO")));
            cs.setInt(4, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.SUBMODULO")));
            cs.setInt(5, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.ACTIVOS.CONFIGURACIONID")));
            cs.registerOutParameter(6, OracleTypes.NUMBER);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.NUMBER);
            cs.execute();
            
            return cs.getDouble(8) == CODIGO_EXITO;
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, sql);
            throw e;
        } finally {
            Conexion.cerrarRecursos(conn,cs, null);
        }
    }
    

    @Override
    public Object hayComunicacion(Object solicitud) throws Exception {
        return Utilerias.evaluarConexionCentral(solicitud.toString(), NUMERO_INTENTOS);
    }

    @Override
    public Integer obtenerMaximoNumeroIntentosXApagar(Object solicitud) throws Exception {
        CallableStatement cs=null;
        Connection conn = null;        
        String sql = null;
        try{
            conn = Conexion.obtenerConexion();
            
            sql = SION.obtenerParametro(Modulo.VENTA, "USRVELIT.PAGENERICOS.SPCONSULTAPARAMETROS");
                   
            cs = conn.prepareCall(sql);
            cs.setInt(1, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.MAXINTENTOS.PAIS")));
            cs.setInt(2, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.MAXINTENTOS.SISTEMA")));
            cs.setInt(3, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.MAXINTENTOS.MODULO")));
            cs.setInt(4, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.MAXINTENTOS.SUBMODULO")));
            cs.setInt(5, Integer.parseInt(SION.obtenerParametro(Modulo.VENTA, "VENTA.MARKETEC.CUPONES.MAXINTENTOS.CONFIGURACIONID")));
            cs.registerOutParameter(6, OracleTypes.NUMBER);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.NUMBER);
            cs.execute();
            
            return cs.getInt(8);
        } catch (Exception e) {
            SION.logearExcepcion(Modulo.VENTA, e, sql);
            throw e;
        } finally {
            Conexion.cerrarRecursos(conn,cs, null);
        }
    }

    
    
}
