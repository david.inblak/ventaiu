/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.clientews.tarjeta.rest.dto;

/**
 *
 * @author dramirezr
 */
public class PeticionReversoTarjetaChipDto {
    private long uId;
    private String ipTerminal;
    private long tiendaId;
    
    private int paPaisId;
    private long paConciliacionId;
    private String afiliacion;
    
    private boolean chip;
    private double comision;
    private boolean falloLecturaChip;
    private String fechaLocal;
    private String horaLocal;
    private double monto;
    private String numTerminal;
    private long pagoTarjetaId;
    private String trace;
    private String track2;
    private String canalPago;

    public String getAfiliacion() {
        return afiliacion;
    }

    public void setAfiliacion(String afiliacion) {
        this.afiliacion = afiliacion;
    }

    public String getCanalPago() {
        return canalPago;
    }

    public void setCanalPago(String canalPago) {
        this.canalPago = canalPago;
    }

    public boolean isChip() {
        return chip;
    }

    public void setChip(boolean chip) {
        this.chip = chip;
    }

    public double getComision() {
        return comision;
    }

    public void setComision(double comision) {
        this.comision = comision;
    }

    public boolean isFalloLecturaChip() {
        return falloLecturaChip;
    }

    public void setFalloLecturaChip(boolean falloLecturaChip) {
        this.falloLecturaChip = falloLecturaChip;
    }

    public String getFechaLocal() {
        return fechaLocal;
    }

    public void setFechaLocal(String fechaLocal) {
        this.fechaLocal = fechaLocal;
    }

    public String getHoraLocal() {
        return horaLocal;
    }

    public void setHoraLocal(String horaLocal) {
        this.horaLocal = horaLocal;
    }

    public String getIpTerminal() {
        return ipTerminal;
    }

    public void setIpTerminal(String ipTerminal) {
        this.ipTerminal = ipTerminal;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getNumTerminal() {
        return numTerminal;
    }

    public void setNumTerminal(String numTerminal) {
        this.numTerminal = numTerminal;
    }

    public long getPaConciliacionId() {
        return paConciliacionId;
    }

    public void setPaConciliacionId(long paConciliacionId) {
        this.paConciliacionId = paConciliacionId;
    }

    public int getPaPaisId() {
        return paPaisId;
    }

    public void setPaPaisId(int paPaisId) {
        this.paPaisId = paPaisId;
    }

    public long getPagoTarjetaId() {
        return pagoTarjetaId;
    }

    public void setPagoTarjetaId(long pagoTarjetaId) {
        this.pagoTarjetaId = pagoTarjetaId;
    }

    public long getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(long tiendaId) {
        this.tiendaId = tiendaId;
    }

    public String getTrace() {
        return trace;
    }

    public void setTrace(String trace) {
        this.trace = trace;
    }

    public String getTrack2() {
        return track2;
    }

    public void setTrack2(String track2) {
        this.track2 = track2;
    }

    public long getuId() {
        return uId;
    }

    public void setuId(long uId) {
        this.uId = uId;
    }

    @Override
    public String toString() {
        return "PeticionReversoTarjetaChipDto{" + "uId=" + uId + ", ipTerminal=" + ipTerminal + ", tiendaId=" + tiendaId + ", paPaisId=" + paPaisId + ", paConciliacionId=" + paConciliacionId + ", afiliacion=" + afiliacion + ", chip=" + chip + ", comision=" + comision + ", falloLecturaChip=" + falloLecturaChip + ", fechaLocal=" + fechaLocal + ", horaLocal=" + horaLocal + ", monto=" + monto + ", numTerminal=" + numTerminal + ", pagoTarjetaId=" + pagoTarjetaId + ", trace=" + trace + ", track2=" + track2 + ", canalPago=" + canalPago + '}';
    }


    
}
