/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.clientews.tarjeta.rest.dto;

/**
 *
 * @author dramirezr
 */
public class RespuestaPagoTarjetaChipDto {
    private int codError;
    private String descError;
    private String afiliacion;
    private String banco;
    private String bitMap;
    private String c55;
    private String c63;
    private String codRespuesta;
    private String codProceso;
    private String fechaHora;
    private String fechaLocal;
    private String horaLocal;
    private String mensaje;
    private String monto;
    private String noAutorizacion;
    private String numAplicacionPAN;
    private String numTerminal;
    private long pagoTarjetaId;
    private String referencia;
    private String tipoMsg;
    private String tipoTarjeta;
    private String trace;

    public RespuestaPagoTarjetaChipDto() {
            super();
    }

    public int getCodError() {
        return codError;
    }

    public void setCodError(int codError) {
        this.codError = codError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }      

    public String getAfiliacion() {
            return afiliacion;
    }

    public void setAfiliacion(String afiliacion) {
            this.afiliacion = afiliacion;
    }

    public String getBanco() {
            return banco;
    }

    public void setBanco(String banco) {
            this.banco = banco;
    }

    public String getBitMap() {
            return bitMap;
    }

    public void setBitMap(String bitMap) {
            this.bitMap = bitMap;
    }

    public String getC55() {
            return c55;
    }

    public void setC55(String c55) {
            this.c55 = c55;
    }

    public String getC63() {
            return c63;
    }

    public void setC63(String c63) {
            this.c63 = c63;
    }

    public String getCodRespuesta() {
            return codRespuesta;
    }

    public void setCodRespuesta(String codRespuesta) {
            this.codRespuesta = codRespuesta;
    }

    public String getCodProceso() {
            return codProceso;
    }

    public void setCodProceso(String codProceso) {
            this.codProceso = codProceso;
    }

    public String getFechaHora() {
            return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
            this.fechaHora = fechaHora;
    }

    public String getFechaLocal() {
            return fechaLocal;
    }

    public void setFechaLocal(String fechaLocal) {
            this.fechaLocal = fechaLocal;
    }

    public String getHoraLocal() {
            return horaLocal;
    }

    public void setHoraLocal(String horaLocal) {
            this.horaLocal = horaLocal;
    }

    public String getMensaje() {
            return mensaje;
    }

    public void setMensaje(String mensaje) {
            this.mensaje = mensaje;
    }

    public String getMonto() {
            return monto;
    }

    public void setMonto(String monto) {
            this.monto = monto;
    }

    public String getNoAutorizacion() {
            return noAutorizacion;
    }

    public void setNoAutorizacion(String noAutorizacion) {
            this.noAutorizacion = noAutorizacion;
    }

    public String getNumAplicacionPAN() {
            return numAplicacionPAN;
    }

    public void setNumAplicacionPAN(String numAplicacionPAN) {
            this.numAplicacionPAN = numAplicacionPAN;
    }

    public String getNumTerminal() {
            return numTerminal;
    }

    public void setNumTerminal(String numTerminal) {
            this.numTerminal = numTerminal;
    }

    public long getPagoTarjetaId() {
            return pagoTarjetaId;
    }

    public void setPagoTarjetaId(long pagoTarjetaId) {
            this.pagoTarjetaId = pagoTarjetaId;
    }

    public String getReferencia() {
            return referencia;
    }

    public void setReferencia(String referencia) {
            this.referencia = referencia;
    }

    public String getTipoMsg() {
            return tipoMsg;
    }

    public void setTipoMsg(String tipoMsg) {
            this.tipoMsg = tipoMsg;
    }

    public String getTipoTarjeta() {
            return tipoTarjeta;
    }

    public void setTipoTarjeta(String tipoTarjeta) {
            this.tipoTarjeta = tipoTarjeta;
    }

    public String getTrace() {
            return trace;
    }

    public void setTrace(String trace) {
            this.trace = trace;
    }

    @Override
    public String toString() {
            return "RespuestaPagoBean ["
                            + ", afiliacion=" + afiliacion 
                            + ", banco=" + banco 
                            + ", bitMap=" + bitMap 
                            + ", c55=" + c55 
                            + ", c63=" + c63 
                            + ", codRespuesta=" + codRespuesta 
                            + ", codProceso=" + codProceso 
                            + ", fechaHora=" + fechaHora 
                            + ", fechaLocal=" + fechaLocal 
                            + ", horaLocal=" + horaLocal 
                            + ", mensaje="+ mensaje 
                            + ", monto=" + monto 
                            + ", noAutorizacion=" + noAutorizacion 
                            + ", numAplicacionPAN=" + numAplicacionPAN
                            + ", numTerminal=" + numTerminal 
                            + ", pagoTarjetaId=" + pagoTarjetaId 
                            + ", referencia=" + referencia 
                            + ", tipoMsg=" + tipoMsg 
                            + ", tipoTarjeta=" + tipoTarjeta 
                            + ", trace=" + trace
                            + "]";
    }
}
