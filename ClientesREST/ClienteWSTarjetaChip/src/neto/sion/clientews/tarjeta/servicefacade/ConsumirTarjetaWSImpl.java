package neto.sion.clientews.tarjeta.servicefacade;

import java.net.SocketTimeoutException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto;
import neto.sion.clientews.tarjeta.dto.PeticionReversoTarjetaDto;
import neto.sion.clientews.tarjeta.dto.RespuestaPagoTarjetaDto;
import neto.sion.clientews.tarjeta.dto.RespuestaReversoTarjetaDto;

import neto.sion.clientews.tarjeta.rest.dto.PeticionPagoTarjetaChipDto;
import neto.sion.clientews.tarjeta.rest.dto.PeticionReversoTarjetaChipDto;
import neto.sion.clientews.tarjeta.rest.dto.RespuestaPagoTarjetaChipDto;
import neto.sion.clientews.tarjeta.rest.dto.RespuestaReversoTarjetaChipDto;
import neto.sion.gen.http.bean.RestFullControladoException;
import neto.sion.gen.http.conexion.ContextHttp;
import neto.sion.gen.http.conexion.HttpsConn;/**/
import neto.sion.tienda.genericos.configuraciones.SION;
import neto.sion.tienda.genericos.utilidades.Modulo;


public class ConsumirTarjetaWSImpl implements ConsumirTarjetaWS
{

	/*private TarjetaChip51ServiceStub serviceStub;
	private ServiceClient client;*/
        
        private HttpsConn clienteTarjetaChip;
    
        private String endPointPagoTarjetas;
        private String aplicaPagoTarjetaPath;
        private String registraReversoPath;

	public ConsumirTarjetaWSImpl() throws Exception
	{
	    //this(SION.obtenerParametro(Modulo.VENTA, "endpoint.tarjetachipservice"), Integer.valueOf(SION.obtenerParametro(Modulo.VENTA, "tarjeta.axis2.timeout").trim()));
               
            endPointPagoTarjetas  = SION.obtenerParametro(Modulo.VENTA, "endpoint.rest.venta.pagostarjeta.chip");
            aplicaPagoTarjetaPath = SION.obtenerParametro(Modulo.VENTA, "endpoint.rest.venta.pagostarjeta.chip.aplicapago");
            registraReversoPath   = SION.obtenerParametro(Modulo.VENTA, "endpoint.rest.venta.pagostarjeta.chip.registrareverso");
            
            clienteTarjetaChip = new HttpsConn(endPointPagoTarjetas, ContextHttp.getHttpsContext(), Modulo.VENTA);
            int contimeout = Integer.parseInt( SION.obtenerParametro(Modulo.VENTA, "endpoint.rest.venta.pagostarjeta.chip.conntimeout"));
            int readtimeout= Integer.parseInt( SION.obtenerParametro(Modulo.VENTA, "endpoint.rest.venta.pagostarjeta.chip.readtimeout"));
            clienteTarjetaChip.setTimeouts(contimeout, readtimeout);
	}//ConsumirTarjetaWSImpl

	public ConsumirTarjetaWSImpl(String _endPoint, long _timeOut) throws Exception
	{
             System.out.println("EndPoint: " + _endPoint);
		/*serviceStub = new TarjetaChip51ServiceStub(SION.obtenerConfigurationContext(), _endPoint);
        serviceStub._getServiceClient().getOptions().setTimeOutInMilliSeconds(_timeOut);
        client = serviceStub._getServiceClient();
        client.engageModule("rampart");*/
	}//ConsumirTarjetaWSImpl

	public RespuestaPagoTarjetaDto solicitudPagoTarjeta(PeticionPagoTarjetaDto _peticion) throws RemoteException
	{
            RespuestaPagoTarjetaDto respuesta = new RespuestaPagoTarjetaDto();
            try {
                SION.log(Modulo.VENTA, "Solicitud pago recibida: " + _peticion.toString(), Level.INFO);
                //respuesta = respuestaPagoTarjetaBean2Dto(serviceStub.solicitudPagoTarjeta(peticionPagoTarjetaDto2Bean(_peticion)));

                /**/PeticionPagoTarjetaChipDto sol = peticionPagoTarjetaDto2Rest(_peticion);
                RespuestaPagoTarjetaChipDto resp = (RespuestaPagoTarjetaChipDto) clienteTarjetaChip.enviarPost(
                        sol, RespuestaPagoTarjetaChipDto.class, aplicaPagoTarjetaPath);

                respuesta = respuestaPagoTarjetaRest2Dto(resp);

                SION.log(Modulo.VENTA, "Respuesta pago recibida: " + respuesta.toString(), Level.INFO);
                
            } //solicitudPagoTarjeta
            catch (Exception ex) {                
                Logger.getLogger(ConsumirTarjetaWSImpl.class.getName()).log(Level.SEVERE, null, ex);
                if( ex instanceof RestFullControladoException){
                    respuesta.setCodError(-2);
                    respuesta.setDescError(ex.getMessage()+"|");
                }
            }
            return respuesta;
	}//solicitudPagoTarjeta

	public RespuestaReversoTarjetaDto solicitudReversoTarjeta(PeticionReversoTarjetaDto _peticion) throws RemoteException
	{
            RespuestaReversoTarjetaDto respuesta=null;
            try {
                SION.log(Modulo.VENTA, "Solicitud reverso recibida: " + _peticion.toString(), Level.INFO);
                //respuesta = respuestaReversoTarjetaBean2Dto(serviceStub.solicitudReversoTarjeta(peticionReversoTarjetaDto2Bean(_peticion)));

                PeticionReversoTarjetaChipDto sol = peticionReversoTarjetaDto2Rest(_peticion);
                RespuestaReversoTarjetaChipDto resp = (RespuestaReversoTarjetaChipDto) clienteTarjetaChip.enviarPost(
                        sol, RespuestaReversoTarjetaChipDto.class, registraReversoPath);

                respuesta = respuestaReversoTarjetaRest2Dto(resp);

                SION.log(Modulo.VENTA, "Respuesta reverso recibida: " + respuesta.toString(), Level.INFO);
                
            } //solicitudReversoTarjeta
            catch (Exception ex) {
                Logger.getLogger(ConsumirTarjetaWSImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            return respuesta;
	}//solicitudReversoTarjeta

	/*public PeticionPagoTarjetaDto peticionPagoTarjetaBean2Dto (PeticionPagoTarjetaBean _peticion)
	{
		PeticionPagoTarjetaDto peticion = new PeticionPagoTarjetaDto();
		peticion.setC55(_peticion.getC55());
		peticion.setC63(_peticion.getC63());
		peticion.setChip(_peticion.getChip());
		peticion.setComision(_peticion.getComision());
		peticion.setFalloLecturaChip(_peticion.getFalloLecturaChip());
		peticion.setMonto(_peticion.getMonto());
		peticion.setNumTerminal(_peticion.getNumTerminal());
		peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
		peticion.setPaPaisId(_peticion.getPaPaisId());
		peticion.setTiendaId(_peticion.getTiendaId());
		peticion.setTrack1(_peticion.getTrack1());
		peticion.setTrack2(_peticion.getTrack2());
		peticion.setCanalPago(_peticion.getCanalPago());
		return peticion;
	}*/

	public PeticionPagoTarjetaChipDto peticionPagoTarjetaDto2Rest (PeticionPagoTarjetaDto _peticion)
	{
		PeticionPagoTarjetaChipDto peticion = new PeticionPagoTarjetaChipDto();
		peticion.setC55(_peticion.getC55());
		peticion.setC63(_peticion.getC63());
		peticion.setChip(_peticion.isChip());
		peticion.setComision(_peticion.getComision());
		peticion.setFalloLecturaChip(_peticion.isFalloLecturaChip());
		peticion.setMonto(_peticion.getMonto());
		peticion.setNumTerminal(_peticion.getNumTerminal());
		peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
		peticion.setPaPaisId(_peticion.getPaPaisId());
		peticion.setTiendaId(_peticion.getTiendaId());
		peticion.setTrack1(_peticion.getTrack1());
		peticion.setTrack2(_peticion.getTrack2());
		peticion.setCanalPago(_peticion.getCanalPago());
		return peticion;
	}
        /*public PeticionPagoTarjetaBean peticionPagoTarjetaDto2Bean (PeticionPagoTarjetaDto _peticion)
	{
		PeticionPagoTarjetaBean peticion = new PeticionPagoTarjetaBean();
		peticion.setC55(_peticion.getC55());
		peticion.setC63(_peticion.getC63());
		peticion.setChip(_peticion.isChip());
		peticion.setComision(_peticion.getComision());
		peticion.setFalloLecturaChip(_peticion.isFalloLecturaChip());
		peticion.setMonto(_peticion.getMonto());
		peticion.setNumTerminal(_peticion.getNumTerminal());
		peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
		peticion.setPaPaisId(_peticion.getPaPaisId());
		peticion.setTiendaId(_peticion.getTiendaId());
		peticion.setTrack1(_peticion.getTrack1());
		peticion.setTrack2(_peticion.getTrack2());
		peticion.setCanalPago(_peticion.getCanalPago());
		return peticion;
	}*/

	public RespuestaPagoTarjetaDto respuestaPagoTarjetaRest2Dto (RespuestaPagoTarjetaChipDto _respuesta)
	{
		RespuestaPagoTarjetaDto respuesta = new RespuestaPagoTarjetaDto();
		respuesta.setAfiliacion(_respuesta.getAfiliacion());
		respuesta.setBanco(_respuesta.getBanco());
		respuesta.setBitMap(_respuesta.getBitMap());
		respuesta.setC55(_respuesta.getC55());
		respuesta.setC63(_respuesta.getC63());
		respuesta.setCodError(_respuesta.getCodError());
		respuesta.setCodProceso(_respuesta.getCodProceso());
		respuesta.setCodRespuesta(_respuesta.getCodRespuesta());
		respuesta.setDescError(_respuesta.getDescError());
		respuesta.setFechaHora(_respuesta.getFechaHora());
		respuesta.setFechaLocal(_respuesta.getFechaLocal());
		respuesta.setHoraLocal(_respuesta.getHoraLocal());
		respuesta.setMensaje(_respuesta.getMensaje());
		respuesta.setMonto(_respuesta.getMonto());
		respuesta.setNoAutorizacion(_respuesta.getNoAutorizacion());
		respuesta.setNumAplicacionPAN(_respuesta.getNumAplicacionPAN());
		respuesta.setNumTerminal(_respuesta.getNumTerminal());
		respuesta.setPagoTarjetaId(_respuesta.getPagoTarjetaId());
		respuesta.setReferencia(_respuesta.getReferencia());
		respuesta.setTipoMsg(_respuesta.getTipoMsg());
		respuesta.setTipoTarjeta(_respuesta.getTipoTarjeta());
		respuesta.setTrace(_respuesta.getTrace());
		return respuesta;
	}
        /*public RespuestaPagoTarjetaDto respuestaPagoTarjetaBean2Dto (RespuestaPagoTarjetaBean _respuesta)
	{
		RespuestaPagoTarjetaDto respuesta = new RespuestaPagoTarjetaDto();
		respuesta.setAfiliacion(_respuesta.getAfiliacion());
		respuesta.setBanco(_respuesta.getBanco());
		respuesta.setBitMap(_respuesta.getBitMap());
		respuesta.setC55(_respuesta.getC55());
		respuesta.setC63(_respuesta.getC63());
		respuesta.setCodError(_respuesta.getCodError());
		respuesta.setCodProceso(_respuesta.getCodProceso());
		respuesta.setCodRespuesta(_respuesta.getCodRespuesta());
		respuesta.setDescError(_respuesta.getDescError());
		respuesta.setFechaHora(_respuesta.getFechaHora());
		respuesta.setFechaLocal(_respuesta.getFechaLocal());
		respuesta.setHoraLocal(_respuesta.getHoraLocal());
		respuesta.setMensaje(_respuesta.getMensaje());
		respuesta.setMonto(_respuesta.getMonto());
		respuesta.setNoAutorizacion(_respuesta.getNoAutorizacion());
		respuesta.setNumAplicacionPAN(_respuesta.getNumAplicacionPAN());
		respuesta.setNumTerminal(_respuesta.getNumTerminal());
		respuesta.setPagoTarjetaId(_respuesta.getPagoTarjetaId());
		respuesta.setReferencia(_respuesta.getReferencia());
		respuesta.setTipoMsg(_respuesta.getTipoMsg());
		respuesta.setTipoTarjeta(_respuesta.getTipoTarjeta());
		respuesta.setTrace(_respuesta.getTrace());
		return respuesta;
	}

	public RespuestaPagoTarjetaBean respuestaPagoTarjetaDto2Bean (RespuestaPagoTarjetaDto _respuesta)
	{
		RespuestaPagoTarjetaBean respuesta = new RespuestaPagoTarjetaBean();
		respuesta.setAfiliacion(_respuesta.getAfiliacion());
		respuesta.setBanco(_respuesta.getBanco());
		respuesta.setBitMap(_respuesta.getBitMap());
		respuesta.setC55(_respuesta.getC55());
		respuesta.setC63(_respuesta.getC63());
		respuesta.setCodError(_respuesta.getCodError());
		respuesta.setCodProceso(_respuesta.getCodProceso());
		respuesta.setCodRespuesta(_respuesta.getCodRespuesta());
		respuesta.setDescError(_respuesta.getDescError());
		respuesta.setFechaHora(_respuesta.getFechaHora());
		respuesta.setFechaLocal(_respuesta.getFechaLocal());
		respuesta.setHoraLocal(_respuesta.getHoraLocal());
		respuesta.setMensaje(_respuesta.getMensaje());
		respuesta.setMonto(_respuesta.getMonto());
		respuesta.setNoAutorizacion(_respuesta.getNoAutorizacion());
		respuesta.setNumAplicacionPAN(_respuesta.getNumAplicacionPAN());
		respuesta.setNumTerminal(_respuesta.getNumTerminal());
		respuesta.setPagoTarjetaId(_respuesta.getPagoTarjetaId());
		respuesta.setReferencia(_respuesta.getReferencia());
		respuesta.setTipoMsg(_respuesta.getTipoMsg());
		respuesta.setTipoTarjeta(_respuesta.getTipoTarjeta());
		respuesta.setTrace(_respuesta.getTrace());
		return respuesta;
	}

	public PeticionReversoTarjetaDto peticionReversoTarjetaBean2Dto (PeticionReversoTarjetaBean _peticion)
	{
		PeticionReversoTarjetaDto peticion = new PeticionReversoTarjetaDto();
		peticion.setAfiliacion(_peticion.getAfiliacion());
		peticion.setChip(_peticion.getChip());
		peticion.setComision(_peticion.getComision());
		peticion.setFalloLecturaChip(_peticion.getFalloLecturaChip());
		peticion.setFechaLocal(_peticion.getFechaLocal());
		peticion.setHoraLocal(_peticion.getHoraLocal());
		peticion.setMonto(_peticion.getMonto());
		peticion.setNumTerminal(_peticion.getNumTerminal());
		peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
		peticion.setPagoTarjetaId(_peticion.getPagoTarjetaId());
		peticion.setPaPaisId(_peticion.getPaPaisId());
		peticion.setTiendaId(_peticion.getTiendaId());
		peticion.setTrace(_peticion.getTrace());
		peticion.setTrack2(_peticion.getTrack2());
		peticion.setCanalPago(_peticion.getCanalPago());
		return peticion;
	}*/

        
	public PeticionReversoTarjetaChipDto peticionReversoTarjetaDto2Rest (PeticionReversoTarjetaDto _peticion)
	{
		PeticionReversoTarjetaChipDto peticion = new PeticionReversoTarjetaChipDto();
		peticion.setAfiliacion(_peticion.getAfiliacion());
		peticion.setChip(_peticion.isChip());
		peticion.setComision(_peticion.getComision());
		peticion.setFalloLecturaChip(_peticion.isFalloLecturaChip());
		peticion.setFechaLocal(_peticion.getFechaLocal());
		peticion.setHoraLocal(_peticion.getHoraLocal());
		peticion.setMonto(_peticion.getMonto());
		peticion.setNumTerminal(_peticion.getNumTerminal());
		peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
		peticion.setPagoTarjetaId(_peticion.getPagoTarjetaId());
		peticion.setPaPaisId(_peticion.getPaPaisId());
		peticion.setTiendaId(_peticion.getTiendaId());
		peticion.setTrace(_peticion.getTrace());
		peticion.setTrack2(_peticion.getTrack2());
		peticion.setuId(_peticion.getUId());
		peticion.setCanalPago(_peticion.getCanalPago());
		return peticion;
	}
	/*public PeticionReversoTarjetaBean peticionReversoTarjetaDto2Bean (PeticionReversoTarjetaDto _peticion)
	{
		PeticionReversoTarjetaBean peticion = new PeticionReversoTarjetaBean();
		peticion.setAfiliacion(_peticion.getAfiliacion());
		peticion.setChip(_peticion.isChip());
		peticion.setComision(_peticion.getComision());
		peticion.setFalloLecturaChip(_peticion.isFalloLecturaChip());
		peticion.setFechaLocal(_peticion.getFechaLocal());
		peticion.setHoraLocal(_peticion.getHoraLocal());
		peticion.setMonto(_peticion.getMonto());
		peticion.setNumTerminal(_peticion.getNumTerminal());
		peticion.setPaConciliacionId(_peticion.getPaConciliacionId());
		peticion.setPagoTarjetaId(_peticion.getPagoTarjetaId());
		peticion.setPaPaisId(_peticion.getPaPaisId());
		peticion.setTiendaId(_peticion.getTiendaId());
		peticion.setTrace(_peticion.getTrace());
		peticion.setTrack2(_peticion.getTrack2());
		peticion.setUId(_peticion.getUId());
		peticion.setCanalPago(_peticion.getCanalPago());
		return peticion;
	}*/

	public RespuestaReversoTarjetaDto respuestaReversoTarjetaRest2Dto (RespuestaReversoTarjetaChipDto _respuesta)
	{
		RespuestaReversoTarjetaDto respuesta = new RespuestaReversoTarjetaDto();
		respuesta.setAfiliacion(_respuesta.getAfiliacion());
		respuesta.setAutorizacion(_respuesta.getAutorizacion());
		respuesta.setBitMap(_respuesta.getBitMap());
		respuesta.setCodError(_respuesta.getCodError());
		respuesta.setCodProceso(_respuesta.getCodProceso());
		respuesta.setCodRespuesta(_respuesta.getCodRespuesta());
		respuesta.setDescError(_respuesta.getDescError());
		respuesta.setDescripcion(_respuesta.getDescripcion());
		respuesta.setFechaHora(_respuesta.getFechaHora());
		respuesta.setFechaLocal(_respuesta.getFechaLocal());
		respuesta.setHoraLocal(_respuesta.getHoraLocal());
		respuesta.setMensaje(_respuesta.getMensaje());
		respuesta.setMonto(_respuesta.getMonto());
		respuesta.setReferencia(_respuesta.getReferencia());
		respuesta.setTerminal(_respuesta.getTerminal());
		respuesta.setTarjetaId(_respuesta.getTarjetaId());
		respuesta.setTipoMensaje(_respuesta.getTipoMensaje());
		respuesta.setTrace(_respuesta.getTrace());
		return respuesta;
	}
        /*public RespuestaReversoTarjetaDto respuestaReversoTarjetaBean2Dto (RespuestaReversoTarjetaBean _respuesta)
	{
		RespuestaReversoTarjetaDto respuesta = new RespuestaReversoTarjetaDto();
		respuesta.setAfiliacion(_respuesta.getAfiliacion());
		respuesta.setAutorizacion(_respuesta.getAutorizacion());
		respuesta.setBitMap(_respuesta.getBitMap());
		respuesta.setCodError(_respuesta.getCodError());
		respuesta.setCodProceso(_respuesta.getCodProceso());
		respuesta.setCodRespuesta(_respuesta.getCodRespuesta());
		respuesta.setDescError(_respuesta.getDescError());
		respuesta.setDescripcion(_respuesta.getDescripcion());
		respuesta.setFechaHora(_respuesta.getFechaHora());
		respuesta.setFechaLocal(_respuesta.getFechaLocal());
		respuesta.setHoraLocal(_respuesta.getHoraLocal());
		respuesta.setMensaje(_respuesta.getMensaje());
		respuesta.setMonto(_respuesta.getMonto());
		respuesta.setReferencia(_respuesta.getReferencia());
		respuesta.setTerminal(_respuesta.getTerminal());
		respuesta.setTarjetaId(_respuesta.getTarjetaId());
		respuesta.setTipoMensaje(_respuesta.getTipoMensaje());
		respuesta.setTrace(_respuesta.getTrace());
		return respuesta;
	}

	public RespuestaReversoTarjetaBean respuestaReversoTarjetaDto2Bean (RespuestaReversoTarjetaDto _respuesta)
	{
		RespuestaReversoTarjetaBean respuesta = new RespuestaReversoTarjetaBean();
		respuesta.setAfiliacion(_respuesta.getAfiliacion());
		respuesta.setAutorizacion(_respuesta.getAutorizacion());
		respuesta.setBitMap(_respuesta.getBitMap());
		respuesta.setCodError(_respuesta.getCodError());
		respuesta.setCodProceso(_respuesta.getCodProceso());
		respuesta.setCodRespuesta(_respuesta.getCodRespuesta());
		respuesta.setDescError(_respuesta.getDescError());
		respuesta.setDescripcion(_respuesta.getDescripcion());
		respuesta.setFechaHora(_respuesta.getFechaHora());
		respuesta.setFechaLocal(_respuesta.getFechaLocal());
		respuesta.setHoraLocal(_respuesta.getHoraLocal());
		respuesta.setMensaje(_respuesta.getMensaje());
		respuesta.setMonto(_respuesta.getMonto());
		respuesta.setReferencia(_respuesta.getReferencia());
		respuesta.setTerminal(_respuesta.getTerminal());
		respuesta.setTarjetaId(_respuesta.getTarjetaId());
		respuesta.setTipoMensaje(_respuesta.getTipoMensaje());
		respuesta.setTrace(_respuesta.getTrace());
		return respuesta;
	}*/
}
