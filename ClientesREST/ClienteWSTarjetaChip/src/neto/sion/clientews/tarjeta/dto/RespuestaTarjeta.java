package neto.sion.clientews.tarjeta.dto;

public class RespuestaTarjeta
{
	private int codError;
	private String descError;

	public RespuestaTarjeta()
	{
		this.codError = -99;
		this.descError = "ERROR: VACIA";
	}

	public int getCodError() {
		return codError;
	}

	public void setCodError(int codError) {
		this.codError = codError;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	@Override
	public String toString() {
		return "RespuestaTarjeta ["
				+ "codError=" + codError
				+ ", descError=" + descError
				+ "]";
	}
}
