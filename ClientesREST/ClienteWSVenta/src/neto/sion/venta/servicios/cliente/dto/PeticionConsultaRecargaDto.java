/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvega
 */
public class PeticionConsultaRecargaDto {

    private int pais;
    private long tiendaId;
    private long articuloId;
    private String codigoBarras;
    private String numeroCelular;
    private long tiempoAireId;

    public int getPais() {
        return pais;
    }

    public void setPais(int pais) {
        this.pais = pais;
    }

    public long getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(long tiendaId) {
        this.tiendaId = tiendaId;
    }

    public long getArticuloId() {
        return articuloId;
    }

    public void setArticuloId(long articuloId) {
        this.articuloId = articuloId;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public String getNumeroCelular() {
        return numeroCelular;
    }

    public void setNumeroCelular(String numeroCelular) {
        this.numeroCelular = numeroCelular;
    }

    public long getTiempoAireId() {
        return tiempoAireId;
    }

    public void setTiempoAireId(long tiempoAireId) {
        this.tiempoAireId = tiempoAireId;
    }

    @Override
    public String toString() {
        return "PeticionConsultaRecargaDto [pais=" + pais + ", tiendaId="
                + tiendaId + ", articuloId=" + articuloId + ", codigoBarras="
                + codigoBarras + ", numeroCelular=" + numeroCelular
                + ", tiempoAireId=" + tiempoAireId + "]";
    }
}
