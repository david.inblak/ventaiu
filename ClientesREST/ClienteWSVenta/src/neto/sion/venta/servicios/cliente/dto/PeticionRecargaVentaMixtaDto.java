/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvega
 */
public class PeticionRecargaVentaMixtaDto {

    private long articuloId;
    private double monto;
    private String terminal;
    private String numeroTelefono;
    private int companiaTelefonicaId;
    private long tiendaId;
    private String fechaOperacion;
    private long usuarioId;
    private int folio;
    private int pais;
    private String codigoBarras;
    private long consecutivoTiempoAire;

    public long getArticuloId() {
        return articuloId;
    }

    public void setArticuloId(long articuloId) {
        this.articuloId = articuloId;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public int getCompaniaTelefonicaId() {
        return companiaTelefonicaId;
    }

    public void setCompaniaTelefonicaId(int companiaTelefonicaId) {
        this.companiaTelefonicaId = companiaTelefonicaId;
    }

    public long getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(long tiendaId) {
        this.tiendaId = tiendaId;
    }

    public String getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(String fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public int getPais() {
        return pais;
    }

    public void setPais(int pais) {
        this.pais = pais;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public long getConsecutivoTiempoAire() {
        return consecutivoTiempoAire;
    }

    public void setConsecutivoTiempoAire(long consecutivoTiempoAire) {
        this.consecutivoTiempoAire = consecutivoTiempoAire;
    }

    @Override
    public String toString() {
        return "PeticionRecargaVentaMixtaDto [articuloId=" + articuloId
                + ", monto=" + monto + ", terminal=" + terminal
                + ", numeroTelefono=" + numeroTelefono
                + ", companiaTelefonicaId=" + companiaTelefonicaId
                + ", tiendaId=" + tiendaId + ", fechaOperacion="
                + fechaOperacion + ", usuarioId=" + usuarioId + ", folio="
                + folio + ", pais=" + pais + ", codigoBarras=" + codigoBarras
                + ", consecutivoTiempoAire=" + consecutivoTiempoAire + "]";
    }
}
