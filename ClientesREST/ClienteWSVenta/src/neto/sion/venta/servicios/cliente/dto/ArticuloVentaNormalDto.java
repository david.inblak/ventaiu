/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvega
 */
public class ArticuloVentaNormalDto {

    private long articuloId;
    private String codigoBarras;
    private int divisionId;
    private double cantidad;
    private double precio;
    private double costo;
    private double descuento;
    private double iva;

    public long getArticuloId() {
        return articuloId;
    }

    public void setArticuloId(long articuloId) {
        this.articuloId = articuloId;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double getIva() {
        return iva;
    }

    @Override
    public String toString() {
        return "ArticuloVentaNormalDto [articuloId=" + articuloId
                + ", codigoBarras=" + codigoBarras + ", divisionId="
                + divisionId + ", cantidad=" + cantidad + ", precio=" + precio
                + ", costo=" + costo + ", descuento=" + descuento + ", iva="
                + iva + "]";
    }
}
