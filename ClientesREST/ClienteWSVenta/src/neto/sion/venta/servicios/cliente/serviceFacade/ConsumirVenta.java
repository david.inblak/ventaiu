package neto.sion.venta.servicios.cliente.serviceFacade;

import neto.sion.tienda.genericos.exception.SionException;
import neto.sion.venta.servicios.cliente.dto.*;

/**
 * Interfaz que expone los metodos del Servicio de Venta que pueden ser
 * consumidos.
 *
 * @author Carlos V. Perez L.
 */
public interface ConsumirVenta {

    /**
     * *
     * Realiza la llamada al metodo registrarVenta expuesto por el WS de Venta
     *
     * @param _peticion
     * @return Un objeto con los detalles del registro de la venta.
     * @throws SionException
     */
    public RespuestaVentaDto registrarVenta(PeticionVentaDto _peticion) throws SionException;

    /**
     * *
     * Obtiene el detalle de una venta, llamando al metodo obtenerDetalleVenta
     * expuesto por el WS de Venta.
     *
     * @param _peticion
     * @return Un objeto con los datos de la venta solicitada.
     * @throws SionException
     */
    public RespuestaDetalleVentaDto obtenerDetalleVenta(PeticionDetalleVentaDto _peticion) throws SionException;

    /**
     * *
     * Realiza la devolucion de una venta, llamando al metodo
     * obtenerDevolucionVenta expuesto por el WS de venta.
     *
     * @param _peticion
     * @return Un objeto con los detalles de la devolucion.
     * @throws SionException
     */
    public RespuestaVentaDto obtenerDevolucionVenta(PeticionDevolucionDto _peticion) throws SionException;

    /**
     * *
     * Realiza una llamada al metodo registrarVentaTA expuesto por el WS de
     * Venta.
     *
     * @param _peticion
     * @return Un objeto con los detalles de la venta de TA.
     * @throws SionException
     */
    public RespuestaVentaDto registrarVentaTA(PeticionVentaTADto _peticion) throws SionException;

    /**
     * *
     * Metodo que regresa es estatus de un cajero
     *
     * @param _cajeroId
     * @return Un objeto con el detalle del estatus.
     * @throws SionException
     */
    public RespuestaBloqueoVentaDto obtenerEstatusCajero(PeticionBloqueoDto _peticion) throws SionException;

    /**
     * *
     * Metodo que regresa todos los datos necesarios para la reimpresion de
     * vouchers.
     *
     * @param _peticion
     * @return Un objeto con los detalles necesarios para la reimpresion un
     * ticket.
     * @throws SionException
     */
    public RespuestaActReimpVoucherDto actualizarReimpresionVoucher(PeticionReimpresionDto _peticion) throws SionException;

    /**
     * *
     * Metodo que consulta una transaccion en central.
     *
     * @param _peticion
     * @return Un objeto con los detalles de la consulta.
     * @throws SionException
     */
    public RespuestaConsultaTransaccionCentralDto consultarTransaccionCentral(PeticionTransaccionCentralDto _peticion) throws SionException;

    /**
     * *
     * Realiza la llamada al metodo registrarVentaMixta expuesto por el WS de
     * Venta
     *
     * @param _peticion
     * @return Un objeto con los detalles de venta mixta.
     * @throws SionException
     */
    public RespuestaVentaMixtaDto registrarVentaMixta(PeticionVentaMixtaDto _peticion) throws SionException;

    public RespuestaActReimpTicketDto confirmarReimpresionTicket(PeticionActReimpTicketDto _peticion) throws SionException;

    /**
     * *
     * Metodo que consulta una tarjeta en central.
     *
     * @param _peticion
     * @return Un objeto con los detalles de la tarjeta.
     * @throws SionException
     */
    public RespuestaTarjetaDto consultarTarjeta(PeticionTarjetaDto _peticion) throws SionException;

    /**
     * *
     * Metodo que consulta una recarga en central.
     *
     * @param _peticion
     * @return Un objeto con los detalles de la tarjeta.
     * @throws SionException
     */
    public abstract RespuestaConsultaTransaccionTADto consultarTransaccionTACentral(PeticionTransaccionCentralDto paramPeticionTransaccionCentralDto)
            throws SionException;

    /**
     * Método que consulta las útlimas 10 transacciones grabadas en central
     *
     * @return Un objeto con las transacciones
     * @throws SionException
     */
    public abstract RespuestaUltimastransaccionesVentaDto consultarUltimasTransacciones(PeticionUltimasTransaccionesDto _peticion) throws SionException;

    /**
     * Método que consulta el detalle de una vetna para su reimpresión, contempla ieps
     * @param _peticion
     * @return
     * @throws SionException 
     */
    public abstract RespuestaReimpresionDto consultarReimpresion(PeticionTicketReimpresionDto _peticion)
            throws SionException;
    
    /****reimpresion devolucion**/
    
    /**
     * Método que consulta un ticket de venta o recarga TA para su reimpresión
     *
     * @param _peticion Un objeto de petición de detalle de ticket
     * @return Un objeto
     * @throws SionException
     */
    public abstract RespuestaTicketReimpresionDto consultarDetalleTicket(PeticionTicketReimpresionDto _peticion)
            throws SionException;

}