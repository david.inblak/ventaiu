package neto.sion.venta.servicios.cliente.dto;

import neto.sion.tienda.genericos.dto.Base;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * *
 * Clase que representa un articulo.
 *
 * @author Carlos V. Perez L.
 *
 */
public class ArticuloDto extends Base {

    @JsonProperty("fiArticuloId") private long fiArticuloId;
    @JsonProperty("fcCdGbBarras") private String fcCdGbBarras;
    @JsonProperty("fnCantidad") private double fnCantidad;
    @JsonProperty("fnPrecio") private double fnPrecio;
    @JsonProperty("fnCosto") private double fnCosto;
    @JsonProperty("fnDescuento") private double fnDescuento;
    @JsonProperty("fnIva") private double fnIva;
    @JsonProperty("fcNombreArticulo") private String fcNombreArticulo;
    @JsonProperty("fiAgranel") private double fiAgranel;
    @JsonProperty("fnIepsId") private int fnIepsId;
    @JsonProperty("tipoDescuento") private int tipoDescuento;

    public ArticuloDto() {
        this.fiArticuloId = 0;
        this.fcCdGbBarras = "";
        this.fnCantidad = 0;
        this.fnPrecio = 0;
        this.fnCosto = 0;
        this.fnDescuento = 0;
        this.fnIva = 0;
        this.fcNombreArticulo = "";
        this.fiAgranel = 0;
        this.fnIepsId = 0;
    }
    
    
    /*
     * @Deprecated Empezar a usar la nueva versión con tipo de descuento incluido en el constructor.
    
    public ArticuloDto(long _fiArticuloId, String _fcCdGbBarras, double _fnCantidad, double _fnPrecion,
           double _fnCosto, double _fnDescuento, double _fnIva, String _fcNombreArticulo, double _fiAgranel, int _fnIepsId) {
        this.fiArticuloId = _fiArticuloId;
        this.fcCdGbBarras = _fcCdGbBarras;
        this.fnCantidad = _fnCantidad;
        this.fnPrecio = _fnPrecion;
        this.fnCosto = _fnCosto;
        this.fnDescuento = _fnDescuento;
        this.fnIva = _fnIva;
        this.fcNombreArticulo = _fcNombreArticulo;
        this.fiAgranel = _fiAgranel;
        this.fnIepsId = _fnIepsId;
        this.tipoDescuento = 0;
    }*/


    public ArticuloDto(long _fiArticuloId, String _fcCdGbBarras, double _fnCantidad, double _fnPrecion,
            double _fnCosto, double _fnDescuento, double _fnIva, String _fcNombreArticulo, double _fiAgranel, int _fnIepsId,
            int _fiTipoDescuentos) {
        this.fiArticuloId = _fiArticuloId;
        this.fcCdGbBarras = _fcCdGbBarras;
        this.fnCantidad = _fnCantidad;
        this.fnPrecio = _fnPrecion;
        this.fnCosto = _fnCosto;
        this.fnDescuento = _fnDescuento;
        this.fnIva = _fnIva;
        this.fcNombreArticulo = _fcNombreArticulo;
        this.fiAgranel = _fiAgranel;
        this.fnIepsId = _fnIepsId;
        this.tipoDescuento = _fiTipoDescuentos;
    }

    public double getFiAgranel() {
        return fiAgranel;
    }

    public void setFiAgranel(double fiAgranel) {
        this.fiAgranel = fiAgranel;
    }

    public String getFcNombreArticulo() {
        return fcNombreArticulo;
    }

    public void setFcNombreArticulo(String fcNombreArticulo) {
        this.fcNombreArticulo = fcNombreArticulo;
    }

    public long getFiArticuloId() {
        return fiArticuloId;
    }

    public void setFiArticuloId(long fiArticuloId) {
        this.fiArticuloId = fiArticuloId;
    }

    public String getFcCdGbBarras() {
        return fcCdGbBarras;
    }

    public void setFcCdGbBarras(String fcCdGbBarras) {
        this.fcCdGbBarras = fcCdGbBarras;
    }

    public double getFnCantidad() {
        return fnCantidad;
    }

    public void setFnCantidad(double fnCantidad) {
        this.fnCantidad = fnCantidad;
    }

    public double getFnPrecio() {
        return fnPrecio;
    }

    public void setFnPrecio(double fnPrecio) {
        this.fnPrecio = fnPrecio;
    }

    public double getFnCosto() {
        return fnCosto;
    }

    public void setFnCosto(double fnCosto) {
        this.fnCosto = fnCosto;
    }

    public double getFnDescuento() {
        return fnDescuento;
    }

    public void setFnDescuento(double fnDescuento) {
        this.fnDescuento = fnDescuento;
    }

    public double getFnIva() {
        return fnIva;
    }

    public void setFnIva(double fnIva) {
        this.fnIva = fnIva;
    }

    public int getFnIepsId() {
        return fnIepsId;
    }

    public void setFnIepsId(int fnIepsId) {
        this.fnIepsId = fnIepsId;
    }

    public int getTipoDescuento() {
        return tipoDescuento;
    }

    public void setTipoDescuento(int tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }

    @Override
    public String toString() {
        return "ArticuloDto{" + "fiArticuloId=" + fiArticuloId + ", fcCdGbBarras=" + fcCdGbBarras + ", fnCantidad=" + fnCantidad + ", fnPrecio=" + fnPrecio + ", fnCosto=" + fnCosto + ", fnDescuento=" + fnDescuento + ", fnIva=" + fnIva + ", fcNombreArticulo=" + fcNombreArticulo + ", fiAgranel=" + fiAgranel + ", fnIepsId=" + fnIepsId + ", tipoDescuento=" + tipoDescuento + '}';
    }

    

    
}
