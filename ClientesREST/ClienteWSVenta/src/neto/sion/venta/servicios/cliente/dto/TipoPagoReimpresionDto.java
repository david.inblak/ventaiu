/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvega
 */
public class TipoPagoReimpresionDto {

    private int fiTipoPagoId;
    private double montoPago;
    private double fnMontoRecibido;

    public int getFiTipoPagoId() {
        return fiTipoPagoId;
    }

    public void setFiTipoPagoId(int fiTipoPagoId) {
        this.fiTipoPagoId = fiTipoPagoId;
    }

    public double getMontoPago() {
        return montoPago;
    }

    public void setMontoPago(double montoPago) {
        this.montoPago = montoPago;
    }

    public double getFnMontoRecibido() {
        return fnMontoRecibido;
    }

    public void setFnMontoRecibido(double fnMontoRecibido) {
        this.fnMontoRecibido = fnMontoRecibido;
    }

    @Override
    public String toString() {
        return "TipoPagoReimpresionDto{" + "fiTipoPagoId=" + fiTipoPagoId + ", montoPago=" + montoPago + ", fnMontoRecibido=" + fnMontoRecibido + '}';
    }
}
