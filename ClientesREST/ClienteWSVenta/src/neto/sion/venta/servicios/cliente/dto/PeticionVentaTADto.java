package neto.sion.venta.servicios.cliente.dto;

import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;
import java.util.Arrays;

/**
 * *
 * Clase que representa una Peticion de venta de T.A
 *
 * @author Carlos V. Perez L.
 *
 */
public class PeticionVentaTADto extends PeticionBaseVentaDto {

    private ArticuloDto[] articulos;
    private String paFechaOper;
    private double paMontoTotalVta;
    private String paTerminal;
    private int paTipoMovto;
    private long paUsuarioId;
    private TipoPagoDto[] tiposPago;
    private long telefono;
    private int companiaTelefonicaId;
    private int fueraLinea;
    private int numFolio;
    private long paTransaccionDev;

    public ArticuloDto[] getArticulos() {
        return articulos;
    }

    public void setArticulos(ArticuloDto[] articulos) {
        this.articulos = articulos;
    }

    public String getPaFechaOper() {
        return paFechaOper;
    }

    public void setPaFechaOper(String paFechaOper) {
        this.paFechaOper = paFechaOper;
    }

    public double getPaMontoTotalVta() {
        return paMontoTotalVta;
    }

    public void setPaMontoTotalVta(double paMontoTotalVta) {
        this.paMontoTotalVta = paMontoTotalVta;
    }

    public String getPaTerminal() {
        return paTerminal;
    }

    public void setPaTerminal(String paTerminal) {
        this.paTerminal = paTerminal;
    }

    public int getPaTipoMovto() {
        return paTipoMovto;
    }

    public void setPaTipoMovto(int paTipoMovto) {
        this.paTipoMovto = paTipoMovto;
    }

    public long getPaUsuarioId() {
        return paUsuarioId;
    }

    public void setPaUsuarioId(long paUsuarioId) {
        this.paUsuarioId = paUsuarioId;
    }

    public TipoPagoDto[] getTiposPago() {
        return tiposPago;
    }

    public void setTiposPago(TipoPagoDto[] tiposPago) {
        this.tiposPago = tiposPago;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    public int getCompaniaTelefonicaId() {
        return companiaTelefonicaId;
    }

    public void setCompaniaTelefonicaId(int companiaTelefonicaId) {
        this.companiaTelefonicaId = companiaTelefonicaId;
    }

    public int getFueraLinea() {
        return fueraLinea;
    }

    public void setFueraLinea(int fueraLinea) {
        this.fueraLinea = fueraLinea;
    }

    public long getPaTransaccionDev() {
        return paTransaccionDev;
    }

    public void setPaTransaccionDev(long paTransaccionDev) {
        this.paTransaccionDev = paTransaccionDev;
    }

    public int getNumFolio() {
        return numFolio;
    }

    public void setNumFolio(int numFolio) {
        this.numFolio = numFolio;
    }

    @Override
    public String toString() {
        return "PeticionVentaTADto{" + "articulos=" +Arrays.toString(articulos)  + ", paFechaOper=" + paFechaOper + ", paMontoTotalVta=" + paMontoTotalVta + ", paTerminal=" + paTerminal + ", paTipoMovto=" + paTipoMovto + ", paUsuarioId=" + paUsuarioId + ", tiposPago=" + Arrays.toString(tiposPago) + ", telefono=" + telefono + ", companiaTelefonicaId=" + companiaTelefonicaId + ", fueraLinea=" + fueraLinea + ", numFolio=" + numFolio + ", paTransaccionDev=" + paTransaccionDev + '}';
    }
}
