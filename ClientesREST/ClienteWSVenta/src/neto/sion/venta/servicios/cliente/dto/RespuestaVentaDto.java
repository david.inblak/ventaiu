package neto.sion.venta.servicios.cliente.dto;

import neto.sion.tienda.genericos.dto.Base;

/**
 * *
 * Clase que representa la respuesta de una venta.
 *
 * @author Carlos V. Perez L.
 *
 */
public class RespuestaVentaDto extends Base {

    private long paTransaccionId;
    private int paCdgError;
    private String paDescError;
    private long paTaNumOperacion;
    private BloqueoDto[] paTypCursorBlqs;

    public BloqueoDto[] getPaTypCursorBlqs() {
        return paTypCursorBlqs;
    }

    public void setPaTypCursorBlqs(BloqueoDto[] paTypCursorBlqs) {
        this.paTypCursorBlqs = paTypCursorBlqs;
    }

    public long getPaTransaccionId() {
        return paTransaccionId;
    }

    public void setPaTransaccionId(long paTransaccionId) {
        this.paTransaccionId = paTransaccionId;
    }

    public int getPaCdgError() {
        return paCdgError;
    }

    public void setPaCdgError(int paCdgError) {
        this.paCdgError = paCdgError;
    }

    public String getPaDescError() {
        return paDescError;
    }

    public void setPaDescError(String paDescError) {
        this.paDescError = paDescError;
    }
    
    public long getPaTaNumOperacion() {
        return paTaNumOperacion;
    }

    public void setPaTaNumOperacion(long paTaNumOperacion) {
        this.paTaNumOperacion = paTaNumOperacion;
    }

    @Override
    public String toString() {
        return "RespuestaVentaDto{" + "paTransaccionId=" + paTransaccionId + ", paCdgError=" + paCdgError + ", paDescError=" + paDescError + ", paTaNumOperacion=" + paTaNumOperacion + ", paTypCursorBlqs=" + paTypCursorBlqs + '}';
    }
}
