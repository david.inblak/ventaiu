package neto.sion.venta.servicios.cliente.dto;

import java.util.Arrays;
import neto.sion.tienda.genericos.dto.Base;

public class ReimpresionVoucherDto extends Base {

    private int cdgError;
    private String descError;
    private long paUsuarioId;
    private String paFechaCreacion;
    private String paEstacionTrabajo;
    private ReimpresionTarjetaDto[] paReimpresionesTar;

    public int getCdgError() {
        return cdgError;
    }

    public void setCdgError(int cdgError) {
        this.cdgError = cdgError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    public long getPaUsuarioId() {
        return paUsuarioId;
    }

    public void setPaUsuarioId(long paUsuarioId) {
        this.paUsuarioId = paUsuarioId;
    }

    public String getPaFechaCreacion() {
        return paFechaCreacion;
    }

    public void setPaFechaCreacion(String paFechaCreacion) {
        this.paFechaCreacion = paFechaCreacion;
    }

    public String getPaEstacionTrabajo() {
        return paEstacionTrabajo;
    }

    public void setPaEstacionTrabajo(String paEstacionTrabajo) {
        this.paEstacionTrabajo = paEstacionTrabajo;
    }

    public ReimpresionTarjetaDto[] getPaReimpresionesTar() {
        return paReimpresionesTar;
    }

    public void setPaReimpresionesTar(ReimpresionTarjetaDto[] paReimpresionesTar) {
        this.paReimpresionesTar = paReimpresionesTar;
    }

    @Override
    public String toString() {
        return "RespuestaReimpresionVoucherDto [cdgError=" + cdgError
                + ", descError=" + descError + ", paUsuarioId=" + paUsuarioId
                + ", paFechaCreacion=" + paFechaCreacion
                + ", paEstacionTrabajo=" + paEstacionTrabajo
                + ", paReimpresionTar=" + Arrays.toString(paReimpresionesTar)
                + "]";
    }
}
