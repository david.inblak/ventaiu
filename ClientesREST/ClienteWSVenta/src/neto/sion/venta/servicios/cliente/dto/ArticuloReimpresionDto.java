/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvega
 */
public class ArticuloReimpresionDto {

    private double fnCantidad;
    private double fnPrecio;
    private double fnIva;
    private double fnDescuento;
    private String fcNombreArticulo;

    public double getFnCantidad() {
        return fnCantidad;
    }

    public void setFnCantidad(double fnCantidad) {
        this.fnCantidad = fnCantidad;
    }

    public double getFnPrecio() {
        return fnPrecio;
    }

    public void setFnPrecio(double fnPrecio) {
        this.fnPrecio = fnPrecio;
    }

    public double getFnIva() {
        return fnIva;
    }

    public void setFnIva(double fnIva) {
        this.fnIva = fnIva;
    }

    public double getFnDescuento() {
        return fnDescuento;
    }

    public void setFnDescuento(double fnDescuento) {
        this.fnDescuento = fnDescuento;
    }

    public String getFcNombreArticulo() {
        return fcNombreArticulo;
    }

    public void setFcNombreArticulo(String fcNombreArticulo) {
        this.fcNombreArticulo = fcNombreArticulo;
    }

    @Override
    public String toString() {
        return "ArticuloReimpresionDto{" + "fnCantidad=" + fnCantidad + 
                ", fnPrecio=" + fnPrecio + ", fnIva=" + fnIva + ", fnDescuento=" + 
                fnDescuento + ", fcNombreArticulo=" + fcNombreArticulo + '}';
    }
}
