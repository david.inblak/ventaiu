package neto.sion.venta.servicios.cliente.dto;

import neto.sion.tienda.genericos.dto.Base;

/**
 * Clase que representa la respuesta de una peticion de transaccion a central.
 *
 * @author Carlos V. Perez L.
 */
public class RespuestaConsultaTransaccionCentralDto extends Base {

    private int paCdgError;
    private String paDescError;
    private long paTransaccionId;

    public int getPaCdgError() {
        return paCdgError;
    }

    public void setPaCdgError(int paCdgError) {
        this.paCdgError = paCdgError;
    }

    public String getPaDescError() {
        return paDescError;
    }

    public void setPaDescError(String paDescError) {
        this.paDescError = paDescError;
    }

    public long getPaTransaccionId() {
        return paTransaccionId;
    }

    public void setPaTransaccionId(long paTransaccionId) {
        this.paTransaccionId = paTransaccionId;
    }

    @Override
    public String toString() {
        return "RespuestaTransaccionCentralDto{" + "paCdgError=" + paCdgError + ", paDescError=" + paDescError + ", paTransaccionId=" + paTransaccionId + '}';
    }
}
