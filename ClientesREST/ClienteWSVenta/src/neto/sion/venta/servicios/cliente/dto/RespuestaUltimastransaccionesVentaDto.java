/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

import java.util.Arrays;

/**
 *
 * @author fvega
 */
public class RespuestaUltimastransaccionesVentaDto {
    
    private UltimasTransaccionesVentaDto[] ultimasTransaccionesVentaDtos;

    public UltimasTransaccionesVentaDto[] getUltimasTransaccionesVentaDtos() {
        return ultimasTransaccionesVentaDtos;
    }

    public void setUltimasTransaccionesVentaDtos(UltimasTransaccionesVentaDto[] ultimasTransaccionesVentaDtos) {
        this.ultimasTransaccionesVentaDtos = ultimasTransaccionesVentaDtos;
    }

    @Override
    public String toString() {
        return "RespuestaUltimastransaccionesVentaDto{" + "ultimasTransaccionesVentaDtos=" + 
                Arrays.toString(ultimasTransaccionesVentaDtos) + '}';
    }
    
    
    
}
