/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvega
 */
public class RespuestaConsultaRecargaDto {

    private long transaccionTA;
    private int codigoError;
    private String descError;

    public long getTransaccionTA() {
        return transaccionTA;
    }

    public void setTransaccionTA(long transaccionTA) {
        this.transaccionTA = transaccionTA;
    }

    public int getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    @Override
    public String toString() {
        return "RespuestaConsultaRecargaDto [transaccionTA=" + transaccionTA
                + ", codigoError=" + codigoError + ", descError=" + descError
                + "]";
    }
}
