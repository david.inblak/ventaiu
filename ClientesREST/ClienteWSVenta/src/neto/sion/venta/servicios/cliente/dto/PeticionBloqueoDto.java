package neto.sion.venta.servicios.cliente.dto;

import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;

/**
 * *
 * Clase que representa la peticion de los datos de bloqueo de venta
 *
 * @author Carlos V. Perez L.
 *
 */
public class PeticionBloqueoDto extends PeticionBaseVentaDto {

    private long paUsuarioId;

    public long getPaUsuarioId() {
        return paUsuarioId;
    }

    public void setPaUsuarioId(long paUsuarioId) {
        this.paUsuarioId = paUsuarioId;
    }

    @Override
    public String toString() {
        return "PeticionBloqueoDto [paUsuarioId=" + paUsuarioId
                + super.toString() + "]";
    }
}
