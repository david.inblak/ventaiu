/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.sion.venta.servicios.cliente.dto;

/**
 *
 * @author fvega
 */
public class ArticuloTAReimpresionDto {

    private long numOperacion;
    private long numReferencia;
    private String numero;
    private int fiEmpresaId;
    private String compania;
    private String fcNombreArticulo;

    public long getNumOperacion() {
        return numOperacion;
    }

    public void setNumOperacion(long numOperacion) {
        this.numOperacion = numOperacion;
    }

    public long getNumReferencia() {
        return numReferencia;
    }

    public void setNumReferencia(long numReferencia) {
        this.numReferencia = numReferencia;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getFiEmpresaId() {
        return fiEmpresaId;
    }

    public void setFiEmpresaId(int fiEmpresaId) {
        this.fiEmpresaId = fiEmpresaId;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }

    public String getFcNombreArticulo() {
        return fcNombreArticulo;
    }

    public void setFcNombreArticulo(String fcNombreArticulo) {
        this.fcNombreArticulo = fcNombreArticulo;
    }

    @Override
    public String toString() {
        return "ArticuloTAReimpresionDto [numOperacion=" + numOperacion
                + ", numReferencia=" + numReferencia + ", numero=" + numero
                + ", fiEmpresaId=" + fiEmpresaId + ", compania=" + compania
                + ", fcNombreArticulo=" + fcNombreArticulo + "]";
    }
}
