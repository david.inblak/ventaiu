package neto.sion.venta.servicios.cliente.dto;

import neto.sion.venta.base.cliente.dto.PeticionBaseVentaDto;

/**
 * *
 * Clase que representa la peticion de los datos para la reimpresion de un
 * ticket.
 *
 * @author Carlos V. Perez L.
 *
 */
public class PeticionReimpresionDto extends PeticionBaseVentaDto {

    private String paNumTransaccion;
    private int paParamTipoTrans;
    private long paMovimientoId;
    private ReimpresionDto[] paArrReimpresiones;

    public String getPaNumTransaccion() {
        return paNumTransaccion;
    }

    public void setPaNumTransaccion(String paNumTransaccion) {
        this.paNumTransaccion = paNumTransaccion;
    }

    public int getPaParamTipoTrans() {
        return paParamTipoTrans;
    }

    public void setPaParamTipoTrans(int paParamTipoTrans) {
        this.paParamTipoTrans = paParamTipoTrans;
    }

    public ReimpresionDto[] getPaArrReimpresiones() {
        return paArrReimpresiones;
    }

    public void setPaArrReimpresiones(ReimpresionDto[] paArrReimpresiones) {
        this.paArrReimpresiones = paArrReimpresiones;
    }

    public long getPaMovimientoId() {
        return paMovimientoId;
    }

    public void setPaMovimientoId(long paMovimientoId) {
        this.paMovimientoId = paMovimientoId;
    }

    @Override
    public String toString() {
        return "PeticionReimpresionDto{" + "paNumTransaccion=" + paNumTransaccion + ", paParamTipoTrans=" + paParamTipoTrans + ", paMovimientoId=" + paMovimientoId + ", paArrReimpresiones=" + paArrReimpresiones + '}';
    }
}
