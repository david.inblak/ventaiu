
package neto.sion.venta.tarjeta.pt.iso8583.nip.util;

import neto.sion.clientews.tarjeta.dto.RespuestaPagoTarjetaDto;
import neto.sion.venta.tarjeta.pt.iso8583.nip.dto.RespuestaPagatodoDto;
import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.RespuestaPagatodoNipBean;


public class ControlRespuestas {
	
	public ControlRespuestas(){
		
	}
	
    public neto.sion.clientews.tarjeta.dto.RespuestaPagoTarjetaDto respuestaPagoTarjeta_Dto2Dto (RespuestaPagoTarjetaDto _respuesta)
    {
        neto.sion.clientews.tarjeta.dto.RespuestaPagoTarjetaDto respuesta = new neto.sion.clientews.tarjeta.dto.RespuestaPagoTarjetaDto ();
        respuesta.setAfiliacion(_respuesta.getAfiliacion());
        respuesta.setBanco(_respuesta.getBanco());
        respuesta.setBitMap(_respuesta.getBitMap());
        respuesta.setC55(_respuesta.getC55());
        respuesta.setC63(_respuesta.getC63());
        respuesta.setCodError(_respuesta.getCodError());
        respuesta.setCodProceso(_respuesta.getCodProceso());
        respuesta.setCodRespuesta(_respuesta.getCodRespuesta());
        respuesta.setDescError(_respuesta.getDescError());
        respuesta.setFechaHora(_respuesta.getFechaHora());
        respuesta.setFechaLocal(_respuesta.getFechaLocal());
        respuesta.setHoraLocal(_respuesta.getHoraLocal());
        respuesta.setMensaje(_respuesta.getMensaje());
        respuesta.setMonto(_respuesta.getMonto());
        respuesta.setNoAutorizacion(_respuesta.getNoAutorizacion());
        respuesta.setNumAplicacionPAN(_respuesta.getNumAplicacionPAN());
        respuesta.setNumTerminal(_respuesta.getNumTerminal());
        respuesta.setPagoTarjetaId(_respuesta.getPagoTarjetaId());
        respuesta.setReferencia(_respuesta.getReferencia());
        respuesta.setTipoMsg(_respuesta.getTipoMsg());
        respuesta.setTipoTarjeta(_respuesta.getTipoTarjeta());
        respuesta.setTrace(_respuesta.getTrace());
        return respuesta;
    }
    
    
    
    public RespuestaPagatodoDto respPagatodo_Bean2Dto(RespuestaPagatodoDto _respPagatodoBean){
    	
    	RespuestaPagatodoDto respPagatodoDto = new RespuestaPagatodoDto();
    	
    	if(_respPagatodoBean!=null){
    		//if( _respPagatodoBean.getEsPagoExitoso() ){
	    		respPagatodoDto.setrDescRespuesta(_respPagatodoBean.getrDescRespuesta());
	        	respPagatodoDto.setrDescMsgObligatorio(_respPagatodoBean.getrDescMsgObligatorio());
	        	respPagatodoDto.setrIdGrupo(_respPagatodoBean.getrIdGrupo());
	        	respPagatodoDto.setrIdSucursal(_respPagatodoBean.getrIdSucursal());
	        	respPagatodoDto.setrIdTerminal(_respPagatodoBean.getrIdTerminal());
	        	respPagatodoDto.setrIdOperador(_respPagatodoBean.getrIdOperador());
	        	respPagatodoDto.setrNoTicket(_respPagatodoBean.getrNoTicket());
	        	respPagatodoDto.setrReferencia(_respPagatodoBean.getrReferencia());
	        	respPagatodoDto.setrNoConfirmacion(_respPagatodoBean.getrNoConfirmacion());
	        	respPagatodoDto.setrNoSecUnicoPT(_respPagatodoBean.getrNoSecUnicoPT());
	        	respPagatodoDto.setrNoMsgObligatorio(_respPagatodoBean.getrNoMsgObligatorio());
	        	respPagatodoDto.setrNoMsgPromocion(_respPagatodoBean.getrNoMsgPromocion());
	        	respPagatodoDto.setrSaldoDisponibleTarjeta(_respPagatodoBean.getrSaldoDisponibleTarjeta());
	        	respPagatodoDto.setrMontoAbonado(_respPagatodoBean.getrMontoAbonado());
	        	respPagatodoDto.setrS110(_respPagatodoBean.getrS110());
    		//}
    	}
    	
    	
    	return respPagatodoDto;
    }
}
