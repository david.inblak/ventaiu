package neto.sion.venta.tarjeta.pt.iso8583.nip.dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class PeticionVentaCombinadaDto {

		@JsonProperty("peticionVenta")
		PeticionVentaDto peticionVenta;
		@JsonProperty("peticionVentaArticulos")
		PeticionVentaArticulosDto peticionVentaArticulos;
		@JsonProperty("requestPT")
		PeticionPagatodoNipDto requestPT;
		public PeticionVentaDto getPeticionVenta() {
			return peticionVenta;
		}
		public void setPeticionVenta(PeticionVentaDto peticionVenta) {
			this.peticionVenta = peticionVenta;
		}
		public PeticionVentaArticulosDto getPeticionVentaArticulos() {
			return peticionVentaArticulos;
		}
		public void setPeticionVentaArticulos(PeticionVentaArticulosDto peticionVentaArticulos) {
			this.peticionVentaArticulos = peticionVentaArticulos;
		}
		public PeticionPagatodoNipDto getRequestPT() {
			return requestPT;
		}
		public void setRequestPT(PeticionPagatodoNipDto requestPT) {
			this.requestPT = requestPT;
		}
		
		

}
