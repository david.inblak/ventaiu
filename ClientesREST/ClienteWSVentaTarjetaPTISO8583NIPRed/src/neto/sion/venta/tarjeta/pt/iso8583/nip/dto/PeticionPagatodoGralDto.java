package neto.sion.venta.tarjeta.pt.iso8583.nip.dto;

public class PeticionPagatodoGralDto extends PeticionBaseVentaDto {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String idAgente;
    private String fechaHoraTransaccion;
    private String idGrupo;
    private String idSucursal;
    private String idTerminal;
    private String idOperador;
    private String noTicket;
    private String sku;
    private String referencia;
    private String idCliente;
    private String monto;
    private String formaLectura;
    private String noConfirmacion;
    private String noSecUnicoPT;
    private String categoria;
    private String[] listaProductos;


    public String getIdAgente() {
            return idAgente;
    }
    public void setIdAgente(String idAgente) {
            this.idAgente = idAgente;
    }
    public String getFechaHoraTransaccion() {
            return fechaHoraTransaccion;
    }
    public void setFechaHoraTransaccion(String fechaHoraTransaccion) {
            this.fechaHoraTransaccion = fechaHoraTransaccion;
    }
    public String getIdGrupo() {
            return idGrupo;
    }
    public void setIdGrupo(String idGrupo) {
            this.idGrupo = idGrupo;
    }
    public String getIdSucursal() {
            return idSucursal;
    }
    public void setIdSucursal(String idSucursal) {
            this.idSucursal = idSucursal;
    }
    public String getIdTerminal() {
            return idTerminal;
    }
    public void setIdTerminal(String idTerminal) {
            this.idTerminal = idTerminal;
    }
    public String getIdOperador() {
            return idOperador;
    }
    public void setIdOperador(String idOperador) {
            this.idOperador = idOperador;
    }
    public String getNoTicket() {
            return noTicket;
    }
    public void setNoTicket(String noTicket) {
            this.noTicket = noTicket;
    }
    public String getSku() {
            return sku;
    }
    public void setSku(String sku) {
            this.sku = sku;
    }
    public String getReferencia() {
            return referencia;
    }
    public void setReferencia(String referencia) {
            this.referencia = referencia;
    }
    public String getIdCliente() {
            return idCliente;
    }
    public void setIdCliente(String idCliente) {
            this.idCliente = idCliente;
    }
    public String getMonto() {
            return monto;
    }
    public void setMonto(String monto) {
            this.monto = monto;
    }
    public String getFormaLectura() {
            return formaLectura;
    }
    public void setFormaLectura(String formaLectura) {
            this.formaLectura = formaLectura;
    }
    public String getNoConfirmacion() {
            return noConfirmacion;
    }
    public void setNoConfirmacion(String noConfirmacion) {
            this.noConfirmacion = noConfirmacion;
    }
    public String getNoSecUnicoPT() {
            return noSecUnicoPT;
    }
    public void setNoSecUnicoPT(String noSecUnicoPT) {
            this.noSecUnicoPT = noSecUnicoPT;
    }
    public String getCategoria() {
            return categoria;
    }
    public void setCategoria(String categoria) {
            this.categoria = categoria;
    }
    public String[] getListaProductos() {
            return listaProductos;
    }
    public void setListaProductos(String[] listaProductos) {
            this.listaProductos = listaProductos;
    }

    @Override
    public String toString() {
        return "PeticionPagatodoGralDto{" + "idAgente=" + idAgente + ", fechaHoraTransaccion=" + fechaHoraTransaccion + ", idGrupo=" + idGrupo + ", idSucursal=" + idSucursal + ", idTerminal=" + idTerminal + ", idOperador=" + idOperador + ", noTicket=" + noTicket + ", sku=" + sku + ", referencia=" + referencia + ", idCliente=" + idCliente + ", monto=" + monto + ", formaLectura=" + formaLectura + ", noConfirmacion=" + noConfirmacion + ", noSecUnicoPT=" + noSecUnicoPT + ", categoria=" + categoria + ", listaProductos=" + listaProductos + '}';
    }

    
}
