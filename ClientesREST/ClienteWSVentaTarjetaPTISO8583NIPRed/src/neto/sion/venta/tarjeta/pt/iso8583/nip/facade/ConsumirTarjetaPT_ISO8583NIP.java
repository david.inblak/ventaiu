package neto.sion.venta.tarjeta.pt.iso8583.nip.facade;

import neto.sion.venta.tarjeta.pt.iso8583.nip.dto.*;

public interface ConsumirTarjetaPT_ISO8583NIP {
	public RespuestaPagatodoDto consultaSaldoPTdo ( 
            PeticionPagatodoNipDto _peticionConSaldo) throws WSException;

public RespuestaCombinadaVentaDto registrarVentaTarjetaPTdo(PeticionVentaDto paramPeticionVentaDto, 
																neto.sion.venta.servicios.cliente.dto.PeticionVentaDto paramPeticionVentaDto1, 
																PeticionPagatodoNipDto paramPeticionPagatodoDto) throws WSException;

public RespuestaCombinadaVentaDto registrarVentaTarjetaPagaTodo(
            neto.sion.clientews.tarjeta.dto.PeticionPagoTarjetaDto _peticionVenta, 
            neto.sion.venta.servicios.cliente.dto.PeticionVentaDto _peticionVentaN,
            PeticionPagatodoNipDto _requestPT)
                            throws WSException;

public long consultarTransaccionCentral(
	PeticionTransaccionCentralDto _peticion)
			throws WSException;

}
