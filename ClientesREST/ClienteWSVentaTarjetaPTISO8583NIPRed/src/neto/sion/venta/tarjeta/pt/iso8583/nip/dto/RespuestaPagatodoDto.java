package neto.sion.venta.tarjeta.pt.iso8583.nip.dto;

import org.codehaus.jackson.annotate.JsonProperty;

import neto.sion.venta.tarjeta.pt.iso8583.nip.red.proxy.WSVentaTarjetaPTISO8583NIPRedServiceStub.RespuestaPagatodoNipBeanE;

public class RespuestaPagatodoDto {
	@JsonProperty("rCodigoRespuesta")
    private String rCodigoRespuesta;
	@JsonProperty("rDescRespuesta")
    private String rDescRespuesta;
	@JsonProperty("rDescMsgObligatorio")
    private String rDescMsgObligatorio;
	@JsonProperty("rIdGrupo")
    private String rIdGrupo;
	@JsonProperty("rIdSucursal")
    private String rIdSucursal;
	@JsonProperty("rIdTerminal")
    private String rIdTerminal;
	@JsonProperty("rIdOperador")
    private String rIdOperador;
	@JsonProperty("rNoTicket")
    private String rNoTicket;
	@JsonProperty("rReferencia")
    private String rReferencia;
	@JsonProperty("rNoConfirmacion")
    private String rNoConfirmacion;
	@JsonProperty("rNoSecUnicoPT")
    private String rNoSecUnicoPT;
	@JsonProperty("rNoMsgObligatorio")
    private String rNoMsgObligatorio;
	@JsonProperty("rNoMsgPromocion")
    private String rNoMsgPromocion;
	@JsonProperty("rSaldoDisponibleTarjeta")
    private String rSaldoDisponibleTarjeta;
	@JsonProperty("rMontoAbonado")
    private String rMontoAbonado;
	@JsonProperty("rS110")
    private String rS110;

    public String getrCodigoRespuesta() {
        return rCodigoRespuesta;
    }
    public void setrCodigoRespuesta(String rCodigoRespuesta) {
        this.rCodigoRespuesta = rCodigoRespuesta;
    }
    public String getrDescRespuesta() {
        return rDescRespuesta;
    }
    public void setrDescRespuesta(String rDescRespuesta) {
        this.rDescRespuesta = rDescRespuesta;
    }
    public String getrDescMsgObligatorio() {
        return rDescMsgObligatorio;
    }
    public void setrDescMsgObligatorio(String rDescMsgObligatorio) {
        this.rDescMsgObligatorio = rDescMsgObligatorio;
    }
    public String getrIdGrupo() {
        return rIdGrupo;
    }
    public void setrIdGrupo(String rIdGrupo) {
        this.rIdGrupo = rIdGrupo;
    }
    public String getrIdSucursal() {
        return rIdSucursal;
    }
    public void setrIdSucursal(String rIdSucursal) {
        this.rIdSucursal = rIdSucursal;
    }
    public String getrIdTerminal() {
        return rIdTerminal;
    }
    public void setrIdTerminal(String rIdTerminal) {
        this.rIdTerminal = rIdTerminal;
    }
    public String getrIdOperador() {
        return rIdOperador;
    }
    public void setrIdOperador(String rIdOperador) {
        this.rIdOperador = rIdOperador;
    }
    public String getrNoTicket() {
        return rNoTicket;
    }
    public void setrNoTicket(String rNoTicket) {
        this.rNoTicket = rNoTicket;
    }
    public String getrReferencia() {
        return rReferencia;
    }
    public void setrReferencia(String rReferencia) {
        this.rReferencia = rReferencia;
    }
    public String getrNoConfirmacion() {
        return rNoConfirmacion;
    }
    public void setrNoConfirmacion(String rNoConfirmacion) {
        this.rNoConfirmacion = rNoConfirmacion;
    }
    public String getrNoSecUnicoPT() {
        return rNoSecUnicoPT;
    }
    public void setrNoSecUnicoPT(String rNoSecUnicoPT) {
        this.rNoSecUnicoPT = rNoSecUnicoPT;
    }
    public String getrNoMsgObligatorio() {
        return rNoMsgObligatorio;
    }
    public void setrNoMsgObligatorio(String rNoMsgObligatorio) {
        this.rNoMsgObligatorio = rNoMsgObligatorio;
    }
    public String getrNoMsgPromocion() {
        return rNoMsgPromocion;
    }
    public void setrNoMsgPromocion(String rNoMsgPromocion) {
        this.rNoMsgPromocion = rNoMsgPromocion;
    }
    public String getrSaldoDisponibleTarjeta() {
        return rSaldoDisponibleTarjeta;
    }
    public void setrSaldoDisponibleTarjeta(String rSaldoDisponibleTarjeta) {
        this.rSaldoDisponibleTarjeta = rSaldoDisponibleTarjeta;
    }
    public String getrMontoAbonado() {
        return rMontoAbonado;
    }
    public void setrMontoAbonado(String rMontoAbonado) {
        this.rMontoAbonado = rMontoAbonado;
    }
    public String getrS110() {
        return rS110;
    }
    public void setrS110(String rS110) {
        this.rS110 = rS110;
    }

    @Override
    public String toString() {
        return "RespuestaPagatodoDto{" + "rCodigoRespuesta=" + rCodigoRespuesta + ", rDescRespuesta=" + rDescRespuesta + ", rDescMsgObligatorio=" + rDescMsgObligatorio + ", rIdGrupo=" + rIdGrupo + ", rIdSucursal=" + rIdSucursal + ", rIdTerminal=" + rIdTerminal + ", rIdOperador=" + rIdOperador + ", rNoTicket=" + rNoTicket + ", rReferencia=" + rReferencia + ", rNoConfirmacion=" + rNoConfirmacion + ", rNoSecUnicoPT=" + rNoSecUnicoPT + ", rNoMsgObligatorio=" + rNoMsgObligatorio + ", rNoMsgPromocion=" + rNoMsgPromocion + ", rSaldoDisponibleTarjeta=" + rSaldoDisponibleTarjeta + ", rMontoAbonado=" + rMontoAbonado + ", rS110=" + rS110 + '}';
    }
    
    
    
    public void crearRespuesta(RespuestaPagatodoDto metRespuesta) {
        this.setrCodigoRespuesta		(metRespuesta.getrCodigoRespuesta			());
        this.setrDescRespuesta			(metRespuesta.getrDescRespuesta				());
        this.setrDescMsgObligatorio		(metRespuesta.getrDescMsgObligatorio                    ());
        this.setrIdGrupo			(metRespuesta.getrIdGrupo				());
        this.setrIdSucursal			(metRespuesta.getrIdSucursal				());
        this.setrIdTerminal			(metRespuesta.getrIdTerminal				());
        this.setrIdOperador			(metRespuesta.getrIdOperador				());
        this.setrNoTicket			(metRespuesta.getrNoTicket				());
        this.setrReferencia			(metRespuesta.getrReferencia				());
        this.setrNoConfirmacion			(metRespuesta.getrNoConfirmacion			());
        this.setrNoSecUnicoPT			(metRespuesta.getrNoSecUnicoPT				());
        this.setrNoMsgObligatorio		(metRespuesta.getrNoMsgObligatorio			());
        this.setrNoMsgPromocion			(metRespuesta.getrNoMsgPromocion			());
        this.setrSaldoDisponibleTarjeta         (metRespuesta.getrSaldoDisponibleTarjeta                ());
        this.setrMontoAbonado			(metRespuesta.getrMontoAbonado				());
        this.setrS110				(metRespuesta.getrS110					());
    }

    public void crearRespuesta(RespuestaPagatodoNipBeanE metRespuesta) {
        this.setrCodigoRespuesta		(metRespuesta.getRCodigoRespuesta			());
        this.setrDescRespuesta			(metRespuesta.getRDescRespuesta				());
        this.setrDescMsgObligatorio		(metRespuesta.getRDescMsgObligatorio                    ());
        this.setrIdGrupo				(metRespuesta.getRIdGrupo				());
        this.setrIdSucursal				(metRespuesta.getRIdSucursal				());
        this.setrIdTerminal				(metRespuesta.getRIdTerminal				());
        this.setrIdOperador				(metRespuesta.getRIdOperador				());
        this.setrNoTicket				(metRespuesta.getRNoTicket				());
        this.setrReferencia				(metRespuesta.getRReferencia				());
        this.setrNoConfirmacion			(metRespuesta.getRNoConfirmacion			());
        this.setrNoSecUnicoPT			(metRespuesta.getRNoSecUnicoPT				());
        this.setrNoMsgObligatorio		(metRespuesta.getRNoMsgObligatorio			());
        this.setrNoMsgPromocion			(metRespuesta.getRNoMsgPromocion			());
        this.setrSaldoDisponibleTarjeta	(metRespuesta.getRSaldoDisponibleTarjeta                ());
        this.setrMontoAbonado			(metRespuesta.getRMontoAbonado				());
        this.setrS110					(metRespuesta.getRS110					());
    }
}
